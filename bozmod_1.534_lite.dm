#modname "bozmod 1.534 lite"
#description "This mod adds a lot of new content. The mod is also in YAML format, meaning that you can enable YAML syntax in your favorite text editor (e.g. VS Code) and have an easily traversible mod
If you like this mod, support my other game development! Lookup Dominion Master on discord or Patreon.

This mod must be loaded AFTER loading Dominions Enhanced and when specifying mods to Clockwork, make sure that this mod comes after DE sequentially. Otherwise you will get errors.
Author Boz, Warpman and Executor
Contributors Red Rob, Lucid, Zewwy, BING_XI_LAO, GFSnl, Megu, Elite Zeon, Pymous (http://www.pymous.com), Pymous (http://www.pymous.com)"
#icon "./bozmod/Bozmod_banner_Lite.tga"
#version 1.534

--THIS FILE IS IN YAML FORMAT, to browse it nicely - open it in VS code and click on the markdown button at the bottom, select YAML and away you go. More details here
--https://code.visualstudio.com/docs/languages/overview#_changing-the-language-for-the-selected-file
-- PATCH NOTES
    -- PATCH NOTES 1.534 -> 1.535
        -- Edranor Tome of concealment is now tome of illusions.
        -- spell at level 0 to summon large amounts of phantasmal infantry
    -- PATCH NOTES 1.533 -> 1.534
        -- Venedia and Dom Daniel added back in
        -- Juhera  
            -- moved to MA
            -- given better forts
            -- UW build forts
        -- Item id usage optimize
            -- sling of accuracy (id 17)
            -- axe of sharpness (id 22)
            -- rat tail (id 57)
            -- enchanted helm (id 169)
            -- enchanted ring mail (id 209)
            -- medalion of vengeance (id 298)
            -- jesus boots of water walking (id 432)
        -- U items work properly now
        -- Phlegra 
            -- polemarch automata cost up to 20
            -- removed dominion from Courts
            -- added domconflict to Palaces you get in capitals (CONQUER MORE CAPITALS!)
        -- EA Catfish
            -- Pearl item (DE) now blesses owner 
        -- Krieg 
            -- assassin and blood sniffer now look different 
            -- removed villain from roster 
            -- cultists weapons order switched
        -- Nihialas removed. ALL OF THEM.
        -- ongpwhatever removed.
        -- albion removed. 
        -- bhod, chako, sawaiwhatever removed
        -- cleared all the removed nations items spells and sites, 
        -- added juhera to bozmod folder
        -- fixed slayer of apostles old age bug and sprites 
        -- fixed shrimp queen pretender magenta sprite issues
        -- fixed the atrocious shiva sprite from extra pretenders 
        -- fixed cangob pup idle sprite canvas size Dimensions
        -- 

    -- PATCH NOTES 1.532 -> 1.533
        -- Ledan 
            -- added the nation
            -- initial attempt to balance out pretenders for Dominions 5 level from dominions 4...
            -- added some evocation spells so not all their national stuff is summons only
        -- U 
            -- added the nation
            -- items changed so only U has them.
            -- Song of Palm and Foot to level 5
            -- removed all the versions of Dreams of Rlyeh except for 1
        -- TC
            -- Minister of rituals event generation buffed
        -- Cangobia
            -- Thunderspeaker leveled with Draconspeaker (they are meant to be roughly equivalent in stats and specials)
            -- Cangoboomers level 4 -> 5 and instead of 6 per cast you get 4.
            -- 1 less fire gem income
        -- Man
            -- Add knight god and cheapified him to MA and LA Man
        -- Monkeys
            -- Bandar King cheap for monkeys all ages and Lanka
            -- Add skeleton hand to Lanka
        -- Aspho
            -- pandemoniacs now have hidden holy paths so that twiceborn gives you proper carrion lords
            -- vine dudes have in-built pre-battle cast
            -- national items added 
            -- spell to get carrion chaff as commander 
            -- extra misc slots for commanders to get all the tentacles 
        -- ITEMS
            -- Spell casting items got a tiny price reduction in general
            -- Some items related to luck got fortunebringer now 
        -- GENERAL
            -- some typos fixed, including magicboost
            -- thrones now get 1-2 guardians that stay hidden and remove candles until you patrol them out and kill them 
            -- thrones get thematic commander protection 
        -- PRETENDERS
            -- blood dragon now has D1B1 paths
            -- Black dragon's precision from 12 to 5
            -- Megalodon land shape has proper ID now, reduced the popkill 
            -- Kong point reduction for bandar improved to -40
            -- man gets the knight trinity 
            -- skeleton arm now poor amphib to get thero to use it 
            -- psydragon and dracoboloth reapplied to nations 
            -- hands hp reducded, heal applied
            -- divine anvil forge bonus improved to have niche over Hephaestos
            -- ma ermor senator now gets extra prod\gold with respective scales, domchance reduced to 10>7
            -- slayer of apostles trinity now reanimates dead 
            -- blacksteel colossus prot 2 points up, paths reversed to E2 F1 to match other collossi 
            -- sea star nerfed - only misc slots 
            -- gigajug buffed - extra misc slots
            -- destroyer of worlds, saddest 4-armed dude gets +1 att def 
            -- rlyeh pile of slag pretender gets regeneration 1%
            -- brazen bull gets blood 1
            -- kami of storm gets stormpower 3            
        -- Lemuria
            -- Censor natprot leveled with melee lemurs, spawns one licktor as micro bodyguard
        
        -- Catfish
            -- Catfish got crowns now
            -- catfish immobile needs no infrastructure and has Astral
            -- returned dracobolith
        -- Krieg 
            -- new pretender, basically super-arch heretic 
            -- new hero, fallen angel lady
            -- spell that makes a temple from blood slaves
            -- summons got more expensive
            -- armed gorillas
            -- sprites got a facelift
            -- can now summon efreet with fire gems
            -- three new items, defensive, offensive and protective miscs 
            -- reality breaches now dont give fire to avoid efreet spam
            -- labs now explode only if you try using them
            -- imps cant spawn on your newly conquered fort
            -- doggo no longer have upkeep
            -- demoniac have candles\20 chance to also get tier3 freespawn
            -- ring of corruption bumped to const 4 to avoid early prophet corruptions
        -- SPELLS 
            -- become low demon 
            -- become demon 
            -- transcend flesh 
        -- ITEMS 
            -- some items now have extra events effect 
            -- hardwood club and berserker pelt get additional stat benefits

    -- PATCH NOTES 1.531 - 1.532
        -- MA Ermor
            -- Events tweaked to be easier to no longer require pop
    -- PATCH NOTES 1.531 -> 1.532
        -- MA Ermor
            -- Death bless 1 DONE
            -- Base death 0 DONE
            -- Given purple robe item DONE
            -- Lictors given lictor axes DONE
            -- events for mages and ermorian cultists DONE
                -- magic3/death3/lab domchance 2 -> spectral mage DONE
                -- magic1/death1/temple domchance 5 -> ermorian cultist DONE
        -- GENERAL
            -- Forest fire now generates fire elementals correctly DONE
            -- Tortuga cannon PD now correctly has cannons - DONE
            -- Cangob egg hatches fixed, werent working correctly before - DONE
            -- Descriptions on dragons updated - DONE
            -- Description on virulent priest updated - DONE
        -- Cangobia
            -- Crown of Devouring 50 popkill 100 supply - DONE
            -- Now gets Draconfuries - DONE
            -- Soldiers spears downgraded, given hide shields and javelins - DONE
            -- cangobs given throw rocks - DONE
    -- PATCH NOTES 1.530 -> 1.531
            -- GENERAL 
                -- Yomi gets the evil hand instead of good one.
                -- Krieg Chonker hero summons fixed, now gets dogs properly
                -- MA Ulm gets national discount on flamethrowers 
                -- Grunts now berserk properly 
                -- lab blowing up imps now temporary, no longer clugging the forts 
                -- human gacha now gets you stealthy commanders regardless of domscore
                -- Phlegran sites no longer cause yeti mountain to appear
    -- PATCH NOTES 1.528 -> 1.530
            -- GENERAL
                -- Azi no longer changed, added Bane Dragon
                -- senator events silent 
                -- undead national senator versions now also work, albeit only in forts
                -- undead national senators now properly immortal and also recouperate  
                -- dragon of virtue can now shapechange when wounded in humanform like normal dragons and has woundfend    
                -- slayer of apostles buffed, now can cast better 
                -- godzilla buffed, now melts forts 10% chance and casts faster
                -- Icon (juggernaut pretender) now spreads dom like a juggernaut and has innate caster, so that it can cast while trampling stuff
                -- squidragon and dracoboleth get +1S bonus. does not add bless points!
                -- skeleton on throne generates some chaff across the empire 
                -- rainbow diviner now can divine
                -- megalodon gets water and sharkman shape
                -- most nations got thematic god cost reductions
                -- spring and chaos sneks got tiny buffs (chaospower and N1 respectively)
                -- king in yellow cost by 40
                -- black dragon cost up by 10
                -- mega efreet site now spawns correctly
                -- lady of whips now has #seduce 
                -- thief of fortune now punishes L3 more harshly and blesses Misf3 more generously
                -- banefire dragon added
                -- god hand added
                -- skelly hand added
                -- Ind no longer has fairy dragon, no dompower1 allowed!
                -- woundfend values lowered to realistic levels
                -- most pretenders got DE recuperation (with notable exception of knight trinity)
                -- mushushu buffed to dragon levels
                -- kong secondshape now has attack value previously missing
                -- knight trinity #dompower was bugged, now they have order power
             -- Krieg
                -- fort archers changed to gogs
                -- gogs cheaper
                -- gogs no longer AoE, just shoot fire bolts now
                -- pd changes, from imps to dogs 
                -- wall archers now cultists
                -- wall archer bonuses provided by gates reduced to gogs and magogs
                -- Flaming horses buffed, now more survivable 
                -- grunts got berserk now
                -- event causing imps to sometimes join enemy siege forces removed
                -- Dominatrix shapechange now has #sesuce not #succubus
                --hellhounds and purebreed hellhounds gold cost reduced, rp cost buffed to make them "order" gameplan troops
            -- MA Ermor
                -- recruitable soldiers no longer require RP to be recruited
            -- Cangob
                -- Dinosaur with no rider no longer leads troops
            -- Phlegra 
                up to 0.49 version, including
                ironclad pikemen when you want a chud sized pike
                guilders replaced with resource buying
                added magus mini researcher
                fixed dreadnaught transform
                transformation for 5-batch now needs preachers
                industrial sites per fort instead of "anywhere guild sites"
                basileus lose W1
                magistrates lose holy
                judge loses inquisitor
                spy down to S1
                hero head got proper bodyguard and slots
                pop back to normal values
                polemarchs now can make line
                pikemen now need more rp and less gold, mirror from halberdeers
                grenadiers cheaper in resources, more rp cost
                smiths 10 gold cheaper, lose death random
                not_oppressor_generals now 175 gold not 200
                polemarch automata hp and natprot up, closer to clockwork creature levels
                aoe mindburn now scales aoe with mage level
    -- PATCH NOTES 1.527 -> 1.528
            -- GENERAL
                -- disabled nations moved to EA
    -- PATCH NOTES 1/526 -> 1.527
            -- GENERAL
                -- Dwarven hammer 15E to 10E, forge bonus 2 -> 1
                -- Dwarven lord hammer 20E, forge bonus 2, cursed.
            -- Krieg
                -- Demoniacs ID fixed
    -- PATCH NOTES 1.525b -> 1.526
            -- GENERAL 
                -- incorporated OneAge DE and changed descriptions on removed nations.
                -- Most Dragons got darkvision
                -- Fairy dragon got spirit sight
    -- PATCH NOTES 1.525 -> 1.525b
            -- Hotfix
                -- remove meditation/bless from raider captains, doesn't work
    -- PATCH NOTES 1.524 -> 1.525
        -- Cangobia
            -- Chieftans got raider
            -- egg sacrifice domchance 4 -> 2, puppies 4d6 -> 2d6, no longer shape changing
            -- Kinslayer Axe from F2 -> F3.. 7 -> 12 gems
            -- Remove draconic totem
            -- breeders got domsummon2 back
            -- Longneck cost 12 -> 11
            -- PD fixes / buffs
    -- PATCH NOTES 1.523 -> 1.524
        -- MA Ermor
            -- added reclimited little mining units (like undead miners of diamedulla)
            -- forests spawn undead animals
            -- new events
                -- farms with 10k+, labs, death 3 and magic 1 have a domchance * 10 to attract montag weight 3 (necromancer/conjurer/plague cult leader) and montag weight 1 (Sorcerer, Black Witch, Circle Master)
                -- labs, death 3, magic 1 have a domchance to attract montag weight 3 (necromancer/conjurer/plague cult leader)
            -- they should have black plate longdead and soulless that cost resources only
            -- they should have cheap summonable plague zombies that have reanimator
            -- they should have a spell that buffs their soulless to have disease aura
            -- they should have a spell that buffs their longdead to have withering weapon
    -- PATCH NOTES 1.1522 -> 1.1523
        -- CANGOBIA
            -- Tyrant summon now summons just normal Tyrants (non-commander version)
            -- Cangobs revert back to using XP shape to transform
    -- PATCH NOTES 1.1521 -> 1.522
        -- GENERAL    
            -- LA Lemuria added to MA
        -- CANGOBIA
            -- Mothers got startage 6
            -- Crazies can spawn off normal Cangobs too
            -- Crazies random cast 40 -> 80
            -- Crazies fast cast 20
            -- Mothers 95 -> 90 gold
    -- PATCH NOTES 1.1520 -> 1.521
        -- CANGOBIA
            -- Younglings got startage 1
    -- PATCH NOTES 1.159 -> 1.520
        -- CANGOBIA
            -- Breeders 80 -> 95
            -- Crazies spawn chance halved
            -- Puppies need 7 not 9 hp to grow
            -- Raid leaders get fur armor
            -- Cangobs got pillage bonus
    -- PATCH NOTES 1.157 -> 1.519
        -- GENERAL 
            -- EA Pangaea and EA Ermor moved to MA
        -- CANGOBIA
            -- Added cangob stalker assassin
            -- MR buffed on most things
            -- Progenitor lost swallow, gained banish returning 30
            -- Descriptions updated for many things
            -- Stegadons lost siege bonus
            -- gave Cangobia bonus vs larger axe
            -- buffed morale/MR on Cangobs
            -- Cangob crazy chance doubled to 2x domchance
            -- MOthers got makemonster4 for pups
            -- pups now corpse-eater grow into cangobs instead of xp shape
            -- Added Mycomagia fort thing
    -- PATCH NOTES 1.516 -> 1.517
        -- GENERAL
            -- Forest fire now makes fire elementals if you have a F2 mage present, doesn't require summon anymore - this is to prevent a bug in dominions that happens when a recruit site is no longer present disrupting other orders
        -- TORTUGA
            -- Feyseer gold 85 -> 70
            -- Black hand Pirate MR 10 -> 11
            -- Damned Port 50>25 gold, 3>1 Death gems
            -- Black Hand Pirate 40 -> 45 gold        
            -- Scallywag Standard received 3 poison resistance
            -- Sea Witch 2>4 RP
            -- Cannoneers limited to 1/turn
            -- Damned/Cursed Cannoneers siege strength 25>10
            -- Cannoneer siege strength 25>20
            -- Cursed pirates resource prices 30 -> 35
            -- Cannoneer siege strength 25>20
            -- Cursed pirates resource prices increased
            -- Ghost Ship 25>35 gems
            -- Cursed Cannoneer recruitment point price 75>44
    -- PATCH NOTES 1.514 -> 1.516
        -- GENERAL
            -- Removed Floating market, Magic mine, Golden goose
        -- PRETENDERS        
            -- First efreet cost 280 -> 200, ability to transform changed and documented in the description
            -- Overlord 180 -> 200, size 2->3, hp 25->32
            -- Lady of whips 269 -> 250, 1 less lifesteal attack, size 5->4, 69 hp->49 
            -- Horned reaper -> remove events, remove 2nd form, 234->230, unsurround 6->1.
        -- CANGOBIA
            -- Progenitor moved to levle 9 spell for Cangobs
            -- Moutainspine removed
            -- temples -> 600 gold and 1 fire gem
            -- Raptor rider 40 gp -> 45
            -- Breeders 3d6 pups -> 2d6
            -- Draconic totems removed
            -- Crown of warding and exploding mushrooms cheaper
            -- Dragonwax F2 -> F4 , 10 -> 20 gems
            -- Cangob crazies spawn chance from 8% to domchance
            -- Breeders AND mothers have 5 healing not just mothers
            -- Mothers sprite corrected
            -- Draconguard cost 72 -> 90
            -- Multiheroes magic paths nerfed
            -- Redskull and Dragonslaying spider no longer appear in swarm/creeping doom casts
        -- KRIEG
            -- better descriptions on some units with event triggers
            -- pop-gain events only tied to demonic settlements
            -- small stat/ability tweaks on many units, most notable is movement buff on dogs and boulders/siege bonus on gorillas
            -- reality breach req magic 2 > 1
        -- CONFLUENCE
            -- Tortuga
                -- PD cannons now commanders so they don't scale
            -- Diamedulla
                -- Noxious Mortar 5->0 damage
                -- Flesh Golem 10->8 insanity
                -- Mad alchemist startage 40->44, random spell 10%
                -- Temporal clock cast speed bonus 30->25, Enucmbrance +3, RP 5->3
                -- Jarheads encumbrance 0->2
                -- Phoenix encumbrance 0->2
                -- Collector encumbrance 0->2
                -- Virulent priest  gold cost 55>60, received poor undead leader, regular > poor leadership
                -- Reapers stat change, 18>16 HP, 14>13 att, 13>11 def, 13>14 morale, 18>24 Blood Slave price, research level 4>5 (can be compared to something like Se'ir, though this is not a bless nation)
-- TODO
    -- NATIONS TO ADD
        -- U, Belly of the World DONE
        -- Flying amphibious lightning shooting angelfish
        -- Hivemind elves
        -- Krugos, dark spires - nazi necromancers
        -- Warhammer Dark elves, skaven
        -- Pymous blue disease guys and penguins
    -- TNN
        -- Butterfly Knight    
    -- PANGAEA 
        -- Forge of Bronze - gives resources and recruit access to bronze centaurs of MA
        -- Forge of Iron - gives resources and recruit access to iron units of LA
        -- Arcane Grove - pearl/nature gen and access to centaur sage of LA
    -- GENERAL
        -- better sprite for Scorpion king + give him body slot
        -- add butterfly knight to TNN, Eriu, Man, Pan, EA Ulm and Marverni
        -- Hellpower mushroom 4 cangobi
        -- barbarian horsemen trinity AED with big monster pets 
        -- Skratti pretender should trnsform 2 giant wolf
        -- horadric cube summon that eats research items and gives stuff.
        -- tornado spell
        -- prey mantis pretender
        -- butterfly knight
        -- Horsetaur
        -- Mecha Kaiju pretender
        -- evocation summon deathexploding ball
    -- CANGOBIA
        -- new unique hero that originated the Draconguard (telling story)
        -- new unique hero dragon that is super old and doesnt use magic
        -- new unique T-rex rider hero with cannons, large-slayer spear, fear 15
-- ID Ranges
    --7000-8000 creatures
    --700-730 weapons
    --500-600 items
    --3500+ for spells
    --1400-1410 + 1690-1720 sites
    --13300-13400 + 7000-7020 montags
    --601 eff_id for events
    --575-600 ench_id for events

    -- NEW PRETENDERS 
    -- TITANS
        #newmonster 4909
        #name "Lord of Pearls"
        #spr1 "./ExtraPretenders/PearlLord.tga"
        #spr2 "./ExtraPretenders/PearlLord2.tga"
        #descr "The Lord of Pearls is an ancient Pearl King who has taken the role of a Pretender God. Each month he will collect a magical pearl and can create more using water gems. He is a master of magic and can be adept in several magic paths. Like all tritons the Lord of Pearls is aquatic and cannot leave the deeps."
        #djinn
        #gcost 120
        #size 4
        #hp 55
        #prot 4
        #mr 18
        #mor 30
        #str 15
        #att 12
        #def 12
        #prec 13
        #enc 3
        #mapmove 2
        #ap 22
        #magicskill 4 1
        #gemprod 4 1
        #makepearls 10
        #weapon 151 -- Wand
        #expertleader
        #aquatic
        #heal
        #diseaseres 100
        #maxage 600
        #startage 400
        #pathcost 10
        #startdom 1
        #end

        #newmonster 4910
        #copystats 481 -- High Magus
        #clearmagic
        #clearweapons
        #name "Stone Magus"
        #spr1 "./ExtraPretenders/StoneMagus.tga"
        #spr2 "./ExtraPretenders/StoneMagus2.tga"
        #descr "The Stone Magus is a mage who has attained such power that he has mastered his own mortality. Donning a godly mantle, he has taken the role of a Pretender God. The Stone Magus is a master of magic and can be adept in several magic paths. He is a master mason and is able to create great architectural wonders."
        #humanoid
        #gcost 150
        #hp 10
        #mapmove 2
        #ap 12
        #str 13
        #att 10
        #def 10
        #magicskill 3 1
        #weapon 648 -- Enchanted Hammer
        #startage 250
        #maxage 500
        #goodleader
        #goodmagicleader
        #mason
        #heal
        #diseaseres 100
        #startdom 1
        #pathcost 10
        #homerealm 10
        #noslowrec
        #end

        #newmonster 4911
        #copystats 122 -- Bloodhenge Druid 
        #clearmagic
        #clearweapons
        #name "Archdruid of Bloodhenge"
        #spr1 "./ExtraPretenders/ArchBloodHenge.tga"
        #spr2 "./ExtraPretenders/ArchBloodHenge2.tga"
        #descr "The Archdruid of Bloodhenge is a druid of such great power that he has mastered his own mortality. Donning a godly mantle, he has taken the role of a Pretender God. He is a master of magic and can be adept in several magic paths. As the most senior of the Bloodhenge druids he is an expert at rooting out suitable sacrifices and leads bloody rituals carried out far away from civilization. The Archdruid rides a magical unicorn steed."
        #humanoid
        #gcost 120
        #size 3
        #hp 12
        #str 10
        #att 10
        #def 12
        #enc 4
        #mapmove 3
        #ap 22
        #magicskill 6 1
        #magicskill 7 1
        #goodleader
        #okundeadleader
        #heal
        #diseaseres 100
        #douse 3
        #weapon 523 -- Golden Sickle
        #weapon 330 -- Alicorn
        #mounted
        #itemslots 13446 -- No feet
        #startdom 1
        #pathcost 10
        #homerealm 2 -- Celtic
        #end

        #newmonster 4912
        #copystats 314 -- Mandragora
        #clearmagic
        #clearspec
        #name "First Mandragora"
        #spr1 "./ExtraPretenders/FirstMandragora.tga"
        #spr2 "./ExtraPretenders/FirstMandragora2.tga"
        #descr "The First Mandragora is an ancient collection of bones reanimated by living vines and roots. Long ago a dark presence stirred in the deep forest and was roused to defend the wild. A primal manifestation of the cycle of life and death, it was soon worshipped by those living in the forests. The First Mandragora is magically powerful and manikins will animate to serve it. Its vines strike like whips and their touch can make men fall asleep. The presence of the First Mandragora will spread a sleeping sickness, and nearby enemies may fall into a dreamless slumber. In lands free of civilization it will grow stronger, but it will weaken where men toil."
        #gcost 130
        #humanoid
        #att 7
        #def 7
        #str 13
        #ap 8
        #enc 3
        #slothpower 1
        #sleepaura 6
        #pooramphibian
        #pierceres
        #magicbeing
        #neednoteat
        #forestsurvival
        #spiritsight
        #poisonres 25
        #plant
        #mr 18
        #mor 30
        #magicskill 5 1
        #magicskill 6 1
        #goodleader
        #heal
        #diseaseres 100
        #domsummon2 313 -- Manikin
        #pathcost 10
        #startdom 1
        #end

        #newmonster 4913
        #copystats 2419 -- Mermage
        #clearmagic
        #clearweapons
        #name "Master of the Deeps"
        #spr1 "./ExtraPretenders/MasterDeeps.tga"
        #spr2 "./ExtraPretenders/MasterDeeps.tga"
        #descr "The Master of the Deeps is a Mermage who has attained such power that he has mastered his own mortality. Donning a godly mantle, he has taken the role of a Pretender God. The Master of the Deeps is a master of magic and can be adept in several magic paths. He is a master of both the realm of air and of water and can leave the sea."
        #humanoid
        #gcost 120
        #mr 18
        #mor 30
        #magicskill 1 1
        #magicskill 2 1
        #goodleader
        #heal
        #diseaseres 100
        #landshape 4914
        #weapon 238
        #pathcost 10
        #startdom 1
        #homerealm 9 -- Deeps
        #end

        #newmonster 4914
        #copystats 2420 -- Mermage Landshape
        #clearmagic
        #clearweapons
        #name "Master of the Deeps"
        #spr1 "./ExtraPretenders/MasterDeeps.tga"
        #spr2 "./ExtraPretenders/MasterDeeps.tga"
        #descr "The Master of the Deeps is a Mermage who has attained such power that he has mastered his own mortality. Donning a godly mantle, he has taken the role of a Pretender God. The Master of the Deeps is a master of magic and can be adept in several magic paths. He is a master of both the realm of air and of water and can leave the sea."
        #humanoid
        #gcost 120
        #mr 18
        #mor 30
        #magicskill 1 1
        #magicskill 2 1
        #goodleader
        #heal
        #diseaseres 100
        #watershape 4913
        #weapon 238
        #pathcost 10
        #startdom 1
        #homerealm 0
        #end

        #newmonster 4915
        #spr1 "./ExtraPretenders/Tengukunshu.tga"
        #spr2 "./ExtraPretenders/Tengukunshu2.tga"
        #name "Tengu Kunshu"
        #descr "The Tengu Kunshu is a Tengu King who has grown powerful enough to master his mortality and claim divinity. He is a skilled mage and Kohoha Tengu serve him and can be called each month. He is attended by Tengu Warriors and one will occasionally appear to serve him.  The Tengu Kunshu is a master of magic and can be adept in several magic paths."
        #humanoid
        #gcost 100
        #hp 15
        #size 3
        #mr 18
        #mor 30
        #str 12
        #att 14
        #def 14
        #prec 14
        #enc 2
        #mapmove 3
        #ap 10
        #expertleader
        #maxage 500
        #flying
        #stormimmune
        #mountainsurvival
        #heal
        #diseaseres 100
        #weapon 7 -- quarterstaff
        #weapon 243 -- lightning
        #armor 5 -- leather cuirass
        #armor 148
        #magicskill 1 1
        #makemonsters1 1479 -- Konoha Tengu
        #raredomsummon 1481 -- Tengu Warrior
        #startdom 1
        #pathcost 20
        #end

        #newmonster 4916
        #spr1 "./ExtraPretenders/Archdaktyl.tga"
        #spr2 "./ExtraPretenders/Archdaktyl2.tga"
        #name "Arch Daktyl"
        #descr "The Arch Daktyl was the foremost and most skilled of the servants of the Telkhines. After his lords and masters were imprisoned in Tartarus, he decided to follow in their footsteps and use his impressive skills to claim Godhood for himself. He resides in the capital in a great forge where he can forge weapons and wonders with a skill thought lost to the world. The Arch Daktyl is a master of magic and can be adept in several magic paths."
        #humanoid
        #gcost 120
        #hp 11
        #size 1
        #prot 4
        #mr 18
        #mor 30
        #str 13
        #att 10
        #def 10
        #enc 2
        #mapmove 2
        #ap 6
        #weapon "Enchanted hammer"
        #armor "Crown"
        #armor "Robes"
        #maxage 1500
        #magicbeing
        #amphibian
        #heal
        #diseaseres 100
        #startdom 1
        #pathcost 20
        #mastersmith 1
        #magicskill 3 1
        #nametype 107
        #end

        #newmonster 4917
        #copystats 176 -- Triton
        #clearmagic
        #clearweapons
        #name "Triton Queen"
        #spr1 "./ExtraPretenders/TritonQueen.tga"
        #spr2 "./ExtraPretenders/TritonQueen2.tga"
        #descr "The Triton Queen is a Triton who has attained such power that she has mastered her own mortality. Donning a godly mantle, she has taken the role of a Pretender Goddess. The Triton Queen oversees trade in the renowned Pelagian pearls and will bring additional gold to the treasury each month. She is the figurehead of the Pelagian nation and is revered by all of the triton tribes. The Triton Queen is a master of magic and can be adept in several magic paths."
        #djinn
        #gcost 100
        #mr 18
        #mor 30
        #magicskill 6 1
        #weapon 92 -- Fist
        #armor 148 -- Crown
        #goodleader
        #female
        #heal
        #diseaseres 100
        #gold 30
        #pathcost 10
        #startdom 1
        #end

        #newmonster 4918
        #copystats 1054 -- Siren
        #clearmagic
        #clearweapons
        #name "Siren Sorceress"
        #spr1 "./ExtraPretenders/SirenSorc.tga"
        #spr2 "./ExtraPretenders/SirenSorc2.tga"
        #descr "The Siren Sorceress is a Siren who has attained such power that she has mastered her own mortality. Donning a godly mantle, she has taken the role of a Pretender Goddess. A siren is a magical being of the changing shores. Like most beings of Oceania it is capable of changing shape. While under water, she appears as a beautiful woman with the tail of a fish. When she leaves the sea, she takes the form of a gull with the head and torso of a woman. Sirens are gifted with an enchanting voice and can lure men into the sea with their songs. While hiding in a coastal province, the Siren Sorceress can attempt to lure commanders to a watery grave. The Siren Sorceress is a master of magic and can be adept in several magic paths. While under water, she loses some Air magic skill, and whilst above the waves she will lose some skill in Water magic."
        #djinn
        #gcost 120
        #mr 18
        #mor 30
        #magicskill 2 1
        #magicskill 6 1
        #goodleader
        #heal
        #diseaseres 100
        #landshape 4919
        #pathcost 10
        #startdom 1
        #beckon 16
        #spellsinger
        #chaosrec 0
        #end

        #newmonster 4919
        #copystats 1055 -- Siren Birdform
        #clearmagic
        #clearweapons
        #name "Siren Sorceress"
        #spr1 "./ExtraPretenders/SirenSorcBird.tga"
        #spr2 "./ExtraPretenders/SirenSorcBird2.tga"
        #descr "The Siren Sorceress is a Siren who has attained such power that she has mastered her own mortality. Donning a godly mantle, she has taken the role of a Pretender Goddess. A siren is a magical being of the changing shores. Like most beings of Oceania it is capable of changing shape. While under water, she appears as a beautiful woman with the tail of a fish. When she leaves the sea, she takes the form of a gull with the head and torso of a woman. Sirens are gifted with an enchanting voice and can lure men into the sea with their songs. The Siren Sorceress has long enticed heroes to come to her isle to serve her, and can also attempt to lure commanders to a watery grave while hiding in a coastal province. The Siren Sorceress is a master of magic and can be adept in several magic paths. While under water, she loses some Air magic skill, and whilst above the waves she will lose some skill in Water magic."
        #bird
        #gcost 120
        #mr 18
        #mor 30
        #magicskill 2 1
        #magicskill 6 1
        #goodleader
        #heal
        #diseaseres 100
        #watershape 4918
        #pathcost 10
        #startdom 1
        #homerealm 0
        #beckon 16
        #spellsinger
        #chaosrec 0
        #end

        #newmonster 4920 -- Mermaid Enchantress
        #copystats 1065 -- Merman
        #clearmagic
        #clearweapons
        #name "Mermaid Enchantress"
        #spr1 "./ExtraPretenders/MermaidEnch.tga"
        #spr2 "./ExtraPretenders/MermaidEnch2.tga"
        #descr "The Mermaid Enchantress is a female Merman who has attained such power that she has mastered her own mortality. Donning a godly mantle, she has taken the role of a Pretender Goddess. The Mermaid Enchantress is a master of magic and can be adept in several magic paths. Mermen are amphibious beings related to tritons, but have fish tails instead of legs. Mermen have strange powers of transformation and can remove their tails to walk on dry land. The Mermaid Enchantress dwells in an enchanted undersea garden in the capital."
        #djinn
        #gcost 120
        #mr 18
        #mor 30
        #magicskill 4 1
        #weapon 92 -- Fist
        #armor 148 -- Crown
        #goodleader
        #heal
        #female
        #gemprod 2 1
        #gemprod 4 1
        #diseaseres 100
        #pathcost 10
        #startdom 1
        #homerealm 9
        #landshape 4921
        #end

        #newmonster 4921 -- Mermaid Enchantress Landform
        #copystats 1066 -- Merman Landform
        #clearmagic
        #clearweapons
        #name "Mermaid Enchantress"
        #spr1 "./ExtraPretenders/MermaidEnchLand.tga"
        #spr2 "./ExtraPretenders/MermaidEnchLand2.tga"
        #descr "The Mermaid Enchantress is a female Merman who has attained such power that she has mastered her own mortality. Donning a godly mantle, she has taken the role of a Pretender Goddess. The Mermaid Enchantress is a master of magic and can be adept in several magic paths. Mermen are amphibious beings related to tritons, but have fish tails instead of legs. Mermen have strange powers of transformation and can remove their tails to walk on dry land. The Mermaid Enchantress dwells in an enchanted undersea garden in the capital."
        #humanoid
        #gcost 120
        #mr 18
        #mor 30
        #magicskill 4 1
        #weapon 92 -- Fist
        #armor 148 -- Crown
        #goodleader
        #heal
        #female
        #diseaseres 100
        #pathcost 10
        #gemprod 2 1
        #gemprod 4 1
        #startdom 1
        #homerealm 0
        #watershape 4920
        #end

        #newmonster 4922 -- Wight on Leviathan
        #copystats 1235 -- Leviathan
        #clearweapons
        #name "Lacedon Mage"
        #spr1 "./ExtraPretenders/LacedonMage.tga"
        #spr2 "./ExtraPretenders/LacedonMage2.tga"
        #descr "The Lacedon Mage is the dried husk of a Mage adept in Death magic. Through dark rituals, the mage succeeded in mastering one of mankind's oldest and most urgent goals: to defeat death. Donning a godly mantle, it has taken the role of a Pretender God. The Lacedon Mage rides atop the corpse of a rotting asp turtle, the huge size and heavy armor of the beast making it easy to kill smaller beings by trampling them. The Lacedon Mage is a master of magic and can be adept in several magic paths."
        #humanoid
        #gcost 130
        #hp 35
        #prot 12
        #mr 18
        #mor 30
        #str 12
        #att 11
        #def 11
        #prec 11
        #magicskill 5 2
        #weapon 417 -- Bite
        #weapon 59 -- Rod of Death
        #goodleader
        #okundeadleader
        #mounted
        #heal
        #spiritsight
        #diseaseres 100
        #itemslots 12288 -- No Feet
        #pathcost 20
        #startdom 1
        #homerealm 9
        #landshape 4923
        #secondtmpshape 1235 -- Leviathan
        #end

        #newmonster 4923 -- Wight on Leviathan Landshape
        #copystats 1236 -- Leviathan Landshape
        #clearweapons
        #name "Lacedon Mage"
        #spr1 "./ExtraPretenders/LacedonMageLand.tga"
        #spr2 "./ExtraPretenders/LacedonMageLand2.tga"
        #descr "The Lacedon Mage is the dried husk of a Mage adept in Death magic. Through dark rituals, the mage succeeded in mastering one of mankind's oldest and most urgent goals: to defeat death. Donning a godly mantle, it has taken the role of a Pretender God. The Lacedon Mage rides atop the corpse of a rotting asp turtle, the huge size and heavy armor of the beast making it easy to kill smaller beings by trampling them. The Lacedon Mage is a master of magic and can be adept in several magic paths."
        #humanoid
        #gcost 130
        #hp 35
        #prot 12
        #mr 18
        #mor 30
        #str 12
        #att 11
        #def 11
        #prec 11
        #magicskill 5 2
        #weapon 417 -- Bite
        #weapon 59 -- Rod of Death
        #goodleader
        #okundeadleader
        #mounted
        #heal
        #spiritsight
        #diseaseres 100
        #itemslots 12288 -- No Feet
        #pathcost 20
        #startdom 1
        #homerealm 0
        #watershape 4922
        #secondtmpshape 1236 -- Leviathan
        #end

        #newmonster 4924 -- Sage Queen
        #copystats 813 -- Imperial Consort
        #clearmagic
        #name "Divine Sage Empress"
        #spr1 "./ExtraPretenders/SageQueen.tga"
        #spr2 "./ExtraPretenders/SageQueen2.tga"
        #descr "The Divine Sage Empress is a former Imperial Consort who has risen to the role of Empress. Through long study of ancient texts she has attained such power that she has mastered her own mortality. Now, with her people threatened by subjugation she has donned a godly mantle and taken the role of a Pretender Goddess. A formidable elementalist, her distinct talent is for creating power through innovation, efficiency, and strategy. She has developed methods to not only control raging floods and fires, but also to use them to irrigate the fields and clear new land. She can use the lore of the elements to predict disasters and other bad events and will put in place measures to prevent them. The Divine Sage Empress is a master of magic and can be adept in several magic paths."
        #humanoid
        #gcost 130
        #mr 18
        #mor 30
        #magicskill 0 1
        #magicskill 2 1
        #superiorleader
        #heal
        #diseaseres 100
        #nobadevents 50
        #stealthy 10
        #startage 300
        #maxage 500
        #pathcost 10
        #startdom 1
        #end

        #newmonster 4925 -- Basalt Architect
        #copystats 322 -- King of the Deep
        #clearmagic
        #clearweapons
        #name "Basalt Architect"
        #spr1 "./ExtraPretenders/BasaltArchitect.tga"
        #spr2 "./ExtraPretenders/BasaltArchitect2.tga"
        #descr "The Basalt Architect was the first of the Basalt Kings and was the prime architect of the Basalt City. He has now attained such power that he has mastered his own mortality. Donning a godly mantle, he has taken the role of a Pretender God. The Basalt Architect is a master of magic and can be adept in several magic paths. He can enchant the stone of the city to make great quantities of artifacts, weapons and armor for the Deep Ones."
        #humanoid
        #gcost 150
        #size 5
        #hp 55
        #mr 18
        #mor 30
        #str 21
        #prot 13
        #fear 5
        #fireres 5
        #magicskill 3 1
        #weapon 151 -- Wand
        #resources 40
        #fixforgebonus 2
        #goodleader
        #mason
        #heal
        #twiceborn 3195 -- Wight Mage
        #coldres 5
        #diseaseres 100
        #maxage 2000
        #startage 1000
        #pathcost 10
        #startdom 1
        #end

        #newmonster 5039
        #copystats 481 -- High Magus
        #clearmagic
        #clearweapons
        #name "High Diviner"
        #spr1 "./ExtraPretenders/Wizard.tga"
        #spr2 "./ExtraPretenders/Wizard2.tga"
        #descr "The High Diviner is a sorcerer who has attained such power that he has mastered his own mortality. Donning a godly mantle, he has taken the role of a Pretender God. The Diviner is a master of divination and spying magic and can be adept in several magic paths. He can extend his senses to allow him to cast spells at a range far beyond that of most spellcasters."
        #mountedhumanoid
        #gcost 100
        #hp 10
        #mapmove 3
        #size 3
        #ap 22
        #str 13
        #att 10
        #def 12
        #magicskill 1 1
        #allrange 2
        #weapon 151 -- Wand
        #startage 250
        #maxage 500
        #goodleader
        #goodmagicleader
        #mounted
        #heal
        #spiritsight
        #diseaseres 100
        #startdom 1
        #pathcost 10
        #homerealm 2
        #noslowrec
        #end

        #newmonster 5063
        #copystats 174 -- Triton
        #spr1 "./ExtraPretenders/MerGod.tga"
        #spr2 "./ExtraPretenders/MerGod2.tga"
        #name "Sage of the Sea"
        #descr "The Sage of the Sea is an ancient being that has offered wisdom to many throughout the ages. Now his studies have enabled him to master his own mortality. Donning a Godly mantle he will lead his people as a Pretender God. The Sage is wise beyond imagining and will bestow wisdom on those conducting magical research in the province. He is a master of magic and can be adept in many paths. The Sage is an aquatic being and cannot leave the sea."
        #fixedname "Nereus"
        #djinn
        #gcost 140
        #size 3
        #hp 27
        #mor 30
        #mr 18
        #str 15
        #enc 3
        #def 13
        #ap 14
        #okleader
        #heal
        #diseaseres 100
        #magicskill 4 1
        #magicskill 6 1
        #inspiringres 1
        #researchbonus 10
        #pathcost 20
        #startdom 2
        #homerealm 9 -- Deeps
        #end

        #newmonster 5072
        #copystats 280 -- Seithkona
        #clearmagic
        #clearweapons
        #spr1 "./ExtraPretenders/Norn.tga"
        #spr2 "./ExtraPretenders/Norn2.tga"
        #name "Norn"
        #descr "The Norns are three mysterious beings in the form of robed women that have existing since the dawn of time. They are weavers of fate and guide the lives of men and Gods through their actions and prophesies. The Norns have frail bodies but great magical power that they use to guide the fate of the world. Over time they have been given offerings and worship to try to influence their actions and to bring good luck, however their motives remain inscrutable. With the Pantokrator gone the Norns have now determined that they will take his place to more directly guide the world. All three are skilled in many paths of magic, however one weaves the patterns of the Air, one the strands of the Astral plane and the last knows of the Death of all things."
        #humanoid
        #gcost 130
        #mr 18
        #mor 30
        #goodleader
        #spiritsight
        #heal
        #diseaseres 100
        #weapon 92 -- Fist
        #magicskill 1 1
        #magicskill 4 1
        #magicskill 5 1
        #nobadevents 20
        #triplegod 3 -- A/D/S split
        #triplegodmag 2
        #researchbonus -10
        #startdom 1
        #pathcost 20
        #homerealm 1 -- North
        #end

        #newmonster 5073
        #copystats 280 -- Seithkona
        #clearmagic
        #clearweapons
        #spr1 "./ExtraPretenders/3Maiden.tga"
        #spr2 "./ExtraPretenders/3Maiden2.tga"
        #name "Maiden"
        #descr "In ages past a great Goddess was born in which the power of femininity was embodied in one being. She could bear new life, heal the sick and see future events using divination. Soon after her creation the Pantokrator grew fearful and decided to divide the Goddess into three parts, to more safely contain her power. Each aspect of the Goddess was imprisoned separately to prevent their combined strength from overcoming his bonds. Now, with the Pantokrator gone the shackles are weakening and the Triple Goddess will soon return to guide her daughters once more. The Goddess now resides in three bodies, the Maiden, the Mother and the Crone. The Maiden is the promise of new beginnings and will cause living things to bloom and ripen. She skilled in the primal magic of Fire and Air. The Mother is the power of life and can heal sickness and disease. She is skilled in the hearty magic of Blood and Water. The Crone is the wisdom of age and can predict future events with great accuracy. She is skilled in the arcane magic of Astral and Death."
        #humanoid
        #gcost 150
        #mr 18
        #mor 30
        #goodleader
        #spiritsight
        #heal
        #diseaseres 100
        #weapon 7 -- Quarterstaff
        #magicskill 3 1
        #magicskill 6 1
        #magicboost 0 2
        #magicboost 1 2
        #magicboost 2 -10
        #magicboost 4 -10
        #magicboost 5 -10
        #magicboost 7 -10
        #nobadevents 0
        #supplybonus 30
        #researchbonus -5
        #triplegod 5
        #triple3mon
        #triplegodmag 2
        #startdom 1
        #pathcost 10
        #homerealm 2 -- Celtic
        #end

        #newmonster 5074
        #copystats 280 -- Seithkona
        #clearmagic
        #clearweapons
        #spr1 "./ExtraPretenders/3Mother.tga"
        #spr2 "./ExtraPretenders/3Mother2.tga"
        #name "Mother"
        #descr "In ages past a great Goddess was born in which the power of femininity was embodied in one being. She could bear new life, heal the sick and see future events using divination. Soon after her creation the Pantokrator grew fearful and decided to divide the Goddess into three parts, to more safely contain her power. Each aspect of the Goddess was imprisoned separately to prevent their combined strength from overcoming his bonds. Now, with the Pantokrator gone the shackles are weakening and the Triple Goddess will soon return to guide her daughters once more. The Goddess now resides in three bodies, the Maiden, the Mother and the Crone. The Maiden is the promise of new beginnings and will cause living things to bloom and ripen. She skilled in the primal magic of Fire and Air. The Mother is the power of life and can heal sickness and disease. She is skilled in the hearty magic of Blood and Water. The Crone is the wisdom of age and can predict future events with great accuracy. She is skilled in the arcane magic of Astral and Death."
        #humanoid
        #gcost 150
        #mr 18
        #mor 30
        #goodleader
        #spiritsight
        #heal
        #diseaseres 100
        #weapon 7 -- Quarterstaff
        #magicskill 3 1
        #magicskill 6 1
        #magicboost 0 -10
        #magicboost 1 -10
        #magicboost 2 2
        #magicboost 4 -10
        #magicboost 5 -10
        #magicboost 7 2
        #triplegod 5
        #researchbonus -5
        #autodishealer 1
        #nobadevents 0
        #triplegodmag 2
        #startdom 1
        #pathcost 10
        #end

        #newmonster 5075
        #copystats 280 -- Seithkona
        #clearmagic
        #clearweapons
        #spr1 "./ExtraPretenders/3Crone.tga"
        #spr2 "./ExtraPretenders/3Crone2.tga"
        #name "Crone"
        #descr "In ages past a great Goddess was born in which the power of femininity was embodied in one being. She could bear new life, heal the sick and see future events using divination. Soon after her creation the Pantokrator grew fearful and decided to divide the Goddess into three parts, to more safely contain her power. Each aspect of the Goddess was imprisoned separately to prevent their combined strength from overcoming his bonds. Now, with the Pantokrator gone the shackles are weakening and the Triple Goddess will soon return to guide her daughters once more. The Goddess now resides in three bodies, the Maiden, the Mother and the Crone. The Maiden is the promise of new beginnings and will cause living things to bloom and ripen. She skilled in the primal magic of Fire and Air. The Mother is the power of life and can heal sickness and disease. She is skilled in the hearty magic of Blood and Water. The Crone is the wisdom of age and can predict future events with great accuracy. She is skilled in the arcane magic of Astral and Death."
        #humanoid
        #gcost 150
        #mr 18
        #mor 30
        #str 7
        #att 7
        #def 7
        #mapmove 10
        #goodleader
        #spiritsight
        #heal
        #diseaseres 100
        #weapon 7 -- Quarterstaff
        #magicskill 3 1
        #magicskill 6 1
        #magicboost 0 -10
        #magicboost 1 -10
        #magicboost 2 -10
        #magicboost 4 2
        #magicboost 5 2
        #magicboost 6 -1
        #magicboost 7 -10
        #nobadevents 25
        #triplegod 5
        #researchbonus -5
        #triplegodmag 2
        #startdom 1
        #pathcost 10
        #end

        #newmonster 5076
        #copystats 92 -- Cloud Mage
        #clearmagic
        #clearweapons
        #spr1 "./ExtraPretenders/3PureYellow.tga"
        #spr2 "./ExtraPretenders/3PureYellow2.tga"
        #name "Pure One"
        #descr "The Pure Ones are a pure manifestation of the Way and according to the Celestial Masters are the origin of all sentient beings. They appear as three elderly deities robed in the three basic colours from which all colours originated. The first Pure One embodies the ideals of the Way and is skilled in the path of Water, for the characteristics of Water, softness, flexibility and life-giving, are considered high ideals in the lore of the Way. The second Pure One embodies all the power of the Earthly plane and is skilled in the magic of Earth. The third Pure One embodies the power inherent in living creatures and is skilled in all other paths of magic. Each of them holds a divine object representing their power and authority. One holds the Pearl of Creation, one the Ruyi of Power and one the Fan of Knowledge. Since the disappearance of the Pantokrator the Pure Ones have taken on the mantle of Pretender Gods, to lead their followers to the Throne of Heaven and to reclaim what was once theirs."
        #humanoid
        #fixedname "Daode Tianzun"
        #gcost 130
        #mr 18
        #mor 30
        #magicskill 2 1
        #magicskill 3 1
        #magicskill 4 1
        #goodleader
        #heal
        #diseaseres 100
        #triplegod 2
        #triple3mon
        #triplegodmag 0
        #unify
        #weapon 92 -- Fist
        #startdom 1
        #pathcost 20
        #homerealm 4
        #end

        #newmonster 5077
        #copystats 92 -- Cloud Mage
        #clearmagic
        #clearweapons
        #spr1 "./ExtraPretenders/3PureRed.tga"
        #spr2 "./ExtraPretenders/3PureRed2.tga"
        #name "Pure One"
        #descr "The Pure Ones are a pure manifestation of the Way and according to the Celestial Masters are the origin of all sentient beings. They appear as three elderly deities robed in the three basic colours from which all colours originated. The first Pure One embodies the ideals of the Way and is skilled in the path of Water, for the characteristics of Water, softness, flexibility and life-giving, are considered high ideals in the lore of the Way. The second Pure One embodies all the power of the Earthly plane and is skilled in the magic of Earth. The third Pure One embodies the power inherent in living creatures and is skilled in all other paths of magic. Each of them holds a divine object representing their power and authority. One holds the Pearl of Creation, one the Ruyi of Power and one the Fan of Knowledge. Since the disappearance of the Pantokrator the Pure Ones have taken on the mantle of Pretender Gods, to lead their followers to the Throne of Heaven and to reclaim what was once theirs."
        #humanoid
        #fixedname "Yuanshi Tianzun"
        #gcost 150
        #mr 18
        #mor 30
        #magicskill 2 1
        #magicskill 3 1
        #magicskill 4 1
        #goodleader
        #heal
        #diseaseres 100
        #triplegodmag 0
        #unify
        #weapon 92 -- Fist
        #startdom 1
        #pathcost 20
        #end

        #newmonster 5078
        #copystats 92 -- Cloud Mage
        #clearmagic
        #clearweapons
        #spr1 "./ExtraPretenders/3PureBlue.tga"
        #spr2 "./ExtraPretenders/3PureBlue2.tga"
        #name "Pure One"
        #descr "The Pure Ones are a pure manifestation of the Way and according to the Celestial Masters are the origin of all sentient beings. They appear as three elderly deities robed in the three basic colours from which all colours originated. The first Pure One embodies the ideals of the Way and is skilled in the path of Water, for the characteristics of Water, softness, flexibility and life-giving, are considered high ideals in the lore of the Way. The second Pure One embodies all the power of the Earthly plane and is skilled in the magic of Earth. The third Pure One embodies the power inherent in living creatures and is skilled in all other paths of magic. Each of them holds a divine object representing their power and authority. One holds the Pearl of Creation, one the Ruyi of Power and one the Fan of Knowledge. Since the disappearance of the Pantokrator the Pure Ones have taken on the mantle of Pretender Gods, to lead their followers to the Throne of Heaven and to reclaim what was once theirs."
        #humanoid
        #fixedname "Lingbao Tianzun"
        #gcost 150
        #mr 18
        #mor 30
        #magicskill 2 1
        #magicskill 3 1
        #magicskill 4 1
        #goodleader
        #heal
        #diseaseres 100
        #triplegodmag 0
        #unify
        #weapon 92 -- Fist
        #startdom 1
        #pathcost 20
        #end
    -- MONSTERS 

        #newmonster 4887
        #spr1 "./ExtraPretenders/Kirin.tga"
        #spr2 "./ExtraPretenders/Kirin2.tga"
        #name "Kirin"
        #descr "The Kirin is a wondrous being that has existed since the records of men first began. He has the shape of a flaming horse, a flowing mane, the scales of a dragon and great jewelled antlers. As the most holy and wise creature on the earth he brings good fortune wherever he appears and can judge the hearts of men. In ancient times he spoke to the first people in an auspicious voice sounding like the tinkling of bells and they began to worship him. However, the Kirin in his wisdom saw that this would anger the Pantokrator and forbade such worship, departing the world to appear only in the hour of greatest need. Now, with the Pantokrator gone, the Kirin has returned and his followers cry out for him to take up the mantle of Godhood. Seeing the designs of the other Pretenders he has agreed, and now vies for the Throne of Heaven to rule with wisdom and compassion."
        #quadruped
        #gcost 160
        #size 6
        #prot 20
        #str 22
        #att 13
        #def 12
        #prec 11
        #hp 140
        #ap 24
        #mr 18
        #mor 30
        #enc 2
        #mapmove 26
        #startage 1000
        #maxage 3000
        #magicskill 4 2
        #fireshield 8
        #bringeroffortune 15
        #incunrest -50
        #goodleader
        #weapon 55 -- Hoof
        #weapon 1266 -- Golden Antlers
        #heal
        #speciallook 1
        #diseaseres 100
        #startdom 2
        #pathcost 80
        #homerealm 4 -- Far East
        #end

        #newmonster 4888
        #spr1 "./ExtraPretenders/Cromlech.tga"
        #spr2 "./ExtraPretenders/Cromlech2.tga"
        #name "Cromlech"
        #descr "The Cromlech is an ancient spirit inhabiting three great standing stones. For aeons the spirit has guided its followers, monitoring the seasons, the moon and the stars for auspicious events. The Cromlech ordains the best days for the planting of crops and the casting of rituals for their greatest effect. When all three stones are arranged together its power is greatest, however when removed from the sacred geometry a stone will lose some of its power. The stones would be difficult to destroy in battle even though they cannot strike back. Now, with the Pantokrator gone, the Cromlech has the chance to put the world under its strong Dominion and seize the Throne of Heaven."
        #miscshape
        #gcost 220
        #size 5
        #prot 25
        #str 15
        #att 5
        #def 0
        #prec 5
        #hp 120
        #ap 2
        #mr 18
        #mor 30
        #enc 0
        #mapmove 0
        #triplegod 5
        #triplegodmag 2
        #startage 1000
        #maxage 3000
        #masterrit 1
        #bonusspells 1
        #inanimate
        #immobile
        #neednoteat
        #stonebeing
        #spiritsight
        #blind
        #slashres
        #pierceres
        #poisonres 25
        #magicskill 4 1
        #magicskill 5 1
        #magicskill 6 1
        #superiorleader
        #okmagicleader
        #itemslots 4096 -- 1 misc
        #weapon 0
        #heal
        #diseaseres 100
        #startdom 4
        #pathcost 60
        #homerealm 2 -- Celtic
        #end

        #newmonster 4889
        #copystats 2134 -- Terracotta Soldier
        #clearweapons
        #spr1 "./ExtraPretenders/TerraPrince.tga"
        #spr2 "./ExtraPretenders/TerraPrince2.tga"
        #name "Terracotta Emperor"
        #descr "The Terracotta Emperor is an ancient terracotta statue that has come to life and begun to speak and move of its own volition. The statue is inhabited by a spirit believed to be that of a great Emperor and is worshiped by its subjects. The spirit can awaken further terracotta warriors to serve it and is amassing an army to sweep across the land. With the disappearance of the Pantokrator the Terracotta Emperor has seen a great opportunity, and has now assumed the mantle of a Pretender God."
        #mountedhumanoid
        #gcost 120
        #mounted
        #size 3
        #hp 21
        #ap 22
        #mr 18
        #mor 30
        #mapmove 22
        #startage 1000
        #maxage 3000
        #magicskill 0 1
        #magicskill 3 1
        #expertleader
        #expertmagicleader
        #makemonsters3 -5192 -- Terracotta Warriors
        #weapon 10 -- Falchion
        #weapon 56 -- Hoof
        #armor 2 --- Shield
        #heal
        #montag 0
        #diseaseres 100
        #startdom 2
        #pathcost 20
        #homerealm 0
        #end

        #newmonster 4890
        #spr1 "./ExtraPretenders/Starfish.tga"
        #spr2 "./ExtraPretenders/Starfish2.tga"
        #name "Sea Star"
        #descr "The Sea Star is an ancient being that fell from the heavens into the deep ocean long ago. There it has remained, unseen by the surface world and unnoticed through the passing of ages. Recently it has begun to stir, and has drawn followers from the creatures of the deep. Although slow and deliberate in its movements it has great magical power. The Sea Star is an old and powerful being that has traveled far in search of a world to call its own. Now, with the Pantokrator gone comes the chance to spread its Dominion across the world and rule from the deep waters it calls home."
        #miscshape
        #gcost 200
        #size 6
        #hp 150
        #prot 16
        #mr 18
        #str 22
        #mor 30
        #att 8
        #def 3
        #prec 10
        #ap 2
        #mapmove 6
        #enc 1
        #startage 1000
        #maxage 3000
        #aquatic
        #blind
        #spiritsight
        #bonusspells 1
        #goodleader
        #goodmagicleader
        #unsurr 5
        #poisonres 15
        #magicskill 3 1
        #magicskill 4 1
        #magicskill 6 1
        #weapon 85 -- Tentacle
        #weapon 85 -- Tentacle
        #weapon 85 -- Tentacle
        #weapon 85 -- Tentacle
        #heal
        #diseaseres 100
        #startdom 4
        #pathcost 60
        #homerealm 9
        #itemslots 28672 -- only miscs now
        #end

        #newmonster 4891
        #spr1 "./ExtraPretenders/Gorynch.tga"
        #spr2 "./ExtraPretenders/Gorynch2.tga"
        #name "Zmey Gorynych"
        #descr "The Zmey Gorynych is an ancient weather spirit of great power. Born as a simple snake, the creature began to grow and by 100 years of age was as large as a man. It then underwent a marvelous change, sprouting wings and gaining a roar like thunder. Over the years many would-be heroes have come to slay the beast, however it has bested them all. Now it has set its sights on the Throne of Heaven and has assumed the mantle of a Pretender God."
        #lizard
        #gcost 240
        #size 6
        #hp 125
        #prot 20
        #mr 18
        #str 25
        #mor 30
        #att 15
        #def 12
        #prec 12
        #ap 10
        #mapmove 26
        #enc 2
        #startage 1000
        #maxage 3000
        #fear 10
        #eyes 6
        #dragonlord 1
        #flying
        #wastesurvival
        #mountainsurvival
        #unsurr 2
        #coldres 5
        #shockres 15
        #magicskill 1 2
        #weapon 20 -- Bite
        #weapon 20 -- Bite
        #weapon 20 -- Bite
        #weapon 29 -- Claw
        #weapon 532 -- Tail Sweep
        #heal
        #itemslots 275328 -- 3 crown, 2 misc
        #diseaseres 100
        #startdom 2
        #pathcost 40
        #homerealm 0
        #twiceborn 5771 -- Dracowight
        -- #shapechange 4892
        #end

        #newmonster 4892
        #copystats 1917 -- Knyaz
        #copyspr 1917 -- Knyaz
        #clearweapons
        #clearmagic
        #name "Zmey Gorynych"
        #descr "The Zmey Gorynych is an ancient weather spirit of great power. Born as a simple snake, the creature began to grow and by 100 years of age was as large as a man. It then underwent a marvelous change, sprouting wings and gaining a roar like thunder. It can take the form of a handsome young man astride a magnificent white horse which it uses to seduce and beguile the weak willed. Over the years many would-be heroes have come to slay the beast, however it has bested them all. The physical body of the creature is not well suited for spell casting and its skills will be reduced when casting spells from paths other than Air. Therefore, the Zmey will tend to adopt human form for spellcasting, but when wounded will revert to its true form."
        #mountedhumanoid
        #gcost 220
        #ap 24
        #mr 18
        #mor 30
        #hp 20
        #mapmove 22
        #mounted
        #coldres 5
        #shockres 15
        #seduce 10
        #heal
        #magicskill 1 2
        #dragonlord 1
        #diseaseres 100
        #wastesurvival
        #mountainsurvival
        #weapon 8 -- Broadsword
        #armor 8 -- Chain Mail
        #armor 119 -- Leather Cap
        #startdom 2
        #pathcost 40
        -- #shapechange 4891
        #end

        #newmonster 4893
        #copystats 2234 -- Irminsul
        #copyspr 981 -- Dying Treelord
        #clearmagic
        #name "Tree of Hate"
        #descr "The Tree of Hate is a vengeful spirit of nature inhabiting a great dead tree. In ages past it lived in the center of a great woodland and brought life and growth to the forest, however with the coming of Man the forest was burned and the tree withered and died. For long years the spirit was trapped within the dead trunk as insects burrowed into its rotting form. Now, with disappearance of the Pantokrator it has emerged to take on the mantle of a Pretender God and exact vengeance upon the world of men. The spirit can animate the tree to defend against attackers or perform tasks such as forging items, but it cannot uproot and move around. It is powerful in its Dominion, however in a physical battle it would be possible to chop down the tree."
        #gcost 150
        #hp 220
        #prot 16
        #heal
        #diseaseres 100
        #magicskill 5 2
        #magicskill 6 1
        #ivylord 0
        #batstartsum3 0 -- Remove Bears
        #batstartsum5d6 -9 -- Bugs
        #undead
        #inanimate
        #homerealm 0
        #expertundeadleader
        #end

        #newmonster 4894
        #copystats 2234 -- Irminsul
        #clearweapons
        #clearmagic
        #name "Burning Bush"
        #spr1 "./ExtraPretenders/BurningBush.tga"
        #spr2 "./ExtraPretenders/BurningBush.tga"
        #descr "The Burning Bush is a spirit inhabiting a great bush that has burned for as long as anyone can remember. It occasionally speaks with a thunderous voice and angelic beings can be seen to manifest inside the flames. Occasionally one such being will emerge from the flames to serve the bush. Over the centuries the bush has been regarded with fear and curiosity, however with disappearance of the Pantokrator it now demands worship and has decided to take on the mantle of a Pretender God. The bush burns continually with a great heat that will sear any that get too close.  The spirit is powerful in its Dominion, however in a physical battle it would be possible to chop down the bush if you could withstand the flames."
        #gcost 150
        #hp 170
        #prot 12
        #heal
        #diseaseres 100
        #magicskill 0 1
        #magicskill 4 1
        #magicskill 6 1
        #fireres 15
        #heat 15
        #fireshield 8
        #ivylord 0
        #twiceborn 4893 -- Tree of Hate
        #batstartsum1 543 -- Angel of the Host
        #raredomsummon 543 -- Angel of the Host
        #homerealm 5 -- Middle East
        #end

        #newmonster 4895
        #copystats 1097 -- Lord of the Summer Plague
        #clearweapons
        #clearmagic
        #name "Lord of the Carrion Dead"
        #spr1 "./ExtraPretenders/VultureGod.tga"
        #spr2 "./ExtraPretenders/VultureGod2.tga"
        #descr "The Lord of the Carrion Dead is a vulture-headed Titan who claims dominion over the corpses of all dead things. Death is his domain and bleached bones and rotten meat are his food and drink. He is accompanied by sacred vultures in battle and will manifest magical gems of air and death to ease spellcasting. Each month his followers will present a slave of pure blood to their vulture-headed Lord."
        #gcost 200
        #heal
        #mr 20
        #corpseeater 5
        #deadhp 1
        #diseaseres 100
        #magicskill 1 1
        #magicskill 5 1
        #magicskill 7 1
        #tmpairgems 1
        #tmpdeathgems 1
        #gemprod 7 1
        #fireres 0
        #weapon 10 -- Falchion
        #weapon 671 -- Stone Dagger
        #weapon 404 -- Beak
        #batstartsum1d6 4223 -- Vulture
        #homerealm 7 -- Africa
        #end

        #newmonster 4896
        #name "Nehushtan"
        #spr1 "./ExtraPretenders/Nehushtan.tga"
        #spr2 "./ExtraPretenders/Nehushtan.tga"
        #descr "The Nehushtan is a spirit of healing bound into a statue of a serpent made from copper. For as long as people can remember they have worshipped at the statue and left offerings to cure them of their ills. Now, with the Pantokrator gone the Nehushtan will put the world under its strong Dominion and become the True God. The spirit cannot leave the statue, but it can possess willing targets in order to make its will heard and to perform tasks such as forging items for enchantment. The spirit is tremendously strong in its dominion. In a physical battle, the idol would be difficult to destroy, even though it cannot strike back."
        #miscshape
        #gcost 180
        #size 6
        #hp 180
        #prot 22
        #mr 18
        #mor 30
        #str 15
        #att 5
        #def 0
        #prec 10
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 2 1
        #magicskill 4 1
        #magicskill 6 1
        #weapon 0
        #autohealer 1
        #goodleader
        #immobile
        #inanimate
        #poisonres 25
        #blind
        #spiritsight
        #slashres
        #pierceres
        #neednoteat
        #amphibian
        #heal
        #diseaseres 100
        #bonusspells 1
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homerealm 5 -- Middle East
        #end

        #newmonster 4897
        #name "Great Creator"
        #spr1 "./ExtraPretenders/Demiurge.tga"
        #spr2 "./ExtraPretenders/Demiurge2.tga"
        #descr "The Great Creator is a divine spirit of creation that fashioned the world at the dawn of time. Once the task was completed the Pantokrator had no need of the spirit and it was banished to the celestial realm where it could not affect the mortal world. Now, with the Pantokrator gone the spirit has returned and will once more take command of the world it created. The Great Creator appears as an old bearded man born aloft by clouds. The spirit can call lightning from the heavens and will manifest gems of air, nature and astral magic in combat."
        #fixedname "Yahweh"
        #mountedhumanoid
        #gcost 210
        #hp 125
        #size 6
        #prot 0
        #str 22
        #att 11
        #def 10
        #prec 13
        #mr 20
        #mor 30
        #enc 1
        #ap 16
        #mapmove 26
        #magicskill 1 1
        #magicskill 4 1
        #magicskill 6 1
        #tmpairgems 1
        #tmpastralgems 1
        #tmpnaturegems 1
        #expertleader
        #okmagicleader
        #heal
        #flying
        #float
        #weapon 1846 -- Thunder Bolt
        #weapon 92 -- Fist
        #diseaseres 100
        #maxage 5000
        #startage 1000
        #pathcost 80
        #startdom 3
        #homerealm 0
        #end

        #newmonster 4898
        #copystats 215 -- Virtue
        #clearmagic
        #name "Elohim"
        #spr1 "./ExtraPretenders/Elohim.tga"
        #spr2 "./ExtraPretenders/Elohim2.tga"
        #descr "The Elohim is a divine spirit of Judgment that has existed since ancient times. It claims dominion over the Earth, the Skies and the Heavens and passes judgment on all the creatures of the world. When the Pantokrator arose it abandoned the world, however now it has returned to lead the faithful on the path of righteousness and salvation. The Elohim is surrounded by a powerful Aura of Splendor, and it has a terrible fury that strikes fear into the hearts of men. In combat it will manifest magical gems of earth, air and astral magic for use in spellcasting."
        #fixedname "El"
        #humanoid
        #gcost 260
        #hp 75
        #size 5
        #str 18
        #mr 20
        #magicskill 1 1
        #magicskill 3 1
        #magicskill 4 1
        #tmpairgems 1
        #tmpearthgems 1
        #tmpastralgems 1
        #heal
        #awe 3
        #fear 5
        #invulnerable 20
        #diseaseres 100
        #maxage 5000
        #startage 1000
        #pathcost 80
        #startdom 3
        #homerealm 0
        #end

        #newmonster 4899
        #name "Traveler"
        #spr1 "./ExtraPretenders/Hermes.tga"
        #spr2 "./ExtraPretenders/Hermes2.tga"
        #descr "The Traveler is a giant of divine heritage that once served the Pantokrator as a divine messenger. His winged sandals and helmet allowed him to stride through the air with incredible speed carrying messages and warnings from his master. With the Pantokrator gone he has decided to ascend the Throne of Heaven and become the true God. The Traveler is tireless and has supernatural perceptive abilities. He is surrounded by an aura of splendour gifted to him by his former master and can fly through even the fiercest storms. He bears a Caduceus that will rob the will of those it strikes. In combat he moves with unearthly speed."
        #fixedname "Hermes"
        #humanoid
        #gcost 200
        #hp 45
        #size 4
        #prot 0
        #mr 18
        #mor 30
        #str 16
        #att 13
        #def 16
        #prec 12
        #enc 1
        #mapmove 36
        #ap 28
        #magicskill 1 1
        #magicskill 5 1
        #invulnerable 20
        #expertleader
        #flying
        #heal
        #stormimmune
        #spiritsight
        #spreaddom 2
        #awe 3
        #reinvigoration 2
        #unsurr 4
        #patrolbonus 20
        #itemslots 13446 -- No feet slot due to winged boots
        #onebattlespell 610 -- Quicken Self
        #armor 118 -- Half Helmet
        #diseaseres 100
        #twiceborn 5785 -- Wight Giant (Jotun)
        #weapon 1871 -- Caduceus
        #maxage 5000
        #startage 1000
        #pathcost 80
        #startdom 2
        #homerealm 3 -- Mediterranean
        #end

        #newmonster 4900
        #copystats 247 -- Freak
        #spr1 "./ExtraPretenders/GiantMarkata.tga"
        #spr2 "./ExtraPretenders/GiantMarkata2.tga"
        #name "Markata Freak"
        #descr "This is a horribly deformed markata of immense size created by vile magic. The Markata Freak is stupid and will lumber uncontrollably across the battlefield, its hoots and hollers striking fear into the enemy."
        #magicboost 53 -10
        #undisciplined
        #transformation 0
        #end

        #newmonster 4901
        #copystats 246 -- Freak Lord
        #clearmagic
        #clearweapons
        #name "Markata Master"
        #spr1 "./ExtraPretenders/VanaraFreakLord.tga"
        #spr2 "./ExtraPretenders/VanaraFreakLord2.tga"
        #descr "The Markata Master was a Vanara mage obsessed with discovering the secret to eternal life. Finding markata easily enticed into his laboratory and rarely missed he carried out many horrifying experiments on the chittering creatures. Eventually he mastered the process and cast aside his own mortality. Donning a godly mantle, he has now taken the role of a Pretender God. The Markata Master is a master of magic and is adept in several of the magic paths. His experiments continue and each month he will release a few markata from his laboratory to serve him. He rides a horrifying Markata Freak created with vile magic."
        #mountedhumanoid
        #gcost 120
        #magicskill 7 1
        #weapon 151 -- Wand
        #startage 250
        #maxage 500
        #heal
        #secondtmpshape 4900 -- Giant Markata
        #diseaseres 100
        #douse 3
        #beastmaster 1
        #domsummon 1118 -- Markata
        #domsummon2 1120 -- Markata Archer
        #domsummon20 4900 -- Markata Freak
        #startdom 1
        #pathcost 10
        #homerealm 0
        #end

        #newmonster 4902
        #name "Skull Idol"
        #spr1 "./ExtraPretenders/GrinningSkull.tga"
        #spr2 "./ExtraPretenders/GrinningSkull.tga"
        #descr "The Skull Idol is a primordial spirit of blood and death that was bound into a stone edifice when the world was young. The Pantokrator decreed that the spirit could not emerge from the stone until the end times, when the world would die at the spirits hand and be renewed. With the Pantokrator gone, its powers have begun to manifest themselves and it is now worshipped as a reawakening god. As the spirit gains strength its powers manifest, causing living things to die and the dead to rise to serve it. The spirit cannot leave the statue, but it can possess willing targets in order to make its will heard and to perform tasks such as forging items for enchantment. The spirit is tremendously strong in its dominion. In a physical battle, the idol would be difficult to destroy, even though it cannot strike back."
        #miscshape
        #gcost 160
        #size 6
        #hp 180
        #prot 24
        #mr 18
        #mor 30
        #str 15
        #att 5
        #def 0
        #prec 10
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 4 1
        #magicskill 5 1
        #magicskill 7 1
        #weapon 0
        #gemprod 5 3
        #incscale 3 -- +Death
        #goodleader
        #expertundeadleader
        #immobile
        #inanimate
        #poisonres 25
        #blind
        #spiritsight
        #stonebeing
        #slashres
        #pierceres
        #neednoteat
        #amphibian
        #heal
        #domsummon -2 -- Longdead
        #domsummon2 442 -- Shade Beasts
        #domsummon20 566 -- Ghost
        #raredomsummon 1422 -- Civateteo
        #diseaseres 100
        #bonusspells 1
        #itemslots 4096 -- 1 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homerealm 6 -- Middle America
        #end

        #newmonster 4903
        #copystats 244 -- Arch Mage
        #clearmagic
        #name "Drake Breeder"
        #spr1 "./ExtraPretenders/DrakeLord.tga"
        #spr2 "./ExtraPretenders/DrakeLord2.tga"
        #descr "The Drake Breeder is an archmage that has a strong affinity for drakes and dragonkind. He has bred draconic creatures of all kinds and shows a strong aptitude for the task. Having learned all he can of magic the only thing left is to claim divinity. The Drake Breeder can summon 2 additional drakes when casting summoning magic, and each month can breed a wyvern using magical techniques. He is a master of many magical paths and rides a wyvern he has tended from birth."
        #mountedhumanoid
        #gcost 120
        #size 5
        #hp 12
        #str 10
        #att 10
        #def 12
        #enc 4
        #mapmove 22
        #ap 7
        #magicskill 1 1
        #goodleader
        #okundeadleader
        #heal
        #flying
        #wastesurvival
        #mountainsurvival
        #dragonlord 2
        #makemonsters1 520 -- Wyvern
        #diseaseres 100
        #weapon 92 -- Fist
        #weapon 19 -- Bite
        #secondtmpshape 520
        #mounted
        #itemslots 13446 -- No feet
        #startdom 1
        #pathcost 10
        #homerealm 10 -- Default
        #end

        #newmonster 4904
        #name "First Valkyrie"
        #spr1 "./ExtraPretenders/FirstValk.tga"
        #spr2 "./ExtraPretenders/FirstValk2.tga"
        #descr "The First Valkyrie is the daughter of a great Vanir Drott and was the first to be granted the ability to fly in ancient times. She pledged her service as a messenger of death in return for the powers of flight and magical longevity. Over the centuries the power of her patron waned and they are now long gone. However, the First Valkyrie remains and some say she has Aesir blood in her veins. She has since decided to lead her people and assume the mantle of a Pretender God to claim the Throne of Heaven. The First Valkyrie is protected by a Divine aura and few would dare to strike at her. She bears enchanted arms forged in Vanhalla and like all vanir can cloak her appearance in illusion. She has survived the fiercest storms and now lightning will not harm her."
        #fixedname "Sváfa"
        #humanoid
        #gcost 200
        #hp 25
        #size 3
        #prot 0
        #mr 18
        #mor 30
        #str 16
        #att 15
        #def 16
        #prec 14
        #enc 1
        #mapmove 32
        #ap 14
        #magicskill 1 1
        #magicskill 5 1
        #expertleader
        #flying
        #heal
        #stormimmune
        #spiritsight
        #spreaddom 2
        #stealthy 0
        #onebattlespell 604 -- Luck
        #female
        #illusion
        #awe 3
        #shockres 15
        #illusion
        #diseaseres 100
        #weapon 1862 -- Lightning Sword
        #armor 196 -- Golden Scale
        #armor 66 -- Weightless Shield
        #maxage 5000
        #startage 1000
        #pathcost 80
        #startdom 2
        #homerealm 0
        #end

        #newmonster 4905
        #name "Great Gull of the Sea"
        #spr1 "./ExtraPretenders/Seagull.tga"
        #spr2 "./ExtraPretenders/Seagull2.tga"
        #descr "The Great Gull of the Sea is a terrible bird of gigantic proportions from an earlier era, when monsters and giants roamed the world. It has keen eyesight and can fly through the fiercest coastal storms. When the previous Pantokrator rose to power he imprisoned and banished all that threatened his power and the Gull was forever imprisoned. Now, with the Pantokrator gone, it is breaking free and its shrill cry can once more be heard around the world. The Great Gull can soar over the ocean to distant lands and its screeches bring fear to the hearts of men."
        #bird
        #gcost 170
        #hp 110
        #size 6
        #prot 20
        #mr 18
        #mor 30
        #str 22
        #att 13
        #def 14
        #prec 14
        #enc 1
        #mapmove 22
        #ap 8
        #magicskill 1 1
        #magicskill 2 1
        #goodleader
        #flying
        #heal
        #stormimmune
        #fear 5
        #patrolbonus 50
        #diseaseres 100
        #sailing 6 6
        #weapon 408 -- Talons
        #weapon 404 -- Beak
        #weapon 677 -- Wing buff
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #twiceborn 1388 -- Ziz
        #pathcost 80
        #startdom 2
        #homerealm 0
        #end

        #newmonster 4906
        #copystats 2795 -- Dog of the Underworld
        #clearmagic
        #spr1 "./ExtraPretenders/Xolotl.tga"
        #spr2 "./ExtraPretenders/Xolotl2.tga"
        #name "Divine Monstrous Dog"
        #descr "The Divine Monstrous Dog was born to the Teteo Inan in ancient times. It is a creature of death and was tasked with guarding the sun during its journey through the underworld each night. When the Pantokrator cut off the head of the Goddess all her children were imprisoned and the task of guarding the sun was passed to the Zotz. Following the imprisonment of the Monstrous Dog and his twin brother, the Feathered Serpent, twins came to be seen as an abomination and and even now in Mictlan one is slain soon after birth to dwell with the Dog in the underworld. Now the baying of the Monstrous Dog is heard once more in the realm of the living. The Monstrous Dog feeds on the dead and gains hit points if enough corpses are present"
        #fixedname "Xolotl"
        #gcost 170
        #prot 16
        #magicskill 3 1
        #magicskill 5 1
        #homerealm 6 -- MesoAmerica
        #twiceborn 5769 -- Wight Beast
        #batstartsum1d6 3083 -- Sacred Hound
        #heal
        #end

        #newmonster 4907
        #copystats 2786 -- Celestial Gryphon
        #clearweapons
        #clearmagic
        #spr1 "./ExtraPretenders/Quetzal.tga"
        #spr2 "./ExtraPretenders/Quetzal2.tga"
        #name "Divine Feathered Serpent"
        #descr "The first Feathered Serpent was born to the Teteo Inan in ancient times. It was a creature of the air and brought hurricanes and devastation to the lands of men. From its divine essence were created the Couatl, mythical serpent creatures that inhabit swamps and jungles. When the Pantokrator cut off the head of the Goddess he imprisoned her children for eternity, however now the Feathered Serpent has returned to claim the world. It can fly on the winds and is served by the Centzonmimixcoa, the Cloud Serpents."
        #gcost 180
        #snake
        #weapon 462 -- Deadly Poison Bite
        #weapon 532 -- Tail Sweep
        #weapon 677 -- Wing Buff
        #prot 20
        #stormimmune
        #magicskill 1 1
        #magicskill 6 1
        #batstartsum1d6 5677 -- Centonmixzoa
        #homerealm 6 -- MesoAmerica
        #diseaseres 100
        #twiceborn 5770 -- Necrophidian
        #heal
        #end


        #newmonster 4936
        #name "Island Crab"
        #spr1 "./ExtraPretenders/IslandCrab.tga"
        #spr2 "./ExtraPretenders/IslandCrab2.tga"
        #descr "The Island Crab is a monstrous crab born at the dawn of time, when monsters and giants roamed the world. In the great battle between Gods the crab nipped the Pantokrator on the toe to distract him. Enraged, the Pantokrator imprisoned and banished those that had defied him and the Island Crab was imprisoned for eternity. Now with the Pantokrator gone, the shackles are weakening and the waves of the stirring crab can once more be felt across the oceans of the world. The Island Crab is immensely well protected by the great shell it bears. The Crab is slow, however any lesser beings that stand against it will be crushed underfoot."
        #troglodyte
        #fixedname "Cancer"
        #gcost 170
        #size 6
        #hp 220
        #prot 20
        #mr 18
        #mor 30
        #str 26
        #att 14
        #def 8
        #prec 10
        #enc 3
        #mapmove 2
        #ap 8
        #magicskill 3 2
        #weapon 29 -- Claw
        #itemslots 12288 -- 2 misc
        #trample
        #amphibian
        #heal
        #darkvision 50
        #diseaseres 100
        #goodleader
        #maxage 1000
        #startage 500
        #twiceborn 5769 -- Wight Beast
        #startdom 2
        #pathcost 80
        #homerealm 9 -- Deeps
        #end

        #newmonster 4937
        #name "Night Hunter"
        #spr1 "./ExtraPretenders/NightHunter.tga"
        #spr2 "./ExtraPretenders/NightHunter2.tga"
        #descr "The Night Hunter is a monstrous bat born in the darkness before the dawn of time. It has stalked the creatures of the world for eternity and drunk the blood of Gods and men alike. When the previous Pantokrator ascended he banished the creatures of darkness and the Night Hunter was imprisoned for eternity. Now with the Pantokrator gone, the shackles are weakening and the smell of fresh blood awakens ancient desires in the creature once more. The Night Hunter haunts the darkness and flies on silent wings. The creature can detect the scent of those suitable for blood magic and in its wake come great bats eager to slake their thirst. It can see in the purest darkness and can drain the life blood of the living."
        #bird
        #gcost 160
        #size 6
        #hp 85
        #prot 20
        #mr 18
        #mor 30
        #str 23
        #att 13
        #def 14
        #prec 10
        #enc 2
        #mapmove 22
        #ap 8
        #magicskill 5 1
        #magicskill 7 1
        #fear 5
        #flying
        #spiritsight
        #douse 3
        #blind
        #heal
        #darkpower 3
        #stealthy 0
        #diseaseres 100
        #weapon 677 -- Wing Buff
        #weapon 63 -- Life Drain
        #itemslots 12288 -- 2 misc
        #batstartsum1d6 1357 -- Beast Bat
        #goodleader
        #maxage 1000
        #startage 500
        #twiceborn 1388 -- Ziz
        #startdom 2
        #pathcost 80
        #homerealm 10
        #end

        #newmonster 4938
        #name "Great White Stag"
        #spr1 "./ExtraPretenders/WhiteStag.tga"
        #spr2 "./ExtraPretenders/WhiteStag2.tga"
        #descr "The Great White Stag is a legendary stag that has existed in the deepest forests since the dawn of time. It is a symbol of the thrill of the hunt and the joy of discovery. It was worshiped by the first men of the forest as a god of the wild and of the hunt. When the previous Pantokrator rose to power he imprisoned and banished all that threatened his power and the Great White Stag was forever imprisoned. Now, with the Pantokrator gone, the Great White Stag has returned once more to claim dominance over the world. The Stag is a majestic creature and many enemies will hesitate to strike it in battle."
        #quadruped
        #gcost 150
        #size 5
        #hp 125
        #prot 20
        #mr 18
        #mor 30
        #str 22
        #att 13
        #def 14
        #prec 10
        #enc 2
        #mapmove 3
        #ap 20
        #magicskill 1 1
        #magicskill 6 1
        #awe 1
        #weapon 1849 -- Great Antlers
        #weapon 55 -- Hoof
        #batstartsum1d6 2228 -- Deer
        #itemslots 12288 -- 2 misc
        #heal
        #diseaseres 100
        #stealthy 10
        #goodleader
        #forestsurvival
        #beastmaster 2
        #maxage 1000
        #startage 500
        #twiceborn 5769 -- Wight Beast
        #startdom 2
        #pathcost 80
        #homerealm 2 -- Celtic
        #end

        #newmonster 4939 -- Black Tortoise
        #name "Black Tortoise"
        #spr1 "./ExtraPretenders/BlackTortoise.tga"
        #spr2 "./ExtraPretenders/BlackTortoise2.tga"
        #descr "The Black Tortoise is one of the Legendary Guardian animals that manifested at the dawn of time, when monsters and giants roamed the world. Worshipped by the first men as Divine protectors, the Guardian animals allowed mankind to develop civilization free from the predation of monstrous beings. When the previous Pantokrator rose to power he imprisoned and banished all that threatened his power and the Legendary Guardian animals were forever imprisoned. Now, with the Pantokrator gone, the Black Tortoise is breaking free to once again protect its subjects by donning the mantle of God itself. The shell of the Tortoise is incredibly tough and difficult to pierce. It is a symbol of the winter and is stronger in the frozen months. The Black Tortoise is massive and will trample smaller creatures with its great bulk."
        #fixedname "Zhi Ming"
        #quadruped
        #gcost 170
        #size 6
        #hp 180
        #prot 20
        #str 22
        #att 12
        #def 8
        #mr 18
        #mor 30
        #enc 2
        #ap 8
        #mapmove 8
        #trample
        #heal
        #diseaseres 100
        #pooramphibian
        #goodleader
        #coldres 15
        #winterpower 25
        #magicskill 3 2
        #weapon 20 -- Bite
        #maxage 1000
        #startage 500
        #itemslots 12416 -- Head, 2 misc
        #twiceborn 5769 -- Wight Beast
        #startdom 2
        #pathcost 80
        #homerealm 4 -- Far East
        #end

        #newmonster 4940
        #name "White Tiger"
        #spr1 "./ExtraPretenders/WhiteTiger.tga"
        #spr2 "./ExtraPretenders/WhiteTiger2.tga"
        #descr "The White Tiger is one of the Legendary Guardian animals that manifested at the dawn of time, when monsters and giants roamed the world. Worshipped by the first men as Divine protectors, the Guardian animals allowed mankind to develop civilization free from the predation of monstrous beings. When the previous Pantokrator rose to power he imprisoned and banished all that threatened his power and the Legendary Guardian animals were forever imprisoned. Now, with the Pantokrator gone, the White Tiger is breaking free to once again protect its subjects by donning the mantle of God itself. It is taller than an elephant and can tear through the heaviest armor with its teeth and claws. The White Tiger is a symbol of the Autumn and is stronger in the fall months. The roar of the beast strikes fear into the hearts of the enemy."
        #fixedname "Jian Bing"
        #quadruped
        #gcost 150
        #size 6
        #hp 150
        #prot 20
        #str 28
        #att 14
        #def 12
        #mr 18
        #mor 30
        #prec 10
        #enc 2
        #goodleader
        #fear 5
        #heal
        #diseaseres 100
        #forestsurvival
        #fallpower 25
        #magicskill 6 2
        #batstartsum1d6 1140 -- Tiger
        #weapon 630 -- Ghost Rending Bite
        #weapon 29 -- Claw
        #weapon 29 -- Claw
        #itemslots 12288 -- 2 misc
        #twiceborn 5769 -- Wight Beast
        #startdom 2
        #pathcost 80
        #homerealm 4 -- Far East
        #end

        #newmonster 4941
        #name "Vermillion Bird"
        #spr1 "./ExtraPretenders/VermillionBird.tga"
        #spr2 "./ExtraPretenders/VermillionBird2.tga"
        #descr "The Vermillion Bird is one of the Legendary Guardian animals that manifested at the dawn of time, when monsters and giants roamed the world. Worshipped by the first men as Divine protectors, the Guardian animals allowed mankind to develop civilization free from the predation of monstrous beings. When the previous Pantokrator rose to power he imprisoned and banished all that threatened his power and the Legendary Guardian animals were forever imprisoned. Now, with the Pantokrator gone, the Vermillion Bird is breaking free to once again protect its subjects by donning the mantle of God itself. It is perpetually covered in flames and will be stronger in hotter conditions and weaker in the cold. The Vermillion Bird is associated with the South and Summer and is stronger in the warmer months. Its flames burn so brightly that most creatures will hesitate to strike it."
        #fixedname "Ling Guang"
        #bird
        #gcost 170
        #hp 120
        #size 6
        #prot 18
        #str 26
        #att 14
        #def 13
        #mr 18
        #mor 30
        #prec 10
        #enc 2
        #fireres 25
        #heat 6
        #fireshield 10
        #summerpower 25
        #magicskill 0 2
        #flying
        #heal
        #diseaseres 100
        #goodleader
        #maxage 1000
        #startage 500
        #awe 1
        #weapon 404 -- Beak
        #weapon 61 -- Fire Breath
        #weapon 229 -- Flame Strike
        #itemslots 12288 -- 2 misc
        #twiceborn 1388 -- Ziz
        #startdom 2
        #pathcost 80
        #homerealm 4 -- Far East
        #end

        #newmonster 4942
        #name "Saltwater Serpent"
        #spr1 "./ExtraPretenders/PrimordialSerpent.tga"
        #spr2 "./ExtraPretenders/PrimordialSerpent2.tga"
        #descr "The Saltwater Serpent is a monstrous female sea serpent born in the great dark waters at the dawn of time. As she roared and smote in the chaos of original creation the Serpent gave birth to a multitude of monsters through the mixing of salt and fresh water. Soon the Pantokrator discovered a plot by the serpent to use her offspring in a war against him and he punished her with eternal imprisonment. Now with the Pantokrator gone, the Primordial Serpent has returned to creation to fill it with her monstrous children. Whilst she is beneath the waves monstrous creatures will occasionally be born to the serpent as her Dominion grows strong."
        #fixedname "Tiamat"
        #snake
        #gcost 170
        #size 6
        #hp 150
        #prot 20
        #mr 18
        #mor 30
        #str 23
        #att 13
        #def 10
        #prec 10
        #enc 2
        #mapmove 2
        #ap 10
        #magicskill 2 1
        #magicskill 6 1
        #weapon 65 -- Venomous Fangs
        #fear 5
        #regeneration 10
        #poisoncloud 6
        #poisonres 10
        #amphibian
        #goodleader
        #heal
        #female
        #darkvision 50
        #diseaseres 100
        #domsummon20 565 -- Sea Serpent
        #raredomsummon 438 -- Kraken
        #itemslots 12288 -- 2 misc
        #twiceborn 5770 -- Necrophidian
        #maxage 1000
        #startage 500
        #startdom 2
        #pathcost 80
        #homerealm 9 -- Deeps
        #end

        #newmonster 4943
        #name "Great Water Lizard"
        #spr1 "./ExtraPretenders/WaterLizard.tga"
        #spr2 "./ExtraPretenders/WaterLizard2.tga"
        #descr "The Great Water Lizard is a monstrous amphibian born at the dawn of time, when monsters and giants roamed the world. It once slept beneath a great lake by an ancient city and was worshipped by the strange beings that dwelled upon on the shore. When the humans of the city cruelly slaughtered the beings, the Water Lizard stirred. On the anniversary of the slaughter, the Water Lizard rose up and utterly destroyed the city, so completely that not even ruins remained. Fearful of its power the Pantokrator banished the creature back below the waves, never to return. Now with the Pantokrator gone it stirs once more and will rise to conquer the land by donning the mantle of God itself."
        #lizard
        #gcost 150
        #size 6
        #hp 165
        #prot 20
        #mr 18
        #mor 30
        #str 25
        #att 15
        #def 11
        #prec 10
        #enc 2
        #mapmove 3
        #ap 20
        #magicskill 6 2
        #weapon 461 -- Swallow
        #weapon 29 -- Claw
        #weapon 532 -- Tail Sweep
        #itemslots 12288 -- 2 misc
        #fear 5
        #heal
        #diseaseres 100
        #amphibian
        #darkvision 50
        #poisonres 10
        #goodleader
        #digest 2
        #maxage 1000
        #startage 500
        #twiceborn 5769 -- Wight Beast
        #startdom 2
        #pathcost 80
        #homerealm 9 -- Deeps
        #end

        #newmonster 4944
        #name "Piscean"
        #spr1 "./ExtraPretenders/Barracuda.tga"
        #spr2 "./ExtraPretenders/Barracuda2.tga"
        #descr "The Piscean is a monstrous fish born at the dawn of time, when monsters and giants roamed the world. It has terrorised coastal civilizations and preyed upon the undersea kingdoms of the first Tritons. When the previous Pantokrator rose to power he imprisoned and banished all that threatened his creation and the Piscean was imprisoned for eternity. Now with the Pantokrator gone, the shackles are weakening and the great fish begins to stir once more. The Piscean is a monstrous creature that will strike terror in those who oppose it and is surrounded by shoals of lesser fish that feed upon the morsels it leaves."
        #snake
        #gcost 130
        #size 6
        #hp 148
        #prot 21
        #mr 18
        #mor 30
        #str 25
        #att 14
        #def 14
        #prec 5
        #enc 2
        #mapmove 2
        #ap 22
        #magicskill 2 2
        #weapon 20 -- Bite
        #weapon 589 -- Tail Slap
        #batstartsum4d6 2858 -- Large Fish
        #goodleader
        #aquatic
        #heal
        #darkvision 50
        #diseaseres 100
        #poisonres 5
        #fear 5
        #itemslots 12288 -- 2 misc
        #twiceborn 1235 -- Leviathan
        #maxage 1000
        #startage 500
        #startdom 2
        #pathcost 80
        #homerealm 9 -- Deeps
        #end

        #newmonster 4945
        #name "Divine Turtle"
        #spr1 "./ExtraPretenders/Turtle.tga"
        #spr2 "./ExtraPretenders/Turtle2.tga"
        #descr "The Divine Turtle is a great turtle that has swum the oceans since the dawn of time. It was worshiped by the beings of the undersea as a creature of two worlds, that of air and of water. It swims forever searching for a place to lay its eggs, which some say contain the seeds of the next world. When the previous Pantokrator rose to power he imprisoned and banished all that threatened his creation and the Turtle was unable to enter the world. Now with the Pantokrator gone it has realized the aspirations of the other Pretenders and will protect its subjects by donning the mantle of God itself."
        #quadruped
        #gcost 150
        #size 6
        #hp 160
        #prot 20
        #mr 18
        #mor 30
        #str 24
        #att 12
        #def 10
        #prec 10
        #enc 2
        #mapmove 2
        #ap 16
        #magicskill 1 1
        #magicskill 2 1
        #weapon 404 -- Beak
        #goodleader
        #trample
        #amphibian
        #heal
        #female
        #darkvision 50
        #diseaseres 100
        #landshape 4946
        #itemslots 12288 -- 2 misc
        #twiceborn 1235 -- Leviathan
        #maxage 1000
        #startage 500
        #startdom 2
        #pathcost 80
        #homerealm 9 -- Deeps
        #end

        #newmonster 4946
        #name "Divine Turtle"
        #spr1 "./ExtraPretenders/TurtleLand.tga"
        #spr2 "./ExtraPretenders/TurtleLand2.tga"
        #descr "The Divine Turtle is a great turtle that has swum the oceans since the dawn of time. It was worshipped by the creatures of the undersea as a creature of two worlds, the world of air and water. It swims forever searching for a place to lay its eggs, which some say contain the seeds of the next world. When the previous Pantokrator rose to power he imprisoned and banished all that threatened his creation and the Turtle was unable to enter the world. Now with the Pantokrator gone it has realized the aspirations of the other Pretenders and will protect its subjects by donning the mantle of God itself."
        #quadruped
        #gcost 150
        #size 6
        #hp 160
        #prot 20
        #mr 18
        #mor 30
        #str 24
        #att 12
        #def 10
        #prec 10
        #enc 2
        #mapmove 8
        #ap 4
        #magicskill 1 1
        #magicskill 2 1
        #weapon 404 -- Beak
        #goodleader
        #trample
        #amphibian
        #heal
        #female
        #darkvision 50
        #diseaseres 100
        #watershape 4945
        #itemslots 12288 -- 2 misc
        #twiceborn 1235 -- Leviathan
        #maxage 1000
        #startage 500
        #startdom 2
        #pathcost 80
        #end

        #newmonster 4947
        #name "Void Larva"
        #spr1 "./ExtraPretenders/VoidLarva.tga"
        #spr2 "./ExtraPretenders/VoidLarva2.tga"
        #descr "The Void is a realm of strange sights and sounds half dreamed and half imagined. The most powerful Void beings are akin to Gods however most care little for the dealings of this world. This embryonic Void being of great power appeared through the Void Gate and is worshipped by the starspawn of R'lyeh. The Larva does not communicate in any comprehensible way, but its priests divine its will through arcane rituals. Incredibly powerful, the being will drain the life force of those it touches. Partially of the void, it will be difficult to hurt with mundane weaponry."
        #miscshape
        #gcost 150
        #size 5
        #hp 111
        #prot 16
        #mr 20
        #mor 30
        #str 18
        #att 14
        #def 12
        #prec 10
        #enc 2
        #mapmove 3
        #ap 16
        #magicskill 4 2
        #weapon 86 -- Mind Blast
        #weapon 636 -- Life Drain Tentacle
        #weapon 85 -- Tentacle
        #weapon 85 -- Tentacle
        #noleader
        #heal
        #blind
        #diseaseres 100
        #twiceborn 4947
        #ethereal
        #amphibian
        #neednoteat
        #spiritsight
        #fear 10
        #voidsanity 20
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 500
        #startdom 2
        #pathcost 80
        #end

        #newmonster 4948
        #name "Formless One"
        #spr1 "./ExtraPretenders/Formless.tga"
        #spr2 "./ExtraPretenders/Formless2.tga"
        #descr "The Formless One is an otherworldly being called from the Void by the Starspawns of R'lyeh. It appears as an enormous mass of slowly swirling dark goo that ripples and flows in strange patterns. The being has grown to enormous proportions with passing aeons and is now worshipped by the Starspawns as a bringer of the Void. The Formless One can regenerate wounds and will use its great bulk to crush those that oppose it in battle."
        #miscshape
        #gcost 170
        #size 6
        #hp 115
        #prot 16
        #mr 18
        #mor 30
        #str 22
        #att 10
        #def 10
        #prec 10
        #enc 2
        #mapmove 2
        #ap 10
        #magicskill 3 2
        #weapon 90 -- Crush
        #noleader
        #twiceborn 4948
        #regeneration 10
        #fireres 15
        #coldres 15
        #shockres 15
        #poisonres 15
        #blind
        #trample
        #amphibian
        #neednoteat
        #bluntres
        #spiritsight
        #heal
        #diseaseres 100
        #voidsanity 20
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 500
        #startdom 2
        #pathcost 80
        #regeneration 1
        #end

        #newmonster 4949
        #name "Great Wise Whale"
        #spr1 "./ExtraPretenders/WiseWhale.tga"
        #spr2 "./ExtraPretenders/WiseWhale.tga"
        #descr "The Great Wise Whale was born at the dawn of time and was one of the first beings to swim in the primordial deeps. When the previous Pantokrator rose to power it sank to the bottom of the ocean in a deep slumber. Over time it has grown larger, wiser and stronger and now it has come to claim the mantle of Godhood. The Whale remembers everything that has occurred in the world and it is said its death will herald the end of time itself."
        #snake
        #gcost 230
        #size 6
        #hp 250
        #prot 14
        #mr 18
        #mor 30
        #str 30
        #att 10
        #def 6
        #prec 5
        #enc 1
        #mapmove 8
        #ap 6
        #magicskill 1 1
        #magicskill 2 1
        #magicskill 6 1
        #weapon 547 -- Buff
        #expertleader
        #aquatic
        #trample
        #heal
        #blind
        #diseaseres 100
        #inspiringres 1
        #itemslots 12288 -- 2 misc
        #twiceborn 1235 -- Leviathan
        #maxage 10000
        #startage 5000
        #startdom 4
        #pathcost 80
        #homerealm 9 -- Deeps
        #end

        #newmonster 4950
        #name "Abyssal Carcass"
        #spr1 "./ExtraPretenders/LongdeadWhale.tga"
        #spr2 "./ExtraPretenders/LongdeadWhale.tga"
        #descr "The Abyssal Carcass is an ancient spirit from the lightless depths of the ocean inhabiting the long dead remains of a mighty whale. The spirit uses powerful death magic to keep the corpse animated and in a state of unnatural undeath as it roams the seas. The great bulk of the creature makes it difficult to destroy, however the bones are brittle."
        #snake
        #gcost 200
        #size 6
        #hp 150
        #prot 12
        #mr 18
        #mor 30
        #str 26
        #att 12
        #def 6
        #prec 5
        #enc 0
        #mapmove 2
        #ap 6
        #magicskill 2 1
        #magicskill 5 2
        #weapon 547 -- Buff
        #okleader
        #expertundeadleader
        #aquatic
        #pierceres
        #undead
        #inanimate
        #neednoteat
        #spiritsight
        #heal
        #poisonres 25
        #coldres 15
        #diseaseres 100
        #fear 10
        #itemslots 12288 -- 2 misc
        #maxage 1000
        #startage 250
        #startdom 4
        #pathcost 80
        #homerealm 9 -- Deeps
        #end

        #newmonster 4952
        #name "Icon"
        #spr1 "./ExtraPretenders/Jugger.tga"
        #spr2 "./ExtraPretenders/Jugger.tga"
        #descr "The Icon is a powerful spirit that inhabits a colossal sacred structure. The structure has been mounted with religious idols and covered by a layer of gold to reflect the glory of the spirit within. Originally immobile, with the awakening of the spirit it has been mounted on two pairs of enormous wheels to provide mobility to the Lord. The Icon is tremendously strong in its dominion and magically powerful. The conveyance can be destroyed, but not easily and its massive bulk will scatter those that stand against it."
        #miscshape
        #gcost 250
        #size 6
        #hp 200
        #prot 20
        #mr 18
        #mor 30
        #str 20
        #att 5
        #def 5
        #prec 5
        #enc 0
        #mapmove 3
        #ap 6
        #magicskill 4 3
        #weapon 547 -- Buff
        #expertleader
        #trample
        #inanimate
        #spiritsight
        #heal
        #diseaseres 100
        #blind
        #itemslots 28672 
        #poisonres 15
        #maxage 2000
        #startage 1000
        #startdom 4
        #pathcost 80
        #bonusspells 1
        #spreaddom 2
        #end

        #newmonster 4954
        #spr1 "./ExtraPretenders/Firstsirrush.tga"
        #spr2 "./ExtraPretenders/Firstsirrush2.tga"
        #name "First Mushussu"
        #descr "The First Mushussu claims to be the most ancient of all-dragon kin, born at the dawn of time from the primordial swamps of Ur when monsters and giants roamed the world. Like lesser Mushussus it has a scaled body, the paws of a lion, the hindlegs of an eagle and a serpent tail. When the previous Pantokrator rose to power he imprisoned and banished all that threatened his creation and the creature was unable to enter the world. Now with the Pantokrator gone it has realized the aspirations of the other Pretenders and will lead the Enkidu by donning the mantle of God itself."
        #quadruped
        #gcost 200
        #hp 120
        #size 6
        #prot 20
        #mr 18
        #mor  30
        #str 25
        #att 15
        #def 12
        #prec 10
        #enc 2
        #mapmove 3
        #ap 16
        #weapon 399 -- Gore
        #weapon 29 -- Claw
        #weapon 398 -- Venomous fangs
        #itemslots 12416
        #poisonres 15
        #fear 5
        #swampsurvival
        #heal
        #diseaseres 100
        #goodleader
        #dragonlord 2
        #magicskill 2 1
        #magicskill 6 1
        #twiceborn 5769 -- Wight Beast
        #pathcost 80
        #startdom 2
        #maxage 1000
        #nametype 151
        #end

        #newmonster 4955
        #name "God Spider"
        #spr1 "./ExtraPretenders/GodSpider.tga"
        #spr2 "./ExtraPretenders/GodSpider2.tga"
        #descr "The God Spider is a monstrous spider from an earlier era, when monsters and giants roamed the world. Fearsome, cunning and patient it claims to be lord of all spiders and can strike from the darkness with strong webs and lethal poison. When people began to worship it hoping to be spared from its hunger the Pantokrator banished it to the outer darkness for eternity, where it would be condemned to weave its webs forever with no prey to catch. But now the Pantokrator is gone and the God Spider has returned to claim the world."
        #quadruped
        #gcost 150
        #hp 120
        #size 6
        #prot 20
        #mr 18
        #mor 30
        #str 18
        #att 15
        #def 13
        #prec 8
        #enc 2
        #mapmove 3
        #ap 24
        #expertleader
        #maxage 5000
        #poisonres 15
        #eyes 8
        #forestsurvival
        #heal
        #domsummon2 5784 -- Wolf Spiders
        #diseaseres 100
        #itemslots 12288
        #weapon 65 -- fangs
        #weapon 261 -- web
        #weapon 262 -- web spit
        #pathcost 80
        #magicskill 5 1
        #magicskill 6 1
        #startdom 2
        #homerealm 7
        #twiceborn 5769 -- Wight Beast
        #assassin
        #patience 4
        #scalewalls
        #stealthy 20
        #fear 5
        #darkvision 100
        #darkpower 2
        #nametype 126 -- machaka male
        #end

        #newmonster 5050
        #name "Brazen Bull"
        #spr1 "./ExtraPretenders/BrazenBull.tga"
        #spr2 "./ExtraPretenders/BrazenBull2.tga"
        #descr "The Brazen Bull is a terrible statue of a bull cast in bronze and animated by a bloodthirsty spirit. A hatch allows sacrifices to be placed inside, where they will be burnt to a crisp by the roaring furnace that burns in the belly of the bull. The screams of the sacrifices are channeled to create the bellowing of the demon bull and smoke pours from its mouth and nose. Over centuries the bull has demanded ever greater sacrifices to sate its hunger. Countless offerings later and now with the Pantokrator gone, the Brazen Bull has the chance of putting the world under its strong dominion and becoming the True God."
        #quadruped
        #gcost 150
        #hp 180
        #size 6
        #prot 20
        #mr 18
        #mor 30
        #str 28
        #att 13
        #def 12
        #prec 8
        #enc 0
        #mapmove 3
        #ap 16
        #expertleader
        #okundeadleader
        #maxage 5000
        #startage 1000
        #poisonres 25
        #heat 10
        #deathfire 20
        #trample
        #inanimate
        #neednoteat
        #heal
        #diseaseres 100
        #poisonres 25
        #fireres 15
        #popkill 5
        #weapon 55 -- Hoof
        #weapon 48 -- Fire Flare
        #itemslots 12288
        #pathcost 80
        #magicskill 0 2
        #magicskill 7 1
        #startdom 2
        #end

        #newmonster 5055
        #name "Megalodon"
        #spr1 "./ExtraPretenders/Megalodon.tga"
        #spr2 "./ExtraPretenders/Megalodon2.tga"
        #descr "The Megalodon is a giant prehistoric shark that has swum the oceans since the dawn of time. It is aquatic and cannot leave the water. Over the centuries it has devoured thousands of victims and the concentration of blood has awakened great power in the creature. Now it hungers for the blood of Gods, and desires to rule so that it may feed on sacrifices brought from all the corners of the world. The Megalodon's hunger is insatiable and it will devour any that stand in its path. It is followed by sharks that feed from the scraps left by the beast."
        #snake
        #gcost 130
        #size 6
        #hp 150
        #prot 20
        #mr 18
        #mor 30
        #str 25
        #att 12
        #def 10
        #prec 5
        #enc 2
        #mapmove 2
        #ap 22
        #magicskill 7 2
        #weapon 20 -- Bite
        #weapon 589 -- Tail Slap
        #batstartsum1d6 816 -- Shark
        #goodleader
        #okundeadleader
        #aquatic
        #heal
        #darkvision 50
        #diseaseres 100
        #fear 5
        #berserk 5
        #popkill 5
        #itemslots 12288 -- 2 misc
        #twiceborn 1235 -- Leviathan
        #maxage 1000
        #startage 500
        #startdom 2
        #pathcost 80
        #homerealm 9 -- Deeps
        #end

        #newmonster 5059
        #name "Ruk"
        #spr1 "./ExtraPretenders/Ruk.tga"
        #spr2 "./ExtraPretenders/Ruk2.tga"
        #descr "The Ruk is a terrible bird of gigantic proportions from an earlier era, when monsters and giants roamed the world. It is sometimes mistaken for a flying mountain as it soars overhead. When the previous Pantokrator rose to power he imprisoned and banished all that threatened his power and the Ruk was forever imprisoned. Now, with the Pantokrator gone, the Ruk is breaking free to once again terrorise the world. It prefers to feed on elephants by catching them in its claws and dropping them from a great height. The skin of the Ruk is as hard as stone and its screeches bring fear to the hearts of men. In a siege the Ruk would be worth fifty normal men."
        #bird
        #gcost 180
        #hp 130
        #size 6
        #prot 18
        #mr 18
        #mor 30
        #str 28
        #att 13
        #def 14
        #prec 8
        #enc 1
        #mapmove 22
        #ap 8
        #magicskill 3 2
        #fear 5
        #goodleader
        #flying
        #heal
        #diseaseres 100
        #mountainsurvival
        #weapon 408 -- Talons
        #weapon 404 -- Beak
        #itemslots 12288 -- 2 misc
        #siegebonus 50
        #maxage 500
        #startage 100
        #twiceborn 1388 -- Ziz
        #pathcost 80
        #startdom 2
        #homerealm 5 -- Middle East
        #end

        #newmonster 5061
        #spr1 "./ExtraPretenders/BigBear.tga"
        #spr2 "./ExtraPretenders/BigBear2.tga"
        #name "Spirit Bear"
        #descr "Born at the dawn of time the Spirit Bear represents an earlier era, when monsters and giants roamed the world. The Spirit Bear is a huge bear with white fur that roams the frozen wastes. It has a legendary fury and over time the warriors of the cold north began to worship the beast. The Pantokrator saw this and imprisoned the beast for eternity. Now with the Pantokrator gone the Spirit Bear is free to roam the frozen lands once more."
        #fixedname "Nanook"
        #quadruped
        #gcost 160
        #hp 188
        #size 6
        #prot 20
        #mr 18
        #mor 30
        #str 28
        #att 13
        #def 10
        #prec 8
        #enc 2
        #mapmove 3
        #ap 14
        #magicskill 4 2
        #weapon 20 -- Bite
        #weapon 29 -- Claw
        #startage 1000
        #maxage 2000
        #fear 5
        #berserk 7
        #coldres 15
        #woundfend 2
        #diseaseres 100
        #snow
        #forestsurvival
        #heal
        #spiritsight
        #goodleader
        #okmagicleader
        #twiceborn 5769 -- Wight Beast
        #pathcost 80
        #startdom 2
        #homerealm 1
        #end

        #newmonster 5064
        #spr1 "./ExtraPretenders/Froggod.tga"
        #spr2 "./ExtraPretenders/Froggod2.tga"
        #name "Great Toad"
        #descr "The Great Toad is a gigantic monstrous Toad that has existed since the dawn of time, when monsters and giants roamed the world. The physical bulk of the Toad is awesome and its appetite is unending. In ages past it devoured beasts and men until the Pantokrator imprisoned it to prevent it from consuming the whole world. Now, with the Pantokrator gone, the shackles of the eternal prison are weakening and the Great Toad is preparing to return once more. The toad has thick leathery skin and its huge form renders weapons unable to pierce it mostly ineffectual."
        #fixedname "Tsathoggua"
        #quadruped
        #gcost 190
        #hp 190
        #size 6
        #prot 16
        #mr 18
        #mor 30
        #str 24
        #att 13
        #def 10
        #prec 8
        #enc 2
        #mapmove 14
        #ap 14
        #magicskill 6 2
        #weapon 461 -- Swallow
        #weapon 29 -- Claw
        #startage 1000
        #maxage 2000
        #fear 5
        #poisonres 25
        #poisonskin 20
        #poisoncloud 8
        #diseaseres 100
        #twiceborn 5769 -- Wight Beast
        #popkill 10
        #swampsurvival
        #bluntres
        #slashres
        #coldblood
        #amphibian
        #heal
        #goodleader
        #supplybonus -30
        #pathcost 80
        #startdom 2
        #homerealm 6 -- Middle America
        #end
    -- MESSENGERS  

        #newmonster 4956
        #name "Tathagata of Air"
        #spr1 "./ExtraPretenders/TathagataAir.tga"
        #spr2 "./ExtraPretenders/TathagataAir2.tga"
        #descr "The Five Wisdom Tathagatas are emanations of the Pantokrator made to embody aspects of nature and enlightenment. Each Tathagata would lead the faithful to new understanding and wisdom. Over time they began to be worshipped in their own right and the Pantokrator imprisoned them in a fit of rage. The Tathagata of Air embodies the wisdom of perfect practice and the avoidance of envy. The winds protect it and will deflect enemy missiles aimed against the Tathagata. Now with the Pantokrator gone it has realized the aspirations of the other Pretenders and will protect its subjects by donning the mantle of God itself. Each Tathagata is protected by a Wisdom King, a divine warrior that manifests when they are threatened with harm."
        #humanoid
        #fixedname "Amoghasiddhi"
        #gcost 180
        #size 4
        #hp 45
        #prot 3
        #mr 18
        #mor 30
        #str 18
        #att 14
        #def 14
        #prec 12
        #enc 2
        #mapmove 2
        #ap 12
        #weapon 1840 -- Perfect Fist
        #goodleader
        #heal
        #diseaseres 100
        #batstartsum1 4961 -- Wisdom King
        #awe 1
        #airshield 80
        #maxage 1000
        #startage 500
        #magicskill 1 2
        #pathcost 80
        #startdom 2
        #homerealm 8 -- India
        #end

        #newmonster 4957
        #name "Tathagata of Fire"
        #spr1 "./ExtraPretenders/TathagataFire.tga"
        #spr2 "./ExtraPretenders/TathagataFire2.tga"
        #descr "The Five Wisdom Tathagatas are emanations of the Pantokrator made to embody aspects of nature and enlightenment. Each Tathagata would lead the faithful to new understanding and wisdom. Over time they began to be worshipped in their own right and the Pantokrator imprisoned them in a fit of rage. The Tathagata of Fire embodies the wisdom of meditation and the avoidance of selfishness. It is surrounded by a shield of flames that will burn attackers. Now with the Pantokrator gone it has realized the aspirations of the other Pretenders and will protect its subjects by donning the mantle of God itself. Each Tathagata is protected by a Wisdom King, a divine warrior that manifests when they are threatened with harm."
        #humanoid
        #fixedname "Amitabha"
        #gcost 180
        #size 4
        #hp 45
        #prot 3
        #mr 18
        #mor 30
        #str 18
        #att 14
        #def 14
        #prec 12
        #enc 2
        #mapmove 2
        #ap 12
        #weapon 1840 -- Perfect Fist
        #goodleader
        #heal
        #diseaseres 100
        #batstartsum1 4961 -- Wisdom King
        #awe 1
        #fireshield 8
        #maxage 1000
        #startage 500
        #magicskill 0 2
        #pathcost 80
        #startdom 2
        #homerealm 8 -- India
        #end

        #newmonster 4958
        #name "Tathagata of Earth"
        #spr1 "./ExtraPretenders/TathagataEarth.tga"
        #spr2 "./ExtraPretenders/TathagataEarth2.tga"
        #descr "The Five Wisdom Tathagatas are emanations of the Pantokrator made to embody aspects of nature and enlightenment. Each Tathagata would lead the faithful to new understanding and wisdom. Over time they began to be worshipped in their own right and the Pantokrator imprisoned them in a fit of rage. The Tathagata of Earth embodies the wisdom of equanimity and the avoidance of greed. It draws strength from the ground to refresh and reinvigorate it in battle. Now with the Pantokrator gone it has realized the aspirations of the other Pretenders and will protect its subjects by donning the mantle of God itself. Each Tathagata is protected by a Wisdom King, a divine warrior that manifests when they are threatened with harm."
        #fixedname "Ratnasambhava"
        #humanoid
        #gcost 180
        #size 4
        #hp 45
        #prot 8
        #mr 18
        #mor 30
        #str 18
        #att 14
        #def 14
        #prec 12
        #enc 2
        #mapmove 2
        #ap 12
        #weapon 1840 -- Perfect Fist
        #goodleader
        #heal
        #diseaseres 100
        #batstartsum1 4961 -- Wisdom King
        #awe 1
        #reinvigoration 4
        #maxage 1000
        #startage 500
        #magicskill 3 2
        #pathcost 80
        #startdom 2
        #homerealm 8 -- India
        #end

        #newmonster 4959
        #name "Tathagata of Water"
        #spr1 "./ExtraPretenders/TathagataWater.tga"
        #spr2 "./ExtraPretenders/TathagataWater2.tga"
        #descr "The Five Wisdom Tathagatas are emanations of the Pantokrator made to embody aspects of nature and enlightenment. Each Tathagata would lead the faithful to new understanding and wisdom. Over time they began to be worshipped in their own right and the Pantokrator imprisoned them in a fit of rage. The Tathagata of Water embodies the wisdom of reflection and the avoidance of aggression. It acts with divine speed and grace. Now with the Pantokrator gone it has realized the aspirations of the other Pretenders and will protect its subjects by donning the mantle of God itself. Each Tathagata is protected by a Wisdom King, a divine warrior that manifests when they are threatened with harm."
        #fixedname "Akshobhya"
        #humanoid
        #gcost 180
        #size 4
        #hp 45
        #prot 3
        #mr 18
        #mor 30
        #str 18
        #att 14
        #def 14
        #prec 12
        #enc 2
        #mapmove 2
        #ap 12
        #weapon 1840 -- Perfect Fist
        #goodleader
        #heal
        #diseaseres 100
        #batstartsum1 4961 -- Wisdom King
        #awe 1
        #onebattlespell 610 -- Quicken Self
        #maxage 1000
        #startage 500
        #magicskill 2 2
        #pathcost 80
        #startdom 2
        #homerealm 8 -- India
        #end

        #newmonster 4960
        #name "Tathagata of Void"
        #spr1 "./ExtraPretenders/TathagataVoid.tga"
        #spr2 "./ExtraPretenders/TathagataVoid2.tga"
        #descr "The Five Wisdom Tathagatas are emanations of the Pantokrator made to embody aspects of nature and enlightenment. Each Tathagata would lead the faithful to new understanding and wisdom. Over time they began to be worshipped in their own right and the Pantokrator imprisoned them in a fit of rage. The Tathagata of Void embodies the wisdom of emptiness and the avoidance of ignorance. It is only partially in this realm and is very hard to affect except with magic. Now with the Pantokrator gone it has realized the aspirations of the other Pretenders and will protect its subjects by donning the mantle of God itself. Each Tathagata is protected by a Wisdom King, a divine warrior that manifests when they are threatened with harm."
        #humanoid
        #gcost 180
        #size 4
        #hp 45
        #prot 3
        #mr 18
        #mor 30
        #str 18
        #att 14
        #def 14
        #prec 12
        #enc 2
        #mapmove 2
        #ap 12
        #weapon 1840 -- Perfect Fist
        #goodleader
        #heal
        #diseaseres 100
        #batstartsum1 4961 -- Wisdom King
        #awe 1
        #ethereal
        #maxage 1000
        #startage 500
        #magicskill 4 2
        #pathcost 80
        #startdom 2
        #homerealm 8 -- India
        #end

        #newmonster 4961
        #copystats 499 -- Nataraja
        #clearweapons
        #clearmagic
        #name "Wisdom King"
        #spr1 "./ExtraPretenders/WisdomKing.tga"
        #spr2 "./ExtraPretenders/WisdomKing2.tga"
        #descr "The Wisdom Kings or Guardian Kings are divine warriors created to protect the Tathagatas. Whereas the Tathagatas represent pure concepts and teach through compassion, Wisdom Kings are the embodiment of the wheel of injunction and teach through fear, shocking nonbelievers into faith. They are wrathful manifestations of the divine, many-armed and wreathed in flames."
        #humanoid
        #gcost 0
        #size 6
        #mr 18
        #mor 18
        #invulnerable 20
        #speciallook 1
        #fear 5
        #holy
        #heal
        #ambidextrous 6
        #itemslots 7326 -- 4 hands, 1 misc
        #diseaseres 100
        #weapon 1841 -- Sword of Divine Fire
        #weapon 1841 -- Sword of Divine Fire
        #weapon 1842 -- Magic Lariat
        #homerealm 0
        #end

        #newmonster 5027
        #copystats 499 -- Nataraja
        #clearweapons
        #clearmagic
        #name "Wisdom King"
        #spr1 "./ExtraPretenders/WisdomKing.tga"
        #spr2 "./ExtraPretenders/WisdomKing2.tga"
        #descr "The Wisdom King is a divine warrior that was assigned to protect the Tathagata. Now with the Pantokrator gone his full powers have begun to manifest and he has amassed worshippers of his own. In his Wisdom he has realised the aspirations of the other Pretender Gods and will attain the role of Pantokrator to protect his followers. The Wisdom King is the embodiment of the wheel of injunction and teaches through fear, shocking nonbelievers into faith. He is a wrathful manifestation of the divine, many-armed and wreathed in flames. The Wisdom King is highly resilient and will suffer permanent injuries less often than most creatures. In combat he will manifest magical Fire, Astral and Death gems to aid in spellcasting."
        #humanoid
        #gcost 250
        #hp 95
        #prot 14
        #awe 1
        #mr 20
        #woundfend 1
        #fear 10
        #expertleader
        #heal
        #ambidextrous 6
        #itemslots 7326 -- 4 hands, 1 misc
        #diseaseres 100
        #batstartsum2 0
        #weapon 1841 -- Sword of Divine Fire
        #weapon 1841 -- Sword of Divine Fire
        #weapon 1842 -- Magic Lariat
        #magicskill 0 1
        #magicskill 4 1
        #magicskill 5 1
        #tmpfiregems 1
        #tmpastralgems 1
        #tmpdeathgems 1
        #end

        #newmonster 4962
        #name "Sea Nymph"
        #spr1 "./ExtraPretenders/SeaNymph.tga"
        #spr2 "./ExtraPretenders/SeaNymph2.tga"
        #descr "The Sea Nymph is an ancient shapeshifting spirit of the ocean. She was once forced to marry a mortal at the behest of the Pantokrator, however she escaped back to the sea after birthing a son. When above the waves she will take the form of a roaring flame. Now the spirit has realized the aspirations of the other Pretenders and will protect her subjects by donning the mantle of God itself."
        #fixedname "Thetis"
        #djinn
        #gcost 180
        #size 4
        #hp 55
        #prot 0
        #mr 18
        #mor 30
        #str 16
        #att 13
        #def 13
        #enc 0
        #ap 12
        #uwheat 6
        #magicskill 0 1
        #magicskill 2 1
        #poisonres 15
        #coldres 10
        #fireres 10
        #goodleader
        #goodmagicleader
        #amphibian
        #neednoteat
        #slashres
        #pierceres
        #bluntres
        #neednoteat
        #spiritsight
        #heal
        #diseaseres 100
        #female
        #giftofwater 70
        #darkvision 100
        #mapmove 16
        #weapon 90 -- Crush
        #pathcost 40
        #startdom 2
        #homerealm 9 -- Deeps
        #landshape 4963
        #end

        #newmonster 4963
        #copystats 596 -- Size 4 Fire Elemental
        #copyspr 596 -- Size 4 Fire Elemental
        #name "Sea Nymph"
        #descr "The Sea Nymph is an ancient shapeshifting spirit of the ocean. She was once forced to marry a mortal at the behest of the Pantokrator, however she escaped back to the sea after birthing a son. When above the waves she will take the form of a roaring flame. Now the spirit has realized the aspirations of the other Pretenders and will protect her subjects by donning the mantle of God itself."
        #miscshape
        #hp 50
        #mr 18
        #mor 30
        #uwdamage 0
        #fireres 10
        #magicskill 0 1
        #magicskill 2 1
        #secondshape 0
        #watershape 4962
        #heal
        #female
        #spiritsight
        #diseaseres 100
        #amphibian
        #pathcost 40
        #startdom 2
        #end

        #newmonster 4964
        #copystats 1241 -- Samurai
        #clearweapons
        #cleararmor
        #name "Eelfolk Samurai"
        #spr1 "./MagicEnhanced/EEEelfolk.tga"
        #spr2 "./MagicEnhanced/EEEelfolk2.tga"
        #descr "The Eelfolk are strange half-men that dwell in freshwater rivers. They are able to leave the water and claim that they are the descendants of freshwater dragons. They are sometimes called upon by the Dragon Kings and maintain an uneasy alliance with their deep-sea cousins. Eelfolk samurai are skilled swordsmen and carry blades of treasure-coral coated in poison. They are able to enter the sea, however they prefer to avoid lengthy stays as the salt irritates their skin."
        #hp 12
        #mor 13
        #att 12
        #def 12
        #weapon 1882 -- Coral Sword
        #weapon 20 -- Bite
        #armor 129 -- Samurai Armor
        #amphibian
        #end

        #newmonster 4965
        #copystats 1562 -- Void Spectre
        #clearmagic
        #name "Void Essence"
        #spr1 "./ExtraPretenders/VoidEssence.tga"
        #spr2 "./ExtraPretenders/VoidEssence2.tga"
        #descr "This is the spirit of a starspawn that was not content to lay dead but dreaming of the void. It has returned to the world and will use the wisdom of the void to claim the mantle of a God. The Void Essence is highly attuned to the minds of those who are asleep and can bring the wisdom of the stars to the dreams of entire nations. When the Void Essence is in an enemy territory, it will bring insanity to both enemy forces and the general population."
        #djinn
        #gcost 180
        #hp 35
        #mor 30
        #invulnerable 10
        #fear 10
        #heal
        #spreaddom 2
        #diseaseres 100
        #magicskill 5 2
        #pathcost 80
        #startdom 2
        #end

        #newmonster 4966
        #copystats 872 -- Ghost King
        #name "Spectral Ba'al"
        #spr1 "./ExtraPretenders/SpectralBaal.tga"
        #spr2 "./ExtraPretenders/SpectralBaal2.tga"
        #descr "This is the spirit of a Ba'al that has escaped from Sheol and returned to claim the world of the living. The Ba'als were Rephaite Lords of ages past. Living lives of luxury and splendor, they led their kin in the absence of the Nephilim. However, godlike puissance and absolute power over their subjects made them arrogant and depraved. The Ba'als called themselves gods and drank the blood of the Avvim to gain their strength. A Spectral Ba'al inherits the power of their father, and has gained knowledge of Death magic due to his time spent in Sheol. The sacred spirits of dead Rephaim can be summoned by the Ba'al from Sheol to serve him. Spectres are ethereal and able to enter the sea."
        #humanoid
        #gcost 180
        #hp 60
        #size 5
        #str 20
        #def 15
        #fireres 5
        #magicskill 5 1
        #magicskill 7 1
        #spreaddom 2
        #invulnerable 10
        #weapon 59 -- Rod of Death
        #weapon 331 -- Gore
        #makemonsters1 5999 -- Ghostly Rephaite
        #nametype 149 -- Hinnom
        #wastesurvival
        #heal
        #diseaseres 100
        #pathcost 80
        #startdom 2
        #homerealm 0
        #end

        #newmonster 4967
        #name "Once and Future King"
        #spr1 "./ExtraPretenders/OnceFutureKing.tga"
        #spr2 "./ExtraPretenders/OnceFutureKing2.tga"
        #descr "The Once and Future King is the first and greatest ruler of Man. Foremost of the Lord Wardens, he was enchanted by the first Witches of Avalon so that no one could cause him lasting harm and bears magical armaments. 
        He ruled wisely for many years, until one day he went into the forest of Avalon to sleep until the nation would need his wise rule again. 
        That time has come and he has awakened to lead Man as God-King. The Once and Future King is always accompanied by two Wardens of Avalon, and more will arrive as his dominion grows strong.
        The blood of the Tuathas runs strong in his veins granting him innate magical powers."
        #humanoid
        #gcost 150
        #hp 25
        #str 15
        #att 15
        #def 16
        #magicskill 1 1
        #magicskill 6 1
        #mr 18
        #mor 30
        #enc 2
        #mapmove 3
        #ap 12
        #prec 12
        #expertleader
        #forestsurvival
        #illusion
        #heal
        #stealthy 25
        #inspirational 1
        #ambidextrous 3
        #weapon 74 -- 1h sword of sharpness
        #weapon 74 -- 1h sword of sharpness
        #armor 201 -- Armor of Knights
        #armor 174 -- Helmet of champions
        #batstartsum2 65 -- Warden of Avalon
        #raredomsummon 65 -- Warden of Avalon
        #startdom 2
        #pathcost 40
        #fixedname "Arturus"
        #end

        #newmonster 5037
        #copystats 1975 -- Iron Angel
        #name "Blacksteel Angel"
        #spr1 "./ExtraPretenders/BlacksteelAngel.tga"
        #spr2 "./ExtraPretenders/BlacksteelAngel2.tga"
        #descr "The Blacksteel Angel is a divine being that has come to this world to teach men to trust in themselves and the steel they wield. In its quest to rid the world of magic it has now taken the mantle of a Pretender God seeking the Throne of Ascension. Armed with formidable gear, the Blacksteel Angel silently states the strength of superior skill and true craftsmanship. Through superior craftsmanship the local province will be able to equip more soldiers each month. The Angel teaches men not to trust in magic and demonstrates the weakness of false faiths by stopping sacred beings in their tracks. Fanatical adherents of other faiths will find themselves awed, belittled and unable to strike it. The Blacksteel Angel never uses magic and his fist will strike unbelievers with utmost judgment. It is extremely resilient and will rarely suffer permanent injuries."
        #humanoid
        #gcost 120
        #hp 95
        #size 5
        #woundfend 3
        #resources 50
        #startdom 2
        #pathcost 250
        #expertleader
        #heal
        #montag 5186 -- Recruit Black Iron Infantry
        #diseaseres 100
        #end

        #newmonster 5051
        #name "Gallu Demon"
        #spr1 "./ExtraPretenders/Gallu.tga"
        #spr2 "./ExtraPretenders/Gallu2.tga"
        #descr "The Gallu Demon is an extremely powerful spirit being created to enforce the laws of the underworld. Anyone unfortunate enough to commit serious transgressions against divine laws or who escape the underworld are hunted by the Gallu. Although not malign, the Gallu Demon is utterly ruthless and implacable in its duties. It is said the Annunaki of Growth and Rebirth was seized by the Gallu and brought to the underworld unwillingly, to begin his first rebirth cycle. Now the Gallu Demon wishes not only to enforce the divine laws but to decree them, and so has taken the mantle of a Pretender God seeking to become the Pantokrator."
        #fixedname "Asag"
        #humanoid
        #gcost 200
        #hp 65
        #size 5
        #prot 0
        #mr 18
        #mor 30
        #str 24
        #att 14
        #def 14
        #prec 11
        #enc 2
        #mapmove 3
        #ap 18
        #weapon 181 -- implementor axe
        #armor 7 -- scale cuirass
        #itemslots 15494 -- 2 hands, head, body, feet, 2 misc
        #demon
        #flying
        #expertleader
        #goodundeadleader
        #maxage 2000
        #invulnerable 20
        #fear 5
        #patrolbonus 10
        #incunrest -20
        #neednoteat
        #spiritsight
        #heal
        #onebattlespell 604 -- Personal Luck
        #diseaseres 100
        #magicskill 1 1
        #magicskill 5 1
        #startdom 2
        #pathcost 80
        #end

        #newmonster 5068
        #name "Celestial Fox"
        #spr1 "./ExtraPretenders/NineTails.tga"
        #spr2 "./ExtraPretenders/NineTails2.tga"
        #descr "The Celestial Fox is an ancient fox spirit that has existed for millenia. Like all animal spirits it gained in power as it grew older, first learning the secret of transforming itself into human form. Over time it has gained great wisdom and magical knowledge, and now it will take on the mantle of a Pretender God seeking to become the Pantokrator. The Celestial Fox has nine tails and is golden in colour. It can take the form of a beautiful maiden to lure the unwary."
        #fixedname "Nine Tails"
        #quadruped
        #gcost 160
        #hp 20
        #size 2
        #prot 0
        #mr 18
        #mor 30
        #str 10
        #att 12
        #def 14
        #prec 13
        #enc 2
        #mapmove 20
        #ap 18
        #weapon 20 -- Bite
        #itemslots 12288 -- 2 misc
        #magicbeing
        #animal
        #goodleader
        #goodmagicleader
        #startage 1000
        #maxage 2000
        #neednoteat
        #spiritsight
        #researchbonus 10
        #heal
        #diseaseres 100
        #magicskill 1 1
        #magicskill 4 1
        #magicskill 6 1
        #startdom 2
        #pathcost 20
        #homerealm 4
        #shapechange 5069 -- Human shape
        #end

        #newmonster 5069
        #copystats 1433 -- Kitsune
        #copyspr 1433 -- Kitsune
        #clearmagic
        #name "Celestial Fox"
        #descr "The Celestial Fox is an ancient fox spirit that has existed for millenia. Like all animal spirits it gained in power as it grew older, first learning the secret of transforming itself into human form. Over time it has gained great wisdom and magical knowledge, and now it will take on the mantle of a Pretender God seeking to become the Pantokrator. The Celestial Fox has nine tails and is golden in colour. It can take the form of a beautiful maiden to lure the unwary."
        #fixedname "Nine Tails"
        #mr 18
        #mor 30
        #magicbeing
        #animal
        #goodleader
        #goodmagicleader
        #startage 1000
        #maxage 2000
        #neednoteat
        #spiritsight
        #researchbonus 10
        #heal
        #seduce 10
        #stealthy 25
        #diseaseres 100
        #magicskill 1 1
        #magicskill 4 1
        #magicskill 6 1
        #magicboost 53 -1
        #startdom 2
        #pathcost 20
        #shapechange 5068 -- Fox Shape
        #end

        #newmonster 5070
        #spr1 "./ExtraPretenders/SunWukong.tga"
        #spr2 "./ExtraPretenders/SunWukong2.tga"
        #name "Monkey King"
        #descr "The Monkey King is a divine monkey spirit born from a magic stone. To attract the attention of the Pantokrator and gain immortality he first stole from the Dragon King and then defied the Kings of Hell by wiping the names of all his subjects from the Book of Life and Death. When the Pantokrator heard of his antics he granted him the lowest place in Heaven to keep a close eye upon him, however the Monkey King was furious at this. He rebelled and led a group of lesser deities against the forces of Heaven until he was captured and sealed below a mountain. Now, with the Pantokrator gone the Monkey King will soon escape to vie for control of the Throne of Heaven. He rides a magic cloud that bears him through the skies, and his strength is legendary. The Monkey King is skilled in the magic of the wind, the water and the earth."
        #fixedname "Sun Wukong"
        #humanoid
        #gcost 160
        #hp 25
        #size 3
        #prot 0
        #mr 18
        #mor 30
        #str 22
        #att 14
        #def 14
        #prec 14
        #enc 2
        #mapmove 20
        #ap 14
        #weapon 238 -- Magic Staff
        #magicbeing
        #animal
        #flying
        #superiorleader
        #beastmaster 1
        #startage 1000
        #maxage 2000
        #heal
        #diseaseres 100
        #magicskill 1 1
        #magicskill 2 1
        #magicskill 3 1
        #startdom 2
        #pathcost 40
        #homerealm 4
        #end

        #newmonster 6769
        #spr1 "./ExtraPretenders/Hanuman.tga"
        #spr2 "./ExtraPretenders/Hanuman2.tga"
        #name "Divine Monkey"
        #descr "The Divine Monkey was born long ago to his father, the God of Wind and an Apsara in the form of a Vanara. In his youth he was granted wishes by his father and wished to be unharmed by fire or water and to be as swift as the wind. After serving the Monkey King for many years he became known as a devout and loyal servant of the Pantokrator, serving as messenger for the One True God and aiding him against his enemies. After a particularly difficult battle the Pantokrator offered him a great reward, however the Monkey asked only for a place at his feet to worship him. Touched, the Pantokrator blessed his servant with immortality. One day however, the Pantokrator was gone and his gifts had waned. Bereft, the Divine Monkey has decided to don the yoke of Godhood and lead his people in the battle for Ascension. He is unharmed by flames, can survive under the water and is a competent general of the monkeyfolk. His physical and magical prowess are legendary and he has power over the air, the water and the land."
        #fixedname "Hanuman"
        #humanoid
        #gcost 170
        #hp 25
        #size 2
        #prot 0
        #mr 18
        #mor 30
        #str 22
        #att 14
        #def 14
        #prec 14
        #enc 2
        #mapmove 20
        #ap 14
        #weapon 1840 -- Perfect Fist
        #weapon 175 -- Chi Kick
        #fireres 15
        #pooramphibian
        #animal
        #forestsurvival
        #expertleader
        #spiritsight
        #beastmaster 2
        #startage 1000
        #maxage 2000
        #heal
        #diseaseres 100
        #magicskill 1 1
        #magicskill 2 1
        #magicskill 6 1
        #startdom 2
        #pathcost 40
        #homerealm 8
        #end

        #newmonster 5071
        #spr1 "./ExtraPretenders/ThroneLich.tga"
        #spr2 "./ExtraPretenders/ThroneLich2.tga"
        #name "Returned King"
        #descr "The Returned King is a long-dead monarch that has recently risen again to rule over the land of the living. Skilled in necromancy, the King will animate the dead to serve him as his Dominion grows strong. The magic that animates the King also binds him to his Throne and he cannot leave it by any means. Now the King will don the mantle of a Pretender God to attain the title of Pantokrator and escape his imprisonment."
        #humanoid
        #gcost 100
        #hp 25
        #size 4
        #prot 0
        #mr 18
        #mor 30
        #str 15
        #att 12
        #def 0
        #prec 12
        #enc 0
        #mapmove 0
        #ap 2
        #weapon 92 -- Fist
        #armor 148 -- Crown
        #immobile
        #unteleportable
        #undead
        #inanimate
        #coldres 15
        #poisonres 25
        #pierceres
        #neednoteat
        #spiritsight
        #okleader
        #expertundeadleader
        #domsummon -2 -- Longdead
        #startage 1000
        #maxage 2000
        #heal
        #diseaseres 100
        #magicskill 5 1
        #startdom 2
        #pathcost 20
        #homerealm 10
        #end



        ---------  TITANS  ------------------------------

        #newmonster 4969
        #name "Kami of the Earth"
        #spr1 "./ExtraPretenders/OkamiEarth.tga"
        #spr2 "./ExtraPretenders/OkamiEarth2.tga"
        #descr "The Kami is a giant of divine heritage. The previous Pantokrator created him to bring strength and guidance to the peoples of the earth. When the Pantokrator saw how he was revered he flew into a rage and the Kami was imprisoned in a clam under the sea. With the Pantokrator gone, the imprisonment has ended and the Kami has returned to the world to claim it as his own. The Kami can command earth elementals to rise and serve him each month. He draws strength from the earth itself and in battle will manifest Earth gems to aid in spellcasting."
        #fixedname "Sarutahiko"
        #humanoid
        #gcost 180
        #size 6
        #hp 122
        #prot 5
        #mr 20
        #mor 30
        #str 22
        #att 14
        #def 12
        #prec 14
        #enc 3
        #mapmove 3
        #ap 12
        #magicskill 3 3
        #reinvigoration 4
        #makemonsters2 495 -- Size 4 Earth Elemental
        #weapon 345 -- Fly Whisk
        #armor 159 -- Imperial Robes
        #expertleader
        #heal
        #diseaseres 100
        #maxage 3000
        #startage 1000
        #pathcost 60
        #homerealm 4 -- Far East
        #startdom 3
        #tmpearthgems 3
        #end

        #newmonster 4971
        #name "Kami of Fertility"
        #spr1 "./ExtraPretenders/OkamiFertility.tga"
        #spr2 "./ExtraPretenders/OkamiFertility.tga"
        #descr "The Kami is a giant of divine heritage. The previous Pantokrator created her to spread fertility across the world, however When he saw how she was worshipped he imprisoned her below the earth. With the Pantokrator gone, the prison is weakening and the Kami can return once more to the world she loves. Plants will bloom in her presence and the harvest will be bountiful in a province where she dwells, generating thirty percent additional tax for the treasury. In combat attackers will be ensnared in vines before they are able to harm her, and the Kami will manifest Earth and Nature gems to aid in spellcasting."
        #fixedname "Inari"
        #humanoid
        #gcost 240
        #size 6
        #hp 92
        #prot 3
        #mr 20
        #mor 30
        #str 22
        #att 14
        #def 13
        #prec 14
        #enc 2
        #mapmove 3
        #ap 16
        #magicskill 6 2
        #magicskill 3 1
        #supplybonus 50
        #entangle
        #weapon 92 -- Fist
        #armor 159 -- Imperial Robes
        #superiorleader
        #heal
        #diseaseres 100
        #maxage 3000
        #startage 1000
        #pathcost 60
        #homerealm 4 -- Far East
        #startdom 3
        #tmpnaturegems 2
        #tmpearthgems 1
        #female
        #end

        #newevent
        #rarity 5
        #req_monster 4971 -- Kami of Fertility
        #msg "Kami Taxboost"
        #notext
        #nolog
        #taxboost 30
        #end

        #newmonster 4973
        #name "Kami of War"
        #spr1 "./ExtraPretenders/Bishamon.tga"
        #spr2 "./ExtraPretenders/Bishamon2.tga"
        #descr "The Kami is a giant of divine heritage. The previous Pantokrator created him as a great warrior to defend the lands of the east. When the Pantokrator saw how he was revered by warriors and generals he flew into a rage and imprisoned him for eternity. With the Pantokrator gone, he can return to the world to lead his followers in battle against those that would deny the true God. Armed and armoured for battle, he is the patron of fighters and those that act with honour. The Kami wields a demon-slaying spear and a pagoda of light that will intimidate the weak-willed. In battle he will manifest Fire, Earth and Nature gems to aid in spellcasting."
        #fixedname "Bishamon"
        #humanoid
        #gcost 200
        #size 6
        #hp 98
        #prot 5
        #mr 20
        #mor 30
        #str 24
        #att 14
        #def 14
        #prec 14
        #enc 2
        #mapmove 3
        #ap 16
        #magicskill 0 1
        #magicskill 3 1
        #magicskill 6 1
        #weapon 712 -- Apotropaic Spear
        #armor 19 -- Full Plate Mail
        #armor 118 -- Half Helmet
        #superiorleader
        #heal
        #diseaseres 100
        #awe 1
        #maxage 3000
        #startage 1000
        #pathcost 60
        #homerealm 4 -- Far East
        #startdom 3
        #tmpfiregems 1
        #tmpnaturegems 1
        #tmpearthgems 1
        #end

        #newmonster 4974
        #name "Lord of Floods"
        #spr1 "./ExtraPretenders/LordFloods.tga"
        #spr2 "./ExtraPretenders/LordFloods2.tga"
        #descr "The Lord of Floods is a great being of the seas that was born at the dawn of time, when the sea was untamed and filled with monstrous beings. He was given power over the water of the land and the deeps by a previous Pantokrator, however he misused his power to bring floods and devastation. The Pantokrator saw what his servant had wrought and set a great warrior to bind him. The battle that followed raged for many days and nights and ended with the sky being knocked off its axis and causing all rivers to flow to the southeast. After being imprisoned at the bottom of the ocean he has now begun to stir once more. The Lord of Floods can travel below the waves and is feared by all living beings. Now he seeks to become the True God and the world will tremble. In combat he will manifest Water and Earth gems to aid in spellcasting."
        #fixedname "Guan-Yong"
        #djinn
        #gcost 250
        #size 6
        #hp 130
        #prot 16
        #mr 20
        #mor 30
        #str 24
        #att 14
        #def 14
        #prec 12
        #enc 2
        #mapmove 2
        #ap 8
        #amphibian
        #fear 5
        #itemslots 13446 -- No Feet
        #magicskill 2 2
        #magicskill 3 1
        #weapon 20 -- Bite
        #weapon 29 -- Claw
        #weapon 1844 -- Earth Shake
        #expertleader
        #heal
        #poisonres 15
        #diseaseres 100
        #maxage 3000
        #startage 1000
        #pathcost 60
        #homerealm 4 -- Far East
        #startdom 3
        #tmpwatergems 2
        #tmpearthgems 1
        #end

        #newmonster 4975
        #name "Lord of Knowledge"
        #spr1 "./ExtraPretenders/Ganesha.tga"
        #spr2 "./ExtraPretenders/Ganesha2.tga"
        #descr "The Lord of Knowledge is an elephantine giant of divine heritage. He is wise beyond imagining and will share his wisdom with his followers. Sacred White elephants will sometimes appear to serve him when called from the forest. He is worshipped by his followers as the Master of learning and writing and now he is out to become the True God. In combat he will manifest magical Water, Astral and Nature gems to aid in spellcasting."
        #fixedname "Ganesha"
        #humanoid
        #gcost 220
        #size 6
        #hp 150
        #prot 8
        #mr 20
        #mor 30
        #str 22
        #att 12
        #def 12
        #prec 12
        #enc 3
        #mapmove 3
        #ap 16
        #magicskill 2 1
        #magicskill 4 1
        #magicskill 6 1
        #researchbonus 20
        #inspiringres 2
        #domsummon20 5724 -- White Elephant
        #makemonsters1 5724 -- White Elephant
        #weapon 614 -- Tusk
        #weapon 92 -- Punch
        #expertleader
        #heal
        #ambidextrous 2
        #itemslots 7326 -- 4 hands, 1 misc
        #diseaseres 100
        #maxage 3000
        #startage 1000
        #pathcost 60
        #startdom 3
        #homerealm 8 -- India
        #tmpwatergems 1
        #tmpastralgems 1
        #tmpnaturegems 1
        #end

        #newmonster 4976
        #name "Terror of the Deep"
        #spr1 "./ExtraPretenders/DeepTerror.tga"
        #spr2 "./ExtraPretenders/DeepTerror2.tga"
        #descr "This huge creature was born at the dawn of time, when monsters and giants roamed the world. Since ancient times is has slumbered dreamlessly in the deepest gorges of the ocean where even the faintest sunlight does not reach. Now awoken, it is worshipped by the lesser beings of the ocean. The Terror of the Deep is amphibian and can leave its watery home when required. It rarely communicates with its followers, however if displeased it opens its enormous mouth and swallows the unfortunate whole. Now it is out to become the true God to extend its rule above the waves. In combat the creature will manifest magical Water and Death gems to aid in spellcasting."
        #humanoid
        #gcost 250
        #size 6
        #hp 166
        #prot 18
        #mr 20
        #mor 30
        #str 30
        #att 14
        #def 12
        #prec 8
        #enc 2
        #mapmove 3
        #ap 12
        #magicskill 2 1
        #magicskill 5 2
        #weapon 461 -- Swallow
        #weapon 29 -- Claw
        #fear 10
        #heal
        #diseaseres 100
        #amphibian
        #darkvision 100
        #poisonres 10
        #goodleader
        #spiritsight
        #digest 2
        #maxage 3000
        #startage 1000
        #pathcost 60
        #startdom 3
        #homerealm 9 -- Deeps
        #tmpwatergems 1
        #tmpdeathgems 2
        #end

        #newmonster 4977
        #name "Prince of the Deeps"
        #spr1 "./ExtraPretenders/ElementalPrince.tga"
        #spr2 "./ExtraPretenders/ElementalPrince2.tga"
        #descr "The Prince of the Deeps is an Elemental Prince of water that has taken the role of a Pretender God. Dissatisfied with ruling over the deepest waters he has decided to claim the Throne of Heaven and rule over all the waters of the world. The Prince of the Deeps is composed entirely of water and is very difficult to harm. He can regenerate wounds and will eventually heal any injury over time. The Prince of the Deeps is skilled in water magic and can bring warriors under the water. In combat he will manifest magical Water gems to aid in spellcasting."
        #djinn
        #gcost 200
        #size 6
        #hp 85
        #prot 0
        #mr 20
        #mor 30
        #str 22
        #att 14
        #def 14
        #prec 12
        #enc 0
        #mapmove 2
        #ap 16
        #magicskill 2 3
        #gemprod 2 1
        #darkvision 100
        #spiritsight
        #batstartsum2 411 -- Size 3 Water Elemental
        #weapon 90 -- Crush
        #weapon 90 -- Crush
        #expertleader
        #heal
        #diseaseres 100
        #regeneration 20
        #poisonres 15
        #giftofwater 500
        #slashres
        #bluntres
        #pierceres
        #aquatic
        #neednoteat
        #woundfend 99
        #maxage 3000
        #startage 1000
        #itemslots 13446 -- No feet
        #pathcost 60
        #startdom 3
        #homerealm 9 -- Deeps
        #tmpwatergems 2
        #end

        #newmonster 4978
        #name "Danavaraja"
        #descr "The Danavaraja is the lord of the Danavas, demon titans of ancient times who after the great wars with the Devatas of Kailasa were banished to the Nether Realms. Now free from his prison he wishes to claim this world as his and become a demon-god. The Danavaraja is surrounded by a blasphemous aura which is anathema to divine beings. He is terrifying to behold and can call forth Rakshasa Warriors to serve him. In combat the creature will manifest a magical Fire gem to aid in spellcasting, and each month it will select from amongst its followers those of pure blood to serve as Blood Slaves."
        #spr1 "./ExtraPretenders/Danavaraja.tga"
        #spr2 "./ExtraPretenders/Danavaraja.tga"
        #humanoid
        #gcost 250
        #hp 100
        #size 6
        #prot 10
        #mor 30
        #mr 20
        #enc 2
        #str 25
        #att 14
        #def 14
        #prec 12
        #mapmove 3
        #ap 17
        #fireres -5
        #fear 5
        #ambidextrous 8
        #chaospower 2
        #magicskill 1 1
        #magicskill 7 2
        #demon
        #neednoteat
        #heal
        #spiritsight
        #diseaseres 100
        #itemslots 7326
        #weapon "Unholy sword"
        #weapon "Unholy spear"
        #weapon "Unholy axe"
        #armor 50 -- Weightless Scale
        #armor "Shield"
        #haltheretic 9
        #superiorleader
        #expertundeadleader
        #makemonsters1 1737 -- Rakshasa Warrior
        #nametype 129
        #startdom 3
        #pathcost 60
        #tmpairgems 1
        #gemprod 7 2
        #end

        #newmonster 4979
        #spr1 "./ExtraPretenders/Firstbornrhuax.tga"
        #spr2 "./ExtraPretenders/Firstbornrhuax2.tga"
        #name "Firstborn of Rhuax"
        #descr "The Firstborn of Rhuax is the first and most powerful of the Lavaborn and was spawned at the dawn of time by the King of Magma himself.
        He shares much of his father's power and appearance, manifesting as a being of molten rock constantly surrounded by heat and flames. He can summon beings of magma to serve him and aid in battle, but yearns for the worship of the people of the world and has decided to claim Godhood. In combat the creature will manifest magical Fire and Earth gems to aid in spellcasting"
        #djinn
        #gcost 250
        #hp 80
        #size 5
        #prot 9
        #mr 20
        #mor 30
        #str 22
        #att 13
        #def 11
        #prec 12
        #enc 0
        #magicskill 0 3
        #magicskill 3 1
        #expertleader
        #heal
        #spiritsight
        #diseaseres 100
        #ap 12
        #mapmove 2
        #fireres 25
        #heat 7
        #fireshield 8
        #darkvision 100
        #firepower 1
        #wastesurvival
        #neednoteat
        #batstartsum2 640 -- Magma Child
        #makemonsters1 640 -- Magma Child
        #maxage 1000
        #weapon "Flame strike"
        #itemslots 13446
        #poisonres 10
        #nametype 101
        #startdom 3
        #pathcost 80
        #tmpfiregems 3
        #tmpearthgems 1
        #end

        #newmonster 4980
        #spr1 "./ExtraPretenders/Colossus.tga"
        #spr2 "./ExtraPretenders/Colossus2.tga"
        #name "Blacksteel Colossus"
        #descr "The Blacksteel Colossus is the masterpiece of Ulm's mage-smiths, a gigantic nearly-invulnerable construct crafted using only the finest alloys. The people of Ulm worshipped it as a symbol of the power of human ingenuity and made it their own god and idol. In time it gained a semblance of will and began to move under its own power. The Colossus is mindless and mostly does what its worshipers believe it would do. It punishes harshly and rewards sparingly. The Colossus is extremely hard to destroy and, while slow, can tirelessly crush lesser foes with its great fists."
        #humanoid
        #gcost 300
        #minprison 1
        #hp 125
        #size 6
        #str 28
        #att 7
        #def 7
        #prec 7
        #prot 27
        #fireres 15
        #coldres 15
        #shockres 15
        #poisonres 25
        #siegebonus 50
        #ap 8
        #poorleader
        #bonusspells 1
        #neednoteat
        #spiritsight
        #inanimate
        #mr 20
        #mor 50
        #maxage 5000
        #enc 0
        #mapmove 16
        #pathcost 80
        #startdom 4
        #magicskill 0 1
        #magicskill 3 2
        #weapon "Shatterfist"
        #weapon "Shatterfist"
        #fixedname "Schwarzer Riese"
        #end

        #newmonster 4981
        #name "Neter of the River"
        #descr "The Neter is a giant of divine heritage. Fearsome and more beastly than his brethren, he was made guardian of the warm rivers and marshes of the world by the Pantokrator, but when he joined the rebellion of the lesser gods he was banished. Now with the Pantokrator gone the Neter of the River has returned to claim the world as his own. Crocodiles will crawl from nearby rivers and lakes to serve the Neter of the River, and occasionally a great Sacred Crocodile will lumber into the temple grounds to accept offers of meat and sacrifices. In combat the Neter will manifest magical Water and Nature gems to aid in spellcasting."
        #spr1 "./ExtraPretenders/Neterriver.tga"
        #spr2 "./ExtraPretenders/Neterriver2.tga"
        #humanoid
        #gcost 240
        #hp 110
        #size 6
        #prot 18
        #mr 20
        #mor 30
        #att 13
        #def 11
        #str 25
        #prec 12
        #enc 2
        #mapmove 3
        #poisonres 10
        #ap 14
        #fear 5
        #weapon "Bite"
        #weapon 29 -- Claw
        #weapon "Tail sweep"
        #domsummon2 2185 -- Crocodile
        #domsummon20 2186 -- Sacred Crocodile
        #pathcost 60
        #startdom 3
        #homerealm 7 -- Africa
        #coldblood
        #pooramphibian
        #swampsurvival
        #expertleader
        #heal
        #diseaseres 100
        #magicskill 2 1
        #magicskill 6 2
        #fixedname "Suchos"
        #tmpwatergems 1
        #tmpnaturegems 2
        #end

        #selectmonster 956
        #spr1 "./ExtraPretenders/Mothertuathas.tga"
        #spr2 "./ExtraPretenders/Mothertuathas2.tga"
        #clearweapons
        #weapon "Golden spear"
        #cleararmor
        #armor "Golden shield"
        #descr "The Mother of Tuathas is a demigoddess of immense size. 
        She is the mother of the Tuatha De Danann who once ruled the lands of Man. She is skilled in Air and Nature magic and is a master of illusion, capable of hiding her true identity. She is attended by an honour guard of Tuatha warriors and more can be called from their ancestral homes to serve her. Long ago the Mother of Tuathas retreated from the world to the fey realm. Now she has returned from the Land of the Ever Young to claim the mantle of God and lead her people to victory. In combat she will manifest magical Air and Nature gems to aid in spellcasting."
        #humanoid
        #gcost 200
        #forestsurvival
        #heal
        #att 14
        #mr 20
        #diseaseres 100
        #darkvision 50
        #ironvul 2
        #batstartsum1d6 1753 -- Tuatha Warrior.
        #makemonsters3 1753 -- Tuatha Warriors
        #fixedname "Danu"
        #tmpairgems 2
        #tmpnaturegems 1
        #end

        #newmonster 4982
        #name "Titan of the Sun"
        #spr1 "./ExtraPretenders/Titansun.tga"
        #spr2 "./ExtraPretenders/Titansun2.tga"
        #descr "The Titan of the Sun is a giant of divine heritage. The Pantokrator tasked him with driving the chariot of the Sun across the sky each day, but his splendor inspired mortals to worship and the Pantokrator eventually grew jealous and banished him. Now that the Pantokrator is gone the Titan of the Sun will shine again. His brilliance is so great that enemies will falter and any who attempt to strike him may be struck permanently blind. In combat he can call down the rays of the sun to burn his enemies, and will manifest magical gems of Fire and Astral magic to aid in spellcasting. He is joined by the Cattle of the Sun, magnificent curved-horned beasts that will trample his enemies."
        #humanoid
        #gcost 210
        #hp 90
        #size 6
        #awe 3
        #eyeloss
        #str 24
        #prot 5
        #mr 20
        #mor 30
        #att 12
        #def 12
        #enc 2
        #expertleader
        #heal
        #diseaseres 100
        #prec 12
        #mapmove 3
        #ap 16
        #pathcost 60
        #homerealm 3 -- Mediterranean
        #startdom 3
        #magicskill 0 2
        #magicskill 4 1
        #batstartsum2 3177 -- Red Cattle
        #weapon 1855 -- Burning Ray
        #weapon "Fist"
        #armor "Crown"
        #fireres 15
        #fixedname "Helios"
        #tmpfiregems 2
        #tmpastralgems 1
        #end

        #newmonster 4983
        #spr1 "./ExtraPretenders/Titanwarfarming.tga"
        #spr2 "./ExtraPretenders/Titanwarfarming2.tga"
        #name "Titan of War and Farming"
        #descr "The Titan of War and Farming is a giant of divine heritage. He was once worshipped only as a patron of agriculture who made fields fertile and defended farmers from bandits and wild beasts, but as his worshippers grew in influence and ambition he also became a god of noble warfare, conquest and bravery. The Pantokrator grew fearful of his growing power and popularity and banished him for eternity. Now the Pantokrator is gone and the Titan will soon lead his nation to glory and rightful dominion over the world. As a rustic protector and benevolent conqueror the Titan of War and Farming inspires the local population to defend their homes and fields, and even the recently conquered will take up arms to fight against their former compatriots. He is highly resilient and will suffer permanent injuries less often than most creatures. He will also manifest magical gems of Fire, Earth and Nature magic to aid in spellcasting."
        #humanoid
        #gcost 210
        #hp 100
        #size 6
        #prot 5
        #mr 20
        #mor 30
        #str 26
        #att 15
        #def 15
        #prec 12
        #enc 2
        #superiorleader
        #heal
        #diseaseres 100
        #ap 16
        #mapmove 3
        #woundfend 1
        #magicskill 0 1
        #magicskill 3 1
        #magicskill 6 1
        #weapon 201 -- Magic Spear
        #armor 200 -- Champions Cuirass
        #armor "Hoplite helmet"
        #armor "Shield"
        #incprovdef 2
        #pathcost 60
        #homerealm 3 -- Mediterranean
        #startdom 3
        #fixedname "Mamers"
        #tmpfiregems 1
        #tmpearthgems 1
        #tmpnaturegems 1
        #end

        -- #newmonster 4984
        -- #spr1 "./ExtraPretenders/Mistresshunt.tga"
        -- #spr2 "./ExtraPretenders/Mistresshunt2.tga"
        -- #name "Mistress of the Hunt and the Moon"
        -- #descr "The Mistress of the Hunt and the Moon a giantess of divine heritage sprung from the first rays of moonlight. She is worshipped as a goddess of hunters and beasts and claims dominion over the wild forests and mountains and the moon shining in the heavens. It is said that her bow never misses its target. In combat she is joined by a pack of baying hunting hounds that will attack her enemies."
        -- #humanoid
        -- #gcost 10000
        -- #female
        -- #hp 85
        -- #size 6
        -- #prot 3
        -- #mr 18
        -- #mor 30
        -- #str 20
        -- #att 13
        -- #def 13
        -- #prec 14
        -- #enc 2
        -- #expertleader
        -- #ap 18
        -- #mapmove 3
        -- #forestsurvival
        -- #mountainsurvival
        -- #weapon "Enchanted Spear"
        -- #weapon 152 -- Longbow of Accuracy
        -- #armor "Leather cuirass"
        -- #nametype 108
        -- #homerealm 3 -- Mediterranean
        -- #startdom 3
        -- #magicskill 4 1
        -- #magicskill 6 2
        -- #stealthy 10
        -- #darkvision 50
        -- #animalawe 3
        -- #beastmaster 3
        -- #pathcost 60
        -- #fixedname "Artemis"
        -- #batstartsum1d6 3083 -- Hounds
        -- #end

        #newmonster 4985
        #name "Tetrachire"
        #spr1 "./ExtraPretenders/Tetrachire.tga"
        #spr2 "./ExtraPretenders/Tetrachire2.tga"
        #descr "The Tetrachire is a four-armed, two-headed giant born from the Great Mother and the most fearsome of her offspring. His rage and ferocity knew no equal among the titans and they feared him so much that the Pantokrator made him his personal guardian, until even he grew too afraid of his terrible wrath and decided to banish him. The Tetrachire is a primordial being attuned to the raw power of Earth, but with a savage and chaotic nature. He refuses to be constrained in combat, however he is highly resilient and will suffer permanent injuries less often than most creatures."
        #humanoid
        #gcost 250
        #size 6
        #hp 120
        #fear 5
        #str 28
        #prot 8
        #mr 20
        #mor 30
        #att 13
        #def 11
        #enc 2
        #expertleader
        #heal
        #diseaseres 100
        #prec 8
        #mapmove 3
        #ap 16
        #magicskill 3 2
        #weapon "Fist"
        #weapon "Fist"
        #weapon "Fist"
        #weapon "Fist"
        #berserk 7
        #woundfend 1
        #homerealm 3 -- Mediterranean
        #fixedname "Briareos"
        #itemslots 6558
        #chaospower 1
        #incunrest 50
        #ambidextrous 4
        #startdom 2
        #pathcost 80
        #homerealm 3 -- mediterranean
        #end

        #newmonster 5041
        #copyspr 775 -- Tartarian Titan
        #name "Tartarian"
        #descr "The Tartarian is an ancient Pretender god condemned to eternal imprisonment by the Pantokrator in ages past. Now, with the Pantokrator gone it has escaped its imprisonment in Tartarus and will finally claim the Throne of Heaven. The titan has tremendous power, but is somewhat insane after suffering aeons of perpetual pain. The body of the ancient god is dead, but its willpower keeps it alive in a state of constant decay. In combat it can manifest magical death and astral gems to aid in spellcasting."
        #humanoid
        #gcost 210
        #size 6
        #hp 150
        #prot 8
        #mr 20
        #mor 30
        #str 22
        #att 12
        #def 12
        #prec 12
        #enc 0
        #mapmove 3
        #ap 16
        #magicskill 5 2
        #magicskill 4 1
        #weapon 299 -- Enchanted Sickle
        #undead
        #neednoteat
        #coldres 15
        #poisonres 25
        #fear 10
        #shatteredsoul 5
        #pierceres
        #goodleader
        #goodundeadleader
        #heal
        #spiritsight
        #female
        #diseaseres 100
        #maxage 3000
        #startage 100
        #pathcost 60
        #startdom 3
        #tmpdeathgems 2
        #tmpastralgems 1
        #end

        #newmonster 5048
        #spr1 "./ExtraPretenders/OnyxStatue.tga"
        #spr2 "./ExtraPretenders/OnyxStatue2.tga"
        #name "Onyx Statue"
        #descr "When the Earth Made Flesh was imprisoned for eternity her children mourned her loss. The Pale Ones decided to create a great statue in her honour, crafted from onyx in memory of her sacrifice. The statue was adorned with gold and placed in the Chamber of the Seal to guard over the great evil contained within. Over centuries the Pale Ones came to worship the statue as if it was the Earth Made Flesh herself and eventually it began to move. Some Oracles claim the statue is inhabited by the spirit of the banished progenitor, others say it moves only by the will of the Pale Ones. All agree it speaks with the voice of God. The Onyx Statue is made of stone and would be difficult to destroy in combat. In combat it can manifest magical Water and Earth gems to aid in spellcasting."
        #humanoid
        #gcost 260
        #size 5
        #hp 130
        #prot 24
        #mr 20
        #mor 50
        #str 24
        #att 12
        #def 10
        #prec 12
        #enc 0
        #mapmove 3
        #ap 8
        #magicskill 2 1
        #magicskill 3 2
        #weapon 562 -- Stone Fist
        #stonebeing
        #neednoteat
        #noheal
        #magicbeing
        #inanimate
        #slashres
        #pierceres
        #pooramphibian
        #spiritsight
        #heal
        #female
        #diseaseres 100
        #darkvision 100
        #poisonres 25
        #goodleader
        #goodmagicleader
        #maxage 3000
        #startage 1000
        #pathcost 60
        #startdom 3
        #tmpwatergems 1
        #tmpearthgems 2
        #end

        #newmonster 5049
        #copystats 1490
        #spr1 "./ExtraPretenders/Antumbral.tga"
        #spr2 "./ExtraPretenders/Antumbral2.tga"
        #name "Antumbral"
        #descr "The Antumbral is a powerful shadow being that was once the soul of a great Agarthan mage. In ages past it sacrificed itself to seal the Chamber, however now it has returned to this realm as the seal weakens to lead its brethren. It seeks to attain the role of Pantokrator from beyond the grave to enact revenge on the surface races. The Antumbral is served by Umbrals that will appear when his Dominion grows strong. Umbrals are shadow beings resembling ancient Pale Ones with elongated faces and drooling mouths. They are ethereal, undead beings capable of draining life from the living. In combat the Antumbral can manifest magical Earth and Death gems to aid in spellcasting."
        #humanoid
        #gcost 220
        #size 6
        #hp 110
        #prot 0
        #mr 20
        #mor 30
        #str 24
        #att 13
        #def 13
        #prec 12
        #enc 0
        #mapmove 3
        #ap 16
        #magicskill 3 1
        #magicskill 5 2
        #undead
        #ethereal
        #neednoteat
        #amphibian
        #spiritsight
        #heal
        #diseaseres 100
        #coldres 15
        #poisonres 25
        #stealthy 0
        #domsummon20 2497 -- Penumbral
        #raredomsummon 1490 -- Umbral
        #makemonsters1 1490 -- Umbral
        #goodleader
        #goodundeadleader
        #maxage 3000
        #startage 1000
        #pathcost 60
        #startdom 3
        #tmpearthgems 1
        #tmpdeathgems 2
        #end

        #newmonster 5052
        #spr1 "./ExtraPretenders/GodEmp.tga"
        #spr2 "./ExtraPretenders/GodEmp2.tga"
        #name "Deified Emperor"
        #descr "In Pythium each Emperor is deified upon death, gaining Divine authority upon transcending the mortal sphere. This God-Emperor has returned to the land of the living to to lead his people and attain the role of Pantokrator. To this end he has manifested a Divine body with a stature to match his authority. The Emperor has gained in magical knowledge since achieving Divine status. The presence of the God-Emperor will calm the populace and reduce unrest in the province. In combat he can manifest gems of Air, Water and Astral magic to aid in spellcasting. The Emperor is always accompanied by a loyal bodyguard of Emerald Guard."
        #fixedname "Claudius"
        #humanoid
        #gcost 230
        #hp 95
        #size 5
        #prot 4
        #mr 20
        #mor 30
        #str 23
        #att 12
        #def 12
        #prec 12
        #enc 2
        #mapmove 3
        #ap 16
        #magicskill 1 1
        #magicskill 2 1
        #magicskill 4 2
        #weapon 92 -- Fist
        #startage 1000
        #maxage 2000
        #shockres 5
        #inspirational 1
        #expertleader
        #heal
        #twiceborn 5767 -- Wight Giant
        #batstartsum2d6 7 -- Emerald Guard
        #diseaseres 100
        #goodmagicleader
        #awe 1
        #incunrest -50
        #pathcost 60
        #startdom 3
        #tmpairgems 1
        #tmpwatergems 1
        #tmpastralgems 2
        #end

        #newmonster 5053
        #copystats 931 -- Ivy King
        #clearmagic
        #spr1 "./ExtraPretenders/GreenMan.tga"
        #spr2 "./ExtraPretenders/GreenMan2.tga"
        #name "Green Man"
        #descr "The Green Man was an ancient spirit of nature that once ruled over the Ivy Kings and their subjects. He appears as a large figure formed from plants with a great beard and crown of flowers. Now he has reawoken and will reaffirm the Ivy Kingdoms by attaining the role of Pantokrator. The Ivy Kings lived long before the coming of man, but fell into a deep slumber ages ago. The vine men followed their Kings to sleep and were all but forgotten. Vine beings will reawaken to serve the Green Man as his Dominion grows strong. He is skilled in Nature magic and can summon more vine men and vine ogres with each casting. In combat he can manifest gems of Nature magic to aid in spellcasting."
        #fixedname "Viridios"
        #gcost 180
        #size 6
        #hp 105
        #mr 20
        #mor 30
        #str 25
        #mapmove 3
        #ap 16
        #expertleader
        #heal
        #diseaseres 100
        #goodmagicleader
        #magicskill 6 3
        #ivylord 5
        #domsummon 361 -- Vine Man
        #domsummon2 362 -- Vine Ogre
        #pathcost 60
        #startdom 3
        #homerealm 2 -- Celtic
        #tmpnaturegems 3
        #end

        #newmonster 5054
        #spr1 "./ExtraPretenders/AngelLord.tga"
        #spr2 "./ExtraPretenders/AngelLord2.tga"
        #name "Angelic Lord"
        #descr "The Angelic Lord is a powerful being from the Celestial Sphere and was the first of the Heavenly Host. Once in the service of the Pantokrator, he was imprisoned after his master became fearful of his power and divine presence. Now the shackles are weakening, and he has chosen to claim this world as his and have heavenly hosts proclaim his glory. The Angelic Lord is surrounded by a divine splendor that must be shielded lest it strike the world with unbearable might. Anyone striking Him will in turn be struck by awe and heavenly fire. In combat he can manifest gems of Fire, Air and Astral magic to aid in spellcasting."
        #fixedname "Metatron"
        #humanoid
        #gcost 260
        #hp 85
        #size 6
        #prot 0
        #mor 30
        #str 21
        #att 13
        #def 13
        #mr 20
        #prec 13
        #enc 2
        #mapmove 24
        #ap 10
        #mapmove 26
        #fireshield 8
        #awe 3
        #invulnerable 15
        #heal
        #diseaseres 100
        #weapon 95 -- Flambeau
        #magicskill 0 1
        #magicskill 1 1
        #magicskill 4 1
        #startage 1000
        #fireres 5
        #shockres 5
        #maxage 3000
        #pathcost 80
        #startdom 3
        #tmpairgems 1
        #tmpfiregems 1
        #tmpastralgems 1
        #startage 1000
        #maxage 2000
        #fireshield 8
        #flying
        #neednoteat
        #spiritsight
        #magicbeing
        #expertleader
        #goodmagicleader
        #homerealm 0
        #end

        #newmonster 5060
        #copyspr 2066
        #name "Grigori"
        #descr "At the dawn of time, on the sacred mount Hermon, descended six angels in full celestial splendor to the natives of the holy land to aid and instruct them in righteousness and wisdom. The Avvim were a people strong and beautiful of mind and body. They were adept students and their culture flourished under the tutelage of the Watchers. But before long the Grigori became enamored with the Avvim, shared their burdens and taught them things that should not be taught. Tempted by the beauty of the Avvim, they took their daughters as wives and sinned before the Celestial Powers. Their offspring were the Nephilim, giants of great power, abominable to the world. When the Celestial Powers lashed out in fury, the Grigori were banished to the infernal realms. Now, with the Pantokrator gone one of the Grigori can feel his shackles weakening and will return to the world to claim the mantle of Godhood."
        #fixedname "Amazarak"
        #humanoid
        #gcost 260
        #hp 66
        #size 6
        #prot 0
        #mr 20
        #mor 30
        #str 21
        #att 13
        #def 13
        #prec 13
        #enc 2
        #mapmove 24
        #ap 10
        #magicskill 3 1
        #magicskill 4 1
        #magicskill 7 1
        #weapon 92 -- Fist
        #startage 1000
        #maxage 2000
        #shockres 10
        #fireres 10
        #fear 10
        #invulnerable 20
        #damagerev 1
        #nobadevents 20
        #flying
        #neednoteat
        #demon
        #heal
        #spiritsight
        #diseaseres 100
        #magicbeing
        #expertleader
        #goodmagicleader
        #pathcost 60
        #startdom 3
        #homerealm 0
        #tmpearthgems 1
        #tmpastralgems 1
        #gemprod 7 1
        #minprison 1
        #end

        #newmonster 5065
        #spr1 "./ExtraPretenders/BullDemonKing.tga"
        #spr2 "./ExtraPretenders/BullDemonKing2.tga"
        #name "Bull Demon King"
        #descr "The Bull Demon King is a Lord of the Underworld that can take the form of a gigantic white Bull. In ages past he terrorised the people of the world until the Pantokrator bid the Celestial General lead his armies to subdue him and trap him in the Underworld. The Bull Demon King is powerful but arrogant, and is the most senior of the Demon Kings. He likes to think of himself as equal to the great sages of heaven and will become furious if his wisdom is questioned. Now, with the Pantrokrator gone the Bull Demon King will once more rule the realm of men. In Bull form he is physically powerful but it is not well suited for spellcasting."
        #fixedname "Gyūmaō"
        #humanoid
        #gcost 260
        #hp 76
        #size 6
        #prot 4
        #mr 20
        #mor 30
        #str 21
        #att 13
        #def 11
        #prec 14
        #enc 2
        #mapmove 14
        #ap 14
        #magicskill 3 1
        #magicskill 6 1
        #magicskill 7 1
        #weapon 379 -- No Dachi
        #armor 1 -- Buckler
        #armor 7 -- Scale Mail Cuirass
        #researchbonus -15
        #startage 1000
        #maxage 2000
        #fireres 10
        #demon
        #heal
        #spiritsight
        #berserk 5
        #diseaseres 100
        #expertleader
        #okundeadleader
        #pathcost 60
        #startdom 3
        #homerealm 4
        #shapechange 5066
        #end

        #newmonster 5066
        #copyspr 979 -- Great White Bull
        #name "Bull Demon King"
        #descr "The Bull Demon King is a Lord of the Underworld that can take the form of a gigantic white Bull. In ages past he terrorised the people of the world until the Pantokrator bid the Celestial General lead his armies to subdue him and trap him in the Underworld. The Bull Demon King is powerful but arrogant, and is the most senior of the Demon Kings. He likes to think of himself as equal to the great sages of heaven and will become furious if his wisdom is questioned. Now, with the Pantrokrator gone the Bull Demon King will once more rule the realm of men. In Bull form he is physically powerful but it is not well suited for spellcasting."
        #fixedname "Gyūmaō"
        #quadruped
        #gcost 270
        #hp 180
        #size 6
        #prot 12
        #mr 20
        #mor 30
        #str 30
        #att 13
        #def 11
        #prec 8
        #enc 2
        #mapmove 16
        #ap 14
        #startage 1000
        #maxage 2000
        #fireres 10
        #magicskill 3 1
        #magicskill 6 1
        #magicskill 7 1
        #weapon 55 -- Hoof
        #weapon 331 -- Gore
        #trample
        #demon
        #heal
        #spiritsight
        #magicboost 53 -1
        #berserk 5
        #itemslots 12416 -- Head + 2 misc
        #diseaseres 100
        #expertleader
        #okundeadleader
        #pathcost 60
        #startdom 3
        #homerealm 0
        #shapechange 5065
        #end

        #newmonster 5067
        #copystats 1368 -- Seraph
        #clearmagic
        #clearweapons
        #spr1 "./ExtraPretenders/PeacockAngel.tga"
        #spr2 "./ExtraPretenders/PeacockAngel2.tga"
        #name "Peacock Angel"
        #descr "At the beginning of time the Pantokrator created seven angelic beings from his own illumination. Foremost was the Peacock Angel, a being of light itself. Upon creating the first man from the dust of the earth the Pantokrator decreed all the angels must bow before it, however the Peacock Angel refused. He reasoned a being of light should not bow before a creature of the dust. At this the other angels cried out and called him a Devil and a Heretic. The Pantokrator however praised him for seeing the truth and bade him sit at the side of the Throne of Heaven. Now, with the Pantokrator gone the Peacock Angel will take his place and bestow blessings and misfortune as he sees fit. Anyone striking at his glory will in turn be struck by awe and blindness. In combat he can manifest gems of Water, Air and Nature magic to aid in spellcasting."
        #fixedname "Melek Taus"
        #humanoid
        #gcost 240
        #hp 67
        #size 6
        #prot 0
        #mor 30
        #str 20
        #att 14
        #def 13
        #mr 20
        #prec 13
        #enc 2
        #mapmove 24
        #ap 10
        #awe 5
        #invulnerable 15
        #heal
        #diseaseres 100
        #weapon 92 -- Fist
        #magicskill 2 1
        #magicskill 1 1
        #magicskill 6 1
        #startage 1000
        #poisonres 0
        #shockres 0
        #fireres 0
        #fireshield 0
        #maxage 3000
        #pathcost 80
        #startdom 3
        #unsurr 5
        #tmpairgems 1
        #tmpwatergems 1
        #tmpnaturegems 1
        #flying
        #neednoteat
        #spiritsight
        #magicbeing
        #expertleader
        #goodmagicleader
        #homerealm 0
        #end

        #newmonster 5079
        #spr1 "./ExtraPretenders/SunFather.tga"
        #spr2 "./ExtraPretenders/SunFather.tga"
        #name "Sun Father"
        #descr "In ages past the Pantokrator begat a son, and he became the Lord of the Sun. Each morning he would ride his chariot across the sky, dragging the sun on its eternal journey. In time he had two daughters, the Morning Star and Evening Star, who would open and close the gates for his great chariot to aid him in his task. As the people of the world saw the life giving rays of the sun they began to worship it above the Pantokrator and he grew jealous of his child. He imprisoned the Sun Father along with his two offspring in a pit from which no light could escape and gave the task of driving the Sun Chariot to another. Now, with the Pantokrator departed, the Sun Father has returned along with his daughters to claim his birthright and ascend the Throne of Heaven. The Sun Father is skilled in all the magic of the skies, whilst his two daughters are skilled in the magic of the dawn and the night sky. His Daughters will lose some magical power whilst away from their Father, however he can call them to him in times of need with but a word."
        #humanoid
        #fixedname "Dazbog"
        #gcost 300
        #hp 95
        #size 6
        #prot 4
        #mor 30
        #str 20
        #att 14
        #def 13
        #mr 20
        #prec 13
        #enc 2
        #mapmove 18
        #ap 18
        #awe 1
        #heal
        #diseaseres 100
        #weapon 172 -- Magic Sceptre
        #weapon 383 -- Throw Flames
        #magicskill 0 1
        #magicskill 1 1
        #magicskill 4 1
        #startage 1000
        #fireres 15
        #maxage 3000
        #tmpfiregems 1
        #tmpairgems 1
        #tmpastralgems 1
        #spiritsight
        #expertleader
        #goodmagicleader
        #triplegod 1
        #triple3mon
        #unify
        #pathcost 80
        #startdom 3
        #homerealm 1
        #end

        #newmonster 5080
        #spr1 "./ExtraPretenders/Morning_Star.tga"
        #spr2 "./ExtraPretenders/Morning_Star2.tga"
        #name "Morning Star"
        #descr "In ages past the Pantokrator begat a son, and he became the Lord of the Sun. Each morning he would ride his chariot across the sky, dragging the sun on its eternal journey. In time he had two daughters, the Morning Star and Evening Star, who would open and close the gates for his great chariot to aid him in his task. As the people of the world saw the life giving rays of the sun they began to worship it above the Pantokrator and he grew jealous of his child. He imprisoned the Sun Father along with his two offspring in a pit from which no light could escape and gave the task of driving the Sun Chariot to another. Now, with the Pantokrator departed, the Sun Father has returned along with his daughters to claim his birthright and ascend the Throne of Heaven. The Sun Father is skilled in all the magic of the skies, whilst his two daughters are skilled in the magic of the dawn and the night sky. His Daughters will lose some magical power whilst away from their Father, however he can call them to him in times of need with but a word."
        #humanoid
        #fixedname "Zorya Utrennyaya"
        #gcost 300
        #hp 25
        #size 3
        #prot 0
        #mor 30
        #str 12
        #att 14
        #def 14
        #mr 18
        #prec 12
        #enc 2
        #mapmove 34
        #ap 12
        #awe 5
        #invulnerable 15
        #heal
        #diseaseres 100
        #weapon 275 -- Sun Sword
        #magicskill 0 1
        #magicskill 1 1
        #magicskill 4 1
        #magicboost 0 -2
        #magicboost 2 -10
        #magicboost 3 -10
        #magicboost 4 -10
        #magicboost 5 -10
        #magicboost 6 -10
        #magicboost 7 -10
        #researchbonus -10
        #startage 500
        #fireres 5
        #shockres 5
        #maxage 3000
        #spreaddom 1
        #flying
        #magicbeing
        #spiritsight
        #goodleader
        #okmagicleader
        #female
        #triplegod 1
        #triplegodmag 1
        #pathcost 80
        #startdom 3
        #homerealm 0
        #end

        #newmonster 5081
        #spr1 "./ExtraPretenders/EveningStar.tga"
        #spr2 "./ExtraPretenders/EveningStar2.tga"
        #name "Evening Star"
        #descr "In ages past the Pantokrator begat a son, and he became the Lord of the Sun. Each morning he would ride his chariot across the sky, dragging the sun on its eternal journey. In time he had two daughters, the Morning Star and Evening Star, who would open and close the gates for his great chariot to aid him in his task. As the people of the world saw the life giving rays of the sun they began to worship it above the Pantokrator and he grew jealous of his child. He imprisoned the Sun Father along with his two offspring in a pit from which no light could escape and gave the task of driving the Sun Chariot to another. Now, with the Pantokrator departed, the Sun Father has returned along with his daughters to claim his birthright and ascend the Throne of Heaven. The Sun Father is skilled in all the magic of the skies, whilst his two daughters are skilled in the magic of the dawn and the night sky. His Daughters will lose some magical power whilst away from their Father, however he can call them to him in times of need with but a word."
        #humanoid
        #fixedname "Zorya Vechernyaya"
        #gcost 300
        #hp 25
        #size 3
        #prot 0
        #mor 30
        #str 12
        #att 14
        #def 14
        #mr 18
        #prec 12
        #enc 2
        #mapmove 34
        #ap 12
        #awe 5
        #invulnerable 15
        #heal
        #diseaseres 100
        #weapon 170 -- Sword of Justice
        #magicskill 0 1
        #magicskill 1 1
        #magicskill 4 1
        #magicboost 0 -10
        #magicboost 1 -2
        #magicboost 2 -10
        #magicboost 3 -10
        #magicboost 5 -10
        #magicboost 6 -10
        #magicboost 7 -10
        #researchbonus -10
        #startage 500
        #fireres 5
        #shockres 5
        #maxage 3000
        #spreaddom 1
        #flying
        #magicbeing
        #spiritsight
        #goodleader
        #okmagicleader
        #female
        #triplegod 1
        #triplegodmag 1
        #pathcost 80
        #startdom 3
        #homerealm 0
        #end

        #newmonster 5082
        #spr1 "./ExtraPretenders/Lakshmi.tga"
        #spr2 "./ExtraPretenders/Lakshmi2.tga"
        #name "Tridevi of Prosperity"
        #descr "The Tridevi is a Divine manifestation of cosmic power appearing in three forms. The first, the Tridevi of Prosperity, brings abundance and good fortune to her followers. She is endowed with six auspicious and divine qualities and appears in a Lotus flower to signify her spiritual power. The second, the Tridevi of Wisdom, brings the gift of music, art and knowledge. She embodies the flowing of wisdom like a river and carries a Sitar with which she plays heavenly music. The last, the Tridevi of Divine Strength is a protector and destroyer of Evil. She has the strength and resilience of the mountains and bears a trishula enchanted to slay demons. Together the Tridevi have assumed the mantle of a Pretender God and will lead their people to victory in the battle for Ascension."
        #humanoid
        #fixedname "Lakshmi"
        #gcost 250
        #hp 78
        #size 6
        #prot 0
        #mor 30
        #str 20
        #att 13
        #def 12
        #mr 20
        #prec 14
        #enc 2
        #mapmove 18
        #ap 18
        #heal
        #bringeroffortune 20
        #ambidextrous 4
        #itemslots 7326 -- 4 Hands, 1 misc
        #diseaseres 100
        #weapon 92 -- Fist
        #armor 148 -- Crown
        #magicskill 0 1
        #magicskill 2 1
        #magicskill 3 1
        #startage 1000
        #maxage 3000
        #tmpfiregems 1
        #spiritsight
        #expertleader
        #goodmagicleader
        #female
        #triplegod 2
        #triple3mon
        #triplegodmag 0
        #pathcost 80
        #startdom 3
        #homerealm 8
        #end

        #newmonster 5083
        #spr1 "./ExtraPretenders/Parvati.tga"
        #spr2 "./ExtraPretenders/Parvati2.tga"
        #name "Tridevi of Divine Strength"
        #descr "The Tridevi is a Divine manifestation of cosmic power appearing in three forms. The first, the Tridevi of Prosperity, brings abundance and good fortune to her followers. She is endowed with six auspicious and divine qualities and appears in a Lotus flower to signify her spiritual power. The second, the Tridevi of Wisdom, brings the gift of music, art and knowledge. She embodies the flowing of wisdom like a river and carries a Sitar with which she plays heavenly music. The last, the Tridevi of Divine Strength is a protector and destroyer of Evil. She has the strength and resilience of the mountains and bears a trishula enchanted to slay demons. Together the Tridevi have assumed the mantle of a Pretender God and will lead their people to victory in the battle for Ascension."
        #humanoid
        #fixedname "Parvati"
        #gcost 250
        #hp 95
        #size 6
        #prot 0
        #mor 30
        #str 24
        #att 14
        #def 14
        #mr 20
        #prec 12
        #enc 2
        #mapmove 18
        #ap 18
        #fear 5
        #researchbonus -10
        #heal
        #ambidextrous 6
        #itemslots 7326 -- 4 Hands, 1 misc
        #diseaseres 100
        #weapon 467 -- Apotropaic Trident
        #weapon 716 -- Apotropaic Dagger
        #weapon 1840 -- Perfect Fist
        #armor 148 -- Crown
        #armor 98 -- Robe of the Sorceress
        #magicskill 0 1
        #magicskill 2 1
        #magicskill 3 1
        #triplegodmag 0
        #startage 1000
        #maxage 3000
        #tmpearthgems 1
        #spiritsight
        #superiorleader
        #goodmagicleader
        #female
        #triplegod 2
        #pathcost 80
        #startdom 3
        #end

        #newmonster 5084
        #spr1 "./ExtraPretenders/Saraswati.tga"
        #spr2 "./ExtraPretenders/Saraswati.tga"
        #name "Tridevi of Wisdom"
        #descr "The Tridevi is a Divine manifestation of cosmic power appearing in three forms. The first, the Tridevi of Prosperity, brings abundance and good fortune to her followers. She is endowed with six auspicious and divine qualities and appears in a Lotus flower to signify her spiritual power. The second, the Tridevi of Wisdom, brings the gift of music, art and knowledge. She embodies the flowing of wisdom like a river and carries a Sitar with which she plays heavenly music. The last, the Tridevi of Divine Strength is a protector and destroyer of Evil. She has the strength and resilience of the mountains and bears a trishula enchanted to slay demons. Together the Tridevi have assumed the mantle of a Pretender God and will lead their people to victory in the battle for Ascension."
        #humanoid
        #fixedname "Saraswati"
        #gcost 250
        #hp 68
        #size 6
        #prot 0
        #mor 30
        #str 18
        #att 11
        #def 11
        #mr 20
        #prec 14
        #enc 2
        #mapmove 18
        #ap 18
        #giftofwater 50
        #pooramphibian
        #researchbonus 10
        #heal
        #ambidextrous 2
        #itemslots 7326 -- 4 Hands, 1 misc
        #diseaseres 100
        #weapon 92 -- Fist
        #armor 148 -- Crown
        #magicskill 0 1
        #magicskill 2 1
        #magicskill 3 1
        #triplegodmag 0
        #startage 1000
        #maxage 3000
        #tmpwatergems 1
        #spiritsight
        #goodleader
        #goodmagicleader
        #female
        #triplegod 2
        #pathcost 80
        #startdom 3
        #end

        #newmonster 5085
        #spr1 "./bozmod/monsters/Shiva.tga"
        #spr2 "./bozmod/monsters/Shiva2.tga"
        #name "Trimurti of Destruction"
        #descr "The Trimurti is a Divine manifestation of the cosmic cycle appearing in three forms. The first, the Trimurti of Creation, has the power of growth and new life. He has four faces and sits eternally on a great lotus petal, causing living things around to grow and thrive. The second, the Trimurti of Protection is the preserver of life and maintainer of balance. He is blue-skinned and has great magical power. The last, the Trimurti of Destruction, is a bringer of death and renewal. He eternally dances the powerful and masculine dance of destruction and creation. Together the Trimurti represents the eternal cycle of birth, growth, death and rebirth. Now, with the Pantokrator gone they have the chance to sieze the Throne of Heaven and to remake the world in their image."
        #humanoid
        #fixedname "Shiva"
        #gcost 300
        #hp 95
        #size 6
        #prot 4
        #mor 30
        #str 24
        #att 14
        #def 14
        #mr 20
        #prec 12
        #enc 2
        #mapmove 18
        #ap 18
        #heal
        #fear 5
        #berserk 5
        #ambidextrous 6
        #itemslots 7326 -- 4 Hands, 1 misc
        #diseaseres 100
        #weapon 383 -- Throw Flames
        #weapon 1840 -- Perfect Fist
        #weapon 1840 -- Perfect Fist
        #weapon 391 -- Serpent
        #weapon 175 -- Chi Kick
        #armor 148 -- Crown
        #armor 192 -- Magic Furs
        #magicskill 4 1
        #magicskill 5 1
        #magicskill 6 1
        #tmpdeathgems 1
        #triplegodmag 0
        #startage 1000
        #maxage 3000
        #spiritsight
        #superiorleader
        #goodmagicleader
        #triplegod 5
        #triple3mon
        #pathcost 80
        #startdom 3
        #homerealm 8
        #end

        #newmonster 5086
        #spr1 "./ExtraPretenders/Vishnu.tga"
        #spr2 "./ExtraPretenders/Vishnu.tga"
        #name "Trimurti of Protection"
        #descr "The Trimurti is a Divine manifestation of the cosmic cycle appearing in three forms. The first, the Trimurti of Creation, has the power of growth and new life. He has four faces and sits eternally on a great lotus petal, causing living things around to grow and thrive. The second, the Trimurti of Protection is the preserver of life and maintainer of balance. He is blue-skinned and has great magical power. The last, the Trimurti of Destruction, is a bringer of death and renewal. He eternally dances the powerful and masculine dance of destruction and creation. Together the Trimurti represents the eternal cycle of birth, growth, death and rebirth. Now, with the Pantokrator gone they have the chance to sieze the Throne of Heaven and to remake the world in their image."
        #humanoid
        #fixedname "Vishnu"
        #gcost 300
        #hp 95
        #size 6
        #prot 0
        #mor 30
        #str 22
        #att 11
        #def 11
        #mr 20
        #prec 12
        #enc 2
        #mapmove 18
        #ap 18
        #heal
        #ambidextrous 4
        #itemslots 7326 -- 4 Hands, 1 misc
        #diseaseres 100
        #weapon 718 -- Apotropaic Mace
        #armor 148 -- Crown
        #magicskill 4 1
        #magicskill 5 1
        #magicskill 6 1
        #magicboost 53 1
        #tmpastralgems 1
        #triplegodmag 0
        #startage 1000
        #maxage 3000
        #spiritsight
        #superiorleader
        #goodmagicleader
        #triplegod 5
        #pathcost 80
        #startdom 3
        #end

        #newmonster 5087
        #spr1 "./ExtraPretenders/Brahma.tga"
        #spr2 "./ExtraPretenders/Brahma2.tga"
        #name "Trimurti of Creation"
        #descr "The Trimurti is a Divine manifestation of the cosmic cycle appearing in three forms. The first, the Trimurti of Creation, has the power of growth and new life. He has four faces and sits eternally on a great lotus petal, causing living things around to grow and thrive. The second, the Trimurti of Protection is the preserver of life and maintainer of balance. He is blue-skinned and has great magical power. The last, the Trimurti of Destruction, is a bringer of death and renewal. He eternally dances the powerful and masculine dance of destruction and creation. Together the Trimurti represents the eternal cycle of birth, growth, death and rebirth. Now, with the Pantokrator gone they have the chance to sieze the Throne of Heaven and to remake the world in their image."
        #humanoid
        #fixedname "Brahma"
        #gcost 300
        #hp 95
        #size 6
        #prot 0
        #mor 30
        #str 20
        #att 13
        #def 0
        #mr 20
        #prec 12
        #enc 2
        #mapmove 0
        #ap 2
        #immobile
        #heal
        #spreaddom 1
        #supplybonus 30
        #ambidextrous 2
        #itemslots 7326 -- 4 Hands, 1 misc
        #diseaseres 100
        #weapon 92 -- Fist
        #weapon 92 -- Fist
        #armor 148 -- Crown
        #magicskill 4 1
        #magicskill 5 1
        #magicskill 6 1
        #startage 1000
        #maxage 3000
        #spiritsight
        #goodleader
        #goodmagicleader
        #triplegod 5
        #triplegodmag 0
        #pathcost 80
        #startdom 3
        #end

        #newmonster 5088
        #spr1 "./ExtraPretenders/Triglav.tga"
        #spr2 "./ExtraPretenders/Triglav.tga"
        #name "Lord of Mysteries"
        #descr "In ancient times the Pantokrator appointed a single being to watch over the heavens, the earth and the Underworld. This being saw all that occurred, and reported to the Pantokrator each sin committed so that punishment could be assigned. Over time the being grew fond of the creatures over which it watched, and no longer wished to cause them pain and suffering. He took great golden bands and sealed them over his eyes, that he would not be able to see their sins. When the Pantokrator discovered such treachery he bound his servant below the earth for eternity. Now the shackles grow weaker and the Lord of Mysteries can return to the world he covets. He has three heads for watching the three realms, however the golden bands that block his sight cannot be removed. In combat the Lord of Mysteries will manifest gems of air, earth and death to aid in spellcasting."
        #humanoid
        #fixedname "Triglav"
        #gcost 170
        #hp 95
        #size 6
        #prot 0
        #mor 30
        #str 18
        #att 13
        #def 12
        #mr 20
        #prec 10
        #enc 2
        #mapmove 8
        #ap 8
        #heal
        #diseaseres 100
        #itemslots 16262 -- 3 heads
        #weapon 11 -- Great Sword
        #armor 15 -- Full Leather Armor
        #magicskill 1 1
        #magicskill 3 1
        #magicskill 5 1
        #startage 1000
        #maxage 3000
        #tmpairgems 1
        #tmpearthgems 1
        #tmpdeathgems 1
        #eyes 1
        #startitem 776 -- Golden Bands
        #spiritsight
        #expertleader
        #pathcost 60
        #startdom 3
        #homerealm 1
        #end

        #newevent
        #rarity 5
        #req_targmnr 5088
        #req_targnoitem 776
        #addequip 9
        #msg "Provide Golden Bands [Golden Bands]"
        #notext
        #nolog
        #end

        #newmonster 5089
        #spr1 "./ExtraPretenders/Khonsu.tga"
        #spr2 "./ExtraPretenders/Khonsu2.tga"
        #name "Neter Child"
        #descr "The Triad is a tripartite entity that appears as a Divine Father, Mother and Son that share a single Ka soul. The Son appears living but wrapped in linen and has a fierce aspect. He is strong in the magic of Death and is a righteous defender of the innocent. The Mother has powers of fertility and is the bearer of new life. She is strong in the magic of Nature and bears a mystic Ankh. The Father has Dominion over the heavens and knowledge of all things. He is strong in Astral Magic and wears a double crown to denote his lordly might. The Triad once governed human affairs, however following the rebellion of minor Gods the Pantokrator imprisoned all his servants to prevent further treachery. Now the Pantokrator has departed and the Triad can once more return to the world to claim the Throne of Heaven."
        #humanoid
        #fixedname "Khonsu"
        #gcost 300
        #hp 125
        #size 6
        #prot 8
        #mor 30
        #str 26
        #att 13
        #def 10
        #mr 20
        #prec 10
        #enc 2
        #mapmove 22
        #ap 12
        #heal
        #diseaseres 100
        #weapon 238 -- Magic Staff
        #magicskill 4 1
        #magicskill 5 1
        #magicskill 6 1
        #magicboost 5 1
        #startage 1000
        #maxage 3000
        #tmpastralgems 1
        #tmpdeathgems 1
        #fireres -5
        #fear 5
        #spiritsight
        #expertleader
        #triplegod 5
        #triplegodmag 0
        #triple3mon
        #pathcost 80
        #startdom 3
        #homerealm 7
        #end

        #newmonster 5090
        #spr1 "./ExtraPretenders/Amun.tga"
        #spr2 "./ExtraPretenders/Amun2.tga"
        #name "Neter Father"
        #descr "The Triad is a tripartite entity that appears as a Divine Father, Mother and Son that share a single Ka soul. The Son appears living but wrapped in linen and has a fierce aspect. He is strong in the magic of Death and is a righteous defender of the innocent. The Mother has powers of fertility and is the bearer of new life. She is strong in the magic of Nature and bears a mystic Ankh. The Father has Dominion over the heavens and knowledge of all things. He is strong in Astral Magic and wears a double crown to denote his lordly might. The Triad once governed human affairs, however following the rebellion of minor Gods the Pantokrator imprisoned all his servants to prevent further treachery. Now the Pantokrator has departed and the Triad can once more return to the world to claim the Throne of Heaven."
        #humanoid
        #fixedname "Amun"
        #gcost 300
        #hp 105
        #size 6
        #prot 3
        #mor 30
        #str 22
        #att 13
        #def 13
        #mr 20
        #prec 12
        #enc 2
        #mapmove 22
        #ap 16
        #heal
        #diseaseres 100
        #weapon 238 -- Magic Staff
        #armor 148 -- Crown
        #magicskill 4 1
        #magicskill 5 1
        #magicskill 6 1
        #magicboost 4 1
        #startage 1000
        #maxage 3000
        #tmpastralgems 1
        #tmpnaturegems 1
        #spiritsight
        #expertleader
        #triplegod 5
        #triplegodmag 0
        #pathcost 80
        #startdom 3
        #end

        #newmonster 5091
        #spr1 "./ExtraPretenders/Mut.tga"
        #spr2 "./ExtraPretenders/Mut2.tga"
        #name "Neteret Mother"
        #descr "The Triad is a tripartite entity that appears as a Divine Father, Mother and Son that share a single Ka soul. The Son appears living but wrapped in linen and has a fierce aspect. He is strong in the magic of Death and is a righteous defender of the innocent. The Mother has powers of fertility and is the bearer of new life. She is strong in the magic of Nature and bears a mystic Ankh. The Father has Dominion over the heavens and knowledge of all things. He is strong in Astral Magic and wears a double crown to denote his lordly might. The Triad once governed human affairs, however following the rebellion of minor Gods the Pantokrator imprisoned all his servants to prevent further treachery. Now the Pantokrator has departed and the Triad can once more return to the world to claim the Throne of Heaven."
        #humanoid
        #fixedname "Mut"
        #gcost 300
        #hp 95
        #size 6
        #prot 3
        #mor 30
        #str 20
        #att 13
        #def 13
        #mr 20
        #prec 12
        #enc 2
        #mapmove 22
        #ap 16
        #heal
        #diseaseres 100
        #weapon 92 -- Fist
        #armor 148 -- Crown
        #magicskill 4 1
        #magicskill 5 1
        #magicskill 6 1
        #magicboost 6 1
        #startage 1000
        #maxage 3000
        #tmpnaturegems 1
        #tmpdeathgems 1
        #female
        #spiritsight
        #superiorleader
        #triplegod 5
        #triplegodmag 0
        #pathcost 80
        #startdom 3
        #end

        #newmonster 5092
        #spr1 "./ExtraPretenders/Nyx.tga"
        #spr2 "./ExtraPretenders/Nyx2.tga"
        #name "Titan of the Night"
        #descr "The Titan of the Night is a giant of divine heritage. She was born out of chaos in ancient times and claimed dominion over the night and darkness in all its forms. She gave birth to the Keres, the bloodthirsty daimones of the underworld, and rules them as their queen. In time even the Pantokrator grew fearful of her and bound her in a great cave at the entrance to Tartarus. With the Pantokrator gone it is rumoured she may have already broken her chains and stalks the world unseen. The Titan of the Night can become invisible at will, but sometimes appears as a gaunt winged figure robed in bloody garments. The Titan of the Night is more powerful in darkness and her touch can drain the life from the living. Each month her children will bring her slaves of pure blood, and in combat she will manifest a magic pearl and a death gem for use in spellcasting."
        #humanoid
        #fixedname "Nyx"
        #gcost 230
        #hp 85
        #size 6
        #prot 3
        #mor 30
        #str 20
        #att 14
        #def 14
        #mr 20
        #prec 12
        #enc 2
        #mapmove 30
        #ap 14
        #stealthy 0
        #heal
        #diseaseres 100
        #weapon 63 -- Life Drain
        #magicskill 4 1
        #magicskill 5 1
        #magicskill 7 1
        #startage 1000
        #maxage 3000
        #tmpastralgems 1
        #tmpdeathgems 1
        #gemprod 7 1
        #fear 5
        #darkpower 3
        #female
        #flying
        #invisible
        #spiritsight
        #superiorleader
        #goodundeadleader
        #pathcost 60
        #startdom 3
        #homerealm 3
        #end

        #newmonster 5093
        #spr1 "./ExtraPretenders/Vesna.tga"
        #spr2 "./ExtraPretenders/Vesna.tga"
        #name "Deives of Spring"
        #descr "The Deives of the Seasons is a dualistic entity of divine heritage, granted power over the changing seasons by the Pantokrator. In the Spring she takes the form of a beautiful maiden, always smiling and barefoot with flowers in her hair. For half the year she carries the smell of spring with her wherever she goes and magical gems will spring forth at her bidding each month. When winter comes the Deives changes to become a pale and coldly beautiful woman with long black hair and wolf like claws. In this form she is greatly feared, and is surrounded by the stench of death and the chill wind of midwinter. Her magical powers wax and wane with the changing seasons. Although content for aeons to simply marshal the coming of the seasons, with the disappearance of the Pantokrator her people have begun to worship her and implore her to lead them to glory. Now she has accepted the mantle of a Pretender God and has set her sights on the Throne of Heaven."
        #humanoid
        #fixedname "Vesna"
        #gcost 200
        #hp 95
        #size 6
        #prot 3
        #mor 30
        #str 20
        #att 13
        #def 13
        #mr 20
        #prec 12
        #enc 2
        #mapmove 22
        #ap 14
        #heal
        #springpower 25
        #diseaseres 100
        #weapon 92 -- Fist
        #magicskill 2 1
        #magicskill 5 1
        #magicskill 6 1
        #magicboost 6 2
        #magicboost 5 -2
        #gemprod 6 1
        #gemprod 2 1
        #startage 1000
        #maxage 3000
        #tmpnaturegems 1
        #female
        #superiorleader
        #autumnshape 5094
        #wintershape 5094
        #pathcost 60
        #startdom 3
        #homerealm 1 -- North
        #end

        #newmonster 5094
        #spr1 "./ExtraPretenders/Morana.tga"
        #spr2 "./ExtraPretenders/Morana.tga"
        #name "Deives of Winter"
        #descr "The Deives of the Seasons is a dualistic entity of divine heritage, granted power over the changing seasons by the Pantokrator. In the Spring she takes the form of a beautiful maiden, always smiling and barefoot with flowers in her hair. For half the year she carries the smell of spring with her wherever she goes and magical gems will spring forth at her bidding each month. When winter comes the Deives changes to become a pale and coldly beautiful woman with long black hair and wolf like claws. In this form she is greatly feared, and is surrounded by the stench of death and the chill wind of midwinter. Her magical powers wax and wane with the changing seasons. Although content for aeons to simply marshal the coming of the seasons, with the disappearance of the Pantokrator her people have begun to worship her and implore her to lead them to glory. Now she has accepted the mantle of a Pretender God and has set her sights on the Throne of Heaven."
        #humanoid
        #fixedname "Morana"
        #gcost 200
        #hp 95
        #size 6
        #prot 3
        #mor 30
        #str 20
        #att 13
        #def 13
        #mr 20
        #prec 12
        #enc 2
        #mapmove 22
        #ap 14
        #winterpower 25
        #fear 5
        #cold 8
        #heal
        #diseaseres 100
        #weapon 29 -- Claw
        #magicskill 2 1
        #magicskill 5 1
        #magicskill 6 1
        #magicboost 5 2
        #magicboost 6 -2
        #startage 1000
        #maxage 3000
        #tmpdeathgems 2
        #tmpwatergems 1
        #female
        #superiorleader
        #springshape 5093
        #summershape 5093
        #pathcost 60
        #startdom 3
        #end

        #newmonster 5095
        #spr1 "./ExtraPretenders/Praxidike.tga"
        #spr2 "./ExtraPretenders/Praxidike2.tga"
        #name "Spirit of Justice"
        #descr "The Spirit of Justice and her daughters the Spirit of Moral Virtue and the Spirit of Unanimity have existed since ancient times. Under the reign of the Pantokrator they acted as enforcers of law and agents of his vengeance, punishing wrongdoers and those that had angered the Mighty One. With the Pantokrator gone the people have begun to make offerings to the Spirits to bring them Justice and Virtue, and they have taken on the mantle of a Pretender God to claim the Throne of Heaven for themselves. The Spirits are surrounded with a powerful Aura of Splendor and will be almost impossible for mortals to strike at. Anyone injuring the Spirits will have their blow revisited upon them."
        #humanoid
        #fixedname "Praxidike"
        #gcost 300
        #hp 25
        #size 3
        #prot 0
        #mor 30
        #str 12
        #att 14
        #def 14
        #mr 18
        #prec 12
        #enc 2
        #mapmove 34
        #ap 12
        #awe 3
        #fear 5
        #bloodvengeance 5
        #invulnerable 15
        #heal
        #diseaseres 100
        #weapon 472 -- Flaming Sword
        #armor 71 -- Golden Shield
        #magicskill 0 1
        #magicskill 1 2
        #researchbonus -5
        #startage 500
        #fireres 5
        #shockres 5
        #maxage 3000
        #flying
        #magicbeing
        #blind
        #spiritsight
        #goodleader
        #okmagicleader
        #female
        #onebattlespell 604 -- Personal Luck
        #triplegod 8
        #triple3mon
        #pathcost 80
        #startdom 2
        #homerealm 3
        #end

        #newmonster 5096
        #spr1 "./ExtraPretenders/Arete.tga"
        #spr2 "./ExtraPretenders/Arete2.tga"
        #name "Spirit of Moral Virtue"
        #descr "The Spirit of Justice and her daughters the Spirit of Moral Virtue and the Spirit of Unanimity have existed since ancient times. Under the reign of the Pantokrator they acted as enforcers of law and agents of his vengeance, punishing wrongdoers and those that had angered the Mighty One. With the Pantokrator gone the people have begun to make offerings to the Spirits to bring them Justice and Virtue. Now they have taken on the mantle of a Pretender God to claim the Throne of Heaven for themselves. The Spirits are surrounded with a powerful Aura of Splendor and will be almost impossible for mortals to strike at. Anyone injuring the Spirits will have their blow revisited upon them."
        #humanoid
        #fixedname "Arete"
        #gcost 300
        #hp 25
        #size 2
        #prot 0
        #mor 30
        #str 12
        #att 14
        #def 14
        #mr 18
        #prec 12
        #enc 2
        #mapmove 34
        #ap 12
        #awe 5
        #bloodvengeance 5
        #invulnerable 10
        #inspirational 2
        #heal
        #onebattlespell 604 -- Personal Luck
        #diseaseres 100
        #weapon 474 -- Golden Sword
        #armor 71 -- Golden Shield
        #magicskill 0 1
        #magicskill 1 2
        #researchbonus -5
        #startage 500
        #fireres 5
        #shockres 5
        #maxage 3000
        #flying
        #magicbeing
        #spiritsight
        #goodleader
        #okmagicleader
        #female
        #triplegod 8
        #triplegodmag 1
        #triple3mon
        #pathcost 80
        #startdom 2
        #end

        #newmonster 5097
        #spr1 "./ExtraPretenders/Homonia.tga"
        #spr2 "./ExtraPretenders/Homonia2.tga"
        #name "Spirit of Unanimity"
        #descr "The Spirit of Justice and her daughters the Spirit of Moral Virtue and the Spirit of Unanimity have existed since ancient times. Under the reign of the Pantokrator they acted as enforcers of law and agents of his vengeance, punishing wrongdoers and those that had angered the Mighty One. With the Pantokrator gone the people have begun to make offerings to the Spirits to bring them Justice and Virtue. Now they have taken on the mantle of a Pretender God to claim the Throne of Heaven for themselves. The Spirits are surrounded with a powerful Aura of Splendor and will be almost impossible for mortals to strike at. Anyone injuring the Spirits will have their blow revisited upon them."
        #humanoid
        #fixedname "Homonia"
        #gcost 300
        #hp 25
        #size 2
        #prot 0
        #mor 30
        #str 12
        #att 14
        #def 14
        #mr 18
        #prec 12
        #enc 2
        #mapmove 34
        #ap 12
        #awe 5
        #bloodvengeance 5
        #invulnerable 10
        #heal
        #onebattlespell 604 -- Personal Luck
        #incunrest -50
        #diseaseres 100
        #weapon 202 -- Magic Sword
        #armor 71 -- Golden Shield
        #magicskill 0 1
        #magicskill 1 2
        #researchbonus -5
        #startage 500
        #fireres 5
        #shockres 5
        #maxage 3000
        #magicbeing
        #spiritsight
        #goodleader
        #okmagicleader
        #female
        #triplegod 8
        #triplegodmag 1
        #triple3mon
        #pathcost 80
        #startdom 2
        #end

        #newmonster 5099
        #spr1 "./ExtraPretenders/WolfMan.tga"
        #spr2 "./ExtraPretenders/WolfMan2.tga"
        #name "Vargr"
        #descr "The Vargr is a gigantic wolflike creature born of Fenrer, the Great Adversary. It has the jaws and strength of a great wolf, walks upright and is covered in thick fur. The Vargr possesses tremendous strength and regenerative powers. It has lurked in the deepest parts of the frozen forests of the North since the world was young, watching and waiting. Now, with the Pantokrator gone it has emerged to claim the Throne of Heaven and rule over the entire world. The monstrous creature has inherited the foul temper of his father and is prone to go berserk if anyone opposes him."
        #humanoid
        #fixedname "Skoll"
        #gcost 250
        #hp 85
        #size 5
        #prot 12
        #mor 30
        #str 28
        #att 14
        #def 12
        #mr 20
        #prec 10
        #enc 2
        #mapmove 22
        #ap 14
        #fear 5
        #coldres 15
        #regeneration 10
        #stealthy 0
        #heal
        #forestsurvival
        #berserk 5
        #diseaseres 100
        #weapon 20 -- Bite
        #weapon 29 -- Claw
        #weapon 29 -- Claw
        #magicskill 2 1
        #magicskill 6 1
        #magicskill 7 1
        #startage 1000
        #maxage 3000
        #tmpwatergems 1
        #tmpnaturegems 1
        #gemprod 7 1
        #goodleader
        #beastmaster 1
        #homerealm 1
        #pathcost 60
        #startdom 3
        #end

        -- #newmonster 
        -- #name ""
        -- #spr1 "./ExtraPretenders/.tga"
        -- #spr2 "./ExtraPretenders/.tga"
        -- #descr "."
        -- #gcost 
        -- #size 
        -- #hp 
        -- #prot 
        -- #mr 
        -- #mor 
        -- #str 
        -- #att 
        -- #def 
        -- #prec 
        -- #enc 
        -- #mapmove 
        -- #ap 
        -- #magicskill 
        -- #weapon ""
        -- #XXleader
        -- #maxage 
        -- #startage 
        -- #pathcost 60
        -- #startdom 3
        -- #end
    -- IMMOBILES  

        #newmonster 4986
        #name "Pillar of Eternal Flame"
        #spr1 "./ExtraPretenders/PillarFlame.tga"
        #spr2 "./ExtraPretenders/PillarFlame2.tga"
        #descr "The Pillar of Eternal Flame is a powerful spirit that manifests itself as a pillar of fire that has burned for eternity. The flames represent the life and souls of its followers, which are pure and cannot be polluted. The sacred fire can never be allowed to be extinguished and is tended day and night. The spirit can possess willing beings in order to perform physical tasks such as forging and enchanting items, but cannot leave its spiritual home. The Pillar is a powerful wielder of Fire magic and will create magical gems each month. The intense heat will burn any that attempt to strike at the spirit."
        #miscshape
        #gcost 150
        #size 6
        #hp 75
        #prot 0
        #mr 18
        #mor 30
        #str 20
        #att 10
        #def 0
        #prec 10
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 0 3
        #gemprod 0 2
        #weapon 229 -- Flame Strike
        #expertleader
        #immobile
        #ethereal
        #spiritsight
        #heal
        #diseaseres 100
        #heat 10
        #fireshield 8
        #blind
        #spiritsight
        #firepower 1
        #uwdamage 100
        #neednoteat
        #inanimate
        #bonusspells 1
        #itemslots 4096 -- 1 misc
        #fireres 60
        #poisonres 25
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homerealm 5 -- Middle East
        #end

        #newmonster 4987
        #name "Idol of Watery Wisdom"
        #spr1 "./ExtraPretenders/WateryWisdom.tga"
        #spr2 "./ExtraPretenders/WateryWisdom.tga"
        #descr "The Idol of Watery Wisdom is a primordial spirit of knowledge and water that once served a previous Pantokrator and brought wisdom to the world. When the world had learned enough its services were no longer needed and the spirit was bound in a huge stone statue for eternity. With the Pantokrator gone, its powers have begun to manifest themselves and it is now worshipped as a reawakening god. The spirit cannot leave the statue, but it can possess willing targets in order to make its will heard and to perform tasks such as forging items for enchantment. The spirit is tremendously strong in its dominion. In a physical battle, the statue would be difficult to destroy, even though it cannot strike back."
        #miscshape
        #gcost 170
        #size 6
        #hp 150
        #prot 24
        #mr 18
        #mor 30
        #str 15
        #att 5
        #def 0
        #prec 10
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 2 3
        #weapon 0
        #expertleader
        #immobile
        #inanimate
        #poisonres 25
        #blind
        #spiritsight
        #stonebeing
        #slashres
        #pierceres
        #neednoteat
        #amphibian
        #heal
        #researchbonus 10
        #diseaseres 100
        #bonusspells 1
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homerealm 10 -- Default
        #end

        #newmonster 4988
        #name "Statue of the Sun and Rain"
        #spr1 "./ExtraPretenders/SunAndRain.tga"
        #spr2 "./ExtraPretenders/SunAndRain.tga"
        #descr "The Statue of the Sun and Rain is a primordial spirit that once served a previous Pantokrator and brought gifts of sunshine and rainfall to the world. Once the cycle of the seasons had been set its services were no longer needed and the spirit was bound in a huge stone statue for eternity. With the Pantokrator gone, its powers have begun to manifest themselves and it is now worshipped as a reawakening god. The spirit cannot leave the statue, but it can possess willing targets in order to make its will heard and to perform tasks such as forging items for enchantment. Around the statue the sun will shine and the rain will fall with increased vigour, bringing life and growth to the surrounding province. The spirit is tremendously strong in its dominion. In a physical battle, the statue would be difficult to destroy, even though it cannot strike back."
        #miscshape
        #gcost 160
        #size 6
        #hp 150
        #prot 24
        #mr 18
        #mor 30
        #str 15
        #att 5
        #def 0
        #prec 10
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 0 1
        #magicskill 2 2
        #decscale 3 -- +Growth
        #weapon 0
        #expertleader
        #immobile
        #blind
        #spiritsight
        #inanimate
        #heal
        #diseaseres 100
        #poisonres 25
        #stonebeing
        #slashres
        #pierceres
        #neednoteat
        #amphibian
        #bonusspells 1
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homerealm 6 -- MesoAmerica
        #end

        #newmonster 4989
        #name "Idol of the Sun"
        #spr1 "./ExtraPretenders/SunIdol.tga"
        #spr2 "./ExtraPretenders/SunIdol.tga"
        #descr "The Idol of the Sun is a primordial spirit of the sun that once served a previous Pantokrator and brought life to the world. When the sun had been placed in the sky its services were no longer needed and the spirit was bound in a huge golden statue for eternity. With the Pantokrator gone, its powers have begun to manifest themselves and it is now worshipped as a reawakening god. The spirit cannot leave the statue, but it can possess willing targets in order to make its will heard and to perform tasks such as forging items for enchantment. The statue shines with intense light and enemies will stand dumbfounded or be struck blind if they attempt to strike it. The spirit is tremendously strong in its dominion and radiates power as well as light."
        #miscshape
        #gcost 170
        #size 6
        #hp 180
        #prot 22
        #mr 18
        #mor 30
        #str 15
        #att 5
        #def 0
        #prec 10
        #enc 0
        #mapmove 0
        #ap 2
        #awe 3
        #eyeloss
        #magicskill 0 1
        #magicskill 4 2
        #gemprod 4 1
        #weapon 0
        #expertleader
        #immobile
        #blind
        #spiritsight
        #inanimate
        #heal
        #diseaseres 100
        #poisonres 25
        #slashres
        #pierceres
        #neednoteat
        #amphibian
        #bonusspells 1
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homerealm 10 -- Default
        #end

        #newmonster 4990
        #name "Emissary of Antrax"
        #spr1 "./ExtraPretenders/BaneSpiritCircle.tga"
        #spr2 "./ExtraPretenders/BaneSpiritCircle2.tga"
        #descr "The Emissary of Antrax is a creature of living banefire birthed by the power of the corrupted fire king Antrax. Summoned to this world by its followers using a powerful summoning circle, it has now amassed enough power to claim the title of God itself. Should the summoning circle be disrupted the spirit would immediately return to the Underworld until summoned again. The Emissary will summon servants from the Underworld when its dominion grows strong. Its only weakness is the bond with the Underworld, which makes it possible for priests to banish the creature from this world. As a creature of Antrax the Emissary is surrounded by poisonous banefire and is strong in the magic of the Underworld."
        #djinn
        #gcost 170
        #size 6
        #hp 55
        #prot 0
        #mr 18
        #mor 30
        #str 16
        #att 11
        #def 10
        #prec 12
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 0 2
        #magicskill 5 1
        #weapon 348 -- Banefire Strike
        #goodleader
        #goodundeadleader
        #immobile
        #undead
        #ethereal
        #spiritsight
        #heal
        #diseaseres 100
        #banefireshield 8
        #fear 5
        #domsummon2 5266 -- Banefire Child
        #battlesum1 5266 -- Banefire Child
        #poisonres 25
        #fireres 25
        #neednoteat
        #itemslots 13446 -- No Feet
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #end

        #newmonster 4991
        #name "Infernal Spirit"
        #spr1 "./ExtraPretenders/DemonCircle.tga"
        #spr2 "./ExtraPretenders/DemonCircle2.tga"
        #descr "The Infernal Spirit is a powerful Demonic force that is attempting to claim the Throne of Heaven and subjugate the entire world. Summoned to this world by its followers using a powerful summoning circle, it has now amassed enough power to claim the title of God itself. Should the summoning circle be disrupted the spirit would immediately return to the Inferno until summoned again. The Spirit will summon devils from the Inferno when its dominion grows strong, however this bond with the Inferno makes it possible for magic to banish the creature from this world."
        #djinn
        #gcost 170
        #size 6
        #hp 65
        #prot 12
        #mr 18
        #mor 30
        #str 19
        #att 14
        #def 12
        #prec 12
        #enc 1
        #mapmove 0
        #ap 2
        #magicskill 0 1
        #magicskill 7 2
        #weapon 244 -- Dark Fire Sword
        #weapon 68 -- Barbed Tail
        #domsummon2 638 -- Spine Devils
        #domsummon20 304 -- Devil
        #okleader
        #okundeadleader
        #immobile
        #demon
        #heal
        #spiritsight
        #diseaseres 100
        #poisonres 15
        #fireres 15
        #itemslots 13446 -- No Feet
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homerealm 5 -- Middle East
        #end

        #newmonster 4992
        #name "Plague Idol"
        #spr1 "./ExtraPretenders/PlagueIdol.tga"
        #spr2 "./ExtraPretenders/PlagueIdol.tga"
        #descr "This Idol has been around for a very long time and has been an important place of worship for the sick and dying. Through the ages those afflicted with plagues and illnesses of all kinds have left offerings here, and those dead of disease have been buried at the foot of the idol. There are whispers that in the dead of night occasionally Demons are seen taking the diseased offerings. Countless offerings later and now with the Pantokrator gone, the Plague Idol has the chance to put the world under its strong dominion and becoming the True God."
        #miscshape
        #gcost 160
        #size 6
        #hp 150
        #prot 24
        #mr 18
        #mor 30
        #str 15
        #att 5
        #def 0
        #prec 12
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 1 1
        #magicskill 5 2
        #autodisgrinder 2
        #weapon 0
        #diseasecloud 10
        #domsummon20 1662 -- Disease Demon
        #goodleader
        #goodundeadleader
        #immobile
        #blind
        #spiritsight
        #inanimate
        #poisonres 25
        #slashres
        #pierceres
        #neednoteat
        #amphibian
        #stonebeing
        #heal
        #diseaseres 100
        #bonusspells 1
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homerealm 10 -- Default
        #end

        #newmonster 4993
        #name "Divine Egg"
        #spr1 "./ExtraPretenders/DivineEgg.tga"
        #spr2 "./ExtraPretenders/DivineEgg.tga"
        #descr "The Divine Egg is a gigantic egg that has existed as long as any can remember. What laid the egg or why it has not hatched are a mystery known only to the Pantokrator himself. Its followers claim that the egg contains the next Universe, and it will crack open on Doomsday when the world falls into fire. The Divine Egg is infused with the magic of new life, and around it living things will grow and heal at an amazing rate. With the Pantokrator gone, its powers have begun to manifest themselves and it is now worshipped as a reawakening god. The Egg cannot move, but it can possess willing targets in order to make its will heard and to perform tasks such as forging items for enchantment. The Egg is tremendously strong in its dominion. In a physical battle, the Egg would be difficult to destroy, even though it cannot strike back."
        #miscshape
        #gcost 170
        #size 6
        #hp 120
        #prot 25
        #mr 18
        #mor 30
        #str 15
        #att 5
        #def 0
        #prec 12
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 0 1
        #magicskill 6 2
        #weapon 0
        #autohealer 1
        #deathfire 30
        #expertleader
        #immobile
        #blind
        #spiritsight
        #neednoteat
        #amphibian
        #heal
        #diseaseres 100
        #bonusspells 1
        #itemslots 4096 -- 1 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homerealm 10 -- Default
        #end

        #newmonster 4994
        #name "Spirit of the Well"
        #spr1 "./ExtraPretenders/SpiritWell.tga"
        #spr2 "./ExtraPretenders/SpiritWell2.tga"
        #descr "The Spirit of the Well is a powerful spirit that inhabits a deep well leading to the centre of the earth. Through the well the spirit can access the deep waters of the world to draw on their strength. The spirit appears at the well to perform tasks such as forging items for enchantment. The Spirit of the Well is immobile and cannot leave the well it inhabits. With the Pantokrator gone, its powers have begun to manifest themselves and it is now worshipped as an awakening god. The Spirit is tremendously strong in its dominion and magically powerful. The spirit can be destroyed, but not easily."
        #djinn
        #gcost 160
        #size 6
        #hp 65
        #prot 0
        #mr 18
        #mor 30
        #str 18
        #att 15
        #def 10
        #prec 12
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 2 2
        #magicskill 3 1
        #gemprod 2 1
        #weapon 90 -- Crush
        #expertleader
        #immobile
        #poisonres 25
        #slashres
        #pierceres
        #bluntres
        #heal
        #diseaseres 100
        #female
        #neednoteat
        #amphibian
        #itemslots 13446 -- No Feet
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homerealm 10 -- Default
        #end

        #newmonster 4995
        #name "Sacred Grove"
        #spr1 "./ExtraPretenders/SacredGrove.tga"
        #spr2 "./ExtraPretenders/SacredGrove.tga"
        #descr "The Sacred Grove is a powerful spirit that inhabits a lush grove of great importance. The spirit cannot leave the grove, but it can animate plants and inhabit animals in order to make its will heard and to perform tasks such as forging items for enchantment. The spirit is tremendously strong in its dominion and it is also magically powerful. In a physical battle the grove would be difficult to destroy, and is protected by animals it has called. The grove is completely immobile and cannot move even by magic."
        #djinn
        #gcost 150
        #size 6
        #hp 180
        #prot 12
        #mr 18
        #mor 30
        #str 15
        #att 5
        #def 0
        #prec 12
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 2 1
        #magicskill 6 2
        #gemprod 6 1
        #weapon 0
        #batstartsum1d6 694 -- Great Bear
        #batstartsum2d6 1084 -- Moose
        #batstartsum3d6 284 -- Wolf
        #batstartsum4d6 549 -- Boar
        #expertleader
        #immobile
        #blind
        #spiritsight
        #inanimate
        #heal
        #poisonres 25
        #neednoteat
        #bonusspells 1
        #unteleportable
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homerealm 3 -- Mediterranean
        #end

        #newmonster 4996
        #name "Blood Soaked Edifice"
        #spr1 "./ExtraPretenders/BloodMonolith.tga"
        #spr2 "./ExtraPretenders/BloodMonolith2.tga"
        #descr "The Blood Soaked Edifice is a powerful spirit that inhabits a huge standing stone. Once a pure spirit of the Earth, it has been tainted by foul blood rituals. As the ground was soaked with sacrificial blood the spirit gained a craving for the blood of the innocent. The ground itself will sometime rise up in a corrupted form to serve the spirit as its dominion grows stronger. The spirit cannot leave the Edifice, but it can possess the weak willed in order to make its will heard and to perform tasks such as forging items for enchantment. The spirit is tremendously strong in its dominion and it is also magically powerful. In a physical battle, the stone would be difficult to destroy, even though it cannot strike back."
        #miscshape
        #gcost 160
        #size 6
        #hp 200
        #prot 28
        #mr 18
        #mor 30
        #str 15
        #att 5
        #def 0
        #prec 12
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 3 2
        #magicskill 7 1
        #domsummon20 2282 -- Size 4 Illearth
        #raredomsummon 2280 -- Size 6 Illearth
        #weapon 0
        #expertleader
        #okundeadleader
        #immobile
        #blind
        #heal
        #diseaseres 100
        #spiritsight
        #inanimate
        #poisonres 25
        #slashres
        #pierceres
        #neednoteat
        #amphibian
        #stonebeing
        #bonusspells 1
        #itemslots 4096 -- 1 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homerealm 1 -- North
        #end

        #newmonster 4997
        #name "Spirit of Bones"
        #spr1 "./ExtraPretenders/BonePile.tga"
        #spr2 "./ExtraPretenders/BonePile.tga"
        #descr "The Spirit of Bones is an ancient spirit of nature created to impose the cycle of life and death upon the world in ages past. Once the cycle was in place its services were no longer needed and the spirit was bound beneath the earth for eternity. Over the centuries many animals and men have travelled to the site of the spirits imprisonment to die, and now a great pile of bones and skulls marks the place where the spirit is bound. With the Pantokrator gone, its powers have begun to manifest themselves and it is now worshipped as a reawakening god. The spirit cannot leave its spiritual home, but it can possess willing targets in order to make its will heard and to perform tasks such as forging items for enchantment. As its dominion grows strong vines may animate the corpses of the bone pile to serve it. In a physical battle, the bone pile would be difficult to destroy, even though it cannot strike back."
        #miscshape
        #gcost 150
        #size 6
        #hp 120
        #prot 12
        #mr 18
        #mor 30
        #str 14
        #att 5
        #def 0
        #prec 12
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 5 2
        #magicskill 6 1
        #weapon 0
        #domsummon20 314 -- Mandragora
        #reanimator 10
        #batstartsum1 861 -- Ettin Mandragora
        #okleader
        #okundeadleader
        #immobile
        #blind
        #heal
        #diseaseres 100
        #spiritsight
        #inanimate
        #poisonres 25
        #neednoteat
        #amphibian
        #bonusspells 1
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homerealm 3 -- Mediterranean
        #end

        #newmonster 4998
        #name "Statue of the Silent God"
        #spr1 "./ExtraPretenders/StatueSilent.tga"
        #spr2 "./ExtraPretenders/StatueSilent.tga"
        #descr "The Statue of the Silent God is a primordial spirit that once served a previous Pantokrator. Little is known about the spirit or its purpose and it never speaks about this to its followers. For reasons unknown the spirit was bound in a huge stone statue for eternity by the Pantokrator and its name was expunged from all written records. With the Pantokrator gone, its powers have begun to manifest themselves and it is now once more worshipped as a reawakening god. The spirit cannot leave the statue, but it can possess willing targets to perform tasks such as forging items for enchantment. The spirit is tremendously strong in its dominion. In a physical battle, the statue would be difficult to destroy, even though it cannot strike back."
        #miscshape
        #gcost 160
        #size 6
        #hp 150
        #prot 24
        #mr 18
        #mor 30
        #str 15
        #att 5
        #def 0
        #prec 12
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 0 1
        #magicskill 4 1
        #magicskill 5 1
        #weapon 0
        #expertleader
        #okundeadleader
        #immobile
        #blind
        #heal
        #diseaseres 100
        #spiritsight
        #inanimate
        #stonebeing
        #poisonres 25
        #slashres
        #pierceres
        #neednoteat
        #amphibian
        #bonusspells 1
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homerealm 10 -- Default
        #end

        #newmonster 4999
        #name "Idol of Winter"
        #spr1 "./ExtraPretenders/WinterIdol.tga"
        #spr2 "./ExtraPretenders/WinterIdol.tga"
        #descr "The Idol of Winter is a primordial spirit of the north that once served a previous Pantokrator and brought the season of winter to the world. When the winter was coldest its services were no longer needed and the spirit was bound in a rune-carved idol for eternity. With the Pantokrator gone, its powers have begun to manifest themselves and it is now worshipped as a reawakening god. The spirit cannot leave the idol, but it can possess willing targets in order to make its will heard and to perform tasks such as forging items for enchantment. The spirit is tremendously strong in its dominion. The statue is surrounded by an icy wind that will freeze those nearby, and the entire province will be trapped in permament winter. Winter wolves will appear to serve the spirit when its dominion grows strong."
        #miscshape
        #gcost 170
        #size 6
        #hp 120
        #prot 20
        #mr 18
        #mor 30
        #str 15
        #att 5
        #def 0
        #prec 12
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 2 1
        #magicskill 5 1
        #magicskill 7 1
        #domsummon2 511 -- Winter Wolf
        #weapon 0
        #cold 15
        #incscale 2 -- +Cold
        #expertleader
        #okundeadleader
        #immobile
        #blind
        #heal
        #diseaseres 100
        #spiritsight
        #inanimate
        #coldres 25
        #poisonres 25
        #bluntres
        #pierceres
        #neednoteat
        #amphibian
        #bonusspells 1
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homerealm 1 -- North
        #end

        #newmonster 5000
        #name "Foul Statue"
        #spr1 "./ExtraPretenders/FoulStatue.tga"
        #spr2 "./ExtraPretenders/FoulStatue.tga"
        #descr "This Statue has been around for a very long time and it has always been worshipped only in the dead of night. Rumours spread of foul offerings and unholy rituals performed at the statue in return for dark gifts and blessings. Through the ages its followers have grown in confidence and the spirit of the statue has continued to grow in power. Countless offerings later and now with the Pantokrator gone, the Foul Statue has the chance to put the world under its strong dominion and becoming the True God. The statue is tremendously strong in its dominion. In a physical battle, the statue would be difficult to destroy, even though it cannot strike back."
        #miscshape
        #gcost 160
        #size 6
        #hp 150
        #prot 24
        #mr 18
        #mor 30
        #str 15
        #att 5
        #def 0
        #prec 12
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 0 1
        #magicskill 5 1
        #magicskill 7 1
        #gemprod 7 2
        #weapon 0
        #expertleader
        #okundeadleader
        #immobile
        #blind
        #heal
        #diseaseres 100
        #spiritsight
        #inanimate
        #poisonres 25
        #slashres
        #pierceres
        #neednoteat
        #amphibian
        #stonebeing
        #bonusspells 1
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homerealm 7 -- Africa
        #end

        #newmonster 5001
        #name "Idol of the Seasons"
        #spr1 "./ExtraPretenders/IdolWild.tga"
        #spr2 "./ExtraPretenders/IdolWild.tga"
        #descr "This Idol has been around for a very long time and at the start of each season people have left their offerings. As the seasons change offerings have been left to ensure a bountiful spring and autumn, a long summer, or a temperate winter. Countless offerings later and now with the Pantokrator gone, the Idol of the Seasons has the chance to put the world under its strong dominion and become the True God. Seasonal animal spirits may appear to serve it as its dominion grows strong. The idol is tremendously strong in its dominion. In a physical battle, the idol would be difficult to destroy, even though it cannot strike back."
        #miscshape
        #gcost 170
        #size 6
        #hp 120
        #prot 20
        #mr 18
        #mor 30
        #str 15
        #att 5
        #def 0
        #prec 12
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 0 1
        #magicskill 2 1
        #magicskill 6 1
        #summon1 513 -- Spring Hawk
        #autumnshape 5002
        #wintershape 5003
        #summershape 5004
        #weapon 0
        #goodleader
        #goodmagicleader
        #immobile
        #blind
        #heal
        #diseaseres 100
        #spiritsight
        #inanimate
        #poisonres 25
        #slashres
        #pierceres
        #neednoteat
        #amphibian
        #stonebeing
        #bonusspells 1
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homerealm 2
        #end

        #newmonster 5002
        #name "Idol of the Seasons"
        #spr1 "./ExtraPretenders/IdolWild.tga"
        #spr2 "./ExtraPretenders/IdolWild.tga"
        #descr "This Idol has been around for a very long time and at the start of each season people have left their offerings. As the seasons change offerings were left to ensure a bountiful spring and autumn, a long summer, or a temperate winter. Countless offerings later and now with the Pantokrator gone, the Idol of the Seasons has the chance to put the world under its strong dominion and become the True God. Each season animal spirits will appear to serve it as its dominion grows strong. The idol is tremendously strong in its dominion. In a physical battle, the idol would be difficult to destroy, even though it cannot strike back."
        #miscshape
        #gcost 170
        #size 6
        #hp 120
        #prot 20
        #mr 18
        #mor 30
        #str 15
        #att 5
        #def 0
        #prec 12
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 0 1
        #magicskill 2 1
        #magicskill 6 1
        #domsummon20 512 -- Fall Bear
        #springshape 5001
        #wintershape 5003
        #summershape 5004
        #weapon 0
        #goodleader
        #goodmagicleader
        #immobile
        #blind
        #heal
        #diseaseres 100
        #spiritsight
        #inanimate
        #poisonres 25
        #slashres
        #pierceres
        #neednoteat
        #amphibian
        #stonebeing
        #bonusspells 1
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #end

        #newmonster 5003
        #name "Idol of the Seasons"
        #spr1 "./ExtraPretenders/IdolWild.tga"
        #spr2 "./ExtraPretenders/IdolWild.tga"
        #descr "This Idol has been around for a very long time and at the start of each season people have left their offerings. As the seasons change offerings were left to ensure a bountiful spring and autumn, a long summer, or a temperate winter. Countless offerings later and now with the Pantokrator gone, the Idol of Beasts has the chance to put the world under its strong dominion and become the True God. Each season animal spirits will appear to serve it as its dominion grows strong. The idol is tremendously strong in its dominion. In a physical battle, the idol would be difficult to destroy, even though it cannot strike back."
        #miscshape
        #gcost 170
        #size 6
        #hp 120
        #prot 20
        #mr 18
        #mor 30
        #str 15
        #att 5
        #def 0
        #prec 12
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 0 1
        #magicskill 2 1
        #magicskill 6 1
        #summon1 511 -- Winter Wolf
        #springshape 5001
        #autumnshape 5002
        #summershape 5004
        #weapon 0
        #goodleader
        #goodmagicleader
        #immobile
        #blind
        #heal
        #diseaseres 100
        #spiritsight
        #inanimate
        #poisonres 25
        #slashres
        #pierceres
        #neednoteat
        #amphibian
        #stonebeing
        #bonusspells 1
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #end

        #newmonster 5004
        #name "Idol of the Seasons"
        #spr1 "./ExtraPretenders/IdolWild.tga"
        #spr2 "./ExtraPretenders/IdolWild.tga"
        #descr "This Idol has been around for a very long time and at the start of each season people have left their offerings. As the seasons change offerings were left to ensure a bountiful spring and autumn, a long summer, or a temperate winter. Countless offerings later and now with the Pantokrator gone, the Idol of Beasts has the chance to put the world under its strong dominion and become the True God. Each season animal spirits will appear to serve it as its dominion grows strong. The idol is tremendously strong in its dominion. In a physical battle, the idol would be difficult to destroy, even though it cannot strike back."
        #miscshape
        #gcost 170
        #size 6
        #hp 120
        #prot 20
        #mr 18
        #mor 30
        #str 15
        #att 5
        #def 0
        #prec 12
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 0 1
        #magicskill 2 1
        #magicskill 6 1
        #domsummon20 515 -- Summer Lion
        #springshape 5001
        #autumnshape 5002
        #wintershape 5003
        #weapon 0
        #goodleader
        #goodmagicleader
        #immobile
        #blind
        #heal
        #diseaseres 100
        #spiritsight
        #inanimate
        #poisonres 25
        #slashres
        #pierceres
        #neednoteat
        #amphibian
        #stonebeing
        #bonusspells 1
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #end

        #newmonster 5005
        #name "Protean Abomination"
        #spr1 "./ExtraPretenders/Abhoth.tga"
        #spr2 "./ExtraPretenders/Abhoth2.tga"
        #descr "The Protean Abomination is a horrid, grayish-green protean pool that has existed since the dawn of time. It was created to bring life to the world, however it became corrupted and was imprisoned for eternity in the deepest part of the world. With the Pantokrator gone, its powers have begun to manifest again and it is worshipped as a reawakening god. Misshapen creatures constantly form in the mass and crawl away from it, their forms ranging from amorphous blobs and singular body parts, to humanoids and monstrous mutants. It is said to be the ultimate source of all miscreation and abomination in the world. The pool has many tentacles and limbs that grab and devour nearby creatures. The creature has a twisted and cynical mind, and can communicate telepathically with those nearby."
        #miscshape
        #gcost 160
        #size 6
        #hp 200
        #prot 12
        #mr 18
        #mor 30
        #str 18
        #att 14
        #def 0
        #prec 12
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 3 1
        #magicskill 6 1
        #magicskill 7 1
        #weapon 636 -- Life Drain Tentacle
        #weapon 636 -- Life Drain Tentacle
        #weapon 636 -- Life Drain Tentacle
        #weapon 636 -- Life Drain Tentacle
        #domsummon2 1576 -- Mad Hybrid
        #domsummon20 966 -- Formless Spawn
        #expertleader
        #immobile
        #blind
        #heal
        #diseaseres 100
        #spiritsight
        #regeneration 10
        #heal
        #poisonres 25
        #bluntres
        #neednoteat
        #amphibian
        #crossbreeder 10
        #bonusspells 1
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #end

        #newmonster 5006
        #name "Idol of the Harvest"
        #spr1 "./ExtraPretenders/HarvestIdol.tga"
        #spr2 "./ExtraPretenders/HarvestIdol.tga"
        #descr "The Idol of the Harvest is a primordial spirit of nature that once served a previous Pantokrator. When he saw how the spirit was venerated by the common folk he had it bound in a huge golden statue for eternity. With the Pantokrator gone, its powers have begun to manifest themselves and it is now worshipped as a reawakening god. The spirit cannot leave the statue, but it can possess willing targets in order to make its will heard and to perform tasks such as forging items for enchantment. The spirit is tremendously strong in its dominion. In a physical battle, the statue would be difficult to destroy, even though it cannot strike back."
        #miscshape
        #gcost 160
        #size 6
        #hp 150
        #prot 24
        #mr 18
        #mor 30
        #str 15
        #att 5
        #def 0
        #prec 12
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 2 1
        #magicskill 3 1
        #magicskill 6 1
        #gemprod 6 1
        #weapon 0
        #expertleader
        #immobile
        #blind
        #heal
        #diseaseres 100
        #spiritsight
        #inanimate
        #poisonres 25
        #fallpower 25
        #slashres
        #pierceres
        #neednoteat
        #amphibian
        #bonusspells 1
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homerealm 10 -- Default
        #end

        #newmonster 5007
        #name "Spirit of the Henge"
        #spr1 "./ExtraPretenders/HengeSpirit.tga"
        #spr2 "./ExtraPretenders/HengeSpirit.tga"
        #descr "The Spirit of the Henge is a powerful spirit that inhabits a series of huge standing stones. The spirit cannot leave the stones, but it can temporarily manifest a physical form in order to make its will heard and to perform tasks such as forging items for enchantment. The spirit is tremendously strong in its dominion and it is also magically powerful. In a physical battle, the stone would be difficult to destroy, even though it cannot strike back."
        #miscshape
        #gcost 170
        #size 6
        #hp 170
        #prot 22
        #mr 18
        #mor 30
        #str 15
        #att 5
        #def 0
        #prec 12
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 3 1
        #magicskill 4 1
        #magicskill 7 1
        #weapon 0
        #batstartsum1 931 -- Ivy King
        #expertleader
        #immobile
        #blind
        #heal
        #diseaseres 100
        #spiritsight
        #inanimate
        #poisonres 25
        #slashres
        #pierceres
        #neednoteat
        #amphibian
        #bonusspells 1
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homerealm 2 -- Celtic
        #end

        #newmonster 5008
        #name "Statue of the Outer Gods"
        #spr1 "./ExtraPretenders/CthulhuStatue.tga"
        #spr2 "./ExtraPretenders/CthulhuStatue.tga"
        #descr "The Statue of the Outer Gods is the spirit of an alien God that inhabits an ancient statue of unknowable origin. Brought to this world by the starspawns, the statue is constructed of a strange greenish stone with unearthly flecks and striations. The spirit cannot leave the statue, but it can mentally control its followers in order to make its will heard and to perform tasks such as forging items for enchantment. The spirit is tremendously strong in its dominion and it is also magically powerful. Mental emanations from the statue will attract strange creatures from the Void, as well as the weak willed as its dominion grows strong. The statue is deeply unsettling and although it cannot strike back in combat enemies may flee rather than face it."
        #miscshape
        #gcost 190
        #size 6
        #hp 160
        #prot 22
        #mr 18
        #mor 30
        #str 15
        #att 5
        #def 0
        #prec 12
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 2 1
        #magicskill 4 1
        #magicskill 5 1
        #weapon 0
        #fear 5
        #domsummon2 753 -- Thing from the Void
        #domsummon20 751 -- Thing that Should Not Be
        #goodleader
        #expertmagicleader
        #immobile
        #blind
        #heal
        #diseaseres 100
        #spiritsight
        #inanimate
        #poisonres 25
        #slashres
        #pierceres
        #neednoteat
        #amphibian
        #bonusspells 1
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #end

        #newmonster 5009
        #name "Ancient Anemone"
        #spr1 "./ExtraPretenders/Anemone.tga"
        #spr2 "./ExtraPretenders/Anemone.tga"
        #descr "The Ancient Anemone is a gigantic sea-dwelling being that possesses intelligence, magic and a very strong dominion. It has been worshipped since the dawn of time by the beings of the sea who provide offerings of fish. Countless offerings later and now with the Pantokrator gone, the Ancient Anemone has the chance to put the world under its strong dominion and becoming the True God. The Anemone cannot move around, but it is also tremendously difficult to kill in combat and can sting attackers with its poisonous tentacles."
        #miscshape
        #gcost 170
        #size 6
        #hp 250
        #prot 16
        #mr 18
        #mor 30
        #str 14
        #att 6
        #def 0
        #prec 12
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 0 2
        #magicskill 2 1
        #weapon 1843 -- Anemone Tentacle
        #weapon 1843 -- Anemone Tentacle
        #weapon 1843 -- Anemone Tentacle
        #weapon 1843 -- Anemone Tentacle
        #expertleader
        #immobile
        #blind
        #heal
        #diseaseres 100
        #spiritsight
        #poisonres 25
        #poisonarmor 5
        #slashres
        #pierceres
        #neednoteat
        #aquatic
        #bonusspells 1
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homerealm 9 -- Deeps
        #end

        #newmonster 5010
        #name "Living Reef"
        #spr1 "./ExtraPretenders/Coral.tga"
        #spr2 "./ExtraPretenders/Coral.tga"
        #descr "The Living Reef is a colony of tiny creatures that have grown over the millenia to become sentient and magically powerful. With the Pantokrator gone, the Living Reef can manifest its full power to become the True God. The reef is tremendously strong in its dominion although it cannot move. In a physical battle, the reef would be difficult to destroy, even though it cannot strike back. It is surrounded by shoals of fish that will aid its servants, and occasionally larger fish will appear from the depths."
        #miscshape
        #gcost 160
        #size 6
        #hp 200
        #prot 20
        #mr 18
        #mor 30
        #str 15
        #att 0
        #def 5
        #prec 0
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 2 1
        #magicskill 3 1
        #magicskill 6 1
        #domsummon 2858 -- Large Fish
        #weapon 0
        #expertleader
        #immobile
        #blind
        #heal
        #diseaseres 100
        #spiritsight
        #poisonres 25
        #slashres
        #pierceres
        #aquatic
        #bonusspells 1
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homerealm 9 -- Deeps
        #end

        #newmonster 5011
        #name "Dark Crystal"
        #spr1 "./ExtraPretenders/DarkCrystal.tga"
        #spr2 "./ExtraPretenders/DarkCrystal2.tga"
        #descr "The Dark Crystal is a great crystal worshipped by the Basalt Kings. Over the centuries it has gained in power and now has the chance to put the world under its strong dominion and becoming the True God. The Kings spend most of their time staring into the Dark Crystal  and in the pale light of the Basalt Kings' antennae, shades and movements can be seen that predict future events. The Crystal can control the minds of the weak willed and the Basalt Kings divine its will from the movements. In a physical battle, the crystal would be difficult to destroy, even though it cannot strike back. It is magically powerful and can reach further with spells of Earth and Astral magic."
        #miscshape
        #gcost 190
        #size 6
        #enc 0
        #hp 90
        #mor 30
        #mr 20
        #prot 18
        #ap 2
        #mapmove 0
        #itemslots 12288
        #magicskill 3 2
        #magicskill 4 2
        #bonusspells 1
        #maxage 5000
        #inanimate
        #blind
        #spiritsight
        #poisonres 25
        #fireres 5
        #coldres 5
        #str 15
        #att 5
        #def 5
        #ap 0
        #eyes 0
        #nobadevents 50
        #astralrange 2
        #earthrange 2
        #immobile
        #expertleader
        #expertmagicleader
        #weapon "Enslave mind"
        #pierceres
        #homerealm 0
        #slashres
        #startdom 4
        #pathcost 40
        #amphibian
        #end

        #newmonster 5012
        #name "Reliquary"
        #spr1 "./ExtraPretenders/Reliquary.tga"
        #spr2 "./ExtraPretenders/Reliquary2.tga"
        #descr "After the death of the Prophet his remains were kept in the temple of the Holy City of Eldregate and venerated as relics while the nation awaited the coming of the mysteryous New God he had promised. Replicas of the shrouds he wore were made and used by his most devoted followers to heal the sick and wounded, and faithful from all around the Empire made pilgrimages to to Eldregate to offer prayers and sacrifices to the Prophet's tomb. As the focus of such devotion the Reliquary gained great spiritual power. One day as many followers of the New Faith were praying it suddenly began to shine with celestial light. More and stronger miracles manifested, and it was clear a powerful spirit had bound itself to the remains. The Bishops were quick to declare the New God they had been waiting for had awakened, and it was the Prophet himself who had ascended to the Celestial sphere after his death to wait for the time when the faith of the people would be strong enough to call him back to the world, and make him into the God he had promised.
        Now Ermor follows the Second Coming of the Prophet, confident that he will lead the Empire to glory and dominion over the whole world."
        #miscshape
        #gcost 190
        #size 4
        #enc 0
        #hp 30
        #mor 30
        #mr 18
        #prot 15
        #autohealer 5
        #immobile
        #mapmove 0
        #magicskill 4 2
        #magicskill 6 1
        #magicbeing
        #bonusspells 1
        #maxage 2000
        #blind
        #heal
        #inanimate
        #diseaseres 100
        #spiritsight
        #poisonres 25
        #fireres 5
        #coldres 5
        #att 5
        #def 5
        #ap 2
        #eyes 0
        #expertleader
        #goodmagicleader
        #clearweapons
        #weapon 0
        #itemslots 12288 -- 2 misc
        #incunrest -50
        #pierceres
        #startdom 4
        #pathcost 40
        #nametype 106
        #end

        #newmonster 5013
        #name "Unholy Reliquary"
        #spr1 "./ExtraPretenders/Unholyreliquary.tga"
        #spr2 "./ExtraPretenders/Unholyreliquary2.tga"
        #descr "In Eldregate, capital of the Empire, unholy rites were performed using dark magic learned from C'tis.
        The goal was resurrect the Prophet, for many had come to believe that he would bring an age of wealth and prosperity where Ermor held ultimate dominion over the world.
        However the ritual failed catastrophically: there was no resurrection but rather the spiritual barriers between the worlds of the dead and the living were shattered and Death was brought forth, killing tens of thousands. 
        Now a powerful spirit of the Underworld is bound to the Prophet's tomb, turning the land to ash and desolation as its dominion spread.
        The Unholy Reliquary is surrounded by a terrifying life-draining aura and because of its close connection to the Underworld generates two Death gems every month."
        #miscshape
        #gcost 190
        #size 4
        #enc 0
        #hp 30
        #mor 30
        #mr 18
        #prot 15
        #itemslots 12288
        #goodleader
        #goodmagicleader
        #expertundeadleader
        #clearweapons
        #weapon "Soul leech"
        #undead
        #magicskill 5 3
        #magicskill 4 1
        #immobile
        #mapmove 0
        #maxage 2000
        #inanimate
        #blind
        #heal
        #diseaseres 100
        #spiritsight
        #bonusspells 1
        #poisonres 25
        #fireres 5
        #coldres 5
        #att 5
        #def 5
        #ap 2
        #onebattlespell "Soul vortex"
        #fear 10
        #eyes 0
        #gemprod 5 2
        #startdom 4
        #pathcost 40
        #nametype 106
        #end

        #newmonster 5014
        #name "Ancestral Barrow"
        #spr1 "./ExtraPretenders/Barrow.tga"
        #spr2 "./ExtraPretenders/Barrow.tga"
        #descr "The Ancestral Barrow is a long-dead ancestor and ruler who has been buried in a sacred mound and worshipped for ages by his descendants and tribespeople. The mound is hard to destroy, surrounded by aura of supernatural fear that frightens those who would defile it and guarded by lesser ancestral guardians. As its dominion grows, more ancestors will arrive to help the nation in war. The mound is completely immobile and cannot move even by magic."
        #miscshape
        #gcost 170
        #hp 120
        #size 6
        #prot 10
        #mr 18
        #mor 30
        #str 15
        #att 5
        #def 0
        #prec 10
        #enc 0
        #mapmove 0
        #immobile
        #eyes 0
        #ap 2
        #clearweapons
        #weapon 0
        #pathcost 50
        #startdom 4
        #inanimate
        #pierceres
        #bluntres
        #slashres
        #poisonres 25
        #fear 10
        #deathcurse
        #neednoteat
        #expertleader
        #magicskill 5 2
        #magicskill 3 1
        #bonusspells 1
        #stonebeing
        #itemslots 12288
        #goodundeadleader
        #blind
        #heal
        #diseaseres 100
        #spiritsight
        #pathcost 40
        #startdom 4
        #homerealm 2 -- Celtic
        #domsummon2 "Ancestral spirit"
        #domsummon20 566 -- Ghost
        #batstartsum2d6 "Ancestral spirit"
        #end

        #newmonster 5015
        #spr1 "./ExtraPretenders/Statuewisdom.tga"
        #spr2 "./ExtraPretenders/Statuewisdom.tga"
        #name "Statue of Wisdom"
        #descr "The Statue of Wisdom is a celestial spirit of wisdom that once served a previous Pantokrator and brought knowledge and enlightenment to the people of the world. Eventually however the Pantokrator began to fear that in their enlightenment the people might turn away from him, and thus bound the spirit to a huge stone statue for eternity. With the Pantokrator gone, its powers have begun to manifest themselves and it is now worshipped as a reawakening god. The spirit cannot leave the statue, but it can possess willing targets in order to make its will heard and to perform tasks such as forging items for enchantment. The spirit is tremendously strong in its dominion. In a physical battle, the statue would be difficult to destroy, even though it cannot strike back."
        #fixedname "Sophia"
        #miscshape
        #gcost 190
        #size 6
        #hp 160
        #prot 22
        #mr 18
        #mor 30
        #str 15
        #att 5
        #def 0
        #prec 12
        #enc 0
        #mapmove 0
        #ap 2
        #weapon 0
        #magicskill 4 3
        #inspiringres 1
        #researchbonus 20
        #expertleader
        #goodmagicleader
        #immobile
        #blind
        #heal
        #diseaseres 100
        #spiritsight
        #inanimate
        #poisonres 25
        #slashres
        #pierceres
        #neednoteat
        #amphibian
        #bonusspells 1
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homerealm 3 -- Mediterranean
        #end

        #newmonster 5016
        #name "Void Crystal"
        #spr1 "./ExtraPretenders/Voidcrystal.tga"
        #spr2 "./ExtraPretenders/Voidcrystal2.tga"
        #descr "The Void Crystal is an alien intelligence with extraordinary mental powers that has been worshipped for thousands of years by the Starspawn. It was part of the shard that brought them to this world, and resonates with the energies of the Void, making the province it is in more magical. The Void Crystal will always act as a communion master and it can project astral magic further than most mages."
        #miscshape
        #gcost 200
        #size 6
        #enc 0
        #hp 90
        #mor 30
        #mr 20
        #prot 18
        #immobile
        #ap 2
        #mapmove 0
        #itemslots 12288
        #magicskill 4 3
        #magicskill 3 1
        #commaster
        #decscale 5 -- +Magic
        #bonusspells 1
        #maxage 5000
        #inanimate
        #blind
        #heal
        #diseaseres 100
        #spiritsight
        #poisonres 25
        #fireres 5
        #coldres 5
        #str 15
        #att 5
        #def 5
        #ap 0
        #eyes 0
        #expertleader
        #expertmagicleader
        #clearweapons
        #weapon "Enslave mind"
        #weapon "Enslave mind"
        #weapon "Mind blast"
        #weapon "Mind blast"
        #voidsanity 20
        #pierceres
        #homerealm 0
        #slashres
        #startdom 4
        #pathcost 40
        #amphibian
        #astralrange 2
        #end

        #newmonster 5017
        #name "Mother of Pearls"
        #spr1 "./ExtraPretenders/Motherpearls.tga"
        #spr2 "./ExtraPretenders/Motherpearls2.tga"
        #descr "The Mother of Pearls is a powerful celestial spirit that inhabits a gigantic pearl clam. The tritons and mermen of the deeps have worshipped it for a long time as a guardian and provider of wealth, granting it a powerful dominion. The spirit cannot leave the clam, but it can possess willing targets in order to make its will heard and perform tasks such as forging items for enchantment. The Mother of Pearls can filter water and moonlight to produce great quantities of magical pearls."
        #miscshape
        #gcost 170
        #hp 100
        #size 6
        #prot 10
        #mr 18
        #mor 30
        #str 15
        #att 5
        #def 5
        #prec 5
        #enc 1
        #immobile
        #mapmove 0
        #aquatic
        #ap 2
        #eyes 0
        #blind
        #spiritsight
        #magicskill 2 1
        #magicskill 4 1
        #magicskill 6 1
        #gemprod 4 1
        #makepearls 7
        #startage 1000
        #maxage 5000
        #slashres
        #pierceres
        #goodleader
        #okmagicleader
        #digest 2
        #bonusspells 1
        #itemslots 12288
        #pathcost 40
        #startdom 4
        #female
        #weapon "Grab and Swallow"
        #homerealm 9 -- Deeps
        #end

        #newmonster 5018
        #name "Nuclear Chaos"
        #spr1 "./ExtraPretenders/NuclearChaosA.tga"
        #spr2 "./ExtraPretenders/NuclearChaosA.tga"
        #descr "The Nuclear Chaos is a powerful void being that has appeared through the Void Gate and is worshipped by the star spawns. Whilst initially weak and unable to move, the creature has the power to grow if left unchecked and will gain in power, mobility and strength over time, or through exertion. However, as the being becomes more powerful it will lash out and become more unpredictable. Eventually the creature may turn on its followers or simply return to the void. The star spawns have certain rituals that are performed at the Void Gate that can reign in the creature, and these must be performed periodically to prevent the creature from rampaging across the entire world. Whilst at the Void Gate Elder Things will emerge to serve the Nuclear Chaos each month."
        #fixedname "Azathoth"
        #miscshape
        #gcost 200
        #size 2
        #hp 30
        #prot 12
        #mr 18
        #mor 30
        #str 15
        #att 5
        #def 0
        #prec 12
        #enc 1
        #mapmove 0
        #ap 2
        #magicskill 4 2
        #magicskill 5 1
        #weapon 284 -- Steal Strength
        #summon1 752 -- Elder Thing
        #okleader
        #goodmagicleader
        #immobile
        #blind
        #spiritsight
        #neednoteat
        #amphibian
        #heal
        #diseaseres 100
        #voidsanity 20
        #bonusspells 1
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #xpshape 3
        #end

        #newmonster 5019
        #name "Nuclear Chaos"
        #spr1 "./ExtraPretenders/NuclearChaosB.tga"
        #spr2 "./ExtraPretenders/NuclearChaosB2.tga"
        #descr "The Nuclear Chaos is a powerful void being that has appeared through the Void Gate and is worshipped by the star spawns. Whilst initially weak and unable to move, the creature has the power to grow if left unchecked and will gain in power, mobility and strength over time, or through exertion. However, as the being becomes more powerful it will lash out and become more unpredictable. Eventually the creature may turn on its followers or simply return to the void. The star spawns have certain rituals that are performed at the Void Gate that can reign in the creature, and these must be performed periodically to prevent the creature from rampaging across the entire world. Whilst at the Void Gate Elder Things will emerge to serve the Nuclear Chaos when its Dominion grows strong."
        #miscshape
        #gcost 200
        #size 4
        #hp 68
        #prot 14
        #mr 18
        #mor 30
        #str 20
        #att 12
        #def 0
        #prec 12
        #enc 1
        #mapmove 0
        #ap 2
        #magicskill 4 2
        #magicskill 5 1
        #weapon 271 -- Life Drain Tentacle
        #weapon 85 -- Tentacle
        #summon1 752 -- Elder Thing
        #okleader
        #goodmagicleader
        #immobile
        #blind
        #spiritsight
        #neednoteat
        #amphibian
        #heal
        #diseaseres 100
        #voidsanity 20
        #bonusspells 1
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #xpshape 6
        #end

        #newmonster 5020
        #name "Nuclear Chaos"
        #spr1 "./ExtraPretenders/NuclearChaosC.tga"
        #spr2 "./ExtraPretenders/NuclearChaosC2.tga"
        #descr "The Nuclear Chaos is a powerful void being that has appeared through the Void Gate and is worshipped by the star spawns. Whilst initially weak and unable to move, the creature has the power to grow if left unchecked and will gain in power, mobility and strength over time, or through exertion. However, as the being becomes more powerful it will lash out and become more unpredictable. Eventually the creature may turn on its followers or simply return to the void. The star spawns have certain rituals that are performed at the Void Gate that can reign in the creature, and these must be performed periodically to prevent the creature from rampaging across the entire world. Whilst at the Void Gate Elder Things will emerge to serve the Nuclear Chaos when its Dominion grows strong."
        #miscshape
        #gcost 200
        #size 4
        #hp 68
        #prot 16
        #mr 18
        #mor 30
        #str 20
        #att 12
        #def 10
        #prec 12
        #enc 1
        #mapmove 3
        #ap 16
        #magicskill 4 2
        #magicskill 5 1
        #weapon 271 -- Life Drain Tentacle
        #weapon 85 -- Tentacle
        #weapon 85 -- Tentacle
        #summon1 752 -- Elder Thing
        #okleader
        #goodmagicleader
        #float
        #blind
        #spiritsight
        #neednoteat
        #amphibian
        #heal
        #diseaseres 100
        #voidsanity 20
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #foreignshape 5021
        #end

        #newmonster 5021
        #name "Nuclear Chaos"
        #spr1 "./ExtraPretenders/NuclearChaosC.tga"
        #spr2 "./ExtraPretenders/NuclearChaosC2.tga"
        #descr "The Nuclear Chaos is a powerful void being that has appeared through the Void Gate and is worshipped by the star spawns. Whilst initially weak and unable to move, the creature has the power to grow if left unchecked and will gain in power, mobility and strength over time, or through exertion. However, as the being becomes more powerful it will lash out and become more unpredictable. Eventually the creature may turn on its followers or simply return to the void. The star spawns have certain rituals that are performed at the Void Gate that can reign in the creature, and these must be performed periodically to prevent the creature from rampaging across the entire world. Whilst at the Void Gate Elder Things will emerge to serve the Nuclear Chaos when its Dominion grows strong."
        #miscshape
        #gcost 200
        #size 4
        #hp 68
        #prot 16
        #mr 18
        #mor 30
        #str 20
        #att 12
        #def 10
        #prec 12
        #enc 1
        #mapmove 3
        #ap 16
        #magicskill 4 2
        #magicskill 5 1
        #weapon 271 -- Life Drain Tentacle
        #weapon 85 -- Tentacle
        #weapon 85 -- Tentacle
        #okleader
        #goodmagicleader
        #float
        #blind
        #spiritsight
        #neednoteat
        #amphibian
        #heal
        #diseaseres 100
        #voidsanity 20
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homeshape 5020
        #end

        #newmonster 5022
        #name "Nuclear Chaos"
        #spr1 "./ExtraPretenders/NuclearChaosD.tga"
        #spr2 "./ExtraPretenders/NuclearChaosD2.tga"
        #descr "The Nuclear Chaos is a powerful void being that has appeared through the Void Gate and is worshipped by the star spawns. Whilst initially weak and unable to move, the creature has the power to grow if left unchecked and will gain in power, mobility and strength over time, or through exertion. However, as the being becomes more powerful it will lash out and become more unpredictable. Eventually the creature may turn on its followers or simply return to the void. The star spawns have certain rituals that are performed at the Void Gate that can reign in the creature, and these must be performed periodically to prevent the creature from rampaging across the entire world. Whilst at the Void Gate Elder Things will emerge to serve the Nuclear Chaos when its Dominion grows strong."
        #miscshape
        #gcost 200
        #size 6
        #hp 118
        #prot 18
        #mr 20
        #mor 30
        #str 25
        #att 12
        #def 8
        #prec 12
        #enc 1
        #mapmove 26
        #ap 16
        #magicskill 4 2
        #magicskill 5 1
        #weapon 271 -- Life Drain Tentacle
        #weapon 271 -- Life Drain Tentacle
        #weapon 85 -- Tentacle
        #weapon 85 -- Tentacle
        #okleader
        #goodmagicleader
        #float
        #blind
        #spiritsight
        #neednoteat
        #amphibian
        #heal
        #diseaseres 100
        #voidsanity 20
        #fear 5
        #insane 5
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homeshape 5020
        #end

        #newmonster 5023
        #name "Nuclear Chaos"
        #spr1 "./ExtraPretenders/NuclearChaosD.tga"
        #spr2 "./ExtraPretenders/NuclearChaosD2.tga"
        #descr "The Nuclear Chaos is a powerful void being that has appeared through the Void Gate and is worshipped by the star spawns. Whilst initially weak and unable to move, the creature has the power to grow if left unchecked and will gain in power, mobility and strength over time, or through exertion. However, as the being becomes more powerful it will lash out and become more unpredictable. Eventually the creature may turn on its followers or simply return to the void. The star spawns have certain rituals that are performed at the Void Gate that can reign in the creature, and these must be performed periodically to prevent the creature from rampaging across the entire world. Whilst at the Void Gate Elder Things will emerge to serve the Nuclear Chaos when its Dominion grows strong."
        #miscshape
        #gcost 200
        #size 6
        #hp 128
        #prot 18
        #mr 20
        #mor 30
        #str 26
        #att 12
        #def 8
        #prec 12
        #enc 1
        #mapmove 26
        #ap 16
        #magicskill 4 2
        #magicskill 5 1
        #weapon 271 -- Life Drain Tentacle
        #weapon 271 -- Life Drain Tentacle
        #weapon 85 -- Tentacle
        #weapon 85 -- Tentacle
        #okleader
        #goodmagicleader
        #float
        #blind
        #spiritsight
        #neednoteat
        #amphibian
        #heal
        #diseaseres 100
        #voidsanity 20
        #fear 5
        #insane 15
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homeshape 5020
        #end

        #newmonster 5030
        #name "Nuclear Chaos"
        #spr1 "./ExtraPretenders/NuclearChaosD.tga"
        #spr2 "./ExtraPretenders/NuclearChaosD2.tga"
        #descr "The Nuclear Chaos is a powerful void being that has appeared through the Void Gate and is worshipped by the star spawns. Whilst initially weak and unable to move, the creature has the power to grow if left unchecked and will gain in power, mobility and strength over time, or through exertion. However, as the being becomes more powerful it will lash out and become more unpredictable. Eventually the creature may turn on its followers or simply return to the void. The star spawns have certain rituals that are performed at the Void Gate that can reign in the creature, and these must be performed periodically to prevent the creature from rampaging across the entire world. Whilst at the Void Gate Elder Things will emerge to serve the Nuclear Chaos when its Dominion grows strong."
        #miscshape
        #gcost 200
        #size 6
        #hp 138
        #prot 18
        #mr 22
        #mor 30
        #str 27
        #att 12
        #def 8
        #prec 12
        #enc 1
        #mapmove 26
        #ap 16
        #magicskill 4 2
        #magicskill 5 1
        #weapon 271 -- Life Drain Tentacle
        #weapon 271 -- Life Drain Tentacle
        #weapon 85 -- Tentacle
        #weapon 85 -- Tentacle
        #okleader
        #goodmagicleader
        #float
        #blind
        #spiritsight
        #neednoteat
        #amphibian
        #heal
        #diseaseres 100
        #voidsanity 20
        #fear 10
        #insane 25
        #defector 5
        #deserter 5
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homeshape 5020
        #end

        #newmonster 5029
        #name "Sacred Grove"
        #spr1 "./ExtraPretenders/SacredGrove2.tga"
        #spr2 "./ExtraPretenders/SacredGrove2.tga"
        #descr "The Sacred Grove is a powerful spirit that inhabits a lush grove of great importance. The spirit cannot leave the grove, but it can animate plants and inhabit animals in order to make its will heard and to perform tasks such as forging items for enchantment. The spirit is tremendously strong in its dominion and it is also magically powerful. In a physical battle the grove would be difficult to destroy, and is protected by animals it has called. The grove is completely immobile and cannot move even by magic."
        #djinn
        #gcost 150
        #size 6
        #hp 180
        #prot 12
        #mr 18
        #mor 30
        #str 15
        #att 5
        #def 0
        #prec 12
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 2 1
        #magicskill 6 2
        #gemprod 6 1
        #weapon 0
        #batstartsum1d6 2398 -- Elephant
        #batstartsum2d6 628 -- Lion
        #batstartsum3d6 1140 -- Tiger
        #batstartsum4d6 1705 -- Great Ape
        #expertleader
        #immobile
        #blind
        #spiritsight
        #inanimate
        #heal
        #diseaseres 100
        #poisonres 25
        #neednoteat
        #bonusspells 1
        #unteleportable
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homerealm 7 -- Africa
        #end

        #newmonster 5031
        #copystats 2465 -- Statue of War
        #name "Statue of War"
        #spr1 "./ExtraPretenders/HinduWarStatue.tga"
        #spr2 "./ExtraPretenders/HinduWarStatue.tga"
        #descr "The Statue of War is a primordial spirit of strife and turmoil once bound in a huge statue by a previous Pantokrator to save the world from its destructive influences. With the Pantokrator gone, its powers are unleashed and strife and war have once more come to the world. Barbarian warriors gather in the land of the statue to wreak havoc and plunder. More barbarians arrive if the turmoil of the province is high. The spirit cannot leave the statue, but it can possess willing targets in order to make its will heard and to perform tasks such as forging items for enchantment. The spirit is tremendously strong in its Dominion. In a physical battle, the statue would be difficult to destroy, even though it cannot strike back."
        #miscshape
        #gcost 150
        #heal
        #diseaseres 100
        #homerealm 8 -- India
        #end

        #newmonster 5032
        #copystats 5015 -- Statue of Wisdom
        #name "Statue of Wisdom"
        #spr1 "./ExtraPretenders/GaneshaStatue.tga"
        #spr2 "./ExtraPretenders/GaneshaStatue.tga"
        #descr "The Statue of Wisdom is a celestial spirit of wisdom that once served a previous Pantokrator and brought knowledge and enlightenment to the people of the world. Eventually however the Pantokrator began to fear that in their enlightenment the people might turn away from him, and thus bound the spirit to a huge statue for eternity. With the Pantokrator gone, its powers have begun to manifest themselves and it is now worshipped as a reawakening god. The spirit cannot leave the statue, but it can possess willing targets in order to make its will heard and to perform tasks such as forging items for enchantment. The spirit is tremendously strong in its dominion. In a physical battle, the statue would be difficult to destroy, even though it cannot strike back."
        #miscshape
        #homerealm 8 -- India
        #end

        #newmonster 5033
        #copystats 958 -- Colossal Head
        #clearmagic
        #name "Smiling Head"
        #spr1 "./ExtraPretenders/SmilingHead.tga"
        #spr2 "./ExtraPretenders/SmilingHead.tga"
        #descr "The Smiling Head is a powerful spirit that inhabits a massive stone that has been chiseled into the shape of a head. The spirit cannot leave the Head, but it can possess willing targets in order to make its will heard and to perform tasks such as forging items for enchantment. The presence of the spirit will bring magic and joy to the world. The spirit is tremendously strong in its Dominion and it is also magically powerful. In a physical battle, the head would be difficult to destroy, even though it cannot strike back."
        #gcost 180
        #miscshape
        #magicskill 3 2
        #magicskill 4 1
        #douse 0
        #decscale 5 -- +Magic
        #homerealm 8 -- India
        #end

        #newmonster 5034
        #copystats 2460 -- Statue of Fertility
        #name "Statue of Creation"
        #spr1 "./ExtraPretenders/HinduStatueOrder.tga"
        #spr2 "./ExtraPretenders/HinduStatueOrder.tga"
        #descr "The Statue of Creation is a primordial spirit of creation that once served a previous Pantokrator to aid in the creation of the world. When the final piece was in place, its services were no longer needed and the spirit was bound in a huge statue for eternity. With the Pantokrator gone, its powers have begun to manifest themselves and it is now worshipped as a reawakening god. The spirit cannot leave the statue, but it can possess willing targets in order to make its will heard and to perform tasks such as forging items for enchantment. The spirit is tremendously strong in its Dominion. In a physical battle, the statue would be difficult to destroy, even though it cannot strike back."
        #homerealm 8 -- India
        #end

        #newmonster 5035
        #copystats 2447 -- Idol of Men
        #name "Shanta Murti"
        #spr1 "./ExtraPretenders/HinduIdol.tga"
        #spr2 "./ExtraPretenders/HinduIdol.tga"
        #descr "The Shanta Murti is a representation of the divine crafted in ages past. Worshipping at the idol is said to bring wealth, health and good fortune and it has been around for a very long time. Through the ages farmers have offered it food when their animals got sick and people in general have made a yearly offering to ensure wealth and happiness. Countless offerings later and now with the Pantokrator gone, the Shanta Murti has the chance of putting the world under its strong dominion and becoming the True God."
        #homerealm 8 -- India
        #eyes 2
        #miscshape
        #end

        #newmonster 5036
        #clearmagic
        #spr1 "./ExtraPretenders/HangingTree.tga"
        #spr2 "./ExtraPretenders/HangingTree.tga"
        #name "Hanging Tree"
        #descr "The Hanging Tree is a powerful spirit that inhabits a great dead tree. For generations the tree has been used to hang the worst criminals, and it is surrounded by the stench of the dead and dying. The spirit cannot leave the tree, but it can animate the corpses of the hanged to defend it from attack, or in order to perform tasks such as forging items for enchantment. The spirit is tremendously strong in its Dominion and it is also magically powerful. Draugr will sometimes rise from their mounds in the vicinity of the tree to serve it. In a physical battle, the tree would be difficult to destroy, and it will animate Draugr to defend itself."
        #miscshape
        #gcost 150
        #size 6
        #hp 150
        #prot 15
        #mr 18
        #mor 30
        #str 14
        #att 5
        #def 0
        #prec 10
        #enc 0
        #mapmove 0
        #ap 2
        #okleader 
        #superiorundeadleader
        #immobile
        #bluntres
        #pierceres
        #inanimate
        #neednoteat
        #blind
        #heal
        #diseaseres 100
        #spiritsight
        #startage 3000
        #maxage 5000
        #fireres -5
        #poisonres 20
        #bonusspells 1
        #fear 10
        #magicskill 1 1
        #magicskill 5 2
        #batstartsum3 2190 -- Draugr
        #domsummon20 2190 -- Draugr
        #itemslots 61440 -- 4 misc
        #startdom 4
        #pathcost 40
        #homerealm 1 -- North
        #end

        #newmonster 5042
        #name "Idol of Thunder"
        #spr1 "./ExtraPretenders/ThunderIdol.tga"
        #spr2 "./ExtraPretenders/ThunderIdol.tga"
        #descr "This Idol has been around for a very long time and it is always surrounded by a great storm. It has been struck by lightning many times but has never been marked by the blasts. Through the ages priests and shamans have left offerings to the sky and the earth and the Idol of Thunder has continued to grow in power. Countless offerings later and now with the Pantokrator gone, the Idol of Thunder has the chance to put the world under its strong dominion and become the True God. The idol cannot move, but it can possess willing targets in order to make its will heard and to perform tasks such as forging items for enchantment. The spirit is knowledgeable in the arts of the forge and will use less gems when creating magical items."
        #fixedname "Horagalles"
        #miscshape
        #gcost 170
        #size 6
        #hp 120
        #prot 20
        #mr 18
        #mor 30
        #str 15
        #att 5
        #def 0
        #prec 12
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 1 2
        #magicskill 3 1
        #weapon 0
        #expertleader
        #immobile
        #blind
        #heal
        #diseaseres 100
        #spiritsight
        #inanimate
        #stormimmune
        #poisonres 25
        #shockres 25
        #onebattlespell 545 -- Storm
        #fixforgebonus 2
        #bluntres
        #pierceres
        #neednoteat
        #amphibian
        #bonusspells 1
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homerealm 1 -- North
        #end

        #newmonster 5043
        #name "Idol of War"
        #spr1 "./ExtraPretenders/WarIdol.tga"
        #spr2 "./ExtraPretenders/WarIdol.tga"
        #descr "This Idol has been around for a very long time and it has always been the most popular place to give your offerings. Through the ages warriors and generals have offered it valuables to receive courage and strength in battle and the Idol of War has continued to grow in power. Barbarian warriors gather in the land of the idol to wreak havoc and plunder. More barbarians arrive as the dominion of the idol grows in strength. Countless offerings later and now with the Pantokrator gone, the Idol of War has the chance of putting the world under its strong dominion and becoming the True God"
        #fixedname "Tyr"
        #miscshape
        #gcost 160
        #size 6
        #hp 120
        #prot 20
        #mr 18
        #mor 30
        #str 15
        #att 5
        #def 0
        #prec 12
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 0 2
        #magicskill 3 1
        #weapon 0
        #expertleader
        #immobile
        #blind
        #heal
        #diseaseres 100
        #spiritsight
        #inanimate
        #poisonres 25
        #domsummon2 1154 -- Forest Warrior
        #bluntres
        #pierceres
        #neednoteat
        #amphibian
        #bonusspells 1
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homerealm 1 -- North
        #end

        #newmonster 5044
        #copystats 2460 -- Statue of Fertility
        #name "Icon of Fertility"
        #spr1 "./ExtraPretenders/FertIcon.tga"
        #spr2 "./ExtraPretenders/FertIcon.tga"
        #descr "The Icon of Fertility is a primordial spirit of fertility that once served a previous Pantokrator and brought life to the world. When the creation was full of life, its services were no longer needed and the spirit was bound in a huge statue for eternity. With the Pantokrator gone, its powers have begun to manifest themselves and it is now worshipped as a reawakening god. The spirit cannot leave the statue, but it can possess willing targets in order to make its will heard and to perform tasks such as forging items for enchantment. The spirit is tremendously strong in its Dominion. In a physical battle, the statue would be difficult to destroy, even though it cannot strike back."
        #miscshape
        #size 6
        #homerealm 1 -- North
        #end

        #newmonster 5045
        #copystats 472 -- Statue of Order
        #name "Daibutsu"
        #spr1 "./ExtraPretenders/Daibutsu.tga"
        #spr2 "./ExtraPretenders/Daibutsu.tga"
        #descr "The Daibutsu is a primordial spirit of order that once served a previous Pantokrator to bring civilization and order to the world. When the final decree was set, its services were no longer needed and the spirit was bound in a huge stone statue for eternity. With the Pantokrator gone, its powers have begun to manifest themselves and it is now worshipped as a reawakening god. The spirit cannot leave the statue, but it can possess willing targets in order to make its will heard and to perform tasks such as forging items for enchantment. The spirit is tremendously strong in its Dominion. In a physical battle, the Daibutsu would be difficult to destroy, even though it cannot strike back."
        #miscshape
        #size 6
        #hp 150
        #homerealm 4 -- Far East
        #end

        #newmonster 5046
        #name "Statue of Mercy"
        #spr1 "./ExtraPretenders/GuanYinStatue.tga"
        #spr2 "./ExtraPretenders/GuanYinStatue.tga"
        #descr "The Statue of Mercy is a primordial spirit of compassion and forgiveness that once served a previous Pantokrator. When the Pantokrator banished a wayward servant the spirit petitioned for forgiveness, and for its impudence was bound in a huge statue for eternity. With the Pantokrator gone, its powers have begun to manifest themselves and it is now worshipped as a reawakening god. The spirit cannot leave the statue, but it can possess willing targets in order to make its will heard and to perform tasks such as forging items for enchantment. The spirit is tremendously strong in its dominion and although it cannot strike back it would be very hard to damage in a fight."
        #miscshape
        #gcost 170
        #size 6
        #hp 120
        #prot 26
        #mr 18
        #mor 30
        #str 15
        #att 5
        #def 0
        #prec 12
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 4 1
        #magicskill 6 2
        #weapon 0
        #expertleader
        #immobile
        #blind
        #heal
        #diseaseres 100
        #spiritsight
        #inanimate
        #poisonres 25
        #bluntres
        #pierceres
        #neednoteat
        #amphibian
        #bonusspells 1
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homerealm 4 -- Far East
        #end

        #newmonster 5047
        #copystats 2449 -- Idol of Sorcery
        #clearmagic
        #name "Spirit Gate"
        #spr1 "./ExtraPretenders/ShintoGate.tga"
        #spr2 "./ExtraPretenders/ShintoGate.tga"
        #descr "The Spirit Gate has been the site of many strange occurences over the years and Sorcerers always make a sacrifice at the gate each full moon. During this time the gate is attuned to the underworld and glimpses of the spirit realm can be seen through it. Through the ages the Spirit Gate has accumulated a huge amount of offerings and it has grown in power so it can rival the mightiest of arch mages. Now with the Pantokrator gone, the Gate has the chance to put the world under its strong dominion and become the True God. Spirits will flock through the gate as its dominion grows stronger, and anyone who dares to strike at it in combat will be permanently cursed."
        #miscshape
        #gcost 190
        #magicskill 5 3
        #magicskill 4 1
        #domsummon2 674 -- Dispossessed spirit
        #domsummon20 1256 -- Shura
        #homerealm 0
        #end

        #newmonster 5056
        #name "Spirit of Sargassum"
        #spr1 "./ExtraPretenders/Sargassum.tga"
        #spr2 "./ExtraPretenders/Sargassum.tga"
        #descr "The Spirit of Sargassum is a powerful nature spirit that inhabits a huge mass of kelp. It often ensnares passing ships in its strands and keeps them trapped for months on end. It has been worshipped since the dawn of time by the beings of the sea who provide offerings to the spirit. Countless offerings later and now with the Pantokrator gone, the Sargassum has the chance to put the world under its strong dominion and becoming the True God. The kelp cannot move around, but it is also tremendously difficult to kill in combat and can grasp attackers with many strands of kelp."
        #miscshape
        #gcost 150
        #size 6
        #hp 250
        #prot 16
        #mr 18
        #mor 30
        #str 14
        #att 6
        #def 0
        #prec 12
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 6 3
        #weapon 85 -- Tentacle
        #weapon 85 -- Tentacle
        #weapon 85 -- Tentacle
        #weapon 85 -- Tentacle
        #expertleader
        #immobile
        #blind
        #heal
        #diseaseres 100
        #spiritsight
        #poisonres 10
        #bluntres
        #pierceres
        #neednoteat
        #aquatic
        #bonusspells 1
        #itemslots 12288 -- 2 misc
        #maxage 5000
        #startage 1000
        #startdom 4
        #pathcost 40
        #homerealm 9 -- Deeps
        #end

        #newmonster 5062
        #spr1 "./ExtraPretenders/AnvilGod.tga"
        #spr2 "./ExtraPretenders/AnvilGod2.tga"
        #name "Divine Anvil"
        #descr "The Divine Anvil is a primordial spirit of creation bound into a large anvil that once taught the people of the world the secret of ironworking. When the Pantokrator saw the smoke from the forges of men he flew into a rage and sealed the Divine Anvil away in the deepest reaches of the earth. Now with the Pantokrator gone, its powers have begun to manifest themselves once more. The spirit cannot leave the anvil, but it can possess willing targets in order to make its will heard and to perform tasks such as forging items for enchantment. Items created using the Anvil require less gems than usual due to the Divine spirit. The spirit is tremendously strong in its dominion. In a physical battle the anvil would be difficult to destroy, even though it cannot strike back."
        #miscshape
        #gcost 170
        #hp 200
        #size 6
        #prot 25
        #mr 18
        #mor 30
        #str 15
        #att 5
        #def 0
        #prec 5
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 0 2
        #magicskill 3 1
        #weapon 0
        #forgebonus 35
        #startage 1000
        #maxage 2000
        #fireres 20
        #poisonres 25
        #heal
        #heat 6
        #immobile
        #inanimate
        #blind
        #neednoteat
        #amphibian
        #pierceres
        #slashres
        #diseaseres 100
        #spiritsight
        #goodleader
        #okmagicleader
        #bonusspells 1
        #pathcost 40
        #startdom 4
        #end

        #newmonster 5098
        #spr1 "./ExtraPretenders/StoneHead.tga"
        #spr2 "./ExtraPretenders/StoneHead.tga"
        #name "Stone Tyrant"
        #descr "The Stone Tyrant is a massive stone that has been chiseled into a shape resembling a Phlegran Tyrant. The people of Phlegra have always left offerings to the edifice to ensure protection from the depredations of the Tyrants. Through the ages the Stone Tyrant has accumulated a huge amount of offerings and it has grown in power so it can rival the mightiest of arch mages. Now with the Pantokrator gone, the Stone Tyrant has the chance of putting the world under its strong dominion and becoming the True God. The spirit cannot leave the chiseled stone, but it can possess willing targets in order to make its will heard and to perform tasks such as forging items for enchantment. The spirit is tremendously strong in its Dominion and will strike terror into the hearts of its enemies. In a physical battle, the head would be difficult to destroy, even though it cannot strike back."
        #miscshape
        #gcost 170
        #hp 300
        #size 6
        #prot 25
        #mr 18
        #mor 30
        #str 15
        #att 5
        #def 0
        #prec 5
        #enc 0
        #mapmove 0
        #ap 2
        #magicskill 0 2
        #magicskill 3 2
        #weapon 0
        #startage 1000
        #maxage 2000
        #immobile
        #inanimate
        #neednoteat
        #amphibian
        #pierceres
        #slashres
        #fear 5
        #diseaseres 100
        #stonebeing
        #spiritsight
        #goodleader
        #okmagicleader
        #bonusspells 1
        #pathcost 40
        #startdom 4
        #end
    -- IMMORTALS  


        #newmonster 5024
        #copystats 2009 -- Rephaite Commander
        #clearweapons
        #cleararmor
        #spr1 "./ExtraPretenders/EERephLich.tga"
        #spr2 "./ExtraPretenders/EERephLich2.tga"
        #name "Rephaite Lich Lord"
        #descr "The Rephaite Lich Lord is a Rephaite King that has transformed himself into an undead creature of great power. Since the Melqarts left the earth and were trapped in Sheol, the Nephilim-worship transformed into ancestor-worship. As the deified dead became central to the cult of the Rephaim, some chose to extend their life indefinitely with the foul ritual of lichcraft. Having died and visited Sheol the Rephaite Lich Lord has returned from the realm of the Gods and now intends to rule over the realm of the living. Should the body of the Lich Lord  be physically destroyed, a new one is formed from the dust of the dead."
        #humanoid
        #gcost 180
        #hp 65
        #prot 8
        #mr 18
        #mor 30
        #enc 0
        #mapmove 3
        #ap 10
        #maxage 1000
        #fireres 0
        #coldres 15
        #poisonres 25
        #supplybonus 0
        #magicskill 5 1
        #magicskill 7 1
        #nametype 149 -- Hinnom
        #weapon 172 -- Magic Sceptre
        #okleader
        #goodundeadleader
        #undead
        #heal
        #diseaseres 100
        #immortal
        #pooramphibian
        #pierceres
        #inanimate
        #neednoteat
        #startdom 2
        #pathcost 40
        #end

        #newmonster 5025
        #copystats 2217
        #clearmagic
        #spr1 "./ExtraPretenders/Wormlord.tga"
        #spr2 "./ExtraPretenders/Wormlord2.tga"
        #name "Worm Lord"
        #descr "Potent nature mages do not die easily, and even death might not stop the more powerful ones. When the worms and bugs that feed of carrion devour the corpse of a dead Nature mage, some of the mages manage to transfer a portion of their mind and power into the crawling mass feeding on their remains. This being houses the life force of such a mage and it is formed by thousands of worms and bugs. Should it be slain in combat it can usually transfer its life force to other nearby bugs and shape a new body capable of casting spells.
        The Worm Lord has learnt all of magic that it can and now there is nothing left but to take the role of a Pretender God."
        #humanoid
        #gcost 200
        #startdom 2
        #pathcost 40
        #magicskill 3 1
        #magicskill 5 1
        #magicskill 6 1
        #pathcost 40
        #okleader
        #heal
        #diseaseres 100
        #okundeadleader
        #str 14
        #hp 15
        #maxage 1000
        #watershape 4908
        #end

        #newmonster 4908
        #copystats 2975
        #copyspr 2975
        #clearmagic
        #name "Worm Lord"
        #descr "Potent nature mages do not die easily, and even death might not stop the more powerful ones. When the worms and bugs that feed of carrion devour the corpse of a dead Nature mage, some of the mages manage to transfer a portion of their mind and power into the crawling mass feeding on their remains. This being houses the life force of such a mage and it is formed by thousands of worms and bugs. Should it be slain in combat it can usually transfer its life force to other nearby bugs and shape a new body capable of casting spells.
        The Worm Lord has learnt all of magic that it can and now there is nothing left but to take the role of a Pretender God."
        #humanoid
        #gcost 200
        #startdom 2
        #pathcost 40
        #magicskill 3 1
        #magicskill 5 1
        #magicskill 6 1
        #pathcost 40
        #okleader
        #heal
        #diseaseres 100
        #okundeadleader
        #str 14
        #hp 15
        #maxage 1000
        #landshape 5025
        #end

        #newmonster 5026
        #name "Heavenly Sage"
        #spr1 "./ExtraPretenders/Heavensage.tga"
        #spr2 "./ExtraPretenders/Heavensage2.tga"
        #descr "The Heavenly Sage is a legendary Master of the Way, hailed as the greatest sage in the earthly realm. He achieved immortality and ascended to Heaven, but returned to the material world to take the role of a Pretender God and lead his people to enlightenment. If his physical body is destroyed he will reappear in the Celestial realm and descend to the capital once more."
        #humanoid
        #gcost 170
        #maxage 1000
        #size 2
        #ap 12
        #mr 18
        #hp 10
        #mor 30
        #str 10
        #att 10
        #def 10
        #prec 12
        #mapmove 3
        #immortal
        #enc 3
        #magicskill 2 1
        #magicskill 4 1
        #neednoteat
        #flying
        #heal
        #diseaseres 100
        #weapon "Fly whisk"
        #armor "Imperial robes"
        #goodleader
        #nametype 121
        #homerealm 4 -- Far East
        #pathcost 40
        #startdom 2
        #fixedname "Lu Dongbin"
        #end

        #newmonster 5998
        #copystats 3009 -- Buffalo
        #copyspr 3177 -- Red Cattle
        #name "Divine Cattle"
        #descr "These Divine cattle are part of the herd of the Duiu of Farming"
        #end

        #newmonster 5999
        #copystats 566 -- Ghost
        #spr1 "./ExtraPretenders/EEGhostReph.tga"
        #spr2 "./ExtraPretenders/EEGhostReph2.tga"
        #name "Ghostly Rephaite"
        #descr "These are the ghosts of long dead Rephaites summoned from Sheol. Ghosts are frightening ethereal beings that can drain the life force from living beings. The spirits of Rephaites are revered by the giants and are sacred."
        #size 4
        #hp 50
        #str 18
        #cold 6
        #holy
        #wastesurvival
        #magicskill 5 1
        #nametype 149 -- Rephaim
        #end



        --- DOM 4 ---

        #selectmonster 158 -- Oracle
        #gcost 170
        #heal
        #diseaseres 100
        #end

        #selectmonster 159 -- Monument
        #gcost 150
        #heal
        #diseaseres 100
        #end

        #selectmonster 472 -- Statue of Order
        #gcost 170
        #heal
        #diseaseres 100
        #end

        #selectmonster 546 -- Void Lurker
        #gcost 150
        #heal
        #diseaseres 100
        #end

        #selectmonster 607 -- Baphomet
        #gcost 190
        #heal
        #diseaseres 100
        #end

        #selectmonster 656 -- Fountain of Blood
        #gcost 140
        #heal
        #diseaseres 100
        #end

        #selectmonster 657 -- Monolith
        #gcost 170
        #heal
        #diseaseres 100
        #end

        #selectmonster 958 -- Colossal Head
        #gcost 150
        #heal
        #diseaseres 100
        #end

        #selectmonster 1025 -- Divine Glyph
        #gcost 160
        #heal
        #diseaseres 100
        #end

        #selectmonster 1346 -- Colossal Fetish
        #descr "The Colossal Fetish is a statue that has come to life from being worshipped for many hundreds of years. It is mindless and mostly does what its worshippers believe it would do. It punishes harshly and rewards sparingly. The Colossal Fetish is made of wood and would be difficult to destroy in combat. In combat the statue will manifest gems of Earth and Nature magic to aid in spellcasting."
        #gcost 260
        #mr 20
        #pooramphibian
        #heal
        #diseaseres 100
        #tmpearthgems 2
        #tmpnaturegems 1
        #end

        #selectmonster 1402 -- Polypal Queen
        #gcost 150
        #heal
        #diseaseres 100
        #end

        #selectmonster 2234 -- Irminsul
        #gcost 170
        #heal
        #diseaseres 100
        #twiceborn 4893 -- Tree of Hate
        #end

        #selectmonster 2447 -- Idol of Men
        #gcost 150
        #heal
        #diseaseres 100
        #end

        #selectmonster 2448 -- Idol of Beasts
        #gcost 150
        #heal
        #diseaseres 100
        #end

        #selectmonster 2449 -- Idol of Sorcery
        #gcost 170
        #heal
        #diseaseres 100
        #end

        #selectmonster 2460 -- Statue of Fertility
        #gcost 150
        #heal
        #diseaseres 100
        #end

        #selectmonster 2461 -- Statue of the Bloody Mother
        #gcost 150
        #heal
        #diseaseres 100
        #end

        #selectmonster 2462 -- Golden Idol
        #gcost 140
        #heal
        #diseaseres 100
        #end

        #selectmonster 2463 -- Statue of War
        #gcost 150
        #heal
        #diseaseres 100
        #end

        #selectmonster 2465 -- Statue of War
        #gcost 150
        #heal
        #diseaseres 100
        #end

        #selectmonster 2503 -- Golden Pillar
        #gcost 140
        #heal
        #diseaseres 100
        #end

        #selectmonster 2696 --  Stone Huaca
        #gcost 150
        #heal
        #diseaseres 100
        #end

        #selectmonster 2847 -- Protgenos of the Sea
        #gcost 160
        #heal
        #diseaseres 100
        #end

        #selectmonster 2848 -- Father of the Sea
        #gcost 160
        #heal
        #diseaseres 100
        #end

        #selectmonster 2850 -- STatue of the Underworld
        #gcost 150
        #heal
        #diseaseres 100
        #end
    -- DOM 3 ---

        #selectmonster 109 -- Dagon
        #gcost 260
        #mr 20
        #def 12
        #hp 139
        #heal
        #diseaseres 100
        #tmpwatergems 1
        #tmpearthgems 2
        #end

        #selectmonster 156 -- Cyclops
        #clearweapons
        #spr1 "./ExtraPretenders/Cyclops.tga"
        #spr2 "./ExtraPretenders/Cyclops2.tga"
        #name "Chthonic Cyclops"
        #descr "The Cyclops is a one-eyed giant sprung from the roots of a great mountain. The body of the Cyclops is huge and as hard as the rock from which it was born. The Cyclops is a master of Earth magic and will use less gems when forging magic items. In battle it will manifest Earth gems to aid in spellcasting."
        #gcost 250
        #att 13
        #def 13
        #mr 20
        #fixforgebonus 2
        #heal
        #diseaseres 100
        #tmpearthgems 3
        #weapon 562 -- Stone Fist
        #weapon 397 -- Kick
        #homerealm 3 -- Mediterranean
        #end

        #selectmonster 157 -- Mother of Monsters
        #clearweapons
        #gcost 200
        #heal
        #diseaseres 100
        #def 13
        #mr 20
        #weapon 29 -- Claw
        #weapon 532 -- Tail Sweep
        #batstartsum1 3168 -- Hound of Twilight
        #batstartsum2 467 -- Foul Beast
        #batstartsum3 453 -- Foul Spawn
        #tmpdeathgems 1
        #tmpnaturegems 1
        #gemprod 7 1
        #end

        #selectmonster 180 -- DemiLich
        #gcost 160
        #heal
        #diseaseres 100
        #end

        #selectmonster 294 -- Nerid
        #descr "The Nerid is an underwater being born at the dawn of time, when the sea was untamed and filled with monstrous beings. She was given power over life in the forests of the shallow seas by a previous Pantokrator. But hubris found her and she gave wisdom and mind to the beasts of the sea, making them aware of her presence. The awakened half-men gave her their devotion and prayers as though she was a god. The Pantokrator was furious and banished his rebellious servant for eternity. Now with the Pantokrator gone, the Nerid is once more free to receive the prayers of men and beasts and ichtysatyrs will flock to her as her Dominion grows strong. The Nerid will manifest gems of Water and Nature magic in combat, however she is aquatic and cannot leave her maritime realm."
        #gcost 180
        #heal
        #diseaseres 100
        #mr 20
        #awe 5
        #tmpwatergems 1
        #tmpnaturegems 2
        #domsummon 2376 -- Ichtysatyr
        #end

        #selectmonster 384 -- Neter of Crafts
        #gcost 220
        #heal
        #diseaseres 100
        #prot 5
        #mr 20
        #fixforgebonus 4
        #pathcost 40
        #resources 100
        #tmpearthgems 2
        #tmpastralgems 1
        #end

        #selectmonster 385 -- Neteret of Joy
        #descr "The Neteret is a giant of divine heritage. A previous Pantokrator gave her powers of creation and she brought joy and fertility to the world. When the Pantokrator became aware of the rebellion of lesser gods, he imprisoned her and the world was bereft of Joy. With the Pantokrator gone, her imprisonment has ended and fertility and happiness have returned to the world. In combat she will manifest magical earth and nature gems to aid in spellcasting. The Neteret will improve the harvest of the province where she dwells, generating additional supplies and increasing the tax revenue by thirty percent."
        #gcost 200
        #prot 5
        #mr 20
        #tmpearthgems 2
        #tmpnaturegems 1
        #heal
        #diseaseres 100
        #end

        #newevent
        #rarity 5
        #req_monster 385 -- Neteret of Joy
        #msg "The province has bloomed in the presence of ##godname##!"
        #nolog
        #taxboost 30
        #end

        #selectmonster 386 -- Neter of the Sun
        #descr "The Neter is a giant of divine heritage. During the rebellion of the lesser gods, the Pantokrator was killed and dismembered. But the Pantokrator was reassembled and brought back by his wife, the Mistress of Magic. The Neter of the Sun was born from the union of the dead Pantokrator and his wife. Upon him was bestowed the authority and might of the sun. He was made a vessel of vengeance and he destroyed the rebellion and slew the Great Antagonist. But the Pantokrator could not reenter the world, bound by his own Law of Death and Rebirth. Now the Neter claims the world in his father's name. In combat he will manifest magic fire gems and astral pearls."
        #clearweapons
        #gcost 210
        #mr 20
        #prot 5
        #att 14
        #def 14
        #weapon 540 -- Staff from the Sun
        #weapon 404 -- Beak
        #tmpfiregems 2
        #tmpastralgems 1
        #heal
        #diseaseres 100
        #end

        #selectmonster 387 -- Neter of Many Names
        #descr "The Neteret is a giant of divine heritage. She was once the wife of a previous Pantokrator, and was given knowledge of all names and the deeper arcana. When her husband was slain and dismembered in a rebellion of lesser gods, she gathered his body parts and returned him to life. However, his death meant that he was for eternity banished to the Underworld. The rebellious pretenders imprisoned her and a new Pantokrator came to rule. With the disappearance of the Pantokrator her prison weakens and She of Many Names is once more worshipped as Mistress of Magic and Lady of This World. In combat she will manifest a magical astral pearl to aid in spellcasting."
        #gcost 160
        #mr 20
        #prot 5
        #tmpastralgems 1
        #heal
        #diseaseres 100
        #end

        #selectmonster 388 -- Neter of Chaos
        #descr "The Neter is a giant of divine heritage. He was once given power over the desert, storms, darkness and destruction by a previous Pantokrator. Endowed with unequaled destructive power and jealous ambition, he led the lesser gods in rebellion against his master and slew and dismembered him. However, the Pantokrator was reassembled by one of his servants and was able to return from the dead to banish the Neter for eternity. With the disappearance of the Pantokrator, the prison weakens and the Neter of Chaos can once more claim the world as his to destroy. In combat he will manifest magical gems of Fire, Air and Death to aid in spellcasting. Each month the Neter can call forth sacred warriors in his image to serve him."
        #gcost 200
        #mr 20
        #prot 5
        #darkvision 100
        #heal
        #makemonsters2 5268 -- Child of Mastabas
        #diseaseres 100
        #tmpfiregems 1
        #tmpairgems 1
        #tmpdeathgems 1
        #end

        #selectmonster 499 -- Nataraja
        #clearweapons
        #descr "When the Devatas and Yakshas of Kailasa were forced to leave the world, their Lord was trapped. Now the Nataraja is free once again to dance the eternal Dance of Death and Birth. The Nataraja is armed with four weapons and has the martial prowess to wield them simultaneously. The Nataraja is surrounded by an aura of celestial fire. In combat he will manifest gems of Fire, Earth and Astral magic to aid in spellcasting. Each month the Nataraja can call forth sacred dancers and musicians from the Celestial realm to serve him."
        #gcost 210
        #mr 20
        #prot 5
        #awe 3
        #makemonsters2 1332 -- Apsara
        #makemonsters1 1335 -- Gandharva
        #weapon 278 -- Lightning Spear
        #weapon 76 -- Fire Sword
        #weapon 245 -- Axe of Sharpness
        #weapon 347 -- Flail
        #weapon 397 -- Kick
        #heal
        #diseaseres 100
        #tmpfiregems 1
        #tmpearthgems 1
        #tmpastralgems 1
        #end

        #selectmonster 501 -- Allfather
        #descr "The Allfather is the first and the last of the Aesir, ancient gods that died in the great war with the Rimtursar. When the war ended the Pantokrator banished the remaining aesir from the world. With the Pantokrator gone the Allfather has returned to the world to claim it as his. The Allfather was the foremost of the Aesir and is a master of magic, a great skald and an outstanding general. Like his subjects, he is a master of illusions and false appearances and is able to pass undetected through enemy lands. Like the Vanjarls of Vanheim, the Allfather is able to cross the oceans by ship. The Allfather is at all times accompanied by two great wolves and his great eight-legged horse who is able to ride the winds. In combat he will manifest magical gems of Air, Astral and Death to aid in spellcasting."
        #gcost 250
        #mr 20
        #prot 5
        #pathcost 20
        #researchbonus 10
        #woundfend 2
        #diseaseres 100
        #tmpairgems 1
        #tmpastralgems 1
        #tmpdeathgems 1
        #end

        #selectmonster 600 -- Titan of War & Wisdom
        #descr "The Titan is a giant sprung from the forehead of a previous Pantokrator, when he had a thought he couldn't contain. Fully grown and armed, she burst forth and claimed divinity. She was eternally imprisoned for her impudence of giving the Pantokrator a headache. Her origin as a thought has made her wise and prudent. Her shield is painted with a Gorgoneion that strikes fear into the hearts of men. She is at all times followed by her owl companion. In combat she will manifest gems of Air, Earth and Astral magic to aid in spellcasting."
        #gcost 260
        #mr 20
        #prot 5
        #hp 105
        #researchbonus 10
        #inspiringres 1
        #heal
        #unsurr 2
        #diseaseres 100
        #tmpairgems 1
        #tmpearthgems 1
        #tmpastralgems 1
        #end

        #selectmonster 602 -- Titan of Heaven
        #clearweapons
        #descr "The Titan is a giant sprung from thunder clouds. Once given authority and power over thunder and the heavens by a previous Pantokrator, he was tasked with meting out justice to mankind. As Lord of the Heavens he could see everything underneath and would punish sinners with thunder and storms. When he gazed upon the world below, he became aware of the beauty of women and could not help himself. He broke his master's decree, entered the world of men and begot offspring infused with his divine seed. The Pantokrator was furious and imprisoned the rebellious titan for eternity. Now, with the Pantokrator gone, lightning and thunder is wrecking the foundations of the prison and the Titan will soon claim the world as his to rule. In combat he will manifest magical earth and air gems to aid in spellcasting."
        #gcost 200
        #mr 20
        #prot 5
        #tmpairgems 2
        #tmpearthgems 1
        #heal
        #weapon 1846 -- Thunder Bolt
        #weapon 231 -- Thunder Fist
        #diseaseres 100
        #end

        #selectmonster 603 -- Teotl of Rain
        #descr "The Teotl is a giant of divine heritage. He was once in charge of bringing rain and fertility to the world and as such, he was one of the most important servants of the previous Pantokrator. But he defied his lord and gave fertility for blood. The Pantokrator feared that his servant would grow powerful on his bloody diet and imprisoned him for eternity. With the Pantokrator gone, the Teotl of Rain is finally free to drink blood from the vessel that is the world. Rainfall will increase wherever he is, and in combat he will manifest magical water and nature gems to aid in spellcasting. Jaguar Toads will emerge from the swamps and forests of his domain to serve him as his Dominion grows strong."
        #gcost 200
        #mr 20
        #prot 5
        #supplybonus 50
        #heal
        #domsummon 1359 -- Jaguar Toad
        #diseaseres 100
        #tmpwatergems 1
        #tmpnaturegems 1
        #gemprod 7 1
        #end

        #selectmonster 604 -- Teotl of the Night
        #descr "The Teotl is a giant of divine heritage. During the reign of a previous Pantokrator he was the Beast of the Night, released by his master to feed on the wicked and the weak. After having eaten thousands and thousands of hearts, he grew in malice and in power. His master became worried and imprisoned the Beast for eternity. Now with the Pantokrator gone, the Beast of the Night has returned and his hunger has grown during his millennial imprisonment. In combat he will manifest a magical death gem, and each month he will hunt out suitable victims from the local population."
        #gcost 240
        #mr 20
        #heal
        #diseaseres 100
        #tmpdeathgems 1
        #gemprod 7 2
        #end

        #selectmonster 605 -- Son of Niefel
        #cleararmor
        #descr "The Son of Niefel is a frost giant descended from the old Rimtursar. The giant is sprung from the glaciers of Niefelheim, the Land of Eternal Frost. His beard is made of icicles and his shield and sword are forged from the purest ice. The Son of Niefel is constantly surrounded by an icy wind and Winter Wolves will come to his aid whenever he is in a cold province. He can call sacred Jotun wolves from the deep forest to serve him each month. In combat he will manifest gems of Water and Death magic to aid in spellcasting."
        #gcost 210
        #mr 20
        #prot 5
        #armor 176 -- Dire Wolf Pelt
        #armor 29 -- Ice Cap
        #armor 28 -- Ice Aegis
        #heal
        #makemonsters2 1309 -- Jotun Wolf
        #diseaseres 100
        #tmpwatergems 2
        #tmpdeathgems 1
        #end

        #selectmonster 606 -- Great Mother
        #descr "The Great Mother is a huge clay being that resembles a swelling, pregnant woman. She gave birth to titans and beasts and finally a previous Pantokrator. Afraid of her progenitive powers the Pantokrator imprisoned her for eternity. With the Pantokrator gone, she and her children have returned to the world. In combat the Great Mother will manifest magical gems of Earth and Nature to aid in spellcasting."
        #gcost 260
        #prot 12
        #hp 165
        #enc 3
        #mr 20
        #heal
        #diseaseres 100
        #end

        #selectmonster 652 -- Void Lord
        #clearweapons
        #descr "The Void Lord is an otherworldly being that spawned in the Void. It was worshiped on that strange and distant star from which the great race of R'lyeh came and now the Starspawns have summoned it to this world. As a Lord of the Void, this being has great power in the world of magic. In the physical world, however, the Void Lord is blind and orients itself with powers of mind and magic. In combat it will manifest gems of Water and Astral magic to aid in spellcasting."
        #gcost 250
        #mr 22
        #hp 148
        #acidshield 8
        #inspirational 2
        #bluntres
        #pierceres
        #slashres
        #heal
        #weapon 636 -- Life Drain Tentacle
        #weapon 29 -- Claw
        #weapon 86 -- Mind Blast
        #diseaseres 100
        #tmpwatergems 1
        #tmpastralgems 2
        #end

        #selectmonster 812 -- Lord of the Wild
        #descr "This being is the lord of an ancient forest and all its inhabitants. The Lord of the Wild practices the old arts and is a master of Nature and Blood magic. Human females in his vicinity will shed their civilized ways and serve the Lord of the Wild in a life of revelry. These females are called maenads and they will come in great numbers in a Dominion with great Turmoil. In combat he will manifest nature gems to aid in spellcasting, and each month he will choose those of pure blood amongst his followers to serve him as Blood Slaves."
        #gcost 250
        #hp 118
        #prot 14
        #douse 3
        #fear 5
        #mr 20
        #tmpnaturegems 2
        #gemprod 7 1
        #seduce 16
        #heal
        #diseaseres 100
        #woundfend 2
        #end

        #selectmonster 905 -- Jade Emperor
        #descr "The Jade Emperor is an ancient Celestial being who has descended from the Celestial Spheres to claim this world as his Empire. He appears as an old, bearded man of huge proportions, dressed in the robes of a bureaucrat. He has great powers of Water, Air and Astral magic and is able to change fate itself. The Jade Emperor will prevent one third of all bad events in the province where he dwells, and can call on the Soldiers of the Celestial court each month. In combat he will manifest magical air, water and astral gems to aid in spellcasting."
        #gcost 200
        #mr 20
        #hp 135
        #prot 5
        #batstartsum2 902 -- Celestial Soldiers
        #makemonsters2 902 -- Celestial Soldiers
        #tmpairgems 1
        #tmpwatergems 1
        #tmpastralgems 1
        #heal
        #diseaseres 100
        #end

        #selectmonster 957 -- Lord of the Desert
        #descr "The Lord of the Desert Sun is a lion-headed Titan who claims dominion over the Sun and the wild lands beyond the borders of civilization. Lions flock to his Dominion and during battle they will come to his aid. He radiates the dry heat of the desert, and in combat will manifest Fire and Nature gems to aid in spellcasting. Each month he can call great sacred lions from the desert to serve him."
        #gcost 200
        #mr 20
        #prot 5
        #heat 10
        #heal
        #makemonsters2 5137 -- Sacred Kithaironic Lion
        #diseaseres 100
        #tmpfiregems 2
        #tmpnaturegems 1
        #end

        #selectmonster 961 -- Titan of the Sea
        #clearweapons
        #descr "The Titan of the Sea is a huge demigod sprung from the depths of the ocean. He was given power over the sea and the rumbling earth by a previous Pantokrator. Afraid of his destructive powers, men began to worship him as though he was a God. The Pantokrator was furious and banished his servant for eternity. Now that the Pantokrator is gone, the Earth Shaker is once more free to shatter cities and receive the prayers of men. The Titan of the Sea can leave the ocean and grant humans the ability to breathe underwater. In combat he will manifest magical earth and water gems to aid in spellcasting. "
        #gcost 210
        #mr 20
        #prot 5
        #armor 148 -- Crown
        #weapon 83 -- Wave Breaker
        #giftofwater 500
        #heal
        #poisonres 10
        #diseaseres 100
        #tmpwatergems 2
        #tmpearthgems 1
        #end

        #selectmonster 1096 -- Destroyer of Worlds
        #clearweapons
        #descr "When the Devatas and Yakshas of Kailasa were forced to leave the world, their Lord was trapped by the Pantokrator. The Destroyer of Worlds is a huge four-armed divinity reawakened to destroy the world that wronged him in ages past. The Destroyer is armed with weapons of iron and lightning. Death and disease are his to give and bestow. He will manifest gems of Air and Death magic in battle to aid in spellcasting."
        #gcost 220
        #mr 20
        #awe 3
        #att 15
        #def 15
        #weapon 278 -- Lightning Spear
        #weapon 76 -- Fire Sword
        #weapon 438 -- Plague Bow
        #weapon 243 -- Lightning
        #heal
        #diseaseres 100
        #tmpairgems 2
        #tmpdeathgems 1
        #end

        #selectmonster 1097 -- Lord of the Summer Plague
        #descr "The Lord of the Summer Plague is a Titan who claims dominion over the summer heat. With the heat come drought and famine, plague and pestilence. The lion-headed Lord is a god of death and his bow strikes men with death or disease. He is surrounded by a cloud of heat and pestilence that will inflict plague upon those that get too close. Each month the Lord of the Summer Plague can summon a plague spirit from the nether realms to blight the living. In combat the Titan will manifest gems of Fire and Death magic to aid in spellcasting."
        #gcost 200
        #mr 20
        #prot 5
        #diseasecloud 10
        #heat 10
        #autodisgrinder 1
        #heal
        #makemonsters1 1662 -- Disease Demon
        #diseaseres 100
        #tmpfiregems 1
        #tmpdeathgems 2
        #end

        #selectmonster 1098 -- Asynja
        #descr "The Asynja is a female Aesir who has survived the death of her kin. The Aesir were ancient gods who were defeated aeons ago by the Rimtursar in a cataclysmic battle. When the war ended the last remaining Aesir were banished by the Pantokrator. Now, with the Pantokrator gone the Asynja has returned to the world to claim it as hers. In combat she will manifest gems of Air and Earth magic to aid in spellcasting. Each month she can call Vakyries from Vanhalla to serve her, and in combat she is protected by a host that descends from the sky."
        #gcost 210
        #mr 20
        #batstartsum1d6 855 -- Valkyries
        #makemonsters3 855 -- Valkyries
        #heal
        #coldres 5
        #diseaseres 100
        #tmpairgems 2
        #tmpearthgems 1
        #end

        #selectmonster 1230 -- Titan of the Forge
        #descr "The Titan of the Forge is a crippled God. Mutilated, imprisoned and given tools of smithing, he served the previous Pantokrator, but was forgotten and left for dead. During the aeons the Forge Lord learned his craft and became a true master when it comes to forging magic items and mundane tools. Now his skills will serve both the farmers and savants of his kingdom. When forging magic items, he requires fewer magic gems than an ordinary mage. The Forge Lord has had a deformed foot since his days in the service of the Pantokrator. In combat he will manifest magical fire and earth gems to aid in spellcasting."
        #gcost 200
        #mr 20
        #resources 50
        #woundfend 2
        #diseaseres 100
        #pathcost 40
        #tmpfiregems 1
        #tmpearthgems 2
        #end

        #selectmonster 1231 -- Drakaina
        #descr "The Drakaina was once a Titaness, beautiful beyond belief. When she spurned the previous Pantokrator she was cursed with a hideous form and was imprisoned on an enchanted isle. She now appears as a woman with the lower part of a huge serpent. From the serpent grow six dogs that hunger for human flesh. For millennia the baying of the dogs have frightened sailors that mistakenly have come too close to her island prison. Now, with the Pantokrator gone, her shackles are weakening and the Drakaina will avenge her millennial imprisonment. In combat she will manifest magical water, death and nature gems to aid in spellcasting."
        #clearweapons
        #gcost 240
        #mr 20
        #def 13
        #prot 16
        #weapon 375 -- Dogs
        #weapon 375 -- Dogs
        #weapon 29 -- Claw
        #weapon 532 -- Tail Sweep
        #heal
        #diseaseres 100
        #tmpwatergems 1
        #tmpdeathgems 1
        #tmpnaturegems 1
        #end

        #selectmonster 1232 -- Old Man of the Sea
        #clearweapons
        #descr "The Old Man of the Sea is possibly the oldest of all the underwater beings born at the dawn of time, when the sea was untamed and filled with monstrous beings. Wise beyond compare he was given power to guide the new-born races of the deeps by a previous Pantokrator. But regardless of his wishes, the tritons begun to worship him as though he was a god. When the Old Man of the Sea mentioned this to the Pantokrator, his master grew jealous and banished his servant for eternity, leaving the races of the deeps bereft of magical guidance. Now with the Pantokrator gone, the Old Man of the Sea is returning to bring wisdom and magic understanding to the races of the deeps. The Old Man of the Sea is able to leave the sea and can bring land-living beings with him into the sea. In combat he can manifest magical water and astral gems to aid in spellcasting, and he can animate the sea itself to serve him."
        #gcost 200
        #mr 20
        #researchbonus 20
        #weapon 238 -- Magic Staff
        #weapon 532 -- Tail Sweep
        #makemonsters1 410 -- Water Elemental
        #heal
        #diseaseres 100
        #tmpwatergems 2
        #tmpastralgems 1
        #end

        #selectmonster 1233 -- Lord of the Waves
        #clearweapons
        #descr "The Lord of the Waves is an underwater being born at the dawn of time, when the sea was untamed and filled with monstrous beings. The Lord of the Waves was given power over all shallow waters and the waves, where the sea meets the winds, by a previous Pantokrator. But hubris found him and he sired the tritons and let them worship him as though he was a god. The Pantokrator was furious and banished his rebellious servant for eternity. Now with the Pantokrator gone, the Lord of the Waves is once more free to receive the prayers of triton and men alike. The Lord of the Waves is able to leave the sea and can bring land-living beings with him into the sea. Whilst in his ocean realm he is guarded at all times by an honour guard of sacred Triton warrior knights. In combat he will manifest gems of Air, Water and Nature magic to aid in spellcasting."
        #gcost 200
        #mr 20
        #weapon 641 -- Bronze Trident
        #weapon 532 -- Tail Sweep
        #batstartsum4 1059 -- Knight of the Deep
        #heal
        #diseaseres 100
        #tmpairgems 1
        #tmpwatergems 1
        #tmpnaturegems 1
        #end

        #selectspell 140
        #copyspell 195 -- Twist Fate
        #name "Nyorai Protection"
        #school -1
        #aoe 666
        #spec 12599296 -- Ignore shields, friendlies only, use UW
        #end

        #selectmonster 1339 -- Nyorai
        #descr "The Nyorai is a giant of divine heritage. He is a lord of plenty and a bringer of good fortune to those who surround him. Because of this and other magical powers, he is worshipped by the lesser people and now he is out to become the True God. In combat he will place all friendly troops under his protection, negating the first damaging blow that would strike them. He can manifest magical earth and astral gems to aid in spellcasting."
        #gcost 200
        #mr 20
        #prot 5
        #bringeroffortune 35
        #tmpastralgems 2
        #tmpearthgems 1
        #onebattlespell 140 -- Nyorai Protection
        #heal
        #diseaseres 100
        #end

        #selectmonster 1340 -- Tiwaz of War
        #clearweapons
        #descr "The Tiwaz is a giant of divine heritage who once was the General of the West in the old Pantokrator's armies. Now that the Pantokrator has fallen, the general will conquer the world and make it his domain. The Tiwaz is a masterful general and in combat all his followers will be filled with a fanatical fervour. He can manifest temporary gems of Fire, Air and Earth magic in combat to aid in spellcasting."
        #gcost 210
        #mr 20
        #prot 5
        #hp 90
        #inspirational 1
        #weapon 75 -- Enchanted Sword
        #onebattlespell 189 -- Fanaticism
        #heal
        #diseaseres 100
        #tmpfiregems 1
        #tmpairgems 1
        #tmpearthgems 1
        #end

        #selectmonster 1341 -- Devi of Darkness
        #descr "The Devi of Darkness is a huge four-armed divinity with a horrible appearance. She was once given power to battle demons by a previous Pantokrator, but her destructive fury made him banish her eternally to save the world from her rage. With the disappearance of the Pantokrator the raging devi has broken free and will subjugate and destroy all demons and likely the world as well. The Devi of Darkness is a slayer of demons and she holds a demon head in her hand and a bowl into which blood from the head constantly pours. Demon blood also pours from her open mouth. The Devi wields a sword which thirsts for demon blood and a trident that forces demons to halt. In combat she will manifest an earth gem to aid in spellcasting, and each month she will select those of pure blood to serve her as Blood Slaves."
        #gcost 260
        #prot 10
        #mr 20
        #darkvision 100
        #heal
        #diseaseres 100
        #tmpearthgems 1
        #gemprod 7 2
        #end

        #selectmonster 1342 -- Titan of Rivers
        #clearweapons
        #descr "The Titan of Rivers is a giant of divine heritage. She claims dominion over the great rivers of the world. Her will and her great pitcher bring life to the land. She can draw from the power of all great rivers and receives two magical Water gems each month. In combat she will pour water from her pitcher which will animate to defend her. In addition she can manifest magical water and astral gems to aid in spellcasting. Each month she can create a permanent Water elemental from her pitcher."
        #gcost 210
        #mr 20
        #tmpwatergems 2
        #tmpastralgems 1
        #batstartsum2 411 -- Size 3 Water Elemental
        #makemonsters1 410 -- Size 4 Water Elemental
        #weapon 496 -- Sacred Pitcher
        #weapon 92 -- Fist
        #heal
        #diseaseres 100
        #end

        #selectmonster 1343 -- Titan of Love
        #clearweapons
        #descr "The Titan of Love is a giant of divine heritage. She is blessed with aphrodisiac beauty and few mortals would dream of harming her, or can resist her alluring glances. Her mere presence brings love to flower and the province in which she dwells will have unrest decreased. In combat she will be accompanied by a few of her current lovers and admirers that will defend her against harm. She can manifest temporary gems of water and nature to aid in spellcasting."
        #gcost 210
        #mr 20
        #prot 5
        #heal
        #weapon 1850 -- Charming Glance
        #weapon 92 -- Fist
        #batstartsum3 -5160 -- Arco Hero
        #diseaseres 100
        #tmpwatergems 1
        #tmpnaturegems 2
        #end

        #selectmonster 1344 -- Devi of Good Fortunes
        #descr "The Devi is a demigod of immense size. She was once the wife of a Previous pantokrator. As a good wife and mother she was given power over the Great River and the fortunes of men. However, the Pantokrator lost interest in her and banished her to another world. Now with the Pantokrator gone she has returned to her land to claim it as hers. She is able to grant her followers the ability to breathe water and good fortune affects the province in which she dwells. In combat she can manifest magical water and astral gems to aid in spellcasting."
        #gcost 210
        #mr 20
        #prot 5
        #giftofwater 500
        #bringeroffortune 50
        #decscale 4 -- +Luck
        #tmpwatergems 1
        #tmpastralgems 2
        #heal
        #diseaseres 100
        #end

        #selectmonster 1345 -- Celestial General
        #clearweapons
        #descr "This wise man was once the General of the East in the old Pantokrator's armies. After the General of the West was almost slain, the Pantokrator became worried. As a precaution, he granted his general a divine body so that he would not easily be slain. As General of the Celestial Armies he is served by Celestial Soldiers and he can call upon others from the celestial realm. Now that the Pantokrator has fallen, the general who calls himself the Celestial General is out to become the new God. In combat he will manifest gems of Air, Earth and Astral magic to aid in spellcasting."
        #gcost 200
        #mr 20
        #prot 5
        #hp 120
        #batstartsum2 902 -- Celestial Soldier
        #makemonsters2 902 -- Celestial Soldier
        #weapon 75 -- Enchanted Sword
        #heal
        #diseaseres 100
        #tmpairgems 1
        #tmpearthgems 1
        #tmpastralgems 1
        #end

        #selectmonster 1348 -- Titan of Serpents & Medicine
        #descr "The Titan of Serpents and Healing is a giant of divine heritage. She is the Mother of Serpents and Mistress of the Medical Arts. Her blessings give health and her wrath gives untimely death. In combat she is always accompanied by a handful of snakes, two of which she holds in her hands as weapons to attack her enemies. Each month she can summon sacred serpents to serve her followers. In combat she will manifest magical gems of nature and death to aid in spellcasting."
        #gcost 200
        #mr 20
        #prot 5
        #autodishealer 0
        #autohealer 3
        #makemonsters3 295 -- Sacred Serpent
        #heal
        #diseaseres 100
        #tmpdeathgems 1
        #tmpnaturegems 2
        #end

        #selectmonster 1370 -- Volla of the Bountiful Forest
        #descr "The Volla is a giant of divine heritage sprung from the headwaters of a mighty river. She claims dominion over the bountiful forest, the streams and everything living in them. She can draw from the magic of all springs and receives two enchanted gems each month. The province where the Volla dwells will be bountiful in all things and will generate thirty percent more tax revenue than usual for the treasury. In combat she will manifest magical gems of water and nature to aid in spellcasting."
        #gcost 200
        #mr 20
        #hp 85
        #heal
        #tmpwatergems 2
        #tmpnaturegems 1
        #diseaseres 100
        #end

        #newevent
        #rarity 5
        #req_monster 1370 -- Volla of the Bountiful Forest
        #msg "The province has bloomed in the presence of ##godname##!"
        #nolog
        #taxboost 30
        #end

        #selectmonster 1371 -- Titan of Death & Rebirth
        #descr "The Titan of Death and Rebirth is a giant of divine heritage. He claims dominion over the cycles of growth, death and rebirth. On his head he wears a calathos from which olive leaves sprout. Within his own dominion he is immortal and will be ritually reborn if slain. In combat he will manifest magical earth, death and nature gems to aid in spellcasting."
        #gcost 250
        #mr 20
        #tmpearthgems 1
        #tmpdeathgems 1
        #tmpnaturegems 1
        #domimmortal
        #heal
        #diseaseres 100
        #end

        #selectmonster 1372 -- Annunaki of the Sky
        #descr "The Annunaki is a giant of divine heritage. He claims dominion over thunder, lightning and everything that flies. With the disappearance of the Pantokrator, there is no one to stop him from claiming the world beneath his aerial dominion. Each month he can call sacred gryphons from the sky to serve him. In battle the Annunaki can manifest magical air gems to aid in spellcasting."
        #gcost 190
        #mr 20
        #prot 5
        #tmpairgems 3
        #batstartsum1 0
        #batstartsum2 368 -- Sacred Gryphon
        #makemonsters2 368 -- Sacred Gryphon
        #heal
        #diseaseres 100
        #end

        #selectmonster 1373 -- Annunaki of the Sweet Water
        #descr "The Annunaki is a giant of divine heritage. During the reign of the previous Pantokrator he was guardian of the underwater ocean and its hidden secrets. With the disappearance of the Pantokrator, he will use these secrets to claim the entirety of this world. The currents move at the command of the Annunaki and will always be favourable to him in combat. This will hinder his enemies and help his allies in combat whilst below the waves. He can manifest temporary water and earth gems to aid in spellcasting."
        #gcost 200
        #mr 20
        #prot 5
        #armor 148 -- Crown
        #heal
        #onebattlespell 913 -- Friendly Currents
        #giftofwater 500
        #tmpwatergems 2
        #tmpearthgems 1
        #diseaseres 100
        #end

        #selectmonster 1374 -- Annunaki of the Morning Star
        #copyspr 0
        #spr1 "./ExtraPretenders/Morningstar.tga"
        #spr2 "./ExtraPretenders/Morningstar2.tga"
        #descr "The Annunaki is a giant of divine heritage. She claims to be the Morning Star and she has powers over the earth, the sky and the stellar heavens. With the Pantokrator gone, there is no other being that can claim dominion over all three worlds. The North Star is her ally and will rise at her command in battle, granting power to Astral mages. She is always accompanied by an owl and in combat will manifest magical air, earth and astral gems to aid in spellcasting."
        #gcost 200
        #mr 20
        #heal
        #onebattlespell 761 -- Light of the Northern Star
        #tmpairgems 1
        #tmpearthgems 1
        #tmpastralgems 1
        #diseaseres 100
        #end

        #selectmonster 1378 -- Lord of the Forest
        #cleararmor
        #clearweapons
        #descr "Usually the Lord of the Forest would be satisfied with the yearly sacrifice of some moose and in return, he would keep the forest safe for the hunters, but with the disappearance of the Pantokrator, the Lord of the Forest has returned to claim godhood. Any that strike at him will be attacked by the forest itself which will ensnare and entangle attackers. In combat he will manifest gems of Earth and Nature magic to aid in spellcasting."
        #gcost 240
        #hp 95
        #mr 20
        #prot 5
        #heal
        #att 14
        #def 14
        #entangle
        #weapon 79 -- Thorn Spear
        #armor 49 -- Lightweight Scale
        #armor 227 -- Magic Helmet
        #diseaseres 100
        #tmpearthgems 1
        #tmpnaturegems 2
        #end

        #selectspell 141
        #name "Gjallarhorn"
        #effect 6043 -- 6turns border summoning
        #nreff 1
        #damage 2531 -- Huskarl
        #explspr 10002
        #sound 47
        #end

        #selectmonster 1379 -- Keeper on the Bridge
        #clearweapons
        #descr "The old Pantokrator was a careful God and he had a single bridge to his citadel of power. This bridge was guarded by the Keeper. The Keeper never sleeps and he sees everything, even in the middle of the night. The Keeper of the Bridge also has skill in Air and Earth magic to complement his already formidable fighting skills. Now that the Pantokrator is gone, the Keeper has seen his chance to become the new God. When defending a castle he counts as 100 normal soldiers. When battle commences he will blow his Gjallarhorn that can be heard in all worlds, drawing warriors to his aid that will arrive as the battle rages. The horn can also stun enemies when blown with great fervour. The Keeper can manifest temporary gems of Air, Earth and Nature magic to aid in spellcasting."
        #gcost 200
        #mr 20
        #prot 5
        #castledef 100
        #heal
        #weapon 691 -- Blacksteel Sword
        #weapon 1851 -- Gjallarhorn
        #onebattlespell 141 -- Gjallarhorn
        #diseaseres 100
        #tmpairgems 1
        #tmpearthgems 1
        #tmpnaturegems 1
        #end

        #selectmonster 1384 -- Solar Disc
        #clearweapons
        #descr "The Solar Disc claims to be a manifestation of the Sun. It gives and takes life and commands great power over Fire. The Solar Disc is extremely hot and will increase heat across the entire province. Anyone who comes close will quickly be burned to cinders. In combat the disc will manifest Fire gems to aid in spellcasting."
        #gcost 240
        #mr 20
        #prot 12
        #decscale 2 -- +HEAT
        #weapon 383 -- Throw Flames
        #weapon 408 -- Talons
        #weapon 350 -- Fire Flare
        #heal
        #diseaseres 100
        #tmpfiregems 3
        #end

        #selectmonster 1561 -- Father of Winters
        #cleararmor
        #clearweapons
        #descr "The Father of Winters likes it cold and his physical power increases in cold provinces. He is constantly surrounded by a large whirlwind of snow that will freeze anyone nearby. In combat he will manifest gems of Air and Water magic to aid in spellcasting."
        #gcost 210
        #mr 20
        #prot 5
        #hp 118
        #iceprot 1
        #weapon 82 -- Frost Brand
        #armor 176 -- dire wolf pelt
        #armor 20 -- Iron Cap
        #heal
        #diseaseres 100
        #tmpairgems 1
        #tmpwatergems 2
        #end

        #selectmonster 1899 -- Fomorian God King
        #clearweapons
        #descr "The Fomorian God King is an ancient demigod and servant of the previous Pantokrator who tasked him to rule the Land of the Watery Dead. When his Fomorian kin ravaged the world, the God King was banished from his dark kingdom and punished for his sins, marked with skin as black as the night. Now, with the Pantokrator gone, the God King is awakening to recreate the kingdom once denied him. The God King has great power over the dead and the sea. In combat he will manifest gems of Water and Death magic to aid in spellcasting."
        #gcost 220
        #mr 20
        #prot 5
        #weapon 201 -- Magic Spear
        #weapon 123 -- Javelin of Flight
        #heal
        #giftofwater 500
        #diseaseres 100
        #tmpwatergems 1
        #tmpdeathgems 2
        #end

        #selectmonster 2082 -- Son of the Fallen
        #descr "The Son of the Fallen is the last of the Nephilim, ancient giants of godlike power. When the other Nephilim lost purpose, he began to hunt them down and devoured them all. Having consumed the flesh and powers of his brethren, the Son of the Fallen has nothing left to live for but achieving godhood. Each month he will choose from amongst his followers those of pure blood to serve him as Blood Slaves. He can also call upon the surviving Rephaites to serve him."
        #gcost 170
        #mr 20
        #heal
        #pathcost 40
        #makemonsters1 2030 -- Rephaite Warrior
        #diseaseres 100
        #gemprod 7 3 
        #end

        #selectmonster 2203 -- Oni Kunshu
        #descr "The Oni Kunshu is a king of kings in the Netherworld, powerful enough to claim this world as his. Like the other Onis, the Oni Kunshu thrives on turmoil and carnage and his dominion will be a war-ridden and broken land. He is highly resilient and will suffer permanent injuries less often than most creatures. Oni will flock to him as his Dominion grows strong. In combat he will manifest magical Fire, Earth and Death gems to aid in spellcasting."
        #gcost 190
        #mr 20
        #inspirational 2
        #incscale 0
        #onisummon 100
        #heal
        #diseaseres 100
        #tmpfiregems 1
        #tmpearthgems 1
        #tmpdeathgems 1
        #end

        #selectmonster 2204 -- Oni Spirit
        #gcost 190
        #mr 20
        #inspirational 2
        #incscale 0
        #onisummon 100
        #heal
        #diseaseres 100
        #end

        #selectmonster 2239 -- Asynja
        #descr "The Asynja is a female Aesir who has survived the death of her kin. The Aesir were ancient gods who were defeated aeons ago by the Rimtursar in a cataclysmic battle. The Asynja was once given the task of guarding the apples of immortality by a previous Pantokrator and ate one, but she was abducted by a descendant of the Rimtursar and lost the remaining apples. The Pantokrator punished her with eternal imprisonment. Now with the disappearance of the Pantokrator she has returned to bring hope and life to the world. In combat he will manifest magical Air and Nature gems to aid in spellcasting. She is a master of healing and will cure afflictions each month."
        #gcost 260
        #mr 20
        #domimmortal
        #autohealer 1
        #heal
        #coldres 5
        #diseaseres 100
        #tmpairgems 1
        #tmpnaturegems 2
        #end

        #selectmonster 2431 -- Titan of the Underworld
        #descr "The Titan of the Underworld is a giant of divine heritage. Once given the task of ruling the dead by a previous Pantokrator the Titan was forbidden to enter the land of the living. Forever banished from the living world the Titan watched in envy as the living went about their business. He made his realm a dull and pale replica of the land under the sun. Now with the Pantokrator gone the Titan of the Underworld can return to the land of the living, with his gifts of death and darkness. The Titan was once given a helmet of invisibility by the cyclops smiths of Tartarus. With the aid of this helmet he has occasionally entered the world of the living to observe and slay those who display hubris. In combat he will manifest magical Death gems to aid in spellcasting. Shades will manifest from the Underworld to serve him as his Dominion grows strong."
        #gcost 230
        #mr 20
        #heal
        #diseaseres 100
        #tmpearthgems 1
        #tmpdeathgems 2
        #end

        #selectmonster 2434 -- Teteo Inan
        #clearweapons
        #descr "The Teteo Inan, Mother of Gods, aided a previous Pantokrator in the creation of the world. When she gave birth to a multitude of children, he became furious, despite the fact that they were his. Convinced that she gave birth to spite him and to lead his offspring against him, he slew her, or at least he tried. When she was decapitated blood spurted forth and formed two great serpents where her head used to be. Having failed to slay her, he punished her with eternal imprisonment instead. Now with the Pantokrator gone, She With the Snake Skirt has returned to creation to claim it as her own. The Teteo Inan wears a horrible necklace made of the skulls, hands and hearts of men, and a skirt of writhing snakes. Once beautiful, she is now a horror to behold. In battle she will manifest gems of earth and nature to aid in spellcasting, and each month she will choose followers of pure blood to serve her as Blood Slaves."
        #gcost 260
        #mr 20
        #prot 5
        #poisonres 15
        #weapon 63 -- Life Drain
        #weapon 65 -- Venomous Fangs
        #weapon 65 -- Venomous Fangs
        #weapon 582 -- Snake Skirt
        #heal
        #diseaseres 100
        #tmpearthgems 1
        #tmpnaturegems 1
        #gemprod 7 1
        #end

        #selectmonster 2435 -- Annunaki of Love and War
        #clearweapons
        #descr "The Annunaki is a giant of divine heritage. She was once the warrior and lover of the previous Pantokrator, but was imprisoned for her violent and promiscuous behavior. She has given birth to heroes, gods and monsters alike. Now with the Pantokrator gone, she is ready to claim a world inhabited by her children. In combat she will manifest gems of Air, Earth and Nature magic to aid in spellcasting."
        #gcost 210
        #mr 20
        #def 14
        #hp 95
        #awe 5
        #weapon 583 -- Taloned Claw
        #weapon 29 -- Claw
        #weapon 677 -- Wing Buff
        #tmpairgems 1
        #tmpearthgems 1
        #tmpnaturegems 1
        #heal
        #diseaseres 100
        #end

        #selectmonster 2436 -- Annunaki of the Moon
        #descr "The Annunaki is a giant of divine heritage. He was once the Guide of the Moon and the principle behind astrology, and as such, one of the most important servants of the Pantokrator. With the Pantokrator gone, he has foreseen that he will become the lord of this world. In combat he can manifest magical astral gems to aid in spellcasting."
        #gcost 220
        #mr 20
        #darkvision 100
        #pathcost 20
        #heal
        #diseaseres 100
        #tmpastralgems 3
        #end

        #selectmonster 2437 -- Annunaki of Growth and Rebirth
        #descr "The Annunaki is a giant of divine heritage. During the reign of the previous Pantokrator he was shepherd of all things living, tied to the cycle of death and rebirth. With each passing year he was killed and reborn as the seasons changed. With the Pantokrator gone, he has defeated the cycle of seasons and will no longer suffer annual death and imprisonment. Now he claims to be be the shepherd of the entire creation and he will bring life to the world. The Annunaki is a god of growth and rebirth and supplies are never sparse in a land where he dwells. He is still affected by the changing of the seasons and is more powerful during the spring. Should a mortal be impudent enough to strike at his exalted form, roots and vines from the earth itself will protect the Annunaki and trap the puny mortal. In combat he will manifest magical earth and nature gems to aid in spellcasting."
        #gcost 250
        #mr 20
        #heal
        #diseaseres 100
        #tmpearthgems 1
        #tmpnaturegems 2
        #end

        #selectmonster 2438 -- Annunaki of the Underworld
        #descr "The Annunaki is a giant of divine heritage. She was once banished to the Underworld by a previous Pantokrator so that she would prevent the dead from returning. There she claimed rulership of the dead and her beastly hounds hunted down those who tried to escape her shaded lands. Now with the Pantokrator gone, she is free to once more return to the land of the living. Her command is death and her intent is to put the world in darkness. Her servants from the underworld have followed her to the world of the living to hunt for living prey. In combat she can manifest magical death gems to aid in spellcasting, and shade beasts will arrive from the underworld to serve her as her Dominion grows strong."
        #gcost 190
        #mr 20
        #tmpdeathgems 3
        #domsummon2 442 -- Shade Beast
        #heal
        #diseaseres 100
        #end

        #selectmonster 2442 -- Teotl of War
        #clearweapons
        #cleararmor
        #descr "The Teotl is a giant of divine heritage. With the deception of the Teteo Inan and her plot against the Pantokrator, the Pantokrator answered by impregnating the Mother of Gods with a ball of feathers. When she gave birth, the Teotl was born fully aware and armed with a serpent stolen from his mother's womb. He was given power and rage by the Pantokrator and promptly slew his older siblings. With the rebellion quelled, the Pantokrator placed him in charge of the Armies of the Western Sun. He became the Warrior Supreme who crushed and devoured the enemies of the world in the name of his master. When the Pantokrator claimed the world and rewrote the Tablets of Destiny, he no longer needed the Warrior Supreme. Imprisoned for eternity, the Teotl raged against his prison, but not until the Pantokrator disappeared and his divine decree ended could he break loose. Now the Teotl of War has come to claim a world rightfully his. The Teotl is war-painted and wears a black war-mask over his face. He is armed with the Serpent he was born with and uses it as an enchanted club that will poison his enemies with its venomous fangs. In combat he will move with incredible speed and ferocity, and can manifest gems of Fire and Nature for use in spellcasting."
        #gcost 190
        #mr 20
        #prot 5
        #weapon 391 -- Serpent
        #weapon 609 -- Grab and Swallow
        #armor 45 -- Jade Mask
        #popkill 5
        #fear 5
        #onebattlespell 610 -- Quicken Self
        #heal
        #diseaseres 100
        #tmpfiregems 1
        #tmpnaturegems 1
        #gemprod 7 1
        #end

        #selectmonster 2443 -- Teotl of the Sky
        #gcost 200
        #descr "The Teotl is a giant of divine heritage. Once a messenger and representative of the Pantokrator, the Teotl of the Sky was given the authority of the Western Sun and the power of thunder. During the rebellion against the Pantokrator he was bereft of command in favor of a blood-thirsty warrior deity who devoured gods and men. Protesting against the order of things and the carnage released upon the population of the land, he was punished with eternal banishment. With the disappearance of the Pantokrator, he has returned to reform the world in his image. The Teotl can summon Spring Hawks to serve him each month, and in combat will manifest temporary gems of fire, air and nature."
        #mr 20
        #overcharged 1
        #stormimmune
        #heal
        #diseaseres 100
        #tmpairgems 1
        #tmpfiregems 1
        #tmpnaturegems 1
        #makemonsters2 513-- Spring Hawk
        #end

        #selectmonster 2444 -- Teotl of the Underworld
        #descr "The Teotl is a giant of divine heritage. He was placed in charge of the dead and the night by a previous Pantokrator. During day he would care for the dead and during night he would stalk the sick and the dying. Prowling the dark, he observed the night skies and saw the stars. Awestruck and envious, he opened his mouth and devoured them all. The Pantokrator was furious and banished him to the Underworld and stripped him of all flesh so that he could not return. Then he remade the night sky and set a Beast of the Night to hunt those who would covet the stars. With the disappearance of the Pantokrator, the Laws of Death were broken and now the Teotl can return to once more eat the stars. Shrouded in darkness, the Teotl of the Underworld wears a necklace of human eyeballs that shine with the light of the stars. The necklace gives him the ability to see everything that has passed and everything that will come to pass. Death follows the Teotl and the dead will rise from their graves to serve him when his dominion is strong. In combat he will manifest magical astral and death gems to aid in spellcasting."
        #gcost 200
        #mr 20
        #domsummon -2 -- Longdead
        #heal
        #diseaseres 100
        #tmpastralgems 1
        #tmpdeathgems 2
        #end

        #selectmonster 2445 -- Neter of the Underworld
        #descr "The Neter is a giant of divine heritage. He was once given rulership of the world by a previous Pantokrator, but was slain and dismembered by his brother. His wife reassembled him and returned him to life, but his master's decree forbid him to return to the land of the living. Instead he became a King of the Dead. Now with the Pantokrator gone, the Neter of the Underworld is free to break the laws of death and return to the land of the living and claim it as his. The Neter of the Underworld is always accompanied by ancient warriors from the Underworld, and can command ancient bones rise from the desert. In combat he can manifest magical death gems to aid in spellcasting."
        #gcost 180
        #mr 20
        #heal
        #diseaseres 100
        #makemonsters2 1980 -- Dust Warrior
        #tmpdeathgems 3
        #end

        #selectmonster 2446 -- Neter of Kings
        #descr "The Neter is a giant of divine heritage. A previous Pantokrator once gave him rulership over the world, and the things that brings life; the sun in the heaven and the rivers and waters of the world. He was imprisoned for eternity when he joined a rebellion of lesser gods. Now with the Pantokrator gone, the Neter is once more free to claim the world as his. He can summon warriors in his image to serve him, and in combat will manifest magical fire, water and astral gems to aid in spellcasting."
        #gcost 210
        #mr 20
        #prot 5
        #amphibian
        #awe 1
        #heal
        #batstartsum1d6 5270 -- Child of Valley
        #makemonsters2 5270 -- Child of Valley
        #diseaseres 100
        #tmpfiregems 1
        #tmpwatergems 1
        #tmpastralgems 1
        #end

        #selectmonster 2450 -- Horned One
        #descr "The Horned One is a giant of divine heritage once put in charge of the wild by a previous Pantokrator. As lord of beasts, berserkers and wild revelry he is also known as the Horny One. When he made an attempt on a mistress of the Pantokrator he was trapped in a magic forest for eternity, with beasts as only company. In the enchanted forest primordial beasts were also bound. The Golden Boar, the Ram-Headed Serpent and the Great White Stag pledged the Horned One their service. Now, with the Pantokrator gone, the Horned One has found a way out of his woodland prison to once more revel in the world with beasts and men. He is highly resilient and will suffer permanent injuries less often than most creatures."
        #gcost 190
        #mr 20
        #domsummon 284 -- Wolf
        #domsummon2 549 -- Boar
        #woundfend 2
        #heal
        #diseaseres 100
        #end

        #selectmonster 2464 -- Neter of the Moon
        #descr "The Neter is a giant of divine heritage. A previous Pantokrator gave him power knowledge the moon and the stars and the wisdom how to use it. As guardian of the celestial bodies he observed what was, what had been, and what would come to pass. During the rebellion of the lesser gods, he observed but did not interfere on either side. For this the Pantokrator imprisoned him for eternity. Now with the pantokrator gone his astral prison is weakening and he will return to guide the world as he wills. The Neter can summon warriors in his image each month, and in combat can manifest magical astral pearls to aid in spellcasting"
        #gcost 180
        #mr 20
        #prot 5
        #researchbonus 20
        #tmpastralgems 3
        #makemonsters2 5267 -- Child of the Sphinx
        #heal
        #diseaseres 100
        #end

        #selectmonster 2502 -- Earth Made Flesh
        #descr "When the first drops of water made the earth hollow, the Roots of the Earth formed into a womb of stone and black water. The primordial water of Apsu caressed the under-earth and a giant woman formed in the Womb of the Earth. Given life and purpose by the Pantokrator, she was charged with caring for the under-earth. But she fell in love with the subterranean lifeforms and was seduced by the eldest Olm and gave birth to strange amphibian humanoids with external gills and cyclops eyes. At first the Pantokrator tolerated her misstep, and she helped him form the Seal that must not be Broken. But the making of the Seal demanded a huge sacrifice of her progeny, and she demanded compensation for her kin. Angry with her impudence he imprisoned her for eternity. Now with the Pantokrator gone her prison is weakening, but so is the Seal. In combat the Earth Made Flesh can manifest magical Earth and Water gems to aid in spellcasting"
        #gcost 260
        #mr 20
        #prot 8
        #regeneration 10
        #bluntres
        #trample
        #inspirational 1
        #heal
        #diseaseres 100
        #tmpwatergems 1
        #tmpearthgems 3
        #end

        #selectmonster 2552 -- Ahura of Wisdom
        #descr "The Ahuras were three celestial beings given might and splendor by a previous Pantokrator. Greatest of the three was the Lord of Wisdom, who in constant struggle with his wicked twin, the Destructive Spirit, would lead men to good deeds. Banished from this world, he has acted through his prophets and his divine messengers, the six Amesha Spentas. Now with the Pantokrator gone, he can return to the world and claim it as his own. The Ahura can call Yazatas from the celestial realm to aid him, and in combat can manifest magical astral gems to aid in spellcasting"
        #gcost 190
        #mr 20
        #prot 5
        #heal
        #makemonsters2 1607 -- Yazad
        #diseaseres 100
        #tmpastralgems 3
        #end

        #selectmonster 2556 -- Celestial Carp
        #descr "The Celestial Carp once dwelled in a decorative pond in the gardens of the Pantokrator. For eternities he lurked under the surface of those still waters, occasionally surfacing to spy on the ruler of all, learning forbidden names and the secrets of the divine. When he realized the Pantokrator had disappeared he decided to usurp the place of his master using the secrets he had gleaned during his patient wait. Used to feed on the flesh of those that displeased his master he has developed a hunger for the flesh of men, especially the clergy of other would be gods. In combat he will manifest magical air and water gems to aid in spellcasting."
        #gcost 190
        #mr 20
        #prot 12
        #prec 13
        #fear 5
        #heal
        #giftofwater 500
        #diseaseres 100
        #tmpwatergems 2
        #tmpairgems 1
        #end

        #selectmonster 2610 -- Angra Mainyu
        #descr "The Angra Mainyu, Spirit of Destruction, was given half the world by a previous Pantokrator. While his wise twin was appointed as the guide of men and claimed as followers those who did good deeds, the Angra Mainyu became the lord of the wicked and the cruel, worshipped by sinners and deceitful men, and he would tempt and lead astray those who followed his benevolent twin. When the Pantokrator finally tired of the game of the opposing principles, he imprisoned them both for eternity. But now the Pantokrator is gone, and the Angra Mainyu feels his prison weaken. Now he will rule this world without an opposing principle to stop him. In combat he can manifest magical fire and death gems to aid in spellcasting. Each month he will gather pure souls from the unsupecting populace."
        #gcost 210
        #mr 20
        #prot 5
        #banefireshield 8
        #batstartsum2 2630 -- Daeva
        #heal
        #diseaseres 100
        #tmpfiregems 1
        #tmpdeathgems 1
        #gemprod 7 1
        #end

        #selectmonster 2685 -- Viracocha
        #descr "The Viracocha is a giant of divine heritage. He was the servant of a previous Pantokrator and aided him in the creation of the world. He was given the splendor of the sun, the rage of the storm and the life-bringing rain as tools and was commanded to bring civilization and order to creation. But the splendor of the sun gave the Viracocha hubris and he tried to create men to inhabit the world. He breathed air into stones and created a race of giants. But the stone giants were imbecilic and violent and they threatened other races created by the Pantokrator. The Pantokrator was displeased and took the rain from his servant and drowned the giants with a great flood. The Viracocha was imprisoned for eternity. Now, with the Pantokrator gone, the Viracocha can return and bring splendor to the world and claim it as his. In combat he will manifest gems of Fire, Air and Water magic to aid in spellcasting."
        #gcost 210
        #mr 20
        #prot 5
        #heal
        #makemonsters2 474 -- Living Statue
        #diseaseres 100
        #tmpfiregems 1
        #tmpairgems 1
        #tmpwatergems 1
        #end

        #selectmonster 2686 -- Apu Inti
        #clearweapons
        #descr "The Apu Inti, Lord Sun, is a giant of divine heritage. When his father sinned against the Pantokrator the Apu Inti inherited the splendor of the sun in his fathers stead. He was given dominion over the Sun and everything upon which it shone. But the Inti became enamored with the beings underneath the sun and the Huacas of the land. He sired the Ayar brothers, divine heroes unchecked by the Pantokrator. The Lord of the world, found the Inti's insolence even worse than his father's, and the Lord Sun and his siblings were imprisoned in caves with unbreakable doors of Garnet and Gold. But now, with the Pantokrator gone, the doors of Garnet and Gold are weakening and they shudder with each strike of the imprisoned Sun. When they finally shatter, the Apu Inti will reclaim a world once his. In combat he can manifest magical fire gems to aid in spellcasting."
        #gcost 190
        #mr 20
        #prot 5
        #heat 10
        #hp 108
        #weapon 20 -- Bite
        #weapon 618 -- Sun Spear
        #heal
        #diseaseres 100
        #tmpfiregems 3
        #end

        #selectmonster 2698 -- Apu Illapa
        #descr "The Apu Illapa, Lord Lightning, is a giant of divine heritage. When his father sinned against the previous Pantokrator the Apu Illapa inherited the rage of the storms in his fathers stead. He was given dominion over the Sky and the Winds and everything in the air. During the rebellion against the Pantokrator he allowed the Sun to remain in the sky and for this he was punished with eternal imprisonment. Now with the Pantokrator gone his shackles are weakening and vast thunder clouds are gathering. Soon he will be free to claim all beneath the sky once more. In combat he will manifest gems of Air magic to aid in spellcasting."
        #clearweapons
        #gcost 190
        #mr 20
        #prot 5
        #stormpower 3
        #overcharged 1
        #weapon 1846 -- Thunder Bolt
        #weapon 404 -- Beak
        #weapon 231 -- Thunder Fist
        #heal
        #diseaseres 100
        #tmpairgems 3
        #end

        #selectmonster 2755 -- Hun Came
        #descr "The Hun Came, One Death, is a giant of divine heritage once placed in charge of the beginning of the cycle of death by a previous Pantokrator. He would cause death and lamentation to the world. He saw that everything in the world would come under his reign sooner or later, but this was not entirely true. Dominion over the cycle of death was shared with his brother Vucub Came, Seven Death. He was given the end of the cycle by the Pantokrator. Hun Came came to realize that his brother represented the end of Hun Came authority and he became jealous. The One Death shared his powers with nine lieutenants, the Bolon-ti-ku, the Nine-Gods, and with their aid he slew his brother and ended the cycle of death and rebirth. With the breach of the natural order set up by the Pantokrator the world became depopulated, wild and inhospitable. The world was plunged into darkness and the dead threatened to replace the living. The Pantokrator became furious and punished his rebellious servant with eternal imprisonment. Now, with the Pantokrator gone, the prison of the Hun Came is weakening and the world might once more be plunged in Darkness. In combat he can manifest magical earth and death gems to aid in spellcasting."
        #gcost 210
        #mr 20
        #heal
        #diseaseres 100
        #tmpearthgems 1
        #tmpdeathgems 2
        #end

        #selectmonster 2849 -- Father of Monsters
        #clearweapons
        #gcost 240
        #mr 20
        #prot 18
        #def 14
        #hp 128
        #weapon 650 -- Torch
        #weapon 649 -- Crab Claw
        #weapon 649 -- Crab Claw
        #weapon 532 -- Tail Sweep
        #heal
        #diseaseres 100
        #end

        #selectmonster 2851 -- Titan of Winds & Waves
        #descr "The Titan of Winds and Waves is a giant of divine heritage. She was given authority and power over the winds and the waves by a previous Pantokrator, but she fell in love with mankind and taught them the secrets of sailing, ship crafting and trade. When the Pantokrator heard the prayers of sailors and tradesmen he realized that she had betrayed his trust and he punished her with eternal imprisonment. The Titan of Winds and Waves can bring men across the sea as well as into the sea. She can also manifest magical water and air gems to aid in spellcasting, and is served by the elements themselves.  In combat the wind will guide the projectiles of her allies, granting greater accuracy."
        #gcost 210
        #mr 20
        #prot 5
        #batstartsum1 570 -- Size 3 Air Elemental
        #batstartsum2 412 -- Size 2 W Elemental
        #armor 148 -- Crown
        #onebattlespell 629 -- Wind Guide
        #giftofwater 500
        #ambidextrous 5
        #heal
        #diseaseres 100
        #tmpairgems 2
        #tmpwatergems 1
        #end

        #selectmonster 2856 -- Son of the Sea
        #cleararmor
        #descr "The Son of the Sea is an ancient demigod sprung from the Sea. Once the servant of the previous Pantokrator he became the psychopomp of the watery dead. He would take the newly dead on his boat 'Wave Sweeper' to the Land of the Watery Dead, where they would live on in the gloomy halls underneath the stormy seas. When the Fomorians rebelled against the Pantokrator and ravaged the world, the Son of the Sea was punished for having given them the means to escape their prison land. The Son of the Sea was imprisoned for millennia, but with the Pantokrator gone, his shackles are weakening and he is ready to enter the world of the living as God and ruler of all. The Son of the Sea is strongly attuned to the sea and the storms, but his task of guiding the souls of the dead has given him great powers over the dead. He is a master of trickery and illusions and can hide his true appearance. With his ship, 'Wave Sweeper', he can bring his followers across the sea, or he can bless them with water-breathing. He will also manifest magical water, air and death gems to aid in spellcasting,"
        #gcost 220
        #mr 20
        #prot 5
        #raiseonkill 50
        #armor 50 -- Weightless Scale
        #armor 92 -- Enchanted Shield
        #armor 148 -- Crown
        #heal
        #giftofwater 500
        #diseaseres 100
        #tmpairgems 1
        #tmpwatergems 1
        #tmpdeathgems 1
        #end

        #selectmonster 2980 -- Uttervast
        #gcost 340
        #heal
        #diseaseres 100
        #end

        #selectmonster 3072 -- Kami of the Sun
        #descr "The Kami of the Sun is the daughter of the previous Pantokrator. Together with her two brothers she was brought into existence when the Pantokrator grieved after banishing his wife to the netherworld. When he wiped his left eye a divine spark was set free and hastened to the heavens. Watching the spark he called it daughter. She reminded him of his banished wife and he endowed her with the brilliance and beauty of the sun. But when the people of the world started to worship her instead of him he got jealous and imprisoned her in a cave for eternity. Now with the Pantokrator gone the Kami of the Sun is returning to the world to bring joy and prosperity to mankind. In combat she will manifest gems of Fire and Astral magic to aid in spellcasting."
        #gcost 220
        #heal
        #mr 20
        #prot 5
        #diseaseres 100
        #tmpfiregems 2
        #tmpastralgems 1
        #end

        #selectmonster 3073 -- Kami of the Moon
        #descr "The Kami of the Moon is the son of the previous Pantokrator. Together with his brother and sister he was brought into existence when the Pantokrator grieved after banishing his wife to the netherworld. When he wiped his right eye a divine spark was set free and hastened to the heavens. Watching the spark he called it son. After giving his daughter the mandate of the sun the Pantokrator grudgingly gave his son the mandate of the moon and the night. But when the people of the world started to worship the sun and the moon instead of him, the Pantokrator got jealous and imprisoned them for eternity. Now with the Pantokrator gone, the Kami of the Moon is breaking free. The Kami of the Moon is sometimes called upon to deliver his followers from deadly diseases. In ages past he offended the sun and now it will not shine upon him, causing a Solar Eclipse wherever he goes. In combat he will manifest gems of Death and Astral magic to aid in spellcasting."
        #gcost 210
        #heal
        #mr 20
        #prot 5
        #onebattlespell 648 -- Solar Eclipse
        #diseaseres 100
        #tmpdeathgems 1
        #tmpastralgems 2
        #end

        #selectmonster 3074 -- Kami of Storms
        #clearweapons
        #descr "The Kami of Storms is the son of the previous Pantokrator. Together with his brother and sister he was brought into existence when the Pantokrator grieved after banishing his wife to the netherworld. When he blew his nose a divine breath was set free and hastened to the heavens. Watching the breath he hesitated to call it son. After giving his daughter the mandate of the sun and his first son the mandate of the moon, the Pantokrator no longer felt obliged to grant a celestial mandate to his snot-born child. Instead he gave it the mandate of the storms and the sea. Infuriated the third child of the Pantokrator rebelled and caused destruction and disorder wherever he went. With rising ire the Pantokrator watched the misdeeds of his unwanted son and finally imprisoned him for eternity. Now with the Pantokrator gone, the Kami of Storms is breaking free to once more wreak havoc upon the world. He wields an enchanted blade known as the Grasscutter Sword which can unleash blasts of air to stun opponents. In combat he will manifest gems of Air and Water magic to aid in spellcasting."
        #gcost 210
        #heal
        #prot 5
        #amphibian
        #mr 20
        #weapon 1852 -- Kusanagi
        #diseaseres 100
        #tmpairgems 2
        #tmpwatergems 1
        #stormpower 3
        #end

        #selectmonster 3076 -- Morrigna
        #descr "The Morrigna is a tripartite entity of divine heritage. She is one with her sisters and her sisters are one with her. The trinity was given power and purpose by a previous Pantokrator. One sister was given mandate over battles, one sister was given mandate over life and death, and one sister was given mandate over the fates of men. She was the Battle Crow, witnessing the slaughter and the dying, pecking at the bodies of the fallen ones. Out of fear and desperation men begun to worship the sisters and they tried to placate her with sacrifice and prayer. The Morrigna accepted their worship and turned battles to their favor. Furious the Pantokrator imprisoned her in the Charnel House, a labyrinthine prison of human bones. Now with the Pantokrator gone the prison is crumbling and the sisters will once more decide the fates of men. The Morrigna is a trinity in one being and can separate her forms at will."
        #gcost 300
        #triplegodmag 0
        #magicboost 1 1
        #heal
        #prot 5
        #mr 20
        #pathcost 80
        #diseaseres 100
        #end

        #selectmonster 3077 -- Morrigna
        #descr "The Morrigna is a tripartite entity of divine heritage. She is one with her sisters and her sisters are one with her. The trinity was given power and purpose by a previous Pantokrator. One sister was given mandate over battles, one sister was given mandate over life and death, and one sister was given mandate over the fates of men. She was the Battle Crow, witnessing the slaughter and the dying, pecking at the bodies of the fallen ones. Out of fear and desperation men begun to worship the sisters and they tried to placate her with sacrifice and prayer. The Morrigna accepted their worship and turned battles to their favor. Furious the Pantokrator imprisoned her in the Charnel House, a labyrinthine prison of human bones. Now with the Pantokrator gone the prison is crumbling and the sisters will once more decide the fates of men. The Morrigna is a trinity in one being and can separate her forms at will."
        #gcost 300
        #triplegodmag 0
        #magicboost 5 1
        #heal
        #prot 5
        #mr 20
        #diseaseres 100
        #end

        #selectmonster 3078 -- Morrigna
        #descr "The Morrigna is a tripartite entity of divine heritage. She is one with her sisters and her sisters are one with her. The trinity was given power and purpose by a previous Pantokrator. One sister was given mandate over battles, one sister was given mandate over life and death, and one sister was given mandate over the fates of men. She was the Battle Crow, witnessing the slaughter and the dying, pecking at the bodies of the fallen ones. Out of fear and desperation men begun to worship the sisters and they tried to placate her with sacrifice and prayer. The Morrigna accepted their worship and turned battles to their favor. Furious the Pantokrator imprisoned her in the Charnel House, a labyrinthine prison of human bones. Now with the Pantokrator gone the prison is crumbling and the sisters will once more decide the fates of men. The Morrigna is a trinity in one being and can separate her forms at will."
        #gcost 300
        #triplegodmag 0
        #magicboost 4 1
        #heal
        #prot 5
        #mr 20
        #diseaseres 100
        #end

        #selectmonster 3079 -- Duiu of Thunder
        #descr "The Duiu of Thunder is a giant of divine heritage once given mandate over thunder and the shifting seasons by a previous Pantokrator. He was given divine regalia to match his mandate, the Wheel of the Turning Year and the Thunderbolt. With the power to mete out thunderous justice, combined with the power to influence that which is important to mankind, namely the harvests during fall, the cold and death of winter, the return of life in the spring, and the growth of summer, his influence over mankind was greater than the Pantokrator's other servants. Hearing the pleas and prayers of men his hubris grew and he accepted worship from mankind. Furious the Pantokrator imprisoned him for eternity. Now with the Pantokrator gone his prison is failing and mankind can once more trust in the cycle of the Turning Year. As Master of the Seasons he is attended by the Seasonal spirits which will appear to serve him each month. In combat he will manifest gems of Fire, Air and Water  magic to aid in spellcasting."
        #gcost 200
        #heal
        #prot 5
        #mr 20
        #yearturn 3
        #summon1 -5185
        #diseaseres 100
        #tmpfiregems 1
        #tmpairgems 1
        #tmpwatergems 1
        #end

        #selectmonster 3080 -- Duiu of Farming
        #descr "The Duiu of Farming is a giant of divine heritage once given mandate over rural life by a previous Pantokrator. Crops, cattle and fertility were his to command. He taught mankind how to grow and harvest crops, make wine and brew beer, and care for cattle of all kinds. So great were his gifts to mankind that men lost faith in their true God, the Pantokrator. Furious the Pantokrator gave his servant wine and beer to drink, and the Duiu drank and fell asleep, a sleep from which he would never awake. But now, with the Pantokrator gone, his drunken stupor has lost its enchantment and the Duiu of Farming is slowly waking up. The Duiu of Farming wields a great hammer and carries a bowl that produces gems of nature. Each month he can summon cattle from his Divine Herd to this realm. In combat he will manifest gems of Water and Nature magic to aid in spellcasting."
        #gcost 210
        #heal
        #mr 20
        #prot 5
        #batstartsum2 5998 -- Divine Cattle
        #makemonsters2 5998 -- Divine Cattle
        #diseaseres 100
        #tmpnaturegems 2
        #tmpwatergems 1
        #end

        #selectmonster 3081 -- Duiu of War
        #descr "The Duiu of War is a giant of divine heritage once given mandate over war and conflict by a Previous Pantokrator. Eager to consummate his mandate the Duiu of War brought strife and turmoil to mankind. Alliances were broken and families divided, tribes were slaughtered and villages burned to the ground. The prayers of mankind took a different tone and the Pantokrator listened and watched. He witnessed a war torn land where mankind no longer prayed to him, but to the destroying god of war and strife. Furious the Pantokrator shackled the Duiu of War on a slab of stone with bonds of lightning, planted an oak atop the shackled Duiu and told the oak to grow for eternity. But now, with the Pantokrator gone, the oak is dying and the power of thunder has been claimed by an unknown entity. Soon the oak will have rotted away and mankind will once more slay their kin. In combat he will manifest gems of Fire and Nature magic to aid in spellcasting, and each month he will select from amongst his followers those of pure blood to serve him as blood slaves. Barbarian warriors will be drawn to the Duiu of War as his Dominion grows strong."
        #gcost 200
        #heal
        #mr 20
        #incscale 0
        #chaospower 1
        #diseaseres 100
        #tmpfiregems 1
        #tmpnaturegems 1
        #gemprod 7 1
        #domsummon2 139 -- Barbarian
        #end

        #selectmonster 3082 -- Matrona of the Healing Spring
        #descr "The Matrona of the Healing Spring is a giant of divine heritage placed to guard a spring by a previous Pantokrator. For some reason the spring was special to the Pantokrator and he instilled his power in the sparkling waters. He told the Matrona never to bathe in the sacred waters and to keep them ever clean. But the Matrona of the Spring couldn't resist and bathed in the blessed waters, and the powers instilled in the spring were suffused in her body. And mankind came to her and begged her to heal them from all ills, and they gave her their prayers. But one day the Pantokrator returned to take his centennial bath, and discovered that the reinvigorating powers of the spring was lost and its waters befouled by the filth of men. Furious he threw the Matrona into the spring and placed a cliff on top of it imprisoning her for eternity. But now, with the Pantokrator gone, a trickle of water is eroding the stone. Soon the Matrona of the Healing Spring will once more bring mercy and wisdom to mankind. In combat she will manifest gems of Water, Astral and Nature magic to aid in spellcasting."
        #gcost 200
        #heal
        #mr 20
        #regeneration 10
        #autohealer 3
        #diseaseres 100
        #tmpwatergems 1
        #tmpastralgems 1
        #tmpnaturegems 1
        #end

        #selectmonster 3086 -- Deives of the Sun
        #descr "The Deives of the Sun is a giant of divine heritage, once given the splendor of the Sun by a previous Pantokrator. Every morning she rose and brought warmth, comfort and fertility to frozen lands. So great were her powers and so great her mercy that mankind turned away from the Pantokrator and gave her their prayers instead. Furious he stripped her of authority and imprisoned her for eternity. But now, with the Pantokrator gone, her prison is crumbling and the Deives can reclaim her lost authority and bring the warmth and comfort of the Sun to mankind once more. The Deives is associated with the legendary Firebirds and can summon one each month to bring good fortune to her followers. In combat she will manifest gems of Fire and Nature magic to aid in spellcasting."
        #gcost 220
        #heal
        #prot 5
        #mr 20
        #makemonsters1 1946 -- Firebird
        #diseaseres 100
        #tmpfiregems 2
        #tmpnaturegems 1
        #end

        #selectmonster 3088 -- Dharmapala of the Underworld
        #descr "The Dharmapalas were giants of divine heritage tasked by a previous Pantokrator with upholding the celestial laws. The Dharmapala of the Underworld was given mandate over the laws of death and rebirth. He took the Throne of the Underworld and made himself judge and ruler of the dead. His demonic servants guided the dead to their next life. But with time his mind darkened, and he was seduced by jealousy and hunger for power. He demanded prayers from men in return for a good afterlife. His judgment lost righteousness and his servant demons wrongfully led the newly dead to the hellish realm of the Dharmapala. When the Pantokrator realized that his most trusted servant had broken the laws of death and rebirth to collect the prayers of men he was furious and imprisoned the Dharmapala in this world, disconnected from the Hell he had created. Now with the Pantokrator gone the Dharmapala is free to once more judge the dead as he sees fit. Each month the Dharmapala can summon one of his sacred Amanojaku guards from the Underworld to serve in this realm. In combat he will manifest gems of Fire and Death magic to aid in spellcasting."
        #gcost 200
        #heal
        #prot 5
        #mr 20
        #makemonsters1 3084 -- Amanojaku
        #diseaseres 100
        #tmpfiregems 1
        #tmpdeathgems 2
        #end

        #selectmonster 3124 -- Titan of Forethought
        #gcost 220
        #heal
        #mr 20
        #diseaseres 100
        #tmpfiregems 1
        #tmpwatergems 1
        #tmpastralgems 1
        #end

        #selectmonster 3203 -- Titan of the Hunt
        #clearweapons
        #descr "The Titan of the Hunt is a giant of divine heritage born at the dawn of time times. She became a huntress of primordial beasts and was entrusted by a previous Pantokrator to care for the wilderness. He gave her a golden bow and authority over the animals of the wild. But mankind began to make offerings to the Huntress and she let herself be worshipped and promised mankind that she would protect the huntsmen and the unmarried women of the world. The Pantokrator sent his servants to punish her for her breach of trust, but she transformed herself into a deer and escaped into the First Forest. Here she met another of the Pantokrator's servants and he gave her hounds to aid her in the Hunt. The Pantokrator was furious and trapped them both in the forest for eternity. Now with the Pantokrator gone the paths of the First Forest are once more open and the Titan of the Hunt can once more hear the prayers of young women and huntsmen. With primordial monsters emerging anew the Titan of the Hunt has taken it upon herself to rid the world of monsters and claim the world as hers. The Titan of the Hunt is always accompanied by her sacred hounds, as well as animals of the wild. She wields a golden bow created by the Titan of the Forge that will always strike the heart of any target, and in combat she can manifest nature gems to aid in spellcasting."
        #gcost 180
        #heal
        #mr 20
        #prot 5
        #weapon 1853 -- Golden Bow
        #weapon 674 -- Bronze Dagger
        #diseaseres 100
        #tmpnaturegems 3
        #end

        #selectmonster 3205 -- Titan of Crossroads
        #gcost 300
        #heal
        #mr 20
        #prot 5
        #diseaseres 100
        #triplegodmag 0
        #magicboost 5 1
        #pathcost 80
        #end

        #selectmonster 3206 -- Titan of Crossroads
        #gcost 300
        #heal
        #mr 20
        #prot 5
        #diseaseres 100
        #triplegodmag 0
        #magicboost 4 1
        #end

        #selectmonster 3207 -- Titan of Crossroads
        #gcost 300
        #mr 20
        #prot 5
        #heal
        #diseaseres 100
        #triplegodmag 0
        #magicboost 6 1
        #end

        #selectmonster 3208 -- Titan of the Spring
        #gcost 250
        #mr 20
        #prot 5
        #heal
        #diseaseres 100
        #tmpdeathgems 2
        #tmpnaturegems 1
        #end

        #selectmonster 3209 -- Titan of Growth
        #descr "The Titan of Growth is a giant of divine heritage born in primordial times. She was once given power over all things growing by a previous Pantokrator. Her blessings and gifts to mankind were numerous and she brought fertility and life to the world. When her daughter was abducted by the Great Adversary she was devastated and abandoned her tasks and all life withered and died. The Pantokrator was furious, imprisoned her for eternity and gave the dominion of growth to a better suited candidate. Now with the Pantokrator gone, the Titan of Growth can return to the world and reclaim her former glory and possibly find her missing daughter. The province where the Titan dwells will be lush and the harvest will be extraordinary, granting thirty percent additional tax revenue to the treasury. In combat she will manifest gems of Earth and Nature magic to aid in spellcasting."
        #gcost 200
        #mr 20
        #prot 5
        #heal
        #diseaseres 100
        #tmpearthgems 1
        #tmpnaturegems 2
        #end

        #newevent
        #rarity 5
        #req_monster 3209 -- Titan of Growth
        #msg "The province has bloomed in the presence of ##godname##!"
        #nolog
        #taxboost 30
        #end
    -- DOM 2 ---

        #selectmonster 120 -- Moloch
        #clearmagic
        #heal
        #diseaseres 100
        #gcost 220
        #fireshield 8
        #magicskill 0 1
        #magicskill 7 1
        #spreaddom 2
        #end

        #selectmonster 138 -- Gorgon
        #clearmagic
        #gcost 200
        #woundfend 2
        #diseaseres 100
        #prot 12
        #magicskill 3 1
        #magicskill 6 1
        #spreaddom 2
        #end

        #selectmonster 179 -- Master Lich
        #clearmagic
        #gcost 160
        #magicskill 5 2
        #heal
        #diseaseres 100
        #end

        #selectmonster 215 -- Virtue
        #clearmagic
        #gcost 200
        #spreaddom 2
        #magicskill 0 1
        #magicskill 1 1
        #onebattlespell 604 -- Personal Luck
        #heal
        #diseaseres 100
        #end

        #selectmonster 216 -- Dragon
        #gcost 220
        #heal
        #diseaseres 100
        #pathcost 40
        #end

        #selectmonster 226 -- Dragon Archmage
        #gcost 220
        #heal
        #diseaseres 100
        #pathcost 40
        #end

        #selectmonster 265 -- Dragon
        #gcost 220
        #heal
        #diseaseres 100
        #pathcost 40
        #end

        #selectmonster 266 -- Dragon
        #gcost 220
        #heal
        #diseaseres 100
        #pathcost 40
        #end

        #selectmonster 267 -- Dragon Frost Father
        #gcost 220
        #heal
        #diseaseres 100
        #pathcost 40
        #end

        #selectmonster 268 -- Dragon Master Enchanter
        #gcost 220
        #heal
        #diseaseres 100
        #pathcost 40
        #end

        #selectmonster 269 -- Wyrm
        #clearmagic
        #gcost 170
        #prot 20
        #heal
        #diseaseres 100
        #magicskill 2 2
        #end

        #selectmonster 320 -- Saurolich
        #gcost 180
        #heal
        #diseaseres 100
        #end

        #selectmonster 383 -- Prince of Death
        #clearmagic
        #gcost 200
        #spreaddom 2
        #prot 5
        #invulnerable 20
        #inspirational 1
        #magicskill 5 2
        #heal
        #raredomsummon 634 -- Handmaiden of Death
        #diseaseres 100
        #end

        #selectmonster 395 -- Lich Queen
        #descr "A Lich Queen is the dried husk of an ancient queen adept in Death magic. Through dark rituals, she succeeded in mastering one of mankind's oldest and most urgent goals, to defeat death. By removing her viscera and hiding it outside her body, she is virtually impossible to slay. Should the body be physically destroyed, a new one is formed from the dust of dead humans. Being immortal, there is nothing left to acquire but godhood. As her Dominion grows strong followers will arise from their ancient resting places to serve her. The Lich Queen lacks the Arch Mage's broad base of magical knowledge, but her reanimated body is more durable than a living body since it is leathery and dry and lacks all organs of importance."
        #clearmagic
        #gcost 170
        #heal
        #diseaseres 100
        #magicskill 0 1
        #magicskill 5 1
        #domsummon20 1980 -- Dust Warrior
        #end

        #selectspell 142
        #name "Power of the Crescent Moon"
        #descr "This spell grants all friendly wolves protection from weapons."
        #details "Invulnerability: 15"
        #school -1
        #researchlevel 0
        #range 0
        #fatiguecost 0
        #effect 23
        #nreff 1
        #aoe 666
        #damage 1099511627776 -- Wolves gain Invuln 15
        #spec 281474989309952 -- Ignores Shields, Friendlies Animals Only, uwok
        #end

        #selectmonster 401 -- Bitch Queen
        #clearmagic
        #descr "The Bitch Queen is a werewolf enchantress of tremendous magical power. The Bitch Queen usually appears in the form of an old hag, but when angered, she transforms into a werewolf. She is attended by a pack of werewolves that do her bidding. Even though her werewolf form is more powerful than her human form, it is still quite weak when compared to the majority of the other Pretender Gods. Werewolves flock to her dominion, and whenever she goes into battle, wolves will come to her aid. Wolves fighting with the Bitch Queen will be granted her protection and will be difficult to harm with mortal weapons."
        #gcost 140
        #startdom 2
        #magicskill 4 1
        #magicskill 6 1
        #domsummon2 633 -- Werewolves
        #heal
        #diseaseres 100
        #onebattlespell 142 -- Blessing of the Bitch Queen
        #end

        #selectmonster 402 -- Bitch Crone
        #clearmagic
        #descr "The Bitch Queen is a werewolf enchantress of tremendous magical power. The Bitch Queen usually appears in the form of an old hag, but when angered, she transforms into a werewolf. She is attended by a pack of werewolves that do her bidding. Even though her werewolf form is more powerful than her human form, it is still quite weak when compared to the majority of the other Pretender Gods. Werewolves flock to her dominion, and whenever she goes into battle, wolves will come to her aid. Wolves fighting with the Bitch Queen will be granted her protection and will be difficult to harm with mortal weapons."
        #gcost 140
        #startdom 2
        #invulnerable 15
        #magicskill 4 1
        #magicskill 6 1
        #domsummon2 633 -- Werewolves
        #heal
        #diseaseres 100
        #onebattlespell 142 -- Blessing of the Bitch Queen
        #end

        #selectmonster 608 -- Phoenix
        #descr "The Phoenix is an immortal heron sprung from the rays of the sun at the first dawn. It is skilled in Air and Fire magic but lacks the physical strength of most Pretenders. If killed in combat it will explode in flames and be immediately reborn. The Phoenix shines with a divine light that will cause weak willed foes to falter rather than strike it."
        #gcost 130
        #prot 12
        #awe 1
        #onebattlespell 679 -- Phoenix Pyre
        #heal
        #diseaseres 100
        #end

        #selectmonster 643 -- Bog Mummy
        #gcost 160
        #heal
        #diseaseres 100
        #end

        #selectmonster 644 -- Dracolich
        #gcost 220
        #prot 16
        #immortal
        #heal
        #diseaseres 100
        #end

        #selectmonster 645 -- Dracolich Bog Mummy
        #gcost 220
        #immortal
        #heal
        #diseaseres 100
        #end

        #selectmonster 655 -- Scorpion King
        #clearmagic
        #descr "The Scorpion King is a spirit of destruction and terror given divine powers by a previous Pantokrator. The beast was placed as a monstrous guardian of the path to immortality beyond Mount Mashu. With the disappearance of the Pantokrator, the beast has claimed the secrets beyond Mashu and arrived to wreak destruction and havoc upon the world. The Scorpion King is a hideous and twisted being with the body of a huge scorpion and the upper torso of a scaly human. It is a mighty beast capable of striking simultaneously with its scorpion stinger and any forged weapons it may carry."
        #gcost 170
        #magicskill 3 2
        #heal
        #diseaseres 100
        #end

        #selectmonster 661 -- Shedu
        #clearmagic
        #gcost 170
        #prot 20
        #heal
        #diseaseres 100
        #magicskill 4 2
        #end

        #selectmonster 779 -- Rams Head Serpent
        #clearmagic
        #clearweapons
        #gcost 160
        #weapon 300 -- Headbutt
        #weapon 90 -- Crush
        #weapon 203 -- Barbed Tail
        #magicskill 3 1
        #magicskill 6 1
        #prot 16
        #heal
        #diseaseres 100
        #woundfend 2
        #end

        #selectmonster 872 -- Ghost King
        #clearmagic
        #gcost 150
        #invulnerable 10
        #magicskill 5 2
        #heal
        #spreaddom 2
        #diseaseres 100
        #end

        #selectmonster 874 -- Divine Emperor
        #gcost 120
        #inspiringres 1
        #commaster
        #heal
        #diseaseres 100
        #end

        #selectmonster 973 -- Ancient Kraken
        #clearmagic
        #gcost 150
        #prot 18
        #heal
        #diseaseres 100
        #woundfend 2
        #magicskill 4 1
        #magicskill 6 1
        #twiceborn 1235 -- Leviathan
        #end

        #selectmonster 639 -- Kraken King
        #twiceborn 1235 -- Leviathan
        #end

        #selectmonster 438 -- Kraken
        #twiceborn 1235 -- Leviathan
        #end

        #selectmonster 978 -- Great Black Bull
        #clearmagic
        #gcost 150
        #prot 14
        #diseaseres 100
        #woundfend 2
        #magicskill 6 2
        #end

        #selectmonster 979 -- Great White Bull
        #clearmagic
        #gcost 170
        #diseaseres 100
        #woundfend 2
        #prot 14
        #magicskill 3 2
        #end

        #selectmonster 1026 -- Carrion Dragon
        #clearspec
        #descr "The Carrion Dragon is the living carcass of an ancient Dragon that was powerful enough not to remain dead after it was slain. Pure hatred and vengeful thoughts rejoined the soul of the dead Dragon with its moss-covered corpse. The Carrion Dragon is able to adopt the shape of a dead Pan. The body of the Carrion Dragon is less suited for spell casting than the shape of the Pan. In dragon shape most of its magic skills are reduced. The paths of Death and Nature are innate to the Carrion Dragon and his skills in those paths are less reduced. The presence of a Carrion Dragon will spread a sleeping sickness, and nearby enemies may fall into a dreamless slumber. In lands free of civilization it will grow stronger, but it will weaken where men toil."
        #lizard
        #gcost 200
        #att 14
        #def 7
        #str 17
        #ap 8
        #slothpower 1
        #sleepaura 9
        #enc 1
        #pooramphibian
        #pierceres
        #magicbeing
        #neednoteat
        #forestsurvival
        #spiritsight
        #poisonres 25
        #plant
        #fear 10
        #stealthy 0
        #reanimpriest
        #shapechange 1027
        #gcost 200
        #heal
        #magicboost 53 -2
        #magicboost 5 1
        #magicboost 6 1
        #diseaseres 100
        #woundfend 2
        #heal
        #diseaseres 100
        #woundfend 2
        #startdom 2
        #pathcost 40
        #itemslots 323712 -- crown and 4 misc slots for all the tentacle porn 
        #end

        #selectmonster 1027 -- Carrion Lord
        #clearspec
        #descr "A Carrion Lord is a dead Pan reanimated and given unholy powers by a Panic Apostate, who forces the soul of the dead Pan to rejoin its own moss-covered carcass. The carcass is entwined with vines and roots that have a life of their own. The Carrion Lord is a powerful wielder of Nature magic, but is also given unholy powers over the dead. The Carrion Lord can create manikins by animating vines, roots and the bones of dead beasts. The presence of a Carrion Lord will spread a sleeping sickness, and nearby enemies may fall into a dreamless slumber. In lands free of civilization it will grow stronger, but it will weaken where men toil."
        #gcost 200
        #att 7
        #def 7
        #str 17
        #ap 8
        #slothpower 1
        #sleepaura 9
        #enc 2
        #pooramphibian
        #pierceres
        #magicbeing
        #neednoteat
        #forestsurvival
        #spiritsight
        #poisonres 25
        #plant
        #fear 5
        #stealthy 0
        #reanimpriest
        #shapechange 1026
        #heal
        #diseaseres 100
        #woundfend 2
        #startdom 2
        #pathcost 40
        #itemslots 64646 -- lots of misc slots for tentacle porn 
        #end

        #selectmonster 1229 -- Son of Fenrer
        #clearmagic
        #clearweapons
        #gcost 150
        #prot 15
        #heal
        #weapon 20 -- Bite
        #weapon 29 -- Claw
        #diseaseres 100
        #magicskill 2 2
        #end

        #selectmonster 1349 -- Devourer of Souls
        #clearmagic
        #gcost 150
        #prot 20
        #heal
        #diseaseres 100
        #magicskill 5 2
        #end

        #selectmonster 1428 -- Bakemono Kunshu
        #gcost 170
        #heal
        #diseaseres 100
        #end

        #selectmonster 1581 -- Risen Oracle
        #gcost 170
        #heal
        #diseaseres 100
        #end

        #selectmonster 1590 -- Ageless Olm
        #clearweapons
        #gcost 160
        #prot 16
        #regeneration 10
        #bluntres
        #makemonsters1 2492 -- Great Olm
        #weapon 449 -- Life Drain
        #weapon 1845 -- Mental Mastery
        #heal
        #diseaseres 100
        #end

        #selectmonster 2137 -- Urmahlullu
        #clearmagic
        #gcost 160
        #ethereal
        #heal
        #prot 16
        #magicskill 1 1
        #magicskill 4 1
        #diseaseres 100
        #end

        #selectmonster 2138 -- Sphinx
        #clearmagic
        #clearweapons
        #descr "The Sphinx is a guardian spirit given divine might by a previous Pantokrator. It has realized the aspirations of the other Pretenders and will protect its subjects by donning the mantle of God itself. It has the appearance of a great winged lion with the head of a woman. The Sphinx is attuned to the powers of the stars and the Underworld. It is protected from harm by a Divine aura that turns away mortal weapons."
        #gcost 160
        #invulnerable 20
        #weapon 197 -- Gaze of Death
        #weapon 29 -- Claw
        #weapon 29 -- Claw
        #magicskill 4 1
        #magicskill 5 1
        #heal
        #diseaseres 100
        #end

        #selectmonster 2202 -- Great Siddha
        #clearmagic
        #clearweapons
        #gcost 180
        #hp 29
        #att 14
        #def 14
        #invulnerable 20
        #spreaddom 2
        #magicskill 1 1
        #magicskill 4 1
        #pathcost 40
        #weapon 1840 -- Perfect Fist
        #weapon 1840 -- Perfect Fist
        #weapon 397 -- Kick
        #onebattlespell 604 -- Personal Luck
        #heal
        #diseaseres 100
        #end

        #selectmonster 2315 -- Melqart
        #clearmagic
        #gcost 150
        #awe 1
        #reinvigoration 2
        #spreaddom 2
        #magicskill 7 1
        #magicskill 3 1
        #heal
        #diseaseres 100
        #end

        #selectmonster 2316 -- Dragon King
        #spr1 "./ExtraPretenders/DragonKing.tga"
        #spr2 "./ExtraPretenders/DragonKing2.tga"
        #descr "The Dragon King is a primordial dragon put in charge of the Eastern Sea by the previous Pantokrator. For millennia he ruled the ocean and brought storms and tidal waves to the coasts of men. When he allowed the Pillar of the East to be stolen by an annoying little monkey, he was punished by eternal imprisonment. Now with the Pantokrator gone, his prison is crumbling, and the world will once more bow before the Dragon King of the East. In true shape the Dragon King is a magnificent serpentine monster with tigers claws and shimmering scales of a carp. It is able to run in the air and swim in the water. It is a master of water magic, but have difficulties mastering other forms of magic. It can take the shape of a dragon headed celestial dignitary more suited for spellcasting. It loses some of its powers in dragon shape. Dragons are resilient to injury and will suffer permanent injuries less often than most creatures."
        #gcost 200
        #awe 2
        #hp 120
        #prot 20
        #woundfend 2
        #heal
        #diseaseres 100
        #end

        #selectmonster 2317 -- Celestial Bureaucrat
        #gcost 200
        #heal
        #diseaseres 100
        #end

        #selectmonster 2318 -- Celestial Dragon
        #descr "The Celestial Dragon is a primordial dragon put in charge of the heavens by the previous Pantokrator. For millennia he delivered celestial decrees and brought order and celestial punishment to mankind. Now, with the Pantokrator gone, he is free to rule the world in its entirety. In true shape the Dragon King is a serpentine monster with tigers claws and shimmering scales of a carp. It is able to run in the air and swim in the water. It is a master of air and astral magic, but have difficulties mastering other forms of magic. It can take the shape of a celestial dignitary more suited for spellcasting. It loses some of its powers in dragon shape. Dragons are resilient to injury and will suffer permanent injuries less often than most creatures."
        #gcost 200
        #awe 1
        #hp 110
        #prot 20
        #woundfend 2
        #startdom 3
        #pathcost 40
        #heal
        #diseaseres 100
        #end

        #selectmonster 2319 -- Celestial Bureaucrat
        #heal
        #diseaseres 100
        #end

        #selectmonster 2440 -- Sea Dragon
        #gcost 150
        #heal
        #diseaseres 100
        #twiceborn 1235 -- Leviathan
        #end

        #selectmonster 2441 -- Sea Dragon Great Seer
        #gcost 150
        #heal
        #diseaseres 100
        #end

        #selectmonster 2457 -- Bodhisattva of Mercy
        #gcost 190
        #spreaddom 2
        #invulnerable 20
        #heal
        #diseaseres 100
        #end

        #selectmonster 2466 -- Kamadhenu
        #clearmagic
        #gcost 170
        #invulnerable 20
        #heal
        #magicskill 4 1
        #magicskill 6 1
        #diseaseres 100
        #end

        #selectmonster 2553 -- Ahura of the Oath
        #clearmagic
        #gcost 180
        #invulnerable 20
        #spreaddom 2
        #magicskill 0 1
        #magicskill 4 1
        #heal
        #onebattlespell 604 -- Personal Luck
        #diseaseres 100
        #end

        #selectmonster 2554 --  Ahura of the Waters
        #clearmagic
        #clearweapons
        #gcost 180
        #invulnerable 20
        #spreaddom 2
        #weapon 1848 -- Staff of Water
        #magicskill 2 1
        #magicskill 4 1
        #heal
        #onebattlespell 604 -- Personal Luck
        #diseaseres 100
        #end

        #selectmonster 2555 -- Spenta Mainyu
        #gcost 180
        #clearmagic
        #invulnerable 20
        #spreaddom 2
        #magicskill 1 1
        #magicskill 4 1
        #heal
        #onebattlespell 604 -- Personal Luck
        #diseaseres 100
        #end

        #selectmonster 2578 -- Yazad King
        #clearmagic
        #clearweapons
        #gcost 180
        #invulnerable 10
        #spreaddom 2
        #weapon 1847 -- Rod of the Yazad
        #batstartsum2 1607 -- Yazad
        #magicskill 1 1
        #magicskill 4 1
        #heal
        #onebattlespell 604 -- Personal Luck
        #diseaseres 100
        #end

        #selectmonster 2608 -- Azi 
        #clearmagic
        #gcost 240
        #magicskill 5 2
        #magicboost 0 0
        #magicboost 5 2
        #magicboost 53 -2
        #heal
        #prot 20
        #diseaseres 100
        #end

        #selectmonster 2609 -- Azi Great Warlock 
        #clearmagic
        #gcost 240
        #magicskill 5 2
        #heal
        #diseaseres 100
        #end

        #selectmonster 2611 -- Gannag Menog
        #clearmagic
        #gcost 180
        #invulnerable 20
        #spreaddom 2
        #magicskill 0 1
        #magicskill 5 1
        #heal
        #onebattlespell 604 -- Personal Luck
        #diseaseres 100
        #end

        #selectmonster 2627 -- Daeva of Wrath
        #gcost 180
        #clearmagic
        #invulnerable 20
        #awe 1
        #spreaddom 2
        #magicskill 0 1
        #magicskill 7 1
        #pathcost 40
        #heal
        #onebattlespell 604 -- Personal Luck
        #diseaseres 100
        #end

        #selectmonster 2693 -- Heavenly Condor
        #gcost 160
        #clearmagic
        #clearweapons
        #prot 20
        #fear 5
        #weapon 408 -- Talons
        #weapon 677 -- Wing Buff
        #weapon 404 -- Beak
        #magicskill 1 2 
        #heal
        #diseaseres 100
        #end

        #selectmonster 2695 -- Sapa Inca
        #gcost 180
        #clearmagic
        #hp 27
        #invulnerable 15
        #att 13
        #def 13
        #spreaddom 2
        #magicskill 0 1
        #magicskill 1 1
        #heal
        #onebattlespell 604 -- Personal Luck
        #diseaseres 100
        #end

        #selectmonster 2699 -- Ayar
        #gcost 160
        #clearmagic
        #cleararmor
        #awe 3
        #spreaddom 2
        #onebattlespell 604 -- Personal Luck
        #armor 50 -- Weightless Scale
        #armor 148 -- Crown
        #magicskill 0 1
        #magicskill 3 1
        #heal
        #diseaseres 100
        #end

        #selectmonster 2737 -- Immortal Coya
        #gcost 180
        #heal
        #diseaseres 100
        #end

        #selectmonster 2756 -- Bolon-ti-ku
        #clearmagic
        #clearweapons
        #gcost 180
        #invulnerable 25
        #hp 33
        #spreaddom 2
        #weapon 43 -- Poisoned claw
        #weapon 43 -- Poisoned claw
        #magicskill 5 1
        #magicskill 7 1
        #heal
        #diseaseres 100
        #end

        #selectmonster 2764 -- Hun Balam
        #clearmagic
        #gcost 160
        #heal
        #prot 20
        #diseaseres 100
        #magicskill 0 1
        #magicskill 7 1
        #end

        #selectmonster 2777 -- Demon Macaw
        #clearmagic
        #clearweapons
        #gcost 160
        #mr 20
        #prot 18
        #hp 88
        #weapon 408 -- talons
        #weapon 404 -- beak
        #weapon 196 -- Killing light
        #magicskill 4 2
        #heal
        #diseaseres 100
        #end

        #selectmonster 2783 -- Drakon
        #clearmagic
        #gcost 220
        #prot 20
        #heal
        #def 12
        #magicskill 6 2
        #diseaseres 100
        #end

        #selectmonster 2784 -- Thrice Horned Boar
        #clearmagic
        #gcost 150
        #prot 18
        #hp 132
        #magicskill 6 2
        #diseaseres 100
        #woundfend 2
        #end

        #selectmonster 2785 -- Solar Eagle
        #clearmagic
        #clearweapons
        #gcost 160
        #prot 18
        #heat 8
        #weapon 408 -- Talons
        #weapon 350 -- Fire Flare
        #weapon 404 -- Beak
        #magicskill 0 1
        #magicskill 1 1
        #diseaseres 100
        #woundfend 2
        #end

        #selectmonster 2786 -- Celestial Gryphon
        #clearmagic
        #gcost 160
        #prot 20
        #stormimmune
        #batstartsum1d6 368 -- Sacred Gryphon
        #makemonsters2 368 -- Sacred Gryphon
        #magicskill 1 2
        #heal
        #diseaseres 100
        #end

        #selectmonster 2787 -- Celestial Lion
        #clearmagic
        #gcost 160
        #prot 18
        #awe 2
        #magicskill 0 1
        #magicskill 1 1
        #heal
        #diseaseres 100
        #end

        #selectmonster 2788 -- Man Eater
        #clearmagic
        #gcost 150
        #prot 20
        #magicskill 7 2
        #heal
        #diseaseres 100
        #end

        #selectmonster 2789 -- Raven of the Underworld
        #clearmagic
        #clearweapons
        #descr "The Raven of the Underworld is a monstrous raven of an earlier era. When the previous Pantokrator rose to power he made the Raven a herald of death and slaughter and gave it powers of farsight and foresight. The raven was tasked with guiding the dead to the underworld and it became wise in the ways of death. But the Raven grew too close to death and the Pantokrator was afraid that it served the Lord of the Underworld rather than himself. The Pantokrator banished the great bird for eternity. Now, with the Pantokrator gone, the Herald of Death and Slaughter can return to the world of men and bring whispers of death to come. The Raven of the Underworld feeds on the dead and regains hit points if enough corpses are present. Those slain by the Raven may be forced to rise again to defend it in battle."
        #gcost 160
        #hp 88
        #prot 18
        #raiseonkill 50
        #weapon 408 -- talons
        #weapon 404 -- beak
        #weapon 677 -- Wing buff
        #magicskill 5 2
        #heal
        #diseaseres 100
        #end

        #selectmonster 2790 -- Myrmecoleon
        #clearmagic
        #gcost 160
        #hp 130
        #magicskill 3 2
        #heal
        #diseaseres 100
        #end

        #selectmonster 2791 -- Earth Serpent
        #clearmagic
        #gcost 180
        #magicskill 3 2
        #diseaseres 100
        #woundfend 2
        #end

        #selectmonster 2792 -- Solar Serpent
        #clearmagic
        #clearweapons
        #gcost 150
        #prot 18
        #heat 8
        #magicskill 0 2
        #weapon 65 --Venomous Fangs
        #weapon 676 -- Firey Breath
        #heal
        #diseaseres 100
        #end

        #selectmonster 2793 -- Serpent of Chaos
        #clearmagic
        #gcost 160
        #prot 20
        #batstartsum1d6 2125 -- Shadow
        #magicskill 5 2
        #heal
        #diseaseres 100
        #end

        #selectmonster 2795 -- Dog of the Underworld 
        #clearmagic
        #gcost 150
        #prot 18
        #magicskill 5 2
        #batstartsum2 566 -- Ghost
        #heal
        #end

        #selectmonster 2796 -- Hound of Hades
        #clearmagic
        #gcost 150
        #prot 20
        #hp 132
        #magicskill 5 2
        #heal
        #diseaseres 100
        #end

        #selectmonster 2797 -- Hieracosphinx
        #clearmagic
        #clearweapons
        #descr "Born at the dawn of time the Hieracosphinx represents an earlier era, when monsters and giants roamed the world. It has the body of a lion with the head and wings of a great falcon. It was given divine might and authority by a previous Pantokrator and served as a guardian spirit of the realms and kingdoms under the sun. Now with the Pantokrator gone it has realized the aspirations of the other Pretenders and will protect its subjects by donning the mantle of God itself. The Hieracosphinx is attuned to the powers of the sky and the sun. It is protected from harm by a Divine aura that turns away mortal weapons."
        #gcost 160
        #invulnerable 20
        #fireshield 8
        #heal
        #stormimmune
        #weapon 404 -- Beak
        #weapon 243 -- Lightning
        #weapon 29 -- Claw
        #weapon 29 -- Claw
        #diseaseres 100
        #magicskill 0 1
        #magicskill 1 1
        #end

        #selectmonster 2798 -- Criosphinx
        #clearmagic
        #descr "Born at the dawn of time the Criosphinx represents an earlier era, when monsters and giants roamed the world. It has the body of a winged lion with the head of a ram. It was given divine might and authority by a previous Pantokrator and served as a guardian spirit which brought prosperity and fertility to the realms and kingdoms of man. Now with the Pantokrator gone it has realized the aspirations of the other Pretenders and will protect its subjects by donning the mantle of God itself. The Criosphinx is attuned to the powers of the land and sky. It is surrounded by a Divine aura that protects if from harm by mundane weapons."
        #gcost 160
        #invulnerable 20
        #heal
        #trample
        #magicskill 1 1
        #magicskill 6 1
        #diseaseres 100
        #end

        #selectmonster 2799 -- Wadjet
        #clearmagic
        #clearweapons
        #descr "The Wadjet, Green One, is a Great Serpent born at the dawn of time, when monsters and giants roamed the world. It made the sacred river its home and became worshiped as protector of the land and a bringer of annual prosperity. During the rebellion of the lesser gods the great serpent remained neutral and tried to protect its subjects from the madness of warring gods. When the Great Antagonist was defeated the Pantokrator imprisoned every spirit and monster that hadn't fought against the Enemy. Now, with the Pantokrator gone, the shackles of the eternal prison is weakening and the Great Serpent is preparing to return to the sacred river and the beings that dwell there. Great Serpents can shed their skin and regenerate their wounds, and the Green One is no exception."
        #gcost 160
        #prot 20
        #weapon 65 -- Venomous Fangs
        #weapon 90 -- Crush
        #magicskill 2 2
        #decscale 3 -- Inc Growth
        #heal
        #diseaseres 100
        #end

        #selectmonster 2800 -- Ormr
        #clearmagic
        #clearweapons
        #gcost 160
        #prot 20
        #weapon 65 -- Venomous Fangs
        #weapon 90 -- Crush
        #magicskill 1 1
        #magicskill 2 1
        #heal
        #diseaseres 100
        #end

        #selectmonster 2801 -- Linnormr
        #clearmagic
        #gcost 180
        #regeneration 5
        #magicskill 5 2
        #prot 20
        #heal
        #diseaseres 100
        #end

        #selectmonster 2852 -- King of Frozen Bones
        #gcost 120
        #heal
        #diseaseres 100
        #end

        #selectmonster 2853 -- Ghost King
        #clearmagic
        #gcost 160
        #invulnerable 10
        #spreaddom 2
        #magicskill 5 2
        #heal
        #diseaseres 100
        #end

        #selectmonster 2855 -- Floating Mind
        #gcost 120
        #researchbonus 10
        #inspiringres 1
        #heal
        #diseaseres 100
        #end

        #selectmonster 2881 -- Telkhine God-King
        #descr "The Telkhine God-King was the ruler of the Telkhines, ancient spirits of the sea with powers to rival the titans. They were masterful sages and metal crafters. They forged artifacts for the servants of the previous Pantokrator and taught the elder cyclopes their arts. But the Telkhines succumbed to hubris and made themselves god-kings to be worshiped by their subjects. When they discovered the means to create malefic poison through the mixing of stygian water and sulfur their lands became poisonous to animals and plants alike. Their reign was put to an end by the Pantokrator and the entire kingdom was swallowed by the sea. The Telkhines themselves were imprisoned in Tartarus for eternity. Now with the Pantokrator gone the mightiest of the Telkhines is breaking free. Telkhines are able to change their shape. In their demonic form they appear with dog heads and flippers instead of hands and their powers over storms and the sea are increased. In human shape their skills in forging are increased. Telkhines are always surrounded by stygian fumes that kills men and beast alike. Even the population in the province where they dwell will slowly suffer and die. He is highly resilient and will suffer permanent injuries less often than most creatures."
        #gcost 230
        #mr 20
        #hp 67
        #att 14
        #def 14
        #heal
        #diseaseres 100
        #end

        #selectmonster 2882 -- Telkhine God-King 
        #descr "The Telkhine God-King was the ruler of the Telkhines, ancient spirits of the sea with powers to rival the titans. They were masterful sages and metal crafters. They forged artifacts for the servants of the previous Pantokrator and taught the elder cyclopes their arts. But the Telkhines succumbed to hubris and made themselves god-kings to be worshiped by their subjects. When they discovered the means to create malefic poison through the mixing of stygian water and sulfur their lands became poisonous to animals and plants alike. Their reign was put to an end by the Pantokrator and the entire kingdom was swallowed by the sea. The Telkhines themselves were imprisoned in Tartarus for eternity. Now with the Pantokrator gone the mightiest of the Telkhines is breaking free. Telkhines are able to change their shape. In their demonic form they appear with dog heads and flippers instead of hands and their powers over storms and the sea are increased. In human shape their skills in forging are increased. Telkhines are always surrounded by stygian fumes that kills men and beast alike. Even the population in the province where they dwell will slowly suffer and die. He is highly resilient and will suffer permanent injuries less often than most creatures."
        #gcost 230
        #mr 20
        #hp 78
        #att 14
        #def 14
        #heal
        #diseaseres 100
        #end

        #selectmonster 2930 -- Hooded Spirit
        #descr "The Hooded Spirits is an entity consisting of three spirits of fertility and growth. It was given the divine spark by a goddess of a sacred spring and served her faithfully until the previous Pantokrator rose to power. When the Pantokrator banished all gods the Hooded Spirits were spared and given the task of bringing fertility and prosperity to all mankind. But the spirits were seduced by men and sired offspring with divine sparks. The Pantokrator was furious and banished his servants for eternity. Now with the Pantokrator gone, the hooded spirits are once more able to enter the world and bring fertility to the land. The Hooded Spirits are able to unify at any time which will bring the spirits together no matter how far apart they are."
        #gcost 250
        #triplegodmag 0
        #heal
        #diseaseres 100
        #end

        #selectmonster 2958 -- Golden Lion
        #clearmagic
        #gcost 160
        #heal
        #prot 20
        #diseaseres 100
        #magicskill 0 1
        #magicskill 6 1
        #end

        #selectmonster 2959 -- Chiranjivi
        #gcost 150
        #heal
        #diseaseres 100
        #startdom 2
        #end

        #selectmonster 2960 -- Apkallu
        #gcost 200
        #heal
        #diseaseres 100
        #end

        #selectmonster 3121 -- Eldest Cyclops
        #gcost 190
        #heal
        #diseaseres 100
        #end

        #selectmonster 3396 -- King in Yellow
        #gcost 200
        #awe 1
        #ethereal
        #poisonres 15
        #teleport
        #heal
        #stealthy 0
        #spreaddom 2
        #diseaseres 100
        #insanify 20
        #mapmove 102
        #armor 231 -- Magic Robes
        #end

        #selectmonster 3416 -- Great Archon
        #gcost 230
        #heal
        #mr 20
        #tmpfiregems 1
        #tmpearthgems 1
        #tmpastralgems 1
        #diseaseres 100
        #userestricteditem 88 -- Alchemist
        #end

        #selectmonster 3394 -- Serpent of the Underworld
        #clearmagic
        #gcost 150
        #magicskill 5 2
        #prot 20
        #woundfend 2
        #gold 20
        #diseaseres 100
        #end

        #selectmonster 3395 -- Demiurge
        #clearmagic
        #clearweapons
        #gcost 150
        #magicskill 4 2
        #prot 20
        #heal
        #diseaseres 100
        #weapon 322 -- Bite
        #weapon 589 -- Tail Slap
        #end

        #selectmonster 3473 -- Firstborn of the Smokeless Flame
        #gcost 260
        #heal
        #mr 20
        #tmpfiregems 2
        #tmpairgems 1
        #diseaseres 100
        #end


        --- DOM 1 ---

        #selectmonster 244 -- Arch Mage
        #gcost 120
        #masterrit 1
        #heal
        #diseaseres 100
        #end

        #selectmonster 245 -- Master Enchanter
        #descr "The Master Enchanter is a mage of such great power that he has mastered his own mortality. Donning a godly mantle, he has taken the role of a Pretender God. He is a master of magic and can be adept in several of the magic paths. The Master Enchanter is a master of magic rituals and he can project his Sorcerous magic one province farther than normally possible. Each month whilst at an Arcane Laboratory he will enchant 1D3 Nature gems for use in ritual magic."
        #gcost 120
        #heal
        #diseaseres 100
        #end

        #newevent
        #rarity 5
        #req_lab 1
        #req_targmnr 245
        #req_pop0ok
        #nation -2
        #msg "##targname## has distilled magical gems at the Arcane Laboratory."
        #nolog
        #1d3vis 6
        #end

        #selectmonster 246 -- Freak Lord
        #gcost 120
        #gemprod 7 2
        #domsummon2 -12
        #heal
        #diseaseres 100
        #end

        #selectmonster 248 -- Arch Mage
        #gcost 120
        #masterrit 1
        #heal
        #diseaseres 100
        #end

        #selectmonster 249 -- Crone
        #gcost 80
        #heal
        #diseaseres 100
        #end

        #selectmonster 250 -- Frost Father
        #gcost 120
        #heal
        #diseaseres 100
        #end

        #selectmonster 251 -- Great Sage
        #gcost 120
        #heal
        #diseaseres 100
        #end

        #selectmonster 270 -- Arch Druid
        #gcost 120
        #domsummon 361 -- Vine Man
        #heal
        #diseaseres 100
        #end

        #selectmonster 485 -- Great Enchantress
        #gcost 120
        #heal
        #gemprod 4 2
        #diseaseres 100
        #end

        #selectmonster 486 -- Great Warlock
        #gcost 100
        #heal
        #diseaseres 100
        #end

        #selectmonster 500 -- Skratti
        #descr "The Skratti is an ancient giant of great power who has taken the role of a Pretender God. The Skratti is a master of magic rituals and very tough, but not as strong as dragons or demigods."
        #gcost 130
        #masterrit 1
        #heal
        #diseaseres 100
        #end

        #selectmonster 509 -- Arch Seraph
        #descr "The Arch Seraph is a Caelian mage of such great power that he has taken the role of a Pretender God. Once Caelum was ruled by semi-divine beings known as Yazatas. When they disappeared from the world the Arch Seraph uncovered their magic secrets and became a god in the eyes of the Caelians. Each month he can perform certain ceremonies whilst at a temple to summon a Yazad to this realm. The Seraph is able to project his Fravashi, guardian spirit, in times of need."
        #gcost 120
        #templetrainer 1607 -- Yazad
        #heal
        #diseaseres 100
        #end

        #selectmonster 550 -- Master Alchemist
        #gcost 110
        #heal
        #diseaseres 100
        #resources 50
        #end

        #selectmonster 653 -- Serpent King
        #descr "The Serpent King is a mighty serpent who is able to take human form. The Serpent King is magically strong enough to claim godhood. In order to lead human soldiers, the Serpent King usually appears in the form of a human sorcerer, but when angered, he transforms into his serpent shape. He is the lord of all serpent-kin and lamias will flock to his dominion, arriving in great numbers when he calls them to service. In addition he can call serpents to his aid. Even though his serpent form is more powerful than his human form, it is still quite weak compared to the majority of the other Pretender Gods."
        #gcost 120
        #domsummon2 403 -- Horned Serpent
        #heal
        #diseaseres 100
        #end

        #selectmonster 654 -- Serpent King 
        #descr "The Serpent King is a mighty serpent who is able to take human form. The Serpent King is magically strong enough to claim godhood. In order to lead human soldiers, the Serpent King usually appears in the form of a human sorcerer, but when angered, he transforms into his serpent shape. He is the lord of all serpent-kin and lamias will flock to his dominion, arriving in great numbers when he calls them to service. In addition he can call serpents to his aid. Even though his serpent form is more powerful than his human form, it is still quite weak compared to the majority of the other Pretender Gods."
        #gcost 120
        #domsummon2 403 -- Horned Serpent
        #heal
        #diseaseres 100
        #end

        #selectmonster 857 -- The Smoking Mirror
        #gcost 130
        #heal
        #diseaseres 100
        #end

        #selectmonster 858 -- Jaguar
        #gcost 130
        #heal
        #diseaseres 100
        #end

        #selectmonster 862 -- Vampire Queen
        #gcost 230
        #hp 25
        #seduce 10
        #darkpower 3
        #batstartsum2 405 -- Vampire
        #heal
        #diseaseres 100
        #end

        #selectmonster 873 -- Great Seer of the Deeps
        #startitem 316 -- Stone Sphere
        #gcost 110
        #heal
        #diseaseres 100
        #end

        #selectmonster 1896 -- Lawgiver
        #gcost 140
        #heal
        #diseaseres 100
        #end

        #selectmonster 1897 -- Feathered Serpent
        #gcost 140
        #heal
        #diseaseres 100
        #end

        #selectmonster 1898 -- Fomorian Sorcerer
        #gcost 130
        #heal
        #diseaseres 100
        #end

        #selectmonster 1905 -- Great Sorceress
        #gcost 150
        #masterrit 1
        #heal
        #diseaseres 100
        #end

        #selectmonster 2205 -- Great Sauromancer
        #gcost 120
        #heal
        #diseaseres 100
        #end

        #selectmonster 2206 -- Eldest Dwarf
        #gcost 120
        #heal
        #diseaseres 100
        #end

        #selectmonster 2207 -- Great Sorcerer
        #gcost 120
        #domsummon 884 -- Great Spider
        #heal
        #diseaseres 100
        #end

        #selectmonster 2208 -- Hunter Spider
        #gcost 120
        #domsummon 884 -- Great Spider
        #heal
        #diseaseres 100
        #end

        #selectmonster 2426 -- Bouda Father
        #gcost 150
        #heal
        #diseaseres 100
        #end

        #selectmonster 2427 -- Werehyena
        #gcost 150
        #heal
        #diseaseres 100
        #end

        #selectmonster 2549 -- Maharishi
        #gcost 120
        #researchbonus 10
        #inspiringres 1
        #heal
        #diseaseres 100
        #end

        #selectmonster 2550 -- Raksharani
        #gcost 150
        #seduce 10
        #heal
        #diseaseres 100
        #end

        #selectmonster 2551 -- Manushya Raksharani
        #gcost 150
        #heal
        #diseaseres 100
        #end

        #selectmonster 2802 -- Svartalf Mastersmith
        #gcost 200
        #heal
        #diseaseres 100
        #end

        #selectmonster 2803 -- Svartalfr Linnorm
        #gcost 200
        #regeneration 5
        #heal
        #diseaseres 100
        #end

        #selectmonster 2922 -- Morghen High Queen
        #gcost 130
        #heal
        #diseaseres 100
        #end

        #selectmonster 2955 -- Leader of the Closed Council
        #descr "The Leader of the Closed Council is an ancient Oracle of the Deep, hailed as the greatest mage of the ages. Donning a godly mantle, the Oracle has taken the role of a Pretender God to lead Agartha to a promised future. The Oracle is able to foresee things that have not yet come to pass and can prevent disasters. Any province that he visits will generate ten percent more tax revenue at month end due to his guidance."
        #gcost 120
        #heal
        #diseaseres 100
        #end

        #newevent
        #rarity 5
        #req_targmnr 2955
        #req_pop0ok
        #nation -2
        #msg "##targname## has increased the taxation of the province."
        #notext
        #nolog
        #taxboost 10
        #end

        #selectmonster 2961 -- Centauride Enchantress
        #descr "The Centauride Enchantress is a mage of such great power that she has taken the role of a Pretender God. She is a master of magic and can be adept in several of the magic paths. Each month whilst at an Arcane Laboratory she will enchant 1D3 Nature gems for use in ritual magic."
        #gcost 120
        #heal
        #diseaseres 100
        #woundfend 2
        #end

        #newevent
        #rarity 5
        #req_lab 1
        #req_targmnr 2961
        #req_pop0ok
        #nation -2
        #msg "##targname## has distilled magical gems at the Arcane Laboratory."
        #nolog
        #1d3vis 6
        #end

        #selectmonster 2977 -- Centaur Great Sage
        #gcost 120
        #heal
        #inspiringres 1
        #researchbonus 15
        #diseaseres 100
        #woundfend 2
        #end

        #selectmonster 2978 -- Master Sorcerer
        #gcost 110
        #onisummon 75
        #heal
        #diseaseres 100
        #end

        #selectmonster 2979 -- Onmyo Hakase
        #gcost 120
        #nobadevents 50
        #masterrit 1
        #heal
        #diseaseres 100
        #end

        #selectmonster 3053 -- Grand Heirophant
        #gcost 110
        #nobadevents 50
        #heal
        #diseaseres 100
        #end

        #selectmonster 3054 -- Firstborn of the Star
        #gcost 120
        #heal
        #diseaseres 100
        #end

        #selectmonster 3055 -- Grand Hydromancer
        #gcost 110
        #heal
        #diseaseres 100
        #end

        #selectmonster 3056 -- Aphroi Sage
        #gcost 110
        #inspiringres 1
        #researchbonus 10
        #heal
        #diseaseres 100
        #woundfend 2
        #end

        #selectmonster 3057 -- Aphroi Sage
        #gcost 110
        #inspiringres 1
        #researchbonus 10
        #heal
        #diseaseres 100
        #woundfend 2
        #end

        #selectmonster 3058 -- God-King of the Deep
        #gcost 120
        #heal
        #diseaseres 100
        #end

        #selectmonster 3059 -- Magister Supreme
        #gcost 120
        #heal
        #diseaseres 100
        #end

        #selectmonster 3060 -- Master
        #gcost 130
        #masterrit 1
        #researchbonus 10
        #heal
        #diseaseres 100
        #end

        #selectmonster 3098 -- First Spawn
        #gcost 150
        #heal
        #itemslots 290944 -- crown, 3 misc --3w
        #diseaseres 100
        #userestricteditem 99
        #montag 5190
        #end

        #selectmonster 3190 -- Bone Mother
        #gcost 120
        #gemprod 5 1
        #heal
        #diseaseres 100
        #end

        #selectmonster 3191 -- Enkidu Great Sage
        #gcost 120
        #researchbonus 20
        #heal
        #diseaseres 100
        #end

        #selectmonster 3192 -- Great Camazotz
        #gcost 130
        #heal
        #diseaseres 100
        #end

        #selectmonster 3204 -- Grey One
        #gcost 150
        #woundfend 2
        #diseaseres 100
        #end

        #selectmonster 3328 -- God Block
        #gcost 140
        #heal
        #diseaseres 100
        #end

        #selectmonster 3329 -- Betyl of The Sun
        #gcost 140
        #heal
        #diseaseres 100
        #end

        #selectmonster 3330 -- Betyl of Writing
        #gcost 140
        #heal
        #diseaseres 100
        #end

        #selectmonster 3331 -- Betyl of The Stars
        #gcost 140
        #heal
        #diseaseres 100
        #end

        #selectmonster 3344 -- Statue of Beginnings
        #gcost 170
        #heal
        #diseaseres 100
        #end

        #selectmonster 3345 -- Wooden Colossus
        #gcost 230
        #heal
        #mr 20
        #diseaseres 100
        #end

        #selectmonster 3346 -- Bronze Colossus
        #gcost 300
        #heal
        #mr 20
        #diseaseres 100
        #end

        #selectmonster 3368 -- Bronze Colossus
        #gcost 300
        #heal
        #mr 20
        #diseaseres 100
        #end

        #selectmonster 3369 -- Illahat of Fate
        #gcost 180
        #heal
        #mr 20
        #diseaseres 100
        #tmpastralgems 3
        #bringeroffortune 25
        #end

        #selectmonster 3370 -- Illahat of Might
        #gcost 200
        #heal
        #mr 20
        #tmpwatergems 1
        #tmpearthgems 1
        #tmpnaturegems 1
        #diseaseres 100
        #awe 1
        #descr "The Ilahat is a giant of divine heritage. A previous Pantokrator took her as his wife and gave her mandate to rule the world. When mankind began to worship her as well, the Pantokrator he was furious, imprisoned her for eternity and punished mankind with labor and strife. Now with the Pantokrator gone the Ilahat is free to once more rule the world, and soldiers flock to her banner to serve. In combat he will manifest gems of Water, Earth and Nature magic to aid in spellcasting."
        #domsummon 5905 -- Light Inf
        #domsummon2 5906 -- Heavy Inf
        #end

        #selectmonster 3384 -- Sahira
        #gcost 180
        #heal
        #diseaseres 100
        #end

        #selectmonster 3386 -- Ilahat of the Sky
        #clearweapons
        #descr "The Ilah is a giant of divine heritage. A previous Pantokrator gave him mandate over the desert skies. When jinnun, demons and primordial beasts rebelled against the Pantokrator and attacked mankind the Ilah became a warrior against the wicked. With time mankind began to worship the Ilah as a God. The Pantokrator was furious and imprisoned the Ilah for eternity. Now with the Pantokrator gone, the Ilah is free to claim the world as his and wage war upon the enemies of man. He is master of the desert winds, which will guide his arrows and dash aside missiles in flight. In combat he will manifest magical Air gems and an Astral Pearl to aid in spellcasting."
        #gcost 200
        #heal
        #diseaseres 100
        #tmpairgems 2
        #tmpastralgems 1
        #airshield 80
        #weapon 757 -- Bronze Scimitar
        #weapon 152 -- Trueshot Longbow
        #batstartsum2d6 517 -- Black Hawk
        #end

        #selectmonster 3387 -- Ilaha of the Sacred Mountain
        #clearweapons
        #gcost 200
        #heal
        #diseaseres 100
        #batstartsum1 5124 -- Great Green Serpent
        #weapon 1844 -- Earth Shake
        #weapon 92 -- Fist
        #end

        #selectmonster 3388 -- Ilahah of the Moon
        #descr "The Ilah is a giant of divine heritage. A previous Pantokrator placed the crescent moon on his brow and gave him mandate over all lands beneath the night skies and all life that begins during the long nights of the desert. When men realized his powers of fertility they began to worship him as a God. The Pantokrator was furious, imprisoned him for eternity and mankind was bereft of the fertility of the crescent moon. Now with the Pantokrator gone, the Ilah is free to once more bring the gifts of the moon to the world. Each month scorpions will crawl from the desert to serve him as his Dominion grows strong. In battle he will shroud the sun so only moonlight remains, and he will create magical pearls and a nature gem to ease spellcasting."
        #gcost 200
        #heal
        #diseaseres 100
        #darkvision 100
        #tmpastralgems 2
        #tmpnaturegems 1
        #onebattlespell 648 -- Solar Eclipse
        #domsummon 2233 -- Giant Scorpion
        #domsummon20 4511 -- Dust Scorpion
        #end

        #newmonster 6570
        #copystats 3376 -- Marid
        #clearmagic
        #name "Marid Sultan"
        #nametype 171
        #spr1 "./bozmod/Juhera/MaridSultan.tga"
        #spr2 "./bozmod/Juhera/MaridSultan2.tga"
        #descr "The Marid Sultan is an ancient being from the dawn of time. He is among the first of the children of Iblis and his sovereignty over the Jinn was uncontested until the rise of the Ifrit and the War of the Unseen. After losing his throne he was banished from the City of Brass along with his brethren and closest servants. Fleeing, they found refuge beneath the waves and have lain hidden there for millenia. Now, with the Pantokrator gone the Marid Sultan can once more ascend the throne, however this time it will be the Throne of Heaven and all shall bow before his might."
        #djinn -- bodytype for hit locations
        #mor 30
        #heal
        #diseaseres 100
        #expertmagicleader
        #goodleader
        #amphibian
        #magicskill 0 1
        #magicskill 1 1
        #magicskill 2 1
        #gcost 250
        #startdom 3
        #pathcost 60
        #end

        #newmonster 6571
        #name "Ghul Lich"
        #nametype 171
        #spr1 "./bozmod/Juhera/ghul_lich1.tga"
        #spr2 "./bozmod/Juhera/ghul_lich2.tga"
        #descr "A Ghul Lich is the dried husk of a Ghul Sorcerer adept in Death magic. Through dark rituals, the Ghul succeeded in mastering one of the Jinn's oldest and most urgent goals: to defeat death. By removing its viscera and hiding it outside its body, the Lich is virtually impossible to slay. Should the body be physically destroyed, a new one is formed from the dust of dead Ghuls. Being immortal, there is nothing left to acquire but godhood. The Ghul Lich is magically stronger than other Ghuls and its magically empowered body is very hard to destroy."
        #gcost 200
        #undead
        #immortal
        #pooramphibian
        #pierceres
        #inanimate
        #neednoteat
        #spiritsight
        #hp 35
        #magicskill 5 3
        #pathcost 30
        #startdom 2
        #fireres -10
        #coldres 15
        #poisonres 25
        #goodleader
        #heal
        #diseaseres 100
        #maxage 800
        #startage 300
        #prot 16
        #mr 18
        #mor 30
        #str 16
        #att 14
        #def 8
        #prec 12
        #enc 0
        #ap 12
        #mapmove 3
        #weapon "quarterstaff"
        #weapon "bite"
        #armor "crown"
        #armor "animal hides"
        #end

        #newmonster 6572
        #name "Zatanai Master"
        #spr1 "Domdaniel/ZMaster.tga"
        #spr2 "Domdaniel/ZMaster2.tga"
        #descr "The greatest of the Zatanai, the Master was one of the first magicians to venture to the caverns of Dom-Dan'yel. For centuries he has amassed magical power and knowledge, until there was only one thing left to attain. Now, with the Pantrokator gone the time has come to seek the ultimate prize. The Zatanai Master is skilled in many paths of magic."
        #nametype 172
        #gcost 130
        #hp 11
        #size 2
        #mr 18
        #mor 18
        #str 10
        #att 10
        #def 10
        #prec 10
        #enc 2
        #ap 12
        #darkvision 100
        #goodleader
        #goodundeadleader
        #spiritsight
        #diseaseres 100
        #heal
        #startage 500
        #maxage 1000
        #taskmaster 2
        #magicskill 0 1
        #magicskill 5 1
        #stealthy 0
        #douse 5
        #stealthy 0
        #amphibian
        #giftofwater 120
        #weapon 7 -- Quarterstaff
        #armor 158 -- Robes
        #armor 249 -- Cloth Headpiece
        #pathcost 10
        #startdom 1
        #montag 5196 -- Zatanai
        #twiceborn 6567 -- Spectral Zatani
        #end
-- Pretender
    -- MegaEfreet
        #newweapon
        #copyweapon 533
        #name "Fiery wrath"
        #ammo 13
        #end

        -- Events
            
            #newevent 
            #rarity 5
            #req_dominion 10
            #req_monster 7234
            #req_targmnr 7094
            #req_targgod 1    
            #nation -2        
            #msg "With a great eruption, the volcano which sealed ##godname## is now broken!"
            #transform 7095
            #killcom 7234 --cleanup
            #kill 20
            #incunrest 10
            #addsite 629 --Huge Crater
            #end

            #newmonster 7234        
            #copyspr 1369
            #copystats 1369
            #stealthy 999
            #name "Dummy" 
            #descr "No need."
            #mr 50
            #hp 700
            #landdamage 40
            #invisible
            #end

            #newevent 
            #rarity 5
            #req_dominion 10
            #req_targpath3 0
            #req_nomonster 7234
            #req_targitem "Staff of Storms"
            #req_targgod 0    
            #msg "##targname## has begun the ritual to release the First Efreet from his bond, in one month the ritual will be completed."        
            #stealthcom 7234 -- dummy stealth
            #end

        #newmonster 7094
        #name "First Efreet"
        #spr1 "./bozmod/Krieg/MegaEfreet1.tga"
        #spr2 "./bozmod/Krieg/MegaEfreet2.tga"
        #hp 180
        #size 6
        #prot 12
        #mr 22
        #mor 30
        #str 23
        #att 12
        #def 0
        #enc 3
        #prec 13
        #ap 0
        #mapmove 0
        #gcost 260
        #pathcost 40
        #startdom 4
        #heat 15
        #firepower 1
        #fireres 30
        #immobile
        #unteleportable
        #spiritsight
        #magicskill 0 2
        #magicskill 1 2
        #descr "Before the age of men dawned, the world belonged to the jinn, their emissaries and researchers having mapped the known world and beyond. Among them were those who were sent to explore the sulphurous depth of inferno, bringing light and prosperity to these hot and desolate lands. Yet as millennia passed, the Jinn emissary proclaimed himself free from the mission, for he would not bow to those beneath his power, the power of the first Efreet. Many followed suit, embracing the black and orange sulphur instead of smokeless flame of the afarit. For his arrogance he and his followers were banished from the throne plane and he himself was chained inside the volcano in the middle of the lands he proclaimed himself the sole ruler of. With pantokrator fettering the ifrit cousins there is no more force that can stop the first efreet, and freedom is finally within grasp. 
        The First Efreet is chained to a volcano, however he can be freed if there is another powerful fire mage (F3 or higher) with a staff of storms in the province and faith is high enough (10 candles). He also automatically casts Heat from Hell at the beginning of the battle."
        #superiorleader
        #superiorundeadleader
        #superiormagicleader
        #weapon "Fiery wrath"
        #weapon 541 -- aoe fire
        #weapon 320
        #weapon 320
        #spreaddom 2
        #itemslots 5254 -- 2 arms no feet 1 misc
        #bonusspells 1
        #onebattlespell "Heat from Hell"
        #saltvul 1
        #invulnerable 25
        #drawsize -60
        #homerealm 8
        #homerealm 5
        #woundfend 50
        #heal
        #end 

        #newmonster 7095
        #name "First Efreet"
        #spr1 "./bozmod/Krieg/MegaFREE1.tga"
        #spr2 "./bozmod/Krieg/MegaFREE2.tga"
        #hp 210
        #size 6
        #prot 6
        #mr 22
        #mor 30
        #str 24
        #att 14
        #def 10
        #enc 2
        #prec 13
        #ap 16
        #mapmove 12
        #gcost 420
        #pathcost 60
        #startdom 4
        #heat 12
        #firepower 1
        #fireres 35
        #shockres 15
        #coldres -5
        #ethereal
        #saltvul 3
        #unseen
        #spiritsight
        #magicskill 0 2
        #magicskill 1 2
        #fixedname "An Nur"
        #descr "Before the age of men dawned, the world belonged to the jinn, their emissaries and researchers having mapped the known world and beyond. Among them were those who were sent to explore the sulphurous depth of inferno, bringing light and prosperity to these hot and desolate lands. Yet as millennia passed, the Jinn emissary proclaimed himself free from the mission, for he would not bow to those beneath his power, the power of the first Efreet. Many followed suit, embracing the black and orange sulphur instead of smokeless flame of the afarit. For his arrogance he and his followers were banished from the throne plane and he himself was chained inside the volcano in the middle of the lands he proclaimed himself the sole ruler of. With pantokrator fettering the ifrit cousins there is no more force that can stop the first efreet, and freedom is finally within grasp. 
        By coercing five archdevils the chains have been broken and there is nothing in this world or any other that can stop the wrath that is the First Efreet!
        *cannot be wished for."
        #superiorleader
        #superiormagicleader
        #invulnerable 25
        #weapon "Fiery wrath"
        #weapon 541 -- aoe fire
        #weapon 320
        #weapon 320
        #weapon 320
        #weapon 320
        #spreaddom 3
        #itemslots 5278 -- 4 arms no feet 1 misc
        #bonusspells 1
        #onebattlespell "Heat from Hell"
        #saltvul 1
        #flying
        #stormimmunity
        #invulnerable 20
        #drawsize -60
        #nowish
        #woundfend 99
        #heal
        #end 
    -- Cosmic Egg
            #newmonster 7144 --Ghidorah egg
            #copystats 4993
            #name "Cosmic Egg"
            #descr "The Cosmic Egg is a mysterious object which has sat in place since time immemorial. Its origins remain unknown, but one thing has become clear - there is something truly spectacular growing inside it. 
            
            With the advent of the ascension wars, the egg has shown the ability to communicate through its shell, and followers flock to it, seeing it as the Pantokrator waiting to be reborn.
            
            *can't be wished for"
            #spr1 "./bozmod/Monsters/goldenegg.tga"
            #spr2 "./bozmod/Monsters/goldenegg.tga"
            #clearmagic
            #hp 85
            #prot 19
            #magicskill 1 3
            #magicskill 4 1
            #pathcost 40
            #startdom 4
            #gcost 200
            #shockres 10
            #voidsanity 20
            #okleader 
            #awe 1
            #mind
            #mr 30
            #blind
            #sunawe 3
            #homerealm 9
            #amphibian
            #end
    -- King of Beasts
        -- Weapons
            #newweapon
            #name "Deadly Afterglow"
            #magic
            #internal
            #inanimateimmune
            #undeadimmune
            #sizeresist
            #bowstr
            #end
            
            #newweapon
            #copyweapon 533 -- dragon fire, it seems zmey one is broken
            #name "Blazing beam"
            #dmg 0
            #range -3
            #ammo 5
            #natural
            #bonus
            #secondaryeffectalways 171 -- no more axe breath
            #uwok
            #end
            
            #newweapon
            #copyweapon 533 -- dragon fire, it seems zmey one is broken
            #name "God-king beam"
            #dmg 5
            #range -3
            #ammo 4
            #natural
            #aoe 5
            #bonus
            #secondaryeffectalways 171 
            #uwok
            #secondaryeffect "Deadly Afterglow"
            #end
            
            #newweapon
            #name "Enormous Stomp"
            #aoe 1
            #dmg 1
            #len 1
            #att 0 
            #def 0
            #blunt 
            #sizeresist 
            #unrepel
            #norepel
            #secondaryeffectalways 699 -- mini stun zone
            #end
        -- Base
            #newmonster 7142 --zilla
            #hp 170
            #name "King of Beasts"
            #size 6
            #descr "The self-proclaimed King of the Beasts is an ancient draconid of unknown origin. His enormous size and desire for peace once led him on a crusade against the mightiest beasts of old. This made him a favourite of the Pantokrator, who imprisoned those enemies the Kind of Beasts was able to subdue but could not slay. The King fell into a slumber for an age once his great campaign was complete.
            With the Pantokrator gone, mighty foes raise their heads once again, and it seems there is no peace to be had unless the mantle of godhood is claimed by the King.
            A being of such stature, wielding powers of fire so great it could bring about the end of the world, though the King has yet to awaken.
            
            After turn 36 there is a 10% chance each turn that this pretender will transform into a much more powerful shape."
            #spr1 "./bozmod/Monsters/Warpman/Zilla1.tga"
            #spr2 "./bozmod/Monsters/Warpman/Zilla2.tga"
            #att 12
            #def 8
            #str 24
            #prec 8
            #prot 21
            #mr 21
            #mor 30 -- 1.514 fix
            #weapon 319 -- bite fix
            #weapon "Tail Sweep"
            #weapon "Enormous Stomp"
            #weapon "Blazing Beam"
            #userestricteditem 7004
            #magicskill 0 3
            #gcost 220
            #pathcost 60
            #startdom 2
            #enc 2
            #mapmove 20
            #ap 12
            #startage 1800
            #maxage 6000
            #heal
            #fireres 35
            #poisonres 5
            #slashres
            #okleader 
            #nowish
            #awe 2
            #animalawe 5
            #dragonlord 3
            #researchbonus -25
            #amphibian
            #darkvision 100 -- no longer afraid of UW dankness
            #fear 10
            #siegebonus 50
            #firerange 1
            #itemslots 274560 -- misc+misc+head+crownonly
            #incunrest 50
            #homerealm 4
            #fastcast 50  -- zilla now drops big spells faster than any other fire mage ы
            #woundfend 50
            #end
        -- Enraged King
            #newmonster 7143 --nuclear zilla
            #hp 370
            #name "Enraged King"
            #size 6
            #descr "Self-proclaimed King of the Beasts is an ancient draconid of unknown origin. His enormous size and desire for peace has led him on a crusade against the mightiest beasts of old, something that Pantokrator actually enjoyed. Having slain or subdued all the beasts, the King has gone into slumber.
            With Pantokrator gone mighty foes rise their heads again. It seems there is no peace to be had unless the mantle of godhood is claimed by the King.
            Mighty stature and enormous power of fire could bring about the end of the world, though the King has yet to awaken. In three years after the ascension wars begin, he will erupt into a greater form.
            The blazing heat that King emits has begun melting and shaking the surrounding world from his hatred."
            #spr1 "./bozmod/Monsters/Warpman/NuclearZilla1.tga"
            #spr2 "./bozmod/Monsters/Warpman/NuclearZilla2.tga"
            #att 15
            #def 8
            #str 28
            #prec 8
            #prot 21
            #mr 22
            #mor 30 -- 1.514 fix 
            #userestricteditem 7004
            #weapon 319 -- bite fix
            #weapon "Tail Sweep"
            #weapon "Enormous Stomp"
            #weapon "God-king Beam"
            #magicboost 0 3 -- fixed that
            #enc 2
            #mapmove 20
            #ap 12
            #startage 1800
            #maxage 6000
            #heal
            #fireres 45
            #poisonres 5
            #slashres
            #okleader 
            #researchbonus -25
            #amphibian            
            #darkvision 100 -- no longer afraid of UW dankness
            #heat 15
            #uwheat 5
            #fear 15
            #siegebonus 200
            #dragonlord 5
            #firerange 2
            #onebattlespell 925 -- heat from hell
            #reaper 5
            #fortkill 10 -- buffed fort melting power to 10% from 5% ы
            #localsun
            #decscale 2
            #incscale 3
            #itemslots 290944 -- misc+misc+misc+head+crownonly
            #incunrest 100 -- fixed that
            #nowish
            #fastcast 50  -- zilla now drops big spells faster than any other fire mage ы
            #woundfend 99
            #end
        -- Event
            #newevent
            #req_targmnr "King of Beasts"
            #req_monster "King of Beasts"
            #transform "Enraged King"
            #rarity 13            
            #msg "A eruption of blazing heat is felt around the world followed by a low rumble, the great roar of ##godname## is heard across the world as he transforms into a walking cosmic inferno"
            #worldunrest 5            
            #req_rare 10
            #killpop 500
            #req_turn 36
            #msg "The fiery heart of the King of the Beasts beats with rage. As if glowing from within, he now marches on, melting his foes in otherworldly fires of doom. Fire and death follow him as the world weeps from his presence"
            #end
    -- The Singularity --------------------------------------------------------------
        --base
            #newmonster 7000
            #name "The Singularity"
            #spr1 "./bozmod/Monsters/singularity.tga"
            #spr2 "./bozmod/Monsters/singularity2.tga"
            #descr "The Singularity is a creature from the void banished from existence by the previous Pantokrator. Where it came from, nobody knows - but what is known is that it consumes dead matter into itself and grows stronger. Its depthless appetite spurs it to obliterate whole provinces. Some offer prayers of appeasement to the abomination, and in time it has gained sentience, guided by their propitiations. It has become a different kind of threat to the world as its old hunger turns towards godly ambition. Its followers demand it consume all heathens first. With the Pantokrator gone, the Singularity is unleashed upon the world once again.
            *impossible to wish for"
        --stats
            #hp 160
            #size 6
            #prot 0
            #mr 18
            #mor 30
            #str 28
            #att 5
            #def 0
            #enc 0
            #prec 12
            #ap 14
            #mapmove 10
            #gcost 220
        --gear
            #weapon 90 -- Crush
            #weapon 90 -- Crush
            #weapon 90 -- Crush
            #itemslots 274560 --[262144 + 128 + 12288] - head, 2 misc, head only crown
        --specials
            #okmagicleader
            #okleader
            #superiorundeadleader
            #undcommand 100
            #heal
            #diseaseres 100
            #voidsanity 10
            #poorleader
            #corpseeater 15
            #popkill 100
            #deadhp 1
            #startdom 4
            #pathcost 80
            #trample
            #undead
            #neednoteat
            #diseaseres 100
            #incunrest 100
            #regeneration 10
            #pierceres
            #bluntres
            #fear 10
            #magicskill 5 2
            #magicskill 7 1
            #nowish -- no borgar
        #end
    -- Red Dragon ------------------------------------------------------------
        --base
            #selectmonster 216            
            #spr1 "./bozmod/Cangobia/reddrag.tga"
            #spr2 "./bozmod/Cangobia/reddrag2.tga"
            #name "Red Dragon"
            #gcost 200
            #clearmagic
            #clearspec
            #descr "The Dragon is an ancient reptile of tremendous physical and magical power. Born before the history of time, the dragons were perceived as threats to the world and imprisoned by the previous Pantokrator millenia ago. Dragons are enormous, scaly, winged beasts capable of breathing fire, poisonous gas or supernatural frost upon enemies. Dragons are closely attuned to the magic that brought them into life and thus focus mainly on the paths associated with their element. Dragons regularly shed scales which can be used towards the casting of spells, or sold for a pretty coin. They hold an apex authority status over other drakes and as such, more drakes will come when bidden by a Dragon."            
            #userestricteditem 7004
        --stats
            #hp 165
            #size 6
            #prot 20
            #mor 30
            #mr 20
            #str 29
            #att 17
            #def 11
            #prec 14
            #mapmove 28
            #ap 14
            #enc 2
        --gear
            #itemslots 274560 --[262144 + 128 + 12288] - head, 2 misc, head only crown
        --specials
            #fireres 25
            #diseaseres 100
            #pathcost 80
            #darkvision 50
            #fear 15
            #dragonlord 4
            #flying
            #heal
            #startdom 2
            #homerealm 10
            #berserk 3
            #heat 6
            #inspirational 2
            #firepower 1
            #gold 80
            #gemprod 0 1
            #magicskill 0 2
        
            #drawsize -30
            #end
    -- Blue Dragon ------------------------------------------------------------
        --base
            #selectmonster 265
            #gcost 200
            #spr1 "./bozmod/Cangobia/bluedrag.tga"
            #spr2 "./bozmod/Cangobia/bluedrag2.tga"
            #name "Blue Dragon"
            #clearmagic
            #clearspec
            #descr "The Dragon is an ancient reptile of tremendous physical and magical power. Born before the history of time, the dragons were perceived as threats to the world and imprisoned by the previous Pantokrator millenia ago. Dragons are enormous, scaly, winged beasts capable of breathing fire, poisonous gas or supernatural frost upon enemies. Dragons are closely attuned to the magic that brought them into life and thus focus mainly on the paths associated with their element. Dragons regularly shed scales which can be used towards the casting of spells, or sold for a pretty coin. They hold an apex authority status over other drakes and as such, more drakes will come when bidden by a Dragon."            
            #userestricteditem 7004
        --specials
            #coldres 25
            #diseaseres 100
            #pathcost 80
            #fear 15
            #dragonlord 3
            #darkvision 50
            #flying
            #heal
            #startdom 2
            #homerealm 10
            #amphibian
            #cold 8
            #snowmove
            #coldpower 1
            #gold 80
            #gemprod 2 1
            #magicskill 2 1
            #magicskill 1 1
        --stats
            #hp 145
            #size 6
            #prot 18
            #mor 30
            #mr 20
            #str 25
            #att 16
            #def 16
            #prec 13
            #mapmove 28
            #ap 13
            #enc 2
        --gear
            #itemslots 274560 --[262144 + 128 + 12288] - head, 2 misc, head only crown
        
            #drawsize -30
            #end
    -- Green Dragon ------------------------------------------------------------
        --base
            #selectmonster 266
            #gcost 200
            #spr1 "./bozmod/Cangobia/greendrag1.tga"
            #spr2 "./bozmod/Cangobia/greendrag2.tga"
            #name "Green Dragon"
            #clearmagic
            #clearspec
            #descr "The Dragon is an ancient reptile of tremendous physical and magical power. Born before the history of time, the dragons were perceived as threats to the world and imprisoned by the previous Pantokrator millenia ago. Dragons are enormous, scaly, winged beasts capable of breathing fire, poisonous gas or supernatural frost upon enemies. Dragons are closely attuned to the magic that brought them into life and thus focus mainly on the paths associated with their element. Dragons regularly shed scales which can be used towards the casting of spells, or sold for a pretty coin. They hold an apex authority status over other drakes and as such, more drakes will come when bidden by a Dragon."            
            #userestricteditem 7004
        --specials
            #poisonres 25
            #diseaseres 100
            #pathcost 80
            #fear 15
            #dragonlord 3
            #darkvision 50
            #flying
            #heal
            #startdom 2
            #homerealm 10
            #swampsurvival
            #swimming
            #gold 80
            #decscale 3
            #gemprod 6 1
            #poisoncloud 4
            #regeneration 5
            #magicskill 6 2
        --stats
            #hp 165
            #size 6
            #prot 18
            #mor 30
            #mr 21
            #str 24
            #att 15
            #def 12
            #prec 12
            #mapmove 25
            #ap 11
            #enc 2
        --gear
            #itemslots 274560 --[262144 + 128 + 12288] - head, 2 misc, head only crown
        
            #drawsize -30
            #end
    -- Fairy Dragon ------------------------------------------------------------
        --base
            #newmonster 7002
            #name "Fairy Dragon"
            #spr1 "./bozmod/Monsters/Fairy_Dragon_Monarch.tga"
            #spr2 "./bozmod/Monsters/Fairy_Dragon_Monarch_Attack.tga"
            #descr "The Fairy Dragon hails from deep and mysterious forests, but in ancient times he would rove far and wide with his friend the Pantokrator. Mirth and magic followed in his wake. Although referred to as a dragon, the Fairy Dragon is only so in name: his body resembles a large seahorse borne by giant butterfly wings. From a pointed snout flows a stream of song which mixes spells and lulls those nearby to sleep. Since his friend’s mysterious departure, the Fairy Dragon has risen to confront the many pretenders who would dishonour the authority of the absent Pantokrator. But to preserve the order set by his lord, the Fairy Dragon must himself ascend as Pantokrator; and only then will he be able to search for his missing friend."
        --specials
            #gcost 210
            #clearspec
            #clearmagic
            #userestricteditem 7004
            #diseaseres 100
            #spiritsight
            #pathcost 20
            #dragonlord 1
            #flying
            #heal
            #startdom 1
            #magicpower 1
            #magicbeing
            #illusion
            #bonusspells 1
            #stealthy 20
            #magicskill 6 1
            #magicskill 4 1
            #magicskill 1 1
            #sleepaura 10
            #spellsinger
            #pooramphibian
            #expertmagicleader
            #woundfend 1
        --gear
            #weapon 20 -- Bite
            #weapon 532 -- Tail sweep
            #itemslots 323712 --[262144 + 61440 + 128] - head only crown + 61440 4misc + 1 head
        --stats
            #hp 46
            #size 4
            #prot 6
            #mor 24
            #mr 23
            #str 14
            #att 11
            #def 16
            #prec 16
            #mapmove 28
            #ap 14
            #enc 2
        #end
    -- Black Dragon ------------------------------------------------------------
        #newmonster 7004
        #name "Black Dragon"
        #desc "The black dragon uses an acidic breath attack, which can blind and poison enemies"
        #spr1 "./bozmod/Monsters/blackdragon.tga"
        #spr2 "./bozmod/Monsters/blackdragon2.tga"
        #descr "The Black Dragon is an ancient reptile of tremendous physical and magical power. Born before the history of time, the dragons were perceived as threats to the world and imprisoned by the previous Pantokrator millenia ago. The Black Dragon is capable of breathing blinding and corrosive acid upon enemies. Dragons are closely attuned to the magic that brought them to life and thus focus on that magic. The Black Dragon is skilled in the lore of potions and substances, its acidic blood and saliva is as useful for alchemy as it is for melting castle gates. Due to his blindness, he cannot easily detect enemies at long distances and his precision is far lower than that of most Dragons."
        #gcost 230 -- slight cost nerf
        #diseaseres 100
        #pathcost 80
        #fear 10
        #dragonlord 1
        #userestricteditem 7004
        #heal
        #startdom 2
        #homerealm 10
        #flying

        #itemslots 274560 --[262144 + 128 + 12288] - head, 2 misc, head only crown

        #weapon "Acid Breath" -- Acid
        #weapon 542 -- Acid
        #weapon 29 -- Claw
        #weapon 29 -- Claw
        #weapon 532 -- Tail sweep

        #hp 120
        #size 6
        #prot 20
        #mor 30
        #mr 19
        #str 25
        #att 15
        #def 11
        #prec 4
        #enc 2
        #mapmove 28
        #ap 10

        #fireres  5
        #researchbonus 5
        #coldres  5
        #magicskill 0 1
        #magicskill 2 1
        #blind
        #acidshield 9
        #alchemy 70
        #siegebonus 50
        #woundfend 10
        #end

        #newweapon 
        #name "Acid Blind"
        #mrnegates
        #dt_aff
        #dmg 4096
        #secondaryeffectalways 515
        #end

        #newweapon
        #name "Acid Breath"
        #dmg 1
        #range -3
        #beam
        #range0
        #nratt 1
        #ammo 4
        #aoe 2
        #halfstr
        #acid
        #armorpiercing
        #flyspr 10179
        #bonus
        #magic
        #secondaryeffectalways "Acid Blind"
        #end
    -- Dracolich ------------------------------------------------------------
        #selectmonster 644
        #str 22
        #incscale 3
        #deathpower 1
        #dreanimator 4
        #gemprod 5 1
        #userestricteditem 7004
        #end
    -- Crystal Dragon ------------------------------------------------------------
        #newmonster 7005
        #name "Crystal Dragon"
        #spr1 "./bozmod/Monsters/crystaldragon.tga"
        #spr2 "./bozmod/Monsters/crystaldragon2.tga"
        #descr "The Crystal Dragon was sculpted by the previous Pantokrator as a symbol of great beauty and power. Unsatisfied with the immaculate statue, he gifted it with life and a pure, incorruptible mind. From then on, the Crystal Dragon was an obedient personal guardian of the Pantokrator, to be feared and adored by all. Now that the Pantokrator is gone, the Crystal Dragon has no other master. It retained its incorruptible mind and artificial life, making it immune to many spells, yet nobody knows how it continues to exist without the Pantokrator’s magic keeping it alive. Many have taken the continued existence of the Crystal Dragon as a sign that the Pantokrator is not truly gone. While His other works crumble, many have come to worship the Crystal Dragon as the sole remaining intercessor of the Pantokrator, or as some have begun to whisper, even his new incarnation. The presence of the Crystal Dragon fills the populace with confidence that the Pantokrator will return to restore order, reducing unrest. Crystal mages from far and wide take great interest in studying the dragon, and join its cause in the hope that their allegiance can buy the secret of its life. The Crystal Dragon was traditionally adorned with treasures offered to the old Pantokrator, and has developed an affinity for wearing many such magical trinkets."

        #gcost 220
        #diseaseres 100
        #pathcost 80
        #fear 10
        #dragonlord 1
        #userestricteditem 7004
        #heal
        #startdom 2

        #itemslots 290944 --[262144 + 28672 + 128] - head only crown + 3misc + 1 head

        #weapon 20 -- Bite
        #weapon 29 -- Claw
        #weapon 29 -- Claw
        #weapon 532 -- Tail sweep

        #hp 130
        #size 6
        #prot 20
        #mor 50
        #mr 25
        #str 25
        #att 8
        #def 4
        #prec 6
        #enc 0
        #mapmove 20
        #ap 6

        #awe 1
        #poisonres 15
        #shockres  5
        #fireres  5
        #inanimate
        #incunrest -50
        #mind
        #magicpower -1
        #gemprod 4 1
        #magicskill 3 1
        #magicskill 4 1
        #pierceres
        #slashres
        #noleader
        #blind
        #stonebeing
        #woundfend 99

        #end

        #newevent
        #rarity 5
        #req_rare 5
        #nation -2    
        #req_monster "Crystal Dragon"
        #com 340 --crystal mage
        #msg "The mystery of the Crystal Dragon's life has attracted a crystal mage to study it"
        #end
    -- Bane Dragon ------------------------------------------------------------
        #newmonster 8212
        #name "Banefire Dragon"
        #spr1 "./bozmod/Monsters/Warpman/BaneDino.tga"
        #spr2 "./bozmod/Monsters/Warpman/BaneDino2.tga"    
        #descr "The Banefire dragon is a primordial dragon as ancient as the world itself. Burning with sickly green flames that leech life from anything they touch, the dragon fought the Pantokrator and was brought low.
        Unable to completely slay the beast, Pantokrator petrified and hurled the bane dragon into the depth of Tartarus, so that nothing would be endangered by it any more.
        After the long fall, the petrified dragon hit the depths and the shell cracked. Over millennia the dragon has been cracking the prison and dragging his form back to the surface, seeking vengeance against the Pantokrator and the world he loved so much.
        With pantokrator gone there is nothing left to wish for but to usurp the throne out of spite."
        #size 6
        #hp 180
        #att 11
        #def 7
        #str 30
        #prec 8
        #prot 20
        #mr 18
        #mor 30
        #weapon 348 --banefire strike
        #weapon "Enormous Stomp"
        #weapon "Tail Sweep"
        #userestricteditem 7004
        #magicskill 0 1
        #magicskill 5 1
        #gcost 200
        #pathcost 80
        #startdom 2
        #enc 4
        #mapmove 18
        #ap 10
        #startage 5800
        #maxage 9000
        #fireres 30
        #poisonres 15
        #slashres
        #okleader 
        #nowish
        #dragonlord 1
        #researchbonus -15
        #fear 15
        #siegebonus 30
        #itemslots 12288
        #incunrest 10
        #popkill 8
        #woundfend 99
        #banefireshield 10
        #heat 7
        #heal
        #end

        #selectmonster 5422
        #rpcost 3
        #gcost 305 gold
        #addupkeep -200
        #end
        
        #selectmonster 5266
        #rpcost 25
        #gcost 25 gold
        #addupkeep -30
        #end

        #newsite 1414
        #name "Banefire Caldera"
        #path 0
        #level 4
        #rarity 5
        #decscale 3
        #decscale 2
        #homemon 5266 --banefire child
        #homecom 5422 --banefire spirit
        #end
        
        #newevent
        #rarity 0
        #req_pregame 1
        #req_owncapital 1
        #req_godismnr  8212
        #addsite 1414
        #notext
        #nolog
        #end
    -- Shadow Dragon ------------------------------------------------------------
        #newmonster 7006
        #name "Shadow Dragon"
        #spr1 "./bozmod/Monsters/shadow_dragon.tga"
        #spr2 "./bozmod/Monsters/shadow_dragon_attack.tga"

        #descr "The Shadow dragon is a being born of darkness that wishes to destroy all the light in the world and envelop everything in its fuligin shadow. In the age preceding the last  Pantokrator, there was a Dragon of Light and a Dragon of Shadow, whose rivalry balanced the primordial forces they represented. Unable to gain an advantage over its counterpart, the Shadow Dragon tricked the Pantokrator into utterly destroying the Dragon of Light. Once the Pantokrator realised his error, the Shadow Dragon was banished into the Void, but never lost sight of the material world. With the Pantokrator is gone, the Shadow Dragon has re-entered the world. With followers drawn from the mortal races, and without its arch nemesis to fight, it has nothing left to achieve but godhood."

        #gcost 230
        #clearspec
        #userestricteditem 7004
        #clearmagic
        #diseaseres 100
        #pathcost 80
        #fear 10
        #darkvision 50
        #dragonlord 1
        #heal
        #startdom 2
        #flying

        #clearweapons
        #weapon 284 --steal strength
        #weapon 284 --steal strength
        #weapon 20 -- Bite
        #weapon 532 -- Tail sweep
        #weapon 568 -- Drake frost

        #poisonres 10
        #coldres 10
        #undead
        #enc 0
        #ethereal
        #stealthy 10
        #invisible
        #cold 3
        #pooramphibian
        #fireres -6
        #undead
        #neednoteat
        #spiritsight
        #saltvul 4
        #darkpower 8
        #magicskill 5 1
        #magicskill 1 1
        #noriverpass


        #hp 105
        #size 6
        #prot 14
        #mor 30
        #mr 19
        #str 16
        #att 15
        #def 16
        #prec 12
        #mapmove 28
        #expertundeadleader
        #ap 10

        #itemslots 274560 --[262144 + 128 + 12288] - head, 2 misc, head only crown
        #end
    -- Soulless Dragon ------------------------------------------------------------
        #newmonster 7007
        #name "Soulless Dragon"
        #spr1 "./bozmod/Monsters/Zombie_dragon.tga"
        #spr2 "./bozmod/Monsters/Zombie_dragon_attack.tga"
        #gcost 240
        #clearspec
        #clearmagic
        #descr "The Soulless Dragon is an ancient reptile of tremendous physical and magic power. Born before the history of time, the dragons were perceived as threats to the world and imprisoned by the previous Pantokrator millenia ago. The Soulless Dragon has contracted a magical disease that mutated within it's body causing it to become soulless, who had cursed it with such a fate it is unknown. Now the Zombie dragon is able to grow larger from consuming bodies of the dead, already being a powerful dragon it presents a huge threat to peace in the world. Dragons are closed attuned to the magic that brought them into life and thus focus on that magic. The Soulless Dragon has become closely attuned to it's growing flock of followers who see it as a new god and it has set out to destroy the other pretenders."

        #diseaseres 100
        #pathcost 80
        #userestricteditem 7004
        #fear 10
        #dragonlord 1
        #heal
        #startdom 2

        #clearweapons
        #weapon 20 -- Bite
        #weapon 29 -- Claw
        #weapon 532 -- Tail sweep
        #weapon 254 -- Plague Breath

        #itemslots 274560 --[262144 + 128 + 12288] - head, 2 misc, head only crown

        #poisonres 25
        #coldres 15
        #inanimate
        #undead
        #enc 0
        #mr 14
        #spiritsight
        #trample
        #bluntres
        #pierceres
        #corpseeater 8
        #deadhp 1
        #raiseonkill 80
        #diseasecloud 8  
        #raiseonkill 80
        #researchbonus -10
        #magicskill 5 1

        #hp 160
        #size 6
        #prot 13
        #mor 50
        #mr 15
        #str 25
        #att 12
        #def 6
        #prec 8
        #mapmove 20
        #ap 10
        #woundfend 10

        #superiorundeadleader

        #end
    -- Mutant Dragon ------------------------------------------------------------
        #newmonster 7008
        #name "Mutant Dragon"
        #spr1 "./bozmod/Monsters/Mutant_Dragon.tga"
        #spr2 "./bozmod/Monsters/Mutant_Flex.tga"
        #descr "The mutant dragon is a creature of mysterious origin, some say it used to be a powerful wizard who delved further than anyone else into the art of cross breeding and genetic research, at some point learning how to merge his own body with that of a powerful dragon. Ultimately, whatever the stories say about the Mutant dragon, one thing is known for sure - it is a creature that is optimized for one thing and that is destruction. It retains, perhaps through this merged scientist - great knowledge of crossbreeding and from it's dragon counterpart, a foul temper and disobedience to it's other head. The Mutant dragon's body is in a constant state of struggle between the two heads that control it, each with their own personality and they aren't always on good terms. The madness of the Mutant dragon spreads to it's populace, while it's arrogance can prevent it from retreating from combat, even if the situations it should are extremely rare."
        #gcost 250
        #clearspec
        #clearmagic
        #diseaseres 100
        #userestricteditem 7004
        #pathcost 80
        #fear 10
        #heal
        #startdom 2

        #weapon 29 -- Claw
        #weapon 29 -- Claw
        #weapon 29 -- Claw
        #weapon 29 -- Claw
        #weapon 20 -- Bite
        #weapon 20 -- Bite

        #poisonres 3
        #fireres 3
        #coldres 3
        #shockres 3
        #crossbreeder 12
        #pathcost 80
        #magicskill 7 1
        #magicskill 6 1  
        #siegebonus 30
        #darkvision 50
        #ambidextrous 6
        #berserk 1
        #incunrest 50
        #shatteredsoul 12

        #hp 130
        #size 6
        #prot 12
        #mor 30
        #mr 16
        #str 21
        #att 16
        #def 14
        #enc 3
        #prec 10
        #mapmove 18
        #ap 10
        #woundfend 10

        #itemslots 274846 --[12288 + 384 + 262144 + 30]  2 mis  + 2 heads  + heads crowns + 4 hands
        #end
    -- Sanguine Dragon ------------------------------------------------------------
        #newitem
        #name "The flaying ring"
        #constlevel 12
        #copyspr 269
        #type 8
        #autospell "Aura of pain"
        #autospellrepeat 1 
        #mainpath 0
        #mainlevel 5
        #nofind
        #cursed
        #curse
        #end

        #newevent
        #rarity 5
        #req_rare 100
        #req_monster 7009
        #req_targmnr 7009 
        #req_targnoitem "The flaying ring" --the flaying ring
        #nolog
        #notext
        #addequip 9
        #msg "Sanguine Dragon equips [The flaying ring]"
        #end

        #newspell
        #copyspell "Agony"
        #name "Aura of pain"
        #range 1
        #school -1
        #end

        #newmonster 7009
        #name "Sanguine Dragon"
        #descr "The Sanguine dragon is an vengeaful creature that was cursed into suffering immense pain for all eternity by the Pantokrator for it's arrogance. Now with the Pantokrator gone, the Sanguine dragon has mastered this curse and turned it into a weapon. 
        Having endured this divine pain for eons, it has learned to channel and share this pain with others. Those unfortunate enough to find themselves near the Sanguine Dragon will bleed profusely and their psyche may be left permanently scarred, while those who strike the Sanguine Dragon will feel the edge of their blows against their own flesh. The Sanguine dragon's is permanently bleeding and it's blood colors the sky, blood red rain follows it wherever it goes.
        
        *impossible to wish for"

        #spr1 "./bozmod/Monsters/sanguine_dragon.tga"
        #spr2 "./bozmod/Monsters/sanguine_dragon_attack.tga"
        #gcost 220

        #diseaseres 100
        #pathcost 80
        #dragonlord 1
        #heal
        #flying
        #startdom 2

        #fear 15
        #magicskill 5 1
        #magicskill 7 1
        #darkvision 50
        #immortal
        #demon
        #curseattacker 5
        #userestricteditem 7004
        #douse 4
        #bloodvengeance 1
        #voidsanity 5
        #onebattlespell 1096 --blood rain
        #deathcurse

        #clearweapons
        #weapon 63 --life drain
        #weapon 29 -- Claw
        #weapon 532 -- Tail sweep

        #hp 150
        #size 6
        #prot 11
        #mor 30
        #mr 20
        #str 23
        #att 15
        #def 12
        #prec 12
        #mapmove 28
        #ap 10
        #enc 3

        #itemslots 274560 --[262144 + 128 + 12288] - head, 2 misc, head only crown
        #nowish

        #end
    -- Psionic  Dragon ------------------------------------------------------------
        #newmonster 7010
        #name "Psionic Dragon"
        #spr1 "./bozmod/Monsters/psionic_dragon_5.tga"
        #spr2 "./bozmod/Monsters/psionic_dragon_5_attack.tga"
        #descr "Illithid have always been anxious to try crossbreeding with different species on the newfound homeworld, yet one experiment went so well they now think it might have been a failure in the long run.
        Dragon mind has proven to be superior to that of Illithid and perhaps even that of the Mind lords. That coupled with the impossible psionic powers granted by the enhanced perception of Illithid and their innate ability to feel and transmit thoughts onto others means there is but one thing this creature needs to attain, godhood - consume all the minds of sentients and become one with the world.
        The body of the Psionic dragon is quite soft and isn't as tough as that of dragons and can be damaged quite easily"
        #gcost 220
        #diseaseres 100
        #pathcost 80
        #fear 10
        #floating
        #heal
        #startdom 2

        #itemslots 274560 --[262144 + 128 + 12288] - head, 2 misc, head only crown

        #clearweapons
        #weapon 63 -- Life drain
        #weapon 20 -- Bite
        #weapon 274 -- Enslave mind
        #weapon 86 -- Mindblast

        #userestricteditem 7004
        #hp 65
        #size 5
        #prot 8
        #mor 30
        #mr 22
        #str 17
        #att 13
        #def 12
        #prec 12
        #mapmove 12
        #ap 10
        #enc 2

        #shockres -7
        #float
        #commaster
        #magicbeing
        #magicpower 1
        #amphibian
        #voidsanity 15
        #spiritsight
        #bonusspells 2
        #magicskill 4 3
        #magicboost 53 1
        #woundfend 1
        #end
    -- Dracobolith -----------------------------------------------------------
        #newmonster 7011
        #name "Dracobolith"
        #spr1 "./bozmod/Monsters/Warpman/Dracobolith.tga"
        #spr2 "./bozmod/Monsters/Warpman/Dracobolith_attack.tga"
        #descr "Aboleth have been always fascinated by the life above waves, and have frequently taken control over minds of creatures above to observe the land they could not grasp.
        Among the creatures above the waves they found the Dragons, majestic creatures that had intellect that rivaled theirs yet had the bodies Aboleth themselves could only dream of ever having.
        Obsessed with the idea of obtaining the perfect body a mind lord of epic skill in grafting began experimenting on bodily augmentations to reach draconic perfection, so that he would be a merge of a perfect mind and a perfected body. 
        Having attained this would mean transcending to Godhood, for what else could one dream of."
        #gcost 210
        #diseaseres 100
        #pathcost 80
        #fear 10
        #dragonlord 1
        #heal
        #startdom 2
        #weapon 269 -- Soul Leech
        #weapon 609 -- grab and swallow
        #weapon 86 -- Mindblast
        #itemslots 274560 --[262144 + 128 + 12288] - head, 2 misc, head only crown
        #userestricteditem 7004
        #hp 230
        #size 6
        #prot 4
        #mor 30
        #mr 22
        #str 16
        #att 12
        #def 10
        #prec 12
        #mapmove 22
        #ap 10
        #enc 3
        #aquatic
        #bonusspells 1
        #darkvision 50
        #bluntres
        #voidsanity 10
        #magicskill 4 1
        #magicskill 2 1
        #magicboost 4 1
        #magicboost 2 1
        #woundfend 10
        #heal
        #end
    -- King of Knights ------------------------------------------------------------
        #newmonster 7012
        #name "King of knights"
        #descr "Once but a petty king, decades of arduous work have helped establish the order of knights that protect the lands from all kinds of monsters, demons, tyrants and walking dead alike.
        With lands finally secure and people cheering for your every deed there is nothing left to do but take your most trusted brothers in arms and proclaim the great crusade against the unclean anew.
        Armed with a lance that brings down tyrants and sword that banishes their lies and leaves no place to hide there is nothing more to fear and nothing more to strive for but a better world and your just rule."
        #spr1 "./bozmod/Monsters/Warpman/King_Knight.tga"
        #spr2 "./bozmod/Monsters/Warpman/King_Knight_attack.tga" 
        #pathcost 80
        #triplegod 1
        #triplegodmag 2
        #triple3mon
        #minprison 0
        #maxprison 1
        #gcost 80
        #hp 25
        #str 13
        #att 15
        #def 15
        #prec 15
        #prot 0
        #size 3
        #mr 15
        #mor 20
        #enc 2
        #mountedhumanoid
        #mounted
        #mapmove 25
        #ap 20
        #eyes 2
        #weapon "Tyrant's bane"
        #weapon "Judgement"
        #armor 14
        #armor 21
        #armor 57
        #weapon 56
        #maxage 1000
        #woundfend 20
        #diseaseres 100
        #chaospower -1
        #expertleader
        #magicskill 0 2
        #magicskill 2 2
        #magicskill 6 2
        #magicboost 53 -5
        #researchbonus -15
        #disbelieve 25
        #batstartsum1d3 22
        #homerealm 3 --mediterranean
        #end
        -- Valiant Knight ------------------------------------------------------------

            #newmonster 7013
            #name "Valiant knight"
            #descr "Once but a squire, tough training regimen, just heart and cold head have let you become a champion of order, the bane of undying and other walking dead.
            With your king calling for a great crusade against all that is evil you can stand no longer.
            Armed with Star of Time, a morningstar that can banish souls of the unliving for good, you are the bastion of hope for all the peasants and citizens that can finally sleep with no fear of ghoul or ghost."
            #spr1 "./bozmod/Monsters/Warpman/Green_knight.tga"
            #spr2 "./bozmod/Monsters/Warpman/Green_Knight_attack.tga" 
            #hp 25
            #str 13
            #att 16
            #def 16
            #prec 15
            #prot 0
            #size 3
            #mr 15
            #mor 20
            #enc 2
            #mountedhumanoid
            #mounted
            #mapmove 25
            #ap 20
            #eyes 2
            #weapon "Star of time"
            #armor 14
            #armor 21
            #armor 57
            #weapon 56
            #maxage 1000
            #woundfend 10
            #diseaseres 100
            #chaospower -1
            #inspirational 2
            #okleader
            #magicskill 0 2
            #magicskill 2 2
            #magicskill 6 2
            #magicboost 53 -5
            #researchbonus -15
            #batstartsum1d3 22
            #end
        -- Just Knight ------------------------------------------------------------

            #newmonster 7014
            #name "Just knight"
            #descr "Once but an old garrison commander, your vigilance and patience have let you become more than just a champion of the order, but also a great judge of character, seeing evil just by looking in the eyes of a human. Demon worshippers have been slain by dozens by your hand, and they say that any sword you have is imbued with just power to smite daemonic.
            With your king calling for a great crusade against all that is evil you can stand no longer.
            Though armed with but a sword, daemons reel from your very touch, as if burnt by holy water."
            #spr1 "./bozmod/Monsters/Warpman/Red_knight.tga"
            #spr2 "./bozmod/Monsters/Warpman/Red_Knight_attack.tga" 
            #hp 28
            #str 13
            #att 15
            #def 15
            #prec 16
            #prot 0
            #size 3
            #mr 15
            #mor 20
            #enc 2
            #mountedhumanoid
            #mounted
            #mapmove 25
            #ap 20
            #eyes 2
            #weapon "Just man's sword"
            #armor 14
            #armor 21
            #armor 57
            #weapon 56
            #maxage 1000
            #woundfend 10
            #diseaseres 100
            #chaospower -1
            #goodleader
            #magicskill 0 2
            #magicskill 2 2
            #magicskill 6 2
            #magicboost 53 -5
            #researchbonus -15
            #incprovdef 1
            #batstartsum1d3 22
            #end
        -- Special Weapons
            #newweapon
            #name "Tyrant's bane"
            #dmg 8
            #att 2
            #len 4
            #sound 89
            #dt_large
            #pierce
            #armorpiercing
            #bonus
            #charge
            #norepel
            #magic
            #end


            #newweapon 
            #name "Judgement"
            #dmg 8
            #att 1
            #len 2
            #sound 8
            #slash
            #unrepel
            #end

            #newweapon 1602
            #name "Star of time"
            #dmg 8
            #att 1
            #len 2
            #sound 8
            #blunt
            #flail
            #magic
            #dt_holy
            #secondaryeffect 1603
            #end

            #newweapon 1603
            #name "Final rest"
            #dmg 10
            #aoe 1
            #dt_aff 49
            #mrnegates
            #undeadonly
            #end

            #newweapon 1604
            #name "Just man's sword"
            #dmg 8
            #att 1
            #len 2
            #sound 8
            #slash
            #dt_holy
            #magic
            #secondaryeffect 1605
            #end

            #newweapon 1605
            #name "Touch of justice"
            #dmg 10
            #aoe 1
            #dt_aff 16
            #demononly
            #end
    -- Slayer of Apostles ------------------------------------------------------------
        #newmonster 7015
        #name "Slayer of apostles"
        #descr "A former lictor of early Ermorian history, slayer saw the turbulent times of the reborn god, the rising and culling of cults all around the empire and the coming of the true Pretender God.
        More fake apostles and pathetic prophets have ben culled by his hand than any other, and so when his old and broken body was interred in a family tomb his spirit reeled against the injustices of fake apostles still breathing.
        His ghostly form still riding a horse has been sighted many times afterwards, long face ever stern, sword of justice in his hand, his arm outstreched as if clutching for the neck of another prophet.
        His mere presence causes false pretenders reel, his grasp paralyzes them with fear and his blade causes even the strong-willed priests faint. Let it be known that nothing holy can sleep tight, for the hunt never ended, and horns shaped in twisted visage of laurels shall be the last thing they see."
        #spr1 "./bozmod/Monsters/Warpman/Death_knight.tga"
        #spr2 "./bozmod/Monsters/Warpman/Death_Knight_Attack.tga" 
        #pathcost 40
        #triplegod 1
        #triplegodmag 2
        #triple3mon
        #minprison 0
        #maxprison 1
        #gcost 120
        #hp 25
        #str 13
        #att 16
        #def 14
        #prec 16
        #prot 2
        #size 3
        #mr 14
        #mor 30
        #enc 0
        #mountedhumanoid
        #mounted
        #mapmove 35
        #ap 35
        #eyes 2
        #weapon 510
        #weapon 477
        #armor 116
        #armor 148
        #weapon 56
        #maxage 1000
        #woundfend 99
        #diseaseres 100
        #goodleader
        #goodundeadleader
        #magicskill 4 2
        #magicskill 5 2
        #magicskill 7 2
        -- magicboost 53 -5 -- buff to slayer himself ы
        #researchbonus -10 -- buff ы
        #undead 
        #poisonres 50
        #coldres 15
        #ethereal
        #haltheretic 5
        #fear 8
        #incscale 0
        #batstartsum2d6 566
        #batstartsum2d3 1541
        #batstartsum1d3 3067
        #raiseonkill 100
        #end
        -- Bringer of Blight ------------------------------------------------------------
            #newmonster 7016
            #name "Bringer of Blight"
            #descr "Not much is known of where this pale rider comes from, what was his name in life or what battles he fought, for the only thing that remains afterwards is blighted countryside, rotting corpses and starving cattle dying next to the road.
            Malign spirit cares not for the shell, changing one corpse for another, as long as his trail of blighted conquest continues there will never be a shortage of bodies.
            Bringer of blight is often known to carry an axe of executioner, and a bow that spreads putrid smoke in all four directions."
            #spr1 "./bozmod/Monsters/Warpman/Blight_Knight.tga"
            #spr2 "./bozmod/Monsters/Warpman/Blight_Knight_Attack.tga" 
            #hp 25
            #str 14
            #att 16
            #def 14
            #prec 16
            #prot 2
            #size 3
            #mr 16
            #mor 30
            #enc 0
            #mountedhumanoid
            #mounted
            #mapmove 25
            #ap 20
            #eyes 2
            #weapon 259
            #armor 33
            #armor 118
            #weapon 56
            #weapon "The Wind"
            #maxage 1000
            #woundfend 99
            #diseaseres 100
            #spiritsight
            #undead 
            #poisonres 50
            #coldres 5
            #okleader
            #goodundeadleader
            #magicskill 4 2
            #magicskill 5 2
            #magicskill 7 2
            #magicboost 53 -3 -- buff ы
            #magicboost 5 3 -- buff ы
            #researchbonus -15
            #leper 5
            #popkill 10
            #pillagebonus 50
            #incscale 3
            #immortal
            #domsummon -2 -- makes skellies ы
            #domsummon -3 -- makes zombies ы
            #domsummon -15 -- makes more zombies with more corpses1 ы
            #domsummon2 -4 -- ghouls too, but little ы
            #heal
            #raiseonkill 100
            #end
        -- Eraser of Magic ------------------------------------------------------------
            #newmonster 7017
            #name "Eraser of magic"
            #spr1 "./bozmod/Monsters/Warpman/Eraser.tga"
            #spr2 "./bozmod/Monsters/Warpman/Eraser_attack.tga"
            #descr "The one known as Eraser of magic was once a tyrant of the Ether clan, and his exploits at conquering all the lands of Arcana are widely known as folklore of the clans, alas spoken in hushed tones for he is not viewed as hero any more.
            It was by his blade that the fae were cut down and drained of the magic they had, it was by his decree that blood seekers rode out in the night, stealing girls from their families to later on feed the war machine, it was his banner that flew atop the last fort of the dragon lords.
            Arcana lies barren and drained by his steel fist, and now he claims to be a god.
            With his enormous strength he can strike with a moonblade single-handed, and his huge stallion has trampled more soldiers than lesser warriors ever saw in their lifetime."
            #hp 35
            #size 4
            #prot 3
            #mor 16
            #mr 20
            #str 17
            #att 16
            #def 15
            #prec 15
            #mapmove 23
            #ap 25
            #enc 1
            #armor 18
            #armor 118
            #armor 73
            #mounted
            #weapon 289
            #weapon 615
            #magicbeing
            #spiritsight
            #magicpower 2 -- buff ы
            #ethereal
            #magicskill 4 2
            #magicskill 5 2
            #magicskill 7 2 
            #magicboost 53 -3 -- buff ы
            #magicboost 4 3 -- buff  ы
            #researchbonus -15
            #woundfend 99
            #diseaseres 100
            #drainimmune
            #goodleader 
            #expertmagicleader
            #incunrest 10
            #maxage 1500
            #incscale 5
            #heal
            #end
        -- special weapons
            #newweapon 
            #name "Putrid smoke"
            #secondaryeffectalways 50
            #aoe 1
            #dmg 5
            #sound 50
            #dt_poison
            #dt_raise 
            #explspr 10200
            #end

            #newweapon 
            #name "The Wind"
            #secondaryeffectalways "Putrid smoke"
            #dmg 12
            #range 35
            #prec -5
            #nratt 4
            #pierce 
            #magic 
            #bowstr
            #twohanded
            #ammo 10
            #flyspr 419
            #nouw
            #end 
    -- God Emperor of the Dunes ------------------------------------------------------------
        #newmonster 7018
        #name "Emperor of the Sands"
        #spr1 "./bozmod/Monsters/God_of_Sands.tga"
        #spr2 "./bozmod/Monsters/God_of_Sands_Attack.tga"
        #descr "The Sand Emperor was once a trusted advisor of the previous Pantokrator. He is gifted with the ability to commune with the sands and from their whispers, he can peer into the future to avoid danger, or make decisions knowing that their outcomes will bring great fortune. Some say that the Pantokrator's disappearance had something to do with the advise that was given to him by the Sand Emperor - nobody truly knows except for the Sand Emperor, who now has obligations to nobody. Having nothing higher left to obtain than godhood, the Sand Emperor has donned the title of a god and seeks to become the new Pantokrator."

        #pathcost 30
        #gcost 180
        #hp 230
        #heal
        #size 6
        #prot 16
        #mor 30
        #mr 22
        #str 23
        #att 5
        #def 2
        #prec 16
        #mapmove 8
        #ap 6
        #enc 3
        #weapon 532 -- tail sweep
        #magicbeing
        #spiritsight
        #magicskill 3 1
        #magicskill 4 2
        #slothresearch 4
        #expertleader
        #expertmagicleader
        #maxage 7000
        #curseluckshield 1
        #awe 1
        #bringeroffortune 30
        #diseaseres 100
        #nobadevents 30
        #spellsinger
        #trample
        #slothpower 2
        #regeneration 5
        #noriverpass
        #fireres 10
        #shockres 8
        #coldres -7
        #luck
        #uwdamage 100
        #bluntres
        #itemslots 1
        #homerealm 10
        #startdom 3        
        #woundfend 50
        #end
    -- Holy Dragon ------------------------------------------------------------
        #newmonster 7019
        #name "Dragon of Blinding Virtue"
        #spr1 "./bozmod/Monsters/Warpman/LightDragon_1.tga"
        #spr2 "./bozmod/Monsters/Warpman/LightDragon_2.tga"
        #descr "Aeons before the ascension wars the dragon of virtue was a beacon of light and a moral compass for many. He was a humble creature who never had ambitions other than to serve and help bring peace to more people. When a Pantokrator arose, the dragon of virtue was an impediment to the absolute authority of the pantokrator who wanted himself to be the only moral beacon of others. After criticizing the pantokrators' judgement one times too many, the pantokrator's patience ran out and he blinded the dragon for his insolence, saying if he cannot bear to see him bestow judgements then he shall not see at all.

        Now the Pantokrator is gone, the dragon of virtue has determined that only through his leadership can the greatest harmony and peace be achieved. He has donned the title of god and followers flock to his cause. The dragon of virtue is pale, his eyes gone by the decree of the Pantokrator and his scales as magnificent as they are brittle. Though almost incapable of fighting, Dragon of virtue is incredibly powerful where the sun touches his realm.
        Taking this chassis additionally gives you Beacon of Light site in your capital, giving your blessed units additional +2 Awe bonus. Enemies taking the site will get the reduced +1 bonus instead."
        #hp 105
        #gcost 210
        #size 6
        #prot 14
        #mor 30
        #mr 20
        #str 22
        #att 13
        #def 9
        #prec 5
        #mapmove 25
        #ap 10
        #enc 1
        #weapon 532
        #weapon 48
        #flying
        #magicbeing
        #blind
        #dragonlord 1
        #sunawe 3
        #dompower 2
        #magicskill 4 1
        #magicskill 0 1
        #magicskill 8 1
        #magicboost 8 -1
        #userestricteditem 7004
        #goodleader 
        #expertmagicleader
        #maxage 700
        #decscale 2
        #fireres 10
        #heat 4
        #eyeloss
        #shapechange 7020
        #diseaseres 100
        #homerealm 3 --mediterranean
        #itemslots 274560 --[262144 + 128 + 12288] - head, 2 misc, head only crown
        #pathcost 80
        #startdom 2
        #woundfend 1
        #heal
        #end

        #newmonster 7020
        #name "Harbinger of Virtue"
        #spr1 "./bozmod/Monsters/Warpman/BlindDude1.tga"
        #spr2 "./bozmod/Monsters/Warpman/BlindDude_2.tga"
        #descr "Aeons before the ascension wars the dragon of virtue was a beacon of light and a moral compass for many. He was a humble creature who never had ambitions other than to serve and help bring peace to more people. When a Pantokrator arose, the dragon of virtue was an impediment to the absolute authority of the pantokrator who wanted himself to be the only moral beacon of others. After criticizing the pantokrators' judgement one times too many, the pantokrator's patience ran out and he blinded the dragon for his insolence, saying if he cannot bear to see him bestoy judgements then he shall not see at all.

        Now the Pantokrator is gone, the dragon of virtue has determined that only through his leadership can the greatest harmony and peace be achieved. He has donned the title of god and followers flock to his cause.

        In his human form the dragon of virtue takes the shape of an old blind priest, and while even less capable of fighting, his oratory skills inspire people to greater deeds when they have no fear of his blinding radience."
        #hp 8
        #size 2
        #prot 0
        #mor 30
        #mr 20
        #str 6
        #att 3
        #def 3
        #prec 3
        #mapmove 14
        #ap 5
        #enc 1
        #weapon 48
        #blind
        #sunawe 1
        #dompower 3
        #magicskill 4 1
        #magicskill 0 1
        #magicboost 8 4
        #spreaddom 2
        #magicboost 53 -2
        #goodleader 
        #expertmagicleader
        #maxage 700
        #fireres 5
        #eyeloss
        #shapechange 7019
        #secondshape 7019 -- now properly turns into dragon when wounded like all dragons ы
        #startdom 2
        #woundfend 1
        #diseaseres 100
        #end   

        #newitem
        #name "Burning halo"
        #constlevel 12
        #copyspr 377
        #type 9
        #autospell "Stellar Cascades"
        #autospellrepeat 3 -- tiny buff
        #mainpath 0
        #mainlevel 5
        #nofind
        #cursed
        #end

        #newsite 1405
        #name "Beacon of Light"
        #path 4
        #level 4
        #loc 512
        #rarity 5
        #blessawe 2
        #dominion 2
        #end

        #newsite 1406
        #name "Unlit Beacon"
        #path 4
        #level 1
        #loc 512
        #rarity 5
        #blessawe 1
        #dominion 1
        #end

        #newevent
        #rarity 5
        #req_targmnr 7019 
        #req_targnoitem "Burning halo" -- the halo
        #nolog
        #notext
        #addequip 9
        #msg "Solar halo engulfed Dragon of Virtue [Burning halo]"
        #end

        #newevent
        #rarity 0
        #req_pregame 1
        #req_owncapital 1
        #req_godismnr  7019
        #addsite 1405
        #notext
        #nolog
        #end

        #newevent
        #rarity 0
        #req_capital 1
        #req_owncapital 0
        #req_site 1
        #removesite 1405
        #addsite 1406
        #msg "The beacon of light no longer shines as it used to, though you still can use remnants [Beacon of Light]"
        #end

        #newevent
        #rarity 0
        #req_owncapital 1
        #req_godismnr "Harbinger of Virtue"
        #req_site 1
        #removesite 1406
        #addsite 1405
        #msg "The beacon of light no longer shines as it used to, though you still can use remnants [Unlit Beacon]"
        #end
    -- Queen of Crustaceans ------------------------------------------------------------
        #newmonster 7021    
        #name "Queen of Crustaceans"
        #spr1 "./bozmod/Monsters/queen_crustacean.tga"
        #spr2 "./bozmod/Monsters/queen_crustacean2.tga"
        #descr "The queen of crustaceans is an ageless primordial entity that existed in a time when all living things were of the ocean. Some say she is the proginator of the Atlantians, others say she is just a legend. Her presence carries the weight of eons and animals are in awe when they see her fearsome visage. She has taken the title of a god to clear the world of the other pretenders and become the new Pantokrator. When she is in friendly dominion, she will attract various shrimps to join her in her cause."

        #pathcost 80
        #gcost 240
        #hp 115
        #heal
        #size 6
        #prot 16
        #mor 30
        #mr 15
        #str 22
        #att 14
        #def 10
        #prec 8
        #mapmove 8
        #ap 7
        #enc 2
        #weapon 532 -- tail sweep
        #weapon 392 -- torch of strife
        #weapon 1606 -- shrimp punch
        #magicskill 6 1
        #magicskill 4 1
        #magicskill 2 1
        #expertleader
        #maxage 70000
        #animalawe 2
        #diseaseres 100
        #shockres -4
        #coldres 5
        #naga
        #aquatic
        #homerealm 9
        #startdom 3
        #domsummon 2369 --large shrimp
        #domsummon2 5909 --shrimp warrior
        #batstartsum4d6 2369
        #woundfend 10
        #diseaseres 100
        #end

        #newweapon 1606
        #name "Shrimp Punch"
        #copyweapon 1575
        #bonus
        #end
    -- chaos snek
        #selectmonster 2793
        #chaospower 1
        #end 
    -- spring  snek
        #selectmonster 2799
        #magicskill 6 1
        #end 
    -- kang in yellow
        #selectmonster 3396
        #gcost 240
        #end 
    -- dead king skellies
        #newevent -- chaff raising for throned king 
        #nation -2
        #rarity 5
        #req_dominion 1
        #req_domchance 10 -- stupid high
        #req_godismnr 5071
        #msg "makes some undeads rise."
        #notext
        #nolog
        #req_pop0ok
        #1d3units -2
        #end

        #newevent -- chaff raising for throned king 
        #nation -2
        #rarity 5
        #req_dominion 1
        #req_fort 1
        #req_domchance 10 -- stupid high
        #req_godismnr 5071
        #msg "makes some undeads rise."
        #notext
        #nolog
        #req_pop0ok
        #1d6units -2
        #3d3units -15
        #end

        #newevent -- chaff raising for throned king 
        #nation -2
        #rarity 5
        #req_dominion 1
        #req_domchance 10 -- stupid high
        #req_godismnr 5071
        #msg "makes some undeads rise."
        #notext
        #nolog
        #req_pop0ok
        #1d3units -15
        #end

        #newevent -- chaff raising for throned king 
        #nation -2
        #rarity 5
        #req_dominion 1
        #req_temple 1
        #req_domchance 10 -- stupid high
        #req_godismnr 5071
        #msg "makes some undeads rise."
        #notext
        #nolog
        #req_pop0ok
        #1d3units 566
        #1d3units 676
        #1d3units 675
        #end
    -- Myrmecoleon 
        #selectmonster 2790
        #poisonres 15
        #fear 10
        #end 
    -- high diviner ы
        #selectmonster 5039
        #nobadevents 25
        #bringeroffortune 5
        #end 
    -- gigashark
        #newmonster 3500
        #name "Anathemant of the seas"    
        #spr1 "./bozmod/Monsters/Warpman/Sharkman1.tga"
        #spr2 "./bozmod/Monsters/Warpman/Sharkman2.tga"
        #hp 66
        #size 4
        #prot 9
        #mr 18
        #mor 30
        #str 18
        #att 12
        #def 12
        #prec 8
        #enc 2
        #ap 12
        #mapmove 16
        #amphibian
        #douse 5
        #descr "It is said that Megalodon is bound to the seas by the decree of Pantokrator, but since he's gone, the shackles are weakening and he can now take shape of a huge humanoid with shark head to go on dry land.
        Supernaturally keen smell lets him find his favourite food with ease, lessening his appetites for regular food greatly.
        While water magic is harder on dry land and he's not as physically strong as his true form beneath the waves, Anathemant of the seas, as he likes to call himself, is still as strong as giants and can bite off huge chunks out of his enemies with ease."
        #weapon "Claws"
        #weapon 20 -- bite, str buffed
        #weapon 690 -- draw blood 
        #gcost 120
        #watershape "Megalodon"
        #maxage 6000
        #popkill 10
        #darkvision 50
        #magicboost 2 -1
        #woundfend 10
        #diseaseres 100
        #heal
        #end     

        #selectmonster 5055
        #magicskill 7 3
        #magicskill 2 1
        #landshape "Anathemant of the seas"
        #amphibian
        #woundfend 10
        #diseaseres 100
        #heal
        #end
    -- Senator Strongarm ------------------------------------------------------------
        #newweapon
        #copyweapon 562
        #name "Iron Fist"
        #iron
        #end

        #newmonster 7035 --senator
        #hp 42
        #name "Senator Strongarm"
        #size 3
        #descr "Senator Strongarm has always been keen to expand the Ermorian empire, always seeking ways to make Ermor a singular superpower, both military and economically.
        His insights into arcane and magic of metals have led him to become what he is, many say that it is impossible to pierce his skin, something many political opponents tried in vain over the years.
        With the prophet shrouded in white came a period of peace and prosperity, something anathema to The Senator, for he believes war to be the one true driver of the economy.
        His domain is in a constant state of improvement, for he is restless in his desire for Ermor to become the god-nation, claiming Imperium once and for all."
        #spr1 "./bozmod/Monsters/Warpman/Senator1.tga"
        #spr2 "./bozmod/Monsters/Warpman/Senator2.tga"
        #att 12
        #def 10
        #str 16
        #prec 11
        #prot 2
        #mr 21
        #weapon "Iron Fist"
        #weapon "Iron Fist"
        #magicskill 3 1
        #magicskill 4 1
        #pathcost 20
        #startdom 1
        #homerealm 3
        #gcost 180
        #enc 1
        #mapmove 16
        #ap 12
        #dompower 2
        #startage 180
        #maxage 600
        #heal
        #fireres 5
        #poisonres 5
        #slashres
        #incprovdef 5
        #shrinkhp 20
        #okleader 
        #command 100
        #inspirational -1
        #researchbonus -25
        #onebattlespell "Ironskin"
        #woundfend 99
        #diseaseres 100
        #end

        #newmonster 7036 --senator
        #hp 84
        #name "Wounded Senator"
        #size 3
        #descr "Senator Strongarm has always been keen to expand the Ermorian empire, always seeking ways to make Ermor a singular superpower, both military and economically.
        His insights into arcane and magic of metals have led him to become what he is, many say that it is impossible to pierce his skin, something many political opponents tried in vain over the years.
        With the prophet shrouded in white came a period of peace and prosperity, something anathema to The Senator, for he believes war to be the one true driver of the economy.
        His domain is in a constant state of improvement, for he is restless in his desire for Ermor to become the god-nation, claiming Imperium once and for all.
        When suffering massive trauma Senator can go on the defensive, healing wounds but getting too stiff to move fast or attack effectively.
        
        The Senator provides a 1% per candle chance to add a tiny amount of gold income, or resources, per province per turn, these events happen silently."
        #spr1 "./bozmod/Monsters/Warpman/SenatorW1.tga"
        #spr2 "./bozmod/Monsters/Warpman/SenatorW2.tga"
        #att 8
        #def 15
        #str 20
        #prec 11
        #prot 10
        #mr 23
        #weapon "Iron Fist"
        #weapon "Iron Fist"
        #magicskill 3 1
        #magicskill 4 1
        #gcost 120
        #enc 3
        #mapmove 16
        #ap 3
        #dompower 2
        #startage 180
        #maxage 600
        #heal
        #regeneration 10
        #fireres 15
        #poisonres 10
        #bluntres
        #slashres
        #incprovdef 5
        #growhp 21
        #okleader 
        #fear 5
        #inspirational -3
        #researchbonus -25
        #onebattlespell "Ironskin"
        #woundfend 99
        #diseaseres 100
        #end

        #newmonster 7700 -- undying senator
        #hp 30
        #name "Undying Senator"
        #size 3
        #descr "Senator Strongarm has always been keen to expand the Ermorian empire, always seeking ways to make Ermor a singular superpower, both military and economically.
        His insights into arcane and magic of metals have led him to become what he is, many say that it is impossible to pierce his skin, something many political opponents tried in vain over the years.
        With the prophet shrouded in white came a period of peace and prosperity, something anathema to The Senator, for he believes war to be the one true driver of the economy.
        His domain was in a constant state of improvement, for he was restless in his desire for Ermor to become the god-nation, claiming Imperium once and for all.
        Even when the cataclysm racked the empire he loved so dearly he would not succumb to injuries, his pride and memories too vivid to be brushed away as easily.
        With his last breath he defied death itself, crumbling into bone and iron dust, to be reformed months later, his desire to lead his once-grand nation to victory.
        If slain, his body shall reform once more, yet it is not as sturdy as it was in life."
        #spr1 "./bozmod/Monsters/Warpman/SkeletonSenator1.tga"
        #spr2 "./bozmod/Monsters/Warpman/SkeletonSenator2.tga"
        #att 13
        #def 13
        #str 18
        #prec 10
        #prot 8
        #mr 22
        #weapon "Iron Fist"
        #weapon "Iron Fist"
        #magicskill 3 1
        #magicskill 4 1
        #magicboost 5 2
        #magicboost 4 1
        #magicboost 3 -1
        #magicboost 2 -1
        #magigboost 1 -1
        #magicboost 0 -1
        #gcost 120
        #enc 0
        #mapmove 18
        #ap 5
        #dompower 1
        #startage 180
        #maxage 6000
        #immortal
        #undead
        #regeneration 15
        #fireres 5
        #coldres 15
        #shockres -5
        #poisonres 25
        #bluntres
        #slashres
        #incprovdef 1
        #okleader 
        #expertundeadleader
        #fear 7
        #inspirational -1
        #researchbonus -25
        #onebattlespell "Ironskin"
        #woundfend 99
        #diseaseres 100
        #heal
        #immortal
        #end

        #newmonster 7701 -- lemur senator
        #hp 30
        #name "Lemur Senator"
        #size 3
        #descr "Senator Strongarm has always been keen to expand the Ermorian empire, always seeking ways to make Ermor a singular superpower, both military and economically.
        His insights into arcane and magic of metals have led him to become what he is, many say that it is impossible to pierce his skin, something many political opponents tried in vain over the years.
        With the prophet shrouded in white came a period of peace and prosperity, something anathema to The Senator, for he believes war to be the one true driver of the economy.
        His domain was in a constant state of improvement, for he was restless in his desire for Ermor to become the god-nation, claiming Imperium once and for all.
        Even when the cataclysm racked the empire he loved so dearly he would not succumb to injuries, his pride and memories too vivid to be brushed away as easily.
        So strong his will was that even after being vanquished, he returned back once the soul gates were opened.
        Even being a shadow of his former self, he is still as resolved to bring his beloved nation to victory."
        #spr1 "./bozmod/Monsters/Warpman/GhostSenator1.tga"
        #spr2 "./bozmod/Monsters/Warpman/GhostSenator2.tga"
        #att 15
        #def 15
        #str 15
        #prec 12
        #prot 10
        #mr 22
        #weapon "Iron Fist"
        #weapon "Iron Fist"
        #magicskill 3 1
        #magicskill 4 1
        #magicboost 5 2
        #magicboost 4 1
        #magicboost 3 -1
        #magicboost 2 -1
        #magigboost 1 -1
        #magicboost 0 -1
        #gcost 120
        #enc 0
        #mapmove 18
        #ap 5
        #dompower 1
        #magicpower 1
        #startage 180
        #maxage 6000
        #immortal
        #undead
        #regeneration 5
        #fireres 5
        #poisonres 25
        #coldres 15
        #ethereal
        #incprovdef 2
        #spreaddom 1
        #okleader 
        #expertundeadleader
        #fear 7
        #inspirational -3
        #woundfend 99
        #diseaseres 100
        #heal
        #immortal
        #end

        #newevent
        #rarity 5
        #req_fornation 44
        #req_pop0ok
        #nation 44
        #req_targmnr 7035
        #transform 7700
        #notext
        #nolog
        #end

        #newevent -- event for ermorian senator gold+res in forts
        #nation -2
        #rarity 5
        #req_dominion 1
        #req_domchance 7 -- stupid high
        #req_godismnr 7700
        #req_fort 1
        #req_order 1
        #msg "Your undead minions established a simplistic fort manufactorum."
        #notext
        #nolog
        #landgold 2
        #landprod 1
        #req_pop0ok
        #end

        #newevent -- event for ermorian senator gold+res in forts
        #nation -2
        #rarity 5
        #req_dominion 1
        #req_domchance 7 -- stupid high
        #req_godismnr 7700
        #req_fort 1
        #req_prod 1
        #msg "Your undead minions established a simplistic fort manufactorum."
        #notext
        #nolog
        #landgold 1
        #landprod 2
        #req_pop0ok
        #end

        #newevent
        #rarity 5
        #req_fornation 82
        #req_pop0ok
        #nation 82
        #req_targmnr 7035
        #transform 7701
        #notext
        #nolog
        #end

        #newevent -- event for ermorian senator gold+res in forts
        #nation -2
        #rarity 5
        #req_dominion 1
        #req_pop0ok
        #req_domchance 10 -- stupid high
        #req_godismnr 7701
        #req_fort 1
        #req_order 1
        #msg "Your undead minions established a simplistic fort manufactorum."
        #notext
        #nolog
        #landgold 1
        #end

        #newevent -- event for ermorian senator gold+res in forts
        #nation -2
        #rarity 5
        #req_dominion 1
        #req_pop0ok
        #req_domchance 10 -- stupid high
        #req_godismnr 7701
        #req_fort 1
        #req_prod 1
        #msg "Your undead minions established a simplistic fort manufactorum."
        #notext
        #nolog
        #landgold 1
        #end

        #newevent 
        #nation -2
        #rarity 5
        #req_dominion 1
        #req_pop0ok
        #req_domchance 2 ы
        #req_godismnr 7035
        #req_order 1
        #msg "Local populace finally embraced the rule of law and order to their heart"
        #notext
        #nolog
        #landgold 1
        #end

        #newevent 
        #rarity 5
        #req_dominion 1
        #req_domchance 2 ы
        #notext
        #nolog
        #req_godismnr 7035
        #req_order 2
        #msg "Local populace finally embraced the rule of law and order to their heart 2"
        #landgold 2
        #end

        #newevent 
        #rarity 5
        #req_dominion 1
        #req_domchance 2 ы
        #notext
        #nolog
        #req_godismnr 7035
        #req_order 3
        #msg "Local populace finally embraced the rule of law and order to their heart 3"
        #landgold 3
        #end

        #newevent 
        #nation -2
        #rarity 5
        #req_dominion 1
        #req_domchance 3 ы
        #req_godismnr 7035
        #req_mintroops 50
        #msg "Stationed garrison is improving the local economy"
        #landgold 1
        #req_pop0ok
        #notext
        #nolog
        #end

        #newevent 
        #nation -2
        #rarity 5
        #req_dominion 1
        #req_domchance 3 ы
        #req_godismnr 7035
        #req_mintroops 50
        #msg "Stationed garrison is improving the local economy"
        #landprod 1
        #req_pop0ok
        #notext
        #nolog
        #end

        #newevent 
        #nation -2
        #rarity 5
        #req_dominion 1
        #req_domchance 3 ы
        #req_godismnr 7035
        #req_mintroops 100
        #msg "Stationed garrison is improving the local economy"
        #landprod 1
        #landgold 1
        #req_pop0ok
        #notext
        #nolog
        #end

        #newevent 
        #nation -2
        #rarity 5
        #req_dominion 1
        #req_domchance 2 ы
        #notext
        #nolog
        #req_godismnr 7035
        #req_prod 1
        #req_order 1
        #msg "Local populace finally embraced the rule of law and order to their heart"
        #landgold 2
        #req_pop0ok
        #end

        #newevent 
        #nation -2
        #rarity 5
        #req_dominion 1
        #req_domchance 2
        #notext
        #nolog
        #req_godismnr 7035
        #req_prod 1
        #msg "Local populace finally embraced the rule of law and order to their heart"
        #landprod 1
        #req_pop0ok
        #end

        #newevent 
        #rarity 5
        #nation -2
        #req_dominion 1
        #req_domchance 1
        #req_godismnr 7035
        #req_prod 2
        #msg "Local populace finally embraced the rule of law and order to their heart 2"
        #landprod 2
        #end

        #newevent 
        #rarity 5
        #nation -2
        #req_dominion 1
        #req_domchance 1
        #req_godismnr 7035
        #req_prod 3
        #msg "Local populace finally embraced the rule of law and order to their heart 3"
        #landprod 3
        #end
    -- Bandar King --------------
        #newweapon
        #copyweapon 532
        #name "Fist slam"
        #dmg -2
        #len 1
        #blunt 
        #aoe 1
        #end

        #newweapon
        #copyweapon 424 -- boulder
        #name "Massive rock"
        #aoe 1
        #secondaryeffectalways 328 -- shatter. rock is extra effective against inanimate stuff
        #end

        #newweapon
        #name "Tackle" -- fatigue damage
        #damage 7 -- to get up to a rounded 30 when on pretender screen
        #len 0
        #blunt -- probably thats a mistake, but it feels like squishy stuff should be harder to squeeze
        #dt_stun
        #natural
        #inanimateimmune -- can't tackle a statue
        #internal -- no mossbody shenanigans
        #norepel
        #unrepel -- monke not afraid of you
        #end

        #newmonster 7130 --monke 1
        #hp 169
        #name "Bandarbarian"
        #fixedname "Kong"
        #size 6
        #descr "The Bandarbarian is a huge intelligent ape once tasked by the Pantokrator with keeping the jungles safe from the beasts that could find home there and reproduce in secret, threatening the variety of wildlife he enjoyed so much.
        Bandarbarian was granted a body unyielding as a mountain and unrelenting as the rivers that pass the jungles he protected. With the pantokrator gone the beasts that were thought exterminated have come out of hiding once again and threaten the jungle.
        He is intelligent, but can be angered and bored quite fast, making him rely on force and not magic for the most part.
        
        At 50% HP this pretender transforms into a more powerful shape."
        #spr1 "./bozmod/Monsters/Warpman/Kong1.tga"
        #spr2 "./bozmod/Monsters/Warpman/Kong2.tga"
        #att 14
        #def 9
        #str 26
        #prec 10
        #prot 5
        #mr 20
        #mor 20
        #weapon "Fist slam"
        #weapon "Fist slam"
        #weapon "Tackle"
        #weapon "Massive rock"
        #magicskill 6 1
        #magicskill 7 1
        #pathcost 80
        #startdom 2
        #gcost 220
        #enc 3
        #mapmove 16
        #ap 16
        #startage 600
        #maxage 3000
        #poisonres 5
        #homerealm 8
        #noleader 
        #researchbonus -25
        #fear 10
        #siegebonus 20
        #mountainsurvival
        #forestsurvival
        #animal
        #trample
        #secondshape 7150
        #woundfend 10
        #diseaseres 100
        #heal
        #end

        #newmonster 7150 --monke 2
        #hp 169
        #name "Bandar King"
        #size 6
        #descr "Bandar king is a huge intelligent ape once tasked by the Pantokrator with keeping the jungles safe from the beasts that could find home there and reproduce in secret, threatening the variety of wildlife he enjoyed so much.
        Bandar king was granted a body unyielding as a mountain and unrelenting as the rivers that pass the jungles he protected. With the pantokrator gone the beasts that were thought exterminated have come out of hiding once again and threaten the jungle.
        King is intelligent, but can be angered and bored quite fast, making him rely on force and not magic for the most part."
        #spr1 "./bozmod/Monsters/Warpman/Kong1.tga"
        #spr2 "./bozmod/Monsters/Warpman/Kong2.tga"
        #att 15
        #def 9
        #str 26
        #prec 10
        #prot 5
        #mr 20
        #mor 50
        #weapon "Fist slam"
        #weapon "Fist slam"
        #weapon "Tackle"
        #magicskill 6 1
        #magicskill 7 1
        #enc 3
        #mapmove 16
        #ap 16
        #startage 600
        #maxage 3000
        #poisonres 5
        #noleader 
        #researchbonus -25
        #fear 13
        #siegebonus 20
        #mountainsurvival
        #forestsurvival
        #animal
        #berserk 5
        #firstshape 7130
        #woundfend 15
        #diseaseres 100
        #heal
        #end
    -- Just a cube
        #newweapon
        #copyweapon "Acid"
        #name "Acid Splash"
        #aoe 1
        #bowstr
        #end

        #newweapon
        #copyweapon "Acid"
        #name "Acid Wave"
        #aoe 2
        #bowstr
        #end
        
        #newmonster 8208
        #hp 5
        #name "Tiny cube"
        #copyspr 2159
        #drawsize -50
        #size 1
        #descr "Just a cube, though it feels awkwardly small for a gelatinous cube. It's origins are mysterious, but it seems to ceaselessly grow, who knows how big it can get.
        
        *for some weird reason not even the allmighty Wish can make such a cube. It truly puzzled the most sophisticated of sages!
        *At 20 HP this pretender will transform into a larger form."
        #att 5
        #def 12
        #str 5
        #prec 0
        #prot 0
        #mr 10
        #mor 50
        #weapon "Acid"
        #magicskill 0 2
        #magicskill 2 2
        #heal
        #pathcost 80
        #startdom 2
        #gcost 120
        #enc 0
        #mapmove 1
        #ap 6
        #startage 5
        #maxage 10000
        #fireres 15
        #coldres 5
        #poisonres 15
        #blind
        #stealthy 25
        #aciddigest 4
        #acidshield 4
        #woundfend 99
        #diseaseres 100
        #slashres 
        #bluntres
        #pierceres
        #magicbeing
        #amphibian
        #mindless
        #stunimmunity
        #growhp 20
        #corpseeater 3
        #deadhp 2
        #noleader 
        #nomagicleader
        #noundeadleader
        #researchbonus -50
        #magicboost 53 -5
        #hpoverflow
        #randomspell 100
        #itemslots 1
        #homerealm 10
        #supplybonus -20
        #nowish
        #end

        #newmonster 8207
        #hp 10
        #name "Gelatin cube"
        #copyspr 2159
        #drawsize -25
        #size 2
        #descr "The slimy body of this cube seems to be unremarkable, yet its appetites seem to be beyond those of regular cubes.
        
        *for some weird reason not even the allmighty Wish can make such a cube. It truly puzzled the most sophisticated of sages!
        *At 40 HP this pretender will transform into a larger form."
        #att 7
        #def 10
        #str 7
        #prec 0
        #prot 0
        #mr 12
        #mor 50
        #weapon "Acid"
        #weapon "Acid"
        #enc 0
        #mapmove 1
        #ap 6
        #startage 5
        #gcost 120
        #maxage 10000
        #fireres 15
        #coldres 5
        #poisonres 15
        #blind
        #stealthy 15
        #aciddigest 4
        #acidshield 5
        #woundfend 99
        #diseaseres 100
        #slashres 
        #bluntres
        #pierceres
        #magicbeing
        #trample
        #trampswallow
        #incorporate 3
        #heal
        #amphibian
        #mindless
        #stunimmunity
        #growhp 40
        #corpseeater 4
        #deadhp 2
        #noleader 
        #nomagicleader
        #noundeadleader
        #researchbonus -40
        #magicboost 53 -4
        #hpoverflow
        #randomspell 80
        #itemslots 4096
        #supplybonus -30
        #nowish
        #end

        #newmonster 8206
        #hp 25
        #name "Acidic slime"
        #copyspr 2159
        #size 3
        #descr "It seems like this particular cube possesses uncanny intelligence, though probes show nothing but insatiable hunger and no soul to speak of.
        
        *for some weird reason not even the allmighty Wish can make such a cube. It truly puzzled the most sophisticated of sages!
        *At 60 HP this pretender will transform into a larger form."
        #att 9
        #def 8
        #str 11
        #prec 0
        #prot 0
        #mr 14
        #mor 50
        #weapon "Acid"
        #weapon "Acid Splash"
        #enc 0
        #mapmove 1
        #ap 6
        #startage 5
        #maxage 10000
        #fireres 15
        #coldres 5
        #poisonres 15
        #blind
        #aciddigest 4
        #incorporate 4
        #heal
        #acidshield 7
        #woundfend 99
        #diseaseres 100
        #slashres 
        #bluntres
        #pierceres
        #gcost 120
        #magicbeing
        #trample
        #trampswallow
        #amphibian
        #mindless
        #stunimmunity
        #growhp 60
        #corpseeater 5
        #deadhp 2
        #noleader 
        #nomagicleader
        #noundeadleader
        #researchbonus -30
        #magicboost 53 -3
        #hpoverflow
        #randomspell 70
        #itemslots 4096
        #supplybonus -40
        #nowish
        #end

        #newmonster 8205
        #hp 45
        #name "Ravenous slime"
        #copyspr 2159
        #drawsize 25
        #size 4
        #descr "After consuming innumerable amounts of corpses, the slime seems to have grown big enough to consume horsemen whole, and it seems that every foe consumed fuels the ravenous hunger of the creature.
        
        *for some weird reason not even the allmighty Wish can make such a cube. It truly puzzled the most sophisticated of sages!
        *At 120 HP this pretender will transform into a larger form."
        #att 10
        #def 7
        #str 15
        #prec 0
        #prot 0
        #mr 15
        #mor 50
        #weapon "Acid Splash"
        #weapon "Acid Splash"
        #enc 0
        #mapmove 1
        #ap 6
        #startage 5
        #maxage 10000
        #fireres 15
        #coldres 5
        #poisonres 15
        #blind
        #aciddigest 4
        #incorporate 5
        #heal
        #acidshield 8
        #woundfend 99
        #diseaseres 100
        #gcost 120
        #slashres 
        #bluntres
        #pierceres
        #magicbeing
        #trample
        #trampswallow
        #corpseeater 6
        #deadhp 2
        #amphibian
        #mindless
        #stunimmunity
        #growhp 120
        #noleader 
        #nomagicleader
        #noundeadleader
        #researchbonus -20
        #magicboost 53 -2
        #hpoverflow
        #randomspell 60
        #miscshape
        #itemslots 12288
        #supplybonus -60
        #nowish
        #end

        #newmonster 8204
        #hp 60
        #name "The Hunger"
        #size 5
        #descr "The slime that is now known as Hunger has consumed people, horses and chariots alike, leaving nothing but slimy desolation. A crown of unknown origin has been spotted flowing within, and scholars have started their efforts in finding out the origin of that artifact, seemingly untouched by the corrosive mass.
        
        *for some weird reason not even the allmighty Wish can make such a cube. Is it the crown or is there some other force at work here?
        *At 220 HP this pretender will transform into a larger form."
        #spr1 "./bozmod/Monsters/EliteZeon/DivineJellyMid1.tga"
        #spr2 "./bozmod/Monsters/EliteZeon/DivineJellyMid2.tga"
        #drawsize 10
        #att 11
        #def 6
        #str 17
        #prec 0
        #prot 0
        #mr 18
        #mor 50
        #weapon "Acid Splash"
        #weapon "Acid Splash"
        #enc 0
        #mapmove 1
        #ap 6
        #startage 5
        #maxage 10000
        #fireres 15
        #coldres 5
        #poisonres 15
        #blind
        #aciddigest 5
        #incorporate 6
        #heal
        #acidshield 10
        #woundfend 99
        #diseaseres 100
        #slashres 
        #bluntres
        #pierceres
        #magicbeing
        #trample
        #trampswallow
        #corpseeater 7
        #deadhp 2
        #amphibian
        #mindless
        #stunimmunity
        #growhp 220
        #noleader 
        #nomagicleader
        #noundeadleader
        #gcost 120
        #researchbonus -10
        #fear 5
        #magicboost 53 -1
        #hpoverflow
        #randomspell 50
        #itemslots 28672
        #miscshape
        #supplybonus -80
        #nowish
        #end

        #newmonster 8203
        #hp 120
        #name "Hunger made manifest"
        #size 6
        #descr "With tally numbering hundreds, if not thousands, the ravenous slime has grown to proportions unheard, of, and it seems to have ambitions, something that nobody could have expected of a mindless soulless mass of acidic slime. A crown now adorns it, aside from numerous other items captured during its path of consumption.
        The crown, it seems, once belonged to an impostor, a pretender god that rivaled the Pantokrator and was said to have been struck down by the allmighty one. Yet the crown has somehow survived, meaning the tale was wrong, for there was nothing that should have survived his strike.
        
        *cannot be wished for.
        *At 400 HP this pretender will transform into a larger form."
        #spr1 "./bozmod/Monsters/EliteZeon/DivineJellyMid1.tga"
        #spr2 "./bozmod/Monsters/EliteZeon/DivineJellyMid2.tga"
        #drawsize 20
        #att 12
        #def 5
        #str 19
        #prec 0
        #prot 0
        #mr 20
        #mor 50
        #weapon "Acid Splash"
        #weapon "Acid Splash"
        #weapon "Devour"
        #enc 0
        #mapmove 1
        #ap 6
        #startage 5
        #maxage 10000
        #fireres 15
        #coldres 5
        #poisonres 15
        #blind
        #aciddigest 5
        #incorporate 7
        #heal
        #acidshield 12
        #woundfend 99
        #diseaseres 100
        #slashres 
        #bluntres
        #pierceres
        #magicbeing
        #trample
        #gcost 120
        #trampswallow
        #corpseeater 10
        #deadhp 2
        #amphibian
        #mindless
        #stunimmunity
        #growhp 400
        #noleader 
        #nomagicleader
        #noundeadleader
        #researchbonus 
        #fear 7
        #hpoverflow
        #poisoncloud 1
        #randomspell 50
        #miscshape
        #itemslots 274560
        #supplybonus -100
        #nowish
        #end

        #newmonster 8202
        #hp 250
        #name "King Slime"
        #size 6
        #descr "Crown adorns the impossible mass of acid that slowly drifts across the lands, consuming all and everything in its path as a tidal wave of hunger and ambition. 
        The rolling mass of acid poisons the very land around it, and those who come too close risk being poisoned by the slime. It seems to be slowly evaporating, but the speed of consuming is not slowing in the slightest.
        Aeons ago the war for supremacy raged, and the greatest of rivals of Pantokrator was brought down low by a slime. In act of consuming a god whole, the slime achieved sentience, accumulating the vast knowledge and ambitions of the fallen god as its own.
        
        *cannot be wished for.
        *At 700 HP this pretender will transform into a larger form."
        #spr1 "./bozmod/Monsters/EliteZeon/DivineJellyLarger1.tga"
        #spr2 "./bozmod/Monsters/EliteZeon/DivineJellyLarger2.tga"
        #att 12
        #def 5
        #str 20
        #prec 0
        #prot 0
        #mr 22
        #mor 30
        #weapon "Acid Splash"
        #weapon "Acid Splash"
        #weapon "Devour"
        #weapon "Devour"
        #enc 0
        #mapmove 1
        #ap 6
        #startage 5
        #maxage 10000
        #fireres 15
        #coldres 5
        #poisonres 15
        #blind
        #aciddigest 6
        #incorporate 8
        #heal
        #acidshield 13
        #woundfend 99
        #diseaseres 100
        #slashres 
        #bluntres
        #pierceres
        #magicbeing
        #trample
        #trampswallow
        #corpseeater 15
        #deadhp 2
        #amphibian
        #mindless
        #stunimmunity
        #growhp 700
        #shrinkhp 120
        #noleader 
        #nomagicleader
        #noundeadleader
        #researchbonus 
        #fear 11
        #gcost 120
        #hpoverflow 
        #popkill 15
        #shrinkhp 120
        #poisoncloud 10
        #randomspell 30
        #miscshape
        #itemslots 290944
        #supplybonus -150
        #nowish
        #end

        #newmonster 8201
        #hp 500
        #name "Consumer of worlds"
        #size 6
        #descr "While every sage knows the story of Eater of the Dead, banished into the void by the Pantokrator for its transgressions, few know of other beings suffering similar fate. When furious Pantokrator found out his archnemesis to be brought down by some slime, he fell into a tantrum, ripping the slime in pieces and flunging it into the void.
        It is said that in the cold embrace of the void, the soul shimmers, coils and dies... all but the primitive side, the animal side. No wonder the cube was still awake. Drifting in the nothingness, it found ways to propel itself by making jets of evaporated acid. Aeons have passed, and the slime, now tiny speck of his former glory, devoid of all thought, has finally arrived back to the world that wronged him. And unlike his first coming, he now has formulated a purpose.
        Consumer of worlds seeks but one thing - becoming one with the world, though even now its mind is impossible to look into and ways are hidden even from the faithful worshipers of it, for the hunger knows not what it does, and seeks not what others do, for donning the mantle of god is but the first step towards consuming all that there is, was and ever will be.
        
        *cannot be wished for
        This is the final form of the slime god."
        #spr1 "./bozmod/Monsters/EliteZeon/DivineJellyTitan1.tga"
        #spr2 "./bozmod/Monsters/EliteZeon/DivineJellyTitan2.tga"
        #att 12
        #def 5
        #str 21
        #prec 0
        #prot 0
        #mr 25
        #mor 30
        #weapon "Acid Splash"
        #weapon "Acid Wave"
        #weapon "Devour"
        #weapon "Devour"
        #enc 0
        #mapmove 1
        #ap 6
        #startage 5
        #maxage 10000
        #fireres 15
        #coldres 5
        #poisonres 15
        #blind
        #aciddigest 7
        #incorporate 9
        #heal
        #acidshield 15
        #woundfend 99
        #diseaseres 100
        #slashres 
        #bluntres
        #pierceres
        #magicbeing
        #trample
        #trampswallow
        #amphibian
        #bonusspells 1
        #stunimmunity
        #noleader
        #nomagicleader
        #noundeadleader
        #fear 15
        #gcost 120
        #corpseeater 30
        #deadhp 2
        #hpoverslow 1000
        #popkill 35
        #shrinkhp 250
        #poisoncloud 10
        #randomspell 50
        #miscshape
        #itemslots 323712
        #supplybonus -200
        #onebattlespell "Foul Vapors"
        #nowish
        #end
    -- Horny
        -- uber rage spell
            #newspell 
            #copyspell 991 -- rage 
            #name "Berserker rage"
            #descr "Lost to rage within, the warrior will fight on until nothing is left."
            #school -1
            #effect 10 
            #damage 256 -- gone berserk
            #nreff 1
            #range 0
            #spec 554320000 -- Ignore Armor & Shields, Undead, lifeless & mindless immune, MR negates easily.                        
            #end
        
            #newspell 
            #copyspell 991 -- rage
            #name "Engulfed in Bloodlust"
            #descr "Anyone coming too close to Horned Reaper will find his mind red with blind rage."
            #school -1
            #aoe 3
            #range 20
            #nextspell "Berserker rage"
            #end                    
        -- gaze item
            #selectitem 688                    
            #clear
            #name "Gaze of Reaper"
            #spr "./bozmod/Krieg/Gaze.tga"
            #descr "All those those who meet Reaper's gaze become filled with bloodlust, hacking at friend and foe alike."
            #constlevel 12
            #mainpath 7
            #mainlevel 10
            #nofind
            #type 8
            #cursed
            #autospell "Engulfed in Bloodlust"
            #autospellrepeat 1
            #end
        -- base
            #newmonster 7096
            #name "Horned One"
            #spr1 "./bozmod/Krieg/RReaper1.tga"
            #spr2 "./bozmod/Krieg/RReaper2.tga"
            #hp 66
            #size 4
            #prot 8
            #mr 20
            #mor 30
            #str 20
            #att 16
            #def 14
            #prec 6
            #enc 2
            #ap 15
            #mapmove 16
            #gcost 210 -- tiny cost reduction
            #chaospower 2
            #pillagebonus 50
            #spiritsight
            #magicskill 0 3
            #magicskill 7 3
            #magicskill 5 3
            #magicboost 5 -3
            #magicboost 0 -2
            #magicboost 7 -2
            #fireres 15
            #fixedname "Reaper"
            #descr "There is not much known about the Horned One, except that he predates the coming of Jinn expedition and will probably see the end of times when the unthinkable happens and pantokrator goes away. A living embodiment of rage and primordial hate, horned reaper scours the umbral plains seeking vengeance agains those that dare stand up to him. Devils have long since tried communicating with the horned one, yet all attempts just added more nicks on the huge scythe of the fiend. With the pantokrator gone the throne plane lies open, and the devils managed to trick the horned one into donning the mantle of god, sending him off away from their realm for good.
            Wherever reaper goes, faith falters and chaos ensures. Not even your own faithful can retain their posture when he is nearby.
            Reaper has no affinity with magic, but blesses his chosen with greater boons than one could expect."
            #noleader
            #noundeadleader
            #nomagicleader
            #combatcaster
            #weapon 1300
            #demon
            #invulnerable 20
            #fear 6
            #popkill 6
            #allret 100
            #itemslots 277638 - only crown
            #armor 2000 -- fused plates
            #drawsize -33
            #pathcost 60
            #startdom 2
            #startitem 688
            #unsurr 1
            #diseaseres 100
            #woundfend 99
            #end 
    -- Overlord
        #newmonster 7098
        #name "Overlord"
        #spr1 "./bozmod/Krieg/Overlord1.tga"
        #spr2 "./bozmod/Krieg/Overlord2.tga"
        #hp 32
        #size 2
        #prot 3
        #mr 20
        #mor 20
        #str 14
        #att 15
        #def 15
        #enc 2
        #prec 12
        #ap 14
        #mapmove 12
        #gcost 200
        #magicskill 0 1
        #magicskill 7 1
        #magicskill 4 1
        #fireres 5
        #fixedname "Nameless One"
        #descr "Once a noble hero fighting the evil off in the name of a righteous god and defeated, imprisoned and tortured into a twisted irony of his former self, Overlord as he is known today was a toy of the previous pantokrator, reveling in the sight of the embers of faith the blasphemer still held onto every now and then. With the pantokrator gone he broke free once more, claiming domain over the very things he once fought. Endless swarms of minions follow suit where his iron soles march and it seems that nothing can stop him from claiming the mantle of godhood itself.
        Imps flock towards him as if moth drawn to the flame, and the stronger the dominion is, the more imps shall come to aid."
        #superiorleader
        #goodundeadleader
        #goodmagicleader
        #combatcaster
        #weapon 11 -- greatsword
        #armor 38
        #armor 40
        #drawsize -60
        #pathcost 20
        #startdom 2
        #domsummon 7051
        #domsummon -1669
        #domsummon2 -1669
        #summon4 -1669
        #batstartsum2d6 7051
        #batstartsum1d6 -1669
        #homerealm 8
        #homerealm 1
        #woundfend 1
        #diseaseres 100
        #heal
        #end
    -- Slann\Meru
        #newmonster 7093
        #name "Lady of Whips"
        #spr1 "./bozmod/Krieg/Meru1.tga"
        #spr2 "./bozmod/Krieg/Meru2.tga"
        #hp 49
        #size 4
        #prot 1
        #mr 21
        #mor 21
        #str 20
        #att 13
        #def 13
        #enc 1
        #prec 13
        #ap 13
        #mapmove 22
        #gcost 250
        #pathcost 60
        #startdom 3
        #awe 1
        #fireres 10
        #stealthy 10
        #seduce 16 ы
        #flying
        #female
        #spiritsight
        #demon
        #magicskill 7 1
        #magicskill 4 1
        #magicskill 0 1
        #fixedname "Meru"
        #descr "She who is known as Lady of Whips was once but a lowly succubus, rising through the ranks of infernal realm slowly and steadily, her silky touch paving the way to demise for anyone who tried to oppose her. Having established herself as the undisputed ruler over the succubae and having a leverage over every devil alive, there is nothing left to achieve in the hellish realm, and so the godly mantle is the only thing left to sate her unending desire.
        All succubae are her lovely pets, and she takes great pleasure in teaching them new tricks, be they magic or more mundane. Chances depend on your dominion power in the province."
        #okleader
        #okundeadleader
        #okmagicleader
        #weapon 63
        #weapon 481
        #weapon 387
        #itemslots 277662 -- 4 arms and crown
        #invulnerable 15
        #drawsize -55
        #homerealm 1
        #homerealm 3
        #woundfend 5
        #diseaseres 100
        #heal
        #end 
    -- God Hand
        #newweapon
        #copyweapon 714 -- tremor
        #name "Grand Slam"
        #aoe 5
        #secondaryeffectalways 869 -- regular stun
        #end
        
        #newweapon
        #name "Giant fist"
        #dmg -3
        #blunt
        #len 0
        #unrepel
        #norepel
        #bonus
        #secondaryeffectalways "Grand Slam"
        #end

        #newweapon
        #copyweapon 73 -- area death
        #name "Drop dead"
        #mrnegates
        #end

        #newweapon
        #copyweapon 449 - big life steal
        #name "Touch of doom"        
        #secondaryeffect "Drop dead"
        #end
        
        #newmonster 8210
        #name "God Hand"
        #miscshape
        #spr1 "./bozmod/Monsters/Warpman/HandY1.tga"
        #spr2 "./bozmod/Monsters/Warpman/Hand8.tga"
        #descr "For ages have the faithful believed that Hand of God has shaped the world, appearing wherever it is necessary and blessing the world with its touch. So great was their belief in The Hand, that Pantokrators own arm achieved sentience. 
        Troubled at first, he almost cut the arm off, but hesitated. After all, people did worship him and his grip on the worldly matters. And so he gave his arm free reign, with one rule only - the arm must always return back, no matter how far it travels and how badly it gets hurt.
        When Pantokrator disappeared, The Hand was away, and upon returning found the throne empty. 
        Physically imposing and able to travel vast distances in a blink of an eye, The Hand always returns back home due to ancient decree."
        #size 6
        #hp 50
        #teleport
        #homesick 40
        #immortal
        #reformtime -3
        #chaospower -2
        #mr 20
        #prot 6
        #mor 30
        #str 28
        #att 13
        #def 8
        #prec 12
        #enc 0
        #ap 10
        #spiritsight
        #weapon "Giant fist"
        #magicskill 4 1
        #magicskill 3 1
        #magicskill 1 1
        #pathcost 80
        #startdom 4
        #eyes 0
        #blind
        #homerealm 10
        #itemslots 61442
        #float
        #gcost 200
        #heal
        #end

        #newmonster 8211
        #name "God hand"
        #miscshape
        #spr1 "./bozmod/Monsters/Warpman/SkellyHand1.tga"
        #spr2 "./bozmod/Monsters/Warpman/SkellyHand3.tga"
        #descr "For ages have the faithful believed that Hand of God has shaped the world, appearing wherever it is necessary and blessing the world with its touch. So great was their belief in The Hand, that Pantokrators own arm achieved sentience. 
        Troubled at first, he almost cut the arm off, but hesitated. After all, people did worship him and his grip on the worldly matters. And so he gave his arm free reign, with one rule only - the arm must always return back, no matter how far it travels and how badly it gets hurt.
        When Pantokrator disappeared, The Hand was away, and upon returning found the throne empty. 
        Physically imposing and able to travel vast distances in a blink of an eye, The Hand always returns back home due to ancient decree."
        #size 6
        #hp 25
        #teleport
        #homesick 40
        #immortal
        #reformtime -3
        #deathpower 2
        #mr 19
        #prot 8
        #mor 30
        #str 20
        #att 13
        #def 8
        #prec 12
        #enc 0
        #ap 10
        #spiritsight
        #weapon "Touch of doom"
        #magicskill 4 1
        #magicskill 5 1
        #magicskill 7 1
        #pathcost 80
        #startdom 3
        #eyes 0
        #blind
        #startage 3000
        #maxage 15000
        #itemslots 61442
        #float
        #undead
        #coldres 15
        #poisonres 35
        #shockres 10
        #invulnerable 20 -- like liches
        #pierceres
        #gcost 180
        #heal
        #pooramphibian -- thero can now use it
        #end
    -- Mushushu
        #selectmonster 4954
        #woundfend 10
        #batstartsum1d3 2962 -- mushushu bros to hang out with
        #mr 20 -- up to dragon levels
        #tmpnaturegems 1
        #end
    -- Habinger (Krieg)
        -- Insurrection spell
            #newspell 
            #copyspell 789 -- corpse candle because I'm lazy ass to figure out spawn at the edge of battlefield specs
            #name "Insurrection"
            #descr "Cultists are known to ambush enemy armies, appearing at the edges of the battlefields, distracting and taking out enemies with precise shots of concealed crossbows before attacking the confused defenders to cut their throats. The cults stir when the Harbinger is leading the battle and will come in vast numbers when his banner is flown."
            #school -1
            #researchlevel 0
            #path 0 8
            #pathlevel 0 2
            #path 1 7
            #pathlevel 1 1
            #fatiguecost 99
            #casttime 300
            #nreff 1020
            #damage -1665
            #end
        -- gigabanner
            #selectitem 689                    
            #clear
            #name "Sigil of Heresy"
            #spr "./Bozmod/Krieg/Sigil.tga"
            #descr "Banner given to the Harbinger as sign of acceptance by the infernal lords. It holds signatures of a dozen and one devil and can be used to summon infernal denizens into reality."
            #constlevel 12
            #mainpath 7
            #mainlevel 3
            #restricted 172
            #type 8
            #cursed
            #autospell "Insurrection"
            #makemonsters5 -1666
            #nofind
            #itemdrawsize -50
            #end                    
        
                   
        #newmonster 7161
        #name "Harbinger"
        #spr1 "./Bozmod/Krieg/Harbinger1.tga"
        #spr2 "./Bozmod/Krieg/Harbinger2.tga"
        #hp 12
        #size 3
        #prot 0
        #mr 18
        #mor 15
        #str 11
        #att 11
        #def 11
        #enc 3
        #prec 12
        #ap 10
        #mapmove 12
        #gcost 166
        #magicskill 7 1
        #magicskill 0 1
        #fixedname "Lucian"
        #descr "The first arch heretic to ever pledge allegiance to the infernal realms and the one responsible for the First Hellgate inscription, promised power beyond reason.
        The Harbinger of Hell leads vast cults whenever he goes and is a diabolist of great skill. He will claim the mantle of godhood as his own, for dominion over throne realm is but a stepping stone in his grandiose plans.
        Being the undisputed master of forcing gates open, chances of getting gates in any of your forts upgraded are doubled."
        #superiorleader
        #goodundeadleader
        #poormagicleader
        #weapon "Quarterstaff"
        #armor "Robes"
        #pathcost 20
        #startdom 1
        #domsummon -1665
        #makemonsters4 -1666
        #domsummon2 -1669
        #summon3 -1665
        #batstartsum5 -1665
        #batstartsum1d6 -1666
        #woundfend 1
        #diseaseres 100
        #heal
        #maxage 500
        #startage 103
        #notdomshape 7162
        #stealthy
        #startitem 689
        #mountedhumanoid
        #mounted
        #end

        #newmonster 7162
        #name "Harbinger"
        #spr1 "./Bozmod/Krieg/Harbinger1.tga"
        #spr2 "./Bozmod/Krieg/Harbinger2.tga"
        #hp 12
        #size 3
        #prot 0
        #mr 18
        #mor 15
        #str 11
        #att 11
        #def 11
        #enc 3
        #prec 12
        #ap 10
        #mapmove 12
        #descr "The first arch heretic to ever pledge allegiance to the infernal realms and the one responsible for the First Hellgate inscription, promised power beyond reason.
        The Harbinger of Hell leads vast cults whenever he goes and is a diabolist of great skill. He will claim the mantle of godhood as his own, for dominion over throne realm is but a stepping stone in his grandiose plans.
        When travelling outside his domain he will insite local cults, eroding faith in any other pretender and spread his word."
        #superiorleader
        #goodundeadleader
        #poormagicleader
        #weapon "Quarterstaff"
        #armor "Robes"
        #summon4 -1665
        #batstartsum2d6 -1665
        #batstartsum1d6 -1666
        #woundfend 1
        #diseaseres 100
        #heal
        #maxage 500
        #startage 103
        #domshape 7161
        #stealthy
        #heretic 10
        #spreaddom 1
        #mountedhumanoid
        #mounted
        #end
    -- Tortuga pretenders 
        -- The Nightmare of the Deep Fathom --------------------------------------------------------------------------------------------
            #newmonster 7608
            #name "Nightmare of the Deep Fathom"
            #spr1 "./bozmod/Confluence/LA_Tortuga/TortugaGod1.tga" 
            #spr2 "./bozmod/Confluence/LA_Tortuga/TortugaGod2.tga"
            #descr "The Nightmare of the Deep Fathom is a tale from an age long forgotten, used to frighten the mischievous children of Tortuga. The stories say that his greed and will to set the world aflame was so strong that not even death could hope to stop him. Both afraid and fascinated by the Nightmare of the Deep Fathom, pirates and sailors across the Seven Seas began to secretly worship him as though he was a god, either hoping to be spared his wrath, or to join in his misdeeds, inspired by such a terrifying captain. In but a few moons the Seven Seas were plunged into chaos and the world was fraught with terror. Furious at this impudence, the Pantokrator banished the Night of the Deep Fathom to the deepest and darkest recesses of the seas for an eternity. Now that the Pantokrator is gone, the Nightmare of the Deep Fathom has emerged from the depths to yet again pillage the world and receive the prayers of men. Terror and misfortune will spread in any province he is located in."
            #gcost 250
            #pathcost 40
            #startdom 2
            #hp 30
            #size 2
            #prot 8
            #mr 18
            #mor 30
            #str 13
            #att 14
            #def 13
            #prec 12
            #enc 0
            #mapmove 14
            #ap 12
            #weapon 1936 -- Flaming Cutlass
            #weapon 1934 -- Bomb
            #weapon 1935 -- Touch
            #weapon 1935 -- Touch
            #weapon 1935 -- Touch
            #armor "Ring Mail Cuirass"
            #armor "Leather Cap"
            #undead
            #spiritsight
            #amphibian
            #sailing 999 6 
            #startage 1770
            #maxage 5000
            #poisonres 25
            #coldres 15
            #pierceres
            #fear 5
            #deathfire 15
            #fireres 5
            #raiseonkill 100
            #neednoteat
            #incunrest 100
            #shatteredsoul 10
            #expertleader
            #magicskill 5 1 -- D
            #magicskill 7 1 -- B
            #magicskill 0 1 -- F
            #chaospower 1
            #immortal
            #incscale 4 
            #curseluckshield 1
            #montag 2005
            #end

        -- Queen of the Sea -----------------------------------------------------------------------

                            #newmonster 7609
                            #name "Queen of the Sea"
                            #spr1 "./bozmod/Confluence/LA_Tortuga/Pretender1.tga"
                            #spr2 "./bozmod/Confluence/LA_Tortuga/Pretender2.tga"
                            #descr "The Queen of the Sea is an ancient sorceress of tremendous power said to be as old as the first seas. In her domain, the sea, the sorceress will take the shape of a monstrous sea serpent, and grow in size each passing month until she reaches her true and terrifying form, that of an enormous nine headed monster of legends, the Scylla. The physical body of Scylla is not well suited for spell casting. Her magical mastery of all other magic paths will diminish, except for mastery over her own realm, which will increase."
                            #pathcost 10
                            #startdom 1
                            #ap 12
                            #mapmove 14
                            #hp 9
                            #def 12
                            #att 12
                            #str 9
                            #enc 3
                            #amphibian
                            #size 2
                            #prot 0
                            #weapon "Quarterstaff"
                            #gcost 120  
                            #mr 18
                            #prec 12
                            #mor 30
                            #female
                            #goodleader
                            #startage 3000
                            #maxage 10000
                            #magicskill 2 1
                            #watershape 7610
                            #giftofwater 75
                            #end
                            --
                                #newmonster 7610
                                #spr1 "./bozmod/Confluence/LA_Tortuga/Scylla1.tga"
                                #spr2 "./bozmod/Confluence/LA_Tortuga/Scylla1.tga"
                                #name "Queen of the Sea"
                                #descr "The Queen of the Sea is an ancient sorceress of tremendous power said to be as old as the first seas. In her domain, the sea, the sorceress will take the shape of a monstrous sea serpent, and grow in size each passing month until she reaches her true and terrifying form, that of an enormous nine headed monster of legends, the Scylla. The physical body of Scylla is not well suited for spell casting. Her magical mastery of all other magic paths will diminish, except for mastery over her own realm, which will increase."
                                #hp 18
                                #def 7
                                #att 7
                                #str 9
                                #size 1
                                #prot 6
                                #mr 12
                                #mor 30
                                #weapon "Great Head"
                                #eyes 2
                                #amphibian
                                #ap 12
                                #mapmove 12
                                #enc 3
                                #gcost 120  
                                #poisonres 100
                                #fear 5
                                #itemslots 28672
                                #magicboost 2 2
                                #magicboost 0 -2
                                #magicboost 1 -2
                                #magicboost 3 -2
                                #magicboost 4 -2
                                #magicboost 5 -2
                                #magicboost 6 -2
                                #magicboost 7 -2
                                #regeneration 30
                                #landshape 7609
                                #startage 3000
                                #maxage 10000
                                #firstshape 7611
                                #heal
                                #giftofwater 75
                                #end
                            --
                                #newmonster 7611
                                #spr1 "./bozmod/Confluence/LA_Tortuga/Scylla1.tga"
                                #spr2 "./bozmod/Confluence/LA_Tortuga/Scylla1.tga"
                                #name "Queen of the Sea"
                                #descr "The Queen of the Sea is an ancient sorceress of tremendous power said to be as old as the first seas. In her domain, the sea, the sorceress will take the shape of a monstrous sea serpent, and grow in size each passing month until she reaches her true and terrifying form, that of an enormous nine headed monster of legends, the Scylla. The physical body of Scylla is not well suited for spell casting. Her magical mastery of all other magic paths will diminish, except for mastery over her own realm, which will increase."
                                #hp 18
                                #def 7
                                #att 7
                                #str 9
                                #size 1
                                #prot 6
                                #mr 12
                                #mor 30
                                #weapon "Great Head"
                                #eyes 2
                                #amphibian
                                #ap 12
                                #mapmove 12
                                #enc 3
                                #gcost 120  
                                #poisonres 100
                                #fear 5
                                #itemslots 28672
                                #magicboost 2 2
                                #magicboost 0 -2
                                #magicboost 1 -2
                                #magicboost 3 -2
                                #magicboost 4 -2
                                #magicboost 5 -2
                                #magicboost 6 -2
                                #magicboost 7 -2
                                #regeneration 30
                                #landshape 7609
                                #startage 3000
                                #maxage 10000
                                #firstshape 7612
                                #heal
                                #giftofwater 75
                                #end
                            --
                                #newmonster 7612
                                #spr1 "./bozmod/Confluence/LA_Tortuga/Scylla2.tga"
                                #spr2 "./bozmod/Confluence/LA_Tortuga/Scylla2.tga"
                                #name "Queen of the Sea"
                                #descr "The Queen of the Sea is an ancient sorceress of tremendous power said to be as old as the first seas. In her domain, the sea, the sorceress will take the shape of a monstrous sea serpent, and grow in size each passing month until she reaches her true and terrifying form, that of an enormous nine headed monster of legends, the Scylla. The physical body of Scylla is not well suited for spell casting. Her magical mastery of all other magic paths will diminish, except for mastery over her own realm, which will increase."
                                #hp 27
                                #def 8
                                #att 8
                                #str 10
                                #size 2
                                #prot 7
                                #mr 14
                                #mor 30
                                #weapon "Great Head"
                                #weapon "Lesser Heads"
                                #eyes 6
                                #amphibian
                                #ap 12
                                #mapmove 12
                                #enc 3
                                #gcost 120  
                                #poisonres 100
                                #fear 5
                                #itemslots 28672
                                #magicboost 0 -2
                                #magicboost 2 2
                                #magicboost 1 -2
                                #magicboost 3 -2
                                #magicboost 4 -2
                                #magicboost 5 -2
                                #magicboost 6 -2
                                #magicboost 7 -2
                                #regeneration 30
                                #landshape 7609
                                #startage 3000
                                #maxage 10000
                                #firstshape 7613
                                #heal
                                #giftofwater 75
                                #end
                            --
                                #newmonster 7613
                                #spr1 "./bozmod/Confluence/LA_Tortuga/Scylla3.tga"
                                #spr2 "./bozmod/Confluence/LA_Tortuga/Scylla3.tga"
                                #name "Queen of the Sea"
                                #descr "The Queen of the Sea is an ancient sorceress of tremendous power said to be as old as the first seas. In her domain, the sea, the sorceress will take the shape of a monstrous sea serpent, and grow in size each passing month until she reaches her true and terrifying form, that of an enormous nine headed monster of legends, the Scylla. The physical body of Scylla is not well suited for spell casting. Her magical mastery of all other magic paths will diminish, except for mastery over her own realm, which will increase."
                                #hp 36
                                #def 9
                                #att 9
                                #str 12
                                #size 3
                                #prot 8
                                #mr 15
                                #mor 30
                                #weapon "Great Head"
                                #weapon "Lesser Heads"
                                #weapon "Lesser Heads"
                                #eyes 10
                                #amphibian
                                #ap 12
                                #mapmove 12
                                #enc 3
                                #gcost 120  
                                #poisonres 100
                                #fear 5
                                #itemslots 28672
                                #magicboost 0 -2
                                #magicboost 2 2
                                #magicboost 1 -2
                                #magicboost 3 -2
                                #magicboost 4 -2
                                #magicboost 5 -2
                                #magicboost 6 -2
                                #magicboost 7 -2
                                #regeneration 30
                                #landshape 7609
                                #startage 3000
                                #maxage 10000
                                #firstshape 7614
                                #heal
                                #giftofwater 75
                                #end
                            --
                                #newmonster 7614
                                #spr1 "./bozmod/Confluence/LA_Tortuga/Scylla4.tga"
                                #spr2 "./bozmod/Confluence/LA_Tortuga/Scylla4.tga"
                                #name "Queen of the Sea"
                                #descr "The Queen of the Sea is an ancient sorceress of tremendous power said to be as old as the first seas. In her domain, the sea, the sorceress will take the shape of a monstrous sea serpent, and grow in size each passing month until she reaches her true and terrifying form, that of an enormous nine headed monster of legends, the Scylla. The physical body of Scylla is not well suited for spell casting. Her magical mastery of all other magic paths will diminish, except for mastery over her own realm, which will increase."
                                #hp 45
                                #def 10
                                #att 10
                                #str 14
                                #size 4
                                #prot 9
                                #mr 16
                                #mor 30
                                #weapon "Great Head"
                                #weapon "Lesser Heads"
                                #weapon "Lesser Heads"
                                #weapon "Lesser Heads"
                                #eyes 14
                                #amphibian
                                #ap 12
                                #mapmove 12
                                #enc 3
                                #gcost 120  
                                #poisonres 100
                                #fear 5
                                #itemslots 28672
                                #magicboost 0 -2
                                #magicboost 2 2
                                #magicboost 1 -2
                                #magicboost 3 -2
                                #magicboost 4 -2
                                #magicboost 5 -2
                                #magicboost 6 -2
                                #magicboost 7 -2
                                #regeneration 30
                                #landshape 7609
                                #startage 3000
                                #maxage 10000
                                #firstshape 7615
                                #heal
                                #giftofwater 75
                                #end
                            --
                                #newmonster 7615
                                #spr1 "./bozmod/Confluence/LA_Tortuga/Scylla5.tga"
                                #spr2 "./bozmod/Confluence/LA_Tortuga/Scylla5.tga"
                                #name "Queen of the Sea"
                                #descr "The Queen of the Sea is an ancient sorceress of tremendous power said to be as old as the first seas. In her domain, the sea, the sorceress will take the shape of a monstrous sea serpent, and grow in size each passing month until she reaches her true and terrifying form, that of an enormous nine headed monster of legends, the Scylla. The physical body of Scylla is not well suited for spell casting. Her magical mastery of all other magic paths will diminish, except for mastery over her own realm, which will increase."
                                #hp 54
                                #def 12
                                #att 12
                                #str 16
                                #size 5
                                #prot 10
                                #mr 17
                                #mor 30
                                #weapon "Great Head"
                                #weapon "Lesser Heads"
                                #weapon "Lesser Heads"
                                #weapon "Lesser Heads"
                                #weapon "Lesser Heads"
                                #eyes 18
                                #amphibian
                                #ap 12
                                #mapmove 12
                                #enc 3
                                #gcost 120  
                                #poisonres 100
                                #fear 5
                                #itemslots 28672
                                #magicboost 0 -2
                                #magicboost 2 2
                                #magicboost 1 -2
                                #magicboost 3 -2
                                #magicboost 4 -2
                                #magicboost 5 -2
                                #magicboost 6 -2
                                #magicboost 7 -2
                                #regeneration 30
                                #landshape 7609
                                #startage 3000
                                #maxage 10000
                                #firstshape 7616
                                #heal
                                #giftofwater 75
                                #end
                            --
                            #newmonster 7616
                            #spr1 "./bozmod/Confluence/LA_Tortuga/Scylla6.tga"
                            #spr2 "./bozmod/Confluence/LA_Tortuga/Scylla6.tga"
                            #name "Queen of the Sea"
                            #descr "The Queen of the Sea is an ancient sorceress of tremendous power said to be as old as the first seas. In her domain, the sea, the sorceress will take the shape of a monstrous sea serpent, and grow in size each passing month until she reaches her true and terrifying form, that of an enormous nine headed monster of legends, the Scylla. The physical body of Scylla is not well suited for spell casting. Her magical mastery of all other magic paths will diminish, except for mastery over her own realm, which will increase."
                            #hp 63
                            #def 14
                            #att 14
                            #str 18
                            #size 6
                            #prot 10
                            #mr 18
                            #mor 30
                            #weapon "Great Head"
                            #weapon "Lesser Heads"
                            #weapon "Lesser Heads"
                            #weapon "Lesser Heads"
                            #weapon "Lesser Heads"
                            #eyes 18
                            #amphibian
                            #ap 12
                            #mapmove 12
                            #enc 3
                            #gcost 120  
                            #poisonres 100
                            #fear 5
                            #itemslots 28672
                            #magicboost 0 -1
                            #magicboost 2 2
                            #magicboost 1 -1
                            #magicboost 3 -1
                            #magicboost 4 -1
                            #magicboost 5 -1
                            #magicboost 6 -1
                            #magicboost 7 -1
                            #regeneration 30
                            #landshape 7609
                            #startage 3000
                            #maxage 10000
                            #heal
                            #giftofwater 75
                            #end

        -- Water Idol -------------------------------------------------------------------------------------------------------
                            #newmonster 7617
                            #spr1 "./bozmod/Confluence/LA_Tortuga/Water Idol1.tga"
                            #spr2 "./bozmod/Confluence/LA_Tortuga/Water Idol2.tga"
                            #copystats 158
                            #clearmagic
                            #name "Water Idol"
                            #descr "The Water Idol is a powerful spirit which inhabits a fountain. The spirit manipulates the fountain's water to form a physical body, a manifestation of the wild magic of Water, resembling a huge, crowned female being. The body of the spirit is composed of water and is such very difficult to harm. The spirit is very quick to reform its watery body and will quite literally wash away any seeming injury made to its assumed form. Though completly unable to leave the fountain it inhabits, the spirit can still posses other living beings in its proximity in order to make its will heard and to perform tasks such as forging items or performing enchantments. The spirit is tremendously strong in its dominion and it is also very magically powerful. Even though the spirit cannot strike back, it would be very difficult to destroy it."
                            #magicskill 2 3
                            #gcost 160 
                            #hp 25
                            #ap 2
                            #mapmove 0
                            #prot 5
                            #regeneration 100
                            #size 6
                            #bluntres
                            #slashres
                            #pierceres
                            #blind
                            #poisonres 25
                            #prec 10
                            #mor 30
                            #mr 18
                            #str 15
                            #att 5
                            #def 0
                            #neednoteat
                            #amphibian
                            #pathcost 40
                            #enc 0
                            #startdom 4
                            #blind
                            #startage 700
                            #maxage 10000
                            #itemslots 12288
                            #superiorleader
                            #okmagicleader
                            #magicskill 2 3
                            #homerealm 0
                            #end

-- Adding Pretenders
    #selectnation 171
    #addgod "Sanguine Dragon"
    #addgod "Shadow Dragon"
    #addgod "Soulless Dragon"
    #addgod "The Singularity"
    #addgod "Mutant Dragon"
    #end
    
    #selectnation 169
    #addgod 109
    #addgod 120
    #addgod 246
    #addgod 249
    #addgod 486
    #addgod 550
    #addgod 607
    #addgod 652
    #addgod 656
    #addgod 657
    #addgod 873
    #addgod 2980
    #addgod 3396
    #addgod 8212
    #end

    #selectnation 5	--Arcoscephale	Golden Era
    #addgod "Fairy Dragon"
    #addgod "Crystal Dragon"
    #end
    
    #selectnation 6	--Ermor	New Faith
    #addgod "Fairy Dragon"
    #addgod "Crystal Dragon"
    #cheapgod40 7035
    #cheapgod20 7035
    #end
    
    #selectnation 7	--Ulm	Enigma of Steel
    #addgod "Crystal Dragon"
    #cheapgod40 5062 -- gigaanvil
    #end
    
    #selectnation 8	--Marverni	Time of Druids
    #addgod "Fairy Dragon"
    #addgod "Crystal Dragon"
    #addgod "Mutant Dragon"
    #end
    
    #selectnation 9	--Sauromatia	Amazon Queens
    #addgod "Fairy Dragon"
    #addgod "The Singularity"
    #addgod "Dracolich"
    #addgod "Soulless Dragon"
    #addgod "Mutant Dragon"
    #addgod "Sanguine Dragon"
    #addgod 8211 -- skellyhand
    #end
    
    #selectnation 10	--T’ien Ch’i	Spring and Autumn
    #addgod "Crystal Dragon"
    #end
    
    #selectnation 11	--Machaka	Lion Kings
    #addgod "Fairy Dragon"
    #addgod "Crystal Dragon"
    #addgod "Mutant Dragon"
    #addgod "King of Beasts"
    #end
    
    #selectnation 12	--Mictlan	Reign of Blood
    #addgod "The Singularity"
    #addgod "Mutant Dragon"
    #addgod "Sanguine Dragon"
    #end
    
    #selectnation 13	--Abysia	Children of Flame
    #addgod "The Singularity"
    #addgod "Mutant Dragon"       
    #addgod "King of Beasts"
    #addgod 8212 -- banedragon
    #end
    
    #selectnation 14	--Caelum	Eagle Kings
    #addgod "Fairy Dragon"
    #addgod 8210
    #cheapgod20 8210 -- godhand
    #end
    
    #selectnation 15	--C’tis	Lizard Kings
    #addgod "Crystal Dragon"
    #addgod "Fairy Dragon"
    #addgod "The Singularity"
    #addgod "Dracolich"
    #addgod "Shadow Dragon"
    #addgod "Soulless Dragon"
    #addgod "Mutant Dragon"        
    #addgod "King of Beasts"
    #addgod 8211 -- skellyhand
    #addgod 8212 -- banedragon
    #end
    
    #selectnation 16	--Pangaea	Age of Revelry
    #addgod "Fairy Dragon"
    #addgod "Mutant Dragon"
    #addgod "Sanguine Dragon"
    #end
    
    #selectnation 17	--Agartha	Pale Ones
    #addgod "Crystal Dragon"
    #addgod "Dracolich"        
    #addgod "King of Beasts"
    #addgod 8212 -- banedragon
    #end
    
    #selectnation 18	--Tir na n’Og	Land of the Ever Young
    #addgod "Fairy Dragon"
    #cheapgod20 "Fairy Dragon"
    #addgod "Crystal Dragon"
    #end
    
    #selectnation 19	--Fomoria	The Cursed Ones
    #addgod "The Singularity"
    #addgod "Dracolich"
    #end
    
    #selectnation 20	--Vanheim	Age of Vanir
    #addgod "Fairy Dragon"
    #addgod "Crystal Dragon"
    #addgod "Sanguine Dragon"
    #end
    
    #selectnation 21	--Helheim	Dusk and Death
    #addgod "Fairy Dragon"
    #addgod "Crystal Dragon"
    #addgod "The Singularity"
    #addgod "Dracolich"
    #addgod 8212 -- banedragon
    #addgod "Slayer of apostles"
    #addgod 8211 -- skellyhand
    #delgod 8210 -- no good hand
    #delgod 7012 -- no knights
    #delgod 7019 -- no holy dragon
    
    #end
    
    #selectnation 22	--Niefelheim	Sons of Winter
    #addgod "Crystal Dragon"
    #addgod "The Singularity"
    #addgod "Mutant Dragon"
    #addgod "Sanguine Dragon"
    #end
    
    #selectnation 24	--Rus	Sons of Heaven
    #addgod "Fairy Dragon"
    #addgod "Dracolich"
    #end
    
    #selectnation 25	--Kailasa	Rise of the Ape Kings
    #addgod "Fairy Dragon"
    #addgod "Crystal Dragon"
    #end
    
    #selectnation 26	--Lanka	Land of Demons
    #addgod "The Singularity"
    #addgod "Dracolich"
    #addgod 8212 -- banedragon
    #addgod "Mutant Dragon"
    #addgod "Sanguine Dragon"
    #end
    
    #selectnation 27	--Yomi	Oni Kings
    #addgod "The Singularity"
    #addgod "Dracolich"
    #addgod 8212 -- banedragon
    #addgod "Shadow Dragon"
    #addgod "Soulless Dragon"
    #addgod "Sanguine Dragon"
    #addgod "Slayer of apostles"
    #addgod 8211 -- skellyhand
    #delgod 8210 -- no good hand
    #end
    
    #selectnation 28	--Hinnom	Sons of the Fallen
    #addgod "The Singularity"
    #addgod "Dracolich"
    #addgod "Sanguine Dragon"
    #end
    
    #selectnation 29	--Ur	The First City
    #addgod "Crystal Dragon"
    #addgod "Fairy Dragon"
    #end
    
    #selectnation 30	--Berytos	Phoenix Empire
    #addgod "Crystal Dragon"
    #addgod "The Singularity"
    #addgod "Dracolich"
    #addgod "Sanguine Dragon"
    #end
    
    #selectnation 31	--Xibalba	Vigil of the Sun
    #addgod "The Singularity"
    #addgod "Dracolich"
    #addgod 8212 -- banedragon
    #addgod "Mutant Dragon"
    #addgod "Sanguine Dragon"
    #end
    
    #selectnation 32	--Mekone	Brazen Giants
    #addgod "Crystal Dragon"
    #end
    
    #selectnation 33	--Ubar	Kingdom of the Unseen
    #addgod 8212 -- banedragon
    #end
    
    #selectnation 36	--Atlantis	Emergence of the Deep Ones
    #end
    

    
    #selectnation 38	--Pelagia	Pearl Kings
    #addgod 7011
    #end
    
    #selectnation 39	--Oceania	Coming of the Capricorns
    #end
    
    #selectnation 40	--Therodos	Telkhine Spectre
    #addgod "The Singularity"
    #addgod "Dracolich"
    #addgod "Shadow Dragon"
    #addgod "Soulless Dragon"        
    #addgod "King of Beasts"
    #addgod 8211 -- skellyhand
    #delgod 8210 -- no good hand
    #delgod 7012 -- no knights
    #delgod 7019 -- no holy dragon
    #delgod 5095 -- no justice!
    #end
    
    #selectnation 43	--Arcoscephale	The Old Kingdom
    #addgod "Fairy Dragon"
    #addgod "Crystal Dragon"
    #end
    
    #selectnation 44	--Ermor	Ashen Empire
    #addgod "The Singularity"
    #addgod "Dracolich"
    #addgod 8212 -- banedragon
    #addgod "Slayer of apostles"
    #cheapgod20 "Slayer of apostles"
    #addgod "Soulless Dragon"
    #addgod 7035
    #cheapgod20 7035
    #deathblessbonus 1
    #addgod 8211 -- skellyhand
    #delgod 8210 -- no good hand
    #delgod 7012 -- no knights
    #delgod 7019 -- no holy dragon
    #delgod 5095 -- no justice!
    #delgod 3208 -- no spring!
    #delgod 3209 -- no growth!
    #cheapgod40 5071 -- throne king
    #end
    
    #selectnation 45	--Sceleria	Reformed Empire
    #addgod "The Singularity"
    #addgod "Dracolich"
    #addgod 8212 -- banedragon
    #addgod "Shadow Dragon"
    #addgod "Slayer of apostles"
    #addgod "Soulless Dragon"
    #addgod 8211 -- skellyhand
    #cheapgod20 5071 -- throne king
    #end
    
    #selectnation 46	--Pythium	Emerald Empire
    #addgod "Crystal Dragon"
    #addgod "Fairy Dragon"
    #end
    
    #selectnation 47	--Man	Tower of Avalon
    #addgod "Crystal Dragon"
    #addgod "Fairy Dragon"
    #addgod 7012 -- knights
    #cheapgod40 7012 -- knights
    #end
    
    #selectnation 48	--Eriu	Last of the Tuatha
    #addgod "Crystal Dragon"
    #addgod "Fairy Dragon"
    #cheapgod20 "Fairy Dragon"
    #end
    
    #selectnation 49	--Ulm	Forges of Ulm
    #addgod "Crystal Dragon"
    #end
    
    #selectnation 50	--Marignon	Fiery Justice
    #addgod "Crystal Dragon"
    #cheapgod20 "Dragon of Blinding Virtue"
    #end
    
    #selectnation 51	--Mictlan	Reign of the Lawgiver
    #addgod "Crystal Dragon"
    #addgod "The Singularity"
    #addgod "Mutant Dragon"
    #addgod "Sanguine Dragon"
    #end
    
    #selectnation 52	--T’ien Ch’i	Imperial Bureaucracy
    #addgod "Crystal Dragon"
    #end
    
    #selectnation 53	--Machaka	Reign of Sorcerors
    #addgod "Crystal Dragon"
    #addgod "Dracolich"        
    #addgod "King of Beasts"
    #end
    
    #selectnation 54	--Agartha	Golem Cult
    #addgod "Crystal Dragon"
    #cheapgod40 "Crystal Dragon"
    #end
    
    #selectnation 55	--Abysia	Blood and Fire
    #addgod "The Singularity"
    #addgod "Dracolich"
    #addgod "Mutant Dragon"
    #cheapgod20 "Mutant Dragon"
    #addgod 8212 -- banedragon
    #addgod "Sanguine Dragon"        
    #addgod "King of Beasts"
    #end
    
    #selectnation 56	--Caelum	Reign of the Seraphim
    #addgod "Crystal Dragon"
    #addgod "Fairy Dragon"
    #end
    
    #selectnation 57	--C’tis	Miasma
    #addgod "The Singularity"
    #addgod "Dracolich"
    #addgod 8212 -- banedragon
    #addgod "Shadow Dragon"
    #addgod "Soulless Dragon"
    #addgod "Mutant Dragon"
    #addgod 8211 -- skellyhand
    #delgod 8210 -- no good hand
    #delgod 7012 -- no knights
    #delgod 7019 -- no holy dragon
    #delgod 5095 -- no justice!
    #end
    
    #selectnation 58	--Pangaea	Age of Bronze
    #addgod "Fairy Dragon"
    #addgod "Mutant Dragon"
    #end
    
    #selectnation 59	--Asphodel	Carrion Woods
    #addgod "Fairy Dragon"
    #addgod "The Singularity"
    #addgod "Dracolich"
    #addgod "Soulless Dragon"
    #addgod 8211 -- skellyhand
    #delgod 8210 -- no good hand
    #delgod 7012 -- no knights
    #delgod 7019 -- no holy dragon
    #end
    
    #selectnation 60	--Vanheim	Arrival of Man
    #addgod "Crystal Dragon"
    #addgod "Fairy Dragon"
    #addgod 7144
    #end
    
    #selectnation 61	--Jotunheim	Iron Woods
    #addgod "The Singularity"
    #addgod "Dracolich"
    #addgod "Mutant Dragon"
    #addgod "Sanguine Dragon"
    #end
    
    #selectnation 62	--Vanarus	Land of the Chuds
    #addgod "Crystal Dragon"
    #addgod "Fairy Dragon"
    #addgod 7144
    #end
    
    #selectnation 63	--Bandar Log	Land of the Apes
    #addgod "Crystal Dragon"
    #addgod "Fairy Dragon"
    #cheapgod40 7130
    #end
    
    #selectnation 64	--Shinuyama	Land of the Bakemono
    #addgod "The Singularity"
    #cheapgod20 7142 -- zilla wakes up
    #addgod 8211 -- skellyhand
    #end
    
    #selectnation 65	--Ashdod	Reign of the Anakim
    #addgod "Crystal Dragon"
    #end
    
    #selectnation 66	--Uruk	City States
    #addgod "Crystal Dragon"
    #addgod "Fairy Dragon"
    #end
    
    #selectnation 67	--Nazca	Kingdom of the Sun
    #addgod "Crystal Dragon"
    #addgod "The Singularity"
    #addgod "Dracolich"
    #addgod "Shadow Dragon"
    #end
    
    #selectnation 68	--Xibalba	Flooded Caves
    #addgod "The Singularity"
    #addgod "Sanguine Dragon"
    #end
    
    #selectnation 69	--Phlegra	Deformed Giants
    #addgod "Crystal Dragon"
    #end
    
    #selectnation 70	--Phaeacia	Isle of the Dark Ships
    #addgod "Crystal Dragon"
    #end
    
    #selectnation 71	--Ind Magnificent Kingdom of Exalted Virtue
    #addgod "Crystal Dragon"
    -- addgod "Fairy Dragon"
    #addgod "Dragon of Blinding Virtue"
    #cheapgod20 "Dragon of Blinding Virtue"
    #cheapgod40 4952 -- gigajug!
    #cheapgod20 8210 -- god hand
    #end
    
    #selectnation 72	--Na’Ba	Queens of the Desert
    #addgod "Crystal Dragon"
    #addgod 8212 -- banedragon
    #end
    
    #selectnation 73	--Atlantis	Kings of the Deep
    #end
    
    #selectnation 74	--R’lyeh	Fallen Star
    #addgod 7010 -- psydragon
    #addgod 7011 -- dracobolith
    #cheapgod20 7010 -- psydragon
    #cheapgod40 7011 -- dracobolith
    #end
    
    #selectnation 75	--Pelagia	Triton Kings
    #addgod 7011 -- dracobolith
    #end
    
    #selectnation 76	--Oceania	Mermidons
    #addgod 7011 -- dracobolith
    #end
    
    #selectnation 77	--Ys	Morgen Queens
    #addgod "Fairy Dragon"
    #end
    
    #selectnation 80	--Arcoscephale	Sibylline Guidance
    #addgod "Crystal Dragon"
    #addgod "Fairy Dragon"
    #end
    
    #selectnation 81	--Pythium	Serpent Cult
    #addgod "Crystal Dragon"
    #addgod "Fairy Dragon"
    #end
    
    #selectnation 82	--Lemuria	Soul Gate
    #addgod "The Singularity"
    #addgod "Dracolich"
    #addgod 8212 -- banedragon
    #addgod "Shadow Dragon"
    #chepgod20 "Shadow Dragon"
    #addgod "Soulless Dragon"
    #addgod "Slayer of apostles"
    #cheapgod40 "Slayer of apostles"
    #addgod 8211 -- skellyhand
    #delgod 8210 -- no good hand
    #delgod 7012 -- no knights
    #delgod 7019 -- no holy dragon
    #delgod 5095 -- no justice!
    #delgod 3208 -- no spring!
    #delgod 3209 -- no growth!
    -- no santa claus!
    -- no tooth fairy!
    #end
    
    #selectnation 83	--Man	Towers of Chelms
    #addgod "Crystal Dragon"
    #addgod "Fairy Dragon"
    #end
    
    #selectnation 84	--Ulm	Black Forest
    #addgod "Crystal Dragon"
    #addgod "Dracolich"
    #addgod "The Singularity"
    #addgod 8212 -- banedragon
    #addgod "Sanguine Dragon"
    #cheapgod20 "Sanguine Dragon"
    #addgod "Soulless Dragon"
    #addgod "Slayer of apostles"
    #addgod 8211 -- skellyhand
    #delgod 8210 -- no good hand
    #delgod 7012 -- no knights
    #delgod 7019 -- no holy dragon
    #delgod 5095 -- no justice!
    #end
    
    #selectnation 85	--Marignon	Conquerors of the Sea
    #addgod "Crystal Dragon"
    #addgod "Sanguine Dragon"
    #cheapgod20 "Lady of Whips"
    #end
    
    #selectnation 86	--Mictlan	Blood and Rain
    #addgod "The Singularity"
    #addgod "Sanguine Dragon"
    #end
    
    #selectnation 87	--T’ien Ch’i	Barbarian Kings
    #addgod "Crystal Dragon"
    #addgod 7144
    #end
    
    #selectnation 89	--Jomon	Human Daimyos
    #addgod "Crystal Dragon"
    #cheapgod40 7142 -- zilla homeland
    #end
    
    #selectnation 90	--Agartha	Ktonian Dead
    #addgod "Dracolich"
    #addgod "Crystal Dragon"
    #addgod "Slayer of apostles"
    #addgod 8211 -- skellyhand
    #cheapgod20 5071 -- throne king
    #end
    
    #selectnation 91	--Abysia	Blood of Humans
    #addgod "Crystal Dragon"
    #addgod "The Singularity"
    #cheapgod20 "Sanguine Dragon"        
    #cheapgod40 "Mutant Dragon"     
    #addgod "King of Beasts"
    #end
    
    #selectnation 92	--Caelum	Return of the Raptors
    #addgod 7144
    #addgod 8211 -- skellyhand
    #end
    
    #selectnation 93	--C’tis	Desert Tombs
    #addgod "Dracolich"
    #addgod "The Singularity"
    #addgod "Soulless Dragon"
    #addgod 8212 -- banedragon
    #addgod "Slayer of apostles"
    #addgod 8211 -- skellyhand
    #delgod 8210 -- no good hand
    #delgod 7012 -- no knights
    #delgod 7019 -- no holy dragon
    #delgod 5095 -- no justice!
    #end
    
    #selectnation 94	--Pangaea	New Era
    #addgod "Fairy Dragon"
    #end
    
    #selectnation 95	--Midgård	Age of Men
    #addgod "Crystal Dragon"
    #addgod "Fairy Dragon"
    #end
    
    #selectnation 96	--Utgård	Well of Urd
    #addgod "Crystal Dragon"
    #addgod "Fairy Dragon"
    #end
    
    #selectnation 97	--Bogarus	Age of Heroes
    #addgod "Crystal Dragon"
    #addgod "The Singularity"
    #addgod "Dracolich"
    #addgod "Sanguine Dragon"
    #addgod 7144
    #end
    
    #selectnation 98	--Patala	Reign of the Nagas
    #addgod "Crystal Dragon"
    #addgod "Fairy Dragon"
    #end
    
    #selectnation 99	--Gath	Last of the Giants
    #addgod "Crystal Dragon"
    #end
    
    #selectnation 100	--Ragha	Dual Kingdom
    #addgod "Crystal Dragon"
    #addgod "Sanguine Dragon"
    #addgod 7144
    #end
    
    #selectnation 101	--Xibalba	Return of the Zotz
    #addgod "The Singularity"
    #addgod "Dracolich"
    #addgod "Sanguine Dragon"
    #end
    
    #selectnation 102	--Phlegra	Sleeping Giants
    #addgod "Crystal Dragon"
    #end
    
    #selectnation 106	--Atlantis	Frozen Sea
    #end
    
    #selectnation 107	--R’lyeh	Dreamlands
    #addgod "Psionic Dragon"
    #end
    
    #selectnation 108	--Erytheia	Kingdom of Two Worlds
    #addgod "Crystal Dragon"
    #addgod 7144
    #end

    #selectnation 169	--Diamedulla
    #addgod "The Singularity"
    #addgod "Soulless Dragon"
    #addgod "Sanguine Dragon"
    #addgod 8211 -- skellyhand
    #delgod 8210 -- no good hand
    #delgod 7012 -- no knights
    #delgod 7019 -- no holy dragon
    #delgod 5095 -- no justice!
    #end       
    
    #selectnation 170   --Cangobia Fang and Feather
    #addgod "King of Beasts"    
    #addgod 8212      
    #addgod "Shadow Dragon"
    #addgod "Mutant Dragon"        
    #addgod "Crystal Dragon"
    #addgod "Fairy Dragon"
    #addgod "Sanguine Dragon"
    #addgod "Psionic Dragon"
    #addgod "Dragon of Blinding Virtue"
    #addgod 216 --red drag
    #addgod 265  --b drag
    #addgod 266 --g drag
    #addgod 7144 --cosmic egg
    #addgod "Black Dragon"
    #addgod 4993
    #end

    -- Changing Nations
        -- Add knight god to LA Man
            #selectnation 83
            #addgod 7012
            #cheapgod20 7012
            #end
        -- Add knight god to MA Man
            #selectnation 47
            #addgod 7012
            #cheapgod20 7012
            #end
        -- Monkeys
            -- Lanka
                #selectnation 26
                #cheapgod20 7150
                #addgod 8211
                #end
            -- EA Monkeys
                #selectnation 25
                #cheapgod20 7150
                #end
            -- MA Monkeys
                #selectnation 63
                #cheapgod40 7150
                #end
            -- LA Monkeys
                #selectnation 98
                #cheapgod20 7150
                #end
        -- MA Ermor (7011 montag)
            -- Buffs
                #selectnation 44
                #defdeath 0
                #deathblessbonus 1
                #end
            -- Necrocloak     
                #newitem
                #spr "./bozmod/items/necrocloak.tga"
                #name "Blanket of terrible blight"
                #descr "This cloak is worn typically by undead or those immune to diseases and poisons. The wearer is surrounded by a perpetual aura of death which withers all living things around them."
                #mainpath 5
                #leper 10
                #restricted 44
                #mainlevel 3 
                #incscale 3 -- Death
                #diseasecloud 8
                #poisoncloud 4
                #type 5
                #constlevel 6
                #end
            -- Lictors bugg
                #selectmonster 259
                #clearweapons
                #weapon 554 -- lictor axe
                #end
            -- Montagging evil mages
                -- Necromancer
                    #selectmonster 310
                    #montag 7011
                    #montagweight 3
                    #end
                -- Conjurer
                    #selectmonster 94
                    #montag 7011
                    #montagweight 3
                    #end
                -- Plague Cult Leader
                    #newmonster 7044                    
                    #copyspr 2535
                    #copystats 2535
                    #clearspec 
                    #reaper 1
                    #stealthy 0
                    #montag 7011
                    #montagweight 3
                    #end
                -- Sorcerer
                    #selectmonster 339
                    #montag 7011
                    #montagweight 1
                    #end
                -- Blackrose Sorceress
                    #selectmonster 2362
                    #montag 7011
                    #montagweight 1
                    #end
                -- Circle Master
                    #selectmonster 95
                    #montag 7011
                    #montagweight 1
                    #end
            -- Mage events

                -- domchance 5 to get ermorian cultist temple d3 dom5
                #newevent
                #rarity 5
                #req_pop0ok
                #req_domchance 2
                #req_fornation 44
                #req_dominion 6
                #req_death 3
                #req_temple 1
                #nolog
                #nation -2                
                #com 554
                #msg "An aspiring scholar of mortology as joined your ranks."
                #end

                -- domchance 2 to get spectral mage with lab m3d3
                #newevent
                #rarity 5
                #req_pop0ok
                #req_domchance 2
                #req_fornation 44
                #req_magic 3
                #req_death 3
                #req_lab 1
                #nation -2
                #com 329
                #msg "An aspiring scholar of mortology as joined your ranks."
                #end

                -- domchance 10 to get mage with fort lab m1d3
                #newevent
                #rarity 5
                #req_pop0ok
                #req_domchance 10
                #req_fornation 44
                #req_magic 1
                #req_death 3
                #req_lab 1
                #req_fort 1
                #nolog
                #nation -2
                #com -7011
                #msg "An aspiring scholar of mortology as joined your ranks."
                #end


                -- domchance 2 to get mage m1d3
                #newevent
                #rarity 5
                #req_domchance 2
                #req_pop0ok
                #req_fornation 44
                #req_magic 1
                #req_death 3
                #req_lab 1
                #nolog
                #nation -2
                #com -7011
                #msg "An aspiring scholar of mortology as joined your ranks."
                #end
            -- Recruitable Undead   
                -- Longdeads 2h Sword
                    #newmonster 7031
                    #copyspr 2124
                    #copystats 2124
                    #clearweapons
                    #cleararmor
                    #weapon 11 -- great sword
                    #armor 14 --plate hauberk
                    #armor 126 --legionnary helmet
                    #rpcost 0
                    #end 
                -- Longdeads spear shield
                    #newmonster 7038
                    #copyspr 1657
                    #copystats 1657
                    #clearweapons
                    #cleararmor
                    #weapon 28 -- long spear
                    #armor 126 -- legionary helmet
                    #armor 14 --plate hauberk
                    #armor 4 --tower shield
                    #rpcost 0
                    #end 
                -- Wights
                    #newmonster 7039
                    #copyspr 533
                    #copystats 533
                    #rcost 160
                    #clearweapons
                    #cleararmor
                    #armor 2 --shield
                    #weapon 42 --bane blade 1h
                    #armor 126 --legionnary helmet
                    #armor 14 --plate hauberk
                    #gcost 0
                    #end
                -- Miners
                    #newmonster 7026
                    #name "Reborn Miner"
                    #spr1 "./bozmod/Confluence/LA_Diamedulla/ZombieMiner1.tga"
                    #spr2 "./bozmod/Confluence/LA_Diamedulla/ZombieMiner1.tga"
                    #descr "This is a reanimated miner, an unfortunate soul who has been cast into a corpse to dig the earth in perpetuity, searching for the gleam of gold for their cruel masters."
                    #ap 8
                    #mapmove 18
                    #hp 15
                    #mr 6
                    #size 2
                    #str 12
                    #enc 0
                    #att 8
                    #def 8
                    #prec 10
                    #mor 14
                    #gcost 10
                    #slave
                    #weapon "Pick Axe"
                    #undead
                    #poisonres 25
                    #coldres 15
                    #neednoteat
                    #noheal
                    #gold 1
                    #reclimit 6    
                    #rpcost 6
                    #startage 150
                    #maxage 500
                    #end     
                -- Reborn Smith     
                    #newmonster 7025
                    #name "Reborn Smith"
                    #spr1 "./bozmod/Confluence/LA_Diamedulla/ZombieSmith1.tga"
                    #spr2 "./bozmod/Confluence/LA_Diamedulla/ZombieSmith1.tga"
                    #descr "This is a reanimated smith who has been return and imbued into a body to once again forge equipment as they did in their past life. This smith is likely going to produce equipment for fellow undead."
                    #ap 8
                    #mapmove 18
                    #hp 15
                    #mr 6
                    #size 2
                    #str 12
                    #enc 0
                    #att 8
                    #def 8
                    #prec 10
                    #mor 14
                    #slave
                    #gcost 12
                    #weapon "Hammer"
                    #undead
                    #poisonres 25
                    #coldres 15
                    #neednoteat
                    #noheal
                    #inanimate
                    #pooramphibian
                    #darkvision 100
                    #startage 150
                    #maxage 500
                    #reclimit 6   
                    #resources 2
                    #rpcost 6
                    #end
            -- Nation Changes
                #selectnation 44     
                #addrecunit 7026
                #addrecunit 7031
                #addrecunit 7038
                #addrecunit 7039
                #addrecunit 7025
                #end
        -- EA Catfish
            #selectmonster 1401 -- Polypal Mother
            #gcost 100
            #researchbonus -5
            #magicskill 4 1
            #noreqlab
            #noreqtemple
            #userestricteditem 99
            #montag 5190
            #twiceborn 5779
            #end

            #selectmonster 1402 -- Polypal Queen
            #gcost 200
            #heal
            #diseaseres 100
            #pathboost 53 1
            #userestricteditem 99
            #montag 5190
            #twiceborn 5779
            #end


            #selectmonster 1403 -- Giboleth
            #itemslots 290944 -- crown, 3 misc
            #userestricteditem 99
            #montag 5190    
            #end

                #selectmonster 1691 -- Auluudh
                #userestricteditem 99
                #montag 5190
                #end
            
            #selectmonster 1520 -- Aboleth
            #itemslots 290944 -- crown, 3 misc
            #userestricteditem 99
            #montag 5190
            #end

            #selectmonster 1521 -- Mind Lord
            #itemslots 290944 -- crown, 3 misc
            #userestricteditem 99
            #montag 5190
            #end

            #selectmonster 1522 -- Gibodai
            #itemslots 290944 -- crown, 3 misc
            #userestricteditem 99
            #montag 5190
            #end

            #selectmonster 1523 -- Slave Prince
            #magiccommand 15 -- can now ave aboleth and tramplers
            #end

            #selectmonster 2883 -- Abodai
            #itemslots 290944 -- crown, 3 misc
            #userestricteditem 99
            #montag 5190
            #end

            #selectmonster 3451 -- Necrodai big
            #clearweapons
            #itemslots 290944 -- crown, 3 misc
            #weapon 371 -- moustache aoe lifesteal
            #weapon 269 -- Soul Leech
            #weapon 274 -- Enslave Mind
            #end

            #selectmonster 3452 -- Necrodai small
            #itemslots 290944 -- crown, 3 misc
            #end
        -- MA TC
            #selectmonster 804
            #bringeroffortune 5 -- 2 vanilla
            #end

        -- Aspho
            
        -- ASPHODEL

            #selectspell 341 -- Quick Roots
            #spec 549470352 -- AN, Ignores Shields, Affects Friendly non-lifeless Magic Beings Only, UWOK
            #casttime 25
            #end

            #selectspell 342 -- Regrowth
            #spec 549470352 -- AN, Ignores Shields, Affects Friendly non-lifeless Magic Beings Only, UWOK
            #casttime 33
            #end

            #selectspell 343 -- Mend the Dead
            #name "Mending Vines"
            #descr "An unholy prayer that instantly mends the bones, vines and roots of a Manikin or carrion beast."
            #spec 549470352 -- AN, Ignores Shields, Affects Friendly non-lifeless Magic Beings Only, UWOK
            #casttime 50
            #end

            #selectspell 344 -- Puppet Mastery
            #spec 549474448 -- AN, Ignores Shields, Affects Friendly non-lifeless Magic Beings Only, UWOK, MR-Negates
            #end

            #selectspell 345 -- Carrion Growth
            #spec 566247568 -- AN, Ignores Shields, Affects Friendly non-lifeless Magic Beings Only, UWOK, MR-Negates Easily
            #end

            #selectnation 59 -- Asphodel
            #descr "Asphodel was once part of Pangaea, a woodland nation inhabited by wild beings connected with nature. In ancient times the woods covered much of the world, but that has changed. Man is no longer beast and the world of the wild is disappearing. In response to the destruction of the wild forests a dark and hateful God has arisen. The Panic Apostates of the Carrion Grove have revolted and struck back upon the civilized world. They and their followers have been touched by the Dark God and their hides are colored as black as the mood of the vengeful forest. An enchanted sleeping sickness now spreads from the Carrion Grove, causing civilized men to succumb to eternal dreamless slumber. Vines spread across quiet cities as the forest reclaims the land."
            #summary "Race: Forest beings and vine carrion beings
            Military: Satyr and minotaur infantry, centaur warriors and archers, harpies, hordes of carrion beasts
            Magic: Nature, Death, some Earth and Water
            Priests: Average, Carrion priests can reanimate, Temples generate Nature gems
            Dominion: Causes dreamless slumber, greatly reducing tax income. Vines reanimate corpses as manikins and carrion beasts, more in forests. Temples and Growth scales increase reanimation rates. Magic scales give better reanimations."
            #domkill 0
            #templegems 6
            #spreadlazy 3
            #defdeath -3
            #defsloth 3
            #natureblessbonus 1
            #deathblessbonus 1
            #templecost 600
            #forestlabcost 350
            #foresttemplecost 300
            #end

            #newevent
            #rarity 5
            #req_domowner 59 -- Aspho
            #req_owncapital 0
            #req_dominion 1
            #req_maxdominion 1
            #msg "Dominion taxloss."
            #notext
            #nolog
            #taxboost -20
            #end

            #newevent
            #rarity 5
            #req_domowner 59 -- Aspho
            #req_owncapital 0
            #req_dominion 2
            #req_maxdominion 2
            #msg "Dominion taxloss."
            #notext
            #nolog
            #taxboost -40
            #end

            #newevent
            #rarity 5
            #req_domowner 59 -- Aspho
            #req_owncapital 0
            #req_dominion 3
            #req_maxdominion 3
            #msg "Dominion taxloss."
            #notext
            #nolog
            #taxboost -50
            #end

            #newevent
            #rarity 5
            #req_domowner 59 -- Aspho
            #req_owncapital 0
            #req_dominion 4
            #req_maxdominion 4
            #msg "Dominion taxloss."
            #notext
            #nolog
            #taxboost -60
            #end

            #newevent
            #rarity 5
            #req_domowner 59 -- Aspho
            #req_owncapital 0
            #req_dominion 5
            #req_maxdominion 5
            #msg "Dominion taxloss."
            #notext
            #nolog
            #taxboost -65
            #end

            #newevent
            #rarity 5
            #req_domowner 59 -- Aspho
            #req_owncapital 0
            #req_dominion 6
            #req_maxdominion 6
            #msg "Dominion taxloss."
            #notext
            #nolog
            #taxboost -70
            #end

            #newevent
            #rarity 5
            #req_domowner 59 -- Aspho
            #req_owncapital 0
            #req_dominion 7
            #req_maxdominion 7
            #msg "Dominion taxloss."
            #notext
            #nolog
            #taxboost -75
            #end

            #newevent
            #rarity 5
            #req_domowner 59 -- Aspho
            #req_owncapital 0
            #req_dominion 8
            #req_maxdominion 8
            #msg "Dominion taxloss."
            #notext
            #nolog
            #taxboost -80
            #end

            #newevent
            #rarity 5
            #req_domowner 59 -- Aspho
            #req_owncapital 0
            #req_dominion 9
            #req_maxdominion 9
            #msg "Dominion taxloss."
            #notext
            #nolog
            #taxboost -85
            #end

            #newevent
            #rarity 5
            #req_domowner 59 -- Aspho
            #req_owncapital 0
            #req_dominion 10
            #req_maxdominion 10
            #msg "Dominion taxloss."
            #notext
            #nolog
            #taxboost -90
            #end

            #newevent
            #rarity 5
            #req_domowner 59
            #req_mydominion 0
            #req_maxdominion -1
            #msg "Dominion taxloss."
            #notext
            #nolog
            #taxboost -10
            #end

            #newevent
            #rarity 5
            #req_domowner 59
            #req_mydominion 0
            #req_domchance 5
            #msg "Dominion taxloss."
            #notext
            #nolog
            #taxboost -40
            #end

            #newevent
            #rarity 5
            #req_domowner 59
            #req_mydominion 0
            #req_domchance 5
            #msg "Dominion taxloss."
            #notext
            #nolog
            #taxboost -40
            #end

            #newevent
            #rarity 5
            #req_domowner 59 -- Aspho
            #req_mydominion 1
            #req_owncapital 1
            #req_fort 1
            #req_domchance 10
            #nation -2
            #msg "Fort spawn statues."
            #notext
            #nolog
            #1unit 5788 -- Overgrown Statue
            #end

            #newevent
            #rarity 5
            #req_domowner 59 -- Aspho
            #req_owncapital 0
            #req_fort 1
            #req_mydominion 1
            #req_dominion 5
            #req_domchance 5
            #nation -2
            #msg "Fort spawn statues."
            #notext
            #nolog
            #1unit 5788 -- Overgrown Statue
            #end

            #newevent
            #rarity 5
            #req_domowner 59 -- Aspho
            #req_mydominion 1
            #req_fort 1
            #req_dominion 7
            #req_domchance 8
            #nation -2
            #msg "Fort spawn extra statue."
            #notext
            #nolog
            #1unit 5788 -- Overgrown Statue
            #end

            #newevent
            #rarity 5
            #req_domowner 59 -- Aspho
            #req_mydominion 1
            #req_dominion 5
            #req_domchance 5
            #nation -2
            #msg "Spawn Mandragora."
            #notext
            #nolog
            #1unit 314 -- Mandragora
            #end

            #newevent
            #rarity 5
            #req_domowner 59 -- Aspho
            #req_mydominion 1
            #req_dominion 7
            #req_domchance 8
            #nation -2
            #msg "Spawn extra Mandragora."
            #notext
            #nolog
            #1unit 314 -- Mandragora
            #end

            #selectspell 340 -- Carrion Lord
            #fatiguecost 2500
            #end

            #selectspell 338 -- Carrion Centaur
            #fatiguecost 700
            #end

            #selectmonster 901 -- Black Dryad
            #carcasscollector 3
            #end

            #selectmonster 2480 -- Dryad Hag
            #carcasscollector 6
            #end

            #selectmonster 711 -- Carrion Lady
            #clearmagic
            #magicskill 5 2
            #magicskill 6 2
            #magicskill 8 2
            #end

            #selectmonster 714 -- Carrion Centaur
            #clearmagic
            #magicskill 5 1
            #magicskill 6 1
            #magicskill 8 1
            #end

            #newmonster 6370
            #copystats 313 -- Manikin
            #copyspr 313 -- Manikin
            #clearspec
            #descr "A human skeleton reanimated by living vines and roots. Its vines strike like whips and their touch can make men fall asleep. In lands free of civilization it will grow stronger, but it will weaken where men toil."
            #att 6
            #def 6
            #str 10
            #slothpower 1
            #pooramphibian
            #pierceres
            #magicbeing
            #neednoteat
            #mor 50
            #enc 3
            #forestsurvival
            #spiritsight
            #poisonres 25
            #plant
            #end

            #selectmonster 313 -- Manikin
            #firstshape 6370
            #end

            #selectmonster 314 -- Mandragora
            #clearspec
            #descr "A human skeleton reanimated by living vines and roots. Its vines strike like whips and their touch can make men fall asleep. In lands free of civilization it will grow stronger, but it will weaken where men toil."
            #att 8
            #def 8
            #str 14
            #slothpower 1
            #pooramphibian
            #pierceres
            #magicbeing
            #neednoteat
            #mor 50
            #enc 3
            #forestsurvival
            #spiritsight
            #poisonres 25
            #plant
            #end

            #selectmonster 712 -- Satyr Manikin
            #clearspec
            #descr "Unholy rites performed by the Carrion Lords have caused vines and roots to reanimate the bones of dead satyrs, creating the satyr manikins. In lands free of civilization they will grow stronger, but they will weaken where men toil."
            #att 6
            #def 8
            #str 11
            #ap 6
            #slothpower 1
            #pooramphibian
            #pierceres
            #magicbeing
            #neednoteat
            #mor 50
            #enc 3
            #forestsurvival
            #spiritsight
            #poisonres 25
            #plant
            #end

            #selectmonster 713 -- Harpy Manikin
            #clearspec
            #descr "Unholy rites performed by the Carrion Lords have caused vines and roots to reanimate the bones of a dead harpy. The strange apparition lacks the ability to fly and can only hop. In lands free of civilization it will grow stronger, but it will weaken where men toil."
            #att 4
            #def 4
            #str 8
            #mapmove 22
            #slothpower 1
            #pooramphibian
            #pierceres
            #magicbeing
            #neednoteat
            #mor 50
            #enc 3
            #forestsurvival
            #spiritsight
            #poisonres 25
            #plant
            #end

            #selectmonster 1005 -- Minotaur Manikin
            #clearspec
            #descr "Unholy rites performed by the Carrion Lords have caused vines and roots to reanimate the bones of dead minotaurs, creating the minotaur manikins. In lands free of civilization they will grow stronger, but they will weaken where men toil."
            #att 5
            #def 5
            #str 15
            #ap 6
            #slothpower 1
            #pooramphibian
            #pierceres
            #magicbeing
            #neednoteat
            #mor 50
            #enc 3
            #forestsurvival
            #spiritsight
            #poisonres 25
            #plant
            #end

            #selectmonster 710 -- Carrion Lord
            #clearspec
            #descr "A Carrion Lord is a dead Pan reanimated and given unholy powers by a Panic Apostate, who forces the soul of the dead Pan to rejoin its own moss-covered carcass. The carcass is entwined with vines and roots that have a life of their own. The Carrion Lord is a powerful wielder of Nature magic, but is also given unholy powers over the dead. The Carrion Lord can create manikins by animating vines, roots and the bones of dead beasts. The presence of a Carrion Lord will spread the sleeping sickness of Asphodel, and nearby enemies may fall into a dreamless slumber. In lands free of civilization it will grow stronger, but it will weaken where men toil."
            #att 8
            #def 8
            #str 19
            #ap 8
            #enc 3
            #slothpower 1
            #sleepaura 9
            #pooramphibian
            #pierceres
            #magicbeing
            #neednoteat
            #mor 16
            #forestsurvival
            #spiritsight
            #poisonres 25
            #plant
            #holy
            #heal
            #fear 5
            #stealthy 0
            #reanimpriest
            #end

            #selectmonster 711 -- Carrion Lady
            #clearspec
            #descr "A Carrion Lady is a dead dryad reanimated and given unholy powers by a Panic Apostate, who forces the soul of the dead dryad to rejoin its own moss-covered carcass. The carcass is entwined with vines and roots that have a life of their own. The Carrion Lady has retained some of her former skills in the use of Nature magic, but her priestly powers are corrupted. She has unholy powers over the dead and is able to create manikins by animating vines, roots and the bones of dead beasts. Their minds are clouded by the will of the Apostates and they perform poorly at other tasks. The presence of a Carrion Lady will spread the sleeping sickness of Asphodel, and nearby enemies may fall into a dreamless slumber. In lands free of civilization she will grow stronger, but she will weaken where men toil."
            #att 9
            #def 11
            #str 11
            #ap 8
            #enc 3
            #slothpower 1
            #sleepaura 6
            #pooramphibian
            #pierceres
            #magicbeing
            #neednoteat
            #mor 14
            #forestsurvival
            #spiritsight
            #poisonres 25
            #plant
            #holy
            #heal
            #stealthy 0
            #reanimpriest
            #researchbonus -4
            #itemslots 31878 -- 3 miscs for tentacles
            #end

            #selectmonster 714 -- Carrion Centaur
            #clearspec
            #descr "A carrion centaur is the reanimated carcass of a centaur Hierophant. The soul of the centaur is forced to rejoin its own moss-covered carcass by a Panic Apostate. The bones are entwined with vines and roots that have a life of their own. The priestly powers of the former centaur are corrupted. Instead, the carrion centaur has unholy powers over the dead and is able to create manikins by animating vines, roots and the bones of dead beasts. Their minds are clouded by the will of the Apostates and they perform poorly at other tasks. The presence of a Carrion Centaur will spread the sleeping sickness of Asphodel, and nearby enemies may fall into a dreamless slumber. In lands free of civilization it will grow stronger, but it will weaken where men toil."
            #att 7
            #def 11
            #str 15
            #ap 18
            #enc 3
            #slothpower 1
            #sleepaura 6
            #pooramphibian
            #pierceres
            #magicbeing
            #neednoteat
            #mor 12
            #forestsurvival
            #spiritsight
            #poisonres 25
            #plant
            #holy
            #heal
            #reanimpriest
            #researchbonus -4
            #itemslots 29830 -- No Feet, extra misc for tentacles 
            #end

            #selectmonster 715
            #clearspec
            #descr "Unholy rites performed by the Carrion Lords have caused vines and roots to reanimate the bones of dead animals, creating the carrion beasts. In lands free of civilization they will grow stronger, but they will weaken where men toil."
            #att 7
            #def 7
            #str 9
            #ap 16
            #enc 3
            #slothpower 1
            #pooramphibian
            #pierceres
            #magicbeing
            #neednoteat
            #mor 50
            #forestsurvival
            #spiritsight
            #poisonres 25
            #plant
            #itemslots 12288
            #end

            #selectmonster 716
            #clearspec
            #descr "Unholy rites performed by the Carrion Lords have caused vines and roots to reanimate the bones of dead animals, creating the carrion beasts. In lands free of civilization they will grow stronger, but they will weaken where men toil."
            #str 10
            #ap 12
            #slothpower 1
            #pooramphibian
            #pierceres
            #magicbeing
            #neednoteat
            #mor 50
            #enc 3
            #forestsurvival
            #spiritsight
            #poisonres 25
            #plant
            #itemslots 12288
            #end

            #selectmonster 717
            #clearspec
            #descr "Unholy rites performed by the Carrion Lords have caused vines and roots to reanimate the bones of dead animals, creating the carrion beasts. In lands free of civilization they will grow stronger, but they will weaken where men toil."
            #str 14
            #ap 18
            #slothpower 1
            #pooramphibian
            #pierceres
            #magicbeing
            #neednoteat
            #mor 50
            #enc 3
            #forestsurvival
            #spiritsight
            #poisonres 25
            #plant
            #itemslots 12288
            #end

            #selectmonster 718
            #clearspec
            #descr "Unholy rites performed by the Carrion Lords have caused vines and roots to reanimate the bones of dead animals, creating the carrion beasts. In lands free of civilization they will grow stronger, but they will weaken where men toil."
            #att 5
            #str 20
            #ap 8
            #slothpower 1
            #pooramphibian
            #pierceres
            #magicbeing
            #neednoteat
            #mor 50
            #enc 3
            #forestsurvival
            #spiritsight
            #poisonres 25
            #plant
            #itemslots 12288
            #end

            #selectmonster 1005
            #clearspec
            #descr "Unholy rites performed by the Carrion Lords have caused vines and roots to reanimate the bones of dead minotaurs, creating the minotaur manikins. In lands free of civilization they will grow stronger, but they will weaken where men toil."
            #att 5
            #def 5
            #str 14
            #slothpower 1
            #pooramphibian
            #pierceres
            #magicbeing
            #neednoteat
            #mor 50
            #enc 3
            #forestsurvival
            #spiritsight
            #poisonres 25
            #plant
            #end

            #selectmonster 1006
            #clearspec
            #descr "Unholy rites performed by the Carrion Lords have caused vines and roots to reanimate the bones of dead animals, creating the carrion beasts. In lands free of civilization they will grow stronger, but they will weaken where men toil."
            #att 5
            #str 21
            #ap 10
            #slothpower 1
            #pooramphibian
            #pierceres
            #magicbeing
            #neednoteat
            #trample
            #mor 50
            #enc 2
            #forestsurvival
            #spiritsight
            #poisonres 25
            #plant
            #itemslots 12288
            #end

            #selectmonster 2311 -- Heirophant
            #descr "The awakening of the Vengeful Woods has altered the very essence of Asphodel's inhabitants. No longer are white calves born in the Sacred Groves. Instead, black centaurs are born, mirroring the dark mood of the forests. These black calves are trained as priests and guardians of the sacred groves of Asphodel, singing songs to please the Awakening God. Hierophants can affect carrion beasts with their prayers and have some skill in Death or Nature magic. The presence of a Black Centaur will spread the sleeping sickness of Asphodel, and nearby enemies may fall into a dreamless slumber. In lands free of civilization they will grow stronger, but they will weaken where men toil."
            #att 10
            #def 12
            #str 13
            #slothpower 1
            #sleepaura 3
            #end

            #selectmonster 2312 -- Heirophantide
            #descr "The awakening of the Vengeful Woods has altered the very essence of Asphodel's inhabitants. No longer are white calves born in the Sacred Groves. Instead, black centaurs are born, mirroring the dark mood of the forests. These black calves are trained as priests and guardians of the sacred groves of Asphodel, singing songs to please the Awakening God. Hierophants can affect carrion beasts with their prayers and have some skill in Death or Nature magic. The presence of a Black Centaur will spread the sleeping sickness of Asphodel, and nearby enemies may fall into a dreamless slumber. In lands free of civilization they will grow stronger, but they will weaken where men toil."
            #att 10
            #def 14
            #str 11
            #slothpower 1
            #sleepaura 3
            #end


            #selectmonster 1093
            #clearspec
            #descr "Unholy rites performed by the Carrion Lords have caused vines and roots to reanimate the bones of dead Hierophants. The unholy power of the Vengeful God has granted the sagittarian carcass magical weapons made of wood and vines. Sagittarian carcasses are holy to the followers of the Vengeful God. In lands free of civilization they will grow stronger, but they will weaken where men toil."
            #att 7
            #def 9
            #str 14
            #slothpower 1
            #pooramphibian
            #pierceres
            #magicbeing
            #neednoteat
            #forestsurvival
            #spiritsight
            #holy
            #enc 3
            #poisonres 25
            #plant
            #end

            #selectmonster 787 -- Black Centaur
            #clearweapons
            #att 11
            #def 14
            #str 14
            #ap 28
            #slothpower 1
            #berserk 0
            #sleepaura 3
            #gcost 10040
            #weapon 651 -- Bronze lance
            #weapon 55 -- Hoof
            #weapon 21 -- Javelin
            #descr "The awakening of the Vengeful Woods has altered the very essence of Asphodel's inhabitants. No longer are white calves born in the Sacred Groves. Instead, black centaurs are born, mirroring the dark mood of the forests. These black calves are trained as sacred warriors and emissaries of the wild. The presence of a Black Centaur will spread the sleeping sickness of Asphodel, and nearby enemies may fall into a dreamless slumber. In lands free of civilization they will grow stronger, but they will weaken where men toil."
            #end

            #newspell
            #name "Dark Revival"
            #descr "Through an Unholy rite the caster causes vines and roots to reanimate the bones of dead Hierophants. Unholy power has granted the sagittarian carcasses magical weapons made of wood and vines, and these creations are holy to the followers of the Vengeful God."
            #school 4
            #researchlevel 3
            #path 0 6
            #pathlevel 0 2
            #path 1 8
            #pathlevel 1 2
            #effect 10001
            #nreff 3
            #damage 1093
            #fatiguecost 400
            #onlygeosrc 128 -- Forest
            #restricted 59 -- MA Asphodel
            #end

            #newspell
            #name "Carrion Kin"
            #descr "Through an Unholy rite the caster causes vines and roots to reanimate the bones of a dead beast. It can be anything from a wolf carcass to a corpse of an elephant, but in the end the beast is given a new purpose, acting as a familiar for the mage. It can be given items and act as a commander, albeit a poor one."
            #school 4
            #researchlevel 1
            #path 0 6
            #pathlevel 0 1
            #path 1 8
            #pathlevel 1 1
            #effect 10021
            #damage -5200
            #fatiguecost 100
            #onlygeosrc 128 -- Forest
            #restricted 59 -- MA Asphodel
            #end

            #selectmonster 715 -- Carrion Beast (doggo)
            #montag 5200
            #itemslots 28672
            #end

            #selectmonster 716 -- Carrion Beast (doggo)
            #montag 5200
            #itemslots 28672
            #end

            #selectmonster 1006 -- Carrion Beast (elephant)
            #montag 5200
            #itemslots 290944
            #end

            #selectmonster 5539 -- Carrion Beast (bigass cat)
            #montag 5200
            #itemslots 290944
            #end

            #selectmonster 314 -- mandragora
            #montag 5200
            #end

            #selectmonster 313 -- manikin
            #montag 5200
            #end

            #selectmonster 712 -- manikin
            #montag 5200
            #end

            #selectmonster 1005 -- mino manikin
            #montag 5200
            #end

            #newspell
            #copyspell 712 -- Tangle Vines
            #name "Strangle Vines"
            #descr "Vines and roots will animate to ensnare anything living in the targeted area and strangle the life from them. The ensnared victims cannot move or attack anyone until they have destroyed the vines holding them. The stronger a victim is, the faster the vines will be destroyed and the more fertile the province is, the stronger the vines will be. Anyone slain by the vines will be raised as an undead creature serving the caster."
            #details "Str +DRN vs 19 to get free. The difficulty is increased or decreased by the Growth/Death scale of the province and is also increased by +1 in Forests and reduced by -1 in Wastelands."
            #researchlevel 2
            #path 1 5
            #pathlevel 1 1
            #range 30
            #prec 3
            #restricted 59 -- MA Asphodel
            #spec 1152921505152647168 -- Ignore Shields, UW OK, Undead & Lifeless immune, Secondaryeffect
            #end

            #newspell
            #name "Strangulation"
            #school -1
            #researchlevel 0
            #effect 74 -- Unlife Damage
            #nreff 1
            #aoe 1
            #damage 8
            #spec 545804416 -- Ignore Shields, UW OK, MR Neg, AN, Undead & Lifeless Immune
            #end

            #newspell
            #copyspell 712 -- Tangle Vines
            #name "Strangling Growth"
            #descr "Vines and roots will animate to ensnare anything living in the targeted area and strangle the life from them. The ensnared victims cannot move or attack anyone until they have destroyed the vines holding them. The stronger a victim is, the faster the vines will be destroyed and the more fertile the province is, the stronger the vines will be. Anyone slain by the vines will be raised as an undead creature serving the caster."
            #details "Str +DRN vs 19 to get free. The difficulty is increased or decreased by the Growth/Death scale of the province and is also increased by +1 in Forests and reduced by -1 in Wastelands."
            #researchlevel 6
            #path 0 6
            #pathlevel 0 3
            #path 1 5
            #pathlevel 1 1
            #range 30
            #aoe 1003
            #prec 3
            #fatiguecost 30
            #restricted 59 -- MA Asphodel
            #spec 1152921505152647168 -- Ignore Shields, UW OK, Undead & Lifeless immune, Secondaryeffect
            #end

            #newspell
            #name "Vine Strangle"
            #school -1
            #researchlevel 0
            #effect 74 -- Unlife Damage
            #nreff 1
            #aoe 1
            #damage 1007
            #spec 545804416 -- Ignore Shields, UW OK, MR Neg, AN, Undead & Lifeless Immune
            #end


            #newspell
            #copyspell 860 -- Create Mandragora
            #name "Revive Carrion Beasts"
            #descr "The caster causes vines and roots to animate animal corpses creating Carrion Beasts. Powerful mages can make more of the beasts with each casting of the spell. Carrion Beasts are undead and will fall apart if left on the battlefield without undead leadership."
            #school 4
            #researchlevel 4
            #path 0 5
            #path 1 6
            #nreff 2006
            #fatiguecost 1000
            #damage 5539
            #restricted 59 -- MA Asphodel
            #end

            #newspell
            #copyspell 724 -- Pack of Wolves
            #name "Animate Deadwood"
            #descr "The Panic Apostate gives unholy life to a grove of trees that have died. The rotting timber will move in a dreadful parody of life, animated by an intense hatred of those that would despoil the wild. These animated trees will serve their awakener until they are destroyed. More powerful mages can animate more trees with each casting."
            #school 4
            #researchlevel 4
            #path 0 6
            #path 1 5
            #pathlevel 0 3
            #pathlevel 1 2
            #effect 10001
            #fatiguecost 1000
            #damage 5288 -- Corrupted Tree
            #nreff 505
            #onlygeosrc 128 -- Forest
            #restricted 59 -- MA Asphodel
            #end

            #newspell
            #name "Raise Carrion Dragon"
            #descr "The Panic Apostate gives unholy life and powers to the carcass of a rotting Dragon. The Carrion Dragon is physically powerful and a wielder of strong Nature and Death magic. As a manifestation of the power of Death it is also given unholy powers over the dead. The Carrion Dragon can create Manikins by animating vines, roots and the bones of dead beasts."
            #school 4
            #researchlevel 7
            #path 0 5 -- Death
            #pathlevel 0 4
            #path 1 6 -- Nature
            #pathlevel 1 3
            #fatiguecost 5000
            #effect 10021
            #damage 5248 -- Carrion Dragon
            #nreff 1
            #restricted 59 -- Asphodel
            #end

            #newspell
            #name "Gaia's Vengeance"
            #descr "The full fury of the vengeful forest is unleashed and civilized men tremble in their fragile hovels. Vines will creep throughout the Dominion of the Dark God, strangling sleepers and reanimating them as manikins. Enemy commanders venturing into the Dominion will find themselves assaulted by unholy vine creatures when they rest. Attacks will be more likely in forested provinces and less likely in a lifeless waste, however the vine creatures cannot animate underwater. This enchantment lasts until someone dispels it or the caster dies."
            #details "Dominion * 10% chance additional Manikins will spawn & Population -2% per month. Growth scales in friendly Dominion cause assassinations by vine creatures on enemy commanders. High Dominion, more Growth scales or Forests increase chance, Low Dominion or waste decreases chance."
            #school 4
            #researchlevel 7
            #path 0 6
            #pathlevel 0 6
            #path 1 5
            #pathlevel 1 3
            #effect 10081
            #damage 478 -- Gaia's Vengeance
            #nreff 1
            #fatiguecost 6000
            #restricted 59 -- Asphodel
            #end

            #newspell
            #name "Carrion Wild"
            #descr "Deep within a forest, the corpse of an ancient Lord of the Wild is reanimated. The carcass is entwined with vines and roots that have a life of their own. The Wild Mandragora retains some knowledge of nature magic it possessed in life, and has gained a knowledge of death magic from the reanimation process. This spell can only be cast in a forested province."
            #school 4
            #researchlevel 8
            #path 0 6
            #path 1 5
            #pathlevel 0 6
            #pathlevel 1 4
            #effect 10021
            #damage 5305 -- Wild Mandragora
            #researchlevel 8
            #fatiguecost 4500
            #onlygeosrc 128 -- Forest
            #restricted 59 -- Asphodel
            #end

            #newspell
            #copyspell 1024 -- Melancholia
            #name "Sleeping Curse"
            #descr "The caster lays a curse upon a distant kingdom that afflicts it with the sleeping sickness of Asphodel. The populace will begin to fall into a deep and dreamless sleep. Crops will go unharvested, vines will be free to grow and livestock will wander. Soldiers fall asleep at their posts and even the temples are left untended as the priests succumb to the curse. The Dominion of the local god will be weakened by the power of Asphodel."
            #details "Scale of Sloth is set to 3. Dominion is reduced by 1-2. All units in the province must make a MR check vs 10 and a morale check vs 10. If they fail both they fall asleep forever."
            #researchlevel 4
            #school 4
            #path 0 6
            #pathlevel 0 3
            #path 1 5
            #pathlevel 1 2
            #provrange 3
            #fatiguecost 1000
            #restricted 59 -- Ashodel
            #end

            #newspell
            #copyspell 990 -- Sleep
            #name "Dreamless Sleep"
            #descr "The caster reaches out and causes an enemy to fall into a deep and dreamless sleep."
            #researchlevel 0
            #school 4
            #restricted 59 -- Ashodel
            #end

            #newspell
            #copyspell 1030 -- Syllable of Sleep
            #name "Deep Slumber"
            #descr "The caster calls on the power of Asphodel to bring a deep and dreamless sleep. The spell is indiscriminate and targets a whole squad of soldiers."
            #researchlevel 3
            #school 4
            #pathlevel 0 3
            #aoe 10
            #restricted 59 -- Ashodel
            #end

            #newspell
            #name "Enchant Army"
            #descr "The caster enchants the entire enemy army to put them into a deep and dreamless sleep. The spell can be resisted by those of strong will."
            #school 5
            #school 4
            #researchlevel 8
            #path 0 6
            #pathlevel 0 6
            #range 0
            #aoe 666
            #nreff 1
            #effect 11
            #damage 1024 -- Sleep
            #fatiguecost 400
            #spec 562970752 -- AN, Enemies Only, MR-Easy Neg, Ignores Shields, No Effect on Mindless/Undead/Lifeless, UWOK
            #restricted 59 -- Ashodel
            #end

            #selectspell 990 -- Sleep
            #notfornation 59 -- Asphodel
            #end

            
        -- Gaia's Vengeance Montag list

            #selectmonster 717 -- Carrion Beast (Medium)
            #montag 5163
            #end

            #selectmonster 718 -- Carrion Beast (large)
            #montag 5163
            #montagweight 2
            #end

            #selectmonster 714 -- Carrion Centaur
            #montag 5163
            #montagweight 2
            #end

            #selectmonster  711 -- Carrion Lady
            #montag 5163
            #end

            #selectmonster 330 -- Dark Vines
            #montag 5163
            #montagweight 1
            #end

            -- apostate now actually able to get you proper carrion lords
                #selectmonster 709
                #noreqtemple
                #holy
                #magicskill 8 3 -- actually 
                #magicboost 8 -2
                #end

                #selectmonster 710
                #onebattlespell 342 
                #end

                #selectmonster 711
                #onebattlespell 341
                #end
                #selectmonster 710
                #onebattlespell 342 
                #end

                #selectmonster 711
                #onebattlespell 341
                #end
