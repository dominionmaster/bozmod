#modname "bozmod 1.5"
#description "Adds many new pretenders, especially dragons, buffs existing dragons. Adds many logistical options and makes existing logistical options cheaper and more accessible. Nerfs things that scale very well. Adds investment options for players. This mod is designed to be played on very large games. If you like this mod, support my other game development! Lookup Dominion Master on discord or Patreon.
Author Boz
Contributors Warpman, Red Rob, Lucid, Executor, BING_XI_LAO"
#icon "./bozmod/Bozmod_banner.tga"
#version 1.5

--THIS FILE IS IN YAML FORMAT, to browse it nicely - open it in VS code and click on the markdown button at the bottom, select YAML and away you go. More details here
--https://code.visualstudio.com/docs/languages/overview#_changing-the-language-for-the-selected-file

--ID Ranges
    --7000-7200 creatures
    --700-730 weapons
    --500-600 items
    --3500+ for spells
    --1400-1410 + 1690-1720 sites
    --13300-13400 + 7000-7010 montags
    --601 eff_id for events
    --575-600 ench_id for events

--PATCH NOTES
    --V 1.5
    --added white minotaur summon spells to pangaea EA/MA and Asphodel
    --reduced nerf on globals
    --nerfed dispell
    --added Diamedulla with changes from original
        -- gave diamedulla 2 new summons, necrotitan and necrotaur
        -- gave diamedulla necropocalypse global
        -- removed 3 item event from diamedulla
        -- flesh golem rec 1 -> rec 2
    --new pretenders
        -- godzilla with fire event to transform
        -- golden egg with event to trasform into Ghidora
        -- senator armstrong
    --pretender changes
        -- mutant dragon insanity 15 -> 12
        -- mutant dragon cost 260 -> 250
        -- red/blue/green dragon buffed
        -- description changes on many pretenders
        -- remove fly from zombie dragon and lowered it's MR by 2
    --new nations
        -- Krieg based on HOMM inferno
        -- Cangobia based on dingo-gnolls from Australia that worship dracons (dinosaurs)
    --Items
        -- added crossbreeding 10 vial item

--TODOs
    --description and pathfix on bane dragon
    --update decsription on the kngihts to be 3rd person
    --sprite bug on bringer of blight

-- Pretender
    -- Cosmic Egg
            #newmonster 7144 --Ghidorah egg
            #copystats 4993
            #name "Cosmic Egg"
            #descr "The Cosmic Egg is a mysterious object which has sat in place since time immemorial. Its origins remain unknown, but one thing has become clear - there is something truly spectacular growing inside it. 
            
            With the advent of the ascension wars, the egg has shown the ability to communicate through its shell, and followers flock to it, seeing it as the Pantokrator waiting to be reborn."
            #spr1 "./bozmod/Monsters/goldenegg.tga"
            #clearmagic
            #magicskill 1 3
            #magicskill 6 1
            #pathcost 40
            #startdom 4
            #gcost 180
            #shockres 10
            #okleader 
            #mind
            #mr 30
            #blind
            #sunawe 3
            #pooramphibian
            #homerealm 1
            #end

        -- Lightning Breath
            #newweapon
            #name "Lightning Breath"
            #dmg 1
            #range -3
            #beam
            #range0
            #nratt 1
            #ammo 9
            #aoe 4
            #halfstr
            #shock
            #armorpiercing
            #flyspr 10307
            #nouw
            #magic
            #end
            --#transform "The Progenitor"
                #newevent 
                #req_targmnr "Cosmic Egg"
                #req_monster "Cosmic Egg"
                #req_godismnr "Cosmic Egg"
                #req_owncapital 1
                #req_pop0ok
                #nation -2
                #code 500
                #rarity 13
                #req_rare 10
                #req_turn 36
                #msg "With an earth shattering boom, first crack has appeared on the cosmic egg ##godname##, the blast was heard across the entire world, surely this is the beginning of something terrible!"
                #worldunrest 4
                #kill 10
                #end

                #newevent 
                #req_targmnr "Cosmic Egg"
                #req_monster "Cosmic Egg"
                #req_godismnr "Cosmic Egg"
                #req_owncapital 1
                #req_pop0ok
                #nation -2
                #transform "The Progenitor"
                #req_code 500
                #rarity 13
                #code 0
                #msg "With a mighty blast, the cosmic egg ##godname## has hatched! A great three headed beast has emerged whose presence is felt across the world!"
                #worldincscale 4
                #kill 20
                #worldunrest 6
                #end


        -- The Progenitor
            -- Events
                #newevent
                #rarity 5
                #req_monster 7141 --progenitor
                #unrest 35
                #kill 5
                #msg "The chaos storm that follows the Progenitor has devastated this province"
                #req_land
                #req_pop0ok
                #end
            -- Weapons
                #newweapon
                #copyweapon "Wing Buff"
                #name "Titanic Wing Buff"
                #aoe 2
                #end

                #newweapon
                #copyweapon "Cold Breath"
                #name "Storm Breath"                    
                #dmg 1
                #ammo 6
                #aoe 4
                #halfstr
                #shock
                #armorpiercing
                #nouw
                #magic
                #end

            -- Monster
                #newmonster 7141
                #name "The Progenitor" 
                #clearspec
                #unique
                #spr1 "./bozmod/Cangobia/progenitor.tga"
                #spr2 "./bozmod/Cangobia/progenitor2.tga"
                --base stats
                    #hp 280 
                    #size 6 
                    #prot 28 
                    #mr 23 
                    #mor 30 
                    #str 31 
                    #att 21 
                    #def 12 
                    #prec 15 
                    #ap 16
                    #enc 2
                    #mapmove 32
                --resistances
                    #spiritsight     
                    #poisonres 25
                    #fireres -5
                    #coldres 15
                    #shockres 25                    
                    #stormimmune
                    #eyes 6
                #goodleader
                #awe 2
                #nowish
                #flying
                #dragonlord 5
                #fear 15
                #itemslots 262656
                #regeneration 25
                #weapon "Swallow"
                #weapon 322 --bite
                #weapon 322 --bite
                #weapon 322 --bite
                #maxage 10000
                #weapon "Titanic Wing Buff"
                #weapon "Storm Breath"                
                #onebattlespell "Storm"
                #end
    -- King of Beasts
        -- Weapons
            #newweapon
            #name "Deadly Afterglow"
            #magic
            #internal
            #inanimateimmune
            #undeadimmune
            #sizeresist
            #bowstr
            #end
            
            #newweapon
            #copyweapon 61
            #name "Blazing beam"
            #dmg 0
            #range -3
            #ammo 5
            #natural
            #bonus
            #secondaryeffectalways 171
            #uwok
            #end
            
            #newweapon
            #copyweapon 61
            #name "God-king beam"
            #dmg 5
            #range -3
            #ammo 4
            #natural
            #aoe 5
            #bonus
            #secondaryeffectalways 17
            #uwok
            #secondaryeffect "Deadly Afterglow"
            #end
            
            #newweapon
            #copyweapon 532
            #name "Enormous Stomp"
            #aoe1
            #blunt 
            #sizeresist 
            #aoe 1
            #end
        -- Base
            #newmonster 7142 --zilla
            #hp 170
            #name "King of Beasts"
            #size 6
            #descr "The self-proclaimed King of the Beasts is an ancient draconid of unknown origin. His enormous size and desire for peace once led him on a crusade against the mightiest beasts of old. This made him a favourite of the Pantokrator, who imprisoned those enemies the Kind of Beasts was able to subdue but could not slay. The King fell into a slumber for an age once his great campaign was complete.
            With the Pantokrator gone, mighty foes raise their heads once again, and it seems there is no peace to be had unless the mantle of godhood is claimed by the King.
            A being of such stature, wielding powers of fire and water could bring about the end of the world, though the King has yet to awaken."
            #spr1 "./bozmod/Monsters/Warpman/Zilla1.tga"
            #spr2 "./bozmod/Monsters/Warpman/Zilla2.tga"
            #att 12
            #def 8
            #str 24
            #prec 8
            #prot 21
            #mr 21
            #weapon "Bite"
            #weapon "Tail Sweep"
            #weapon "Enormous Stomp"
            #weapon "Blazing Beam"
            #magicskill 0 2
            #pathcost 80
            #startdom 2
            #gcost 280
            #enc 2
            #mapmove 20
            #ap 12
            #startage 1800
            #maxage 6000
            #heal
            #fireres 35
            #poisonres 5
            #slashres
            #okleader 
            #nowish
            #awe 2
            #animalawe 5
            #researchbonus -25
            #pooramphibian
            #fear 10
            #siegebonus 50
            #firerange 1
            #minprison 1
            #itemslots 12288
            #incunrest 50
            #homerealm 4

            #end
        -- Enraged King
            #newmonster 7143 --nuclear zilla
            #hp 370
            #name "Enraged King"
            #size 6
            #descr "The self-proclaimed King of the Beasts is an ancient draconid of unknown origin. His enormous size and desire for peace once led him on a crusade against the mightiest beasts of old. This made him a favourite of the Pantokrator, who imprisoned those enemies the Kind of Beasts was able to subdue but could not slay. The King fell into a slumber for an age once his great campaign was complete.
            With the Pantokrator gone, mighty foes raise their heads once again, and it seems there is no peace to be had unless the mantle of godhood is claimed by the King.
            A being of such stature, wielding powers of fire and water could bring about the end of the world, though the King has yet to awaken.
            The blazing heat that the King emits has begun to melt and shake the surrounding world with his hatred."
            #spr1 "./bozmod/Monsters/Warpman/NuclearZilla1.tga"
            #spr2 "./bozmod/Monsters/Warpman/NuclearZilla2.tga"
            #att 15
            #def 8
            #str 28
            #prec 8
            #prot 21
            #mr 22
            #weapon "Bite"
            #weapon "Tail Sweep"
            #weapon "Enormous Stomp"
            #weapon "God-king Beam"
            #magicskill 0 2
            #gcost 280
            #enc 2
            #mapmove 20
            #ap 12
            #startage 1800
            #maxage 6000
            #heal
            #fireres 45
            #poisonres 5
            #slashres
            #okleader 
            #researchbonus -25
            #pooramphibian
            #heat 15
            #uwheat 5
            #fear 15
            #siegebonus 200
            #dragonlord 2
            #firerange 2
            #onebattlespell 925 -- heat from hell
            #reaper 5
            #fortkill 5
            #localsun
            #decscale 2
            #incscale 3
            #end
        -- Event
            #newevent
            #req_targmnr "King of Beasts"
            #req_monster "King of Beasts"
            #transform "Enraged King"
            #rarity 13            
            #msg "A eruption of blazing heat is felt around the world followed by a low rumble, the great roar of ##godname## is heard across the world as he transforms into a walking cosmic inferno"
            #worldunrest 5            
            #req_rare 10
            #killpop 500
            #req_turn 36
            #msg "The fiery heart of the King of the Beasts beats with rage. As if glowing from within, he now marches on, melting his foes in otherworldly fires of doom. Fire and death follow him as the world weeps from his presence"
            #end
    -- The Singularity --------------------------------------------------------------
        --base
            #newmonster 7000
            #name "The Singularity"
            #spr1 "./bozmod/Monsters/singularity.tga"
            #spr2 "./bozmod/Monsters/singularity2.tga"
            #descr "The Singularity is a creature from the void banished from existence by the previous Pantokrator. Where it came from, nobody knows - but what is known is that it consumes dead matter into itself and grows stronger. Its depthless appetite spurs it to obliterate whole provinces. Some offer prayers of appeasement to the abomination, and in time it has gained sentience, guided by their propitiations. It has become a different kind of threat to the world as its old hunger turns towards godly ambition. Its followers demand it consume all heathens first. With the Pantokrator gone, the Singularity is unleashed upon the world once again."
        --stats
            #hp 160
            #size 6
            #prot 0
            #mr 18
            #mor 30
            #str 28
            #att 5
            #def 0
            #prec 12
            #ap 14
            #mapmove 10
            #gcost 220
        --gear
            #weapon 90 -- Crush
            #weapon 90 -- Crush
            #weapon 90 -- Crush
            #itemslots 274560 --[262144 + 128 + 12288] - head, 2 misc, head only crown
        --specials
            #okmagicleader
            #okleader
            #superiorundeadleader
            #undcommand 100
            #heal
            #diseaseres 100
            #voidsanity 10
            #poorleader
            #corpseeater 15
            #popkill 100
            #deadhp 1
            #startdom 4
            #pathcost 80
            #trample
            #undead
            #neednoteat
            #diseaseres 100
            #incunrest 100
            #regeneration 10
            #pierceres
            #bluntres
            #fear 10
            #magicskill 5 2
            #magicskill 7 1
        #end
    -- Red Dragon ------------------------------------------------------------
        --base
            #selectmonster 216
            #gcost 200
            #clearmagic
            #clearspec
        --stats
            #hp 165
            #size 6
            #prot 20
            #mor 30
            #mr 20
            #str 29
            #att 17
            #def 11
            #prec 14
            #mapmove 28
            #ap 14
            #enc 2
        --gear
            #itemslots 274560 --[262144 + 128 + 12288] - head, 2 misc, head only crown
        --specials
            #fireres 25
            #diseaseres 100
            #pathcost 80
            #fear 15
            #dragonlord 3
            #flying
            #heal
            #startdom 2
            #homerealm 10
            #berserk 3
            #heat 6
            #inspirational 2
            #firepower 1
            #gold 80
            #gemprod 0 1
            #magicskill 0 2
        #end
    -- Blue Dragon ------------------------------------------------------------
        --base
            #selectmonster 265
            #gcost 200
            #clearmagic
            #clearspec
        --specials
            #coldres 25
            #diseaseres 100
            #pathcost 80
            #fear 15
            #dragonlord 3
            #flying
            #heal
            #startdom 2
            #homerealm 10
            #amphibian
            #cold 8
            #snowmove
            #coldpower 1
            #gold 80
            #gemprod 2 1
            #magicskill 2 1
            #magicskill 1 1
        --stats
            #hp 145
            #size 6
            #prot 18
            #mor 30
            #mr 20
            #str 25
            #att 16
            #def 16
            #prec 13
            #mapmove 28
            #ap 13
            #enc 2
        --gear
            #itemslots 274560 --[262144 + 128 + 12288] - head, 2 misc, head only crown
        #end
    -- Green Dragon ------------------------------------------------------------
        --base
            #selectmonster 266
            #gcost 200
            #clearmagic
            #clearspec
        --specials
            #poisonres 25
            #diseaseres 100
            #pathcost 80
            #fear 15
            #dragonlord 3
            #flying
            #heal
            #startdom 2
            #homerealm 10
            #swampsurvival
            #swimming
            #gold 80
            #decscale 3
            #gemprod 6 1
            #poisoncloud 4
            #regeneration 12
            #magicskill 6 2
        --stats
            #hp 165
            #size 6
            #prot 18
            #mor 30
            #mr 21
            #str 24
            #att 15
            #def 12
            #prec 12
            #mapmove 25
            #ap 11
            #enc 2
        --gear
            #itemslots 274560 --[262144 + 128 + 12288] - head, 2 misc, head only crown
        #end
    -- Fairy Dragon ------------------------------------------------------------
        --base
            #newmonster 7002
            #name "Fairy Dragon"
            #spr1 "./bozmod/Monsters/Fairy_Dragon_Monarch.tga"
            #spr2 "./bozmod/Monsters/Fairy_Dragon_Monarch_Attack.tga"
            #descr "The Fairy Dragon hails from deep and mysterious forests, but in ancient times he would rove far and wide with his friend the Pantokrator. Mirth and magic followed in his wake. Although referred to as a dragon, the Fairy Dragon is only so in name: his body resembles a large seahorse borne by giant butterfly wings. From a pointed snout flows a stream of song which mixes spells and lulls those nearby to sleep. Since his friend’s mysterious departure, the Fairy Dragon has risen to confront the many pretenders who would dishonour the authority of the absent Pantokrator. But to preserve the order set by his lord, the Fairy Dragon must himself ascend as Pantokrator; and only then will he be able to search for his missing friend."
        --specials
            #gcost 210
            #clearspec
            #clearmagic
            #diseaseres 100
            #pathcost 20
            #dragonlord 1
            #flying
            #heal
            #startdom 1
            #magicpower 1
            #magicbeing
            #illusion
            #bonusspells 1
            #stealthy 20
            #magicskill 6 1
            #magicskill 4 1
            #magicskill 1 1
            #sleepaura 10
            #spellsinger
            #pooramphibian
            #expertmagicleader
        --gear
            #weapon 20 -- Bite
            #weapon 532 -- Tail sweep
            #itemslots 323712 --[262144 + 61440 + 128] - head only crown + 61440 4misc + 1 head
        --stats
            #hp 46
            #size 4
            #prot 6
            #mor 24
            #mr 23
            #str 14
            #att 11
            #def 16
            #prec 16
            #mapmove 28
            #ap 14
            #enc 2
        #end
    -- Black Dragon ------------------------------------------------------------
        #newmonster 7004
        #name "Black Dragon"
        #desc "The black dragon uses an acidic breath attack, which can blind and poison enemies"
        #spr1 "./bozmod/Monsters/blackdragon.tga"
        #spr2 "./bozmod/Monsters/blackdragon2.tga"
        #descr "The Black Dragon is an ancient reptile of tremendous physical and magical power. Born before the history of time, the dragons were perceived as threats to the world and imprisoned by the previous Pantokrator millenia ago. The Black Dragon is capable of breathing blinding and corrosive acid upon enemies. Dragons are closely attuned to the magic that brought them to life and thus focus on that magic. The Black Dragon is skilled in the lore of potions and substances, its acidic blood and saliva is as useful for alchemy as it is for melting castle gates."
        #gcost 220
        #diseaseres 100
        #pathcost 80
        #fear 10
        #dragonlord 1
        #heal
        #startdom 2
        #homerealm 10
        #flying

        #itemslots 274560 --[262144 + 128 + 12288] - head, 2 misc, head only crown

        #weapon "Acid Breath" -- Acid
        #weapon 542 -- Acid
        #weapon 29 -- Claw
        #weapon 29 -- Claw
        #weapon 532 -- Tail sweep

        #hp 120
        #size 6
        #prot 20
        #mor 30
        #mr 19
        #str 25
        #att 15
        #def 11
        #prec 12
        #enc 2
        #mapmove 28
        #ap 10

        #fireres  5
        #researchbonus 5
        #coldres  5
        #magicskill 0 1
        #magicskill 2 1
        #blind
        #acidshield 9
        #alchemy 70
        #siegebonus 50
        
        #end

        #newweapon 
        #name "Acid Blind"
        #mrnegates
        #dt_aff
        #dmg 4096
        #secondaryeffectalways 515
        #end

        #newweapon
        #name "Acid Breath"
        #dmg 1
        #range -3
        #beam
        #range0
        #nratt 1
        #ammo 4
        #aoe 2
        #halfstr
        #acid
        #armorpiercing
        #flyspr 10179
        #bonus
        #magic
        #secondaryeffectalways "Acid Blind"
        #end
    -- Dracolich ------------------------------------------------------------
        #selectmonster 644
        #str 22
        #incscale 3
        #deathpower 1
        #dreanimator 4
        #gemprod 5 1
        #end
    -- Crystal Dragon ------------------------------------------------------------
        #newmonster 7005
        #name "Crystal Dragon"
        #spr1 "./bozmod/Monsters/crystaldragon.tga"
        #spr2 "./bozmod/Monsters/crystaldragon2.tga"
        #descr "The Crystal Dragon was sculpted by the previous Pantokrator as a symbol of great beauty and power. Unsatisfied with the immaculate statue, he gifted it with life and a pure, incorruptible mind. From then on, the Crystal Dragon was an obedient personal guardian of the Pantokrator, to be feared and adored by all. Now that the Pantokrator is gone, the Crystal Dragon has no other master. It retained its incorruptible mind and artificial life, making it immune to many spells, yet nobody knows how it continues to exist without the Pantokrator’s magic keeping it alive. Many have taken the continued existence of the Crystal Dragon as a sign that the Pantokrator is not truly gone. While His other works crumble, many have come to worship the Crystal Dragon as the sole remaining intercessor of the Pantokrator, or as some have begun to whisper, even his new incarnation. The presence of the Crystal Dragon fills the populace with confidence that the Pantokrator will return to restore order, reducing unrest. Crystal mages from far and wide take great interest in studying the dragon, and join its cause in the hope that their allegiance can buy the secret of its life. The Crystal Dragon was traditionally adorned with treasures offered to the old Pantokrator, and has developed an affinity for wearing many such magical trinkets."

        #gcost 220
        #diseaseres 100
        #pathcost 80
        #fear 10
        #dragonlord 1
        #heal
        #startdom 2

        #itemslots 290944 --[262144 + 28672 + 128] - head only crown + 3misc + 1 head

        #weapon 20 -- Bite
        #weapon 29 -- Claw
        #weapon 29 -- Claw
        #weapon 532 -- Tail sweep

        #hp 130
        #size 6
        #prot 20
        #mor 50
        #mr 25
        #str 25
        #att 8
        #def 4
        #prec 6
        #enc 0
        #mapmove 20
        #ap 6

        #awe 1
        #poisonres 15
        #shockres  5
        #fireres  5
        #inanimate
        #incunrest -50
        #mind
        #magicpower -1
        #gemprod 4 1
        #magicskill 3 1
        #magicskill 4 1
        #pierceres
        #slashres
        #noleader
        #blind
        #stonebeing

        #end

        #newevent
        #rarity 5
        #req_rare 5
        #nation -2    
        #req_monster "Crystal Dragon"
        #com 350 --crystal mage
        #msg "The mystery of the Crystal Dragon's life has attracted a crystal mage to study it"
        #end
    -- Bane Dragon ------------------------------------------------------------
        #selectmonster 2608
        #name "Bane Dragon"
        #str 23
        #descr "The Bane Dragon is an ancient reptile of tremendous physical and magical power, imprisoned like the other dragons millennia ago by the previous Pantokrator. In a prior era the Bane Dragon was tricked by a Necromancer, whose potions granted strength, but slowly rendered those who drank them into half-dead slaves. When the Dragon realised his mistake, his rage burned hotter than the poison in his veins, and he destroyed the Necromancer. Corrupted by the power of the grave, the Bane Dragon’s breath is more powerful than ever, and those who strike the beast with short weapons will be scorched by a veil of banefire."
        #clearweapons
        #clearmagic 
        #weapon 532 -- Tail sweep
        #weapon 533 -- Dragon Fire
        #weapon 20 -- Bite
        #weapon 20 -- Bite
        #weapon 20 -- Bite
        #weapon 348 -- Banefire strike
        #banefireshield 8
        #end
    -- Shadow Dragon ------------------------------------------------------------
        #newmonster 7006
        #name "Shadow Dragon"
        #spr1 "./bozmod/Monsters/shadow_dragon.tga"
        #spr2 "./bozmod/Monsters/shadow_dragon_attack.tga"

        #descr "The Shadow dragon is a being born of darkness that wishes to destroy all the light in the world and envelop everything in its fuligin shadow. In the age preceding the last  Pantokrator, there was a Dragon of Light and a Dragon of Shadow, whose rivalry balanced the primordial forces they represented. Unable to gain an advantage over its counterpart, the Shadow Dragon tricked the Pantokrator into utterly destroying the Dragon of Light. Once the Pantokrator realised his error, the Shadow Dragon was banished into the Void, but never lost sight of the material world. With the Pantokrator is gone, the Shadow Dragon has re-entered the world. With followers drawn from the mortal races, and without its arch nemesis to fight, it has nothing left to achieve but godhood."

        #gcost 230
        #clearspec
        #clearmagic
        #diseaseres 100
        #pathcost 80
        #fear 10
        #dragonlord 1
        #heal
        #startdom 2
        #flying

        #clearweapons
        #weapon 284 --steal strength
        #weapon 284 --steal strength
        #weapon 20 -- Bite
        #weapon 532 -- Tail sweep
        #weapon 568 -- Drake frost

        #poisonres 10
        #coldres 10
        #undead
        #enc 0
        #ethereal
        #stealthy 10
        #invisible
        #cold 3
        #pooramphibian
        #fireres -6
        #undead
        #neednoteat
        #spiritsight
        #saltvul 4
        #darkpower 8
        #magicskill 5 1
        #magicskill 1 1
        #noriverpass


        #hp 105
        #size 6
        #prot 14
        #mor 30
        #mr 19
        #str 16
        #att 15
        #def 16
        #prec 12
        #mapmove 28
        #expertundeadleader
        #ap 10

        #itemslots 274560 --[262144 + 128 + 12288] - head, 2 misc, head only crown
        #end
    -- Soulless Dragon ------------------------------------------------------------
        #newmonster 7007
        #name "Soulless Dragon"
        #spr1 "./bozmod/Monsters/Zombie_dragon.tga"
        #spr2 "./bozmod/Monsters/Zombie_dragon_attack.tga"
        #gcost 240
        #clearspec
        #clearmagic
        #descr "The Soulless Dragon is an ancient reptile of tremendous physical and magic power. Born before the history of time, the dragons were perceived as threats to the world and imprisoned by the previous Pantokrator millenia ago. The Soulless Dragon has contracted a magical disease that mutated within it's body causing it to become soulless, who had cursed it with such a fate it is unknown. Now the Zombie dragon is able to grow larger from consuming bodies of the dead, already being a powerful dragon it presents a huge threat to peace in the world. Dragons are closed attuned to the magic that brought them into life and thus focus on that magic. The Soulless Dragon has become closely attuned to it's growing flock of followers who see it as a new god and it has set out to destroy the other pretenders."

        #diseaseres 100
        #pathcost 80
        #fear 10
        #dragonlord 1
        #heal
        #startdom 2

        #clearweapons
        #weapon 20 -- Bite
        #weapon 29 -- Claw
        #weapon 532 -- Tail sweep
        #weapon 254 -- Plague Breath

        #itemslots 274560 --[262144 + 128 + 12288] - head, 2 misc, head only crown

        #poisonres 25
        #coldres 15
        #inanimate
        #undead
        #enc 0
        #mr 14
        #spiritsight
        #trample
        #bluntres
        #pierceres
        #corpseeater 8
        #deadhp 1
        #raiseonkill 80
        #diseasecloud 8  
        #raiseonkill 80
        #researchbonus -10
        #magicskill 5 1

        #hp 160
        #size 6
        #prot 13
        #mor 50
        #mr 15
        #str 25
        #att 12
        #def 6
        #prec 8
        #mapmove 20
        #ap 10

        #superiorundeadleader

        #end
    -- Mutant Dragon ------------------------------------------------------------

        #newmonster 7008
        #name "Mutant Dragon"
        #spr1 "./bozmod/Monsters/Mutant_Dragon.tga"
        #spr2 "./bozmod/Monsters/Mutant_Flex.tga"
        #descr "The mutant dragon is a creature of mysterious origin, some say it used to be a powerful wizard who delved further than anyone else into the art of cross breeding and genetic research, at some point learning how to merge his own body with that of a powerful dragon. Ultimately, whatever the stories say about the Mutant dragon, one thing is known for sure - it is a creature that is optimized for one thing and that is destruction. It retains, perhaps through this merged scientist - great knowledge of crossbreeding and from it's dragon counterpart, a foul temper and disobedience to it's other head. The Mutant dragon's body is in a constant state of struggle between the two heads that control it, each with their own personality and they aren't always on good terms. The madness of the Mutant dragon spreads to it's populace, while it's arrogance can prevent it from retreating from combat, even if the situations it should are extremely rare."
        #gcost 250
        #clearspec
        #clearmagic
        #diseaseres 100
        #pathcost 80
        #fear 10
        #heal
        #startdom 2

        #weapon 29 -- Claw
        #weapon 29 -- Claw
        #weapon 29 -- Claw
        #weapon 29 -- Claw
        #weapon 20 -- Bite
        #weapon 20 -- Bite

        #poisonres 3
        #fireres 3
        #coldres 3
        #shockres 3
        #crossbreeder 12
        #pathcost 80
        #magicskill 7 1
        #magicskill 6 1  
        #siegebonus 30
        #ambidextrous 6
        #berserk 1
        #incunrest 50
        #shatteredsoul 12

        #hp 130
        #size 6
        #prot 12
        #mor 30
        #mr 16
        #str 21
        #att 16
        #def 14
        #enc 3
        #prec 10
        #mapmove 18
        #ap 10

        #itemslots 274846 --[12288 + 384 + 262144 + 30]  2 mis  + 2 heads  + heads crowns + 4 hands
        #end
    -- Sanguine Dragon ------------------------------------------------------------

        #selectitem 500
        #name "The flaying ring"
        #constlevel 12
        #copyspr 269
        #type 8
        #autospell "Aura of pain"
        #autospellrepeat 1 
        #mainpath 0
        #mainlevel 5
        #nofind
        #cursed
        #curse
        #end

        #newevent
        #rarity 5
        #req_rare 100
        #req_monster 7009
        #req_targmnr 7009 
        #req_targnoitem 500 --the flaying ring
        #nolog
        #notext
        #addequip 9
        #msg "Sanguine Dragon equips [The flaying ring]"
        #end

        #newspell
        #copyspell "Agony"
        #name "Aura of pain"
        #range 1
        #school -1
        #end

        #newmonster 7009
        #name "Sanguine Dragon"
        #descr "The Sanguine dragon is an vengeaful creature that was cursed into suffering immense pain for all eternity by the Pantokrator for it's arrogance. Now with the Pantokrator gone, the Sanguine dragon has mastered this curse and turned it into a weapon. 
        Having endured this divine pain for eons, it has learned to channel and share this pain with others. Those unfortunate enough to find themselves near the Sanguine Dragon will bleed profusely and their psyche may be left permanently scarred, while those who strike the Sanguine Dragon will feel the edge of their blows against their own flesh. The Sanguine dragon's is permanently bleeding and it's blood colors the sky, blood red rain follows it wherever it goes."

        #spr1 "./bozmod/Monsters/sanguine_dragon.tga"
        #spr2 "./bozmod/Monsters/sanguine_dragon_attack.tga"
        #gcost 220

        #diseaseres 100
        #pathcost 80
        #dragonlord 1
        #heal
        #flying
        #startdom 2

        #fear 15
        #magicskill 7 3
        #immortal
        #demon
        #curseattacker 5
        #douse 4
        #bloodvengeance 1
        #voidsanity 5
        #onebattlespell 1096 --blood rain
        #deathcurse

        #clearweapons
        #weapon 63 --life drain
        #weapon 29 -- Claw
        #weapon 532 -- Tail sweep

        #startitem 500

        #hp 150
        #size 6
        #prot 11
        #mor 30
        #mr 20
        #str 23
        #att 15
        #def 12
        #prec 12
        #mapmove 28
        #ap 10
        #enc 3

        #itemslots 274560 --[262144 + 128 + 12288] - head, 2 misc, head only crown

        #end
    -- Psionic  Dragon ------------------------------------------------------------

        #newmonster 7010
        #name "Psionic Dragon"
        #spr1 "./bozmod/Monsters/psionic_dragon_5.tga"
        #spr2 "./bozmod/Monsters/psionic_dragon_5_attack.tga"
        #descr "Illithid have always been anxious to try crossbreeding with different species on the newfound homeworld, yet one experiment went so well they now think it might have been a failure in the long run.
        Dragon mind has proven to be superior to that of Illithid and perhaps even that of the Mind lords. That coupled with the impossible psionic powers granted by the enhanced perception of Illithid and their innate ability to feel and transmit thoughts onto others means there is but one thing this creature needs to attain, godhood - consume all the minds of sentients and become one with the world.
        The body of the Psionic dragon is quite soft and isn't as tough as that of dragons and can be damaged quite easily"
        #gcost 220
        #diseaseres 100
        #pathcost 80
        #fear 10
        #floating
        #heal
        #startdom 2

        #itemslots 274560 --[262144 + 128 + 12288] - head, 2 misc, head only crown

        #clearweapons
        #weapon 63 -- Life drain
        #weapon 20 -- Bite
        #weapon 274 -- Enslave mind
        #weapon 86 -- Mindblast


        #hp 65
        #size 5
        #prot 8
        #mor 30
        #mr 22
        #str 17
        #att 13
        #def 12
        #prec 12
        #mapmove 12
        #ap 10
        #enc 2

        #shockres -7
        #float
        #commaster
        #magicbeing
        #magicpower 1
        #amphibian
        #voidsanity 15
        #spiritsight
        #bonusspells 2
        #magicskill 4 3
        #end
    -- Dracobolith -----------------------------------------------------------

        #newmonster 7011
        #name "Dracobolith"
        #spr1 "./bozmod/Monsters/Warpman/Dracobolith.tga"
        #spr2 "./bozmod/Monsters/Warpman/Dracobolith_attack.tga"
        #descr "Aboleth have been always fascinated by the life above waves, and have frequently taken control over minds of creatures above to observe the land they could not grasp.
        Among the creatures above the waves they found the Dragons, majestic creatures that had intellect that rivaled theirs yet had the bodies Aboleth themselves could only dream of ever having.
        Obsessed with the idea of obtaining the perfect body a mind lord of epic skill in grafting began experimenting on bodily augmentations to reach draconic perfection, so that he would be a merge of a perfect mind and a perfected body. 
        Having attained this would mean transcending to Godhood, for what else could one dream of."
        #gcost 210
        #diseaseres 100
        #pathcost 80
        #fear 10
        #dragonlord 1
        #heal
        #startdom 2

        #clearweapons
        #weapon 269 -- Soul Leech
        #weapon 609 -- grab and swallow
        #weapon 86 -- Mindblast

        #itemslots 274560 --[262144 + 128 + 12288] - head, 2 misc, head only crown

        #hp 230
        #size 6
        #prot 4
        #mor 30
        #mr 22
        #str 16
        #att 12
        #def 10
        #prec 12
        #mapmove 22
        #ap 10
        #enc 3

        #aquatic
        #bonusspells 1
        #bluntres
        #voidsanity 10
        #pathcost 80
        #magicskill 4 1
        #magicskill 2 1
        #end
    -- King of Knights ------------------------------------------------------------

        #newmonster 7012
        #name "King of knights"
        #descr "Once but a petty king, decades of arduous work have helped establish the order of knights that protect the lands from all kinds of monsters, demons, tyrants and walking dead alike.
        With lands finally secure and people cheering for your every deed there is nothing left to do but take your most trusted brothers in arms and proclaim the great crusade against the unclean anew.
        Armed with a lance that brings down tyrants and sword that banishes their lies and leaves no place to hide there is nothing more to fear and nothing more to strive for but a better world and your just rule."
        #spr1 "./bozmod/Monsters/Warpman/King_Knight.tga"
        #spr2 "./bozmod/Monsters/Warpman/King_Knight_attack.tga" 
        #pathcost 80
        #triplegod 1
        #triplegodmag 2
        #triple3mon
        #minprison 0
        #maxprison 1
        #gcost 80
        #hp 25
        #str 13
        #att 15
        #def 15
        #prec 15
        #prot 0
        #size 3
        #mr 15
        #mor 20
        #enc 2
        #mountedhumanoid
        #mounted
        #mapmove 25
        #ap 20
        #eyes 2
        #weapon "Tyrant's bane"
        #weapon "Judgement"
        #armor 201
        #armor 21
        #armor 57
        #weapon 56
        #maxage 1000
        #woundfend 100
        #dompower 1
        #expertleader
        #magicskill 0 2
        #magicskill 2 2
        #magicskill 6 2
        #magicboost 53 -5
        #researchbonus -15
        #disbelieve 25
        #batstartsum1d3 22
        #batstartsum1d3 2359
        #homerealm 3 --mediterranean
        #end
        -- Valiant Knight ------------------------------------------------------------

            #newmonster 7013
            #name "Valiant knight"
            #descr "Once but a squire, tough training regimen, just heart and cold head have let you become a champion of order, the bane of undying and other walking dead.
            With your king calling for a great crusade against all that is evil you can stand no longer.
            Armed with Star of Time, a morningstar that can banish souls of the unliving for good, you are the bastion of hope for all the peasants and citizens that can finally sleep with no fear of ghoul or ghost."
            #spr1 "./bozmod/Monsters/Warpman/Green_knight.tga"
            #spr2 "./bozmod/Monsters/Warpman/Green_Knight_attack.tga" 
            #hp 25
            #str 13
            #att 16
            #def 16
            #prec 15
            #prot 0
            #size 3
            #mr 15
            #mor 20
            #enc 2
            #mountedhumanoid
            #mounted
            #mapmove 25
            #ap 20
            #eyes 2
            #weapon "Star of time"
            #armor 201
            #armor 21
            #armor 57
            #weapon 56
            #maxage 1000
            #woundfend 100
            #dompower 1
            #inspirational 2
            #okleader
            #magicskill 0 2
            #magicskill 2 2
            #magicskill 6 2
            #magicboost 53 -5
            #researchbonus -15
            #batstartsum1d3 22
            #end
        -- Just Knight ------------------------------------------------------------

            #newmonster 7014
            #name "Just knight"
            #descr "Once but an old garrison commander, your vigilance and patience have let you become more than just a champion of the order, but also a great judge of character, seeing evil just by looking in the eyes of a human. Demon worshippers have been slain by dozens by your hand, and they say that any sword you have is imbued with just power to smite daemonic.
            With your king calling for a great crusade against all that is evil you can stand no longer.
            Though armed with but a sword, daemons reel from your very touch, as if burnt by holy water."
            #spr1 "./bozmod/Monsters/Warpman/Red_knight.tga"
            #spr2 "./bozmod/Monsters/Warpman/Red_Knight_attack.tga" 
            #hp 28
            #str 13
            #att 15
            #def 15
            #prec 16
            #prot 0
            #size 3
            #mr 15
            #mor 20
            #enc 2
            #mountedhumanoid
            #mounted
            #mapmove 25
            #ap 20
            #eyes 2
            #weapon "Just man's sword"
            #armor 201
            #armor 21
            #armor 57
            #weapon 56
            #maxage 1000
            #woundfend 100
            #dompower 1
            #goodleader
            #magicskill 0 2
            #magicskill 2 2
            #magicskill 6 2
            #magicboost 53 -5
            #researchbonus -15
            #incprovdef 1
            #batstartsum1d3 22
            #end
        -- Special Weapons
            #newweapon
            #name "Tyrant's bane"
            #dmg 8
            #att 2
            #len 4
            #sound 89
            #dt_large
            #pierce
            #armorpiercing
            #bonus
            #charge
            #norepel
            #magic
            #end


            #newweapon 
            #name "Judgement"
            #dmg 8
            #att 1
            #len 2
            #sound 8
            #slash
            #unrepel
            #end

            #newweapon 1602
            #name "Star of time"
            #dmg 8
            #att 1
            #len 2
            #sound 8
            #blunt
            #flail
            #magic
            #dt_holy
            #secondaryeffect 1603
            #end

            #newweapon 1603
            #name "Final rest"
            #dmg 10
            #aoe 1
            #dt_aff 49
            #mrnegates
            #undeadonly
            #end

            #newweapon 1604
            #name "Just man's sword"
            #dmg 8
            #att 1
            #len 2
            #sound 8
            #slash
            #dt_holy
            #magic
            #secondaryeffect 1605
            #end

            #newweapon 1605
            #name "Touch of justice"
            #dmg 10
            #aoe 1
            #dt_aff 16
            #demononly
            #end
    -- Slayer of Apostles ------------------------------------------------------------

        #newmonster 7015
        #name "Slayer of apostles"
        #descr "A former lictor of early Ermorian history, Hector saw the turbulent times of the reborn god, the rising and culling of cults all around the empire and the coming of the true Pretender God.
        More fake apostles and pathetic prophets have ben culled by his hand than any other, and so when his old and broken body was interred in a family tomb his spirit reeled against the injustices of fake apostles still breathing.
        His ghostly form still riding a horse has been sighted many times afterwards, long face ever stern, sword of justice in his hand, his arm outstreched as if clutching for the neck of another prophet.
        His mere presence causes false pretenders reel, his grasp paralyzes them with fear and his blade causes even the strong-willed priests faint. Let it be known that nothing holy can sleep tight, for the hunt never ended, and horns shaped in twisted visage of laurels shall be the last thing they see."
        #spr1 "./bozmod/Monsters/Warpman/Death_knight.tga"
        #spr2 "./bozmod/Monsters/Warpman/Death_Knight_Attack.tga" 
        #pathcost 80
        #triplegod 1
        #triplegodmag 2
        #triple3mon
        #minprison 0
        #maxprison 1
        #gcost 120
        #hp 25
        #str 12
        #att 16
        #def 14
        #prec 16
        #prot 2
        #size 3
        #mr 14
        #mor 30
        #enc 0
        #mountedhumanoid
        #mounted
        #mapmove 35
        #ap 35
        #eyes 2
        #weapon 510
        #weapon 477
        #armor 116
        #armor 148
        #weapon 56
        #maxage 1000
        #woundfend 100
        #goodleader
        #goodundeadleader
        #magicskill 4 2
        #magicskill 5 2
        #magicskill 7 2
        #magicboost 53 -5
        #researchbonus -15
        #undead 
        #poisonres 50
        #coldres 15
        #ethereal
        #haltheretic 3
        #fear 5
        #incscale 0
        #batstartsum1d6 566
        #batstartsum1d3 1541
        #batstartsum1d3 3067
        #end
        -- Bringer of Blight ------------------------------------------------------------
            #newmonster 7016
            #name "Bringer of Blight"
            #descr "Not much is known of where this pale rider comes from, what was his name in life or what battles he fought, for the only thing that remains afterwards is blighted countryside, rotting corpses and starving cattle dying next to the road.
            Malign spirit cares not for the shell, changing one corpse for another, as long as his trail of blighted conquest continues there will never be a shortage of bodies.
            Bringer of blight is often known to carry an axe of executioner, and a bow that spreads putrid smoke in all four directions."
            #spr1 "./bozmod/Monsters/Warpman/Blight_Knight.tga"
            #spr2 "./bozmod/Monsters/Warpman/Blight_Knight_Attack.tga" 
            #hp 25
            #str 12
            #att 16
            #def 14
            #prec 16
            #prot 2
            #size 3
            #mr 16
            #mor 30
            #enc 2
            #mountedhumanoid
            #mounted
            #mapmove 25
            #ap 20
            #eyes 2
            #weapon 259
            #armor 33
            #armor 118
            #weapon 56
            #weapon "The Wind"
            #maxage 1000
            #woundfend 100
            #spiritsight
            #undead 
            #poisonres 50
            #coldres 5
            #okleader
            #goodundeadleader
            #magicskill 4 2
            #magicskill 5 2
            #magicskill 7 2
            #magicboost 53 -5
            #researchbonus -15
            #leper 5
            #popkill 10
            #pillagebonus 50
            #incscale 3
            #end
        -- Eraser of Magic ------------------------------------------------------------

            #newmonster 7017
            #name "Eraser of magic"
            #spr1 "./bozmod/Monsters/Warpman/Eraser.tga"
            #spr2 "./bozmod/Monsters/Warpman/Eraser_attack.tga"
            #descr "The one known as Eraser of magic was once a tyrant of the Ether clan, and his exploits at conquering all the lands of Arcana are widely known as folklore of the clans, alas spoken in hushed tones for he is not viewed as hero any more.
            It was by his blade that the fae were cut down and drained of the magic they had, it was by his decree that blood seekers rode out in the night, stealing girls from their families to later on feed the war machine, it was his banner that flied atop the last fort of the dragon lords.
            Arcana lies barren and drained by his steel fist, and now he claims to be a god.
            With his enormous strength he can strike with a moonblade single-handed, and his huge stallion has trampled more soldiers than many saw in their lifetime."
            #hp 35
            #size 4
            #prot 3
            #mor 16
            #mr 20
            #str 17
            #att 16
            #def 15
            #prec 15
            #mapmove 23
            #ap 25
            #enc 1
            #armor 18
            #armor 118
            #armor 73
            #mounted
            #weapon 289
            #weapon 615
            #magicbeing
            #spiritsight
            #magicpower 1
            #ethereal
            #magicskill 4 2
            #magicskill 5 2
            #magicskill 7 2
            #magicboost 53 -5
            #researchbonus -15
            #woundfend 100
            #drainimmune
            #goodleader 
            #expertmagicleader
            #incunrest 10
            #maxage 700
            #incscale 5
            #end
        -- special weapons
            #newweapon 
            #name "Putrid smoke"
            #secondaryeffectalways 50
            #aoe 1
            #dmg 5
            #sound 50
            #dt_poison
            #dt_raise 
            #explspr 10041
            #end

            #newweapon 
            #name "The Wind"
            #secondaryeffectalways "Putrid smoke"
            #dmg 12
            #range 35
            #prec -5
            #nratt 4
            #pierce 
            #magic 
            #bowstr
            #twohanded
            #ammo 10
            #flyspr 419
            #nouw
            #end 
    -- God Emperor of the Dunes ------------------------------------------------------------

        #newmonster 7018
        #name "Emperor of the Sands"
        #spr1 "./bozmod/Monsters/God_of_Sands.tga"
        #spr2 "./bozmod/Monsters/God_of_Sands_Attack.tga"
        #descr "The Sand Emperor was once a trusted advisor of the previous Pantokrator. He is gifted with the ability to commune with the sands and from their whispers, he can peer into the future to avoid danger, or make decisions knowing that their outcomes will bring great fortune. Some say that the Pantokrator's disappearance had something to do with the advise that was given to him by the Sand Emperor - nobody truly knows except for the Sand Emperor, who now has obligations to nobody. Having nothing higher left to obtain than godhood, the Sand Emperor has donned the title of a god and seeks to become the new Pantokrator."

        #pathcost 30
        #gcost 180
        #hp 230
        #heal
        #size 6
        #prot 16
        #mor 30
        #mr 22
        #str 23
        #att 5
        #def 2
        #prec 16
        #mapmove 8
        #ap 6
        #enc 3
        #weapon 532 -- tail sweep
        #magicbeing
        #spiritsight
        #magicskill 3 1
        #magicskill 4 2
        #slothresearch 4
        #expertleader
        #expertmagicleader
        #maxage 7000
        #curseluckshield 1
        #awe 1
        #bringeroffortune 30
        #diseaseres 100
        #nobadevents 30
        #spellsinger
        #trample
        #slothpower 2
        #regeneration 5
        #noriverpass
        #fireres 10
        #shockres 8
        #coldres -7
        #luck
        #uwdamage 100
        #bluntres
        #itemslots 1
        #homerealm 10
        #startdom 3
        #end
    -- Holy Dragon ------------------------------------------------------------

        #newmonster 7019
        #name "Dragon of Blinding Virtue"
        #spr1 "./bozmod/Monsters/Warpman/LightDragon_1.tga"
        #spr2 "./bozmod/Monsters/Warpman/LightDragon_2.tga"
        #descr "Aeons before the ascension wars the dragon of virtue was a beacon of light and a moral compass for many. He was a humble creature who never had ambitions other than to serve and help bring peace to more people. When a Pantokrator arose, the dragon of virtue was an impediment to the absolute authority of the pantokrator who wanted himself to be the only moral beacon of others. After criticizing the pantokrators' judgement one times too many, the pantokrator's patience ran out and he blinded the dragon for his insolence, saying if he cannot bear to see him bestow judgements then he shall not see at all.

        Now the Pantokrator is gone, the dragon of virtue has determined that only through his leadership can the greatest harmony and peace be achieved. He has donned the title of god and followers flock to his cause. The dragon of virtue is pale, his eyes gone by the decree of the Pantokrator and his scales as magnificent as they are brittle. Though almost incapable of fighting, Dragon of virtue is incredibly powerful where the sun touches his realm.
        Taking this chassis additionally gives you Beacon of Light site in your capital, giving your blessed units additional +2 Awe bonus. Enemies taking the site will get the reduced +1 bonus instead."
        #hp 105
        #gcost 210
        #size 6
        #prot 14
        #mor 30
        #mr 20
        #str 22
        #att 13
        #def 9
        #prec 5
        #mapmove 25
        #ap 10
        #enc 1
        #weapon 532
        #weapon 48
        #flying
        #magicbeing
        #blind
        #dragonlord 1
        #sunawe 3
        #dompower 2
        #magicskill 4 1
        #magicskill 0 1
        #magicskill 8 1
        #magicboost 8 -1
        #goodleader 
        #expertmagicleader
        #maxage 700
        #decscale 2
        #fireres 10
        #heat 4
        #eyeloss
        #shapechange 7020
        #startdom 2
        #diseaseres 100
        #homerealm 3 --mediterranean
        #itemslots 274560 --[262144 + 128 + 12288] - head, 2 misc, head only crown
        #pathcost 80
        #end

        #newmonster 7020
        #name "Harbinger of Virtue"
        #spr1 "./bozmod/Monsters/Warpman/BlindDude1.tga"
        #spr2 "./bozmod/Monsters/Warpman/BlindDude_2.tga"
        #descr "Aeons before the ascension wars the dragon of virtue was a beacon of light and a moral compass for many. He was a humble creature who never had ambitions other than to serve and help bring peace to more people. When a Pantokrator arose, the dragon of virtue was an impediment to the absolute authority of the pantokrator who wanted himself to be the only moral beacon of others. After criticizing the pantokrators' judgement one times too many, the pantokrator's patience ran out and he blinded the dragon for his insolence, saying if he cannot bear to see him bestoy judgements then he shall not see at all.

        Now the Pantokrator is gone, the dragon of virtue has determined that only through his leadership can the greatest harmony and peace be achieved. He has donned the title of god and followers flock to his cause.

        In his human form the dragon of virtue takes the shape of an old blind priest, and while even less capable of fighting, his oratory skills inspire people to greater deeds when they have no fear of his blinding radience."
        #hp 8
        #size 2
        #prot 0
        #mor 30
        #mr 20
        #str 6
        #att 3
        #def 3
        #prec 3
        #mapmove 14
        #ap 5
        #enc 1
        #weapon 48
        #blind
        #sunawe 1
        #dompower 3
        #magicskill 4 1
        #magicskill 0 1
        #magicboost 8 4
        #spreaddom 2
        #magicboost 53 -2
        #goodleader 
        #expertmagicleader
        #maxage 700
        #fireres 5
        #eyeloss
        #shapechange 7019
        #diseaseres 100
        #end   

        #selectitem 501
        #name "Burning halo"
        #constlevel 12
        #copyspr 377
        #type 9
        #autospell "Stellar Cascades"
        #autospellrepeat 2 
        #mainpath 0
        #mainlevel 5
        #nofind
        #cursed
        #end

        #newsite 1405
        #name "Beacon of Light"
        #path 4
        #level 4
        #loc 512
        #rarity 5
        #blessawe 2
        #dominion 2
        #end

        #newsite 1406
        #name "Unlit Beacon"
        #path 4
        #level 1
        #loc 512
        #rarity 5
        #blessawe 1
        #dominion 1
        #end

        #newevent
        #rarity 5
        #req_rare 100
        #req_monster 7019
        #req_targmnr 7019 
        #req_targnoitem 501 -- the halo
        #nolog
        #notext
        #addequip 9
        #msg "Solar halo engulfed Dragon of Virtue [Burning halo]"
        #end


        #newevent
        #rarity 0
        #req_pregame 1
        #req_owncapital 1
        #req_godismnr  7019
        #addsite 1405
        #notext
        #nolog
        #end

        #newevent
        #rarity 0
        #req_capital 1
        #req_owncapital 0
        #req_site 1
        #removesite 1405
        #addsite 1904
        #msg "The beacon of light no longer shines as it used ti, though you still can use remnants [Beacon of Light]"
        #end

        #newevent
        #rarity 0
        #req_owncapital 1
        #req_godismnr "Harbinger of Virtue"
        #req_site 1
        #removesite 1904
        #addsite 1405
        #msg "The beacon of light no longer shines as it used to, though you still can use remnants [Unlit Beacon]"
        #end
    -- Queen of Crustaceans ------------------------------------------------------------
        #newmonster 7021    
        #name "Queen of Crustaceans"
        #spr1 "./bozmod/Monsters/queen_crustacean.tga"
        #spr2 "./bozmod/Monsters/queen_crustacean2.tga"
        #descr "The queen of crustaceans is an ageless primordial entity that existed in a time when all living things were of the ocean. Some say she is the proginator of the Atlantians, others say she is just a legend. Her presence carries the weight of eons and animals are in awe when they see her fearsome visage. She has taken the title of a god to clear the world of the other pretenders and become the new Pantokrator. When she is in friendly dominion, she will attract various shrimps to join her in her cause."

        #pathcost 80
        #gcost 240
        #hp 115
        #heal
        #size 6
        #prot 16
        #mor 30
        #mr 15
        #str 22
        #att 14
        #def 10
        #prec 8
        #mapmove 8
        #ap 7
        #enc 2
        #weapon 532 -- tail sweep
        #weapon 392 -- torch of strife
        #weapon 1606 -- shrimp punch
        #magicskill 6 1
        #magicskill 4 1
        #magicskill 2 1
        #expertleader
        #maxage 70000
        #animalawe 2
        #diseaseres 100
        #shockres -4
        #coldres 5
        #naga
        #aquatic
        #homerealm 9
        #startdom 3
        #domsummon 2369 --large shrimp
        #domsummon2 5909 --shrimp warrior
        #domsummon20 "Sea Killer"
        #batstartsum4d6 2369
        #end

        #newweapon 1606
        #name "Shrimp Punch"
        #copyweapon 1575
        #bonus
        #end
    -- Senator Strongarm ------------------------------------------------------------


        #newweapon
        #copyweapon 562
        #name "Iron Fist"
        #iron
        #end

        #newmonster 7035 --senator
        #hp 42
        #name "Senator Strongarm"
        #size 3
        #descr "Senator Strongarm has always been keen to expand the Ermorian empire, always seeking ways to make Ermor a singular superpower, both military and economically.
        His insights into arcane and magic of metals have led him to become what he is, many say that it is impossible to pierce his skin, something many political opponents tried in vain over the years.
        With the prohpet shrouded in white came a period of peace and prosperity, something anathema to The Senator, for he believes war to be the one true driver of the economy.
        His domain is in a constant state of improvement, for he is restless in his desire for Ermor to become the god-nation, claiming Imperium once and for all."
        #spr1 "./bozmod/Monsters/Warpman/Senator1.tga"
        #spr2 "./bozmod/Monsters/Warpman/Senator2.tga"
        #att 12
        #def 10
        #str 16
        #prec 11
        #prot 2
        #mr 21
        #weapon "Iron Fist"
        #weapon "Iron Fist"
        #magicskill 3 1
        #magicskill 4 1
        #pathcost 20
        #startdom 1
        #homerealm 3
        #gcost 180
        #enc 1
        #mapmove 16
        #ap 12
        #dompower 2
        #startage 180
        #maxage 600
        #heal
        #fireres 5
        #poisonres 5
        #slashres
        #incprovdef 5
        #shrinkhp 20
        #okleader 
        #command 100
        #inspirational -1
        #researchbonus -25
        #onebattlespell "Ironskin"
        #end

        #newmonster 7036 --senator
        #hp 84
        #name "Wounded Senator"
        #size 3
        #descr "Senator Strongarm has always been keen to expand the Ermorian empire, always seeking ways to make Ermor a singular superpower, both military and economically.
        His insights into arcane and magic of metals have led him to become what he is, many say that it is impossible to pierce his skin, something many political opponents tried in vain over the years.
        With the prohpet shrouded in white came a period of peace and prosperity, something anathema to The Senator, for he believes war to be the one true driver of the economy.
        His domain is in a constant state of improvement, for he is restless in his desire for Ermor to become the god-nation, claiming Imperium once and for all.
        When suffering massive trauma Senator can go on the defensive, healing wounds but getting too stiff to move fast or attack effectively.
        
        The Senator provides a 1% per candle chance to add a tiny amount of gold income, or resources, per province per turn, these events happen silently."
        #spr1 "./bozmod/Monsters/Warpman/SenatorW1.tga"
        #spr2 "./bozmod/Monsters/Warpman/SenatorW2.tga"
        #att 8
        #def 15
        #str 20
        #prec 11
        #prot 10
        #mr 23
        #weapon "Iron Fist"
        #weapon "Iron Fist"
        #magicskill 3 1
        #magicskill 4 1
        #gcost 120
        #enc 3
        #mapmove 16
        #ap 3
        #dompower 2
        #startage 180
        #maxage 600
        #heal
        #regeneration 10
        #fireres 15
        #poisonres 10
        #bluntres
        #slashres
        #incprovdef 5
        #growhp 21
        #okleader 
        #fear 5
        #inspirational -3
        #researchbonus -25
        #onebattlespell "Ironskin"
        #end


        #newevent 
        #nation -2
        #rarity 5
        #req_dominion 1
        #req_domchance 1
        #req_godismnr 7035
        #req_order 1
        #msg "Local populace finally embraced the rule of law and order to their heart"
        #landgold 1
        #nolog
        #end

        #newevent 
        #rarity 5
        #req_dominion 1
        #req_domchance 1
        #req_godismnr 7035
        #req_order 2
        #msg "Local populace finally embraced the rule of law and order to their heart 2"
        #nolog
        #landgold 2
        #end

        #newevent 
        #rarity 5
        #req_dominion 1
        #req_domchance 1
        #req_godismnr 7035
        #req_order 3
        #msg "Local populace finally embraced the rule of law and order to their heart 2"
        #nolog
        #landgold 2
        #end

        #newevent 
        #nation -2
        #rarity 5
        #req_dominion 1
        #req_domchance 1
        #req_godismnr 7035
        #req_mintroops 50
        #msg "Local garrison is improving the local economy"
        #nolog
        #landgold 1
        #end

        #newevent 
        #nation -2
        #rarity 5
        #req_dominion 1
        #req_domchance 1
        #req_godismnr 7035
        #req_mintroops 50
        #msg "Local garrison is improving the local economy"
        #nolog
        #landprod 1
        #end

        #newevent 
        #nation -2
        #rarity 5
        #req_dominion 1
        #req_domchance 1
        #req_godismnr 7035
        #req_prod 1
        #msg "Local populace finally embraced the rule of law and order to their heart"
        #nolog
        #landprod 1
        #end

        #newevent 
        #rarity 5
        #nation -2
        #req_dominion 1
        #req_domchance 1
        #req_godismnr 7035
        #req_prod 2
        #msg "Local populace finally embraced the rule of law and order to their heart 2"
        #nolog
        #landprod 2
        #end

        #newevent 
        #rarity 5
        #nation -2
        #req_dominion 1
        #req_domchance 1
        #req_godismnr 7035
        #req_prod 3
        #msg "Local populace finally embraced the rule of law and order to their heart 2"
        #nolog
        #landprod 2
        #end
-- Items 
    -- New Items
        #selectitem 502
        #spr "./bozmod/items/sparrow.tga"
        #name "Statuette of Silver Sparrows"
        #descr "This silver statuette is used to attract spring hawks. It has a serene beauty to and is prized by thieves"
        #mainpath 1
        #type 8
        #mainlevel 4
        #constlevel 6
        #makemonsters2 513 -- Spring Hawk
        #end

        #selectitem 507
        #spr "./bozmod/items/transfusionvial.tga"
        #name "Mutator Blood"
        #descr "This mixture of magical microorganisms and blood adapts to the parameters of it's hosts blood, which is very useful for keeping more and better crossbreeds alive. After doing the horrific acts that result in the creation of misbreds, many that would otherwise die, could be saved using the blood in this vial as a transfusion. By keeping a small amount inside the vial and adding fresh blood from the user, the microorganisms multiply and effectively refresh the properties of the blood inside, allowing it to be used again."
        #mainpath 7
        #mainlevel 3 
        #type 8
        #crossbreeder 10
        #constlevel 6
        #end


        #selectitem 508
        #spr "./bozmod/items/firetome.tga"
        #name "Tome of Fire"
        #descr "A fire mage of great power has enscribed their knowledge of pyromancy in this book, the condensed magical knowledge on it's arcane pages gives the book a life of it's own and this life can be transferred into an elemental of fire, which spawns next to the user at the start of each battle. Additionally, having all their knowledge written down allows for more efficient use of fire magic and even correlating knowledge to find secrets previously hidden in plain sight."
        #mainpath 0
        #mainlevel 6 
        #batstartsum1 594 --big fire elemental
        #magicboost 0 1
        #type 8
        #constlevel 6
        #end

        #selectitem 509
        #spr "./bozmod/items/airtome.tga"
        #name "Tome of Air"
        #descr "An air mage of great power has enscribed their knowledge of auralmancy in this book, the condensed magical knowledge on it's arcane pages gives the book a life of it's own and this life can be transferred into an elemental of air, which spawns next to the user at the start of each battle. Additionally, having all their knowledge written down allows for more efficient use of air magic and even correlating knowledge to find secrets previously hidden in plain sight."
        #mainpath 1
        #mainlevel 6 
        #batstartsum1 567 --big air elemental
        #magicboost 1 1
        #type 8
        #constlevel 6
        #end

        #selectitem 510
        #spr "./bozmod/items/watertome.tga"
        #name "Tome of Water"
        #descr "A water mage of great power has enscribed their knowledge of aquamancy in this book, the condensed magical knowledge on it's arcane pages gives the book a life of it's own and this life can be transferred into an elemental of water, which spawns next to the user at the start of each battle. Additionally, having all their knowledge written down allows for more efficient use of water magic and even correlating knowledge to find secrets previously hidden in plain sight."
        #mainpath 2
        #mainlevel 6
        #batstartsum1 408 --big water elemental
        #magicboost 2 1
        #type 8
        #constlevel 6
        #end

        #selectitem 511
        #spr "./bozmod/items/earthtome.tga"
        #name "Tome of Earth"
        #descr "An earth mage of great power has enscribed their knowledge of geomancy in this book, the condensed magical knowledge on it's arcane pages gives the book a life of it's own and this life can be transferred into an elemental of earth, which spawns next to the user at the start of each battle. Additionally, having all their knowledge written down allows for more efficient use of earth magic and even correlating knowledge to find secrets previously hidden in plain sight."
        #mainpath 3
        #mainlevel 6
        #batstartsum1 493 --big earth elemental
        #magicboost 3 1
        #type 8
        #constlevel 6
        #end
    -- Changed Items
        #selectitem 297 --soul contract
        #constlevel 6
        #itemcost1 340
        #itemcost2 20
        #end

        #selectitem "Rune Stone Uruz"
        #constlevel 6
        #itemcost1 250
        #end
-- Spells
    -- Changed Spells        
        -- Summons
            #selectspell "Revive Lemur Consul"
            #researchlevel 3
            #end

            #selectspell "Revive Lemur Acolyte"
            #fatiguecost 700
            #end

            #selectspell "Revive Acolyte"
            #fatiguecost 700
            #end

            #selectspell "Revive Grand Lemur"
            #researchlevel 4
            #end

            #selectspell "Contact Boar of Carnutes"
            #fatiguecost 1500
            #researchlevel 6
            #end

            #selectspell "Enchant Morgen Wraith"
            #fatiguecost 2000
            #researchlevel 6
            #end

            #selectspell "Summon Rainbow Serpent"
            #fatiguecost 15000
            #end

            #selectspell "Summon Rainbow Serpent"
            #fatiguecost 15000
            #end
        -- Globals
            #selectspell "Burden of Time"
            #clear
            #end

            #selectspell "Magia Mortuus"
            #clear
            #end

            #selectspell "Arcane Nexus"
            #clear
            #end

            #selectspell "Astral Corruption"
            #clear
            #end

            #selectspell "Acid Seas"
            #clear
            #end

            #selectspell "Utterdark"
            #clear
            #end

            #selectspell "Poison Earth"
            #clear
            #end

            #selectspell "Blasphemy"
            #clear
            #end

            #selectspell "Rise of the Insects"
            #clear
            #end

            #selectspell "The Looming Hell"
            #fatiguecost 21000
            #end

            #selectspell "Abyssal Gate"
            #fatiguecost 16000
            #end

            #selectspell "Malediction"
            #fatiguecost 15000
            #end

            #selectspell "Illwinter"
            #fatiguecost 28000
            #end

            #selectspell "Blood Harvest"
            #fatiguecost 12000
            #end

            #selectspell "Solomon's Demise"
            #clear
            #end

            #selectspell "Enchanted Forests"
            #fatiguecost 12000
            #end

            #selectspell "Sea of Ice"
            #clear
            #end

            #selectspell "Second Sun"
            #fatiguecost 12000
            #end

            #selectspell "Release the Nosoi"
            #clear
            #end

            #selectspell "Thaw the Ancient Ones"
            #fatiguecost 19000
            #end

            #selectspell "End the Deep Slumber"
            #fatiguecost 12000
            #end

            #selectspell "Rivers of Lava"
            #fatiguecost 12000
            #end

            #selectspell "Foul Air"
            #fatiguecost 15000
            #end

            #selectspell "Gift of Nature's Bounty"
            #fatiguecost 16000
            #end

            #selectspell "End of Days"
            #clear
            #end

            #selectspell "Riches from Beneath"
            #fatiguecost 12000
            #end

            #selectspell "Theft of the Sun"
            #clear
            #end

            #selectspell "Perpetual Storm"
            #clear
            #end

            #selectspell "Celestial Rainbow"
            #fatiguecost 16000
            #end

            #selectspell "Dreamtime"
            #fatiguecost 12000
            #end

            #selectspell "Lunar Potency"
            #fatiguecost 12000
            #end

            #selectspell "Gaia's Vengeance"
            #fatiguecost 12000
            #end

            #selectspell "Spreading Miasma"
            #fatiguecost 12000
            #end

            #selectspell "Ragnarok"
            #fatiguecost 12000
            #end

            -- the stars are right
            #selectspell 2352
            #fatiguecost 12000
            #end

            -- the stars are right
            #selectspell 2351
            #fatiguecost 12000
            #end

            #selectspell "Samhain"
            #fatiguecost 12000
            #end

            #selectspell "Lure of the Deep"
            #fatiguecost 11000
            #end

            #selectspell "Thetis' Blessing"
            #fatiguecost 12000
            #end

            #selectspell "Read the Stars"
            #fatiguecost 15000
            #end

            #selectspell "Thief of Fortune"
            #fatiguecost 12000
            #end

            #selectspell "Gift of Health"
            #fatiguecost 12000
            #end

            #selectspell "Oceanian Assault"
            #fatiguecost 12000
            #end


            #selectspell "Doom from the Heavens"
            #fatiguecost 8000
            #end

            #selectspell "Whispering Winds"
            #fatiguecost 14000
            #end

            #selectspell "Soul Gate"
            #fatiguecost 14000
            #end

            #selectspell "Gates of Death"
            #fatiguecost 14000
            #end

            #selectspell "Masters of the Sea"
            #clear
            #end

            -- whispers of the Rlyea
            #selectspell "Whispers of R'lyeh"
            #fatiguecost 12000
            #end

            #selectspell "Manna from the Heavens"
            #fatiguecost 12000
            #end

            #selectspell "The Wrath of God"
            #fatiguecost 12000
            #end

            #selectspell "The Wrath of the Titans"
            #fatiguecost 12000
            #end

            #selectspell "Dispel"
            #fatiguecost 8000
            #end

            #selectspell "Arcane Dispel"
            #fatiguecost 6000
            #end

            #selectspell "Greater Dispel"
            #fatiguecost 4000
            #end
        -- Other Rituals
            #selectspell "Stygian Paths"
            #fatiguecost 1200
            #researchlevel 5
            #end

            #selectspell "Faery Trod"
            #fatiguecost 1200
            #researchlevel 6
            #end
    -- New Spells
        -- Summons
            -- Summon Forest Avenger
                #newspell
                #name "Summon Forest Avenger"
                #descr "While most white minotaurs become warrior priests, many opt to simply become warriors, though they still possess an innate connection with the forests and giving them further training to become a commander will ignite their priestly powers"
                #school 0
                #researchlevel 3
                #path 0 3
                #pathlevel 0 2
                #path 1 6
                #pathlevel 1 1
                #fatiguecost 300
                #damagemon "White Minotaur"
                #effect 10001
                #nreff 1
                #restricted 58 --ma pan
                #restricted 16 --ea pan
                #restricted 59 --ma asphodel
                #end
            -- Avengers of the Forest
                #newspell
                #name "Avengers of the Forest"
                #descr "While most white minotaurs become warrior priests, many opt to simply become warriors, though they still possess an innate connection with the forests and giving them further training to become a commander will ignite their priestly powers. This spell summons a large contingent of white minotaurs"
                #school 0
                #researchlevel 8
                #path 0 3
                #pathlevel 0 4
                #path 1 6
                #pathlevel 1 2
                #fatiguecost 1500
                #damagemon "White Minotaur"
                #effect 10001
                #nreff 1001
                #restricted 58 --ma pan
                #restricted 16 --ea pan
                #restricted 59 --ma asphodel
                #end
            -- Disciple of Krom 
                #newspell
                #name "Call Disciple of Krom"
                #descr "Summons a Barbarian priest king, with a greatsword of sharpness, horned helmet, belt of giant strength and chi boots. The presence of a barbarian king creates unrest and attracts other barbarians to join the pretender god, however warring chieftans of other barbarian chiefs will occasionally attack him to usurp his rule. The Disciples of Krom, do not use any items except those holy in their faith"
                #school 0
                #researchlevel 6
                #path 0 3
                #pathlevel 0 4
                #fatiguecost 4500
                #damagemon "Disciple of Krom"
                #effect 10021
                #nreff 1
                #end

                #newmonster 7022
                #name "Disciple of Krom"
                #spr1 "./bozmod/Monsters/barbarian_king.tga"
                #spr2 "./bozmod/Monsters/barbarian_king_attack.tga"
                #descr "The Disciple of Krom is a barbarian king who worships steel and war. He attracts other barbarians to join him in his servitude to the pretender god, however rival barbarian chiefs will seek to usurp the king and gain the pretenders' favor"
            
                #hp 42
                #size 4
                #prot 6
                #mor 30
                #mr 14
                #str 20
                #att 18
                #def 12
                #prec 10
                #mapmove 14
                #ap 14
                #enc 2
                #weapon 108 -- greatsword of sharness
                #weapon 334 -- magic gore
                #weapon 175 --chi kick
                #itemslots 1
            
                #magicskill 8 2
                #expertleader
                #diseaseres 50
                #shockres 3
                #startage 30
                #coldres 3
                #reinvigoration 3
                #fireres 3
                #poisonres 3
                #inspirational 1
                #taskmaster 1
                #holy
                #incunrest 30
                #domsummon 139 --large shrimp
                #batstartsum1d6 139
                #batstartsum1d6 140
                #end
            
                #newevent
                #rarity 1
                #req_monster "Disciple of Krom"
                #req_targmnr "Disciple of Krom"
                #assassin 7023
                #end
            
                ---------- USURPING BARBARIAN --------------
            
                #newmonster 7023
                #name "Usurping Barbarian King"
                #copyspr 147
                #descr "This barbarian king rejects the rule of the Disciple of Krom and has managed to find an opportunity to make an attempt on his life"
                #copystats 147
                #batstartsum1d6 139
                #batstartsum1d6 140
                #end
            -- Discover Forest Grove
                #newspell
                #name "Discover Forest Grove"
                #onlygeosrc 128
                #descr "Caster ventures into the forest to enlist the help city of sprites. Sprites tend to live inside a forest grove, hidden from sight unless magical means are used, such as this."
                #school 5
                #researchlevel 6
                #path 0 6
                #pathlevel 0 4
                #fatiguecost 4000
                #damage 7024
                #effect 10021
                #nreff 1
                #end

                #newmonster 7024
                #name "Forest Grove"
                #copyspr 5029
                #copystats 5029
                #clearmagic
                #clearspec
                #poisonres 25
                #inanimate
                #neednoteat
                #heal
                #spiritsight
                #blind
                #bonusspells 1
                #gemprod 6 1
                #magicskill 8 3
                #domsummon2 "Sprite"
                #end
            -- Awaken Coral Ancient 

                #newspell
                #name "Awaken Coral Ancient"
                #descr "The caster awakens a great reef dwelling ancient, who produces magical gems."
                #details "Summon an immobile creature that produces 2 water gems and 1 gem of either nature, fire or astral"
                #school 0
                #researchlevel 6
                #path 0 2
                #pathlevel 0 5
                #effect 10021
                #fatiguecost 5000
                #spec 8388608 -- UW OK
                #damage -7000 -- Great seaweed thing montag
                #nreff 1
                #end

                --Living Reef with gemgen
                    #newmonster 7029
                    #copystats 5010
                    #copyspr 5010
                    #clearspec
                    #montag 7000
                    #immobile
                    #bluntres
                    #slashres
                    #pierceres
                    #aquatic
                    #neednoteat
                    #heal
                    #spiritsight
                    #bonusspells 1
                    #blind
                    #poisonres 15
                    #diseaseres 50
                    #supplybonus 30
                    #itemslots 1
                    #gemprod 2 2
                    #gemprod 4 1
                    #end

                --Ancient Anemone with gemgen
                    #newmonster 7025
                    #copystats 5009
                    #copyspr 5009                    
                    #clearspec
                    #montag 7000
                    #immobile
                    #bluntres
                    #pierceres
                    #aquatic
                    #neednoteat
                    #heal
                    #spiritsight
                    #bonusspells 1
                    #blind
                    #poisonres 15
                    #diseaseres 50
                    #supplybonus 30
                    #itemslots 1
                    #gemprod 2 2
                    #gemprod 0 1
                    #end

                --Spirit of Sargassum with gemgen
                    #newmonster 7026
                    #copyspr 5056
                    #copystats 5056
                    #clearspec
                    #montag 7000
                    #immobile
                    #bluntres
                    #pierceres
                    #aquatic
                    #neednoteat
                    #heal
                    #spiritsight
                    #gemprod 2 2
                    #gemprod 6 1
                    #bonusspells 1
                    #blind
                    #poisonres 15
                    #diseaseres 50
                    #supplybonus 30
                    #itemslots 1
                    #end
            -- Tree of Life

                #newmonster 7030
                #copystats 5328
                #copyspr 5328 --awakened tree
                #name "Tree of Life"
                #clearspec 
                #mapmove 0
                #poisonres 25
                #summerpower 25
                #stealthy 0
                #blind
                #plant
                #deathpower -2
                #bluntres
                #pierceres
                #magicbeing
                #neednoteat
                #forestsurvival
                #spiritsight
                #supplybonus 60
                #bonusspells 1
                #itemslots 1
                #fireres -5
                #ivylord 3
                #magicskill 6 3
                #magicskill 8 2
                #holy
                #immobile
                #heal
                #gemprod 6 3
                #autodishealer 2
                #end


                #newspell
                #name "Tree of Life"
                #descr "The caster summons a life giving, sentient tree known as a Tree of Life. The Tree of Life is a skilled nature mage, who is able to communicate through the rustling of it's leaves and it's fruit can cure diseases. The seeds from it's fruit can also be harvested and condensed into nature gems."
                #school 4
                #researchlevel 5
                #path 0 6
                #pathlevel 0 4
                #path 1 3
                #pathlevel 1 1
                #effect 10021
                #fatiguecost 5000
                #damage 7030 -- Tree of life
                #nreff 1
                #end
            -- Soul Collector
                #selectitem 504
                #copyitem 132 --sickle crop is pain
                #copyspr "Harvest Blade"
                #name "Reaper Scythe"
                #descr "The Reaper Scythe is a spirit weapon usable only by soul collectors, it's blade harvests the souls of those killed by it and provides the user with death gems. Those who survive a hit from the scythe will decay to death within minutes."
                #constlevel 12
                #type 2
                #nofind
                #cursed
                #end

                #newmonster 7028
                #copystats 491
                #name "Soul Collector"
                #spr1 "./bozmod/Monsters/soulcollector1.tga"
                #spr2 "./bozmod/Monsters/soulcollector2.tga" 
                #clearspec s
                #clearweapons
                #coldres 15
                #poisonres 25
                #stealthy 65
                #fear 15
                #pooramphibian
                #undead
                #ethereal
                #float
                #neednoteat
                #spiritsight
                #naga 
                #startitem 504 --reapers scythe
                #end


                #newspell
                #name "Soul Collector"
                #descr "The caster summons a powerful collector of souls, a ghost-like entity armed with a scythe that harvests the souls of the newly dead into death gems."
                #school 5
                #researchlevel 7
                #path 0 5
                #pathlevel 0 6
                #effect 10021
                #fatiguecost 4000
                #damage 7028 --soul collector
                #nreff 1
                #end
            -- Stygian Ship

                #selectitem 506
                #copyitem 999 --magilum boat
                #name "Stygian Ship"
                #copyspr 362
                #constlevel 12
                #type 8
                #nofind
                #cursed
                #end

                #newspell
                #name "Summon Stygian Ferryman"
                #descr "The caster summons a Stygian Ferryman, an undead creature that is able to use a Stygian Ship to cross the underworld."
                #details "Astral navigators can use the Astral Ship magic item."
                #school 3
                #researchlevel 9
                #path 0 5
                #pathlevel 0 5
                #path 1 2 
                #pathlevel 1 2
                #effect 10021
                #fatiguecost 5000
                #damage 7034 -- Stygian Ferryman
                #nreff 1
                #end

                #newmonster 7034
                #copystats 1717 --Soulless Bandar Warrior
                #spr1 "./bozmod/Monsters/ferryman1.tga"
                #spr2 "./bozmod/Monsters/ferryman2.tga"
                #name "Stygian Ferryman"
                #descr "Stygian Ferrymen are servants of the underworld, they usually carry souls of the dead into the underworld and for this they have the ability to teleport vast distances in the real world. Through the use of magic heether-wather the caster was able to subvert one of these ferrymen into their own employ, under the service the the ascending god. The Ferryman primarily serves to transports troops and especially dead ones, however they are a skilled mage of death and water in their own right."
                #clearspec
                #clearweapons
                #weapon 89 --snake staff
                #undead
                #inanimate
                #coldres 15
                #poisonres 25
                #fireres -5
                #spiritsight
                #mr 30
                #inanimate
                #neednoteat
                #pooramphibian
                #magicskill 5 3
                #magicskill 2 2
                #superiorundeadleader
                #goodleader
                #goodmagicleader
                #startitem 506 --stygian ship
                #end
            -- Golden Goose

                #newmonster 7031
                #copystats 5413 --blessed swan
                #spr1 "./bozmod/Monsters/goldgoose.tga"
                #spr2 "./bozmod/Monsters/goldgoose.tga"
                #name "Golden Goose"
                #descr "Golden goose is a mystical creature born from the trunk of a generous tree. Each month, it lays a golden egg that weighs 25 gold coins which can be added to the teasuries' coffers. The golden goose attracts many thiefs and rumors of such a wondrous creature spread quickly, raising turmoil in the province. The goose is very hungry, probably to produce those golden eggs and it's no good at fighting, however few would willingly fight it. Those that do touch the goose will find themselves stuck to it as it's feathers trap anyone who touches it with ill intent."
                #clearspec 
                #awe 1
                #holy
                #undisciplined
                #animal
                #flying
                #supplybonus -5
                #incscale 0
                #slimer 30
                #gold 25
                #itemslots 1
                #end
            
                #newspell
                #name "Summon Golden Goose"
                #descr "Golden goose is a mystical creature born from the trunk of a generous tree. Each month, it lays a golden egg that weighs 25 gold coins which can be added to the teasuries' coffers. The golden goose attracts many thiefs and rumors of such a wondrous creature spread quickly, raising turmoil in the province. The goose is very hungry, probably to produce those golden eggs and it's no good at fighting, however few would willingly fight it. Those that do touch the goose will find themselves stuck to it as it's feathers trap anyone who touches it with ill intent."
                #school 0
                #researchlevel 4
                #path 0 6
                #pathlevel 0 2
                #path 1 1
                #pathlevel 1 1
                #effect 10001
                #fatiguecost 1500
                #damage 7031 -- Golden Goose
                #nreff 1
                #end
        -- Other Rituals
            -- Forest Fire 

                #newspell
                #name "Forest Fire"
                #descr "The caster begins a forest fire to harvest magical gems from the embers. The forest fire makes it difficult to live in the province and death is increased. However fire elementals will be born from the flame with allegiance to the one who made the flames. The flames greatly aggravate the inhabitants of the forest who may attack the local administration"
                #details "Creates a site that generates 2 fire gems per turn, increases heat, death and unrest. Produces a medium sized fire elemental per turn. Small chance province is attacked by powers of nature"
                #school 4
                #researchlevel 6
                #path 0 0
                #pathlevel 0 3
                #friendlyench 1
                #hiddenench 1
                #effect 10082
                #onlygeosrc 128
                #damage 575
                #fatiguecost 500
                #end

                #newsite 1400
                #name "Burning Forest"
                #path 0
                #loc 2
                #incscale 3
                #decscale 2
                #summon 597 --size 3 fire elemental
                #gems 0 2
                #decunrest -3
                #end

                #newevent
                #rarity 5
                #req_myench 575
                #req_pop0ok
                #req_freesites 1
                #addsite 1400
                #req_nositenbr 1400
                #msg "create burning forest site"
                #notext
                #nolog
                #end

                #newevent
                #rarity 5
                #req_noench 575
                #req_pop0ok
                #req_site 1
                #removesite 1400
                #msg "The forest fire has subsided [Burning Forest]"
                #end

                #newevent 
                #rarity 5
                #req_site 1
                #req_pop0ok
                #req_rare 5
                #msg "The inhabitants of the forest are attacking the local administration for burning the forest [Burning Forest]"
                #2com 237 --dryad
                #6d6units 227 --satyr sneak
                #4d6units 361 --vineman
                #unrest 10
                #killpop 3
                #end
            -- Floating Market

                #newspell
                #name "Create Floating Market"
                #descr "The caster constructs a great floating bazaar that boosts commerce in the province and attracts all manner of wonders. However such a wonderful place of commerce also attracts various misfortunes on the lands below."
                #school -1
                #researchlevel 0
                #path 0 1
                #pathlevel 0 4
                #path 1 4
                #pathlevel 1 1
                #effect 10082
                #damage 576
                #fatiguecost 4000
                #nreff 1
                #end


                #newspell
                #copyspell 106 -- Record of Creation
                #name "Floating Market"
                #descr "The caster constructs a great floating bazaar that boosts commerce in the province and attracts all manner of wonders. However such a wonderful place of commerce also attracts various misfortunes on the lands below."
                #details "Can only be cast in Mountains. The spell permanently creates a magic site that adds 40 gold and increases misfortune, also gives 1d3 air gems per turn through silent events"
                #school 3
                #researchlevel 6
                #path 0 1
                #pathlevel 0 4
                #path 1 4
                #pathlevel 1 1
                #effect 10083
                #onlygeosrc 4194304
                #damage -1
                #fatiguecost 4000
                #nextspell "Create Floating Market"
                #end

                #newsite 1401
                #name "Floating Market"
                #path 1
                #gold 40
                #incscale 4
                #end

                #newevent
                #rarity 5
                #req_site 1
                #msg "Traders in the floating market are making profits and they pay you rent for their stalls [Floating Market]"
                #nolog
                #notext
                #1d3vis 1 -- air gems
                #end

                #newevent
                #rarity 5
                #req_ench 576
                #req_pop0ok
                #req_freesites 1
                #addsite 1401
                #req_nositenbr 1401
                #msg "Your mages have created a floating market high in the skies, it will remain here forever, safe from bandits. The heightened security will attract greater commerce and the merchants will pay regular rent to the sites' owner in air gems[Floating Market]"
                #end
            -- Magic Mine

                #newspell
                #name "Create Magic Mine"
                #descr "creates a magic mine site"
                #school -1
                #researchlevel 0
                #path 0 3
                #pathlevel 0 4
                #effect 10082
                #damage 577
                #fatiguecost 5000
                #nreff 1
                #end

                #newspell
                #copyspell 106 -- Record of Creation
                #name "Magic Mine"
                #descr "With this spell, the caster enters into the mouth of a cave and magically opens the esophagus of the earth. The cave extends and widens, rock condensing further into itself to sustain the larger area, while the valuable minerals and precious stones are left exposed and easy to collect."
                #details "The spell permanently creates a magic site that provides 20 gold and 4 earth gems, while increasing misfortune and can be attacked by jealous dwarves"
                #school 3
                #researchlevel 6
                #path 0 3
                #pathlevel 0 4
                #effect 10083
                #onlygeosrc 4096
                #damage -1
                #fatiguecost 5000
                #nextspell "Create Magic Mine"
                #end

                #newsite 1402
                #name "Magic Mine"
                #path 3
                #gems 3 4
                #res 30
                #gold 20
                #end

                #newevent
                #rarity 5
                #req_site 1
                #req_rare 5
                #msg "Jealous of your mine, a band of dwarves seeking to establish a colony have attacked your province [Magic Mine]"
                #2com 324 --dwarf elder
                #6d6units 3392 --hoburg pikemen
                #2d6units 1808 --iron boar
                #end

                #newevent
                #rarity 5
                #req_ench 577
                #req_pop0ok
                #req_freesites 1
                #addsite 1402
                #req_nositenbr 1402
                #msg "Your mage has conducted the magic ritual and created a magical mine, workers are being sent in to collect the treasures[Magic Mine]"
                #end
            -- Commune with Earth
                #newspell 
                #school 5
                #name "Commune with Earth"
                #effect 10019
                #descr "The caster communes with the earth and asks for it's strength. This allows the caster to jump from one mountain peak to another"
                #researchlevel 4
                #path 0 3
                #pathlevel 0 2
                #fatiguecost 400
                #provrange 10
                #onlygeosrc 4194304
                #onlygeodst 4194304
                #end
            -- Reaping of Souls
                #newspell 
                #school 5
                #copyspell "Raven Feast"
                #name "Reaping of Souls"
                #descr "The caster harvests the souls of the newly dead from a distant province and condenses them into death gems. Provinces struck by plagues or containing recent battlefields can give the caster large amounts of Death Gems. All unburied dead in a province are consumed. Enemy provinces can be targeted".
                #details "Death gems gained from a province with about 100 unburied corpses: 5. Death gems gained from a province with about 400 unburied corpses: 10."
                #researchlevel 4
                #path 0 4
                #pathlevel 0 2
                #path 1 5
                #pathlevel 1 2
                #fatiguecost 200
                #end
            -- Astral Gate Line
                -- Astral Gate
                    #selectitem 503
                    #name "Astral Gate Key"
                    #constlevel 12
                    #copyspr 878
                    #type 8
                    #mainpath 0
                    #mainlevel 5
                    #spell "Gateway"
                    #nofind
                    #cursed
                    #horrormark
                    #nofind
                    #end

                    #newmonster 7027
                    #copystats 5047
                    #name "Astral Gate"
                    #spr1 "./bozmod/Monsters/astralgate.tga"
                    #clearmagic
                    #clearspec
                    #inanimate
                    #pierceres
                    #slashres
                    #neednoteat
                    #amphibian
                    #itemslots 4096
                    #homesick 100
                    #startitem 503
                    #mind
                    #end

                    #newspell
                    #name "Create Astral Gate"
                    #descr "Summons an astral gate"
                    #school -1
                    #researchlevel 0
                    #path 0 3
                    #pathlevel 0 2
                    #path 0 4
                    #pathlevel 0 5
                    #effect 10021
                    #damage 7027
                    #fatiguecost 4000
                    #nreff 1
                    #end

                    #newspell
                    #name "Astral Gate"
                    #descr "The caster builds a magical gateway that allows the transportation of troops through the astral plane to another location. The gate can only be open for some time, so an initial investment of astral pearls is required, however this is a small price to pay as the astral plane is bountiful in magic and from the gate, many more astral pearls may be harvested than those that are initially invested. Opening doors to the astral plane comes with a cost as astral magic oozes out and taints the population, horrors are attracted to this taint. Flocks of horrors will occasionally emerge from the gate to pillage the province. The astral gate may be used to teleport units inside it as per the gateway spell"
                    #school 3
                    #researchlevel 7
                    #path 0 3
                    #pathlevel 0 2
                    #path 0 4
                    #pathlevel 0 5
                    #effect 10082
                    #damage 578
                    #fatiguecost 2500
                    #onlyatsite 1770
                    #nextspell "Create Astral Gate"
                    #ap 0
                    #end

                    #newsite 1403
                    #name "Astral Gate"
                    #path 4
                    #horrormark 10
                    #curse 5
                    #incscale 3
                    #decunrest -8
                    #end

                    #newevent
                    #rarity 5
                    #req_ench 578
                    #req_pop0ok
                    #req_freesites 1
                    #addsite 1403
                    #req_nositenbr 1403
                    #msg "An astral gate is opened, magical energies flood outwards![Astral Gate]"
                    #end

                    #newevent
                    #rarity 5
                    #req_site 1
                    #req_pop0ok
                    #nation -2
                    #com "Astral Gate"
                    #req_nomonster "Astral Gate"
                    #msg "An astral gate is opened, magical energies flood outwards![Astral Gate]"
                    #nolog
                    #notext
                    #end

                    #newevent
                    #rarity 5
                    #req_noench 578
                    #req_pop0ok
                    #req_site 1
                    #removesite 1403
                    #msg "The astral gate energy has burnt out and it closes, for now[Astral Gate]"
                    #end

                    #newevent
                    #rarity 5
                    #req_noench 578
                    #req_pop0ok
                    #req_site 1
                    #killcom 7027
                    #msg "The astral gate energy has burnt out and it closes, for now[Astral Gate]"
                    #notext
                    #nolog
                    #end

                    #newevent
                    #rarity 1
                    #req_site 1
                    #msg "A flock of horrors attracted by the Astral Gate have emerged and attack the populace![Astral Gate]"
                    #tempunits 1
                    #unrest 30
                    #incdom -3
                    #killpop 30
                    #1d6units 2215 --bellymaw horror
                    #2d6units 2216 --brass claw horror
                    #8d6units 2214 --spine membrane horror
                    #end

                    #newevent
                    #rarity 2
                    #req_site 1
                    #req_rare 5
                    #msg "A truly magnificent horror followed by a flock of smaller horrors has emerged from the Astral Gate, the entire province faces obliteration![Astral Gate]"
                    #tempunits 1
                    #com 1913 --Hruvur, abomination of desolation
                    #2com 2211  --mindslime horror
                    #unrest 80
                    #incdom -3
                    #killpop 300
                    #kill 40
                    #2d6units 2215 --bellymaw horror
                    #3d6units 2216 --brass claw horror
                    #16d6units 2214 --spine membrane horror
                    #end
                -- Harvester Golem

                    #newmonster 7032
                    #copystats 474 --Living statue
                    #spr1 "./bozmod/Monsters/harvestergolem1.tga"
                    #spr2 "./bozmod/Monsters/harvestergolem2.tga"
                    #name "Harvester Golem"
                    #clearweapons
                    #weapon "Stone Fist"

                    #descr "Harvester golems are used to extract astral pearls from Astral gates. It's too dangerous to use living beings for this, even if their lives are expendable as they could turn into horrors and attack their masters. Only creatures with no life at all, not even microorganisms can safely enter the astral plane without being corrupted by horrors. For this purpose, harvester golems were invented and now can be used to safely wander back and forth through an Astral gate collecting pearls."
                    #end

                    #newspell
                    #name "Summon Harvester Golem"
                    #descr "A harvester golem is created with this spell."
                    #details "Place a harvester golem at an astral gate, each golem will produce 1d6 pearls each turn."
                    #school 0
                    #school 3
                    #researchlevel 8
                    #path 1 3
                    #pathlevel 1 1
                    #path 0 4
                    #pathlevel 0 2
                    #effect 10021
                    #fatiguecost 1200
                    #damage 7032 -- Harvester Golem
                    #nreff 1
                    #end

                    #newevent
                    #rarity 5
                    #req_site 1
                    #req_pop0ok
                    #nation -2
                    #req_monster "Harvester Golem"
                    #2d6vis 4
                    #msg "One of your harvester golem has harvested some astral pearls![Astral Gate]"
                    #end
                -- Astral Ship

                    #selectitem 505
                    #copyitem 372 --gatestone
                    #name "Astral Ship"
                    #copyspr 362
                    #descr "The astral ship can carry an army over fast distances"
                    #constlevel 12
                    #type 8
                    #nofind
                    #cursed
                    #end

                    #newspell
                    #name "Contact Astral Navigator"
                    #descr "An astral navigator is recruited with this spell."
                    #details "Astral navigators can use the Astral Ship magic item."
                    #school 3
                    #researchlevel 9
                    #path 0 4
                    #pathlevel 0 5
                    #effect 10021
                    #fatiguecost 4000
                    #damage 7033 -- Astral Navigator
                    #onlyatsite 1403 --astral gate
                    #nreff 1
                    #end

                    #newmonster 7033
                    #copystats 1032 --Royal Navigator
                    #copyspr 1032
                    #name "Astral Navigator"
                    #descr "Astral Navigators are mages who have learned to use astral ships that allow armies to travel vast distances through the astral plane. While the armies are themselves safe during this travel the astral navigators are less than perfectly sane from all the sight-seeing they've done, sometimes they need to gaze at the stars."
                    #clearspec
                    #insane 20
                    #expertleader
                    #expertmagicleader
                    #expertundeadleader
                    #startitem 505 --astral ship
                    #end
            -- Become Meteor

                #newspell
                #copyspell "Fires from Afar"
                #name "Meteor Burn"
                #school -1
                #end

                #newspell
                #copyspell "Crumble"
                #name "Meteor Impact"
                #nextspell "Meteor Burn"
                #school -1
                #end

                #newspell
                #copyspell "Teleport"
                #name "Become Meteor"
                #descr "The caster channels the power of the stars to carry them from location to location. The speed that this happens is very great and causes intense heat effects on the caster and creating an explosion where they land, damaging some of the surroundings. If this spell is used to target it will damage the walls."
                #descr "Teleports the user, then casts crumble at the destination followed by fires from afar"
                #school 1
                #researchlevel 7
                #path 0 0
                #pathlevel 0 3
                #effect 10019
                #provrange 8
                #fatiguecost 1400
                #nextspell "Meteor Impact"
                #end
            -- Infernal Topology

                #newspell
                #copyspell "Stygian Paths"
                #name "Infernal Topology"
                #descr "The caster uses a portal to hell to transfer an army from one point to another, however this is not without cost as some of the casters' troops may get lost in hell on the way."
                #details "Copies Stygian Paths spell with slightly more range and vengeance of the dead"
                #school 6
                #researchlevel 7
                #path 0 7
                #pathlevel 0 5
                #provrange 12
                #fatiguecost 4000
                #end
-- Nations
    -- New Nations
        -- Diamedulla - Big thanks for Executor for making this awesome nation
            -- Name Types       
                #selectnametype 250
                #clear
                #addname "DSS-87"
                #addname "DSS-84"
                #addname "DSS-81"
                #addname "DSS-0"
                #addname "DSS-3"
                #addname "DSS-6"
                #addname "DSS-9"
                #addname "DSS-12"
                #addname "DSS-15"
                #addname "DSS-18"
                #addname "DSS-21"
                #addname "DSS-24"
                #addname "DSS-27"
                #addname "DSS-30"
                #addname "DSS-33"
                #addname "DSS-36"
                #addname "DSS-39"
                #addname "DSS-42"
                #addname "DSS-45"
                #addname "DSS-48"
                #addname "DSS-51"
                #addname "DSS-54"
                #addname "DSS-57"
                #addname "DSS-60"
                #addname "DSS-63"
                #addname "DSS-66"
                #addname "DSS-69"
                #addname "DSS-72"
                #addname "DSS-75"
                #addname "DSS-78"
                #addname "DSS-90"
                #addname "DSS-93"
                #addname "DSS-96"
                #addname "DSS-99"
                #end
            -- Events            
                -- Mad Alch Embrace Void 
                    #newevent
                    #rarity 5
                    #req_rare 1
                    #req_land 1
                    #req_pop0ok
                    #req_monster 7417
                    #req_indepok 1
                    #req_targmnr 7417
                    #req_fornation 169
                    #msg "One of your Mad Alchemists has finally stepped completely beyond the threshold of sanity, embracing the Void and drinking from the Astral wellspring of madness. His mind now writhes with Void maggots and resists any attempts to treat or lessen the madness."
                    #transform 7525
                    #end
                        #newevent
                        #rarity 5
                        #req_rare 1
                        #req_land 1
                        #req_pop0ok
                        #req_monster 7417
                        #req_indepok 1
                        #req_targmnr 7417
                        #req_fornation 169
                        #msg "One of your Mad Alchemists has finally stepped completely beyond the threshold of sanity, embracing the Void and drinking from the Astral wellspring of madness. His mind now writhes with Void maggots and resists any attempts to treat or lessen the madness."
                        #transform 7525
                        #end
                            #newevent
                            #rarity 5
                            #req_rare 1
                            #req_land 1
                            #req_pop0ok
                            #req_2monsters 7417
                            #req_indepok 1
                            #req_targmnr 7417
                            #req_fornation 169
                            #msg "One of your Mad Alchemists has finally stepped completely beyond the threshold of sanity, embracing the Void and drinking from the Astral wellspring of madness. His mind now writhes with Void maggots and resists any attempts to treat or lessen the madness."
                            #transform 7525
                            #end
                                #newevent
                                #rarity 5
                                #req_rare 3
                                #req_land 1
                                #req_pop0ok
                                #req_5monsters 7417
                                #req_indepok 1
                                #req_targmnr 7417
                                #req_fornation 169
                                #msg "One of your Mad Alchemists has finally stepped completely beyond the threshold of sanity, embracing the Void and drinking from the Astral wellspring of madness. His mind now writhes with Void maggots and resists any attempts to treat or lessen the madness."
                                #transform 7525
                                #end		
                    
                        Once activated, the effects of the Convergence Sphere cannot be stopped any other way short of destroying the Void artifact. Unless Diamedulla is stopped, everyone in the entire world will soon be turned into a sickening abomination!"
                        #end
                -- Transform Virulent Priest into Virulent Emissary [Worm Tank] -------------------------------------------------------------------
            
            
                    #newevent
                    #rarity 5
                    #req_land 1
                    #req_pop0ok
                    #req_indepok 1
                    #req_targmnr 7412
                    #req_fornation 169
                    #req_monster 7507 -- Worm Tank
                    #msg "No text needed."
                    #nation 169
                    #transform 7522
                    #notext
                    #nolog
                    #end
                -- Transform Necromorph into Necrobrute [Parasyte Tank] -------------------------------------------------------------------
            
                    #newevent
                    #rarity 5
                    #req_land 1
                    #req_pop0ok
                    #req_fornation 169
                    #req_monster 7506 -- Parasyte Tank
                    #msg "No text needed."
                    #delay 0
                    #notext
                    #nolog
                    #end 
                    
                    #newevent
                    #rarity 5
                    #req_land 1
                    #req_rare 75
                    #req_pop0ok
                    #req_indepok 1
                    #req_fornation 169
                    #req_monster 7431 
                    #msg "No text needed."
                    #nation 169
                    #killmon 7431
                    #1unit -13478
                    #notext
                    #nolog
                    #end
                    --
                    
                    #newevent
                    #rarity 5
                    #req_land 1
                    #req_pop0ok
                    #req_fornation 169
                    #req_monster 7506 -- Parasyte Tank
                    #msg "No text needed."
                    #delay 0
                    #notext
                    #nolog
                    #end  
                    
                        #newevent
                        #rarity 5
                        #req_land 1
                        #req_rare 75
                        #req_pop0ok
                        #req_indepok 1
                        #req_fornation 169
                        #req_monster 7432 
                        #msg "No text needed."
                        #nation 169
                        #killmon 7432
                        #1unit -13478
                        #notext
                        #nolog
                        #end
                        --
                    
                    #newevent
                    #rarity 5
                    #req_land 1
                    #req_pop0ok
                    #req_fornation 169
                    #req_monster 7506 - Parasyte Tank
                    #msg "No text needed."
                    #delay 0
                    #notext
                    #nolog
                    #end  
                        
                            #newevent
                            #rarity 5
                            #req_land 1
                            #req_rare 75
                            #req_pop0ok
                            #req_indepok 1
                            #req_fornation 169
                            #req_monster 7433 
                            #msg "No text needed."
                            #nation 169
                            #killmon 7433
                            #1unit -13478
                            #notext
                            #nolog
                            #end
                                --
                    
                    #newevent
                    #rarity 5
                    #req_land 1
                    #req_pop0ok
                    #req_fornation 169
                    #req_monster 7506 -- Parasyte Tank
                    #msg "No text needed."
                    #delay 0
                    #notext
                    #nolog
                    #end  		
                                
                                #newevent
                                #rarity 5
                                #req_land 1
                                #req_rare 75
                                #req_pop0ok
                                #req_indepok 1
                                #req_fornation 169
                                #req_monster 7434 
                                #msg "No text needed."
                                #nation 169
                                #killmon 7434
                                #1unit -13478
                                #notext
                                #nolog
                                #end
                                --
                                
                    #newevent
                    #rarity 5
                    #req_land 1
                    #req_pop0ok
                    #req_fornation 169
                    #req_monster 7506 -- Parasyte Tank
                    #msg "No text needed."
                    #delay 0
                    #notext
                    #nolog
                    #end  
                                
                                    #newevent
                                    #rarity 5
                                    #req_land 1
                                    #req_rare 75
                                    #req_pop0ok
                                    #req_indepok 1
                                    #req_fornation 169
                                    #req_monster 7435 
                                    #msg "No text needed."
                                    #nation 169
                                    #killmon 7435
                                    #1unit -13478
                                    #notext
                                    #nolog
                                    #end		
                                    --
                                    
                    #newevent
                    #rarity 5
                    #req_land 1
                    #req_pop0ok
                    #req_fornation 169
                    #req_monster 7506 -- Parasyte Tank
                    #msg "No text needed."
                    #delay 0
                    #notext
                    #nolog
                    #end  			
                                    
                                        #newevent
                                        #rarity 5
                                        #req_land 1
                                        #req_rare 75
                                        #req_pop0ok
                                        #req_indepok 1
                                        #req_fornation 169
                                        #req_monster 7436
                                        #msg "No text needed."
                                        #nation 169
                                        #killmon 7436
                                        #1unit -13478
                                        #notext
                                        #nolog
                                        #end		
                                        --
                                        
                    #newevent
                    #rarity 5
                    #req_land 1
                    #req_pop0ok
                    #req_fornation 169
                    #req_monster 7506 -- Parasyte Tank
                    #msg "No text needed."
                    #delay 0
                    #notext
                    #nolog
                    #end  				
                                        
                                            #newevent
                                            #rarity 5
                                            #req_land 1
                                            #req_rare 75
                                            #req_pop0ok
                                            #req_indepok 1
                                            #req_fornation 169
                                            #req_monster 7437
                                            #msg "No text needed."
                                            #nation 169
                                            #killmon 7437
                                            #1unit -13478
                                            #notext
                                            #nolog
                                            #end		
                                            --
                                            
                    #newevent
                    #rarity 5
                    #req_land 1
                    #req_pop0ok
                    #req_fornation 169
                    #req_monster 7506 -- Parasyte Tank
                    #msg "No text needed."
                    #delay 0
                    #notext
                    #nolog
                    #end  					
                                            
                                                #newevent
                                                #rarity 5
                                                #req_land 1
                                                #req_rare 75
                                                #req_pop0ok
                                                #req_indepok 1
                                                #req_fornation 169
                                                #req_monster 7438 
                                                #msg "No text needed."
                                                #nation 169
                                                #killmon 7438
                                                #1unit -13478
                                                #notext
                                                #nolog
                                                #end		

                -- Necropocalypse Events -------------------------------------------------------------------
                        
                    -- Nercomorph Freespawn
                        #newevent
                        #rarity 5
                        #req_fornation 169
                        #msg "No text needed."
                        #notext
                        #nolog
                        #delay 0
                        #end
                    
                    #newevent
                    #rarity 5
                    #req_land 1
                    #req_fornation 169
                    #req_ench 579
                    #msg "No text needed." 
                    #nation 169
                    #2d6units -13423
                    #nation -2
                    #killpop 5
                    #notext
                    #nolog
                    #end	

                    #newevent 
                    #rarity 5
                    #req_rare 5
                    #req_land 1
                    #req_ench 579
                    #req_notfornation 169
                    #msg "The population of ##landname## has suffered from a Necromorph outbreak! They attack the local garrison"
                    #2com -13478 --necrobrute
                    #10d6units -13423 
                    #unrest 15
                    #killpop 5
                    #end

                -- Drop Payload -------------------------------------------------------------------
            
            
                    #newevent
                    #rarity 5
                    #req_indepok 1
                    #req_monster 7476
                    #req_targitem 577
                    #req_targforeignok
                    #req_minpop 30
                    #msg "A strange plague is spreading in ##landname##! A strange and sickly fog has settled over numerous, remote villages, its foul fumes killing many. Those killed by this poisonous fog seem to come back to life after mere days or even hours, turning into many-limbed and horrifying monstrosities that crave for nothing but slaughter! Some few surviving witnesses to these strange events claim to have seen a flying ship high in the clouds, sailing in an ever spreading cloud of noxious miasma, prior to the outbreak."
                    #nation 169
                    #killpop 30
                    #5d6units -13423
                    #2com -13423
                    #end
                -- Trinity Assemble/Disassemble -------------------------------------------------------------------
            
            
                    #newevent
                    #rarity 5
                    #req_pop0ok
                    #req_indepok 1
                    #req_monster 7445
                    #req_fornation 169
                    #req_ench 846
                    #msg "No text needed.[Clockwork Horror]" -- 1st of the 3 present
                    #req_site 1
                    #notext
                    #nolog
                    #delay 0
                    #end
                    --
                        #newevent
                        #rarity 5
                        #req_pop0ok
                        #req_indepok 1
                        #req_monster 7446
                        #req_fornation 169
                        #req_ench 846
                        #msg "No text needed." -- 2nd of the 3 present
                        #notext
                        #nolog
                        #delay 0
                        #end
                        --
                            #newevent
                            #rarity 5
                            #req_pop0ok
                            #req_indepok 1
                            #req_monster 7447
                            #req_fornation 169
                            #req_ench 846
                            #msg "No text needed." -- 3rd of the 3 present
                            #notext
                            #nolog
                            #delay 0
                            #end
                            --
                                #newevent
                                #rarity 5
                                #req_pop0ok
                                #req_indepok 1
                                #req_fornation 169
                                #req_ench 846
                                #msg "No text needed." -- 3rd of the 3 present
                                #killcom 7445
                                #killcom 7446
                                #killcom 7447
                                #notext
                                #nolog
                                #delay 0
                                #end
                                --
                                    #newevent
                                    #rarity 5
                                    #req_pop0ok
                                    #req_indepok 1
                                    #req_fornation 169
                                    #req_ench 846
                                    #msg "No text needed." -- Trinity Created
                                    #nation 169
                                    #com 7448
                                    #notext
                                    #nolog
                                    #end	
                    
                    #newevent
                    #rarity 5
                    #req_pop0ok
                    #req_indepok 1
                    #req_ench 845
                    #msg "No text needed." -- Trinity disbanded into 3 seperate entities
                    #req_targmnr 7448
                    #poison 999
                    #nation 169
                    #com 7445
                    #com 7446
                    #com 7447
                    #notext
                    #nolog
                    #end		
                -- Virulent Temple -------------------------------------------------------------------
            
                    -- Triggers Virulent Temple placement + delay event
                        #newevent
                        #rarity 5
                        #req_land 1
                        #req_pop0ok
                        #req_freesites 1
                        #req_targforeignok
                        #req_monster 7413      -- Virulent Priest - transformed version
                        #req_indepok 1
                        #msg "No text needed." -- Trigger event when a Virulent Priests starts preaching
                        #req_nositenbr 1691
                        #req_targorder 43      -- Hide and Preach
                        #delay25 3
                        #notext 
                        #nolog
                        #end
                        
                            #newevent           
                            #rarity 5
                            #req_pop0ok
                            #req_indepok 
                            #req_freesites 1
                            #req_targforeignok
                            #req_monster 7413 -- Virulent Priest - transformed version
                            #req_targorder 43      -- Hide and Preach
                            #msg "There are whispers of a Virulent Temple being established somewhere in ##landname##, and a Virulent Priest preaching of the wonders of Diamedulla, promising to show the face of the God of the Void, and the blessings he bestows upon all his followers."          - Adds a Virulent Temple
                            #req_nositenbr 1691
                            #addsite 1691
                            #stealthcom 7509
                            #end 
            
                    -- Necromorphs spawn in Virulent Temple, 18% rare
                        
                        #newevent
                        #rarity 5
                        #req_land 1
                        #req_pop0ok
                        #req_indepok
                        #req_rare 18
                        #req_nomonster 7509 -- Ensures this event doesn't happen the turn the Virulent Temple is placed
                        #req_monster 7413   -- Virulent Priest - transformed version
                        #req_minpop 20      -- At least 200 pop in the province 
                        #nation 169         -- Diamedulla
                        #msg "People have been lead astray by a false preacher in ##landname##. Many foolish people have followed the Virulent Priest into his secret temple and been shown the true, horrifying face of the God of Void.[Virulent Temple]"
                        #req_site 1
                        #killpop 20         -- Kills 200 population
                        #4d3units -13423
                        #4d3units -13423
                        #4d3units -13423
                        #2com -13423
                        #end
            
                    -- Nercomorph spawn in Virulent Temple, initial turn only
                        
                        #newevent
                        #rarity 5
                        #req_land 1
                        #req_pop0ok
                        #req_indepok
                        #req_monster 7509   -- So it onl happens on the one turn
                        #nation 169         -- Diamedulla
                        #req_minpop 20      -- At least 200 pop in the province 
                        #msg "People have been lead astray by a false preacher in ##landname##. Many foolish people have followed the Virulent Priest into his secret temple and been shown the true, horrifying face of the God of Void.[Virulent Temple]"
                        #req_site 1
                        #killpop 20         -- Kills 200 population
                        #4d3units -13423
                        #4d3units -13423
                        #4d3units -13423
                        #2com -13423
                        #end
            
                    -- Remove Virulent Temple	
                    
                        #newevent
                        #rarity 5                 -- Happens always when requirements are met
                        #req_pop0ok               -- Happens in 0 population lands
                        #req_indepok              -- Happens for indies too
                        #req_nomonster 7413       -- No Virulent Priest present in any capacity
                        #msg "With no Virulent Priest present, the Virulent Temple falls silent.[Virulent Temple]" 
                        #req_commander 1          -- Requires a non-hiding commander present
                        #req_site 1               -- Requires Virulent Temple present
                        #removesite 1691          -- Removes Virulent Temple
                        #end 
                
                    -- Dummy trigger monster
                        
                        #newmonster 7509
                        #copyspr 1369
                        #copystats 1369
                        #stealthy 999
                        #name "Virulent Temple Dummy"
                        #descr "No need."
                        #mr 50
                        #mor 50
                        #hp 100
                        #landdamage 120
                        #invisible
                        #end
                                
                    -- Virulent Horror 
            
                    -- Replace Virulent Temple with Horror
                    
                        #newevent
                        #rarity 5
                        #req_land 1
                        #req_pop0ok
                        #req_indepok 
                        #req_monster 7523
                        #msg "There are whispers of a Virulent Horror being established somewhere in ##landname##, and a Virulent Emissary preaching of the wonders of Diamedulla, promising to show the face of the God of the Void, and the blessings he bestows upon all his followers.[Virulent Temple]"
                        #req_site 1 
                        #removesite 1691
                        #addsite 1692
                        #end 	
            
                    -- Triggers Virulent Horror placement + delay event
                        #newevent
                        #rarity 5
                        #req_pop0ok
                        #req_land 1
                        #req_freesites 1
                        #req_targforeignok
                        #req_monster 7523      -- Virulent Emissary - transformed version
                        #req_indepok 1
                        #msg "No text needed." -- Trigger event when a Virulent Emissary starts preaching
                        #req_nositenbr 1692
                        #req_targorder 43      -- Hide and Preach
                        #delay25 3
                        #notext 
                        #nolog
                        #end
                        
                            #newevent
                            #rarity 5
                            #req_pop0ok
                            #req_indepok 
                            #req_land 1
                            #req_freesites 1
                            #req_targforeignok
                            #req_monster 7523      -- Virulent Emissary - transformed version
                            #req_targorder 43      -- Hide and Preach
                            #msg "There are whispers of a Virulent Horror being established somewhere in ##landname##, and a Virulent Emissary preaching of the wonders of Diamedulla, promising to show the face of the God of the Void, and the blessings he bestows upon all his followers."          - Adds a Virulent Horror
                            #req_nositenbr 1692
                            #addsite 1692
                            #stealthcom 7524
                            #end 
            
                    -- Triggers Virulent Temple placement + delay event in Diamedulla lands only
                        #newevent
                        #rarity 5
                        #req_land 1
                        #req_pop0ok
                        #req_freesites 1
                        #req_targforeignok
                        #req_monster 7413      -- Virulent Priest - transformed version
                        #req_indepok 1
                        #msg "No text needed." -- Trigger event when a Virulent Priests starts preaching
                        #req_nositenbr 1691
                        #req_targorder 6      -- Preach
                        #delay25 3
                        #notext 
                        #nolog
                        #end
                        
                            #newevent           
                            #rarity 5
                            #req_pop0ok
                            #req_indepok 
                            #req_freesites 1
                            #req_targforeignok
                            #req_monster 7413 -- Virulent Priest - transformed version
                            #req_targorder 6      -- Preach
                            #msg "There are whispers of a Virulent Temple being established somewhere in ##landname##, and a Virulent Priest preaching of the wonders of Diamedulla, promising to show the face of the God of the Void, and the blessings he bestows upon all his followers."          - Adds a Virulent Temple
                            #req_nositenbr 1691
                            #addsite 1691
                            #stealthcom 7509
                            #end 
                
                    -- Triggers Virulent Temple placement + delay event in Diamedulla lands only
                        #newevent
                        #rarity 5
                        #req_pop0ok
                        #req_land 1
                        #req_freesites 1
                        #req_targforeignok
                        #req_monster 7523      -- Virulent Emissary - transformed version
                        #req_indepok 1
                        #msg "No text needed." -- Trigger event when a Virulent Emissary starts preaching
                        #req_nositenbr 1692
                        #req_targorder 6      -- Preach
                        #delay25 3
                        #notext 
                        #nolog
                        #end
                        
                            #newevent
                            #rarity 5
                            #req_pop0ok
                            #req_indepok 
                            #req_land 1
                            #req_freesites 1
                            #req_targforeignok
                            #req_monster 7523      -- Virulent Emissary - transformed version
                            #req_targorder 6      -- Preach
                            #msg "There are whispers of a Virulent Horror being established somewhere in ##landname##, and a Virulent Emissary preaching of the wonders of Diamedulla, promising to show the face of the God of the Void, and the blessings he bestows upon all his followers."          - Adds a Virulent Horror
                            #req_nositenbr 1692
                            #addsite 1692
                            #stealthcom 7524
                            #end 	
                
                    -- Necromorphs spawn in Virulent Horror, 18% rare
                
                        #newevent
                        #rarity 5
                        #req_land 1
                        #req_pop0ok
                        #req_indepok
                        #req_rare 18
                        #req_nomonster 7524 -- Ensures this event doesn't happen the turn the Virulent Temple is placed
                        #req_monster 7523   -- Virulent Emissasry - transformed version
                        #req_minpop 20      -- At least 200 pop in the province 
                        #nation 169         -- Diamedulla
                        #msg "People have been lead astray by a false preacher in ##landname##. Many foolish people have followed the Virulent Emissary into his secret temple and been shown the true, horrifying face of the God of Void.[Virulent Horror]"
                        #req_site 1
                        #killpop 20         -- Kills 200 population
                        #4d3units -13423
                        #4d3units -13423
                        #4d3units -13478
                        #2com -13423
                        #end
            
                    -- Nercomorph spawn in Virulent Horror, initial turn only
                
                        #newevent
                        #rarity 5
                        #req_land 1
                        #req_pop0ok
                        #req_indepok
                        #req_monster 7524   -- So it onl happens on the one turn
                        #nation 169         -- Diamedulla
                        #req_minpop 20      -- At least 200 pop in the province 
                        #msg "People have been lead astray by a false preacher in ##landname##. Many foolish people have followed the Virulent Emissary into his secret temple and been shown the true, horrifying face of the God of Void.[Virulent Horror]"
                        #req_site 1
                        #killpop 20         -- Kills 200 population
                        #4d3units -13423
                        #4d3units -13423
                        #4d3units -13478
                        #2com -13423
                        #end
                
                    -- Remove Virulent Horror	
            
                        #newevent
                        #rarity 5
                        #req_pop0ok
                        #req_indepok 
                        #req_nomonster 7523
                        #msg "With no Virulent Priest present, the Virulent Temple falls silent.[Virulent Horror]" - Enemies remove Virulent Temple
                        #req_commander 1
                        #req_site 1 
                        #removesite 1692
                        #end 	
                
                    -- Dummy trigger monster
                
                        #newmonster 7524
                        #copyspr 1369
                        #copystats 1369
                        #stealthy 999
                        #name "Virulent Temple Dummy"
                        #descr "No need."
                        #mr 50
                        #mor 50
                        #hp 100
                        #landdamage 120
                        #invisible
                        #end	
                -- Transform Marker ------------------------------------------------------------------
                
                
                    #newmonster 7473	
                    #copyspr 1369
                    #copystats 1369
                    #stealthy 999
                    #name "Cipher Dummy"
                    #descr "No need."
                    #mr 50
                    #mor 50
                    #hp 100
                    #landdamage 100
                    #invisible
                    #end
                
                    #newmonster 7474	
                    #copyspr 1369
                    #copystats 1369
                    #stealthy 999
                    #name "Cipher Dummy"
                    #descr "No need."
                    #mr 50
                    #mor 50
                    #hp 100
                    #landdamage 100
                    #invisible
                    #end
                
                    #newevent
                    #req_rare 3
                    #req_unique 1
                    #req_monster 7472 -- Cipher Engine
                    #rarity 5
                    #req_fornation 169
                    #req_indepok
                    #msg "Progress! The first of the three components of the Void Marker has finally been revealed with the aid of the great deciphering devices. The secrets of the Void are closer than ever.[Void Pulse]"
                    #magicitem 9
                    #stealthcom 7473
                    #end
                    
                        #newevent
                        #req_rare 3
                        #req_unique 1
                        #req_deadmnr 7473
                        #req_nomnr 7473
                        #req_monster 7472 -- Cipher Engine
                        #rarity 5
                        #req_fornation 169
                        #req_indepok
                        #msg "The second component of the Void Marker has been deciphered! The Morphing Matrix stands ready for use. We are but one step away from finally unlocking the full potential of the Void Marker![Morphing Matrix]"
                        #magicitem 9
                        #stealthcom 7474
                        #end
                    
                            #newevent
                            #req_rare 3
                            #req_unique 1
                            #req_deadmnr 7474
                            #req_nomnr 7474
                            #req_monster 7472 -- Cipher Engine
                            #rarity 5
                            #req_fornation 169
                            #req_indepok
                            #msg "The last missing piece of the puzzle finally falls into place as the third and main component of the Void Marker is revealed, the Convergence Sphere. The full purpose of the Marker is within grasp and the last grand experiment may finally commence![Convergence Sphere]"
                            #magicitem 9
                            #end
                -- Turn Off Spawning Sack --------------------------------------------------------------
                
                    -- Turn off regular
                        #newevent
                        #rarity 5
                        #req_pop0ok
                        #req_maxpop 5 -- 50 population max
                        #msg "No text needed."
                        #req_fornation 169
                        #req_targmnr 7415
                        #transform 7464
                        #notext
                        #nolog
                        #end
                        --
                        #newevent
                        #rarity 5
                        #req_pop0ok
                        #req_maxpop 5 -- 50 population max
                        #msg "No text needed."
                        #req_fornation 169
                        #req_targmnr 7415
                        #transform 7464
                        #notext
                        #nolog
                        #end
                        --
                        #newevent
                        #rarity 5
                        #req_pop0ok
                        #req_maxpop 5 -- 50 population max
                        #msg "No text needed."
                        #req_fornation 169
                        #req_targmnr 7415
                        #transform 7464
                        #notext
                        #nolog
                        #end
                            -- Turn off domsummon version
                            #newevent
                            #rarity 5
                            #req_pop0ok
                            #req_maxpop 5 -- 50 population max
                            #msg "No text needed."
                            #req_fornation 169
                            #req_targmnr 7457
                            #transform 7464
                            #notext
                            #nolog
                            #end
                            --
                            #newevent
                            #rarity 5
                            #req_pop0ok
                            #req_maxpop 5 -- 50 population max
                            #msg "No text needed."
                            #req_fornation 169
                            #req_targmnr 7457
                            #transform 7464
                            #notext
                            #nolog
                            #end
                            --
                            #newevent
                            #rarity 5
                            #req_pop0ok
                            #req_maxpop 5 -- 50 population max
                            #msg "No text needed."
                            #req_fornation 169
                            #req_targmnr 7457
                            #transform 7464
                            #notext
                            #nolog
                            #end
                    
                        -- Turned off to regular form
                        #newevent
                        #rarity 5
                        #req_pop0ok
                        #req_mimpop 6 -- 60 population min
                        #msg "No text needed."
                        #req_fornation 169
                        #req_targmnr 7464
                        #transform 7415
                        #notext
                        #nolog
                        #end
                        --
                        #newevent
                        #rarity 5
                        #req_pop0ok
                        #req_mimpop 6 -- 60 population min
                        #msg "No text needed."
                        #req_fornation 169
                        #req_targmnr 7464
                        #transform 7415
                        #notext
                        #nolog
                        #end
                        --
                        #newevent
                        #rarity 5
                        #req_pop0ok
                        #req_mimpop 6 -- 60 population min
                        #msg "No text needed."
                        #req_fornation 169
                        #req_targmnr 7464
                        #transform 7415
                        #notext
                        #nolog
                        #end
                                    
                    -- Spawning Sack > Powerful Spawning Sack --------------------------------------------------------------
                    
                    
                        #newevent
                        #req_pop0ok
                        #req_indepok 1
                        #req_monster 7456 -- Specimen Tank
                        #req_targforeignok
                        #req_targmnr 7415
                        #rarity 5
                        #msg "No text needed. Spawning Sack transformed."
                        #req_notfornation 169
                        #transform 7457
                        #notext
                        #nolog
                        #end
                        --
                        #newevent
                        #req_pop0ok
                        #req_indepok 1
                        #req_monster 7456 -- Specimen Tank
                        #req_fornation 169
                        #req_targforeignok
                        #req_targmnr 7415
                        #rarity 5
                        #msg "No text needed. Spawning Sack transformed."
                        #transform 7457
                        #notext
                        #nolog
                        #end
                        --
                        #newevent
                        #req_pop0ok
                        #req_indepok 1
                        #req_monster 7456 -- Specimen Tank
                        #req_fornation 169
                        #req_targforeignok
                        #req_targmnr 7415
                        #rarity 5
                        #msg "No text needed. Spawning Sack transformed."
                        #transform 7457
                        #notext
                        #nolog
                        #end
                        --
                        #newevent
                        #req_pop0ok
                        #req_indepok 1
                        #req_monster 7456 -- Specimen Tank
                        #req_fornation 169
                        #req_targforeignok
                        #req_targmnr 7415
                        #rarity 5
                        #msg "No text needed. Spawning Sack transformed."
                        #transform 7457
                        #notext
                        #nolog
                        #end
            -- Weapons
                -- Chomp            
                
                    #newweapon
                    #name "Chomp"
                    #dmg -5
                    #att 0
                    #def 0
                    #len 0
                    #sound 38
                    #armorpiercing
                    #pierce
                    #norepel
                    #end
                -- Mech Drill            
            
                    #newweapon
                    #name "Mechanical Drill"
                    #dmg 6
                    #att 1
                    #def 0
                    #len 1
                    #sound 12
                    #armorpiercing
                    #rcost 10
                    #pierce
                    #ironweapon
                    #end
                -- Wrench 
                
                    
                    #newweapon
                    #name "Wrench"
                    #dmg 2
                    #att 1
                    #def 0
                    #len 1
                    #sound 12
                    #rcost 1
                    #blunt
                    #ironweapon
                    #end
                -- Spew Vommit 
                
                
                    #newweapon 
                    #copyweapon 50
                    #name "Spew Vommit"
                    --dt_cap
                    --armorpiercing
                    --acid
                    #nratt 3
                    #att -2
                    #sound 83
                    #range -3
                    #ammo 5
                    --nostr
                    #flyspr 404 3 313 4
                    #secondaryeffect 414 1605
                    #bonus
                    --range0
                    #end
                -- Overwhelm 
                
                
                    #newweapon 
                    #name "Overwhelm"
                    #dmg 5
                    #att 5
                    #def 0
                    --nratt 6
                    #charge
                    #ammo 1
                    #sound 12
                    #pierce
                    #slash
                    #bonus
                    --unrepel
                    #sizeresist
                    #end
                -- Pierce-Slash Thingy 
                
                
                    #newweapon "Augmented Limb"
                    #name "Augmented Limb"
                    #dmg 4
                    #att 1
                    #def 0
                    #len 1
                    #sound 12
                    #pierce
                    #slash
                    #bonus
                    #end
                -- Poison 
                
                    
                    #newweapon 1610
                    #name "Poison"
                    #dmg 10
                    #armornegating
                    #dt_poison
                    #poison
                    #secondaryeffect 414
                    #end
                    #end
                -- Mechanical Arm 
                
                
                    #newweapon 
                    #name "Mechanical Arm"
                    #dmg 0
                    #att 0
                    #def 1
                    #len 3
                    #sound 10
                    #blunt
                    #armorpiercing
                    #secondaryeffect 91
                    #bonus
                    #ironweapon
                    #rocst 5
                    #end            
                -- Blade 
                
                    
                    #newweapon 
                    #name "Mutated Limb"
                    #dmg 5
                    #att 1
                    #def 0
                    #len 0
                    #sound 10
                    #slash
                    #pierce
                    #bonus
                    #end
                -- Noxious Breath 
                
                
                    #newweapon 
                    #name "Violent Fit"
                    #mrnegates
                    #dmg 100
                    #dt_realstun
                    #armornegating
                    #mrnegateseasily
                    #end
                    
                    #newweapon 
                    #copyweapon 569  
                    #name "Noxious Breath"
                    #dmg 0
                    #nostr
                    --range0
                    #range 7
                    #magic
                    #ammo 12
                    #rcost 20
                    #sound 16
                    #att -2
                    #aoe 1
                    #nostr
                    #beam
                    --range0
                    #nratt -2
                    #mrnegates
                    #secondaryeffectalways "Violent Fit" 
                    #end 
                -- Corrosive Shell 
                
                
                        #newweapon
                        #copyweapon 542   515
                        #name "Area Acid"
                        #dmg 6
                        #aoe 5
                        #explspr 10055
                        #end
                        
                    #newweapon
                    #name "Corrosive Shell"
                    #dmg 12
                    #range 20
                    #ammo 4
                    #sound 15
                    #blunt
                    #nostr
                    #bonus
                    #flyspr 111 1
                    #secondaryeffectalways "Area Acid"
                    #nouw
                    #end                    
                -- Hook 
                
                    
                    #newweapon 
                    #name "Immobilize"
                    #dmg 100
                    #dt_realstun
                    #end
                    
                    #newweapon 
                    #name "Hook"
                    #dmg 1
                    #pierce
                    #len 1
                    #sound 7
                    #secondaryeffect "Immobilize"
                    #rcost 3
                    #end
                -- Slash 
                
                
                    #newweapon 
                    #name "Hook"
                    #dmg 1
                    #slash
                    #len 0
                    #sound 7
                    #rcost 0
                    #end
                -- Noxious Mortar 
                
                
                        #newweapon 
                        #copyweapon 569  -- Drake Gas  - linger 4
                        #name "Noxious Fumes"
                        #aoe 5
                        #explspr 10055
                        #range 0
                        #ammo 0
                        #dmg 5
                        #nostr
                        #end
                        
                    #newweapon 
                    #name "Noxious Mortar"
                    #dmg 30
                    #armorpiercing
                    #att -2
                    #aoe 0              -- 1 Old setting
                    #nratt -3
                    #sound 16
                    #range 50
                    #ammo 8
                    #rcost 0
                    #blunt
                    #ironweapon
                    #norepel
                    #unrepel
                    #nostr
                    #nouw
                    #flyspr 362 4
                    #secondaryeffectalways "Noxious Fumes"
                    #explspr 10055
                    #end
                -- Psychic Blast 
                
                    
                    #newweapon 
                    #copyweapon 293   
                    #name "Psychic Blast"
                    #range 0
                    #aoe 25
                    #ammo 0
                    #friendlyimmune
                    #att 5
                    #explspr 10141
                    #end
            -- Units
                    -- Worker 
                        #newmonster 7400
                        #name "Worker"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/Worker1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/Worker1.tga"
                        #descr "When the secret order of the Black Alchemists finally reveled themselves and lead a bloody revolution against the Imperium, the proletariat, the simple workers of Antikythera were promised a wonderful, glorious future; a higher purpose, an escape from the menial clockwork factories. The idyllic future envisioned by the Black Alchemist for the proletariat turned out to be closer to a nightmare, as is often the case in revolutions. Though the working conditions of the average working man have not deteriorated too significantly since the splintering of the Imperium, the working people of Diamedulla do live in an ever present fear under the rule of the Black Alchemist and their cultist-like disciples. Those that are observed to under-perform in their accorded duties, or those that seem to express doubts or criticisms of any kind simply vanish in the middle of the night along with their entire families. The people of Diamedulla have been restrained of many basic freedoms once taken for granted. Even death does not offer peace and a chance to finally rest as the monstrous Collectors appear to take away the bodies of the recently deceased as soon as word of passing spreads. Indeed, not reporting on such matter has even been deemed a criminal offense recently."
                        #ap 12
                        #mapmove 14
                        #hp 10
                        #mr 9
                        #size 2
                        #str 9
                        #enc 0
                        #att 9
                        #def 9
                        #prec 10
                        #mor 5
                        #gcost 6
                        #slave
                        #weapon "Fist"
                        #reclimit 12
                        #secondshape 7439
                        #rpcost 3
                        #end
                        
                            #newmonster 7439
                            #name "Zombie Worker"
                            #spr1 "./bozmod/Confluence/LA_Diamedulla/ZombieWorker1.tga"
                            #spr2 "./bozmod/Confluence/LA_Diamedulla/ZombieWorker1.tga"
                            #descr "Even death provides no escape for the people of Diamedulla, as those who die simply seem to be reanimated days later and brought back to life through the mysterious influence of the strange void artifact known as the Void Marker. Indeed, this extraordinary property of the object is what lead to the forming of the order of the Black Alchemists and the ensuing civil war which nearly destroyed the Imperium and lead to the forming of Diamedulla."
                            #ap 8
                            #mapmove 18
                            #hp 15
                            #mr 6
                            #size 2
                            #str 12
                            #enc 0
                            #att 8
                            #def 8
                            #prec 10
                            #mor 50
                            #slave
                            #weapon "Fist"
                            #undead
                            #poisonres 25
                            #coldres 15
                            #neednoteat
                            #undisciplined
                            #noheal
                            #inanimate
                            #pooramphibian
                            #darkvision 100
                            #startage 30
                            #maxage 500
                            #gcost 0
                            #end
                    -- Mechanic 
                    
                    
                        #newmonster 7401
                        #name "Mechanic"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/Mechanic1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/Mechanic1.tga"
                        #descr "Though the life of a mechanic is by no means an easy life in Diamedulla, it does hold certain unalienable rights that most common and easily replaceable workers lack. Though menial labor is all too common in Diamedulla, skilled laborers are still a commodity, most of them having died in the initial sabotages of the Antikytheran production centers and the ensuing war.A mechanic might toil away in the assembly lines of Diamedulla, or the infamous Clockwork Horror factories, but at least they live free of the fear of waking up in a strange underground laboratory, strapped to an operating table in front of a apathetic, or even worse, excited Flesh Carver."
                        #ap 12
                        #mapmove 14
                        #hp 10
                        #mr 9
                        #size 2
                        #str 9
                        #enc 3
                        #att 9
                        #def 9
                        #prec 10
                        #mor 5
                        #gcost 9
                        #slave
                        #weapon "Wrench"
                        #siegebonus 1
                        #castledef 1
                        #reclimit 9
                        #secondshape 7440
                        #rpcost 4
                        #end
                        
                            #newmonster 7440
                            #name "Zombie Mechanic"
                            #spr1 "./bozmod/Confluence/LA_Diamedulla/ZombieMechanic1.tga"
                            #spr2 "./bozmod/Confluence/LA_Diamedulla/ZombieMechanic1.tga"
                            #descr "Even death provides no escape for the people of Diamedulla, as those who die simply seem to be reanimated days later and brought back to life through the mysterious influence of the strange void artifact known as the Void Marker. Indeed, this extraordinary property of the object is what lead to the forming of the order of the Black Alchemists and the ensuing civil war which nearly destroyed the Imperium and lead to the forming of Diamedulla."
                            #ap 8
                            #mapmove 18
                            #hp 15
                            #mr 6
                            #size 2
                            #str 12
                            #enc 0
                            #att 8
                            #def 8
                            #prec 10
                            #mor 50
                            #slave
                            #weapon "Wrench"
                            #undead
                            #poisonres 25
                            #coldres 15
                            #neednoteat
                            #undisciplined
                            #noheal
                            #inanimate
                            #pooramphibian
                            #darkvision 100
                            #startage 30
                            #maxage 500
                            #siegebonus 1
                            #castledef 1
                            #gcost 0
                            #end
                    -- Smith 
                        #newmonster 7402
                        #name "Smith"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/Smith1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/Smith1.tga"
                        #descr "Though the life of a smith is by no means an easy life in Diamedulla, it does hold certain unalienable rights that most common and easily replaceable workers lack. Though menial labor is all too common in Diamedulla, skilled laborers are still a commodity, most of them having died in the initial sabotages of the Antikytheran production centers and the ensuing war.A smith might toil away in the foundries of Diamedulla, endlessly casting clockwork bodies and various prosthetics, but at least they live free of the fear of waking up in a strange underground laboratory, strapped to an operating table in front of a apathetic, or even worse, excited Flesh Carver."
                        #ap 12
                        #mapmove 14
                        #hp 10
                        #mr 9
                        #size 2
                        #str 10
                        #enc 3
                        #att 9
                        #def 9
                        #prec 10
                        #mor 5
                        #gcost 9
                        #slave
                        #weapon "Hammer"
                        #resources 2
                        #reclimit 6
                        #secondshape 7441
                        #rpcost 5
                        #end
                        
                            #newmonster 7441
                            #name "Zombie Smith"
                            #spr1 "./bozmod/Confluence/LA_Diamedulla/ZombieSmith1.tga"
                            #spr2 "./bozmod/Confluence/LA_Diamedulla/ZombieSmith1.tga"
                            #descr "Even death provides no escape for the people of Diamedulla, as those who die simply seem to be reanimated days later and brought back to life through the mysterious influence of the strange void artifact known as the Void Marker. Indeed, this extraordinary property of the object is what lead to the forming of the order of the Black Alchemists and the ensuing civil war which nearly destroyed the Imperium and lead to the forming of Diamedulla."
                            #ap 8
                            #mapmove 18
                            #hp 15
                            #mr 6
                            #size 2
                            #str 12
                            #enc 0
                            #att 8
                            #def 8
                            #prec 10
                            #mor 50
                            #slave
                            #weapon "Hammer"
                            #undead
                            #poisonres 25
                            #coldres 15
                            #neednoteat
                            #undisciplined
                            #noheal
                            #inanimate
                            #pooramphibian
                            #darkvision 100
                            #startage 30
                            #maxage 500
                            #resources 2
                            #gcost 0
                            #end
                    -- Miner 
                        #newmonster 7403
                        #name "Miner"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/Miner1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/Miner1.tga"
                        #descr "Though the life of a miner is by no means an easy life in Diamedulla, it does hold certain unalienable rights that most common and easily replaceable workers lack. Though menial labor is all too common in Diamedulla, skilled laborers are still a commodity, most of them having died in the initial sabotages of the Antikytheran production centers and the ensuing war. A miner might toil away in the gold, copper, or iron mines of Diamedulla, but at least they live free of the fear of being taken in the dead of night and waking up in strange underground laboratories, strapped to operating tables in front of apathetic, or even worse, excited Flesh Carvers."
                        #ap 12
                        #mapmove 14
                        #hp 10
                        #mr 9
                        #size 2
                        #str 10
                        #enc 3
                        #att 9
                        #def 9
                        #prec 10
                        #mor 5
                        #gcost 12
                        #slave
                        #weapon "Pick Axe"
                        #gold 1
                        #reclimit 6    
                        #secondshape 7442
                        #rpcost 6
                        #end
                        
                            #newmonster 7442
                            #name "Zombie Miner"
                            #spr1 "./bozmod/Confluence/LA_Diamedulla/ZombieMiner1.tga"
                            #spr2 "./bozmod/Confluence/LA_Diamedulla/ZombieMiner1.tga"
                            #descr "Even death provides no escape for the people of Diamedulla, as those who die simply seem to be reanimated days later and brought back to life through the mysterious influence of the strange void artifact known as the Void Marker. Indeed, this extraordinary property of the object is what lead to the forming of the order of the Black Alchemists and the ensuing civil war which nearly destroyed the Imperium and lead to the forming of Diamedulla."
                            #ap 8
                            #mapmove 18
                            #hp 15
                            #mr 6
                            #size 2
                            #str 12
                            #enc 0
                            #att 8
                            #def 8
                            #prec 10
                            #mor 50
                            #slave
                            #weapon "Pick Axe"
                            #undead
                            #poisonres 25
                            #coldres 15
                            #neednoteat
                            #undisciplined
                            #noheal
                            #inanimate
                            #pooramphibian
                            #darkvision 100
                            #startage 30
                            #maxage 500
                            #gold 1
                            #gcost 0
                            #end
                        
                                #newmonster 7443
                                #name "Zombie Miner"
                                #spr1 "./bozmod/Confluence/LA_Diamedulla/ZombieMiner1.tga"
                                #spr2 "./bozmod/Confluence/LA_Diamedulla/ZombieMiner1.tga"
                                #descr "Even death provides no escape for the people of Diamedulla, as those who die simply seem to be reanimated days later and brought back to life through the mysterious influence of the strange void artifact known as the Void Marker. Indeed, this extraordinary property of the object is what lead to the forming of the order of the Black Alchemists and the ensuing civil war which nearly destroyed the Imperium and lead to the forming of Diamedulla."
                                #ap 8
                                #mapmove 18
                                #hp 15
                                #mr 6
                                #size 2
                                #str 12
                                #enc 0
                                #att 8
                                #def 8
                                #prec 10
                                #mor 50
                                #slave
                                #weapon "Pick Axe"
                                #undead
                                #poisonres 25
                                #coldres 15
                                #neednoteat
                                #undisciplined
                                #noheal
                                #inanimate
                                #pooramphibian
                                #darkvision 100
                                #startage 30
                                #maxage 500
                                #gold 1
                                #montag 13409
                                #gcost 0
                                #end
                        
                                #newmonster 7444
                                #name "Zombie Miner"
                                #spr1 "./bozmod/Confluence/LA_Diamedulla/ZombieMiner1.tga"
                                #spr2 "./bozmod/Confluence/LA_Diamedulla/ZombieMiner1.tga"
                                #descr "Even death provides no escape for the people of Diamedulla, as those who die simply seem to be reanimated days later and brought back to life through the mysterious influence of the strange void artifact known as the Void Marker. Indeed, this extraordinary property of the object is what lead to the forming of the order of the Black Alchemists and the ensuing civil war which nearly destroyed the Imperium and lead to the forming of Diamedulla."
                                #ap 8
                                #mapmove 18
                                #hp 15
                                #mr 6
                                #size 2
                                #str 12
                                #enc 0
                                #att 8
                                #def 8
                                #prec 10
                                #mor 50
                                #slave
                                #weapon "Pick Axe"
                                #undead
                                #poisonres 25
                                #coldres 15
                                #neednoteat
                                #undisciplined
                                #noheal
                                #inanimate
                                #pooramphibian
                                #darkvision 100
                                #startage 30
                                #maxage 500
                                #montag 13409
                                #gcost 0
                                #deserter 100 --Basically gives 1 Zombie Miner per 2 Miners
                                #end
                    -- Jarhead(s)                     
                        -- Base
                            #newmonster 7404
                            #name "Jarhead"
                            #spr1 "./bozmod/Confluence/LA_Diamedulla/SpaceBanefire1.tga"
                            #spr2 "./bozmod/Confluence/LA_Diamedulla/SpaceBanefire1.tga"
                            #descr "Jarheads compose the bulk of the Diamedullan infantry corps. These peculiar, semi-mechanical soldier are the results of extensive Flesh Carver experiments and come in many varieties, equipped with vastly differing arms, as Jarheads require almost no training process and can as such be given any number of weapons to utilize. As Diamedulla lacks conventional, professional soldiers to protect its borders, but has no such lack of unskilled laborers, a desperate solution was sought to turn simple workers into skilled, or rather efficient soldiers. The end result of such experiments were Jarheads, soldiers whose entire bodies, apart from their heads, were replaced with metallic exoskeletons and outfitted with various weapons. Though the clockwork bodies of these solders are somewhat crude with bursts of very rigid movements, the awesome strength and swiftness of those same movements more than make up for their rigidity and even the lack of conventional martial training. With merely a few weeks of getting accustomed to his new host body, a Jarhead can perform on the same level as a professional soldier trained for many years by dint of sheer physical capabilities."
                            #prot 18
                            #ap 10
                            #mapmove 14
                            #hp 7
                            #mr 12
                            #size 2
                            #str 14
                            #enc 0
                            #att 10
                            #def 10
                            #prec 8
                            #mor 10
                            #gcost 18
                            #rpcost 11
                            #rcost 23
                            #startage 26
                            #maxage 150
                            #poisonres 15
                            #diseaseres 100
                            #inanimate
                            #neednoteat
                            #undcommand 20
                            #noitem
                            #firstshape -13416
                            #weapon "Augmented Limb"
                            #armor "Shield"
                            #end
                        -- AXE
                    
                                #newmonster 7423
                                #name "Jarhead"
                                #spr1 "./bozmod/Confluence/LA_Diamedulla/SpaceAxe1.tga"
                                #spr2 "./bozmod/Confluence/LA_Diamedulla/SpaceAxe1.tga"
                            #descr "Jarheads compose the bulk of the Diamedullan infantry corps. These peculiar, semi-mechanical soldier are the results of extensive Flesh Carver experiments and come in many varieties, equipped with vastly differing arms, as Jarheads require almost no training process and can as such be given any number of weapons to utilize. As Diamedulla lacks conventional, professional soldiers to protect its borders, but has no such lack of unskilled laborers, a desperate solution was sought to turn simple workers into skilled, or rather efficient soldiers. The end result of such experiments were Jarheads, soldiers whose entire bodies, apart from their heads, were replaced with metallic exoskeletons and outfitted with various weapons. Though the clockwork bodies of these solders are somewhat crude with bursts of very rigid movements, the awesome strength and swiftness of those same movements more than make up for their rigidity and even the lack of conventional martial training. With merely a few weeks of getting accustomed to his new host body, a Jarhead can perform on the same level as a professional soldier trained for many years by dint of sheer physical capabilities."
                            #prot 18
                                #ap 10
                                #mapmove 14
                                #hp 7
                                #mr 12
                                #size 2
                                #str 14
                                #enc 0
                                #att 10
                                #def 10
                                #prec 8
                                #mor 10
                            #gcost 18
                            #rpcost 11
                                #rcost 25
                            #startage 26
                            #maxage 150
                                #poisonres 15
                                #diseaseres 100
                                #inanimate
                                #neednoteat
                                #undcommand 20
                                #noitem
                                #montag 13416
                                #armor "Shield"
                                #weapon "Axe"
                                #weapon 592
                                #end
                        -- BANEFIRE
                    
                                #newmonster 7424
                                #name "Jarhead"
                                #spr1 "./bozmod/Confluence/LA_Diamedulla/SpaceBanefire1.tga"
                                #spr2 "./bozmod/Confluence/LA_Diamedulla/SpaceBanefire1.tga"
                            #descr "Jarheads compose the bulk of the Diamedullan infantry corps. These peculiar, semi-mechanical soldier are the results of extensive Flesh Carver experiments and come in many varieties, equipped with vastly differing arms, as Jarheads require almost no training process and can as such be given any number of weapons to utilize. As Diamedulla lacks conventional, professional soldiers to protect its borders, but has no such lack of unskilled laborers, a desperate solution was sought to turn simple workers into skilled, or rather efficient soldiers. The end result of such experiments were Jarheads, soldiers whose entire bodies, apart from their heads, were replaced with metallic exoskeletons and outfitted with various weapons. Though the clockwork bodies of these solders are somewhat crude with bursts of very rigid movements, the awesome strength and swiftness of those same movements more than make up for their rigidity and even the lack of conventional martial training. With merely a few weeks of getting accustomed to his new host body, a Jarhead can perform on the same level as a professional soldier trained for many years by dint of sheer physical capabilities."
                            #prot 18
                                #ap 10
                                #mapmove 14
                                #hp 7
                                #mr 12
                                #size 2
                                #str 14
                                #enc 0
                                #att 10
                                #def 10
                                #prec 8
                                #mor 10
                            #gcost 18
                            #rpcost 11
                                #rcost 25
                            #startage 26
                            #maxage 150
                                #poisonres 15
                                #diseaseres 100
                                #inanimate
                                #neednoteat
                                #undcommand 20
                                #noitem
                                #montag 13416
                                #armor "Shield"
                                #weapon "Augmented Limb"
                                #end
                        -- BARBED
                    
                                #newmonster 7425
                                #name "Jarhead"
                                #spr1 "./bozmod/Confluence/LA_Diamedulla/SpaceBarbed1.tga"
                                #spr2 "./bozmod/Confluence/LA_Diamedulla/SpaceBarbed1.tga"
                            #descr "Jarheads compose the bulk of the Diamedullan infantry corps. These peculiar, semi-mechanical soldier are the results of extensive Flesh Carver experiments and come in many varieties, equipped with vastly differing arms, as Jarheads require almost no training process and can as such be given any number of weapons to utilize. As Diamedulla lacks conventional, professional soldiers to protect its borders, but has no such lack of unskilled laborers, a desperate solution was sought to turn simple workers into skilled, or rather efficient soldiers. The end result of such experiments were Jarheads, soldiers whose entire bodies, apart from their heads, were replaced with metallic exoskeletons and outfitted with various weapons. Though the clockwork bodies of these solders are somewhat crude with bursts of very rigid movements, the awesome strength and swiftness of those same movements more than make up for their rigidity and even the lack of conventional martial training. With merely a few weeks of getting accustomed to his new host body, a Jarhead can perform on the same level as a professional soldier trained for many years by dint of sheer physical capabilities."
                            #prot 18
                                #ap 10
                                #mapmove 14
                                #hp 7
                                #mr 12
                                #size 2
                                #str 14
                                #enc 0
                                #att 10
                                #def 10
                                #prec 8
                                #mor 10
                            #gcost 18
                            #rpcost 11
                                #rcost 25
                            #startage 26
                            #maxage 150
                                #poisonres 15
                                #diseaseres 100
                                #inanimate
                                #neednoteat
                                #undcommand 20
                                #noitem
                                #montag 13416
                                #armor "Shield"
                                #weapon "Augmented Limb"
                                #end
                        -- CANNON
                    
                                #newmonster 7426
                                #name "Jarhead"
                                #spr1 "./bozmod/Confluence/LA_Diamedulla/SpaceCanon1.tga"
                                #spr2 "./bozmod/Confluence/LA_Diamedulla/SpaceCanon1.tga"
                            #descr "Jarheads compose the bulk of the Diamedullan infantry corps. These peculiar, semi-mechanical soldier are the results of extensive Flesh Carver experiments and come in many varieties, equipped with vastly differing arms, as Jarheads require almost no training process and can as such be given any number of weapons to utilize. As Diamedulla lacks conventional, professional soldiers to protect its borders, but has no such lack of unskilled laborers, a desperate solution was sought to turn simple workers into skilled, or rather efficient soldiers. The end result of such experiments were Jarheads, soldiers whose entire bodies, apart from their heads, were replaced with metallic exoskeletons and outfitted with various weapons. Though the clockwork bodies of these solders are somewhat crude with bursts of very rigid movements, the awesome strength and swiftness of those same movements more than make up for their rigidity and even the lack of conventional martial training. With merely a few weeks of getting accustomed to his new host body, a Jarhead can perform on the same level as a professional soldier trained for many years by dint of sheer physical capabilities."
                            #prot 18
                                #ap 10
                                #mapmove 14
                                #hp 7
                                #mr 12
                                #size 2
                                #str 14
                                #enc 0
                                #att 10
                                #def 10
                                #prec 8
                                #mor 10
                            #gcost 18
                            #rpcost 11
                                #rcost 25
                            #startage 26
                            #maxage 150
                                #poisonres 15
                                #diseaseres 100
                                #inanimate
                                #neednoteat
                                #undcommand 20
                                #noitem
                                #montag 13416
                                #armor "Shield"
                                #weapon "Noxious Breath"
                                #end
                        -- DOUBLE SPIKE
                    
                                #newmonster 7427
                                #name "Jarhead"
                                #spr1 "./bozmod/Confluence/LA_Diamedulla/SpaceDouble1.tga"
                                #spr2 "./bozmod/Confluence/LA_Diamedulla/SpaceDouble1.tga"
                            #descr "Jarheads compose the bulk of the Diamedullan infantry corps. These peculiar, semi-mechanical soldier are the results of extensive Flesh Carver experiments and come in many varieties, equipped with vastly differing arms, as Jarheads require almost no training process and can as such be given any number of weapons to utilize. As Diamedulla lacks conventional, professional soldiers to protect its borders, but has no such lack of unskilled laborers, a desperate solution was sought to turn simple workers into skilled, or rather efficient soldiers. The end result of such experiments were Jarheads, soldiers whose entire bodies, apart from their heads, were replaced with metallic exoskeletons and outfitted with various weapons. Though the clockwork bodies of these solders are somewhat crude with bursts of very rigid movements, the awesome strength and swiftness of those same movements more than make up for their rigidity and even the lack of conventional martial training. With merely a few weeks of getting accustomed to his new host body, a Jarhead can perform on the same level as a professional soldier trained for many years by dint of sheer physical capabilities."
                            #prot 18
                                #ap 10
                                #mapmove 14
                                #hp 7
                                #mr 12
                                #size 2
                                #str 14
                                #enc 0
                                #att 10
                                #def 10
                                #prec 8
                                #mor 10
                            #gcost 18
                            #rpcost 11
                                #rcost 25
                            #startage 26
                            #maxage 150
                                #poisonres 15
                                #diseaseres 100
                                #inanimate
                                #neednoteat
                                #undcommand 20
                                #noitem
                                #montag 13416
                                #weapon "Augmented Limb"
                                #weapon "Augmented Limb"
                                #end
                        -- PARASYTE
                    
                                #newmonster 7428
                                #name "Jarhead"
                                #spr1 "./bozmod/Confluence/LA_Diamedulla/SpaceParasyte1.tga"
                                #spr2 "./bozmod/Confluence/LA_Diamedulla/SpaceParasyte1.tga"
                            #descr "Jarheads compose the bulk of the Diamedullan infantry corps. These peculiar, semi-mechanical soldier are the results of extensive Flesh Carver experiments and come in many varieties, equipped with vastly differing arms, as Jarheads require almost no training process and can as such be given any number of weapons to utilize. As Diamedulla lacks conventional, professional soldiers to protect its borders, but has no such lack of unskilled laborers, a desperate solution was sought to turn simple workers into skilled, or rather efficient soldiers. The end result of such experiments were Jarheads, soldiers whose entire bodies, apart from their heads, were replaced with metallic exoskeletons and outfitted with various weapons. Though the clockwork bodies of these solders are somewhat crude with bursts of very rigid movements, the awesome strength and swiftness of those same movements more than make up for their rigidity and even the lack of conventional martial training. With merely a few weeks of getting accustomed to his new host body, a Jarhead can perform on the same level as a professional soldier trained for many years by dint of sheer physical capabilities."
                            #prot 18
                                #ap 10
                                #mapmove 14
                                #hp 7
                                #mr 12
                                #size 2
                                #str 14
                                #enc 0
                                #att 10
                                #def 10
                                #prec 8
                                #mor 10
                            #gcost 18
                            #rpcost 11
                                #rcost 25
                            #startage 26
                            #maxage 150
                                #poisonres 15
                                #diseaseres 100
                                #inanimate
                                #neednoteat
                                #undcommand 20
                                #noitem
                                #montag 13416
                                #armor "Shield"
                                #weapon "Mechanical Arm"
                                #weapon 63
                                #end
                        -- SPIKE
                    
                                #newmonster 7429
                                #name "Jarhead"
                                #spr1 "./bozmod/Confluence/LA_Diamedulla/SpaceSpike1.tga"
                                #spr2 "./bozmod/Confluence/LA_Diamedulla/SpaceSpike1.tga"
                            #descr "Jarheads compose the bulk of the Diamedullan infantry corps. These peculiar, semi-mechanical soldier are the results of extensive Flesh Carver experiments and come in many varieties, equipped with vastly differing arms, as Jarheads require almost no training process and can as such be given any number of weapons to utilize. As Diamedulla lacks conventional, professional soldiers to protect its borders, but has no such lack of unskilled laborers, a desperate solution was sought to turn simple workers into skilled, or rather efficient soldiers. The end result of such experiments were Jarheads, soldiers whose entire bodies, apart from their heads, were replaced with metallic exoskeletons and outfitted with various weapons. Though the clockwork bodies of these solders are somewhat crude with bursts of very rigid movements, the awesome strength and swiftness of those same movements more than make up for their rigidity and even the lack of conventional martial training. With merely a few weeks of getting accustomed to his new host body, a Jarhead can perform on the same level as a professional soldier trained for many years by dint of sheer physical capabilities."
                            #prot 18
                                #ap 10
                                #mapmove 14
                                #hp 7
                                #mr 12
                                #size 2
                                #str 14
                                #enc 0
                                #att 10
                                #def 10
                                #prec 8
                                #mor 10
                            #gcost 18
                            #rpcost 11
                                #rcost 25
                            #startage 26
                            #maxage 150
                                #poisonres 15
                                #diseaseres 100
                                #inanimate
                                #neednoteat
                                #undcommand 20
                                #noitem
                                #montag 13416
                                #weapon "Mechanical Arm"
                                #weapon "Augmented Limb"
                                #armor "Shield"
                                #end
                        -- XBOW
                    
                                #newmonster 7430
                                #name "Jarhead"
                                #spr1 "./bozmod/Confluence/LA_Diamedulla/SpaceXbow1.tga"
                                #spr2 "./bozmod/Confluence/LA_Diamedulla/SpaceXbow1.tga"
                            #descr "Jarheads compose the bulk of the Diamedullan infantry corps. These peculiar, semi-mechanical soldier are the results of extensive Flesh Carver experiments and come in many varieties, equipped with vastly differing arms, as Jarheads require almost no training process and can as such be given any number of weapons to utilize. As Diamedulla lacks conventional, professional soldiers to protect its borders, but has no such lack of unskilled laborers, a desperate solution was sought to turn simple workers into skilled, or rather efficient soldiers. The end result of such experiments were Jarheads, soldiers whose entire bodies, apart from their heads, were replaced with metallic exoskeletons and outfitted with various weapons. Though the clockwork bodies of these solders are somewhat crude with bursts of very rigid movements, the awesome strength and swiftness of those same movements more than make up for their rigidity and even the lack of conventional martial training. With merely a few weeks of getting accustomed to his new host body, a Jarhead can perform on the same level as a professional soldier trained for many years by dint of sheer physical capabilities."
                            #prot 18
                                #ap 10
                                #mapmove 14
                                #hp 7
                                #mr 12
                                #size 2
                                #str 14
                                #enc 0
                                #att 10
                                #def 10
                                #prec 8
                                #mor 10
                            #gcost 18
                            #rpcost 11
                                #rcost 25
                            #startage 26
                            #maxage 150
                                #poisonres 15
                                #diseaseres 100
                                #inanimate
                                #neednoteat
                                #undcommand 20
                                #noitem
                                #montag 13416
                                #armor "Shield"
                                #weapon "Crossbow"
                                #weapon "Dagger"
                                #end
                    -- Noxious Infantry 
                        #newmonster 7405
                        #name "Noxious Infantry"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/NoxiousInfantry1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/NoxiousInfantry1.tga"
                        #descr "In the upheaval that gave rise to Diamedulla, much was taken from Antikythera. Many technological breakthroughs were stolen by the Black Alchemists and then adapted to better combat their former comrades. One such breakthrough was the creation of the Noxious Breath, the Diamedullan variant of the Flamethrower. Unlike the Firebots of the Imperium of Antikythera, who use heavy plate armoring with think insulation against heat, the Noxious Infantry of Diamedulla use simple and cheap leather armor, alchemically enhanced to protect against poison. Though such light armor did little to protect from either the flamethrowers of Antikythera, or many of its many other deadly weapons, the comparatively low costs needed to create a Noxious Breath and protective gear more than made up for it. 
                        Though the Noxious Infantry is still largely unmodified, unlike the majority of the Diamedullan units, they still use 'head jars', containers placed over their head which further help protect against the inherent dangers of the weapons they use. Still, even such precautions can prove insufficient at times, when operating the Noxious Breath."
                        #ap 12
                        #mapmove 14
                        #hp 10
                        #mr 12
                        #size 2
                        #str 10
                        #enc 3
                        #att 9
                        #def 9
                        #prec 10
                        #mor 10
                        #gcost 35
                        #armor "Full Leather Armor"
                        #armor "Leather Hood"
                        #weapon "Dagger"
                        #weapon "Noxious Breath"
                        #poisonres 8
                        #formationfighter -1
                        #diseaseres 100
                        #startage 25
                        #maxage 55
                        #rpcost 30
                        #rcost 5
                        #end
                    -- Noxious Mortar 
                        #newmonster 7406
                        #name "Noxious Mortar"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/NoxiousCrew1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/NoxiousCrew1.tga"
                        #descr "The Noxious Mortar is an adaptation of the Antikytheran Mortar Crews. Much like their Imperium counterparts, the Noxious Crews use shells filled with alchemical compounds, except that the noxious shells are not filled with explosive acid, but rather poisonous gas which is capable of spreading over a much larger area and lingering for a quite a long while. Though the shells the Noxious Crews fire are unlikely to hit anyone on impact, unless they're unlucky enough to be directly struck by the inbound shell, the noxious gas they release will prove to be much more deadly over a longer period of time, poisoning not only those directly present when the noxious fumes are released, but all others that pass nearby or get caught in the lingering fumes."
                        #ap 4
                        #mapmove 10
                        #hp 10
                        #mr 10
                        #size 6
                        #str 10
                        #enc 3
                        #att 10
                        #def 10
                        #prec 10
                        #mor 9
                        #gcost 150
                        #rcost 50
                        #weapon "Dagger"
                        #weapon "Noxious Mortar"
                        #slowrec
                        #siegebonus 25
                        #noitem 
                        #rpcost 75
                        #secondshape 7471
                        #noriverpass
                        #startage 30
                        #maxage 50
                        #end
                        
                        #newmonster 7471
                        #name "Noxious Mortar"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/Noxious1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/Noxious1.tga"
                        #descr "The Noxious Mortar is a very durable construct. Should the soldier operating it die, a new one will simply take his place after the battle.The Noxious Mortar is quite unlikely to be seriously damaged by potential incoming missile fire, and typically needs to be either dismantled or destroyed on spot."
                        #ap 4
                        #mapmove 10
                        #hp 25
                        #mr 12
                        #prot 10
                        #size 6
                        #str 5
                        #enc 3
                        #att 5
                        #def 5
                        #prec 5
                        #mor 50
                        #inanimate
                        #poisonres 25
                        #neednoteat
                        #immobile
                        #gcost 150
                        #rcost 50
                        #weapon "Dagger"
                        #weapon "Noxious Mortar"
                        #slowrec
                        #deathfire 1
                        #deathdisease 5
                        #noitem 
                        #firstshape 7406
                        #noriverpass
                        #maxage 100
                        #pierceres
                        #noheal
                        #end    
                    -- Juggernaut 
                        --was based on the Antikytheran Deep Sea Diver Man technology. Unlike their counterparts, the juggernauts are
                        --incapable of entering the sea, but are not weighted down by all the insulation and heavy plates                    
                        #newmonster 7422
                        #name "Juggernaut"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/Juggernaut1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/Juggernaut1.tga"
                        #descr "Unit not used."
                        #prot 0
                        #ap 12
                        #mapmove 14
                        #hp 20
                        #mr 12
                        #size 3
                        #str 17
                        #enc 2
                        #att 12
                        #def 10
                        #prec 10
                        #mor 13
                        #gcost 20
                        #rpcost 30
                        #rcost -7
                        #weapon "Mechanical Drill"
                        #armor 19
                        #startage 33
                        #maxage 150
                        #poisonres 8
                        #diseaseres 100
                        #end
                    -- Amalgamation 
                    
                    
                        #newmonster 7408
                        #name "Amalgamation" - Too large to scale walls
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/Amalgamation1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/Amalgamation1.tga"
                        #descr "The practice of creating something more than human, but also less, didn't start in Diamedulla but Antikythera, rather, where attempts were made to combine man and clockwork machine. First, metallic prostheses were used on veterans of war, to help ease their lives, but then as the technology progressed men began to integrate more and more clockwork parts into themselves, disregarding their own limbs in favor of more durable, metallic ones. Such clockwork soldiers eventually proved extreemly effective in Antikytheran wars.Though Diamedulla has since reached much greater heights, or possibly fallen much lower in their research of human anatomy and the physical limits of the human body; all of the research is still based on the original sins of Antikythera which began the horrible practice that would inevitable lead to the creation of all of Diamedulla's horrors. An Amalgamation is one of such horrors. It is a spider-like creature composed of numerous metallic limbs, capable of trampling lesser creatures under it, and even scaling walls."
                        #prot 18      -- standard
                        #ap 16        -- 15-18
                        #mapmove 16   -- 18+
                        #hp 28        -- Avvite=24, Human=10
                        #mr 14        -- 10-12
                        #size 4       -- Avvite=5,  Humna=4
                        #str 20       -- Irrelevant
                        #enc 3        -- 5
                        #att 13       -- 10-12
                        #def 11       -- 14-15
                        #prec 10      -- Irrelevant
                        #mor 12       -- 9-12
                        #gcost 70     -- Avvite=75, Human=45
                        #rcost 45     -- 20-30
                        #rpcost 45    -- ?
                        #insane 5     
                        #trample 
                        #weapon "Augmented Limb"
                        #weapon "Augmented Limb"
                        #diseaseres 100
                        #scalewalls
                        #startage 30
                        #maxage 150
                        #unsur 1
                        #noitem
                        #poisonres 15
                        #end
                    -- HookGoliath 
                        -- Base
                            #newmonster 7450
                            #name "Juggernaut"
                            #spr1 "./bozmod/Confluence/LA_Diamedulla/HookGoliath1.tga"
                            #spr2 "./bozmod/Confluence/LA_Diamedulla/HookGoliath1.tga"
                            #descr "Juggernauts are typically created out of mutilated and eviscerated mutated humans integrated into towering metallic bodies. The Juggernauts are based on Antikytheran Deep Sea Diver technology though on a much larger scale. Unlike the Deep Sea Divers, they are totally incapable of entering the sea due to the massive weight of their bodies, which would prevent them from ever surfacing again. Like many Diamedullan creations, the Juggernauts are also often equipped with various armaments."
                            #prot 18
                            #ap 8
                            #mapmove 12
                            #hp 38
                            #mr 14
                            #size 4
                            #str 20
                            #enc 4
                            #att 12
                            #def 10
                            #prec 8
                            #mor 13
                            #gcost 65
                            #insane 5
                            #diseaseres 100
                            #poisonres 8
                            #noitem
                            #startage 35
                            #maxage 150
                            #heal
                            #rpcost 30
                            #firstshape -13465
                            #weapon "Hook" -- Hook
                            #weapon "Mechanical Drill"
                            #rcost 29
                            #end	
                        -- Drill
                            #newmonster 7451
                            #name "Juggernaut"
                            #spr1 "./bozmod/Confluence/LA_Diamedulla/HookGoliath1.tga"
                            #spr2 "./bozmod/Confluence/LA_Diamedulla/HookGoliath1.tga"
                            #descr "Juggernauts are typically created out of mutilated and eviscerated mutated humans integrated into towering metallic bodies. The Juggernauts are based on Antikytheran Deep Sea Diver technology though on a much larger scale. Unlike the Deep Sea Divers, they are totally incapable of entering the sea due to the massive weight of their bodies, which would prevent them from ever surfacing again. Like many Diamedullan creations, the Juggernauts are also often equipped with various armaments."
                            #prot 18
                            #ap 8
                            #mapmove 12
                            #hp 38
                            #mr 14
                            #size 4
                            #str 20
                            #enc 4
                            #att 12
                            #def 10
                            #prec 8
                            #mor 14
                            #gcost 60
                            #insane 5
                            #diseaseres 100
                            #poisonres 8
                            #noitem
                            #startage 35
                            #maxage 150
                            #heal
                            #rpcost 30
                            #montag 13465
                            #weapon "Hook" -- Hook
                            #weapon "Mechanical Drill"
                            #rcost 40
                            #uwdamage 100
                            #end	
                        -- Banefire
                            #newmonster 7452
                            #name "Juggernaut"
                            #spr1 "./bozmod/Confluence/LA_Diamedulla/BanefireGoliath1.tga"
                            #spr2 "./bozmod/Confluence/LA_Diamedulla/BanefireGoliath1.tga"
                            #descr "Juggernauts are typically created out of mutilated and eviscerated mutated humans integrated into towering metallic bodies. The Juggernauts are based on Antikytheran Deep Sea Diver technology though on a much larger scale. Unlike the Deep Sea Divers, they are totally incapable of entering the sea due to the massive weight of their bodies, which would prevent them from ever surfacing again. Like many Diamedullan creations, the Juggernauts are also often equipped with various armaments."
                            #prot 18
                            #ap 8
                            #mapmove 12
                            #hp 38
                            #mr 14
                            #size 4
                            #str 20
                            #enc 4
                            #att 12
                            #def 10
                            #prec 8
                            #mor 14
                            #gcost 60
                            #insane 5
                            #diseaseres 100
                            #poisonres 8
                            #noitem
                            #startage 35
                            #maxage 150
                            #heal
                            #rpcost 30
                            #montag 13465
                            #weapon "Noxious Breath" -- Banefire
                            #weapon "Mechanical Drill"
                            #rcost 40
                            #uwdamage 100
                            #end	
                        -- Flail
                            #newmonster 7453
                            #name "Juggernaut"
                            #spr1 "./bozmod/Confluence/LA_Diamedulla/FlailGoliath1.tga"
                            #spr2 "./bozmod/Confluence/LA_Diamedulla/FlailGoliath1.tga"
                            #descr "Juggernauts are typically created out of mutilated and eviscerated mutated humans integrated into towering metallic bodies. The Juggernauts are based on Antikytheran Deep Sea Diver technology though on a much larger scale. Unlike the Deep Sea Divers, they are totally incapable of entering the sea due to the massive weight of their bodies, which would prevent them from ever surfacing again. Like many Diamedullan creations, the Juggernauts are also often equipped with various armaments."
                            #prot 18
                            #ap 8
                            #mapmove 12
                            #hp 38
                            #mr 14
                            #size 4
                            #str 20
                            #enc 4
                            #att 12
                            #def 10
                            #prec 8
                            #mor 14
                            #gcost 60
                            #insane 5
                            #diseaseres 100
                            #poisonres 8
                            #noitem
                            #startage 35
                            #maxage 150
                            #heal
                            #rpcost 30
                            #montag 13465
                            #weapon "Mechanical Arm"
                            #weapon 347
                            #rcost 40
                            #uwdamage 100
                            #end	
                    -- Dreadnought 

                        #newmonster 7409
                        #name "Dreadnought"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/Assembly1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/Assembly1.tga"
                        #descr "The Dreadnought is the pinnacle of Diamedullan genius, or perhaps madness. It is a towering golem of flesh and metal, with countless augmentations. Within their bio-mechanical bodies, the Dreadnoughts house a powerful Void Matrix. Once seriously wounded in battle and facing inevitable death, the Dreadnoughts will activate the Void Matrix which will in turn tear a hole in the fabric of reality and create a Void Barrier, a small bubble in which time itself is distorted and halted, preventing the Dreadnought within from moving, but also anyone else from potentially reaching the Dreadnought and killing it. Though the Void Barrier is largely hard to pierce with mundane weapons and many forms of magic, it can still be breached and the Dreadnought within destroyed."
                        #prot 18
                        #ap 18
                        #mapmove 16
                        #hp 43
                        #mr 14
                        #size 4
                        #str 22
                        #enc 3
                        #att 14
                        #def 10
                        #prec 10
                        #mor 15
                        #gcost 100
                        #insane 5
                        #diseaseres 100
                        #fear 5
                        #weapon "Mechanical Drill"
                        #weapon "Noxious Breath"
                        #weapon "Corrosive Shell"
                        #poisonres 8
                        #rcost 15
                        #noitem
                        #startage 40
                        #maxage 150
                        #heal
                        #neednoteat
                        #rpcost 60
                        #unsurr 1
                        #secondshape 7410
                        #end
                        
                            #newmonster 7410
                            #name "Dreadnought"
                            #spr1 "./bozmod/Confluence/LA_Diamedulla/AssemblyShield1.tga"
                            #spr2 "./bozmod/Confluence/LA_Diamedulla/AssemblyShield1.tga"
                            #descr "The Dreadnought is the pinnacle of Diamedullan genius, or perhaps madness. It is a towering golem of flesh and metal, with countless augmentations. Within their bio-mechanical bodies, the Dreadnoughts house a powerful Void Matrix. Once seriously wounded in battle and facing inevitable death, the Dreadnoughts will activate the Void Matrix which will in turn tear a hole in the fabric of reality and create a Void Barrier, a small bubble in which time itself is distorted and halted, preventing the Dreadnought within from moving, but also anyone else from potentially reaching the Dreadnought and killing it. Though the Void Barrier is largely hard to pierce with mundane weapons and many forms of magic, it can still be breached and the Dreadnought within destroyed."
                            #prot 18
                            #invulnerable 25
                            #fireres 25
                            #coldres 25
                            #shockres 25
                            #ap 0
                            #mapmove 0
                            #immobile
                            #hp 1
                            #mr 18
                            #size 4
                            #str 22
                            #enc 3
                            #att 14
                            #def 10
                            #prec 10
                            #mor 30
                            #gcost 100
                            #insane 5
                            #diseaseres 100
                            #fear 5
                            #poisonres 25
                            #rcost 15
                            #noitem
                            #startage 40
                            #maxage 150
                            #heal
                            #neednoteat
                            #rpcost 60
                            #unsurr 1
                            #inanimate
                            #firstshape 7409
                            #end
            -- Commanders
                    -- Flesh Golem 
                        #newmonster 7407
                        #name "Flesh Golem"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/FleshGolem1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/FleshGolem1.tga"
                        #descr "A Flesh Golem is the end result of horrifying and gut-churning experiments. A monster created from a ghoulish collection of severed human body parts, stitched into a single and composite form onto an unwilling test subject, typically a simple Worker. These monstrous creatures have metallic prosthetic extremities and often an uneven and unnatural number of limbs. Flesh Golems are very fast and agile. They are able to charge at their victims, bringing them down and overwhelming them with a flurry of quick and vicious attacks. Due to their rather disturbing and sickening origins, the Flesh Golems are utterly raving mad and horrifying to behold. The Flesh Golems were initially designed to serve as assassins due to their versatile skill set, however the unintended consequence of their raving madness makes them less than ideal assassins, unpredictable and prone to fits of extreme violence and bloodlust. Though they are decidedly more dangerous, they do lack the single-mindedness and patience of a typical assassin."#prot 10 
                        #ap 18
                        #mapmove 18
                        #hp 18
                        #mr 14
                        #size 3
                        #str 14
                        #enc 2
                        #att 12
                        #def 13
                        #prec 10
                        #mor 14
                        #gcost 120
                        #rcost 18
                        #rpcost 2
                        #insane 10
                        #weapon "Overwhelm"
                        #weapon "Augmented Limb"
                        #weapon "Augmented Limb"
                        #weapon "Spew Vommit"
                        #diseaseres 100
                        #poisonres 8
                        #scalewalls
                        #assassin
                        #insane 10
                        #patience -2
                        #stealthy 0
                        #noleader
                        #fear 5
                        #noitem
                        #startage 29
                        #maxage 75
                        #end
                    -- Phoenix 
                    
                        #newmonster 7454
                        #name "Phoenix" 
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/BrainCommander1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/BrainCommander1.tga"
                        #descr "The most capable military minds of Diamedulla are often retrieved from the battlefield and preserved in strange, spider-like mechanical bodies when grievously wounded. Indeed, some of them even preemptively have their heads cut off and placed into the vats mounted on top of the machines to escape the confines of the fragility of the human body. These severed heads placed in fluid containers interface directly with the machine through mysterious bridging of magic and technology. The end result is that a Pheonix controls his mechanical body as a natural extension of his own."
                        #prot 18
                        #ap 18
                        #mapmove 14
                        #hp 33
                        #mr 14
                        #size 4
                        #str 18
                        #enc 0
                        #att 11
                        #def 9
                        #prec 8
                        #mor 14
                        #gcost 10010
                        #diseaseres 100
                        #poisonres 15
                        #noitem
                        #startage 64
                        #maxage 150
                        #heal
                        #rpcost 1
                        #trample
                        #weapon "Augmented Limb"
                        #rcost 30
                        #command -20
                        #goodleader
                        #goodundeadleader
                        #userestricteditem 2234
                        #neednoteat
                        #end
                    -- Virulent Priest 
                    
                        -- Base
                            #newmonster 7412
                            #name "Virulent Priest"
                            #spr1 "./bozmod/Confluence/LA_Diamedulla/VirulentPriest1.tga"
                            #spr2 "./bozmod/Confluence/LA_Diamedulla/VirulentPriest1.tga"
                            #descr "Virulent Priests are the cultists of the Void Marker. They spread the false vision of Diamedulla, promising what they personally deem as paradise, but delivering people to a much more nightmarish reality instead, one of being experimented on and turned into horrifying abominations. Virulent Priests are used to operating in foreign lands, ever seeking new followers under the cover of night, gathering their flock in small numbers and leading them to their secret temples, typically established in warehouses, sewers, and other such unseemly places, hidden from prying eyes. It is in such places, the Viral Temples, that the Virulent Priest unveil the face of their god to those gathered, often turning them into the monstrous abominations, the Necromorphs. Virulent Priests consider themselves chosen by the Void and its emissary, the Void Marker. These individuals have undergone severe physical and psychological degeneration and mutations under the insidious influence of the Marker, and now hold dormant within themselves the same infectious properties of the Void Marker. Virulent Priests can trigger accelerated mutations turning themselves into what they consider prophets of the Void. In such a state a Viral Priest will infect all those he comes in close contact with, turning them into twisted Nercomorphs. 
                            
                            The presence of preaching Virulent Priests in their infectious form will slowly spread an outbreak of Necromorphs from the Viral Temples in any land they're present. The infection will spread as long as there is a preaching Viral Priest and an established Viral Temple. Successfully establishing a well-hidden Viral Temple in any land may take the Viral Priest anywhere from two to four months. Once a accelerated mutation is triggered by the Virulent Priest, the process cannot be reversed. Eventually, even the Virulent Priests themselves will succumb to the potency of the infection that they carry and die, though such a martyr's fate is gladly accepted by all."
                            #ap 12
                            #mapmove 14
                            #hp 9
                            #mr 13
                            #size 2
                            #str 9
                            #enc 3
                            #att 8
                            #def 8
                            #prec 10
                            #mor 9
                            #gcost 55
                            #weapon "Quarterstaff"
                            #shapechange 7413
                            #magicskill 8 1
                            #diseaseres 100
                            #stealthy 0
                            #okleader
                            #rpcost 2
                            #startingaff 1
                            #end

                        -- Infectious Form
                        
                            #newmonster 7413
                            #name "Virulent Priest"
                            #spr1 "./bozmod/Confluence/LA_Diamedulla/VirulentPriest21.tga"
                            #spr2 "./bozmod/Confluence/LA_Diamedulla/VirulentPriest21.tga"
                            #descr "Virulent Priests are the cultists of the Void Marker. They spread the false vision of Diamedulla, promising what they personally deem as paradise, but delivering people to a much more nightmarish reality instead, one of being experimented on and turned into horrifying abominations. Virulent Priests are used to operating in foreign lands, ever seeking new followers under the cover of night, gathering their flock in small numbers and leading them to their secret temples, typically established in warehouses, sewers, and other such unseemly places, hidden from prying eyes. It is in such places, the Viral Temples, that the Virulent Priest unveil the face of their god to those gathered, often turning them into the monstrous abominations, the Necromorphs. Virulent Priests consider themselves chosen by the Void and its emissary, the Void Marker. These individuals have undergone severe physical and psychological degeneration and mutations under the insidious influence of the Marker, and now hold dormant within themselves the same infectious properties of the Void Marker. Virulent Priests can trigger accelerated mutations turning themselves into what they consider prophets of the Void. In such a state a Viral Priest will infect all those he comes in close contact with, turning them into twisted Nercomorphs. 
                            
                            The presence of preaching Virulent Priests in their infectious form will slowly spread an outbreak of Necromorphs from the Viral Temples in any land they're present. The infection will spread as long as there is a preaching Viral Priest and an established Viral Temple. Successfully establishing a well-hidden Viral Temple in any land may take the Viral Priest anywhere from two to four months. Once a accelerated mutation is triggered by the Virulent Priest, the process cannot be reversed. Eventually, even the Virulent Priests themselves will succumb to the potency of the infection that they carry and die, though such a martyr's fate is gladly accepted by all."
                            #ap 12
                            #mapmove 14
                            #hp 9
                            #mr 13
                            #size 2
                            #str 9
                            #enc 3
                            #att 8
                            #def 8
                            #prec 10
                            #mor 9
                            #gcost 55
                            #weapon "Quarterstaff"
                            #magicskill 8 1
                            #diseaseres 20
                            #stealthy 0
                            #incscale 3
                            #okleader
                            #rpcost 2
                            #incunrest 30
                            #end
                    -- Virulent Emissary
                        #newmonster 7522
                        #name "Virulent Emissary"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/VirulentEmissary1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/VirulentEmissary1.tga"
                        #descr "Virulent Emissaries are the cultists of the Void Marker. They spread the false vision of Diamedulla, promising what they personally deem as paradise, but delivering people to a much more nightmarish reality instead, one of being experimented on and turned into horrifying abominations. Virulent Emissaries are used to operating in foreign lands, ever seeking new followers under the cover of night, gathering their flock in small numbers and leading them to their secret temples, typically established in warehouses, sewers, and other such unseemly places, hidden from prying eyes. It is in such places, the Viral Horrors, that the Virulent Emissaries unveil the face of their god to those gathered, often turning them into the monstrous abominations, the Necromorphs. Virulent Emissaries consider themselves chosen by the Void and its herald, the Void Marker. These individuals have undergone severe physical and psychological degeneration and mutations under the insidious influence of the Marker, and now hold dormant within themselves the same infectious properties of the Void Marker. Virulent Emissaries can trigger accelerated mutations turning themselves into what they consider prophets of the Void. In such a state a Viral Emissary will infect all those he comes in close contact with, turning them into twisted Nercomorphs. 
                        
                        The presence of preaching Virulent Emissaries in infectious forms will spread an outbreak of Necromorphs from the Viral Horrors in any land they're present. The infection will spread as long as there is a preaching Viral Emissary and an established Viral Horror. Successfully establishing a well-hidden Viral Temple in any land may take the Viral Emissary two to four months. Once a accelerated mutation is triggered by the Virulent Emissary, the process cannot be reversed. Eventually, even the Virulent Emissaries themselves will succumb to the potency of the infection that they carry and die, though such a martyr's fate is gladly accepted by all."
                        #ap 12
                            #mapmove 14
                            #hp 12
                            #mr 14
                            #size 2
                            #str 12
                            #enc 3
                            #att 10
                            #def 8
                            #prec 10
                            #mor 11
                            #gcost 55
                            #weapon "Quarterstaff"
                            #magicskill 8 1
                            #magicboost 8 1
                            #diseaseres 100
                            #stealthy 0
                            #okleader
                            #rpcost 2
                            #shapechange 7523
                            #end
                            
                            #newmonster 7523
                            #name "Virulent Emissary"
                            #spr1 "./bozmod/Confluence/LA_Diamedulla/VirulentEmissary21.tga"
                            #spr2 "./bozmod/Confluence/LA_Diamedulla/VirulentEmissary21.tga"
                        #descr "Virulent Emissaries are the cultists of the Void Marker. They spread the false vision of Diamedulla, promising what they personally deem as paradise, but delivering people to a much more nightmarish reality instead, one of being experimented on and turned into horrifying abominations. Virulent Emissaries are used to operating in foreign lands, ever seeking new followers under the cover of night, gathering their flock in small numbers and leading them to their secret temples, typically established in warehouses, sewers, and other such unseemly places, hidden from prying eyes. It is in such places, the Viral Horrors, that the Virulent Emissaries unveil the face of their god to those gathered, often turning them into the monstrous abominations, the Necromorphs. Virulent Emissaries consider themselves chosen by the Void and its herald, the Void Marker. These individuals have undergone severe physical and psychological degeneration and mutations under the insidious influence of the Marker, and now hold dormant within themselves the same infectious properties of the Void Marker. Virulent Emissaries can trigger accelerated mutations turning themselves into what they consider prophets of the Void. In such a state a Viral Emissary will infect all those he comes in close contact with, turning them into twisted Nercomorphs. 
                        
                        The presence of preaching Virulent Emissaries in infectious forms will spread an outbreak of Necromorphs from the Viral Horrors in any land they're present. The infection will spread as long as there is a preaching Viral Emissary and an established Viral Horror. Successfully establishing a well-hidden Viral Temple in any land may take the Viral Emissary two to four months. Once a accelerated mutation is triggered by the Virulent Emissary, the process cannot be reversed. Eventually, even the Virulent Emissaries themselves will succumb to the potency of the infection that they carry and die, though such a martyr's fate is gladly accepted by all."
                        #ap 12
                            #mapmove 14
                            #hp 21
                            #mr 16
                            #size 3
                            #str 16
                            #enc 3
                            #att 12
                            #def 8
                            #prec 10
                            #mor 14
                            #gcost 55
                            #weapon "Quarterstaff"
                            #weapon 636
                            #magicskill 8 1
                            #magicboost 8 1
                            #diseaseres 50
                            #stealthy 0
                            #incscale 3
                            #okleader
                            #rpcost 2
                            #incunrest 60
                            #end	
                    -- Collector 
                    
                    
                        #newmonster 7414
                        #name "Collector"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/Collector1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/Collector1.tga"
                        #stealthy 0
                        #assassin
                        #patience 0
                        #descr "The Collectors themselves are the end results of horrifying and ungodly experimentation. They are the boogeymen of Diamedulla, the monsters which wait under your bed to snatch and take you away and do unspeakable, unbearable things. Collectors often stalk individuals suspected of possessing rare traits or abnormalities. Such unfortunates are often taken under the cover of night and brought back to one of the numerous Diamedullan labs for further testing and experimentation.
                        At times, large-scale experiments are also conducted by the Collectors on unsuspecting population. Many people are abducted during the night, never to be seen or heard from again. At times, entire villages have been known to simply vanish without a trace. Such experiments are typically conducted covertly and far in between, less word of the disappearances spreads, causing too much unrest and driving potential test subjects into hiding, thereby hindering the successfulness of future experiments."
                        #size 3
                        #ap 12
                        #mapmove 14
                        #prot 18
                        #hp 16
                        #mr 14
                        #str 16
                        #enc 0
                        #att 11
                        #def 11
                        #prec 12
                        #mor 14
                        #gcost 100
                        #poisonres 15
                        #noitem
                        #startage 45
                        #maxage 150
                        #rcost 35
                        #rpcost 2
                        #rcost 25
                        #weapon "Mechanical Arm"
                        #weapon "Augmented Limb"
                        #inanimate
                        #neednoteat
                        #noleader
                        #float
                        #startitem 574
                        #userestricteditem 2234
                        #neednoteat
                        #end
                    -- Spawning Sack
                    
                            #newmonster 7415
                            #name "Spawning Sack"
                            #spr1 "./bozmod/Confluence/LA_Diamedulla/SpawningSack.tga"
                            #spr2 "./bozmod/Confluence/LA_Diamedulla/SpawningSack.tga"
                            #descr "A Spawning Sack is the birthing place of nightmares, the artificial womb of a horror. Unwilling subjects - for no other kind could possibly exist - are submerged inside the viscous fluids of the sack. Once a subject has been sealed inside the sack; long, metallic spines are then inserted, spines which connect to a vat of preservative fluids where something slithering and with far too many tentacles and sickening, fleshy protuberances is contained. Those unfortunate individuals sealed inside the Spawning Sack will mutate at an accelerated speed over a course of weeks and will emerge as inhuman monstrosities known as Necromorphs, leaving room for others to be placed inside in a repeat process.
                            The creature contained within the vat is an aberration created by the Void Marker. A simple individual who was affected by the insidious hum of the Marker to a much higher degree than is normal, causing him to mutate into something completely non-human and tumorous-looking. The discovery of such a remarkable specimen has shifted the focus to locating other such test subjects with unique or rare properties who might aid the research of the Void Marker."
                            #ap 2
                            #mapmove 0
                            #hp 25
                            #mr 16
                            #size 6
                            #str 20
                            #enc 0
                            #att 5
                            #def 5
                            #prec 5
                            #mor 50
                            #prot 15
                            #gcost 100
                            #noleader
                            #rcost 30
                            #itemslots 4096
                            #poisonres 25
                            #inanimate
                            #immobile
                            #startage -1
                            #maxage 100
                            #neednoteat
                            #rpcost 1
                            #nohof
                            #popkill 1
                            #makemonsters1 -13423
                            #nametype 250
                            #userestricteditem 2234
                            #end
                            
                                #newmonster 7457
                                #name "Necromorphic Spawning Sack"
                                #spr1 "./bozmod/Confluence/LA_Diamedulla/SpawningSack.tga"
                                #spr2 "./bozmod/Confluence/LA_Diamedulla/SpawningSack.tga"
                            #descr "A Spawning Sack is the birthing place of nightmares, the artificial womb of a horror. Unwilling subjects - for no other kind could possibly exist - are submerged inside the viscous fluids of the sack. Once a subject has been sealed inside the sack; long, metallic spines are then inserted, spines which connect to a vat of preservative fluids where something slithering and with far too many tentacles and sickening, fleshy protuberance is contained. Those unfortunate individuals sealed inside the Spawning Sack will mutated at an accelerated speed over a course of weeks and will emerge as inhuman monstrosities known as Necromorphs, leaving room for others to be placed inside in a repeat process.
                            The creature contained within the vat is an aberration created by the Void Marker. A simple individual who was affected by the insidious hum of the Marker to a much higher degree than is normal, causing him to mutate into something completely non-human and tumorous-looking. The discovery of such a remarkable specimen has shifted the focus to locating other such test subjects with unique or rare properties who might aid the research of the Void Marker."
                                #ap 2
                                #mapmove 0
                                #hp 25
                                #mr 16
                                #size 6
                                #str 20
                                #enc 0
                                #att 5
                                #def 5
                                #prec 5
                                #mor 50
                                #prot 15
                                #gcost 60
                                #noleader
                                #rcost 1
                                #itemslots 4096
                                #poisonres 25
                                #inanimate
                                #immobile
                                #startage -1
                                #maxage 100
                                #neednoteat
                                #rpcost 1
                                #nohof
                                #popkill 1
                                #makemonsters1 -13423
                                #domsummon20 -13423
                                #raredomsummon -13423
                                #nametype 250
                                #userestricteditem 2234
                                #end
                            
                                    #newmonster 7464 -- No summon version
                                    #name "Spawning Sack"
                                    #spr1 "./bozmod/Confluence/LA_Diamedulla/SpawningSack.tga"
                                    #spr2 "./bozmod/Confluence/LA_Diamedulla/SpawningSack.tga"
                            #descr "A Spawning Sack is the birthing place of nightmares, the artificial womb of a horror. Unwilling subjects - for no other kind could possibly exist - are submerged inside the viscous fluids of the sack. Once a subject has been sealed inside the sack; long, metallic spines are then inserted, spines which connect to a vat of preservative fluids where something slithering and with far too many tentacles and sickening, fleshy protuberance is contained. Those unfortunate individuals sealed inside the Spawning Sack will mutated at an accelerated speed over a course of weeks and will emerge as inhuman monstrosities known as Necromorphs, leaving room for others to be placed inside in a repeat process.
                            The creature contained within the vat is an aberration created by the Void Marker. A simple individual who was affected by the insidious hum of the Marker to a much higher degree than is normal, causing him to mutate into something completely non-human and tumorous-looking. The discovery of such a remarkable specimen has shifted the focus to locating other such test subjects with unique or rare properties who might aid the research of the Void Marker."
                                    #ap 2
                                    #mapmove 0
                                    #hp 25
                                    #mr 16
                                    #size 6
                                    #str 20
                                    #enc 0
                                    #att 5
                                    #def 5
                                    #prec 5
                                    #mor 50
                                    #prot 15
                                    #gcost 0
                                    #noleader
                                    #rcost 1
                                    #poisonres 25
                                    #inanimate
                                    #immobile
                                    #startage -1
                                    #maxage 100
                                    #neednoteat
                                    #rpcost 1
                                    #nohof
                                    #nametype 250
                                    #itemslots 4096
                                    #userestricteditem 2234
                                    #end
                    -- Nercomorphs
                        -- Two Limbs
                            #newmonster 7431
                            #name "Necromorph"
                            #spr1 "./bozmod/Confluence/LA_Diamedulla/Necro1.tga"
                            #spr2 "./bozmod/Confluence/LA_Diamedulla/Necro1.tga"
                            #descr "What Nercomorphs are is not exactly clear. It has been ascertained that the presence of the Void Marker will very slowly cause degeneration to those in its proximity, causing madness and mutations over long periods of time. The inevitable outcome of such extended insidious effects are Necromorphs; mutated people who are reanimated upon death and turned into something new and abominable by all standards. The effects of the Marker are very gradual, however. Those who are killed before the Marker has had a chance to exert longer, meaningful influence, seem to be simply reanimated as zombified corpses rather than Necromorphs. 
                            After much research and vigorous human experimentation and trial and error, the alchemists of Diamedulla have found a way to significantly accelerate the physical and psychological degeneration and turn people into Necromorphs in specialized devices called Spawning Sacks in a matter of weeks. This discovery has lead to the creation of many new secret containment facilities and laboratories all around Diamedulla.
                            Necromorphs are incredibly infectious. Those they slaughter will quickly rise up again and mutate at a staggering rate, reshaping and growing new limbs in a matter of moments. Such an infection would surely scour the world in weeks if not for the fact that such rampant mutations seem to also be incredibly unstable, as such subject die off almost as quickly as they're created. Necromorphs are extremely aggressive and will attack any uninfected organism on sight. This fortunately seems to exclude most of Diamedulla's populace as every person seems to suffer from the Void Marker's influence on some level, no matter how small or insignificant."
                            #ap 14
                            #mapmove 14
                            #hp 15
                            #mr 10
                            #size 2
                            #str 11
                            #enc 1
                            #att 9
                            #def 7
                            #prec 8
                            #mor 18
                            #berserk 0
                            #gcost 0
                            #weapon "Mutated Limb"
                            #weapon "Mutated Limb"
                            #poisonres 8
                            #diseaseres 100
                            #insane 50
                            #heal
                            #startage 32
                            #maxage 500
                            #undead
                            #poorleader
                            #poorundeadleader
                            #noitem
                            #regeneration 10
                            #raiseonkill 100
                            #raiseshape -13423
                            #montag 13423
                            #undisciplined
                            #neednoteat
                            #end
                        -- Drill and Limb
                            #newmonster 7432
                            #name "Necromorph"
                            #spr1 "./bozmod/Confluence/LA_Diamedulla/Necro2.tga"
                            #spr2 "./bozmod/Confluence/LA_Diamedulla/Necro2.tga"
                            #descr "What Nercomorphs are is not exactly clear. It has been ascertained that the presence of the Void Marker will very slowly cause degeneration to those in its proximity, causing madness and mutations over long periods of time. The inevitable outcome of such extended insidious effects are Necromorphs; mutated people who are reanimated upon death and turned into something new and abominable by all standards. The effects of the Marker are very gradual, however. Those who are killed before the Marker has had a chance to exert longer, meaningful influence, seem to be simply reanimated as zombified corpses rather than Necromorphs. 
                            After much research and vigorous human experimentation and trial and error, the alchemists of Diamedulla have found a way to significantly accelerate the physical and psychological degeneration and turn people into Necromorphs in specialized devices called Spawning Sacks in a matter of weeks. This discovery has lead to the creation of many new secret containment facilities and laboratories all around Diamedulla.
                            Necromorphs are incredibly infectious. Those they slaughter will quickly rise up again and mutate at a staggering rate, reshaping and growing new limbs in a matter of moments. Such an infection would surely scour the world in weeks if not for the fact that such rampant mutations seem to also be incredibly unstable, as such subject die off almost as quickly as they're created. Necromorphs are extremely aggressive and will attack any uninfected organism on sight. This fortunately seems to exclude most of Diamedulla's populace as every person seems to suffer from the Void Marker's influence on some level, no matter how small or insignificant."
                            #ap 14
                            #mapmove 14
                            #hp 15
                            #mr 10
                            #size 2
                            #str 11
                            #enc 1
                            #att 9
                            #def 7
                            #prec 8
                            #mor 18
                            #berserk 0
                            #gcost 0
                            #weapon "Mechanical Drill"
                            #weapon "Mutated Limb"
                            #armor "Shield"
                            #poisonres 8
                            #diseaseres 100
                            #insane 50
                            #heal
                            #startage 32
                            #maxage 500
                            #undead
                            #poorleader
                            #poorundeadleader
                            #noitem
                            #regeneration 10
                            #raiseonkill 100
                            #raiseshape -13423
                            #montag 13423
                            #undisciplined
                            #neednoteat
                            #end
                        -- Two Limbs
                            #newmonster 7433
                            #name "Necromorph"
                            #spr1 "./bozmod/Confluence/LA_Diamedulla/Necro3.tga"
                            #spr2 "./bozmod/Confluence/LA_Diamedulla/Necro3.tga"
                            #descr "What Nercomorphs are is not exactly clear. It has been ascertained that the presence of the Void Marker will very slowly cause degeneration to those in its proximity, causing madness and mutations over long periods of time. The inevitable outcome of such extended insidious effects are Necromorphs; mutated people who are reanimated upon death and turned into something new and abominable by all standards. The effects of the Marker are very gradual, however. Those who are killed before the Marker has had a chance to exert longer, meaningful influence, seem to be simply reanimated as zombified corpses rather than Necromorphs. 
                            After much research and vigorous human experimentation and trial and error, the alchemists of Diamedulla have found a way to significantly accelerate the physical and psychological degeneration and turn people into Necromorphs in specialized devices called Spawning Sacks in a matter of weeks. This discovery has lead to the creation of many new secret containment facilities and laboratories all around Diamedulla.
                            Necromorphs are incredibly infectious. Those they slaughter will quickly rise up again and mutate at a staggering rate, reshaping and growing new limbs in a matter of moments. Such an infection would surely scour the world in weeks if not for the fact that such rampant mutations seem to also be incredibly unstable, as such subject die off almost as quickly as they're created. Necromorphs are extremely aggressive and will attack any uninfected organism on sight. This fortunately seems to exclude most of Diamedulla's populace as every person seems to suffer from the Void Marker's influence on some level, no matter how small or insignificant."
                            #ap 14
                            #mapmove 14
                            #hp 15
                            #mr 10
                            #size 2
                            #str 11
                            #enc 1
                            #att 9
                            #def 7
                            #prec 8
                            #mor 18
                            #berserk 0
                            #gcost 0
                            #weapon "Mutated Limb"
                            #weapon "Mutated Limb"
                            #poisonres 8
                            #diseaseres 100
                            #insane 50
                            #heal
                            #startage 32
                            #maxage 500
                            #undead
                            #poorleader
                            #poorundeadleader
                            #noitem
                            #regeneration 10
                            #raiseonkill 100
                            #raiseshape -13423
                            #montag 13423
                            #undisciplined
                            #neednoteat
                            #end
                        -- Wrench and Limb
                            #newmonster 7434
                            #name "Necromorph"
                            #spr1 "./bozmod/Confluence/LA_Diamedulla/Necro4.tga"
                            #spr2 "./bozmod/Confluence/LA_Diamedulla/Necro4.tga"
                            #descr "What Nercomorphs are is not exactly clear. It has been ascertained that the presence of the Void Marker will very slowly cause degeneration to those in its proximity, causing madness and mutations over long periods of time. The inevitable outcome of such extended insidious effects are Necromorphs; mutated people who are reanimated upon death and turned into something new and abominable by all standards. The effects of the Marker are very gradual, however. Those who are killed before the Marker has had a chance to exert longer, meaningful influence, seem to be simply reanimated as zombified corpses rather than Necromorphs. 
                            After much research and vigorous human experimentation and trial and error, the alchemists of Diamedulla have found a way to significantly accelerate the physical and psychological degeneration and turn people into Necromorphs in specialized devices called Spawning Sacks in a matter of weeks. This discovery has lead to the creation of many new secret containment facilities and laboratories all around Diamedulla.
                            Necromorphs are incredibly infectious. Those they slaughter will quickly rise up again and mutate at a staggering rate, reshaping and growing new limbs in a matter of moments. Such an infection would surely scour the world in weeks if not for the fact that such rampant mutations seem to also be incredibly unstable, as such subject die off almost as quickly as they're created. Necromorphs are extremely aggressive and will attack any uninfected organism on sight. This fortunately seems to exclude most of Diamedulla's populace as every person seems to suffer from the Void Marker's influence on some level, no matter how small or insignificant."
                            #ap 14
                            #mapmove 14
                            #hp 15
                            #mr 10
                            #size 2
                            #str 11
                            #enc 1
                            #att 9
                            #def 7
                            #prec 8
                            #mor 18
                            #berserk 0
                            #gcost 0
                            #weapon "Wrench"
                            #weapon "Mutated Limb"
                            #poisonres 8
                            #diseaseres 100
                            #insane 50
                            #heal
                            #startage 32
                            #maxage 500
                            #undead
                            #poorleader
                            #poorundeadleader
                            #noitem
                            #regeneration 10
                            #raiseonkill 100
                            #raiseshape -13423
                            #montag 13423
                            #undisciplined
                            #neednoteat
                            #end
                        -- Aug and Limb
                            #newmonster 7435
                            #name "Necromorph"
                            #spr1 "./bozmod/Confluence/LA_Diamedulla/Necro5.tga"
                            #spr2 "./bozmod/Confluence/LA_Diamedulla/Necro5.tga"
                            #descr "What Nercomorphs are is not exactly clear. It has been ascertained that the presence of the Void Marker will very slowly cause degeneration to those in its proximity, causing madness and mutations over long periods of time. The inevitable outcome of such extended insidious effects are Necromorphs; mutated people who are reanimated upon death and turned into something new and abominable by all standards. The effects of the Marker are very gradual, however. Those who are killed before the Marker has had a chance to exert longer, meaningful influence, seem to be simply reanimated as zombified corpses rather than Necromorphs. 
                            After much research and vigorous human experimentation and trial and error, the alchemists of Diamedulla have found a way to significantly accelerate the physical and psychological degeneration and turn people into Necromorphs in specialized devices called Spawning Sacks in a matter of weeks. This discovery has lead to the creation of many new secret containment facilities and laboratories all around Diamedulla.
                            Necromorphs are incredibly infectious. Those they slaughter will quickly rise up again and mutate at a staggering rate, reshaping and growing new limbs in a matter of moments. Such an infection would surely scour the world in weeks if not for the fact that such rampant mutations seem to also be incredibly unstable, as such subject die off almost as quickly as they're created. Necromorphs are extremely aggressive and will attack any uninfected organism on sight. This fortunately seems to exclude most of Diamedulla's populace as every person seems to suffer from the Void Marker's influence on some level, no matter how small or insignificant."
                            #ap 14
                            #mapmove 14
                            #hp 15
                            #mr 10
                            #size 2
                            #str 11
                            #enc 1
                            #att 9
                            #def 7
                            #prec 8
                            #mor 18
                            #berserk 0 
                            #gcost 0
                            #weapon "Augmented Limb"
                            #weapon "Mutated Limb"
                            #poisonres 8
                            #diseaseres 100
                            #insane 50
                            #heal
                            #startage 32
                            #maxage 500
                            #undead
                            #poorleader
                            #poorundeadleader
                            #noitem
                            #regeneration 10
                            #raiseonkill 100
                            #raiseshape -13423
                            #montag 13423
                            #undisciplined
                            #neednoteat
                            #end
                        -- Mech and Limb
                            #newmonster 7436
                            #name "Necromorph"
                            #spr1 "./bozmod/Confluence/LA_Diamedulla/Necro6.tga"
                            #spr2 "./bozmod/Confluence/LA_Diamedulla/Necro6.tga"
                            #descr "What Nercomorphs are is not exactly clear. It has been ascertained that the presence of the Void Marker will very slowly cause degeneration to those in its proximity, causing madness and mutations over long periods of time. The inevitable outcome of such extended insidious effects are Necromorphs; mutated people who are reanimated upon death and turned into something new and abominable by all standards. The effects of the Marker are very gradual, however. Those who are killed before the Marker has had a chance to exert longer, meaningful influence, seem to be simply reanimated as zombified corpses rather than Necromorphs. 
                            After much research and vigorous human experimentation and trial and error, the alchemists of Diamedulla have found a way to significantly accelerate the physical and psychological degeneration and turn people into Necromorphs in specialized devices called Spawning Sacks in a matter of weeks. This discovery has lead to the creation of many new secret containment facilities and laboratories all around Diamedulla.
                            Necromorphs are incredibly infectious. Those they slaughter will quickly rise up again and mutate at a staggering rate, reshaping and growing new limbs in a matter of moments. Such an infection would surely scour the world in weeks if not for the fact that such rampant mutations seem to also be incredibly unstable, as such subject die off almost as quickly as they're created. Necromorphs are extremely aggressive and will attack any uninfected organism on sight. This fortunately seems to exclude most of Diamedulla's populace as every person seems to suffer from the Void Marker's influence on some level, no matter how small or insignificant."
                            #ap 14
                            #mapmove 14
                            #hp 15
                            #mr 10
                            #size 2
                            #str 11
                            #enc 1
                            #att 9
                            #def 7
                            #prec 8
                            #mor 18
                            #berserk 0
                            #gcost 0
                            #weapon "Mechanical Arm"
                            #weapon "Mutated Limb"
                            #poisonres 8
                            #diseaseres 100
                            #insane 50
                            #heal
                            #startage 32
                            #maxage 500
                            #undead
                            #poorleader
                            #poorundeadleader
                            #noitem
                            #regeneration 10
                            #raiseonkill 100
                            #raiseshape -13423
                            #montag 13423
                            #undisciplined
                            #neednoteat
                            #end
                        -- Hammer and Limb
                            #newmonster 7437
                            #name "Necromorph"
                            #spr1 "./bozmod/Confluence/LA_Diamedulla/Necro7.tga"
                            #spr2 "./bozmod/Confluence/LA_Diamedulla/Necro7.tga"
                            #descr "What Nercomorphs are is not exactly clear. It has been ascertained that the presence of the Void Marker will very slowly cause degeneration to those in its proximity, causing madness and mutations over long periods of time. The inevitable outcome of such extended insidious effects are Necromorphs; mutated people who are reanimated upon death and turned into something new and abominable by all standards. The effects of the Marker are very gradual, however. Those who are killed before the Marker has had a chance to exert longer, meaningful influence, seem to be simply reanimated as zombified corpses rather than Necromorphs. 
                            After much research and vigorous human experimentation and trial and error, the alchemists of Diamedulla have found a way to significantly accelerate the physical and psychological degeneration and turn people into Necromorphs in specialized devices called Spawning Sacks in a matter of weeks. This discovery has lead to the creation of many new secret containment facilities and laboratories all around Diamedulla.
                            Necromorphs are incredibly infectious. Those they slaughter will quickly rise up again and mutate at a staggering rate, reshaping and growing new limbs in a matter of moments. Such an infection would surely scour the world in weeks if not for the fact that such rampant mutations seem to also be incredibly unstable, as such subject die off almost as quickly as they're created. Necromorphs are extremely aggressive and will attack any uninfected organism on sight. This fortunately seems to exclude most of Diamedulla's populace as every person seems to suffer from the Void Marker's influence on some level, no matter how small or insignificant."
                            #ap 14
                            #mapmove 14
                            #hp 15
                            #mr 10
                            #size 2
                            #str 11
                            #enc 1
                            #att 9
                            #def 7
                            #prec 8
                            #mor 18
                            #berserk 0
                            #gcost 0
                            #weapon "Hammer"
                            #weapon "Mutated Limb"
                            #poisonres 8
                            #diseaseres 100
                            #insane 50
                            #heal
                            #startage 32
                            #maxage 500
                            #undead
                            #poorleader
                            #poorundeadleader
                            #noitem
                            #regeneration 10
                            #raiseonkill 100
                            #raiseshape -13423
                            #montag 13423
                            #undisciplined
                            #neednoteat
                            #end
                        -- Hammer and Limb
                            #newmonster 7438
                            #name "Necromorph"
                            #spr1 "./bozmod/Confluence/LA_Diamedulla/Necro8.tga"
                            #spr2 "./bozmod/Confluence/LA_Diamedulla/Necro8.tga"
                            #descr "What Nercomorphs are is not exactly clear. It has been ascertained that the presence of the Void Marker will very slowly cause degeneration to those in its proximity, causing madness and mutations over long periods of time. The inevitable outcome of such extended insidious effects are Necromorphs; mutated people who are reanimated upon death and turned into something new and abominable by all standards. The effects of the Marker are very gradual, however. Those who are killed before the Marker has had a chance to exert longer, meaningful influence, seem to be simply reanimated as zombified corpses rather than Necromorphs. 
                            After much research and vigorous human experimentation and trial and error, the alchemists of Diamedulla have found a way to significantly accelerate the physical and psychological degeneration and turn people into Necromorphs in specialized devices called Spawning Sacks in a matter of weeks. This discovery has lead to the creation of many new secret containment facilities and laboratories all around Diamedulla.
                            Necromorphs are incredibly infectious. Those they slaughter will quickly rise up again and mutate at a staggering rate, reshaping and growing new limbs in a matter of moments. Such an infection would surely scour the world in weeks if not for the fact that such rampant mutations seem to also be incredibly unstable, as such subject die off almost as quickly as they're created. Necromorphs are extremely aggressive and will attack any uninfected organism on sight. This fortunately seems to exclude most of Diamedulla's populace as every person seems to suffer from the Void Marker's influence on some level, no matter how small or insignificant."
                            #ap 14
                            #mapmove 14
                            #hp 15
                            #mr 10
                            #size 2
                            #str 11
                            #enc 1
                            #att 9
                            #def 7
                            #prec 8
                            #mor 18
                            #berserk 0
                            #gcost 0
                            #weapon "Hammer"
                            #weapon "Mutated Limb"
                            #poisonres 8
                            #diseaseres 100
                            #insane 50
                            #heal
                            #startage 32
                            #maxage 500
                            #undead
                            #poorleader
                            #poorundeadleader
                            #noitem
                            #regeneration 10
                            #raiseonkill 100
                            #raiseshape -13423
                            #montag 13423
                            #undisciplined
                            #neednoteat
                            #end
                    -- Flesh Carver                     
                        #newmonster 7416
                        #name "Flesh Carver"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/FleshCarver1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/FleshCarver1.tga"
                        #descr "Flesh Carvers are the ingenious, or perhaps more fittingly twisted minds behind most of Diamedullas inventions. They bring the Black Alchemists' visions to life, if indeed the embodiment of such horrifying visions can even be considered life, and not a perversion or mockery of it. The gruesome laboratories of the Flesh Carvers are typically set up in secret underground facilities, better to mask the smell of dried blood and rotting corpses, and quell the ever present screams of their latest test subjects. Unlike the alchemists of Diamedulla, the Flesh Carverns are not mad, at least not in any conventional sense. They simply enjoy the ghoulish work they do, and unfortunately for the people of Diamedulla, are exceptionally good at it."
                        #ap 12
                        #mapmove 14
                        #hp 10
                        #mr 15
                        #size 2
                        #str 9
                        #enc 3
                        #att 8
                        #def 8
                        #prec 10
                        #mor 11
                        #gcost 125 -- Autocalc cost increase by twenty five due to E/B random change to twenty five percent from ten
                        #weapon 129
                        #armor 5
                        #magicskill 3 1
                        #magicskill 7 1
                        #custommagic 17408 100
                        #custommagic 17408 25
                        #startage 35
                        #poorleader
                        #rpcost 2
                        #end
                    -- Mad Alchemist 
                    
                        #newmonster 7417 
                        #name "Mad Alchemist"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/MadAlchemist1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/MadAlchemist1.tga"
                        #descr "Selected for their inquisitiveness and scholastic proficiency, Mad Alchemists closely study the strange, warping properties of the Void Marker. They interact with the Void Marker much more closely than anyone else in Diamedulla; an endeavor that has not left them unscarred. As such, they are often a good deal more insane and unstable than Black Alchemists. Indeed, their irrationality is a common point of worry in Diamedulla, for even in the sea of madness that is Diamedulla there might be those who are considered just a little bit too mad. Over time their minds will become more and more warped, until they completely succumb to the Void and embrace it, thinking their raging madness a great wellspring of clarity. Such Mad Alchemists can no longer be helped by any means and will pose an ever increasing danger to the tentative stability of Diamedulla.
                        
                        Mad Alchemists will slowly lose their mind as time passes due to the influence the Void Marker exerts on those near it. As Mad Alchemists observe the passing of time as slowed, due to the Temporal Clocks they use, their actions consequently seem quickened. As such, Mad Alchemists are capable of casting spells much faster than normal."
                        #ap 12
                        #mapmove 14
                        #hp 10
                        #mr 15
                        #size 2
                        #str 9
                        #enc 3
                        #att 8
                        #def 8
                        #prec 10
                        #mor 11
                        #gcost 9990
                        #weapon "Dagger"
                        #armor 159
                        #magicskill 4 1
                        #magicskill 5 1
                        #custommagic 6144 100
                        #custommagic 6144 10
                        #startage 40
                        #poorleader
                        #rpcost 2
                        #insane 9
                        #researchbonus 3
                        #alchemy 50
                        #startitem 569
                        #end
                        
                        #newmonster 7525
                        #name "Mad Alchemist"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/MadAlchemist1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/MadAlchemist1.tga"
                        #descr "Selected for their inquisitiveness and scholastic proficiency, Mad Alchemists closely study the strange, warping properties of the Void Marker. They interact with the Void Marker much more closely than anyone else in Diamedulla; an endeavor that has not left them unscarred. As such, they are often a good deal more insane and unstable than Black Alchemists. Indeed, their irrationality is a common point of worry in Diamedulla, for even in the sea of madness that is Diamedulla there might be those who are considered just a little bit too mad. Over time their minds will become more and more warped, until they completely succumb to the Void and embrace it, thinking their raging madness a great wellspring of clarity. Such Mad Alchemists can no longer be helped by any means and will pose an ever increasing danger to the tentative stability of Diamedulla.
                        
                        Mad Alchemists will slowly lose their mind as time passes due to the influence the Void Marker exerts on those near it. As Mad Alchemists observe the passing of time as slowed, due to the Temporal Clocks they use, their actions consequently seem quickened. As such, Mad Alchemists are capable of casting spells much faster than normal."
                        #ap 12
                        #mapmove 14
                        #hp 10
                        #mr 15
                        #size 2
                        #str 9
                        #enc 3
                        #att 8
                        #def 8
                        #prec 10
                        #mor 50
                        #gcost 9990
                        #weapon "Dagger"
                        #armor 159
                        #magicskill 4 1
                        #magicskill 5 1
                        #custommagic 6144 100
                        #custommagic 6144 10
                        #startage 40
                        #poorleader
                        #rpcost 2
                        #insane 10
                        #researchbonus 3
                        #alchemy 50
                        #startitem 569
                        #end
                    -- Black Alchemist 
                        
                        
                        #newmonster 7418 
                        #copystats 551
                        #clearmagic
                        #name "Black Alchemist"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/BlackAlchemist1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/BlackAlchemist1.tga"
                        #descr "The Black Alchemists are the mad, abusive fathers of Diamedulla. They are those alchemists of Antikythera who either fell victim to the Void Marker's influence or were completely and utterly captivated by it. Black Alchemists are obsessed with the study of the Void Marker. Indeed, their obsession was so high that it caused a civil war. Once the Void Marker was first discovered in Antikythera and put to study, the strange properties of the artifacts were soon made evident, that those in its proximity would slowly succumb to insanity, their minds warped and twisted in unimaginable ways. It is then that a decision was made to destroy the Void Marker before its damage became irreparable, but those alchemists already fascinated by it and the secrets it might hold, or perhaps simply already mad from its influence, rebelled and lead a revolution that would eventually break the Imperium in two after much bloodshed and destruction. Whether Black Alchemists were always somewhat mad even in Antikythera, their madness simply veiled under a thin veneer of eccentricity often associated with the profession, or whether they indeed went mad from interacting with the Void Marker is unknown and ultimately irrelevant. . 
                        
                        Black Alchemists will slowly lose their mind as time passes due to the influence the Void Marker exerts on those near it. As Black Alchemists observe the passing of time as slowed, due to the Temporal Clocks they use, their actions consequently seem quickened. As such, Black Alchemists are capable of casting spells much faster than normal." 
                        #ap 12
                        #mapmove 14
                        #hp 10
                        #mr 16
                        #size 2
                        #str 9
                        #enc 3
                        #att 8
                        #def 8
                        #prec 10
                        #mor 12
                        #gcost 10005
                        #weapon "Fist"
                        #armor "Mask"
                        #armor 159
                        #magicskill 4 1
                        #magicskill 3 1
                        #magicskill 5 1
                        #magicskill 7 1
                        #custommagic 23552 100
                        #custommagic 23552 10
                        #slowrec
                        #startage 45
                        #twiceborn 7449
                        #insane 3
                        #startitem 569
                        #end
                        
                        #newmonster 7449
                        #name "Brain in a Jar"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/BrainJar1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/BrainJar1.tga"
                        #descr "A Brain in a Jar is a Black Alchemist whose head had been harvested upon his death and brought back to a Diamedullan lab to be revived and placed in a fluid container. The process of successfully reviving a head that had possibly been dead for days is a tricky one, and not all patients are revived, indeed the failure rate of such a complex procedure is as high as 30% and depends on many factors and random variables. Once placed in their respective jars, these bottled alchemists, as they are nicknamed, remain in Diamedullan laboratories and continue their work and research. Though they are incapable of performing many tasks in such a state, their peculiar state of existence allows them to conduct research with singular focus. Incidentally, the reviving procedure of these alchemists seems to inadvertently make them immune to the insidious influence of the Void Marker. As such, alchemists placed in a fluid containers will stop slowly losing their mind while conducting the research on the Void Marker, making them excellent research assistants."
                        #firstshape -14174
                        #ap 12
                        #mapmove 14
                        #hp 5
                        #mr 14
                        #size 1
                        #str 5
                        #enc 1
                        #att 5
                        #def 5
                        #prec 10
                        #mor 12
                        #maxage 150
                        #weapon "Mind Blast"
                        #neednoteat
                        #gcost 0
                        #noitem
                        #mastersmith -10
                        #userestricteditem 2233
                        #end
                        
                            #newmonster 7468
                            #name "Brain in a Jar"
                            #spr1 "./bozmod/Confluence/LA_Diamedulla/BrainJar1.tga"
                            #spr2 "./bozmod/Confluence/LA_Diamedulla/BrainJar1.tga"
                            #descr "A Brain in a Jar is a Black Alchemist whose head had been harvested upon his death and brought back to a Diamedullan lab to be revived and placed in a fluid container. The process of successfully reviving a head that had possibly been dead for days is a tricky one, and not all patients are revived, indeed the failure rate of such a complex procedure is as high as 30% and depends on many factors and random variables. Once placed in their respective jars, these bottled alchemists, as they are nicknamed, remain in Diamedullan laboratories and continue their work and research. Though they are incapable of performing many tasks in such a state, their peculiar state of existence allows them to conduct research with singular focus. Incidentally, the reviving procedure of these alchemists seems to inadvertently make them immune to the insidious influence of the Void Marker. As such, alchemists placed in a fluid containers will stop slowly losing their mind while conducting the research on the Void Marker, making them excellent research assistants."
                            #montag 14174
                            #landdamage 100
                            #uwdamage 100
                            #ap 12
                            #mapmove 14
                            #hp 5
                            #mr 14
                            #size 1
                            #str 5
                            #enc 1
                            #att 5
                            #def 5
                            #prec 10
                            #mor 12
                            #maxage 150
                            #weapon "Mind Blast"
                            #neednoteat
                            #gcost 0
                            #noitem
                            #mastersmith -10
                            #userestricteditem 2233
                            #end
                        --
                            #newmonster 7469 
                            #name "Brain in a Jar"
                            #spr1 "./bozmod/Confluence/LA_Diamedulla/BrainJar1.tga"
                            #spr2 "./bozmod/Confluence/LA_Diamedulla/BrainJar1.tga"
                            #descr "A Brain in a Jar is a Black Alchemist whose head had been harvested upon his death and brought back to a Diamedullan lab to be revived and placed in a fluid container. The process of successfully reviving a head that had possibly been dead for days is a tricky one, and not all patients are revived, indeed the failure rate of such a complex procedure is as high as 30% and depends on many factors and random variables. Once placed in their respective jars, these bottled alchemists, as they are nicknamed, remain in Diamedullan laboratories and continue their work and research. Though they are incapable of performing many tasks in such a state, their peculiar state of existence allows them to conduct research with singular focus. Incidentally, the reviving procedure of these alchemists seems to inadvertently make them immune to the insidious influence of the Void Marker. As such, alchemists placed in a fluid containers will stop slowly losing their mind while conducting the research on the Void Marker, making them excellent research assistants."
                            #montag 14174
                            #amphibian
                            #ap 12
                            #mapmove 14
                            #hp 5
                            #mr 14
                            #size 1
                            #str 5
                            #enc 1
                            #att 5
                            #def 5
                            #prec 10
                            #mor 12
                            #maxage 150
                            #weapon "Mind Blast"
                            #neednoteat
                            #gcost 0
                            #noitem
                            #mastersmith -10
                            #userestricteditem 2233
                            #end
                        --
                            #newmonster 7470
                            #name "Brain in a Jar"
                            #spr1 "./bozmod/Confluence/LA_Diamedulla/BrainJar1.tga"
                            #spr2 "./bozmod/Confluence/LA_Diamedulla/BrainJar1.tga"
                            #descr "A Brain in a Jar is a Black Alchemist whose head had been harvested upon his death and brought back to a Diamedullan lab to be revived and placed in a fluid container. The process of successfully reviving a head that had possibly been dead for days is a tricky one, and not all patients are revived, indeed the failure rate of such a complex procedure is as high as 30% and depends on many factors and random variables. Once placed in their respective jars, these bottled alchemists, as they are nicknamed, remain in Diamedullan laboratories and continue their work and research. Though they are incapable of performing many tasks in such a state, their peculiar state of existence allows them to conduct research with singular focus. Incidentally, the reviving procedure of these alchemists seems to inadvertently make them immune to the insidious influence of the Void Marker. As such, alchemists placed in a fluid containers will stop slowly losing their mind while conducting the research on the Void Marker, making them excellent research assistants."
                            #montag 14174
                            #amphibian
                            #ap 12
                            #mapmove 14
                            #hp 5
                            #mr 14
                            #size 1
                            #str 5
                            #enc 1
                            #att 5
                            #def 5
                            #prec 10
                            #mor 12
                            #maxage 150
                            #weapon "Mind Blast"
                            #neednoteat
                            #gcost 0
                            #noitem
                            #mastersmith -10
                            #userestricteditem 2233
                            #end
                    -- Assembly Wagon 
                    
                    
                        #newmonster 7466
                        #name "Assembly Wagon"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/AssemblyWagon1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/AssemblyWagon1.tga"
                        #hp 20
                        #descr "Ever since the reanimating properties of the Void Marker were first made evident in Diamedulla the dead are no longer buried, but collected, rather, either for testing and experimentation purposes or to be employed in the numerous clockwork factories. Indeed, the majority of the simple, menial labor in Diamedulla is now done by dead Workers, reanimated through the influence of the Void Marker and put to use by the Black Alchemists. 
                        An Assembly Wagon is a simple cart that collects the carcasses of the recently deceased and transports them to various industrial centers or laboratories, depending on the needs of either. Sometimes, the corpse filled wagons are even brought along with Diamedullan armies and then released upon foreign lands or armies."
                        #size 6
                        #batstartsum5 197
                        #batstartsum4 197 
                        #batstartsum3 197 
                        #batstartsum2 197
                        #batstartsum1 197
                        #batstartsum2d6 197 
                        #shrinkhp 999
                        #ap 6
                        #mapmove 10
                        #fear 15
                        #prot 10
                        #hp 30
                        #mr 14
                        #str 20
                        #enc 0
                        #att 5
                        #def 5
                        #prec 5
                        #mor 50
                        #gcost 90
                        #poisonres 25
                        #fireres -10
                        #noitem
                        #startage 35
                        #maxage 150
                        #rcost 35
                        #rpcost 42
                        #trample
                        #weapon "Crush"
                        #inanimate
                        #neednoteat
                        #woundfend 99
                        #pierceres
                        #siegebonus 15
                        #incscale 3
                        #noleader
                        #diseasecloud 6
                        #end
                        
                        #newmonster 7467
                        #name "Assembly Wagon"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/AssemblyWagon1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/AssemblyWagon1.tga"
                        #hp 20
                        #descr "Ever since the reanimating properties of the Void Marker were first made evident in Diamedulla the dead are no longer buried, but collected, rather, either for testing and experimentation purposes or to be employed in the numerous clockwork factories. Indeed, the majority of the simple, menial labor in Diamedulla is now done by dead Workers, reanimated through the influence of the Void Marker and put to use by the Black Alchemists. 
                        An Assembly Wagon is a simple cart that collects the carcasses of the recently deceased and transports them to various industrial centers or laboratories, depending on the needs of either. Sometimes, the corpse filled wagons are even brought along with Diamedullan armies and then released upon foreign lands or armies."
                        #size 6
                        #undisciplined
                        #batstartsum5 197
                        #batstartsum4 197 
                        #batstartsum3 197 
                        #batstartsum2 197
                        #batstartsum1 197
                        #batstartsum2d6 197 
                        #firstshape 7466
                        #ap 0
                        #mapmove 0
                        #immobile
                        #fear 15
                        #prot 10
                        #hp 30
                        #mr 14
                        #str 20
                        #enc 0
                        #att 5
                        #def 5
                        #prec 5
                        #mor 50
                        #gcost 100
                        #poisonres 25
                        #fireres -10
                        #noitem
                        #startage 35
                        #maxage 150
                        #rcost 35
                        #rpcost 42
                        #trample
                        #weapon "Crush"
                        #inanimate
                        #neednoteat
                        #woundfend 99
                        #pierceres
                        #siegebonus 15
                        #incscale 3
                        #noleader
                        #diseasecloud 6
                        #end
            -- Other Units
                    -- Green Specimen 1
                        
                        
                        #newmonster 7505
                        #name "Viral Specimen Tank"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/GreenTank1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/GreenTank1.tga"
                        #descr "The Viral Containment Tank contains a specimen which possesses a very rare trait. It is capable of causing powerful and rampant, yet stable mutations in those directly infected by it. It is still unknown exactly what traits and characteristics are requisite in a subject for successful transformation as some experiments seem to succeed remarkably, while others, with a seemingly identical group, on occasion fail to produce almost any results. Whatever the case may be, a successful experiment does seem to require a critical biomass. Typically, around one hundred Workers are needed for a successful experiment, sometimes less, and sometimes more, depending on the quality of the subjects themselves. Once infected, these Workers will rapidly mutate and merge - often violently, piercing and slashing at each other and growing into the bodies of those impaled - into a new class of Necromorphs called Necrobrutes due to their imposing size.A successful experiment will produce anywhere between fifteen and thirty Necrobrutes, often leaving a few excess Zombified Workers in their midst as well.
                        
                        Specimen Containment Tanks can be transported to Diamedullan laboratories by constructing and utilizing the Dimension Splitter."
                        #prot 18
                        #ap 2
                        #mapmove 0
                        #immobile
                        #hp 30
                        #mr 18
                        #size 4
                        #str 5
                        #enc 0
                        #att 5
                        #def 5
                        #prec 5
                        #mor 50
                        #gcost 0
                        #diseaseres 100
                        #poisonres 25
                        #noitem
                        #startage -1
                        #maxage 150
                        #rpcost 1
                        #noleader
                        #inanimate
                        #neednoteat
                        #montag 13781
                        #noitem
                        #userestricteditem 2234
                        #startitem 579
                        #end
                    -- Green Specimen 2
                    
                    
                        #newmonster 7506
                        #name "Parasyte Specimen Tank"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/GreenTank2.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/GreenTank2.tga"
                        #descr "The Parasyte Containment Tank possesses an exceedingly rare ability to further mutate Necromorphs and turn them into Necrobrutes, towering monstrosities with remarkable regenerative powers, often composed of several people. Typically, once a Necromorph is created, all further mutations within the creatures are halted. From then on, their sole purpose seems to become creating more infected bodies and horrifying creatures. With the help of a Parasyte, the twisted Black Alchemists are capable of restarting mutations in several Necromorphs each month as reintroducing Necromorphs to lab conditions safely can be an arduous and time-consuming process.
                        
                        Specimen Containment Tanks can be transported to Diamedullan laboratories by constructing and utilizing the Dimension Splitter."
                        #prot 18
                        #ap 2
                        #mapmove 0
                        #immobile
                        #hp 30
                        #mr 18
                        #size 4
                        #str 5
                        #enc 0
                        #att 5
                        #def 5
                        #prec 5
                        #mor 50
                        #gcost 0
                        #diseaseres 100
                        #poisonres 25
                        #noitem
                        #startage -1
                        #maxage 150
                        #rpcost 1
                        #noleader
                        #inanimate
                        #neednoteat
                        #montag 13781
                        #noitem
                        #userestricteditem 2234
                        #end
                    -- Green Specimen 3
                    
                        
                        #newmonster 7507
                        #name "Larval Specimen Tank"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/GreenTank3.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/GreenTank3.tga"
                        #descr "The Larval Containment Tank is one of the stranger specimens discovered by the Collector. Though the purpose of its mutations were at first unclear, it was not until it accidentally came in contact with a Virulent Priest that the rare traits of such specimen became apparent. The Larval Specimen Tank only ever seems to have effect on Virulent Priests, further cementing their delusions of being chosen to act as divine priests of the Void, announcing its coming. Virulent Priests that come into contact with such a specimen are mutated into Virulent Emissaries, much more infectious versions of themselves, and with higher priestly powers.
                        
                        Specimen Containment Tanks can be transported to Diamedullan laboratories by constructing and utilizing the Dimension Splitter."
                        #prot 18
                        #ap 2
                        #mapmove 0
                        #immobile
                        #hp 30
                        #mr 18
                        #size 4
                        #str 5
                        #enc 0
                        #att 5
                        #def 5
                        #prec 5
                        #mor 50
                        #gcost 0
                        #diseaseres 100
                        #poisonres 25
                        #noitem
                        #startage -1
                        #maxage 150
                        #rpcost 1
                        #noleader
                        #inanimate
                        #neednoteat
                        #montag 13781
                        #noitem
                        #userestricteditem 2234
                        #end
                    -- Specimen 1
                    
                        
                        #newmonster 7456
                        #name "Necromorphic Specimen Tank"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/NecroTank1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/NecroTank1.tga"
                        #descr "A Necromorphic Containment Tank holds a specimen whose unique properties can be used to increase the effectiveness of Spawning Sacks. Over a course of weeks, or months, depending on the number of Spawning Sacks located in any particular lab, the spawning rate of these Necromorph birthing devices will be increased. A Necromorphic Spawning Sack can mutate more people into Necromorphs, especially in lands where Diamedullan influence, and therefor the Void Marker's influence is stronger.
                        
                        Specimen Containment Tanks can be transported to Diamedullan laboratories by constructing and utilizing the Dimension Splitter."
                        #prot 18
                        #ap 2
                        #mapmove 0
                        #immobile
                        #hp 30
                        #mr 18
                        #size 4
                        #str 5
                        #enc 0
                        #att 5
                        #def 5
                        #prec 5
                        #mor 50
                        #gcost 0
                        #diseaseres 100
                        #poisonres 25
                        #noitem
                        #startage -1
                        #maxage 150
                        #rpcost 1
                        #noleader
                        #inanimate
                        #neednoteat
                        #montag 13781
                        #noitem
                        #userestricteditem 2234
                        #end
                    -- Specimen 2
                    
                    
                        #newmonster 7459
                        #name "Cerebellum Specimen Tank"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/BrainTank1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/BrainTank1.tga"
                        #descr "The Cerebellum specimens suffer similar mutations to the Cerebrum specimens in the respect that the mutations seem to affect, or rather expand intellectual capabilities to varying degrees. Though the Cerebellum falls flat compared to the Cerebrum in pure intellect and willpower, it still possesses some rather peculiar abilities, namely that of being able to imprint such mutations on others through nothing other than presence of mind and create new Cerebellum.
                        
                        Specimen Containment Tanks can be transported to Diamedullan laboratories by constructing and utilizing the Dimension Splitter."
                        #prot 18
                        #ap 2
                        #mapmove 0
                        #immobile
                        #hp 30
                        #mr 18
                        #size 4
                        #str 5
                        #enc 0
                        #att 5
                        #def 5
                        #prec 5
                        #mor 30
                        #gcost 0
                        #diseaseres 100
                        #poisonres 25
                        #noitem
                        #startage -1
                        #maxage 150
                        #rpcost 1
                        #noleader
                        #inanimate
                        #neednoteat
                        #weapon "Mind Blast"
                        #makemonsters1 7462
                        #montag 13781
                        #noitem
                        #userestricteditem 2234
                        #end
                        
                            #newmonster 7462
                            #name "Floating Spine"
                            #spr1 "./bozmod/Confluence/LA_Diamedulla/BrainSpine1.tga"
                            #spr2 "./bozmod/Confluence/LA_Diamedulla/BrainSpine1.tga"
                            #descr "A Floating Spine is a human forcibly mutated by the sheer dominating willpower of a Cerebellum. Though the Floating Spine is nothing more than a giant brain attached to a spine, it is still capable of floating and moving through willpower and presence of mind."
                            #prot 0
                            #ap 8
                            #mapmove 8
                            #hp 7
                            #mr 14
                            #size 2
                            #str 6
                            #enc 0
                            #att 7
                            #def 7
                            #prec 9
                            #mor 14
                            #gcost 0
                            #diseaseres 100
                            #noitem
                            #startage 33
                            #maxage 40
                            #rpcost 1
                            #noleader
                            #neednoteat
                            #weapon "Mind Blast"
                            #weapon 63 
                            #spiritsight
                            #poisonres 8
                            #float
                            #end
                    -- Specimen 3
                    
                        
                        #newmonster 7460
                        #name "Cerebrum Specimen Tank"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/BigBrainTank1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/BigBrainTank1.tga"
                        #descr "Every so often a rare specimen like a Cerebrum is discovered by a Collector and rigorously experimented on. The Cerebrum possesses truly remarkable mutations that seem to enhance its intellectual abilities by leaps and bounds. In fact, its mutations are so aggressive that the specimen basically becomes an enlarged heads. The intellectual capabilities of the Cerebrum are so incomprehensibly vast that it is able to blast the minds of lesser beings with sheer willpower.
                        
                        Specimen Containment Tanks can be transported to Diamedullan laboratories by constructing and utilizing the Dimension Splitter."
                        #prot 18
                        #ap 2
                        #mapmove 0
                        #immobile
                        #hp 30
                        #mr 18
                        #size 4
                        #str 5
                        #enc 0
                        #att 5
                        #def 5
                        #prec 5
                        #mor 50
                        #gcost 0
                        #diseaseres 100
                        #poisonres 25
                        #noitem
                        #startage -1
                        #maxage 150
                        #rpcost 1
                        #noleader
                        #inanimate
                        #neednoteat
                        #magicskill 4 4
                        #weapon 274
                        #weapon "Mind Blast"
                        #weapon "Mind Blast"
                        #spiritsight
                        #researchbonus 20
                        #montag 13781
                        #noitem
                        #userestricteditem 2234
                        #end
                    -- Specimen 4
                    
                    
                        #newmonster 7461
                        #name "Necrotizing Specimen Tank"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/UndeadTank1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/UndeadTank1.tga"
                        #descr "Necrotizing specimens are abject failures in every single regard but one; they are seemingly impossible to kill and can suffer massive amounts of damage while still somehow clinging to life even with no regenerative capabilities. This rather fascinating trait makes them excellent research subjects as they can withstand countless vivisections and experimental procedures. The research conducted on these specimens has made it possible to replicate their mutation and create the Necrotic Spines. Though such attempts seem to be directly tied to the strength of the Void Marker's influence in the given lab. The stronger the dominion of the Void Marker in the lab, the easier it will be to create Necrotic Spines, though it still remains a difficult and slow procedure regardless.
                        
                        Specimen Containment Tanks can be transported to Diamedullan laboratories by constructing and utilizing the Dimension Splitter."
                        #prot 18
                        #ap 2
                        #mapmove 0
                        #immobile
                        #hp 30
                        #mr 18
                        #size 4
                        #str 5
                        #enc 0
                        #att 5
                        #def 5
                        #prec 5
                        #mor 50
                        #gcost 0
                        #diseaseres 100
                        #poisonres 25
                        #noitem
                        #startage -1
                        #maxage 150
                        #rpcost 1
                        #noleader
                        #inanimate
                        #neednoteat
                        #eyes 0
                        #bluntres
                        #pierceres
                        #slashres
                        #domsummon20 7463
                        #montag 13781
                        #noitem
                        #userestricteditem 2234
                        #end
                        
                            #newmonster 7463
                            #name "Necrotic Spine"
                            #spr1 "./bozmod/Confluence/LA_Diamedulla/CorpseBomb1.tga"
                            #spr2 "./bozmod/Confluence/LA_Diamedulla/CorpseBomb1.tga"
                            #descr "Necrotic Spines are utterly mutilated subjects infected with the Necrotizing specimen's mutagen. Though the Necrotic Spine is still alive, it is only in the most technical of senses, as all sentience has long been either carved out of the creature, or simply rejected out of sheer pain and the abject horror of its creation. These subjects are carefully vivisected and their insides filled with volatile alchemical compounds, making them walking bombs. Hacking this creature to bits will trigger an explosion which will likely outright kill or paralyze all those unfortunate enough to be caught nearby."
                            #prot 8
                            #ap 8
                            #mapmove 16
                            #hp 11
                            #mr 14
                            #size 3
                            #str 11
                            #enc 0
                            #att 11
                            #def 7
                            #prec 7
                            #mor 50
                            #gcost 0
                            #bluntres
                            #pierceres
                            #slashres
                            #diseaseres 100
                            #spiritsight
                            #poisonres 25
                            #noitem
                            #startage 33
                            #maxage 500
                            #rpcost 1
                            #noleader
                            #neednoteat
                            #weapon "Mutated Limb"
                            #weapon "Mutated Limb"
                            #float
                            #insane 100
                            #deathparalyze 5
                            #deathfire 5
                            #end
            -- Items
                -- Temporal Clock                     
                    
                    #selectitem 569
                    #copyitem "Dimensional Rod"
                    #spr "./bozmod/Confluence/LA_Diamedulla/TimeClock.tga"
                    #name "Temporal Clock"
                    #type 8
                    #weapon 0
                    #tainted 0
                    #magicboost 4 0
                    #descr "Temporal Clocks distort reality in a bubble around their owner. Those in the time bubble will experience the passing of time as somewhat slowed down. Comparatively, the actions of those inside the time bubble will appear superhumanly fast to anyone outside it. The invention of the Temporal Clock is a tightly held Diamedullan secret derived from Void Marker research and its fascinating ability to warp time and space around it. Its existance is only known to the Black Alchemists and Mad Alechemists of Diamedulla who use it to significantly speed up their research of the Marker. As a side-effect of the Temporal Clock, those who use it will age much more quickly as time passes faster for them while inside the time bubble."
                    #constlevel 12
                    #nofind
                    #cursed
                    #mainpath 4
                    #mainlevel 4
                    #secondarypath 3
                    #secondarylevel 4
                    #astralrange 0
                    #researchbonus 5
                    #fastcast 30
                    #restricted 169
                    #end
                -- Viral Amplifier 
                    
                    
                        #newspell
                        #name "Viral Amplification"
                        #descr "Starts an experiment in which roughly a hundred Workers are infected and turned into Necrobrutes."
                        #details "Turns roughly one hundred Workers into a few dozen Necrobrutes. Exact requirements can vary. Success rate of the experiment is 80%, assuming there are enough Workers to conduct it. If there are fewer Workers than required by the specific roll, the experiment will always fail."
                        #school -1 
                        #researchlevel 0
                        #effect 10082
                        #damage 849
                        #end
                        
                            #newevent              -- No Workers to trigger event
                            #rarity 5
                            #req_pop0ok
                            #req_nomonster 7400
                            #req_fornation 169
                            #req_ench 849
                            #msg "There were no Workers to infect with the Viral Containment Tank and conduct an experiment on."
                            #end 
                            
                            #newevent              -- Always gives Workers when experiment is triggered (if Workers are present)
                            #rarity 5
                            #req_pop0ok
                            #req_5monsters 7400
                            #req_fornation 169
                            #req_ench 849
                            #msg "No text needed." -- Gives some zombies regardless of failed exp or not
                            #nation 169
                            #1unit 7439
                            #1unit 7439
                            #1unit 7439
                            #1unit 7439
                            #1unit 7439
                            #notext
                            #nolog
                            #end 
                        
                            #newevent
                            #rarity 5
                            #req_pop0ok
                            #req_fornation 169
                            #req_ench 849
                            #msg "Workers killed 15x 2d6." -- Kills around 100 Workers -- assuming there are that many
                            #req_5monsters 7400
                            #kill2d6mon 7400
                            #kill2d6mon 7400
                            #kill2d6mon 7400
                            #kill2d6mon 7400
                            #kill2d6mon 7400
                            #kill2d6mon 7400
                            #kill2d6mon 7400
                            #kill2d6mon 7400
                            #kill2d6mon 7400
                            #kill2d6mon 7400
                            #kill2d6mon 7400
                            #kill2d6mon 7400
                            #kill2d6mon 7400
                            #kill2d6mon 7400
                            #kill2d6mon 7400
                            #stealthcom 7531
                            #notext
                            #nolog
                            #end	
                    
                                #newevent
                                #rarity 5
                                #req_rare 80      -- 80% chance to gain Necrobrutes, Spawns a dummy too
                                #req_pop0ok
                                #req_fornation 169
                                #req_ench 849
                                #msg "Enables Necrobrute spawning." 
                                #req_5monsters 7400
                                #stealthcom 7532
                                #notext
                                #nolog
                                #end		

                            #newevent
                            #rarity 5
                            #req_pop0ok
                            #req_fornation 169
                            #req_ench 849
                            #req_monster 7532
                            #msg "We have successfully conducted the experiment and mutated scores of Workers into a few dozen Necrobrutes."              -- Gain around 20 Necrobrutes
                            #nation 169
                            #1unit -13478
                            #1unit -13478
                            #1unit -13478
                            #1unit -13478
                            #1unit -13478
                            #1unit -13478
                            #1unit -13478
                            #1unit -13478
                            #1unit -13478
                            #1unit -13478
                            #3d3units -13478
                            #3d3units -13478
                            #end

                        #newevent
                        #rarity 5
                        #req_pop0ok
                        #req_nomonster 7532
                        #req_fornation 169
                        #req_ench 849
                        #msg "The experiment has failed! The Necrobrutes have wasted away before fully finalizing their transformation. Only a few Zombified Workers remain for the effort." - Experiment has failed message
                        #end
                        
                            #newevent
                            #rarity 5
                            #req_pop0ok
                            #req_5monsters 7400
                            #req_fornation 169
                            #req_ench 849
                            #msg "No text needed." -- Gain additional Zombie Workers assuming there were enough left over
                            #nation 169
                            #5d6units 7439
                            #notext
                            #nolog
                            #end  
                    
                    #selectitem 579
                    #spr "./bozmod/Confluence/LA_Diamedulla/ViralAmplifier.tga"
                    #name "Viral Amplifier"
                    #descr "The Viral Amplifier enables the transformation of simple Workers to powerful Necrobrutes."
                    #constlevel 12
                    #mainpath 5
                    #mainlevel 3
                    #secondarypath 4
                    #secondarylevel 3
                    #nofind
                    #cursed
                    #spell "Viral Amplification"
                    #end
                    
                            #newmonster 7531
                            #copyspr 1369
                            #copystats 1369
                            #stealthy 999
                            #name "Viral Amplifier Dummy"
                            #descr "No text needed."
                            #mr 50
                            #mor 50
                            #hp 100
                            #landdamage 120
                            #invisible
                            #end
                            
                            #newmonster 7532
                            #copyspr 1369
                            #copystats 1369
                            #stealthy 999
                            #name "Viral Amplifier Dummy"
                            #descr "No text needed."
                            #mr 50
                            #mor 50
                            #hp 100
                            #landdamage 120
                            #invisible
                            #end         
                -- Dimension Splitter 
                    
                    
                    #selectitem 578
                    #spr "./bozmod/Confluence/LA_Diamedulla/Portal.tga"
                    #name "Dimension Splitter"
                    #descr "The Dimension Splitter is a device capable of ripping a hole through the fabric of reality. It is used by the mad Black Alchemists to quickly transport collected Specimen that exhibit certain peculiar characteristics and quirks to the torture chambers they call laboratories for further study. Though anything can be sent through the Dimension Splitter, only objects locked in containment tanks can endure the warped portal created by this device. Living beings sent through the Dimension Splitter arrive on the other side in horrifying condition, their skin and innards torn and twisted around their bodies.
                    The Dimension Splitter is exceedingly dangerous even when inert. The mere presence of this machine will slowly pollute the land and kill any living thing in an ever expanding circle due to its unnatural and contaminated source of power. " - The thing being teleported must be in a containment sphere
                    #constlevel 2
                    #mainpath 4
                    #mainlevel 2
                    #secondarypath 5
                    #secondarylevel 1
                    #restricteditem 2234
                    #nofind
                    #spell "Teleport"
                    #leper 2
                    #restricted 169
                    #end
                -- Payload 
                    
                    
                    #selectitem 577
                    #spr "./bozmod/Confluence/LA_Diamedulla/PoisonBarrel.tga"
                    #name "Noxious Payload"
                    #descr "A Noxious Payload is a bioalchemical weapon created by the Black Alchemists that spreads much more accelerated mutations which inevitable turn those infected into Nercomorphs. Payloads are extreme hard to disperse over large areas and as such need to be released from considerable heights and the aid of favorable winds to better help spread the compound. Even then, the compound can only infect and kill so many due to the high concentrations needed."
                    #cursed
                    #constlevel 4
                    #nofind
                    #mainpath 6
                    #mainlevel 2
                    #secondarypath 5
                    #secondarylevel 2
                    #restricteditem 2233 -- Restricted to Zeppelin
                    #deathdisease 15
                    #cursed
                    #nofind
                    #restricted 169
                    #end
                -- Assemble/Disassemble Items 
                    
                    
                    #newspell
                    #name "Assemble"
                    #descr "Unifies a Baneful Lantern, a Expedient Collector, and Psychic Construct into a single being, the Diamedullan Trinity."
                    #details "Assembles Trinity. A Trinity can only be assembled in the Clockwork Horror, present in the capital of Diamedulla. Only one Trinity may be assembled per month."
                    #school -1 
                    #researchlevel 0
                    #effect 10082
                    #damage 846
                    #end
                    --
                    #newspell
                    #name "Disassemble"
                    #descr "Disassembles the Diamedullan Trinity into its composing parts, a Baneful Lantern, a Expedient Collector, and Psychic Construct."
                    #details "Though a Trinity may be disassembled anywhere and does not require a Clockwork Horror, only one Trinity may be disassembled per month in any single land."
                    #school -1 
                    #researchlevel 0
                    #effect 10082
                    #damage 845
                    #end
                    
                    
                    #selectitem 575
                    #spr "./bozmod/Confluence/LA_Diamedulla/TrinityGear.tga"
                    #name "Assembly Module"
                    #descr "A module that enables the assembly of multiple creatures into one composite form."
                    #cursed
                    #constlevel 12
                    #nofind
                    #mainpath 3
                    #mainlevel 4
                    #secondarypath 7
                    #secondarylevel 4
                    #spell "Assemble"
                    #cursed
                    #nofind
                    #end
                    
                    #selectitem 576
                    #spr "./bozmod/Confluence/LA_Diamedulla/TrinityGear.tga"
                    #name "Disassembly Module"
                    #descr "A module that enables the disassembly of creatures merged into a singular, composite form."
                    #cursed
                    #constlevel 12
                    #nofind
                    #mainpath 3
                    #mainlevel 4
                    #secondarypath 7
                    #secondarylevel 4
                    #spell "Disassemble"
                    #cursed
                    #nofind
                    #end
                -- Collector Items 
                    
                    
                    #selectitem 574
                    #spr "./bozmod/Confluence/LA_Diamedulla/SurgicalInstruments.tga"
                    #name "Surgical Instruments"
                    #descr "A set of grisly instruments that help the Collectors better conduct their experiments."
                    #cursed
                    #constlevel 12
                    #nofind
                    #mainpath 5
                    #mainlevel 4
                    #secondarypath 7
                    #secondarylevel 4
                    #spell "Initiate Experiment"
                    #end
                -- Void Marker Items 
                    
                    
                    #selectitem 570
                    #spr "./bozmod/Confluence/LA_Diamedulla/MarkerItem1.tga"
                    #name "Void Pulse"
                    #descr "The Void Pulse is one of the three main components of the Void Marker. It possesses the remarkable ability to rip an ever expanding hole in the fabric of reality until the entire world is eventually infested by the Void. Once activated, the minds of all the people in the world will be forcibly exposed to the lingering effects of the Void. Strange sights and sounds will flood the mind of the people and horrifying creatures called Void maggots will settle within them, slowly devouring their sanity from within.
                    Once activated the Void Pulse cannot be disrupted short of destroying the Void Marker."
                    #constlevel 12
                    #nofind
                    #restricteditem 1010
                    #mainpath 4
                    #mainlevel 4
                    #secondarypath 3
                    #secondarylevel 4
                    #end
                    
                    #selectitem 571
                    #spr "./bozmod/Confluence/LA_Diamedulla/MarkerItem2.tga"
                    #name "Morphing Matrix"
                    #descr "The Morphing Matrix is one of the three main components of the Void Marker.It possesses the remarkable ability to rapidly mutate all those under the Void Marker's direct dominion and sphere of influence. Once activated, rampant mutations will quickly turn the population of Diamedulla into the monstrous Necromorphs.
                    
                    Once activated the Morphing Matrix cannot be disrupted short of destroying the Void Marker."
                    #constlevel 12
                    #nofind
                    #restricteditem 1010
                    #mainpath 4
                    #mainlevel 4
                    #secondarypath 3
                    #secondarylevel 4
                    #end
                    
                    #selectitem 572
                    #spr "./bozmod/Confluence/LA_Diamedulla/MarkerItem3.tga"
                    #name "Convergence Sphere"
                    #descr "The Convergence Sphere is one of the three main components of the Void Marker, and by far the deadliest one. It possesses the remarkable power to extend the influence of the Void Marker over the entire world, and amplify it tenfold. Random mutations and tumorous growths will quickly afflict people all across the world once the sphere is activated, and turn them into Necromorphs in a cascading event. Uninhibited, the Convergence Sphere is sure to cull the entire world.
                    
                    Once activated the Convergence Sphere cannot be disrupted in any way short of destroying the Void Marker."
                    #constlevel 12
                    #nofind
                    #restricteditem 1010
                    #mainpath 4
                    #mainlevel 4
                    #secondarypath 3
                    #secondarylevel 4
                    #end
            -- Province Defence
                -- Recombinant 
                    
                    -- 7HP start
                    
                    #newmonster 7483
                    #name "Recombinant" 
                    #spr1 "./bozmod/Confluence/LA_Diamedulla/RecombinantHigh1.tga"
                    #spr2 "./bozmod/Confluence/LA_Diamedulla/RecombinantHigh1.tga"
                    #hp 7
                    #descr "Given free reign, and an endless supply of test subjects, the twisted scientific minds of Diamedulla have invented many horrifying devices over the years. Perhaps the most sickening of such horrors are the Recombinant, those miserable souls impaled on strange devices called 'Dragon's Teeth'. Though impaled, these poor souls somehow remain alive and fully conscious for an extreemly long time, while slowly being twisted and turned into abominations over a course of agonizing weeks, or even months, by the same impaling device through vile black alchemy. 
                    Dragon's Teeth are typically placed on Diamedullan borders, or in front of their secretive fortification to ward off intruders. When approached, the impaling devices will slowly release the Recombinant while also injecting them with one last compound, the final, cruel touch of their deranged creators. Once released, the Recombinant will charge at their enemies and, once close enough - or near death, give off a powerful alchemical explosion, killing those nearby or immobilizing them with foul fumes for other Recombinant to more easily rip them apart. Fully transformed Recombinant do not retain any vestiges of their former personalities and are just mindless, empty husks."
                    #growhp 7
                    #regeneration 5
                    #immobile
                    #ap 2
                    #mapmove 0
                    #mr 10
                    #size 6
                    #str 14
                    #enc 3
                    #att 9
                    #def 9
                    #prec 10
                    #mor 50
                    #gcost 0
                    #undisciplined
                    #noitem
                    #noleader
                    #poisonres 25
                    #neednoteat
                    #montag 13777
                    #fireres 15 
                    #end
                    
                    #newmonster 7482
                    #name "Recombinant" 
                    #spr1 "./bozmod/Confluence/LA_Diamedulla/RecombinantHigh2.tga"
                    #spr2 "./bozmod/Confluence/LA_Diamedulla/RecombinantHigh2.tga"
                    #hp 12
                    #descr "Given free reign, and an endless supply of test subjects, the twisted scientific minds of Diamedulla have invented many horrifying devices over the years. Perhaps the most sickening of such horrors are the Recombinant, those miserable souls impaled on strange devices called 'Dragon's Teeth'. Though impaled, these poor souls somehow remain alive and fully conscious for an extreemly long time, while slowly being twisted and turned into abominations over a course of agonizing weeks, or even months, by the same impaling device through vile black alchemy. 
                    Dragon's Teeth are typically placed on Diamedullan borders, or in front of their secretive fortification to ward off intruders. When approached, the impaling devices will slowly release the Recombinant while also injecting them with one last compound, the final, cruel touch of their deranged creators. Once released, the Recombinant will charge at their enemies and, once close enough - or near death, give off a powerful alchemical explosion, killing those nearby or immobilizing them with foul fumes for other Recombinant to more easily rip them apart. Fully transformed Recombinant do not retain any vestiges of their former personalities and are just mindless, empty husks."
                    #growhp 8
                    #regeneration 5
                    #immobile
                    #ap 2
                    #mapmove 0
                    #mr 10
                    #size 6
                    #str 14
                    #enc 3
                    #att 9
                    #def 9
                    #prec 10
                    #mor 50
                    #gcost 0
                    #undisciplined
                    #noitem
                    #noleader
                    #poisonres 25
                    #neednoteat
                    #fireres 15 
                    #end
                    
                    #newmonster 7481
                    #name "Recombinant" 
                    #spr1 "./bozmod/Confluence/LA_Diamedulla/RecombinantMedium1.tga"
                    #spr2 "./bozmod/Confluence/LA_Diamedulla/RecombinantMedium1.tga"
                    #hp 12
                    #descr "Given free reign, and an endless supply of test subjects, the twisted scientific minds of Diamedulla have invented many horrifying devices over the years. Perhaps the most sickening of such horrors are the Recombinant, those miserable souls impaled on strange devices called 'Dragon's Teeth'. Though impaled, these poor souls somehow remain alive and fully conscious for an extreemly long time, while slowly being twisted and turned into abominations over a course of agonizing weeks, or even months, by the same impaling device through vile black alchemy. 
                    Dragon's Teeth are typically placed on Diamedullan borders, or in front of their secretive fortification to ward off intruders. When approached, the impaling devices will slowly release the Recombinant while also injecting them with one last compound, the final, cruel touch of their deranged creators. Once released, the Recombinant will charge at their enemies and, once close enough - or near death, give off a powerful alchemical explosion, killing those nearby or immobilizing them with foul fumes for other Recombinant to more easily rip them apart. Fully transformed Recombinant do not retain any vestiges of their former personalities and are just mindless, empty husks."
                    #growhp 9
                    #regeneration 5
                    #immobile
                    #ap 2
                    #mapmove 0
                    #mr 10
                    #size 6
                    #str 14
                    #enc 3
                    #att 9
                    #def 9
                    #prec 10
                    #mor 50
                    #gcost 0
                    #undisciplined
                    #noitem
                    #noleader
                    #poisonres 25
                    #neednoteat
                    #fireres 15 
                    #end
                    
                    #newmonster 7480
                    #name "Recombinant" 
                    #spr1 "./bozmod/Confluence/LA_Diamedulla/RecombinantMedium2.tga"
                    #spr2 "./bozmod/Confluence/LA_Diamedulla/RecombinantMedium2.tga"
                    #hp 12
                    #descr "Given free reign, and an endless supply of test subjects, the twisted scientific minds of Diamedulla have invented many horrifying devices over the years. Perhaps the most sickening of such horrors are the Recombinant, those miserable souls impaled on strange devices called 'Dragon's Teeth'. Though impaled, these poor souls somehow remain alive and fully conscious for an extreemly long time, while slowly being twisted and turned into abominations over a course of agonizing weeks, or even months, by the same impaling device through vile black alchemy. 
                    Dragon's Teeth are typically placed on Diamedullan borders, or in front of their secretive fortification to ward off intruders. When approached, the impaling devices will slowly release the Recombinant while also injecting them with one last compound, the final, cruel touch of their deranged creators. Once released, the Recombinant will charge at their enemies and, once close enough - or near death, give off a powerful alchemical explosion, killing those nearby or immobilizing them with foul fumes for other Recombinant to more easily rip them apart. Fully transformed Recombinant do not retain any vestiges of their former personalities and are just mindless, empty husks."
                    #growhp 10
                    #regeneration 5
                    #immobile
                    #ap 2
                    #mapmove 0
                    #mr 10
                    #size 6
                    #str 14
                    #enc 3
                    #att 9
                    #def 9
                    #prec 10
                    #mor 50
                    #gcost 0
                    #undisciplined
                    #noitem
                    #noleader
                    #poisonres 25
                    #neednoteat
                    #fireres 15 
                    #end
                    
                    #newmonster 7479
                    #name "Recombinant" 
                    #spr1 "./bozmod/Confluence/LA_Diamedulla/RecombinantLow1.tga"
                    #spr2 "./bozmod/Confluence/LA_Diamedulla/RecombinantLow1.tga"
                    #hp 12
                    #descr "Given free reign, and an endless supply of test subjects, the twisted scientific minds of Diamedulla have invented many horrifying devices over the years. Perhaps the most sickening of such horrors are the Recombinant, those miserable souls impaled on strange devices called 'Dragon's Teeth'. Though impaled, these poor souls somehow remain alive and fully conscious for an extreemly long time, while slowly being twisted and turned into abominations over a course of agonizing weeks, or even months, by the same impaling device through vile black alchemy. 
                    Dragon's Teeth are typically placed on Diamedullan borders, or in front of their secretive fortification to ward off intruders. When approached, the impaling devices will slowly release the Recombinant while also injecting them with one last compound, the final, cruel touch of their deranged creators. Once released, the Recombinant will charge at their enemies and, once close enough - or near death, give off a powerful alchemical explosion, killing those nearby or immobilizing them with foul fumes for other Recombinant to more easily rip them apart. Fully transformed Recombinant do not retain any vestiges of their former personalities and are just mindless, empty husks."
                    #growhp 11
                    #regeneration 5
                    #immobile
                    #ap 2
                    #mapmove 0
                    #mr 10
                    #size 6
                    #str 14
                    #enc 3
                    #att 9
                    #def 9
                    #prec 10
                    #mor 50
                    #gcost 0
                    #undisciplined
                    #noitem
                    #noleader
                    #poisonres 25
                    #neednoteat
                    #fireres 15 
                    #end
                    
                    #newmonster 7478
                    #name "Recombinant" 
                    #spr1 "./bozmod/Confluence/LA_Diamedulla/RecombinantLow2.tga"
                    #spr2 "./bozmod/Confluence/LA_Diamedulla/RecombinantLow2.tga"
                    #hp 12
                    #descr "Given free reign, and an endless supply of test subjects, the twisted scientific minds of Diamedulla have invented many horrifying devices over the years. Perhaps the most sickening of such horrors are the Recombinant, those miserable souls impaled on strange devices called 'Dragon's Teeth'. Though impaled, these poor souls somehow remain alive and fully conscious for an extreemly long time, while slowly being twisted and turned into abominations over a course of agonizing weeks, or even months, by the same impaling device through vile black alchemy. 
                    Dragon's Teeth are typically placed on Diamedullan borders, or in front of their secretive fortification to ward off intruders. When approached, the impaling devices will slowly release the Recombinant while also injecting them with one last compound, the final, cruel touch of their deranged creators. Once released, the Recombinant will charge at their enemies and, once close enough - or near death, give off a powerful alchemical explosion, killing those nearby or immobilizing them with foul fumes for other Recombinant to more easily rip them apart. Fully transformed Recombinant do not retain any vestiges of their former personalities and are just mindless, empty husks."
                    #growhp 12
                    #regeneration 5
                    #immobile
                    #ap 2
                    #mapmove 0
                    #mr 10
                    #size 6
                    #str 14
                    #enc 3
                    #att 9
                    #def 9
                    #prec 10
                    #mor 50
                    #gcost 0
                    #undisciplined
                    #noitem
                    #noleader
                    #poisonres 25
                    #neednoteat
                    #fireres 15 
                    #end
                    
                    #newmonster 7477
                    #name "Recombinant" 
                    #spr1 "./bozmod/Confluence/LA_Diamedulla/Recombinant1.tga"
                    #spr2 "./bozmod/Confluence/LA_Diamedulla/Recombinant1.tga"
                    #hp 12
                    #descr "Given free reign, and an endless supply of test subjects, the twisted scientific minds of Diamedulla have invented many horrifying devices over the years. Perhaps the most sickening of such horrors are the Recombinant, those miserable souls impaled on strange devices called 'Dragon's Teeth'. Though impaled, these poor souls somehow remain alive and fully conscious for an extreemly long time, while slowly being twisted and turned into abominations over a course of agonizing weeks, or even months, by the same impaling device through vile black alchemy. 
                    Dragon's Teeth are typically placed on Diamedullan borders, or in front of their secretive fortification to ward off intruders. When approached, the impaling devices will slowly release the Recombinant while also injecting them with one last compound, the final, cruel touch of their deranged creators. Once released, the Recombinant will charge at their enemies and, once close enough - or near death, give off a powerful alchemical explosion, killing those nearby or immobilizing them with foul fumes for other Recombinant to more easily rip them apart. Fully transformed Recombinant do not retain any vestiges of their former personalities and are just mindless, empty husks."
                    #deathfire 3
                    #deathparalyze 5
                    #ap 16
                    #mapmove 16
                    #mr 10
                    #size 2
                    #formationfighter -4
                    #str 14
                    #enc 3
                    #att 11
                    #def 9
                    #prec 10
                    #mor 50
                    #gcost 0
                    #undisciplined
                    #noitem
                    #noleader
                    #poisonres 25
                    #neednoteat
                    #weapon "Augmented Limb"
                    #weapon "Augmented Limb"
                    #coldres 5
                    #heat 3
                    #fireres 15 
                    #end
                    
                    -- 8HP start
                    
                        #newmonster 7489
                        #name "Recombinant" 
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/RecombinantHigh2.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/RecombinantHigh2.tga"
                        #hp 8
                    #descr "Given free reign, and an endless supply of test subjects, the twisted scientific minds of Diamedulla have invented many horrifying devices over the years. Perhaps the most sickening of such horrors are the Recombinant, those miserable souls impaled on strange devices called 'Dragon's Teeth'. Though impaled, these poor souls somehow remain alive and fully conscious for an extreemly long time, while slowly being twisted and turned into abominations over a course of agonizing weeks, or even months, by the same impaling device through vile black alchemy. 
                    Dragon's Teeth are typically placed on Diamedullan borders, or in front of their secretive fortification to ward off intruders. When approached, the impaling devices will slowly release the Recombinant while also injecting them with one last compound, the final, cruel touch of their deranged creators. Once released, the Recombinant will charge at their enemies and, once close enough - or near death, give off a powerful alchemical explosion, killing those nearby or immobilizing them with foul fumes for other Recombinant to more easily rip them apart. Fully transformed Recombinant do not retain any vestiges of their former personalities and are just mindless, empty husks."
                        #growhp 8
                        #regeneration 5
                        #immobile
                        #ap 2
                        #mapmove 0
                        #mr 10
                        #size 6
                        #str 14
                        #enc 3
                        #att 9
                        #def 9
                        #prec 10
                        #mor 50
                        #gcost 0
                        #undisciplined
                        #noitem
                        #noleader
                        #poisonres 25
                        #neednoteat
                        #montag 13777
                        #fireres 15 
                        #end
                    
                        #newmonster 7488
                        #name "Recombinant" 
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/RecombinantMedium1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/RecombinantMedium1.tga"
                        #hp 12
                    #descr "Given free reign, and an endless supply of test subjects, the twisted scientific minds of Diamedulla have invented many horrifying devices over the years. Perhaps the most sickening of such horrors are the Recombinant, those miserable souls impaled on strange devices called 'Dragon's Teeth'. Though impaled, these poor souls somehow remain alive and fully conscious for an extreemly long time, while slowly being twisted and turned into abominations over a course of agonizing weeks, or even months, by the same impaling device through vile black alchemy. 
                    Dragon's Teeth are typically placed on Diamedullan borders, or in front of their secretive fortification to ward off intruders. When approached, the impaling devices will slowly release the Recombinant while also injecting them with one last compound, the final, cruel touch of their deranged creators. Once released, the Recombinant will charge at their enemies and, once close enough - or near death, give off a powerful alchemical explosion, killing those nearby or immobilizing them with foul fumes for other Recombinant to more easily rip them apart. Fully transformed Recombinant do not retain any vestiges of their former personalities and are just mindless, empty husks."
                        #growhp 9
                        #regeneration 5
                        #immobile
                        #ap 2
                        #mapmove 0
                        #mr 10
                        #size 6
                        #str 14
                        #enc 3
                        #att 9
                        #def 9
                        #prec 10
                        #mor 50
                        #gcost 0
                        #undisciplined
                        #noitem
                        #noleader
                        #poisonres 25
                        #neednoteat
                        #fireres 15 
                        #end
                    
                        #newmonster 7487
                        #name "Recombinant" 
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/RecombinantMedium2.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/RecombinantMedium2.tga"
                        #hp 12
                    #descr "Given free reign, and an endless supply of test subjects, the twisted scientific minds of Diamedulla have invented many horrifying devices over the years. Perhaps the most sickening of such horrors are the Recombinant, those miserable souls impaled on strange devices called 'Dragon's Teeth'. Though impaled, these poor souls somehow remain alive and fully conscious for an extreemly long time, while slowly being twisted and turned into abominations over a course of agonizing weeks, or even months, by the same impaling device through vile black alchemy. 
                    Dragon's Teeth are typically placed on Diamedullan borders, or in front of their secretive fortification to ward off intruders. When approached, the impaling devices will slowly release the Recombinant while also injecting them with one last compound, the final, cruel touch of their deranged creators. Once released, the Recombinant will charge at their enemies and, once close enough - or near death, give off a powerful alchemical explosion, killing those nearby or immobilizing them with foul fumes for other Recombinant to more easily rip them apart. Fully transformed Recombinant do not retain any vestiges of their former personalities and are just mindless, empty husks."
                        #growhp 10
                        #regeneration 5
                        #immobile
                        #ap 2
                        #mapmove 0
                        #mr 10
                        #size 6
                        #str 14
                        #enc 3
                        #att 9
                        #def 9
                        #prec 10
                        #mor 50
                        #gcost 0
                        #undisciplined
                        #noitem
                        #noleader
                        #poisonres 25
                        #neednoteat
                        #fireres 15 
                        #end
                    
                        #newmonster 7486
                        #name "Recombinant" 
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/RecombinantLow1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/RecombinantLow1.tga"
                        #hp 12
                    #descr "Given free reign, and an endless supply of test subjects, the twisted scientific minds of Diamedulla have invented many horrifying devices over the years. Perhaps the most sickening of such horrors are the Recombinant, those miserable souls impaled on strange devices called 'Dragon's Teeth'. Though impaled, these poor souls somehow remain alive and fully conscious for an extreemly long time, while slowly being twisted and turned into abominations over a course of agonizing weeks, or even months, by the same impaling device through vile black alchemy. 
                    Dragon's Teeth are typically placed on Diamedullan borders, or in front of their secretive fortification to ward off intruders. When approached, the impaling devices will slowly release the Recombinant while also injecting them with one last compound, the final, cruel touch of their deranged creators. Once released, the Recombinant will charge at their enemies and, once close enough - or near death, give off a powerful alchemical explosion, killing those nearby or immobilizing them with foul fumes for other Recombinant to more easily rip them apart. Fully transformed Recombinant do not retain any vestiges of their former personalities and are just mindless, empty husks."
                        #growhp 11
                        #regeneration 5
                        #immobile
                        #ap 2
                        #mapmove 0
                        #mr 10
                        #size 6
                        #str 14
                        #enc 3
                        #att 9
                        #def 9
                        #prec 10
                        #mor 50
                        #gcost 0
                        #undisciplined
                        #noitem
                        #noleader
                        #poisonres 25
                        #neednoteat
                        #fireres 15 
                        #end
                    
                        #newmonster 7485
                        #name "Recombinant" 
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/RecombinantLow2.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/RecombinantLow2.tga"
                        #hp 12
                    #descr "Given free reign, and an endless supply of test subjects, the twisted scientific minds of Diamedulla have invented many horrifying devices over the years. Perhaps the most sickening of such horrors are the Recombinant, those miserable souls impaled on strange devices called 'Dragon's Teeth'. Though impaled, these poor souls somehow remain alive and fully conscious for an extreemly long time, while slowly being twisted and turned into abominations over a course of agonizing weeks, or even months, by the same impaling device through vile black alchemy. 
                    Dragon's Teeth are typically placed on Diamedullan borders, or in front of their secretive fortification to ward off intruders. When approached, the impaling devices will slowly release the Recombinant while also injecting them with one last compound, the final, cruel touch of their deranged creators. Once released, the Recombinant will charge at their enemies and, once close enough - or near death, give off a powerful alchemical explosion, killing those nearby or immobilizing them with foul fumes for other Recombinant to more easily rip them apart. Fully transformed Recombinant do not retain any vestiges of their former personalities and are just mindless, empty husks."
                        #growhp 12
                        #regeneration 5
                        #immobile
                        #ap 2
                        #mapmove 0
                        #mr 10
                        #size 6
                        #str 14
                        #enc 3
                        #att 9
                        #def 9
                        #prec 10
                        #mor 50
                        #gcost 0
                        #undisciplined
                        #noitem
                        #noleader
                        #poisonres 25
                        #neednoteat
                        #fireres 15 
                        #end
                    
                        #newmonster 7484
                        #name "Recombinant" 
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/Recombinant1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/Recombinant1.tga"
                        #hp 12
                    #descr "Given free reign, and an endless supply of test subjects, the twisted scientific minds of Diamedulla have invented many horrifying devices over the years. Perhaps the most sickening of such horrors are the Recombinant, those miserable souls impaled on strange devices called 'Dragon's Teeth'. Though impaled, these poor souls somehow remain alive and fully conscious for an extreemly long time, while slowly being twisted and turned into abominations over a course of agonizing weeks, or even months, by the same impaling device through vile black alchemy. 
                    Dragon's Teeth are typically placed on Diamedullan borders, or in front of their secretive fortification to ward off intruders. When approached, the impaling devices will slowly release the Recombinant while also injecting them with one last compound, the final, cruel touch of their deranged creators. Once released, the Recombinant will charge at their enemies and, once close enough - or near death, give off a powerful alchemical explosion, killing those nearby or immobilizing them with foul fumes for other Recombinant to more easily rip them apart. Fully transformed Recombinant do not retain any vestiges of their former personalities and are just mindless, empty husks."
                        #deathfire 3
                        #deathparalyze 5
                        #ap 16
                        #mapmove 16
                        #mr 10
                        #size 2
                        #formationfighter -4
                        #str 14
                        #enc 3
                        #att 11
                        #def 9
                        #prec 10
                        #mor 50
                        #gcost 0
                        #undisciplined
                        #noitem
                        #noleader
                        #poisonres 25
                        #neednoteat
                        #weapon "Augmented Limb"
                        #weapon "Augmented Limb"
                        #coldres 5
                        #heat 3
                        #fireres 15 
                        #end
                    
                    -- 9HP start
                    
                        #newmonster 7494
                        #name "Recombinant" 
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/RecombinantMedium1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/RecombinantMedium1.tga"
                        #hp 9
                    #descr "Given free reign, and an endless supply of test subjects, the twisted scientific minds of Diamedulla have invented many horrifying devices over the years. Perhaps the most sickening of such horrors are the Recombinant, those miserable souls impaled on strange devices called 'Dragon's Teeth'. Though impaled, these poor souls somehow remain alive and fully conscious for an extreemly long time, while slowly being twisted and turned into abominations over a course of agonizing weeks, or even months, by the same impaling device through vile black alchemy. 
                    Dragon's Teeth are typically placed on Diamedullan borders, or in front of their secretive fortification to ward off intruders. When approached, the impaling devices will slowly release the Recombinant while also injecting them with one last compound, the final, cruel touch of their deranged creators. Once released, the Recombinant will charge at their enemies and, once close enough - or near death, give off a powerful alchemical explosion, killing those nearby or immobilizing them with foul fumes for other Recombinant to more easily rip them apart. Fully transformed Recombinant do not retain any vestiges of their former personalities and are just mindless, empty husks."
                        #growhp 9
                        #regeneration 5
                        #immobile
                        #ap 2
                        #mapmove 0
                        #mr 10
                        #size 6
                        #str 14
                        #enc 3
                        #att 9
                        #def 9
                        #prec 10
                        #mor 50
                        #gcost 0
                        #undisciplined
                        #noitem
                        #noleader
                        #poisonres 25
                        #neednoteat
                        #montag 13777
                        #fireres 15 
                        #end
                    
                        #newmonster 7493
                        #name "Recombinant" 
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/RecombinantMedium2.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/RecombinantMedium2.tga"
                        #hp 12
                    #descr "Given free reign, and an endless supply of test subjects, the twisted scientific minds of Diamedulla have invented many horrifying devices over the years. Perhaps the most sickening of such horrors are the Recombinant, those miserable souls impaled on strange devices called 'Dragon's Teeth'. Though impaled, these poor souls somehow remain alive and fully conscious for an extreemly long time, while slowly being twisted and turned into abominations over a course of agonizing weeks, or even months, by the same impaling device through vile black alchemy. 
                    Dragon's Teeth are typically placed on Diamedullan borders, or in front of their secretive fortification to ward off intruders. When approached, the impaling devices will slowly release the Recombinant while also injecting them with one last compound, the final, cruel touch of their deranged creators. Once released, the Recombinant will charge at their enemies and, once close enough - or near death, give off a powerful alchemical explosion, killing those nearby or immobilizing them with foul fumes for other Recombinant to more easily rip them apart. Fully transformed Recombinant do not retain any vestiges of their former personalities and are just mindless, empty husks."
                        #growhp 10
                        #regeneration 5
                        #immobile
                        #ap 2
                        #mapmove 0
                        #mr 10
                        #size 6
                        #str 14
                        #enc 3
                        #att 9
                        #def 9
                        #prec 10
                        #mor 50
                        #gcost 0
                        #undisciplined
                        #noitem
                        #noleader
                        #poisonres 25
                        #neednoteat
                        #fireres 15 
                        #end
                    
                        #newmonster 7492
                        #name "Recombinant" 
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/RecombinantLow1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/RecombinantLow1.tga"
                        #hp 12
                    #descr "Given free reign, and an endless supply of test subjects, the twisted scientific minds of Diamedulla have invented many horrifying devices over the years. Perhaps the most sickening of such horrors are the Recombinant, those miserable souls impaled on strange devices called 'Dragon's Teeth'. Though impaled, these poor souls somehow remain alive and fully conscious for an extreemly long time, while slowly being twisted and turned into abominations over a course of agonizing weeks, or even months, by the same impaling device through vile black alchemy. 
                    Dragon's Teeth are typically placed on Diamedullan borders, or in front of their secretive fortification to ward off intruders. When approached, the impaling devices will slowly release the Recombinant while also injecting them with one last compound, the final, cruel touch of their deranged creators. Once released, the Recombinant will charge at their enemies and, once close enough - or near death, give off a powerful alchemical explosion, killing those nearby or immobilizing them with foul fumes for other Recombinant to more easily rip them apart. Fully transformed Recombinant do not retain any vestiges of their former personalities and are just mindless, empty husks."
                        #growhp 11
                        #regeneration 5
                        #immobile
                        #ap 2
                        #mapmove 0
                        #mr 10
                        #size 6
                        #str 14
                        #enc 3
                        #att 9
                        #def 9
                        #prec 10
                        #mor 50
                        #gcost 0
                        #undisciplined
                        #noitem
                        #noleader
                        #poisonres 25
                        #neednoteat
                        #fireres 15 
                        #end
                    
                        #newmonster 7491
                        #name "Recombinant" 
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/RecombinantLow2.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/RecombinantLow2.tga"
                        #hp 12
                    #descr "Given free reign, and an endless supply of test subjects, the twisted scientific minds of Diamedulla have invented many horrifying devices over the years. Perhaps the most sickening of such horrors are the Recombinant, those miserable souls impaled on strange devices called 'Dragon's Teeth'. Though impaled, these poor souls somehow remain alive and fully conscious for an extreemly long time, while slowly being twisted and turned into abominations over a course of agonizing weeks, or even months, by the same impaling device through vile black alchemy. 
                    Dragon's Teeth are typically placed on Diamedullan borders, or in front of their secretive fortification to ward off intruders. When approached, the impaling devices will slowly release the Recombinant while also injecting them with one last compound, the final, cruel touch of their deranged creators. Once released, the Recombinant will charge at their enemies and, once close enough - or near death, give off a powerful alchemical explosion, killing those nearby or immobilizing them with foul fumes for other Recombinant to more easily rip them apart. Fully transformed Recombinant do not retain any vestiges of their former personalities and are just mindless, empty husks."
                        #growhp 12
                        #regeneration 5
                        #immobile
                        #ap 2
                        #mapmove 0
                        #mr 10
                        #size 6
                        #str 14
                        #enc 3
                        #att 9
                        #def 9
                        #prec 10
                        #mor 50
                        #gcost 0
                        #undisciplined
                        #noitem
                        #noleader
                        #poisonres 25
                        #neednoteat
                        #fireres 15 
                        #end
                    
                        #newmonster 7490
                        #name "Recombinant" 
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/Recombinant1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/Recombinant1.tga"
                        #hp 12
                    #descr "Given free reign, and an endless supply of test subjects, the twisted scientific minds of Diamedulla have invented many horrifying devices over the years. Perhaps the most sickening of such horrors are the Recombinant, those miserable souls impaled on strange devices called 'Dragon's Teeth'. Though impaled, these poor souls somehow remain alive and fully conscious for an extreemly long time, while slowly being twisted and turned into abominations over a course of agonizing weeks, or even months, by the same impaling device through vile black alchemy. 
                    Dragon's Teeth are typically placed on Diamedullan borders, or in front of their secretive fortification to ward off intruders. When approached, the impaling devices will slowly release the Recombinant while also injecting them with one last compound, the final, cruel touch of their deranged creators. Once released, the Recombinant will charge at their enemies and, once close enough - or near death, give off a powerful alchemical explosion, killing those nearby or immobilizing them with foul fumes for other Recombinant to more easily rip them apart. Fully transformed Recombinant do not retain any vestiges of their former personalities and are just mindless, empty husks."
                        #deathfire 3
                        #deathparalyze 5
                        #ap 16
                        #mapmove 16
                        #mr 10
                        #size 2
                        #formationfighter -4
                        #str 14
                        #enc 3
                        #att 11
                        #def 9
                        #prec 10
                        #mor 50
                        #gcost 0
                        #undisciplined
                        #noitem
                        #noleader
                        #poisonres 25
                        #neednoteat
                        #weapon "Augmented Limb"
                        #weapon "Augmented Limb"
                        #coldres 5
                        #heat 3
                        #fireres 15 
                        #end
                    
                    -- 10HP start
                    
                        #newmonster 7498
                        #name "Recombinant" 
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/RecombinantMedium2.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/RecombinantMedium2.tga"
                        #hp 10
                    #descr "Given free reign, and an endless supply of test subjects, the twisted scientific minds of Diamedulla have invented many horrifying devices over the years. Perhaps the most sickening of such horrors are the Recombinant, those miserable souls impaled on strange devices called 'Dragon's Teeth'. Though impaled, these poor souls somehow remain alive and fully conscious for an extreemly long time, while slowly being twisted and turned into abominations over a course of agonizing weeks, or even months, by the same impaling device through vile black alchemy. 
                    Dragon's Teeth are typically placed on Diamedullan borders, or in front of their secretive fortification to ward off intruders. When approached, the impaling devices will slowly release the Recombinant while also injecting them with one last compound, the final, cruel touch of their deranged creators. Once released, the Recombinant will charge at their enemies and, once close enough - or near death, give off a powerful alchemical explosion, killing those nearby or immobilizing them with foul fumes for other Recombinant to more easily rip them apart. Fully transformed Recombinant do not retain any vestiges of their former personalities and are just mindless, empty husks."
                        #growhp 10
                        #regeneration 5
                        #immobile
                        #ap 2
                        #mapmove 0
                        #mr 10
                        #size 6
                        #str 14
                        #enc 3
                        #att 9
                        #def 9
                        #prec 10
                        #mor 50
                        #gcost 0
                        #undisciplined
                        #noitem
                        #noleader
                        #poisonres 25
                        #neednoteat
                        #montag 13777
                        #fireres 15 
                        #end
                    
                        #newmonster 7497
                        #name "Recombinant" 
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/RecombinantLow1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/RecombinantLow1.tga"
                        #hp 12
                    #descr "Given free reign, and an endless supply of test subjects, the twisted scientific minds of Diamedulla have invented many horrifying devices over the years. Perhaps the most sickening of such horrors are the Recombinant, those miserable souls impaled on strange devices called 'Dragon's Teeth'. Though impaled, these poor souls somehow remain alive and fully conscious for an extreemly long time, while slowly being twisted and turned into abominations over a course of agonizing weeks, or even months, by the same impaling device through vile black alchemy. 
                    Dragon's Teeth are typically placed on Diamedullan borders, or in front of their secretive fortification to ward off intruders. When approached, the impaling devices will slowly release the Recombinant while also injecting them with one last compound, the final, cruel touch of their deranged creators. Once released, the Recombinant will charge at their enemies and, once close enough - or near death, give off a powerful alchemical explosion, killing those nearby or immobilizing them with foul fumes for other Recombinant to more easily rip them apart. Fully transformed Recombinant do not retain any vestiges of their former personalities and are just mindless, empty husks."
                        #growhp 11
                        #regeneration 5
                        #immobile
                        #ap 2
                        #mapmove 0
                        #mr 10
                        #size 6
                        #str 14
                        #enc 3
                        #att 9
                        #def 9
                        #prec 10
                        #mor 50
                        #gcost 0
                        #undisciplined
                        #noitem
                        #noleader
                        #poisonres 25
                        #neednoteat
                        #fireres 15 
                        #end
                    
                        #newmonster 7496
                        #name "Recombinant" 
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/RecombinantLow2.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/RecombinantLow2.tga"
                        #hp 12
                    #descr "Given free reign, and an endless supply of test subjects, the twisted scientific minds of Diamedulla have invented many horrifying devices over the years. Perhaps the most sickening of such horrors are the Recombinant, those miserable souls impaled on strange devices called 'Dragon's Teeth'. Though impaled, these poor souls somehow remain alive and fully conscious for an extreemly long time, while slowly being twisted and turned into abominations over a course of agonizing weeks, or even months, by the same impaling device through vile black alchemy. 
                    Dragon's Teeth are typically placed on Diamedullan borders, or in front of their secretive fortification to ward off intruders. When approached, the impaling devices will slowly release the Recombinant while also injecting them with one last compound, the final, cruel touch of their deranged creators. Once released, the Recombinant will charge at their enemies and, once close enough - or near death, give off a powerful alchemical explosion, killing those nearby or immobilizing them with foul fumes for other Recombinant to more easily rip them apart. Fully transformed Recombinant do not retain any vestiges of their former personalities and are just mindless, empty husks."
                        #growhp 12
                        #regeneration 5
                        #immobile
                        #ap 2
                        #mapmove 0
                        #mr 10
                        #size 6
                        #str 14
                        #enc 3
                        #att 9
                        #def 9
                        #prec 10
                        #mor 50
                        #gcost 0
                        #undisciplined
                        #noitem
                        #noleader
                        #poisonres 25
                        #neednoteat
                        #fireres 15 
                        #end
                    
                        #newmonster 7495
                        #name "Recombinant" 
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/Recombinant1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/Recombinant1.tga"
                        #hp 12
                    #descr "Given free reign, and an endless supply of test subjects, the twisted scientific minds of Diamedulla have invented many horrifying devices over the years. Perhaps the most sickening of such horrors are the Recombinant, those miserable souls impaled on strange devices called 'Dragon's Teeth'. Though impaled, these poor souls somehow remain alive and fully conscious for an extreemly long time, while slowly being twisted and turned into abominations over a course of agonizing weeks, or even months, by the same impaling device through vile black alchemy. 
                    Dragon's Teeth are typically placed on Diamedullan borders, or in front of their secretive fortification to ward off intruders. When approached, the impaling devices will slowly release the Recombinant while also injecting them with one last compound, the final, cruel touch of their deranged creators. Once released, the Recombinant will charge at their enemies and, once close enough - or near death, give off a powerful alchemical explosion, killing those nearby or immobilizing them with foul fumes for other Recombinant to more easily rip them apart. Fully transformed Recombinant do not retain any vestiges of their former personalities and are just mindless, empty husks."
                        #deathfire 3
                        #deathparalyze 5
                        #ap 16
                        #mapmove 16
                        #mr 10
                        #size 2
                        #formationfighter -4
                        #str 14
                        #enc 3
                        #att 11
                        #def 9
                        #prec 10
                        #mor 50
                        #gcost 0
                        #undisciplined
                        #noitem
                        #noleader
                        #poisonres 25
                        #neednoteat
                        #weapon "Augmented Limb"
                        #weapon "Augmented Limb"
                        #coldres 5
                        #heat 3
                        #fireres 15 
                        #end
                    
                    -- 11HP start
                    
                        #newmonster 7501
                        #name "Recombinant" 
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/RecombinantLow1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/RecombinantLow1.tga"
                        #hp 11
                    #descr "Given free reign, and an endless supply of test subjects, the twisted scientific minds of Diamedulla have invented many horrifying devices over the years. Perhaps the most sickening of such horrors are the Recombinant, those miserable souls impaled on strange devices called 'Dragon's Teeth'. Though impaled, these poor souls somehow remain alive and fully conscious for an extreemly long time, while slowly being twisted and turned into abominations over a course of agonizing weeks, or even months, by the same impaling device through vile black alchemy. 
                    Dragon's Teeth are typically placed on Diamedullan borders, or in front of their secretive fortification to ward off intruders. When approached, the impaling devices will slowly release the Recombinant while also injecting them with one last compound, the final, cruel touch of their deranged creators. Once released, the Recombinant will charge at their enemies and, once close enough - or near death, give off a powerful alchemical explosion, killing those nearby or immobilizing them with foul fumes for other Recombinant to more easily rip them apart. Fully transformed Recombinant do not retain any vestiges of their former personalities and are just mindless, empty husks."
                        #growhp 11
                        #regeneration 5
                        #immobile
                        #ap 2
                        #mapmove 0
                        #mr 10
                        #size 6
                        #str 14
                        #enc 3
                        #att 9
                        #def 9
                        #prec 10
                        #mor 50
                        #gcost 0
                        #undisciplined
                        #noitem
                        #noleader
                        #poisonres 25
                        #neednoteat
                        #montag 13777
                        #fireres 15 
                        #end
                    
                        #newmonster 7500
                        #name "Recombinant" 
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/RecombinantLow2.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/RecombinantLow2.tga"
                        #hp 12
                    #descr "Given free reign, and an endless supply of test subjects, the twisted scientific minds of Diamedulla have invented many horrifying devices over the years. Perhaps the most sickening of such horrors are the Recombinant, those miserable souls impaled on strange devices called 'Dragon's Teeth'. Though impaled, these poor souls somehow remain alive and fully conscious for an extreemly long time, while slowly being twisted and turned into abominations over a course of agonizing weeks, or even months, by the same impaling device through vile black alchemy. 
                    Dragon's Teeth are typically placed on Diamedullan borders, or in front of their secretive fortification to ward off intruders. When approached, the impaling devices will slowly release the Recombinant while also injecting them with one last compound, the final, cruel touch of their deranged creators. Once released, the Recombinant will charge at their enemies and, once close enough - or near death, give off a powerful alchemical explosion, killing those nearby or immobilizing them with foul fumes for other Recombinant to more easily rip them apart. Fully transformed Recombinant do not retain any vestiges of their former personalities and are just mindless, empty husks."
                        #growhp 12
                        #regeneration 5
                        #immobile
                        #ap 2
                        #mapmove 0
                        #mr 10
                        #size 6
                        #str 14
                        #enc 3
                        #att 9
                        #def 9
                        #prec 10
                        #mor 50
                        #gcost 0
                        #undisciplined
                        #noitem
                        #noleader
                        #poisonres 25
                        #neednoteat
                        #fireres 15 
                        #end
                    
                        #newmonster 7499
                        #name "Recombinant" 
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/Recombinant1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/Recombinant1.tga"
                        #hp 12
                    #descr "Given free reign, and an endless supply of test subjects, the twisted scientific minds of Diamedulla have invented many horrifying devices over the years. Perhaps the most sickening of such horrors are the Recombinant, those miserable souls impaled on strange devices called 'Dragon's Teeth'. Though impaled, these poor souls somehow remain alive and fully conscious for an extreemly long time, while slowly being twisted and turned into abominations over a course of agonizing weeks, or even months, by the same impaling device through vile black alchemy. 
                    Dragon's Teeth are typically placed on Diamedullan borders, or in front of their secretive fortification to ward off intruders. When approached, the impaling devices will slowly release the Recombinant while also injecting them with one last compound, the final, cruel touch of their deranged creators. Once released, the Recombinant will charge at their enemies and, once close enough - or near death, give off a powerful alchemical explosion, killing those nearby or immobilizing them with foul fumes for other Recombinant to more easily rip them apart. Fully transformed Recombinant do not retain any vestiges of their former personalities and are just mindless, empty husks."
                        #deathfire 3
                        #deathparalyze 5
                        #ap 16
                        #mapmove 16
                        #mr 10
                        #size 2
                        #formationfighter -4
                        #str 14
                        #enc 3
                        #att 11
                        #def 9
                        #prec 10
                        #mor 50
                        #gcost 0
                        #undisciplined
                        #noitem
                        #noleader
                        #poisonres 25
                        #neednoteat
                        #weapon "Augmented Limb"
                        #weapon "Augmented Limb"
                        #coldres 5
                        #heat 3
                        #fireres 15 
                        #end
                    
                    -- 12HP start
                    
                        #newmonster 7503
                        #name "Recombinant" 
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/RecombinantLow2.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/RecombinantLow2.tga"
                        #hp 12
                    #descr "Given free reign, and an endless supply of test subjects, the twisted scientific minds of Diamedulla have invented many horrifying devices over the years. Perhaps the most sickening of such horrors are the Recombinant, those miserable souls impaled on strange devices called 'Dragon's Teeth'. Though impaled, these poor souls somehow remain alive and fully conscious for an extreemly long time, while slowly being twisted and turned into abominations over a course of agonizing weeks, or even months, by the same impaling device through vile black alchemy. 
                    Dragon's Teeth are typically placed on Diamedullan borders, or in front of their secretive fortification to ward off intruders. When approached, the impaling devices will slowly release the Recombinant while also injecting them with one last compound, the final, cruel touch of their deranged creators. Once released, the Recombinant will charge at their enemies and, once close enough - or near death, give off a powerful alchemical explosion, killing those nearby or immobilizing them with foul fumes for other Recombinant to more easily rip them apart. Fully transformed Recombinant do not retain any vestiges of their former personalities and are just mindless, empty husks."
                        #growhp 12
                        #regeneration 5
                        #immobile
                        #ap 2
                        #mapmove 0
                        #mr 10
                        #size 6
                        #str 14
                        #enc 3
                        #att 9
                        #def 9
                        #prec 10
                        #mor 50
                        #gcost 0
                        #undisciplined
                        #noitem
                        #noleader
                        #poisonres 25
                        #neednoteat
                        #montag 13777
                        #fireres 15 
                        #end
                    
                        #newmonster 7502
                        #name "Recombinant" 
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/Recombinant1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/Recombinant1.tga"
                        #hp 12
                    #descr "Given free reign, and an endless supply of test subjects, the twisted scientific minds of Diamedulla have invented many horrifying devices over the years. Perhaps the most sickening of such horrors are the Recombinant, those miserable souls impaled on strange devices called 'Dragon's Teeth'. Though impaled, these poor souls somehow remain alive and fully conscious for an extreemly long time, while slowly being twisted and turned into abominations over a course of agonizing weeks, or even months, by the same impaling device through vile black alchemy. 
                    Dragon's Teeth are typically placed on Diamedullan borders, or in front of their secretive fortification to ward off intruders. When approached, the impaling devices will slowly release the Recombinant while also injecting them with one last compound, the final, cruel touch of their deranged creators. Once released, the Recombinant will charge at their enemies and, once close enough - or near death, give off a powerful alchemical explosion, killing those nearby or immobilizing them with foul fumes for other Recombinant to more easily rip them apart. Fully transformed Recombinant do not retain any vestiges of their former personalities and are just mindless, empty husks."
                        #deathfire 3
                        #deathparalyze 5
                        #ap 16
                        #mapmove 16
                        #mr 10
                        #size 2
                        #formationfighter -4
                        #str 14
                        #enc 3
                        #att 11
                        #def 9
                        #prec 10
                        #mor 50
                        #gcost 0
                        #undisciplined
                        #noitem
                        #noleader
                        #poisonres 25
                        #neednoteat
                        #weapon "Augmented Limb"
                        #weapon "Augmented Limb"
                        #montag 13777
                        #coldres 5
                        #heat 3
                        #fireres 15 
                        #end
                    
                    
                    -- ------------------------------------------------------------------------------------
            -- Summons
                -- Necrobrutes                     
                    -- FORM 1
                        #newmonster 7515
                        #name "Necrobrute"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/Necrobrute1.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/Necrobrute1.tga"
                        #descr "While a Necromorph is created from a mutated and reanimated corpse of a singe individual, the Necrobrute is created from several corpses merged together into a single, sickening form with a ungodly amount of limbs, tentacles, and various, fleshy protrusions. Though the Necromorphs still look vaguely human, a Necrobrute is a completely alien looking creature, and utterly disgusting. Given that the Necrobrute is a whole order above a Necromorph, its discovery, or rather, creation, has given rise to new debates as to the nature and purpose of the Void Marker. It is entirely possible the function of the Marker is not to create either Nercomorphs or Necrobrutes, but something even greater. A creation composed from an endless multitude of hosts."
                        #ap 14
                        #mapmove 14
                        #hp 28
                        #mr 12
                        #size 3
                        #str 17
                        #enc 1
                        #att 11
                        #def 9
                        #prec 8
                        #mor 18
                        #berserk 2
                        #gcost 0
                        #weapon "Mutated Limb"
                        #weapon 63
                        #poisonres 8
                        #diseaseres 100
                        #insane 50
                        #heal
                        #startage 32
                        #maxage 500
                        #poorleader
                        #poorundeadleader
                        #noitem
                        #regeneration 25
                        #raiseonkill 100
                        #raiseshape -13423
                        #montag 13478
                        #undisciplined
                        #neednoteat
                        #undead
                        #end
                    -- FORM 2
                        #newmonster 7516
                        #name "Necrobrute"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/Necrobrute2.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/Necrobrute2.tga"
                        #descr "While a Necromorph is created from a mutated and reanimated corpse of a singe individual, the Necrobrute is created from several corpses merged together into a single, sickening form with a ungodly amount of limbs, tentacles, and various, fleshy protrusions. Though the Necromorphs still look vaguely human, a Necrobrute is a completely alien looking creature, and utterly disgusting. Given that the Necrobrute is a whole order above a Necromorph, its discovery, or rather, creation, has given rise to new debates as to the nature and purpose of the Void Marker. It is entirely possible the function of the Marker is not to create either Nercomorphs or Necrobrutes, but something even greater. A creation composed from an endless multitude of hosts."
                        #ap 14
                        #mapmove 14
                        #hp 28
                        #mr 12
                        #size 3
                        #str 17
                        #enc 1
                        #att 11
                        #def 9
                        #prec 8
                        #mor 18
                        #berserk 2
                        #gcost 0
                        #weapon "Mutated Limb"
                        #weapon 63
                        #poisonres 8
                        #diseaseres 100
                        #insane 50
                        #heal
                        #startage 32
                        #maxage 500
                        #poorleader
                        #poorundeadleader
                        #noitem
                        #regeneration 25
                        #raiseonkill 100
                        #raiseshape -13423
                        #montag 13478
                        #undisciplined
                        #neednoteat
                        #undead
                        #end
                    -- FORM 3
                        #newmonster 7517
                        #name "Necrobrute"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/Necrobrute3.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/Necrobrute3.tga"
                        #descr "While a Necromorph is created from a mutated and reanimated corpse of a singe individual, the Necrobrute is created from several corpses merged together into a single, sickening form with a ungodly amount of limbs, tentacles, and various, fleshy protrusions. Though the Necromorphs still look vaguely human, a Necrobrute is a completely alien looking creature, and utterly disgusting. Given that the Necrobrute is a whole order above a Necromorph, its discovery, or rather, creation, has given rise to new debates as to the nature and purpose of the Void Marker. It is entirely possible the function of the Marker is not to create either Nercomorphs or Necrobrutes, but something even greater. A creation composed from an endless multitude of hosts."
                        #ap 14
                        #mapmove 14
                        #hp 28
                        #mr 12
                        #size 3
                        #str 17
                        #enc 1
                        #att 11
                        #def 9
                        #prec 8
                        #mor 18
                        #berserk 2
                        #gcost 0
                        #weapon "Mutated Limb"
                        #weapon 63
                        #poisonres 8
                        #diseaseres 100
                        #insane 50
                        #heal
                        #startage 32
                        #maxage 500
                        #poorleader
                        #poorundeadleader
                        #noitem
                        #regeneration 25
                        #raiseonkill 100
                        #raiseshape -13423
                        #montag 13478
                        #undisciplined
                        #neednoteat
                        #undead
                        #end
                    -- FORM 4       -- Has the alien brain thingy. Mind Blast? Enslave?
                        #newmonster 7518
                        #name "Necrobrute"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/Necrobrute4.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/Necrobrute4.tga"
                        #descr "While a Necromorph is created from a mutated and reanimated corpse of a singe individual, the Necrobrute is created from several corpses merged together into a single, sickening form with a ungodly amount of limbs, tentacles, and various, fleshy protrusions. Though the Necromorphs still look vaguely human, a Necrobrute is a completely alien looking creature, and utterly disgusting. Given that the Necrobrute is a whole order above a Necromorph, its discovery, or rather, creation, has given rise to new debates as to the nature and purpose of the Void Marker. It is entirely possible the function of the Marker is not to create either Nercomorphs or Necrobrutes, but something even greater. A creation composed from an endless multitude of hosts."
                        #ap 14
                        #mapmove 14
                        #hp 28
                        #mr 12
                        #size 3
                        #str 17
                        #enc 1
                        #att 11
                        #def 9
                        #prec 8
                        #mor 18
                        #berserk 2
                        #gcost 0
                        #weapon "Mutated Limb"
                        #weapon "Chomp"
                        #weapon "Mind Blast"
                        #poisonres 8
                        #diseaseres 100
                        #insane 50
                        #heal
                        #startage 32
                        #maxage 500
                        #poorleader
                        #poorundeadleader
                        #noitem
                        #regeneration 25
                        #raiseonkill 100
                        #raiseshape -13423
                        #montag 13478
                        #undisciplined
                        #neednoteat
                        #undead
                        #end
                    -- FORM 5
                            #newmonster 7519
                            #name "Necrobrute"
                            #spr1 "./bozmod/Confluence/LA_Diamedulla/Necrobrute5.tga"
                            #spr2 "./bozmod/Confluence/LA_Diamedulla/Necrobrute5.tga"
                            #descr "While a Necromorph is created from a mutated and reanimated corpse of a singe individual, the Necrobrute is created from several corpses merged together into a single, sickening form with a ungodly amount of limbs, tentacles, and various, fleshy protrusions. Though the Necromorphs still look vaguely human, a Necrobrute is a completely alien looking creature, and utterly disgusting. Given that the Necrobrute is a whole order above a Necromorph, its discovery, or rather, creation, has given rise to new debates as to the nature and purpose of the Void Marker. It is entirely possible the function of the Marker is not to create either Nercomorphs or Necrobrutes, but something even greater. A creation composed from an endless multitude of hosts."
                            #ap 14
                            #mapmove 14
                            #hp 28
                            #mr 12
                            #size 3
                            #str 17
                            #enc 1
                            #att 11
                            #def 9
                            #prec 8
                            #mor 18
                            #berserk 2
                            #gcost 0
                            #weapon "Mutated Limb"
                            #weapon "Chomp"
                            #poisonres 8
                            #diseaseres 100
                            #insane 50
                            #heal
                            #startage 32
                            #maxage 500
                            #poorleader
                            #poorundeadleader
                            #noitem
                            #regeneration 25
                            #raiseonkill 100
                            #raiseshape -13423
                            #montag 13478
                            #undisciplined
                            #neednoteat
                            #undead
                            #end
                    -- FORM 6
                        #newmonster 7520
                        #name "Necrobrute"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/Necrobrute6.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/Necrobrute6.tga"
                        #descr "While a Necromorph is created from a mutated and reanimated corpse of a singe individual, the Necrobrute is created from several corpses merged together into a single, sickening form with a ungodly amount of limbs, tentacles, and various, fleshy protrusions. Though the Necromorphs still look vaguely human, a Necrobrute is a completely alien looking creature, and utterly disgusting. Given that the Necrobrute is a whole order above a Necromorph, its discovery, or rather, creation, has given rise to new debates as to the nature and purpose of the Void Marker. It is entirely possible the function of the Marker is not to create either Nercomorphs or Necrobrutes, but something even greater. A creation composed from an endless multitude of hosts."
                        #ap 14
                        #mapmove 14
                        #hp 28
                        #mr 12
                        #size 3
                        #str 17
                        #enc 1
                        #att 11
                        #def 9
                        #prec 8
                        #mor 18
                        #berserk 2
                        #gcost 0
                        #weapon "Mutated Limb"
                        #weapon "Chomp"
                        #poisonres 8
                        #diseaseres 100
                        #insane 50
                        #heal
                        #startage 32
                        #maxage 500
                        #poorleader
                        #poorundeadleader
                        #noitem
                        #regeneration 25
                        #raiseonkill 100
                        #raiseshape -13423
                        #montag 13478
                        #undisciplined
                        #neednoteat
                        #undead
                        #end
                    -- FORM 7
                    #newmonster 7521
                    #name "Necrobrute"
                    #spr1 "./bozmod/Confluence/LA_Diamedulla/Necrobrute7.tga"
                    #spr2 "./bozmod/Confluence/LA_Diamedulla/Necrobrute7.tga"
                    #descr "While a Necromorph is created from a mutated and reanimated corpse of a singe individual, the Necrobrute is created from several corpses merged together into a single, sickening form with a ungodly amount of limbs, tentacles, and various, fleshy protrusions. Though the Necromorphs still look vaguely human, a Necrobrute is a completely alien looking creature, and utterly disgusting. Given that the Necrobrute is a whole order above a Necromorph, its discovery, or rather, creation, has given rise to new debates as to the nature and purpose of the Void Marker. It is entirely possible the function of the Marker is not to create either Nercomorphs or Necrobrutes, but something even greater. A creation composed from an endless multitude of hosts."
                    #ap 14
                    #mapmove 14
                    #hp 28
                    #mr 12
                    #size 3
                    #str 17
                    #enc 1
                    #att 11
                    #def 9
                    #prec 8
                    #mor 18
                    #berserk 2
                    #gcost 0
                    #weapon "Mutated Limb"
                    #weapon "Chomp"
                    #weapon 63
                    #poisonres 8
                    #diseaseres 100
                    #insane 50
                    #heal
                    #startage 32
                    #maxage 500
                    #poorleader
                    #poorundeadleader
                    #noitem
                    #regeneration 25
                    #raiseonkill 100
                    #raiseshape -13423
                    #montag 13478
                    #undisciplined
                    #neednoteat
                    #undead
                    #end
                -- Void Beacon 
                
                
                    #newmonster 7510  
                    #copystats 158
                    #clearmagic
                    #name "Void Beacon"
                    #spr1 "./bozmod/Confluence/LA_Diamedulla/VoidBeacon.tga"
                    #spr2 "./bozmod/Confluence/LA_Diamedulla/VoidBeacon.tga"
                    #descr "Not used currently."	
                    #ap 2
                    #mapmove 0
                    #prot 18
                    #hp 75
                    #mr 20
                    #size 6
                    #str 15
                    #enc 0
                    #att 5
                    #def 5
                    #prec 5
                    #mor 50
                    #poisonres 25
                    #coldres 15
                    #neednoteat
                    #inanimate
                    #amphibian
                    #darkvision 100
                    #unteleportable
                    #startage -1
                    #maxage 5000
                    #gcost 0
                    #voidsanity 100
                    #blind
                    #itemslots 28672
                    #stonebeing
                    #weapon 0
                    #noleader
                    #nobadevents 0
                    #bonusspells 0 
                    #homerealm 0
                    #end
                -- Trinity 
                    #newmonster 7445
                    #name "Psychic Construct"
                    #spr1 "./bozmod/Confluence/LA_Diamedulla/BubbleTrinity1.tga"
                    #spr2 "./bozmod/Confluence/LA_Diamedulla/BubbleTrinity1.tga"
                    #descr "The Psychic Construct is a creature created in the Clockwork Horror through extensive physical and mental re-engineering of typically unwilling subjects. The end result of such grisly procedures are abominations devoid of all human characteristics. 
                    The Psychic Construct is capable of capable of projecting a near impenetrable physic bubble around it. It is also capable of blasting those around it with an overwhelming physic attack. "
                    #prot 18
                    #ap 16
                    #mapmove 16
                    #hp 36
                    #mr 16
                    #size 4
                    #str 15
                    #enc 0
                    #att 12
                    #def 12
                    #prec 10
                    #mor 15
                    #holy
                    #gcost 0
                    #diseaseres 100
                    #fear 5
                    #poisonres 15
                    #noitem
                    #startage 35
                    #maxage 150
                    #inanimate
                    #heal
                    #neednoteat
                    #unsurr 1
                    #startitem 575
                    #montag 13109
                    #invulnerable 25
                    #fireres 25
                    #coldres 25
                    #shockres 25
                    #weapon "Psychic Blast"
                    #weapon "Life Drain"
                    #end
                    
                    #newmonster 7446
                    #name "Expedient Collector"
                    #spr1 "./bozmod/Confluence/LA_Diamedulla/CollectorTrinity1.tga"
                    #spr2 "./bozmod/Confluence/LA_Diamedulla/CollectorTrinity1.tga"
                    #descr "The Expedient Collector is a creature created in the Clockwork Horror through extensive physical and mental re-engineering of typically unwilling subjects. The end result of such grisly procedures are abominations devoid of all human characteristics. 
                    The Expedient Collector is a much more powerful and cunning version of an ordinary Collector."
                    #prot 18
                    #ap 16
                    #mapmove 16
                    #hp 36
                    #mr 16
                    #size 4
                    #str 17
                    #enc 0
                    #att 14
                    #def 12
                    #prec 10
                    #mor 15
                    #holy
                    #gcost 0
                    #diseaseres 100
                    #fear 5
                    #poisonres 15
                    #noitem
                    #startage 35
                    #maxage 150
                    #inanimate
                    #heal
                    #neednoteat
                    #unsurr 1
                    #startitem 575
                    #montag 13109
                    #stealthy 15
                    #assassin
                    #patience 2
                    #weapon "Augmented Limb"
                    #weapon "Mechanical Arm"
                    #end
                    
                    #newmonster 7447
                    #name "Baneful Lantern"
                    #spr1 "./bozmod/Confluence/LA_Diamedulla/BanefireTrinity1.tga"
                    #spr2 "./bozmod/Confluence/LA_Diamedulla/BanefireTrinity1.tga"
                    #descr "The Baneful Lantern is a creature created in the Clockwork Horror through extensive physical and mental re-engineering of typically unwilling subjects. The end result of such grisly procedures are abominations devoid of all human characteristics. 
                    The Baneful Lantern is surrounded by and exudes a poisonous banefire aura."
                    #prot 18
                    #ap 16
                    #mapmove 16
                    #hp 36
                    #mr 16
                    #size 4
                    #str 16
                    #enc 0
                    #att 12
                    #def 9
                    #prec 10
                    #mor 15
                    #holy
                    #gcost 0
                    #diseaseres 100
                    #fear 5
                    #poisonres 15
                    #noitem
                    #startage 35
                    #maxage 150
                    #inanimate
                    #heal
                    #neednoteat
                    #unsurr 1
                    #startitem 575
                    #montag 13109
                    #banefireshield 8
                    #weapon 348
                    #weapon 200
                    #weapon 348
                    #weapon 200
                    #weapon 348
                    #weapon 200
                    #end
                    
                    #newmonster 7448
                    #name "Diamedullan Trinity"
                    #spr1 "./bozmod/Confluence/LA_Diamedulla/Pretender1.tga"
                    #spr2 "./bozmod/Confluence/LA_Diamedulla/Pretender1.tga"
                    #descr "Description missing. WIP."
                    #prot 18
                    #ap 16
                    #mapmove 16
                    #hp 108
                    #mr 18
                    #size 6
                    #str 26
                    #enc 0
                    #att 16
                    #def 14
                    #prec 14
                    #mor 18
                    #holy
                    #gcost 0
                    #diseaseres 100
                    #fear 15
                    #poisonres 15
                    #itemslots 61440
                    #startage 35
                    #maxage 150
                    #inanimate
                    #heal
                    #neednoteat
                    #unsurr 3
                    #startitem 576
                    #weapon "Psychic Blast"
                    #weapon "Mechanical Drill"
                    #weapon "Augmented Limb"
                    #weapon "Mechanical Arm"
                    #banefireshield 8
                    #expertleader
                    #eyes 6
                    #end
                -- Noxious Zeppelin
                    #newmonster 7475
                    #name "Noxious Zeppelin" 
                    #spr1 "./bozmod/Confluence/LA_Diamedulla/PlagueShip1.tga"
                    #spr2 "./bozmod/Confluence/LA_Diamedulla/PlagueShip1.tga"
                    #descr "The Noxious Zeppelin is a weaponized adaptation of the Antikytheran Zeppelin. Instead of serving as a simple troop transport, the Noxious Zeppelin has been repurposed to carry numerous containers of deadly alchemical compounds that are dispersed from the air ships over densely populated areas. Such alchemical compounds are extreemly deadly and will kill many of those they come in contact with. As the Noxious Zeppelin can stay high in the air for long periods of time, they are extremely difficult to spot and counteract. Typically, by the time a Noxious Zeppelin has been spotted, numerous barrels will have already been dispersed over a land."
                    #ap 18
                    #mapmove 32
                    #hp 125
                    #mr 18
                    #size 6
                    #str 25
                    #enc 0
                    #att 5
                    #def 5
                    #prec 8
                    #mor 50
                    #gcost 0
                    #float
                    #prot 15
                    #noheal
                    #neednoteat
                    #startage -1
                    #maxage 500
                    #inanimate
                    #noleader
                    #itemslots 4096
                    #poisonres 25
                    #shapechange 7476
                    #weapon "Crush"
                    #stealthy 40
                    #userestricteditem 2233
                    #end
                    
                        #newmonster 7476
                        #name "Noxious Zeppelin" 
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/PlagueShip2.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/PlagueShip2.tga"
                        #descr "The Noxious Zeppelin is a weaponized adaptation of the Antikytheran Zeppelin. Instead of serving as a simple troop transport, the Noxious Zeppelin has been repurposed to carry numerous containers of deadly alchemical compounds that are dispersed from the air ships over densely populated areas. Such alchemical compounds are extreemly deadly and will kill many of those they come in contact with. As the Noxious Zeppelin can stay high in the air for long periods of time, they are extremely difficult to spot and counteract. Typically, by the time a Noxious Zeppelin has been spotted, numerous barrels will have already been dispersed over a land.
                    * This Noxious Zeppelin is in the process of dropping its poisonous gas, and biochemical payload - assuming it has been added. A Noxious Zeppelin can only drop its payload if it has been constructed and transported from a Diamedullan lab onto the Zeppelin."
                        #ap 18
                        #mapmove 32
                        #hp 125
                        #mr 18
                        #size 6
                        #str 25
                        #enc 0
                        #att 5
                        #def 5
                        #prec 8
                        #mor 50
                        #gcost 0
                        #float
                        #prot 15
                        #noheal
                        #neednoteat
                        #startage -1
                        #maxage 500
                        #inanimate
                        #noleader
                        #itemslots 4096
                        #poisonres 25
                        #shapechange 7475
                        #poisoncloud 10
                        #deathdisease 10
                        #deathfire 10
                        #weapon "Crush"
                        #stealthy 40
                        #leper 5
                        #userestricteditem 2233
                        #end
                -- Deciphering Engine
                    #newmonster 7472
                    #copystats 158
                    #clearmagic
                    #name "Deciphering Engine" 
                    #spr1 "./bozmod/Confluence/LA_Diamedulla/Cypher1.tga"
                    #spr2 "./bozmod/Confluence/LA_Diamedulla/Cypher1.tga"
                    #descr "The Void Marker gradually erodes the minds of all those in its proximity, driving them quite mad and crippling their intellectual abilities over time. As such, it is little wonder any progress made on the artifact is typically painfully slow and its mysteries seemingly impossible to unclock. Due to this quite literally maddening obstacle, a device was created, one that could conduct the research unhindered and not fall pray to the Marker's insidious influence. 
                    The Deciphering Engines are towering machines created by the Black Alchemists to aid the research of the Void Marker. Due to their enormous size only one such engine can be operated from any single laboratory. Each Deciphering Engine constructed will add to the progress made on uncovering all of the Void Marker's secrets. The more laboratories there are with Deciphering Engines, the faster the research will be conducted. So far, the Mad Alchemists have ascertained that the Void Marker can directly perform at least three separate, distinct functions, though it yet remains a mystery as to what those functions may be."
                    #ap 2
                    #mapmove 0
                    #prot 18
                    #hp 50
                    #mr 18
                    #size 5
                    #str 15
                    #enc 0
                    #att 5
                    #def 5
                    #prec 5
                    #mor 50
                    #poisonres 25
                    #coldres 15
                    #neednoteat
                    #inanimate
                    #amphibian
                    #darkvision 100
                    #unteleportable
                    #startage -1
                    #maxage 1000
                    #gcost 0
                    #blind
                    #noitem
                    #stonebeing
                    #noleader
                    #nobadevents 0
                    #bonusspells 0 
                    #homerealm 0
                    #fixedresearch 100
                    #magicimmune
                    #end
                -- Reaper 
                    #newmonster 7458
                    #name "Reaper"
                    #spr1 "./bozmod/Confluence/LA_Diamedulla/Reaper1.tga"
                    #spr2 "./bozmod/Confluence/LA_Diamedulla/Reaper1.tga"
                    #descr "Reapers represent some of the very first Diamedullan attempts at transplanting various body parts, and even whole heads onto other test subjects. They typically appear as a grotesque creatures with four arms and a transplanted head, and are horrifying to behold. Reapers are nicknamed for their proclivity to carry huge scythes."
                    #ap 14
                    #mapmove 14
                    #prot 3
                    #hp 25
                    #mr 14
                    #size 3
                    #str 18
                    #enc 3
                    #att 14
                    #def 13
                    #prec 8
                    #mor 13
                    #gcost 0
                    #weapon 506
                    #weapon "Mutated Limb"
                    #weapon "Mutated Limb"
                    #poisonres 5
                    #diseaseres 100
                    #insane 5
                    #startage 32
                    #maxage 150
                    #goodleader
                    #goodundeadleader
                    #itemslots 15366‬
                    #fear 5
                    #holy
                    #end
                -- Void Marker 
                        #newmonster 7465    
                        #copystats 158
                        #clearmagic
                        #name "Void Marker"
                        #spr1 "./bozmod/Confluence/LA_Diamedulla/VoidMarker.tga"
                        #spr2 "./bozmod/Confluence/LA_Diamedulla/VoidMarker.tga"
                        #descr "The Void Marker is a curious Void artifact first discovered in the Antikytheran Imperium, buried and inert. Its discovery and subsequent research done by Antikytheran Technomancers and Alchemists eventually lead to a bloody civil war that split the empire in twine, a weakened Antikythera, and the newly carved-out Diamedulla which sough to control the Void Marker by any means, and unlock its vast mysteries.
                        Upon discovering the insidious properties of the Marker, mainly that is slowly but irrevocably twists and erodes the minds of those around it, the Technomancers advocated to destroy the device before its effects destroyed the whole of Antikythera. Evidently, their prognosis proved correct, though given too late, for a Void Cult had already formed from the ranks of the Alchemists. Their uncharacteristic and strange behavior was at first attributed to the typical eccentricity of those in its profession, and the effects the Marker had already exhibited on many of the possibly already slightly mad Alchemist overlooked until it was too late.
                        Now, the Void Marker sits in the capital of Diamedulla, gradually influencing the people on an entire land, twisting both their bodies and their minds beyond measure. As such, it is little wonder that Diamedulla has turned into a land of nightmare and horrifying creatures under the direction of mad mages and the all too eager scalpels of the Flesh Carvers. Indeed, the creation of the various monsters of Diamedulla is limited seemingly only by the imagination of the mad, which is to say, not at all."	
                        #ap 2
                        #mapmove 0
                        #prot 18
                        #hp 75
                        #mr 20
                        #size 6
                        #str 15
                        #enc 0
                        #att 5
                        #def 5
                        #prec 5
                        #mor 50
                        #poisonres 25
                        #coldres 15
                        #neednoteat
                        #inanimate
                        #amphibian
                        #darkvision 100
                        #unteleportable
                        #startage -1
                        #maxage 5000
                        #gcost 0
                        #voidsanity 100
                        #blind
                        #holy
                        #itemslots 28672
                        #stonebeing
                        #mindslime 25
                        #weapon 0
                        #noleader
                        #nomagicleader
                        #noundeadleader
                        #nobadevents 0
                        #bonusspells 0 
                        #homerealm 0
                        #userestricteditem 1010
                        #command 5
                        #undcommand 5
                        #magiccommand 5
                        #batstartsum1d3 -13478
                        #batstartsum1 -13478
                        #end
                -- Necrotitan 
                    #newmonster 7528
                    #name "Necrotitan"
                    #spr1 "./bozmod/Monsters/Necrotitan1.tga"
                    #spr2 "./bozmod/Monsters/Necrotitan2.tga"
                    #descr "Necrotitans are collosal monsters formed from the fusion of flesh and metal"
                    #ap 14
                    #mapmove 16
                    #prot 12
                    #hp 124
                    #mr 18
                    #size 6
                    #str 26
                    #enc 3
                    #att 18
                    #def 14
                    #prec 12
                    #mor 18
                    #gcost 0
                    #poisonres 10
                    #fireres 5
                    #shockres 5
                    #neednoteat
                    #demon
                    #weapon 229
                    #weapon 331
                    #weapon 397
                    #wastesurvival
                    #diseaseres 100
                    #insane 10
                    #spiritsight
                    #voidsanity 10
                    #startage 231
                    #gcost 300
                    #maxage 1238
                    #okleader
                    #superiorundeadleader
                    #itemslots 13446
                    #magicskill 7 3
                    #magicskill 0 1
                    #magicskill 3 1
                    #fear 10
                    #holy
                    #end    
                -- Necrotaur                             
                    #newmonster 7527
                    #name "Necrotaur"
                    #spr1 "./bozmod/Monsters/Necrotaur1.tga"
                    #spr2 "./bozmod/Monsters/Necrotaur2.tga"
                    #descr "Necrotaurs are void demons who are manifested in the world via the power of the void. They are holy to the cultists of Diamedulla and are capable of wielding banefire from the void."
                    #ap 14
                    #mapmove 16
                    #prot 3
                    #hp 38
                    #mr 16
                    #size 4
                    #str 22
                    #enc 3
                    #att 16
                    #def 11
                    #prec 11
                    #mor 18
                    #gcost 0
                    #weapon 348
                    #poisonres 5
                    #fireres 15
                    #coldres 10
                    #neednoteat
                    #demon
                    #wastesurvival
                    #diseaseres 100
                    #insane 5
                    #voidsanity 10
                    #startage 231
                    #spiritsight
                    #gcost 300
                    #maxage 1238
                    #goodleader
                    #goodundeadleader
                    #itemslots 15494
                    #magicskill 7 2
                    #custommagic 13440 200
                    #fear 5
                    #holy
                    #end
            -- Heroes   
                -- Brain Hero 
                    #newmonster 7455
                    #name "Amalgamated Brain"
                    #spr1 "./bozmod/Confluence/LA_Diamedulla/BrainGeneral1.tga"
                    #spr2 "./bozmod/Confluence/LA_Diamedulla/BrainGeneral1.tga"
                    #descr "Not used yet."
                    #prot 18
                    #ap 18
                    #mapmove 14
                    #hp 25
                    #mr 14
                    #size 4
                    #str 18
                    #enc 0
                    #att 13
                    #def 11
                    #prec 8
                    #mor 14
                    #gcost 0
                    #diseaseres 100
                    #poisonres 15
                    #noitem
                    #startage 54
                    #maxage 150
                    #heal
                    #rpcost 1
                    #unsurr 1
                    #weapon "Augmented Limb"
                    #rcost 29
                    #superiorleader
                    #goodundeadleader
                    #goodmagicleader
                    #end
            -- Pretenders
            -- Spells 
                -- Necropocalypse
                    #newspell
                    #name "Necropocalypse"
                    #descr "This spell unleashes the Necropocalypse on the world"
                    #details "2d6 Necromorphs per province per turn, enemy provinces have 5% chance to suffer 10d6 Necromorph attack"
                    #school 6
                    #restricted "Diamedulla"
                    #researchlevel 9
                    #path 0 7
                    #path 1 4
                    #pathlevel 0 8
                    #pathlevel 1 4
                    #spec 8388608 -- Castable Underwater
                    #nreff 1
                    #fatiguecost 5000
                    #effect 10081
                    #damage 579 -- necropocalypse
                    #spec 8388608 -- Castable Underwater
                    #nreff 1
                    #fatiguecost 24000
                    #end
                -- Void Alchemy
                                        #newspell
                                        #copyspell "Internal Alchemy"
                                        #name "Y"                -- Adds 30 years, removes 5 insanity
                                        #restricted 169                    
                                        #school -1                                                 
                                        #fatiguecost 0
                                        #spec 8388608
                                        #damage -15
                                        #end
                                        --
                                        #newspell
                                        #copyspell 1090                     -- Removes 30 years
                                        #name "T" 
                                        #damage -30                
                                        #school -1                           
                                        #fatiguecost 0                 
                                        #spec 8388608 --UW OK                    
                                        #nextspell "Y"
                                        #end
                    
                                    #newspell
                                    #copyspell "Internal Alchemy"
                                    #name "S"                -- Adds 30 years, removes 5 insanity
                                    #restricted 169                    
                                    #school -1                                                 
                                    #fatiguecost 0
                                    #spec 8388608
                                    #damage -15
                                    #nextspell "T"
                                    #end
                                    --
                                    #newspell
                                    #copyspell 1090                     -- Removes 30 years
                                    #name "A" 
                                    #damage -30                
                                    #school -1                           
                                    #fatiguecost 0                 
                                    #spec 8388608 --UW OK                    
                                    #nextspell "S"
                                    #end
                    
                                #newspell
                                #copyspell "Internal Alchemy"
                                #name "L"                -- Adds 30 years, removes 5 insanity
                                #restricted 169                    
                                #school -1                                                 
                                #fatiguecost 0
                                #spec 8388608
                                #damage -15
                                #nextspell "A"
                                #end
                                --
                                #newspell
                                #copyspell 1090                     -- Removes 30 years
                                #name "P" 
                                #damage -30                
                                #school -1                           
                                #fatiguecost 0                 
                                #spec 8388608 --UW OK                    
                                #nextspell "L"
                                #end
                    
                            #newspell
                            #copyspell "Internal Alchemy"
                            #name "O"                -- Adds 30 years, removes 5 insanity
                            #restricted 169                    
                            #school -1                                                 
                            #fatiguecost 0
                            #spec 8388608
                            #damage -15
                            #nextspell "P"
                            #end
                            --
                            #newspell
                            #copyspell 1090                     -- Removes 30 years
                            #name "R" 
                            #damage -30                
                            #school -1                           
                            #fatiguecost 0                 
                            #spec 8388608 --UW OK                    
                            #nextspell "O"
                            #end
                    
                        #newspell
                        #copyspell "Internal Alchemy"
                        #name "U"                -- Adds 30 years, removes 5 insanity
                        #restricted 169                    
                        #school -1                                                 
                        #fatiguecost 0
                        #spec 8388608
                        #damage -15
                        #nextspell "R"
                        #end
                        --
                        #newspell
                        #copyspell 1090                     -- Removes 30 years
                        #name "E" 
                        #damage -30                
                        #school -1                           
                        #fatiguecost 0                 
                        #spec 8388608 --UW OK                    
                        #nextspell "U"
                        #end
                    
                    #newspell
                    #copyspell "Internal Alchemy"
                    #name "N"                -- Adds 30 years, removes 5 insanity
                    #restricted 169                    
                    #school -1                          
                    #descr "No description needed."                             
                    #researchlevel 0                    
                    #fatiguecost 0
                    #spec 8388608
                    #damage -15
                    #nextspell "E"
                    #end
                    --
                    #newspell
                    #copyspell 1090                     -- Removes 30 years
                    #name "Void Alchemy" 
                    #damage -30
                    #restricted 169                 
                    #school 5                           
                    #descr "Void Alchemy was an attempt by those few remaining, sane Antikytheran Alchemists to cure their brethren of the maddening effects they were exposed to by the hands of the Void Marker. Though Void Alchemy cannot hold insanity at bay, it is more than capable of alleviating the worst of it. Unfortunately, many Mad Alchemists and Black Alchemists refuse to practice Void Alchemy for they do not consider themselves to be mad, and thus in need of it.Void Alchemy is not without some small, inherent risk as the ritual can be very straining on the body. Those of old age may suffer afflictions while performing it, or even expire."              
                    #researchlevel 5 
                    #details "Reduces insanity on Mad Alchemists and Black Alchemists by 30. Can cause death or afflictions to those of old age. Does not work on Mad Alchemists that go mindless."              
                    #path 0 4
                    #pathlevel 0 1                     
                    #path 1 -1                           
                    #fatiguecost 300                 
                    #spec 8388608 --UW OK                    
                    #nextspell "N"
                    #onlymnr 7417 -- Black Alchemist
                    #onlymnr 7418 -- Mad Alchemist
                    #onlymnr 7469 -- Brain in a Jar
                    #onlymnr 7470 -- Brain in a Jar
                    #end
                -- Locate Subject 
                    #newevent              -- 0-5 UNREST
                    #req_rare 40
                    #rarity 5
                    #req_maxunrest 5
                    #req_fornation 169
                    #req_ench 847 -- 1 Turn Enchantment
                    #msg "A successful experiment has been conducted in ##landname## and a subject with very rare characteristics located. Already, it has been placed in a containment tank and is ready to be transported to one of the Diamedullan labs for further study and experimentation."       - Successful experiment
                    #unrest 20
                    #killpop 25
                    #kill 1
                    #inccorpses 50
                    #nation -2
                    #com -13781
                    #stealthcom 7508
                    #end	
                    
                        #newevent              -- 5-15 UNREST
                        #req_rare 30
                        #rarity 5
                        #req_minunrest 6
                        #req_maxunrest 15
                        #req_fornation 169
                        #req_ench 847 -- 1 Turn Enchantment
                        #msg "A successful experiment has been conducted in ##landname## and a subject with very rare characteristics located. Already, it has been placed in a containment tank and is ready to be transported to one of the Diamedullan labs for further study and experimentation."       - Successful experiment
                        #unrest 20
                        #killpop 25
                        #kill 1
                        #inccorpses 50
                        #nation -2
                        #com -13781
                        #stealthcom 7508
                        #end	
                    
                            #newevent              -- 15-30 UNREST
                            #req_rare 20
                            #rarity 5
                            #req_minunrest 16
                            #req_maxunrest 30
                            #req_fornation 169
                            #req_ench 847 -- 1 Turn Enchantment
                            #msg "A successful experiment has been conducted in ##landname## and a subject with very rare characteristics located. Already, it has been placed in a containment tank and is ready to be transported to one of the Diamedullan labs for further study and experimentation."       - Successful experiment
                            #unrest 20
                            #killpop 25
                            #kill 1
                            #inccorpses 50
                            #nation -2
                            #com -13781
                            #stealthcom 7508
                            #end	
                        
                                #newevent              -- 30-60 UNREST
                                #req_rare 10
                                #rarity 5
                                #req_minunrest 31
                                #req_maxunrest 60
                                #req_fornation 169
                                #req_ench 847 -- 1 Turn Enchantment
                                #msg "A successful experiment has been conducted in ##landname## and a subject with very rare characteristics located. Already, it has been placed in a containment tank and is ready to be transported to one of the Diamedullan labs for further study and experimentation."       - Successful experiment
                                #unrest 20
                                #killpop 25
                                #kill 1
                                #inccorpses 50
                                #nation -2
                                #com -13781
                                #stealthcom 7508
                                #end	
                    
                                
                    #newevent
                    #rarity 5
                    #req_nomonster 7508
                    #req_fornation 169
                    #req_ench 847 -- 1 Turn Enchantment - Was orginally placed first in the chain for some reason? Might have been a good one?
                    #msg "The experiment has failed to produce a subject with sufficient abnormalities to merit further study and experimentation."       - Experiment failed, unrest too high       - Failed experiment
                    #stealthcom 7504
                    #unrest 20
                    #killpop 30
                    #kill 1
                    #inccorpses 50
                    #end
                    
                        #newevent                  -- Removes 7508 Dummy
                        #rarity 5
                        #req_targforeignok
                        #req_indepok 1
                        #req_pop0ok
                        #msg "No need."
                        #req_targmnr 7508
                        #poison 999
                        #notext
                        #nolog
                        #end
                        
                            -- Spell Actual
                            
                            
                    #newspell
                    #name "Initiate Experiment"
                    #descr "An experiment is initiated by a Collector, an attempt to locate a subject with abnormalities and uncharacteristic mutations for further study. Only one experiment may be conducted in any land at any given time."
                    #school -1 
                    #details "Captures a specimen in a Containment Tank. Success chance varies with local unrest. The success rate of capturing a specimen in a land with up to 5 unrest = 40% chance. 5 to 15 = 30%, 15 to 30 = 20%, 30 to 60 = 10%. Captured specimens can be moved with Dimension Splitter items."
                    #researchlevel 0
                    #effect 10082
                    #damage 847
                    #end
                    
                        #newmonster 7504	
                        #copyspr 1369
                        #copystats 1369
                        #stealthy 999
                        #name "Test Subject Dummy"
                        #descr "No need."
                        #mr 50
                        #mor 50
                        #hp 100
                        #landdamage 100
                        #invisible
                        #end	
                    
                        #newmonster 7508	
                        #copyspr 1369
                        #copystats 1369
                        #stealthy 999
                        #name "Specimen Containment Tank"
                        #descr "No need."
                        #mr 50
                        #mor 50
                        #hp 100
                        #landdamage 100
                        #invisible
                        #end
                -- Overcharge 
                
                        #newspell
                        #copyspell "Protection from Lightning"
                        #name "Lightning Resistance"
                        #school -1
                        #spec 278937608  -- Can UW, undead+demon only, no demon, ignore shield, hard to hit ethereal
                        #end
                    
                    #newspell 
                    #copyspell "Charge Body"
                    #name "Overcharge"
                    #descr "The bodies of a few undead are charged with electricity. When a charged body is struck in melee combat, electricity will strike both combatants. The damage caused by the electrical charge is deadly and bypasses the protection of armor."
                    #school 1
                    #details "Gives Charged Body buff to undead only."
                    #researchlevel 4
                    #range 10
                    #restricted 169
                    #path 0 1
                    #pathlevel 0 2
                    #path 1 5
                    #pathlevel 1 2
                    #fatiguecost 80
                    #spec 278937608  -- Can UW, undead+demon only, no demon, ignore shield, hard to hit ethereal
                    #aoe 1003 
                    #nextspell "Lightning Resistance"
                    #end
                -- Noxious Zeppelin 
                    
                    
                    #newspell 
                    #name "Construct Plague Ship"
                    #descr "Constructs a Noxious Zeppelin, a dirigible airship capable of traveling great distances while carrying containers of poisonous alchemical compounds that are then dispersed over densely populated areas from on high."
                    #school 3
                    #researchlevel 5
                    #effect 10021
                    #damage 7475
                    #nreff 1
                    #restricted 169
                    #school 3
                    #path 0 1
                    #pathlevel 0 3
                    #path 1 5
                    #pathlevel 1 3
                    #researchlevel 5
                    #fatiguecost 2500 
                    #end
                -- Create Cipher 
                
                    
                    #newspell 
                    #name "Deciphering the Void"
                    #descr "A Deciphering Engine is created, a device used to break the enigma that is the Void Marker and tame all of its secrets. Due to its massive size, only a single Deciphering Engine can be effectively used per laboratory. Each additional laboratory equipped with a Deciphering Engine will speed up the research on the Void Marker."
                    #school 3
                    #details "Each Deciphering Engine summoned has a flat 3% chance to uncover a uncover a Void Marker specific item. There are a total of 3 such items to be uncovered. Only one Deciphering Engine will have an effect per province and stacking more will not increase odds of success."
                    #researchlevel 0
                    #effect 10021
                    #damage 7472
                    #nreff 1
                    #restricted 169
                    #path 0 3 -- E
                    #path 1 4 -- S
                    #pathlevel 0 2
                    #pathlevel 1 2
                    #fatiguecost 5000
                    #end
                -- Create Reper 
                    
                    
                    #newspell 
                    #name "Create Reaper"
                    #descr "With this somewhat archaic procedure several bodies are mutilated and conjoined into three Reapers. Though the creation of Reapers is outdated in Diamedulla due to all the new technological wonders and recent discoveries, the Reapers themselves are still quite deadly and terrifying, though they lack the typical protective gear of Diamedullan soldiers."
                    #school 6
                    #researchlevel 4
                    #effect 10001
                    #damage 7458
                    #nreff 3
                    #restricted 169
                    #path 0 7 -- D
                    #path 1 3 -- E
                    #pathlevel 0 2
                    #pathlevel 1 2
                    #fatiguecost 1800
                    #end
                -- Brain Twiceborn 
                    
                    
                    #newspell
                    #copyspell "Twiceborn"
                    #name "Brain Extraction"
                    #descr "With this grim procedure a Black Alchemist's head is harvested from his body post mortem and placed in a fluid container. The success rate of such a complicated and unorthodox procedure - even for Diamedullan standards - is nowhere close to guaranteed and as such roughly 30% of subjects are lost during the procedure, or within the first few weeks. This type of head transplant procedure is exceedingly difficult as it is never performed on living subjects as is the practice, but rather as a fail safe to attempt to preserve, or rather revive Black Alchemists who have died to unforeseen circumstances.
                    Though decapitating subjects and transplanting their heads onto various other beings or constructs is a common practice in Diamedulla, doing so on already dead subject, possibly days after, not so much. Only the Black Alchemists are privy to such secrets and they guard them closely."
                    #fatiguecost 100
                    #details "A Black Alchemist is revived as a Brain in a Jar in his home province after death. The Brain in a Jar will perish 30% of the time after revival."
                    #path 0 5
                    #pathlevel 0 1
                    #path 1 7
                    #pathlevel 1 1
                    #school 4
                    #researchlevel 4
                    #onlymnr 7418
                    #restricted 169
                    #twiceborn 7449
                    #end
                -- Experiment 
                    #newspell 
                    #name "Clockwork Conversion"
                    #descr "The Clockwork Conversion is the physical and mental re-engineering of victims in the infamous Clockwork Horror, the birthing place of many a Diamedullan horrors. Conversion involves the replacement of body parts - including limbs, organs, and vital systems - with artificial components, or even different types of limbs, organs, and vital systems. Such experimental procedures typically produce a wide variety of creations." 
                    #school 3
                    #details "Creates one of three possible monsters; Psychic Construct, Expedient Collector, and Baneful Lantern. Once all three types of monsters are created, a Diamedullan Trinity may also be created from them. Spell may only be cast in a Clockwork Horror."
                    #researchlevel 5 -- Creates the Unity commanders
                    #effect 10021
                    #damage -13109
                    #nreff 1
                    #restricted 169
                    #path 0 7 -- B
                    #path 1 3 -- E
                    #pathlevel 0 4
                    #pathlevel 1 2
                    #fatiguecost 3000
                    #onlyatsite "Clockwork Horror"
                    #end
                -- Summon Necrotaur 
                    #newspell 
                    #name "Summon Necrotaur"
                    #descr "The caster opens a gate to the void and summons a Necrotaur"
                    #school 6
                    #researchlevel 6
                    #effect 10001
                    #damage 7527
                    #nreff 1
                    #restricted 169
                    #path 0 7 - B
                    #path 1 5 - D
                    #pathlevel 0 3
                    #pathlevel 1 2
                    #fatiguecost 7200
                    #end                        
                -- Summon Necrotitan                        
                    #newspell 
                    #name "Summon Necrotitan"
                    #descr "The caster opens a gate to the void and summons a Necrotitan"
                    #school 6
                    #researchlevel 9
                    #effect 10001
                    #damage 7528
                    #nreff 1
                    #restricted 169
                    #path 0 7 - B
                    #path 1 5 - D
                    #pathlevel 0 6
                    #pathlevel 1 2
                    #fatiguecost 11000
                    #end                        

            -- Sites
                -- Clockwork Horror ---------------------------------------------------------------
                
                    
                    #newsite 1690
                    #name "Clockwork Horror" 
                    #path 5
                    #level 1
                    #gems 3 1
                    #gems 4 1
                    #gems 5 1
                    #gems 7 2
                    #rarity 5
                    #end
                -- Virulent Temple ---------------------------------------------------------------
                    
                    
                    #newsite 1691
                    #name "Virulent Temple" 
                    #path 5
                    #level 0
                    #incscale 3 -- Increases Death
                    #rarity 5
                    #end
                -- Virulent Horror ---------------------------------------------------------------
                        
                        
                        #newsite 1692
                        #name "Virulent Horror" 
                        #path 5
                        #level 0
                        #incscale 3 -- Increases Death
                        #rarity 5
                        #end
            -- Nation Info
                -- Base
                    #selectnation 169
                    #name "Diamedulla"
                    #epithet "Scions of Madness"
                    #era 2
                    #descr "Diamedulla is a new empire, a twisted offspring of the Antikytheran Imperium built upon countless corpses and a river of blood, but most importantly madness. It is an empire centered around a mysterious Void artifact known as the Void Marker. Indeed, it was the discovery of this strange object that gave rise to Diamedulla, or rather the forming of the Void cults that would eventually spark a revolution and bring the entire Imperium to its knees. 
                    By the time the insidious properties of the Void Marker were made known to the Technomancers of Antikythera, the damage had already been done. Madness had already spread among the ranks of those who had investigated the strange artifact, initially covered under a veil of eccentricity that typically accompanied the Alchemists of Antikythera, those mages who came into contact with the Void Marker the most. It was not until an attempt to destroy the Void Marker was made by the Technomancers that many of the now mad Alchemists rebelled. 
                    The ensuing civil war would inevitably end in the splintering of Antikythera and the rise of a new empire ruled by the mad victors. Those who would stop at nothing to ensure their twisted vision was fulfilled for the good of all. As such it is little wonder that life in Diamedulla is far from idyllic. Indeed, what the mad envision as paradise is a grim reality for those with a sliver of sanity, though such people seem to be in ever shorter supply in Diamedulla as the insidious effects of the Void Marker continue to erode the minds of all those in it. Horrifying experiments are now secretly conducted on the working people of Diamedulla. Many a worker goes to bed only to wake up strapped to an operating table under the intense scrutiny of a Flesh Carver holding grisly instruments, or even creatures. Whispers of secret, underground laboratories and ungodly abominations spread across the empire."
                    #summary "Race: Augmented humans.
                    Military: Various slave workers, hybrid heavy infantry, tramplers, towering goliaths of flesh and metal, mortars and gas weapons.
                    Magic: Earth, Astral, Death, Blood
                    Priests: Weak."
                    #brief "Diamedulla is a newly created empire built on countless corpses and a river of blood. It is a nightmarish land that has fallen under the insidious and warping influence of the strange Void artifact known as the Void Marker. Madness rules supreme in Diamedulla, quite literally. It is a place where sickening and unimaginable experiments are performed on the unwilling population, and where horrifying abominations are created under the guidance of its rulers."
                    #color 0.4 0.1 0
                    #flag "./bozmod/Confluence/LA_Diamedulla/EvilAntiFlag.tga"
                    --labcost 500
                    #templepic 9 -- Cathedral 
                    #fortera 3
                    #hatesterr 128
                -- Start Units 
            
            
                    #startcom 7454
                    #startscout 7407 --flesh golem
                    #startunittype1 7404
                    #startunitnbrs1 12
                    #startunittype2 7405 -- Noxious Infantry
                    #startunitnbrs2 6
                -- Start Sites 
            
            
                    #startsite "Clockwork Horror"
                -- PD 
            
                    #defcom1 7454
                    #defcom2 7413
                    #defunit1 -13777  -- Recombinant
                    #defunit2 -13777  -- Recombinant
                    
                    #defmult1 15
                    #defmult2 15
                -- Fort PD 
                
                
                    #wallcom 7454
                    #wallunit 7406
                    #wallmult 1
                -- Recruitable Troops 
                
                    
                    #addrecunit 7400
                    #addrecunit 7401
                    #addrecunit 7402
                    #addrecunit 7403
                    #addrecunit 7404
                    #addrecunit 7405
                    #addrecunit 7406
                    #addrecunit 7466
                    #addrecunit 7408
                    #addrecunit 7450
                    #addrecunit 7409
                -- Recruitable Commanders 
                    
                    #addreccom 426
                    #addreccom 7407
                    #addreccom 7454
                    #addreccom 7415
                    #addreccom 7414
                    #addreccom 7412
                    #addreccom 7416
                    #addreccom 7417
                    #addreccom 7418
                -- National Heroes 
                
                    #multihero1 7455 - Amalgamated Brain
                -- Available Gods 
                    #end
        -- Cangobia fix armorhead, add flying shaman spell
            -- Montags (7001 lesser dracon) (7002 dracon rider) (7003 greater dracon) (7005 dragons) (7004 shamans)
            -- Name Types            
                #selectnametype 251
                #clear
                #addname "Sugbu"
                #addname "Yambul"
                #addname "Zalthu"
                #addname "Snaglak"
                #addname "Noogugh"
                #addname "Varbu"
                #addname "Podagog"
                #addname "Cukgilug"
                #addname "Xarpug"
                #addname "Jughragh"
                #addname "Murbol"
                #addname "Bashuk"
                #addname "Ugor"
                #addname "Mog"
                #addname "Ghak"
                #addname "Murob"
                #addname "Ulumpha"
                #addname "Ushug"
                #addname "Sharn"
                #addname "Dura"
                #addname "Raghat"
                #addname "Brokil"
                #addname "Pargu"
                #addname "Hibub"
                #addname "Jughog"
                #addname "Nurghed"
                #addname "Ditgurat"
                #addname "Durz"
                #addname "Kurdan"
                #addname "Bugdul"
                #addname "Sharamph"
                #addname "Homraz"
                #addname "Sharn"
                #addname "Murob"
                #addname "Oghash"
                #addname "Shagdub"
                #addname "Durgat"
                #addname "Atub"
                #addname "Bolar"
                #addname "Snak"
                #addname "Numhug"
                #addname "Sulmthu"
                #addname "Yakha"
                #addname "Urgran"
                #addname "Vrothu"
                #addname "Sakgu"
                #addname "Sahgigoth"
                #addname "Matuk"
                #addname "Rodagog"
                #addname "Ertguth"
                #addname "Sharn"
                #addname "Orbul"
                #addname "Durgat"
                #addname "Gashnakh"
                #addname "Kharzug"
                #addname "Arob"
                #addname "Dura"
                #addname "Garakh"
                #addname "Shufharz"
                #addname "Gul"
                #addname "Wegub"
                #addname "Eichelberbog"
                #addname "Wurgoth"
                #addname "Durzol"
                #addname "Wudhagh"
                #addname "Digdug"
                #addname "Zabub"
                #addname "Omogulg"
                #addname "Mugdul"
                #addname "Jokgagu"
                #addname "Borgakh"
                #addname "Bulfim"
                #addname "Arob"
                #addname "Durz"
                #addname "Bulfim"
                #addname "Snak"
                #addname "Atub"
                #addname "Bagrak"
                #addname "Snak"
                #addname "Yazgash"
                #addname "Bidgug"
                #addname "Slaugh"
                #addname "Farod"
                #addname "Hagu"
                #addname "Zunuguk"
                #addname "Bogrum"
                #addname "Oogorim"
                #addname "Unrugagh"
                #addname "Onugug"
                #addname "Omugug"
                #addname "Batul"
                #addname "Agrob"
                #addname "Dura"
                #addname "Gluronk"
                #addname "Rogbut"
                #addname "Durgat"
                #addname "Mog"
                #addname "Shadbak"
                #addname "Bula"
                #addname "Durgat"
                #end
            -- Events
                -- Chosen Pup
                        #newevent 
                        #req_targmnr "Cangob Youngling"
                        #killmon 7104
                        #com  "Cangob Shaman"
                        #req_magic 2
                        #req_luck 1
                        #nation -2
                        #rarity 5
                        #msg "One of your younglings has been found to have a Draconic mark, he's become trained to be a Shaman"
                        #req_rare 3
                        #end
                -- Mushroom of Awakening
                        #newevent 
                        #req_targitem "Mushroom of Awakening"
                        #req_targmnr "Cangob Shaman"
                        #transform "Awakened Shaman"
                        #fireboost 1
                        #rarity 5
                        #msg "Your Shaman has consumed enough Mushrooms of Awakening that they have taken hold of his mind, granting new magical insights at the cost of his sanity"
                        #req_rare 20
                        #end
                        
                        #newevent 
                        #req_targitem "Mushroom of Awakening"
                        #req_targmnr "Cangob Shaman"
                        #transform "Awakened Shaman"
                        #airboost 1
                        #msg "Your Shaman has consumed enough Mushrooms of Awakening that they have taken hold of his mind, granting new magical insights at the cost of his sanity"
                        #rarity 5
                        #req_rare 20
                        #end

                        #newevent 
                        #req_targitem "Mushroom of Awakening"
                        #req_targmnr "Cangob Shaman"
                        #transform "Awakened Shaman"
                        #waterboost 1
                        #msg "Your Shaman has consumed enough Mushrooms of Awakening that they have taken hold of his mind, granting new magical insights at the cost of his sanity"
                        #rarity 5
                        #req_rare 20
                        #end

                        #newevent 
                        #req_targitem "Mushroom of Awakening"
                        #req_targmnr "Cangob Shaman"
                        #transform "Awakened Shaman"
                        #earthboost 1
                        #msg "Your Shaman has consumed enough Mushrooms of Awakening that they have taken hold of his mind, granting new magical insights at the cost of his sanity"
                        #rarity 5
                        #req_rare 20
                        #end

                        #newevent 
                        #req_targitem "Mushroom of Awakening"
                        #req_targmnr "Cangob Shaman"
                        #transform "Awakened Shaman"
                        #astralboost 1
                        #msg "Your Shaman has consumed enough Mushrooms of Awakening that they have taken hold of his mind, granting new magical insights at the cost of his sanity"
                        #rarity 5
                        #req_rare 20
                        #end

                        #newevent 
                        #req_targitem "Mushroom of Awakening"
                        #req_targmnr "Cangob Shaman"
                        #transform "Awakened Shaman"
                        #deathboost 1
                        #msg "Your Shaman has consumed enough Mushrooms of Awakening that they have taken hold of his mind, granting new magical insights at the cost of his sanity"
                        #rarity 5
                        #req_rare 20
                        #end

                        #newevent 
                        #req_targitem "Mushroom of Awakening"
                        #req_targmnr "Cangob Shaman"
                        #transform "Awakened Shaman"
                        #natureboost 1
                        #msg "Your Shaman has consumed enough Mushrooms of Awakening that they have taken hold of his mind, granting new magical insights at the cost of his sanity"
                        #rarity 5
                        #req_rare 20
                        #end

                        #newevent 
                        #req_targitem "Mushroom of Awakening"
                        #req_targmnr "Cangob Shaman"
                        #transform "Awakened Shaman"
                        #bloodboost 1
                        #msg "Your Shaman has consumed enough Mushrooms of Awakening that they have taken hold of his mind, granting new magical insights at the cost of his sanity"
                        #rarity 5
                        #req_rare 20
                        #end
            -- Spells
                -- Call the Beakwings
                                    #newspell
                                    #name "Summon Beakwing Scout"
                                    #descr "Bind a number of Dragon lords to your cause"
                                    #details "Summon a lot of Dragons"
                                    #school -1
                                    #researchlevel 4
                                    #path 0 1
                                    #pathlevel 0 2
                                    #path 1 6
                                    #pathlevel 1 1           
                                    #fatiguecost 1200
                                    #damage 7114
                                    #effect 10021
                                    #nreff 1
                                    #restricted "Cangobia"
                                    #end 

                    #newspell
                    #name "Call the Beakwings"
                    #descr "Summons a Beakwing Raider and a Beakwing Scout leading them"
                    #details "This spell is affected by the dragon master ability"
                    #school 0
                    #researchlevel 4
                    #path 0 1
                    #pathlevel 0 2
                    #path 1 6
                    #pathlevel 1 1                    
                    #fatiguecost 1200
                    #damagemon 7113
                    #nextspell "Summon Beakwing Scout"
                    #effect 10001
                    #nreff 1001
                    #restricted "Cangobia"
                    #end
                -- Bind Dragon Lords
                    -- Chains

                                    #newspell
                                    #name "Bind Dragon Lords 2"
                                    #descr "Bind a number of Dragon lords to your cause"
                                    #details "Summon a lot of Dragons"
                                    #school -1
                                    #researchlevel 9
                                    #path 0 0
                                    #pathlevel 0 6                    
                                    #fatiguecost 8000
                                    #damage -7005
                                    #effect 10021
                                    #nreff 1
                                    #restricted "Cangobia"
                                    #end 

                                    #newspell
                                    #name "Bind Dragon Lords 1"
                                    #descr "Bind a number of Dragon lords to your cause"
                                    #details "Summon a lot of Dragons"
                                    #school -1
                                    #researchlevel 9
                                    #path 0 0
                                    #pathlevel 0 6                    
                                    #fatiguecost 8000
                                    #damage -7005
                                    #nextspell "Bind Dragon Lords 2"
                                    #effect 10021
                                    #nreff 1
                                    #restricted "Cangobia"
                                    #end 

                    #newspell
                    #name "Bind Dragon Lords"
                    #descr "Binds three Dragon lords to join your cause"
                    #details "Summon a lot of Dragons"
                    #school 0
                    #researchlevel 8
                    #path 0 0
                    #pathlevel 0 6    
                    #path 1 6
                    #pathlevel 1 3                   
                    #fatiguecost 8000
                    #damage -7005
                    #nextspell "Bind Dragon Lords 1"
                    #effect 10021
                    #nreff 1
                    #restricted "Cangobia"
                    #end 
                -- Draconic Totem
                    #newspell
                    #name "Erect Draconic Totem"
                    #descr "The caster constructs a great totem depicting many Dracons, coiling a great pillar that ends with a gaping maw of a Dracon. From this maw, worshippers may find magical gems of fire."
                    #school 3
                    #researchlevel 0
                    #path 0 0
                    #pathlevel 0 3
                    #path 1 6
                    #pathlevel 1 2
                    #effect 10082
                    #damage 580
                    #fatiguecost 1200
                    #nreff 1
                    #end


                    #newspell
                    #copyspell 106 -- Record of Creation
                    #name "Draconic Totem"
                    #descr "The caster constructs a great totem depicting many Dracons, coiling a great pillar that ends with a gaping maw of a Dracon. From this maw, worshippers may find magical gems of fire. One must be careful as these totems, fickle as the dragons they represent - do not care who worships them and indeed, it might be other nations who adopt some Dragon worship for the benefits it brings"
                    #details "This spell permanently creates a site that grants 20% * preaching level of producing 1d3 fire gems"
                    #school 3
                    #researchlevel 5
                    #path 0 0
                    #pathlevel 0 3
                    #path 1 6
                    #pathlevel 1 2
                    #effect 10083
                    #damage -1
                    #fatiguecost 1200
                    #nextspell "Erect Draconic Totem"
                    #end

                    #newsite 1404
                    #name "Dracon Totem"
                    #path 0
                    #descale 0
                    #end

                    #newevent
                    #rarity 5
                    #req_site 1404
                    #req_preach 20
                    #msg "Your worshippers have been praying at the fire totem for weeks, finally their prayers have been answered and a fire fire gems appeared in the totem's mouth [Dracon Totem]"
                    #1d3vis 0 -- fire gems
                    #end

                    #newevent
                    #rarity 5
                    #req_ench 580
                    #req_pop0ok
                    #req_freesites 1
                    #addsite 1404
                    #req_nositenbr 1404
                    #msg "Your shamans have erected a great draconic monument![Dracon Totem]"
                    #end
                -- Song of Savagery
                    #newspell 
                    #name "Song of Savagery"
                    #school -1
                    #descr "The caster plays an inspiring melody that incites Cangobs into a frenzy"
                    #researchlevel 0
                    #path 0 8
                    #pathlevel 0 1
                    #fat 10
                    #effect 10
                    #damage 256
                    #nreff 1
                    #aoe 3
                    #sample "./bozmod/Cangobia/Mountainspine.wav"
                    #range 20
                    #end                    
                -- Mountainspine
                    #newspell
                    #name "Awaken the Mountainspine"
                    #descr "The caster calls the Mountainspine, the mighty beast that in all of Cangobia's history, is second only to the Progenitor"
                    #school 0
                    #researchlevel 9
                    #path 0 6
                    #pathlevel 0 6
                    #path 1 0
                    #pathlevel 1 3
                    #fatiguecost 11000
                    #damagemon "The Mountainspine"
                    #effect 10021
                    #nreff 1
                    #restricted "Cangobia"
                    #end
                -- Dracon Longneck
                    #newspell
                    #name "Summon Dracon Longneck"
                    #descr "Summons a Dracon Longneck"
                    #details "This creature produces eggs which eventually hatch into Dracons"
                    #school 0
                    #researchlevel 3
                    #path 0 6
                    #pathlevel 0 2
                    #fatiguecost 500
                    #damagemon "Dracon Longneck"
                    #effect 10021
                    #nreff 1
                    #restricted "Cangobia"
                    #end
                -- Spirit Binding
                    #newspell
                    #name "Spirit Binding"
                    #descr "Binds the spirit of the mage to their home, if they die they are reborn as an immobile spirit"
                    #details "twiceborn into ghost shape that adds astral and/or death"
                    #school 5
                    #researchlevel 3
                    #path 0 0
                    #pathlevel 0 2                    
                    #fatiguecost 600
                    #effect 10023
                    #damage 4194304
                    #restricted "Cangobia"
                    #end
                -- Draconic Assault
                    #newspell
                    #name "Draconic Assault"
                    #descr "Summon a swarm of dinosaurs that appear on the edge of the battlefield"
                    #details "This spell is affected by the dragon master ability"
                    #school 5
                    #researchlevel 5
                    #path 0 6
                    #pathlevel 0 2                    
                    #fatiguecost 100
                    #damagemon -7001
                    #effect 43
                    #nreff 1003
                    #restricted "Cangobia"
                    #end
                -- Dracon Tyrant
                    #newspell
                    #name "Summon Dracon Tyrant"
                    #descr "Summons a Dracon Tyrant"
                    #details "This spell is affected by the dragon master ability"
                    #school 0
                    #researchlevel 7
                    #path 0 6
                    #pathlevel 0 2
                    #path 1 0
                    #pathlevel 1 3                    
                    #fatiguecost 2100
                    #damagemon "Cangob Tyrant"
                    #effect 10001
                    #nreff 1001
                    #restricted "Cangobia"
                    #end 
                -- Dracon Armorhead
                    #newspell
                    #name "Summon Dracon Armorhead"
                    #descr "Summons a Dracon Armorhead rider"
                    #details "This spell is affected by the dragon master ability"
                    #school 0
                    #researchlevel 6
                    #path 1 6
                    #pathlevel 1 1
                    #path 0 0
                    #pathlevel 0 2                    
                    #fatiguecost 1800
                    #damagemon "Dracon Armorhead"
                    #effect 10001
                    #nreff 1001
                    #restricted "Cangobia"
                    #end            
                -- Dragonfang Spider
                    #newspell 
                    #restricted  "Cangobia"
                    #name "Dragonslaying Spiders"
                    #school -1
                    #descr "Summons a number of Dragonfang spiders"
                    #details "Dragonfang spiders are little spiders, with soul-slaying attacks"
                    #path 0 6
                    #pathlevel 0 2
                    #path 1 0
                    #pathlevel 1 1
                    #fat 20
                    #effect 1
                    #damage 7136
                    #nreff 1
                    #range 5020
                    #flightspr 404
                    #sound 50
                    #end
                -- Throw Spiders
                    #newspell 
                    #restricted  "Cangobia"
                    #restricted  53 -- MA Machaka
                    #name "Throw Spiders"
                    #school 2
                    #descr "The user throws very poisonous spiders at its enemies"
                    #researchlevel 3
                    #path 0 6
                    #pathlevel 0 2
                    #fat 20
                    #effect 1
                    #damage 7137
                    #nreff 1003
                    #range 5020
                    #flightspr 404
                    #sound 50
                    #end
            -- Weapons 
                -- Dracon Tooth Spear
                    #newweapon
                    #copyweapon "Sharktooth Spear"
                    #name "Dracon Tooth Spear"                    
                    #end
                -- Draconerang
                    #newweapon
                    #name "Draconerang"
                    #dmg -3
                    #sound 15
                    #range -1
                    #rcost 1
                    #slash
                    #blunt
                    #magic
                    #woodenweapon
                    #nratt -1
                    #flyspr 304
                    #ammo 30
                    #nouw
                    #end
                -- Mountainspine
                    #newweapon
                    #copyweapon "Tremor"
                    #name "Mountainspine Earthshock"
                    #sample "./bozmod/Cangobia/Mountainspine.wav"
                    #end
            -- Units                
                -- Cangob Youngling
                    #newmonster 7104
                    #name "Cangob Youngling"                    
                    #spr1 "./bozmod/Cangobia/CangobPup.tga"
                    #spr2 "./bozmod/Cangobia/CangobPup2.tga"
                    #hp 4
                    #size 1
                    #prot 0
                    #mr 6
                    #mor 4
                    #str 5
                    #att 6
                    #def 3
                    #prec 6
                    #ap 10
                    #mapmove 10
                    #gcost 1
                    #rpcost 1
                    #rcost 1
                    #animal
                    #xpshape 4
                    #xploss 100
                    #undisciplined 
                    #weapon 322 --bite
                    #end
                -- Cangob
                    #newmonster 7105
                    #name "Cangob" 
                    #spr1 "./bozmod/Cangobia/Cangob.tga"
                    #spr2 "./bozmod/Cangobia/Cangob2.tga"
                    #hp 10 
                    #size 2 
                    #prot 2 
                    #mr 10 
                    #mor 8 
                    #str 11 
                    #att 9 
                    #def 8 
                    #prec 10 
                    #ap 10
                    #mapmove 18
                    #gcost 8
                    #animal
                    #undisciplined
                    #xpshape 20
                    #xploss 100
                    #rpcost 8
                    #supplybonus -2
                    #rcost 1
                    #weapon 322 --bite
                    #weapon 1 --spear
                    #end
                -- Cangob Soldier
                    #newmonster 7106 
                    #name "Cangob Soldier" 
                    #spr1 "./bozmod/Cangobia/CangobSoldier.tga"
                    #spr2 "./bozmod/Cangobia/CangobSoldier2.tga"
                    #hp 14 
                    #size 2 
                    #prot 2 
                    #mr 11 
                    #mor 10 
                    #str 12 
                    #att 11 
                    #def 10 
                    #prec 10 
                    #ap 10
                    #animal
                    #mapmove 18
                    #gcost 18
                    #rpcost 10
                    #rcost 4
                    #armor "Furs"
                    #supplybonus -2
                    #weapon 322 --bite
                    #weapon "Dracon Tooth Spear" --spear
                    #formationfighter 2
                    #end
                -- Cangob Hunter
                    #newmonster 7107 
                    #name "Cangob Hunter"
                    #spr1 "./bozmod/Cangobia/CangobHunter.tga"
                    #spr2 "./bozmod/Cangobia/CangobHunter2.tga" 
                    #hp 11 
                    #size 2 
                    #prot 2 
                    #mr 10 
                    #mor 10 
                    #str 11 
                    #att 10 
                    #def 10 
                    #prec 11 
                    #ap 11
                    #mapmove 20
                    #gcost 18F
                    #animal
                    #stealthy
                    #armor "Furs"
                    #rpcost 13
                    #rcost 8
                    #supplybonus -1
                    #weapon 322 --bite
                    #weapon 1 --spear
                    #weapon "Draconerang"
                    #forestsurvival
                    #wastesurvival
                    #skirmisher
                    #end
                -- Cangob Savage
                    #newmonster 7108 
                    #name "Cangob Savage"          
                    #spr1 "./bozmod/Cangobia/CangobSavage.tga"
                    #spr2 "./bozmod/Cangobia/CangobSavage2.tga" 
                    #drawsize -25
                    #hp 16 
                    #size 2 
                    #prot 6 
                    #mr 12 
                    #mor 14 
                    #str 14 
                    #att 12 
                    #def 7 
                    #prec 10 
                    #ap 12
                    #mapmove 20
                    #gcost 18
                    #animal
                    #stealthy 
                    #rpcost 14
                    #rcost 1
                    #supplybonus -3
                    #forestsurvival
                    #wastesurvival
                    #weapon 322 --bite
                    #weapon 33 --claws
                    #undisciplined 
                    #berserk  3
                    #end
                -- Cangob Dragonguard
                    #newmonster 7110 
                    #name "Cangob Dragonguard" 
                    #spr1 "./bozmod/Cangobia/Dragonguard.tga"
                    #spr2 "./bozmod/Cangobia/Dragonguard2.tga"
                    #hp 32 
                    #size 4 
                    #prot 10 
                    #mr 13 
                    #mor 16 
                    #str 17 
                    #att 14 
                    #def 12 
                    #prec 11 
                    #ap 14
                    #mapmove 18
                    #gcost 65
                    #rcost 45
                    #rpcost 49
                    #bodyguard 
                    #formationfighter 2
                    #fireres 10
                    #poisonres 5
                    #holy
                    #coldblooded
                    #reclimit 3     
                    #weapon 322 --bite      
                    #weapon 426 --glaive       
                    #armor 36 --dragon scale mail
                    #armor 150 --bone helmet
                    #end
                -- Slave Troll
                    #newmonster 7111 
                    #copystats 518
                    #name "Slave Troll"                     
                    #copyspr 2219
                    #gcost 20
                    #singlebattle
                    #mor 6
                    #clearweapons
                    #weapon 92 --fist
                    #slave                    
                    #supplybonus 25                    
                    #end
                -- Cangob Raptor Rider
                    #newmonster 7112 
                    #name "Cangob Raider" 
                    #montag 7002
                    #spr1 "./bozmod/Cangobia/CangobRaider.tga"
                    #spr2 "./bozmod/Cangobia/CangobRaider2.tga"
                    #hp 11 
                    #size 3 
                    #prot 11 
                    #mr 11 
                    #mor 12 
                    #str 12 
                    #att 12 
                    #def 13 
                    #prec 10 
                    #ap 18
                    #mapmove 22
                    #gcost 40
                    #rcost 5
                    #rpcost 30
                    #mounted                    
                    #supplybonus -2
                    #forestsurvival
                    #weapon 19 --mounted bite
                    #armor "Furs"
                    #holy
                    #coldblood
                    #weapon "Dracon Tooth Spear" --spear
                    #weapon 21 --javelin
                    #secondshape 7132 --raptor
                    #pillagebonus 3
                    #drake
                    #end
                -- Raptor
                    #newmonster 7132 
                    #name "Dracorunner" 
                    #spr1 "./bozmod/Cangobia/Raptor.tga"
                    #spr2 "./bozmod/Cangobia/Raptor2.tga"
                    #montag 7001
                    #hp 28 
                    #size 3 
                    #prot 9 
                    #mr 10 
                    #mor 14 
                    #str 14 
                    #att 13 
                    #def 9 
                    #prec 8 
                    #ap 16
                    #mapmove 22
                    #gcost 50
                    #coldblood
                    #drake
                    #animal
                    #holy
                    #weapon 322 --bite
                    #armor "Furs"
                    #weapon 33 --claws
                    #end        
            -- Commanders
                -- Cangob Beakwing Leader
                    #newmonster 7114 
                    #name "Cangob Beakwing Raid Leader" 
                    #spr1 "./bozmod/Cangobia/BeakwingScout.tga"
                    #spr2 "./bozmod/Cangobia/BeakwingScout2.tga"
                    #hp 11 
                    #size 4 
                    #prot 11 
                    #mr 11 
                    #mor 12 
                    #str 12 
                    #att 12  
                    #def 16 
                    #prec 11 
                    #ap 16
                    #mapmove 32
                    #gcost 70
                    #flying 
                    #mounted
                    #rpcost 1
                    #rcost 4
                    #drake
                    #animal
                    #coldblood 
                    #pillage 1
                    #armor "Furs"
                    #holy
                    #coldblood
                    #patrolbonus 1
                    #poorleader
                    #weapon "Dracon Tooth Spear" --spear
                    #weapon 1680 --beak
                    #weapon 21 --javelin
                    #secondtmpshape 7133
                    #end
                -- Cangob Scout
                    #newmonster 7115 
                    #spr1 "./bozmod/Cangobia/CangobScout.tga"
                    #spr2 "./bozmod/Cangobia/CangobScout2.tga"
                    #copystats 7107
                    #name "Cangob Hunt leader" 
                    #poorleader
                    #rpcost 1
                    #rcost 4
                    #gcost 35
                    #end
                -- Cangob Raptor Leader
                    #newmonster 7116 
                    #copystats 7112
                    #spr1 "./bozmod/Cangobia/CangobRaiderLeader.tga"
                    #spr2 "./bozmod/Cangobia/CangobRaiderLeader2.tga"
                    #name "Cangob Raid Leader" 
                    #okleader  
                    #rpcost 1
                    #rcost 4    
                    #armor 150 --bone helmet            
                    #gcost 60  
                    #end
                -- Cangob Chieftan
                    #newmonster 7117 
                    #copystats 7106
                    #name "Cangob Chieftan" 
                    #spr1 "./bozmod/Cangobia/CangobChief.tga"
                    #spr2 "./bozmod/Cangobia/CangobChief2.tga"
                    #hp 15 
                    #size 2 
                    #prot 11 
                    #mr 11 
                    #mor 13 
                    #taxcollector
                    #str 14 
                    #att 13 
                    #def 12 
                    #prec 10 
                    #ap 10
                    #rpcost 1
                    #mapmove 18    
                    #gcost 40
                    #goodleader
                    #end
                -- Cangob Tyrant
                    #newmonster 7118 
                    #copystats 7122
                    #name "Cangob Tyrant" 
                    #spr1 "./bozmod/Cangobia/CangobTyrant.tga"
                    #spr2 "./bozmod/Cangobia/CangobTyrant2.tga"
                    #hp 16 
                    #prot 5 
                    #mr 12 
                    #mor 15 
                    #str 14 
                    #att 15 
                    #def 12 
                    #prec 11 
                    #ap 11
                    #gcost 260
                    #mounted 
                    #pillage 10 
                    #fear 5
                    #inspirational 2
                    #supplybonus -10
                    #holy
                    #magicskill 0 1
                    #magicskill 6 1                    
                    #custommagic 8320 50
                    #clearweapons
                    #animal
                    #coldblood
                    #weapon "Dracon Tooth Spear"
                    #weapon 417 --big bite
                    #armor "Furs"
                    #armor 150 --bone helmet
                    #mounted
                    #expertleader
                    #secondtmpshape 7122
                    #rpcost 2
                    #end
                -- Cangob Gigant
                    #newmonster 7109 
                    #name "Cangob Gigant" 
                    #spr1 "./bozmod/Cangobia/CangobGigant.tga"
                    #hp 41 
                    #size 4 
                    #prot 5
                    #mr 10 
                    #mor 14 
                    #str 18 
                    #att 12 
                    #def 9 
                    #prec 9 
                    #ap 11
                    #mapmove 19
                    #gcost 90
                    #supplybonus -7
                    #animal
                    #rpcost 2
                    #rcost 5
                    #weapon 322 --bite
                    #magicskill 8 1
                    #spellsinger
                    #forestsurvival
                    #wasteurvival
                    #end
                -- Cangob Shamans
                    -- Fire
                        #newmonster 7119 
                        #copystats 7107
                        #montag 7004
                        #name "Cangob Shaman" 
                        #spr1 "./bozmod/Cangobia/CangobShaman.tga"
                        #spr2 "./bozmod/Cangobia/CangobShaman2.tga"
                        #okleader
                        #researchbonus -4
                        #clearweapons
                        #holy
                        #animal
                        #weapon 7 --quarter staff
                        #weapon 322 --bite
                        #armor "Furs"
                        #rpcost 2
                        #magicskill 0 1
                        #magicskill 6 1
                        #magicskill 8 1
                        #custommagic 8320 50
                        #twiceborn 7139
                        #gcost 100
                        #end
                    -- Air
                        #newmonster 7145
                        #copystats 7119 
                        #spr1 "./bozmod/Cangobia/CangobShamanAir.tga"
                        #spr2 "./bozmod/Cangobia/CangobShamanAir2.tga"
                        #clearmagic
                        #magicskill 1 1 
                        #magicskill 6 1
                        #magicskill 8 1
                        #custommagic 8448 50
                        #end
                    -- Death
                        #newmonster 7146  
                        #copystats 7119 
                        #spr1 "./bozmod/Cangobia/CangobShamanDeath.tga"
                        #spr2 "./bozmod/Cangobia/CangobShamanDeath2.tga"
                        #clearmagic
                        #armor 150 --bone helmet
                        #magicskill 5 1
                        #magicskill 6 1
                        #magicskill 8 1
                        #custommagic 12298 50
                        #end     
                -- Cangob Awakened Shaman
                    #newmonster 7138 
                    #copystats 7107
                    #name "Awakened Shaman" 
                    #spr1 "./bozmod/Cangobia/AwakeShaman.tga"
                    #spr2 "./bozmod/Cangobia/AwakeShaman2.tga"
                    #poorleader
                    #clearweapons
                    #holy
                    #animal
                    #weapon 238 --quarter staff
                    #weapon 322 --bite
                    #armor "Furs"
                    #supplybonus 5
                    #magicskill 0 1
                    #magicskill 6 1
                    #magicskill 8 1
                    #custommagic 8320 50
                    #custommagic 
                    #insane 10
                    #gcost 100
                    #end
                -- Cangob Spirit Shaman
                    #newmonster 7139 
                    #copystats 7107
                    #name "Spirit Shaman" 
                    #copyspr 3279
                    #goodleader
                    #clearweapons
                    #holy
                    #animal
                    #undead
                    #weapon 238 --magic staff
                    #weapon 322 --bite
                    #magicskill 4 1
                    #magicskill 5 1
                    #insane 10
                    #researchbonus 6
                    #homesick 100
                    #end
                -- Cangob Dragonspeaker
                    #newmonster 7120 
                    #name "Cangob Dragonspeaker" 
                    #spr1 "./bozmod/Cangobia/CangobDragonSpeaker.tga"
                    #spr2 "./bozmod/Cangobia/CangobDragonSpeaker2.tga"
                    #expertleader
                    #clearweapons
                    #holy
                    #animal
                    #weapon 7 --quarter staff
                    #weapon 322 --bite
                    #armor "Furs"
                    #supplybonus 15
                    #magicskill 0 2
                    #magicskill 6 2
                    #magicskill 8 3
                    #dragonlord 1
                    #slowrec
                    #rpcost 4
                    #custommagic 8320 100
                    #custommagic 8320 100
                    #gcost 420
                    #end
                -- Cangob Mother                
                    -- Birther 
                        #newevent
                        #req_targmnr "Cangob Mother"
                        #transform "Cangob Breeder"
                        #rarity 5
                        #req_rare 50
                        #nation -2
                        #msg "After a great feast of carrion, your breeder was able to produce a great many pups!"
                        #4d6units 7104
                        #end
                    -- Cangob Mother
                        #newmonster 7124 
                        #copystats 7105
                        #clearspec
                        #animal
                        #supplybonus -8
                        #healer 5
                        #name "Cangob Mother" 
                        #copyspr 3279
                        #gcost 120
                        #end
                    -- Cangob Breeder
                        #newmonster 7125 
                        #copystats 7105
                        #name "Cangob Breeder" 
                        #spr1 "./bozmod/Cangobia/CangobMother.tga"
                        #spr2 "./bozmod/Cangobia/CangobMother2.tga"
                        #clearspec
                        #gcost 80
                        #growhp 50 
                        #healer 3
                        #magicskill 8 1
                        #supplybonus -8
                        #animal
                        #corpseeater 30
                        #domsummon2 7104
                        #deadhp 1
                        #rpcost 1
                        #end 
                -- Longneck
                    #newmonster 7126                     
                    #copystats 2962 --Mushushu
                    #name "Dracon Longneck" 
                    #spr1 "./bozmod/Cangobia/Longneck.tga"
                    #spr2 "./bozmod/Cangobia/Longneck2.tga"                   
                    #montag 7003
                    #prot 5 
                    #mr 11 
                    #mor 12 
                    #att 11 
                    #def 5 
                    #prec 10 
                    #ap 10
                    #mapmove 8
                    #trample
                    #cleaweapons
                    #rpcost 1
                    #gcost 70
                    #rcost 1
                    #noleader
                    #drake
                    #animal
                    #makemonsters1 7127             
                    #weapon 532 --tailsweep
                    #weapon 322 --bite
                    #end
                -- Mountainspine
                    #newmonster 7131 
                    #name "The Mountainspine" 
                    #copystats 5203
                    #clearspec
                    #unique
                    #copyspr 925
                    #hp 320 
                    #size 6 
                    #prot 32 
                    #mr 24 
                    #mor 30 
                    #str 38 
                    #att 18 
                    #def 6 
                    #prec 5 
                    #ap 8
                    #mapmove 24
                    #gcost 1
                    #blind                     
                    #spellsinger
                    #inspirational 3
                    #nowish
                    #magicskill 0 3
                    #magicskill 6 3
                    #magicskill 8 4
                    #custommagic 8320 100
                    #custommagic 8320 100
                    #dragonlord 7
                    #poisonres 25
                    #fireres 15
                    #coldres 5
                    #shockres 10
                    #fear 15
                    #regen 15
                    #clearweapons
                    #weapon "Mountainspine Earthshock"
                    #weapon "Swallow"
                    #weapon "Crush"
                    #weapon "Crush"
                    #weapon "Tail Sweep"
                    #weapon 1116 --gore with charge bonus
                    #siegebonus 150
                    #swimming
                    #sailing 200 4
                    #incunrest 200
                    #popkill 80 
                    #superiorleader
                    #shatteredsoul 10
                    #end
            -- Items
                -- Discounted Items
                    #selectitem "Dragon Sceptre"
                    #nationrebate "Cangobia"
                    #end
                    #selectitem "Red Dragon Scale Mail"
                    #nationrebate "Cangobia"
                    #end
                    #selectitem "Blue Dragon Scale Mail"
                    #nationrebate "Cangobia"
                    #end
                    #selectitem "Green Dragon Scale Mail"
                    #nationrebate "Cangobia"
                    #end
                    #selectitem "Hydra Dragon Scale Mail"
                    #nationrebate "Cangobia"
                    #end
                    #selectitem "Dragonheart Banner"
                    #nationrebate "Cangobia"
                    #end
                    #selectitem "Bone Armor"
                    #nationrebate "Cangobia"
                    #end
                -- Horn of War
                    #selectitem 514
                    #name "Didgeridoo of the savage song"
                    #spr "./bozmod/Cangobia/mushroom1.tga"
                    #descr "This enchanted Didgeridoo allows the user to sing songs that inspire Cangobs into a savage frenzy"
                    #mainpath 0
                    #type 2                    
                    #mainlevel 1
                    #constlevel 2
                    #spell "Song of Savagery"
                    #weapon 238 --magic staff
                    #restricted 170
                    #end
                -- Mushroom of Awakening
                    #newitem
                    #name "Mushroom of Awakening" 
                    #noforgebonus
                    #spr "./bozmod/Cangobia/mushroom1.tga"
                    #descr "This magical mushroom awakens the user at the cost of some of their sanity. Once it's taken, the user will become addicted and always have to have a number of these ready for consumption, however the mushrooms allow the user to take flight through the astral plane with their mind, providing them with +1 to a random magic skill. The mushrooms do not take hold of the user immediately, sometimes it may take a few months."
                    #mainpath 6
                    #type 8
                    #cursed
                    #researchbonus 6                    
                    #mainlevel 2
                    #constlevel 4
                    #restricted 170
                    #end
                -- Mushroom of Insight
                    #newitem
                    #name "Mushroom of Insight" 
                    #noforgebonus
                    #spr "./bozmod/Cangobia/mushroom2.tga"
                    #descr "A common, but dangerous mushroom found in Cangobia. It allows the user to gain supernatural insight, but at a cost of some of their sanity"
                    #mainpath 6
                    #type 8
                    #cursed
                    #mainlevel 1
                    #constlevel 0
                    #shatteredsoul 10
                    #researchbonus 4
                    #restricted 170
                    #itemcost1 -80
                    #end
            -- Summons
                -- Red Dragon ------------------------------------------------------------
                    --base
                        #newmonster 7147
                        #copystats 216
                        #copyspr 216
                        #name "Dragon Lord"
                        #gcost 200
                        #clearmagic
                        #clearspec
                    --stats
                        #hp 165
                        #size 6
                        #prot 20
                        #mor 30
                        #mr 20
                        #str 27
                        #att 17
                        #def 11
                        #prec 14
                        #mapmove 28
                        #ap 14
                        #enc 2
                    --gear
                        #itemslots 274560 --[262144 + 128 + 12288] - head, 2 misc, head only crown
                    --specials
                        #montag 7005
                        #fireres 25
                        #diseaseres 100
                        #fear 10
                        #dragonlord 3
                        #flying
                        #heretic 1
                        #berserk 3
                        #heat 6
                        #inspirational 2
                        #firepower 1
                        #gold 80
                        #maxage 3000
                        #holy
                        #magicskill 0 4
                        #magicskill 8 1
                    #end
                -- White Dragon ------------------------------------------------------------
                    --base
                        #newmonster 7148
                        #copystats 265
                        #copyspr 265
                        #name "Dragon Lord"
                        #gcost 200
                        #clearmagic
                        #clearspec
                    --specials
                        #coldres 10
                        #montag 7005
                        #holy
                        #shockres 10
                        #diseaseres 100
                        #fear 10
                        #dragonlord 3
                        #flying
                        #heretic 1
                        #amphibian
                        #cold 6
                        #snowmove
                        #coldpower 1
                        #gold 80
                        #maxage 3000
                        #magicskill 2 2
                        #magicskill 1 2
                        #magicskill 8 1
                    --stats
                        #hp 145
                        #size 6
                        #prot 18
                        #mor 30
                        #mr 20
                        #str 25
                        #att 16
                        #def 16
                        #prec 13
                        #mapmove 28
                        #ap 13
                        #enc 2
                    --gear
                        #itemslots 274560 --[262144 + 128 + 12288] - head, 2 misc, head only crown
                    #end
                -- Green Dragon ------------------------------------------------------------
                    --base
                        #newmonster 7149
                        #copystats 266
                        #copyspr 266
                        #name "Dragon Lord"
                        #gcost 200
                        #clearmagic
                        #clearspec
                    --specials
                        #poisonres 25
                        #diseaseres 100
                        #montag 7005
                        #fear 10
                        #holy
                        #dragonlord 3
                        #flying
                        #heal
                        #swampsurvival
                        #swimming
                        #gold 80
                        #decscale 3
                        #heretic 1
                        #poisoncloud 4
                        #maxage 3000
                        #regeneration 12
                        #magicskill 6 4
                        #magicskill 8 1
                    --stats
                        #hp 165
                        #size 6
                        #prot 18
                        #mor 30
                        #mr 21
                        #str 24
                        #att 15
                        #def 12
                        #prec 12
                        #mapmove 25
                        #ap 11
                        #enc 2
                    --gear
                        #itemslots 274560 --[262144 + 128 + 12288] - head, 2 misc, head only crown
                    #end                        
                -- Cangob Beakwing Rider
                    #newmonster 7113 
                    #name "Beakwing Raider" 
                    #spr1 "./bozmod/Cangobia/BeakwingRider.tga"
                    #spr2 "./bozmod/Cangobia/BeakwingRider2.tga"
                    #montag 7002
                    #hp 11 
                    #size 4 
                    #prot 11 
                    #mr 11 
                    #mor 12 
                    #str 12 
                    #att 12  
                    #def 16 
                    #prec 11 
                    #ap 16
                    #mapmove 28
                    #gcost 60
                    #rcost 5
                    #rpcost 50
                    #flying 
                    #mounted
                    #drake
                    #animal
                    #coldblood                     
                    #supplybonus -2
                    #pillagebonus 2
                    #armor "Furs"
                    #holy
                    #coldblood
                    #patrolbonus 1
                    #weapon "Dracon Tooth Spear" --spear
                    #weapon 1680 --beak
                    #weapon 21 --javelin
                    #secondtmpshape 7133
                    #end
                -- Stegadon Rider
                    #newmonster 7121 
                    #name "Dracon Armorhead" 
                    #spr1 "./bozmod/Cangobia/ArmorheadRider.tga"
                    #spr2 "./bozmod/Cangobia/ArmorheadRider2.tga"
                    #hp 11 
                    #size 6 
                    #prot 11 
                    #mr 11 
                    #mor 12 
                    #str 12 
                    #att 12 
                    #def 13 
                    #prec 10 
                    #ap 8
                    #mapmove 16
                    #supplybonus -10
                    #gcost 120
                    #holy
                    #drake
                    #weapon 21 --javelin
                    #weapon 702 --gore
                    #weapon 702 --gore (lots of horns)
                    #secondshape 7134 --stegadon
                    #siege 50
                    #end
                -- T-Rex 
                    #newmonster 7122 
                    #copyspr 5237
                    #copystats 5237
                    #montag 7003
                    #name "Dracon Tyrant"
                    #holy
                    #drake
                    #end
                -- Pteradactyl Shaman
                    #newmonster 7123 
                    #name "Thunder Speaker" 
                    #spr1 "./bozmod/Cangobia/FlyingShaman.tga"
                    #spr2 "./bozmod/Cangobia/FlyingShaman2.tga"
                    #hp 12 
                    #size 4 
                    #prot 11 
                    #mr 13 
                    #mor 14
                    #str 12 
                    #att 12  
                    #def 16 
                    #prec 11 
                    #ap 16
                    #mapmove 28
                    #gcost 420
                    #flying 
                    #mounted
                    --#drake it is a drake but we don't want it to benefit from drakesummon
                    #animal
                    #coldblood 
                    #pillage 3
                    #armor "Furs"
                    #holy
                    #coldblood
                    #magicskill 1 2
                    #magicskill 6 2
                    #custommagic 8448 100
                    #custommagic 8448 50
                    #weapon 238 --magic staff
                    #weapon 1680 --beak
                    #armor 150 --bone helmet
                    #stormpower 2
                    #okleader
                    #secondtmpshape 7133
                    #end
                -- Egg    
                    -- Egg Feast
                        #newevent
                        #req_monster 7127
                        #killmon 7127
                        #req_fornation 170
                        #rarity 5
                        #req_rare 20
                        #nation -2
                        #unrest -2
                        #msg "One of your Dracon eggs was marked for a great feast by the Cangobs"
                        #nolog
                        #com 7135
                        #end

                            #newmonster 7135 
                            #name "Great Feast" 
                            #copyspr 4993
                            #copystats 7127
                            #shapechange 7127
                            #clearweapons
                            #supplybonus 100
                            #end   

                        #newevent
                        #nation 170
                        #req_targmnr 7135
                        #killcom 7135
                        #rarity 5
                        #unrest -12
                        #4d6units 7104
                        #incdom 1
                        #msg "One of your Dracon eggs was marked for a great feast by the Cangobs.
                        
                        After a great banquet, your minions are satiated and grateful for this great bounty, glory to Cangobia! After the banquet, your Cangobs had a great night as well, now you have a new litter of future warriors"
                        #end
                    -- Gigantic Egg
                        #newmonster 7127 
                        #name "Gigantic Egg" 
                        #copyspr 4993
                        #drawsize -25
                        #hp 8 
                        #size 4 
                        #prot 16 
                        #mr 11 
                        #mor 12 
                        #str 12 
                        #att 5 
                        #def 5 
                        #prec 10 
                        #ap 0
                        #minsize 
                        #clearweapons
                        #mapmove 0
                        #gcost 50
                        #coldblood
                        #animal
                        #pierceres
                        #slashres
                        #holy
                        #fireres 5
                        #poisonres 20
                        #coldres -5
                        #shockres -5
                        #shapechange 7135
                        #end
                    -- Egg Hatch
                        #newevent
                        #req_targmnr 7127
                        #killmon 7127
                        #transform -7001 --random lesser dracon
                        #rarity 5
                        #req_rare 20
                        #msg "One of your giant eggs has hatched into a Dracon!"
                        #end

                        #newevent"
                        #req_targmnr 7127
                        #killmon 7127
                        #transform -7003 --random greater dracon
                        #rarity 5
                        #domchance 1
                        #msg "One of your giant eggs has hatched into a great Dracon!"
                        #end
                -- Dragon Newt
                    #newmonster 7128 
                    #name "Dragon Newt" 
                    #copyspr 5736
                    #montag 7001
                    #hp 9 
                    #size 2 
                    #prot 5 
                    #mr 11 
                    #mor 12 
                    #str 12 
                    #att 11 
                    #def 10 
                    #prec 10 
                    #ap 11
                    #weapon 322 --bite
                    #flying
                    #fireres 5
                    #drake
                    #holy
                    #coldblood
                    #xpshape 24 --dragon adolescent
                    #mapmove 20
                    #gcost 50
                    #end           
                -- Dragon Adolescent
                    #newmonster 7129 
                    #name "Dragon Adolescent" 
                    #copyspr 6367
                    #hp 36 
                    #size 4 
                    #prot 10 
                    #mr 12 
                    #mor 12 
                    #str 15 
                    #att 13 
                    #def 10 
                    #prec 10 
                    #ap 12
                    #holy
                    #drake
                    #weapon 567 --drake fire
                    #weapon 20 --bite
                    #weapon 33 --claws
                    #flying
                    #coldblood 
                    #fireres 15
                    #coldres -5
                    #mapmove 24
                    #gcost 120
                    #end                    
                -- Beakwing
                    #newmonster 7133 
                    #name "Beakwing" 
                    #spr1 "./bozmod/Cangobia/Beakwing.tga"
                    #spr2 "./bozmod/Cangobia/Beakwing2.tga"
                    #hp 24 
                    #size 4 
                    #montag 7001
                    #prot 9 
                    #mr 10 
                    #mor 14 
                    #str 14 
                    #att 13 
                    #def 9 
                    #prec 8 
                    #ap 16
                    #mapmove 22
                    #gcost 50
                    #coldblood
                    #drake
                    #animal
                    #holy
                    #weapon 322 --bite
                    #armor "Furs"
                    #weapon 33 --claws
                    #end    
                -- Stegadon
                    #newmonster 7134 
                    #copystats 1234 --asp turtle 
                    #name "Armorhead"              
                    #spr1 "./bozmod/Cangobia/Armorhead.tga"
                    #spr2 "./bozmod/Cangobia/Armorhead2.tga"                       
                    #montag 7003
                    #clearspec
                    #animal
                    #trample
                    #siegebonus 30
                    #mapmov 18
                    #berserk 1
                    #supplybonus -10
                    #coldblood
                    #fear 5
                    #weapon 399 --gore
                    #weapon 399 --gore
                    #drake
                    #end 
                -- Dragonfang Spider
                    #newmonster 7136
                    #copystats 2223
                    #name "Dragonfang Spider"
                    #descr "The Dragons of Cangobia sometimes require a clandestine method to kill on their rivals, to avoid retribution from family members they have bred an arachnid with a venom so potent that a single bite could slay a dragon. 
                    Now that Cangobia is making an attempt at world domination, these spiders have become a key tool to slay powerful foes"
                    #copyspr 2223 --large spider
                    #clearweapons
                    #weapon "Consume Soul"
                    #end                            
                -- Redskull Spider
                    #newmonster 7137
                    #copystats 2223
                    #name "Redskull Spider"
                    #descr "Redskull spiders are highly venomous insects native to Cangobia, while not as deadly as Dragonfang Spiders, their bite is still extremely lethal."
                    #copyspr 2223 --large spider
                    #clearweapons
                    #weapon 52 --death poison
                    #end   
            -- Heroes
                -- Draconchild
                    #newmonster 7140 
                    #copystats 7110
                    #name "Draconchild" 
                    #copyspr 7110
                    #expertleader
                    #magicskill 0 2
                    #magicskill 1 2
                    #magicskill 6 1
                    #magicskill 8 3
                    #dragonlord 1
                    #custommagic 8320 100
                    #custommagic 8320 50
                    #end
            -- Pretenders
            -- Sites
                    #newsite 1693
                    #name "Dragon Den" 
                    #path 0
                    #level 0
                    #gems 0 2
                    #gems 1 1
                    #gems 5 1
                    #gems 6 2
                    #res 100
                    #homemon 7110 --dragonguard
                    #homecom 7118 --cangob tyrant
                    #homecom 7120 --cangob dragonspeaker
                    #rarity 5
                    #end
            -- Nation Info
                -- Base
                    #selectnation 170
                    #name "Cangobia"
                    #epithet "Chittering Hordes"
                    #era 2
                    #descr "A nation of dog headed humanoids called Cangobs, that live in a tribal society and worship various Dracons. The Dracons and Cangobs come in many subspecies with a great variety in physical and mental attributes. While Cangobs have a gradient between each individuals' differences, Dracons are split into two distinct groups, the non-sentient kind which resemble ancient dinosaurs and sentient ones, such as Dragons. Cangobians live on the same continent as the Alcherans, now the Dragon lords of Cangobia have learned from their rivals of an outside world and wish to extend their tumultuous domain across the new lands."
                    #summary "Race: Cangobs and Dracons
                    Military: Animal light infantry, Shamans, Dracons filling other roles
                    Magic: Fire, Nature, access to air, astral and death via summons
                    Priests: Strong."
                    #brief "Cangobians are savage tribes of canine-headed humanoids led by their ambitious Dragon lords"
                    #color 0.8 0.6 0
                    #flag "./bozmod/Cangobia/Flag.tga"
                    #startsite "Dragon Den" --dragon den
                    #fortera 0
                    #domdeathsense
                    #fortcost 50
                    #labcost 300
                    #forestlabcost 200                    
                    #templecost 300
                    #foresttemplecost 200
                    #templepic 10
                    #hatesterr 128
                    #natureblessbonus 1                    
                    #fireblessbonus 1
                    #idealcold -1
                    #ainaturenation
                    #aifirenation
                    #aicheapholy
                    #multihero1 7140
                    #aigoodbless 100
                    #merccost 50
                    #hatesterr 4194592 --farm swamp mountain
                -- Start Units 
                    #startcom 7117 --chieftan
                    #startscout 7115 --scout
                    #startunittype1 7106 --soldier
                    #startunitnbrs1 15
                    #startunittype2 7107 -- hunter
                    #startunitnbrs2 12                    
                -- PD             
                    #defcom1 7117 --chief
                    #defcom2 7119 --shaman
                    #defunit1 7106 --soldier
                    #defunit2 7107  --hunter                    
                    #defmult1 20 
                    #defmult2 20
                -- Fort PD 
                    #wallcom 7117
                    #wallunit 7106
                    #wallmult 2
                -- Recruitable Troops      
                    #addforeignunit 7105
                    #addforeignunit 7106
                    #addforeignunit 7107
                    #addforeignunit 7108
                    #addforeignunit 7111
                    #addforeignunit 7112

                    #addrecunit 7105
                    #addrecunit 7106
                    #addrecunit 7107
                    #addrecunit 7108
                    #addrecunit 7109
                    #addrecunit 7111
                    #addrecunit 7112
                -- Recruitable Commanders      
                    #addforeigncom 7115
                    #addforeigncom 7116
                    #addforeigncom 7117
                    #addforeigncom 7119
                    #addforeigncom 7125
                    #addforeigncom 7109

                    #addreccom 7115
                    #addreccom 7116
                    #addreccom 7117
                    #addreccom 7119
                    #addreccom 7109
                    #addreccom 7125

                    #mountaincom 7145
                    #swampcom 7146
                -- Dragon Discount
                    #cheapgod20 "King of Beasts"    
                    #cheapgod20 "Bane Dragon"      
                    #cheapgod20 "Shadow Dragon"
                    #cheapgod20 "Mutant Dragon"        
                    #cheapgod20 "Crystal Dragon"
                    #cheapgod20 "Fairy Dragon"
                    #cheapgod20 "Sanguine Dragon"
                    #cheapgod20 "Psionic Dragon"
                    #cheapgod20 "Dragon of Blinding Virtue"
                    #cheapgod20 216 --red drag
                    #cheapgod20 265  --b drag
                    #cheapgod20 266 --g drag
                    #cheapgod20 7144 --cosmic egg
                    #cheapgod20 4993 
                    #cheapgod20 "Black Dragon"
                -- Available Gods 
                    --#homerealm 7 --only dragons
                    #end
    -- Adding Pretenders
        #selectnation 5	--Arcoscephale	Golden Era
        #addgod "Fairy Dragon"
        #addgod "Crystal Dragon"
        #end
        
        #selectnation 6	--Ermor	New Faith
        #addgod "Fairy Dragon"
        #addgod "Crystal Dragon"
        #end
        
        #selectnation 7	--Ulm	Enigma of Steel
        #addgod "Crystal Dragon"
        #end
        
        #selectnation 8	--Marverni	Time of Druids
        #addgod "Fairy Dragon"
        #addgod "Crystal Dragon"
        #addgod "Mutant Dragon"
        #end
        
        #selectnation 9	--Sauromatia	Amazon Queens
        #addgod "Fairy Dragon"
        #addgod "The Singularity"
        #addgod "Dracolich"
        #addgod "Soulless Dragon"
        #addgod "Mutant Dragon"
        #addgod "Sanguine Dragon"
        #end
        
        #selectnation 10	--T’ien Ch’i	Spring and Autumn
        #addgod "Crystal Dragon"
        #end
        
        #selectnation 11	--Machaka	Lion Kings
        #addgod "Fairy Dragon"
        #addgod "Crystal Dragon"
        #addgod "Mutant Dragon"
        #addgod "King of Beasts"
        #end
        
        #selectnation 12	--Mictlan	Reign of Blood
        #addgod "The Singularity"
        #addgod "Mutant Dragon"
        #addgod "Sanguine Dragon"
        #end
        
        #selectnation 13	--Abysia	Children of Flame
        #addgod "The Singularity"
        #addgod "Mutant Dragon"       
        #addgod "King of Beasts"
        #end
        
        #selectnation 14	--Caelum	Eagle Kings
        #addgod "Fairy Dragon"
        #end
        
        #selectnation 15	--C’tis	Lizard Kings
        #addgod "Crystal Dragon"
        #addgod "Fairy Dragon"
        #addgod "The Singularity"
        #addgod "Dracolich"
        #addgod "Shadow Dragon"
        #addgod "Soulless Dragon"
        #addgod "Mutant Dragon"        
        #addgod "King of Beasts"
        #end
        
        #selectnation 16	--Pangaea	Age of Revelry
        #addgod "Fairy Dragon"
        #addgod "Mutant Dragon"
        #addgod "Sanguine Dragon"
        #end
        
        #selectnation 17	--Agartha	Pale Ones
        #addgod "Crystal Dragon"
        #addgod "Dracolich"        
        #addgod "King of Beasts"
        #end
        
        #selectnation 18	--Tir na n’Og	Land of the Ever Young
        #addgod "Fairy Dragon"
        #addgod "Crystal Dragon"
        #end
        
        #selectnation 19	--Fomoria	The Cursed Ones
        #addgod "The Singularity"
        #addgod "Dracolich"
        #end
        
        #selectnation 20	--Vanheim	Age of Vanir
        #addgod "Fairy Dragon"
        #addgod "Crystal Dragon"
        #addgod "Sanguine Dragon"
        #end
        
        #selectnation 21	--Helheim	Dusk and Death
        #addgod "Fairy Dragon"
        #addgod "Crystal Dragon"
        #addgod "The Singularity"
        #addgod "Dracolich"
        #addgod "Bane Dragon"
        #addgod "Slayer of apostles"
        #end
        
        #selectnation 22	--Niefelheim	Sons of Winter
        #addgod "Crystal Dragon"
        #addgod "The Singularity"
        #addgod "Mutant Dragon"
        #addgod "Sanguine Dragon"
        #end
        
        #selectnation 24	--Rus	Sons of Heaven
        #addgod "Fairy Dragon"
        #addgod "Dracolich"
        #end
        
        #selectnation 25	--Kailasa	Rise of the Ape Kings
        #addgod "Fairy Dragon"
        #addgod "Crystal Dragon"
        #end
        
        #selectnation 26	--Lanka	Land of Demons
        #addgod "The Singularity"
        #addgod "Dracolich"
        #addgod "Bane Dragon"
        #addgod "Mutant Dragon"
        #addgod "Sanguine Dragon"
        #end
        
        #selectnation 27	--Yomi	Oni Kings
        #addgod "The Singularity"
        #addgod "Dracolich"
        #addgod "Bane Dragon"
        #addgod "Shadow Dragon"
        #addgod "Soulless Dragon"
        #addgod "Sanguine Dragon"
        #addgod "Slayer of apostles"
        #end
        
        #selectnation 28	--Hinnom	Sons of the Fallen
        #addgod "The Singularity"
        #addgod "Dracolich"
        #addgod "Sanguine Dragon"
        #end
        
        #selectnation 29	--Ur	The First City
        #addgod "Crystal Dragon"
        #addgod "Fairy Dragon"
        #end
        
        #selectnation 30	--Berytos	Phoenix Empire
        #addgod "Crystal Dragon"
        #addgod "The Singularity"
        #addgod "Dracolich"
        #addgod "Sanguine Dragon"
        #end
        
        #selectnation 31	--Xibalba	Vigil of the Sun
        #addgod "The Singularity"
        #addgod "Dracolich"
        #addgod "Bane Dragon"
        #addgod "Mutant Dragon"
        #addgod "Sanguine Dragon"
        #end
        
        #selectnation 32	--Mekone	Brazen Giants
        #addgod "Crystal Dragon"
        #end
        
        #selectnation 33	--Ubar	Kingdom of the Unseen
        #end
        
        #selectnation 36	--Atlantis	Emergence of the Deep Ones
        #end
        
        #selectnation 37	--R’lyeh	Time of Aboleths
        #addgod "Psionic Dragon"
        #addgod "Dracobolith"
        #end
        
        #selectnation 38	--Pelagia	Pearl Kings
        #addgod "Dracobolith"
        #end
        
        #selectnation 39	--Oceania	Coming of the Capricorns
        #end
        
        #selectnation 40	--Therodos	Telkhine Spectre
        #addgod "The Singularity"
        #addgod "Dracolich"
        #addgod "Shadow Dragon"
        #addgod "Soulless Dragon"        
        #addgod "King of Beasts"
        #end
        
        #selectnation 43	--Arcoscephale	The Old Kingdom
        #addgod "Fairy Dragon"
        #addgod "Crystal Dragon"
        #end
        
        #selectnation 44	--Ermor	Ashen Empire
        #addgod "The Singularity"
        #addgod "Dracolich"
        #addgod "Bane Dragon"
        #addgod "Slayer of apostles"
        #addgod "Soulless Dragon"
        #end
        
        #selectnation 45	--Sceleria	Reformed Empire
        #addgod "The Singularity"
        #addgod "Dracolich"
        #addgod "Bane Dragon"
        #addgod "Shadow Dragon"
        #addgod "Slayer of apostles"
        #addgod "Soulless Dragon"
        #end
        
        #selectnation 46	--Pythium	Emerald Empire
        #addgod "Crystal Dragon"
        #addgod "Fairy Dragon"
        #end
        
        #selectnation 47	--Man	Tower of Avalon
        #addgod "Crystal Dragon"
        #addgod "Fairy Dragon"
        #end
        
        #selectnation 48	--Eriu	Last of the Tuatha
        #addgod "Crystal Dragon"
        #addgod "Fairy Dragon"
        #end
        
        #selectnation 49	--Ulm	Forges of Ulm
        #addgod "Crystal Dragon"
        #end
        
        #selectnation 50	--Marignon	Fiery Justice
        #addgod "Crystal Dragon"
        #end
        
        #selectnation 51	--Mictlan	Reign of the Lawgiver
        #addgod "Crystal Dragon"
        #addgod "The Singularity"
        #addgod "Mutant Dragon"
        #addgod "Sanguine Dragon"
        #end
        
        #selectnation 52	--T’ien Ch’i	Imperial Bureaucracy
        #addgod "Crystal Dragon"
        #end
        
        #selectnation 53	--Machaka	Reign of Sorcerors
        #addgod "Crystal Dragon"
        #addgod "Dracolich"        
        #addgod "King of Beasts"
        #end
        
        #selectnation 54	--Agartha	Golem Cult
        #addgod "Crystal Dragon"
        #end
        
        #selectnation 55	--Abysia	Blood and Fire
        #addgod "The Singularity"
        #addgod "Dracolich"
        #addgod "Mutant Dragon"
        #addgod "Bane Dragon"
        #addgod "Sanguine Dragon"        
        #addgod "King of Beasts"
        #end
        
        #selectnation 56	--Caelum	Reign of the Seraphim
        #addgod "Crystal Dragon"
        #addgod "Fairy Dragon"
        #end
        
        #selectnation 57	--C’tis	Miasma
        #addgod "The Singularity"
        #addgod "Dracolich"
        #addgod "Bane Dragon"
        #addgod "Shadow Dragon"
        #addgod "Soulless Dragon"
        #addgod "Mutant Dragon"
        #end
        
        #selectnation 58	--Pangaea	Age of Bronze
        #addgod "Fairy Dragon"
        #addgod "Mutant Dragon"
        #end
        
        #selectnation 59	--Asphodel	Carrion Woods
        #addgod "Fairy Dragon"
        #addgod "The Singularity"
        #addgod "Dracolich"
        #addgod "Soulless Dragon"
        #end
        
        #selectnation 60	--Vanheim	Arrival of Man
        #addgod "Crystal Dragon"
        #addgod "Fairy Dragon"
        #addgod 7144
        #end
        
        #selectnation 61	--Jotunheim	Iron Woods
        #addgod "The Singularity"
        #addgod "Dracolich"
        #addgod "Mutant Dragon"
        #addgod "Sanguine Dragon"
        #end
        
        #selectnation 62	--Vanarus	Land of the Chuds
        #addgod "Crystal Dragon"
        #addgod "Fairy Dragon"
        #addgod 7144
        #end
        
        #selectnation 63	--Bandar Log	Land of the Apes
        #addgod "Crystal Dragon"
        #addgod "Fairy Dragon"
        #end
        
        #selectnation 64	--Shinuyama	Land of the Bakemono
        #addgod "The Singularity"
        #end
        
        #selectnation 65	--Ashdod	Reign of the Anakim
        #addgod "Crystal Dragon"
        #end
        
        #selectnation 66	--Uruk	City States
        #addgod "Crystal Dragon"
        #addgod "Fairy Dragon"
        #end
        
        #selectnation 67	--Nazca	Kingdom of the Sun
        #addgod "Crystal Dragon"
        #addgod "The Singularity"
        #addgod "Dracolich"
        #addgod "Shadow Dragon"
        #end
        
        #selectnation 68	--Xibalba	Flooded Caves
        #addgod "The Singularity"
        #addgod "Sanguine Dragon"
        #end
        
        #selectnation 69	--Phlegra	Deformed Giants
        #addgod "Crystal Dragon"
        #end
        
        #selectnation 70	--Phaeacia	Isle of the Dark Ships
        #addgod "Crystal Dragon"
        #end
        
        #selectnation 71	--Ind Magn	ificent Kingdom of Exalted Virtue
        #addgod "Crystal Dragon"
        #addgod "Fairy Dragon"
        #end
        
        #selectnation 72	--Na’Ba	Queens of the Desert
        #addgod "Crystal Dragon"
        #end
        
        #selectnation 73	--Atlantis	Kings of the Deep
        #end
        
        #selectnation 74	--R’lyeh	Fallen Star
        #addgod "Psionic Dragon"
        #addgod "Dracobolith"
        #end
        
        #selectnation 75	--Pelagia	Triton Kings
        #addgod "Dracobolith"
        #end
        
        #selectnation 76	--Oceania	Mermidons
        #addgod "Dracobolith"
        #end
        
        #selectnation 77	--Ys	Morgen Queens
        #addgod "Fairy Dragon"
        #end
        
        #selectnation 80	--Arcoscephale	Sibylline Guidance
        #addgod "Crystal Dragon"
        #addgod "Fairy Dragon"
        #end
        
        #selectnation 81	--Pythium	Serpent Cult
        #addgod "Crystal Dragon"
        #addgod "Fairy Dragon"
        #end
        
        #selectnation 82	--Lemuria	Soul Gate
        #addgod "The Singularity"
        #addgod "Dracolich"
        #addgod "Bane Dragon"
        #addgod "Shadow Dragon"
        #addgod "Soulless Dragon"
        #addgod "Slayer of apostles"
        #end
        
        #selectnation 83	--Man	Towers of Chelms
        #addgod "Crystal Dragon"
        #addgod "Fairy Dragon"
        #end
        
        #selectnation 84	--Ulm	Black Forest
        #addgod "Crystal Dragon"
        #addgod "Dracolich"
        #addgod "The Singularity"
        #addgod "Bane Dragon"
        #addgod "Sanguine Dragon"
        #addgod "Soulless Dragon"
        #addgod "Slayer of apostles"
        #end
        
        #selectnation 85	--Marignon	Conquerors of the Sea
        #addgod "Crystal Dragon"
        #addgod "Sanguine Dragon"
        #end
        
        #selectnation 86	--Mictlan	Blood and Rain
        #addgod "The Singularity"
        #addgod "Sanguine Dragon"
        #end
        
        #selectnation 87	--T’ien Ch’i	Barbarian Kings
        #addgod "Crystal Dragon"
        #addgod 7144
        #end
        
        #selectnation 89	--Jomon	Human Daimyos
        #addgod "Crystal Dragon"
        #end
        
        #selectnation 90	--Agartha	Ktonian Dead
        #addgod "Dracolich"
        #addgod "Crystal Dragon"
        #addgod "Slayer of apostles"
        #end
        
        #selectnation 91	--Abysia	Blood of Humans
        #addgod "Crystal Dragon"
        #addgod "The Singularity"
        #addgod "Sanguine Dragon"        
        #addgod "King of Beasts"
        #end
        
        #selectnation 92	--Caelum	Return of the Raptors
        #addgod 7144
        #end
        
        #selectnation 93	--C’tis	Desert Tombs
        #addgod "Dracolich"
        #addgod "The Singularity"
        #addgod "Soulless Dragon"
        #addgod "Bane Dragon"
        #addgod "Slayer of apostles"
        #end
        
        #selectnation 94	--Pangaea	New Era
        #addgod "Fairy Dragon"
        #end
        
        #selectnation 95	--Midgård	Age of Men
        #addgod "Crystal Dragon"
        #addgod "Fairy Dragon"
        #end
        
        #selectnation 96	--Utgård	Well of Urd
        #addgod "Crystal Dragon"
        #addgod "Fairy Dragon"
        #end
        
        #selectnation 97	--Bogarus	Age of Heroes
        #addgod "Crystal Dragon"
        #addgod "The Singularity"
        #addgod "Dracolich"
        #addgod "Sanguine Dragon"
        #addgod 7144
        #end
        
        #selectnation 98	--Patala	Reign of the Nagas
        #addgod "Crystal Dragon"
        #addgod "Fairy Dragon"
        #end
        
        #selectnation 99	--Gath	Last of the Giants
        #addgod "Crystal Dragon"
        #end
        
        #selectnation 100	--Ragha	Dual Kingdom
        #addgod "Crystal Dragon"
        #addgod "Sanguine Dragon"
        #addgod 7144
        #end
        
        #selectnation 101	--Xibalba	Return of the Zotz
        #addgod "The Singularity"
        #addgod "Dracolich"
        #addgod "Sanguine Dragon"
        #end
        
        #selectnation 102	--Phlegra	Sleeping Giants
        #addgod "Crystal Dragon"
        #end
        
        #selectnation 106	--Atlantis	Frozen Sea
        #end
        
        #selectnation 107	--R’lyeh	Dreamlands
        #addgod "Psionic Dragon"
        #end
        
        #selectnation 108	--Erytheia	Kingdom of Two Worlds
        #addgod "Crystal Dragon"
        #addgod 7144
        #end

        #selectnation 169	--Diamedulla
        #addgod "The Singularity"
        #addgod "Soulless Dragon"
        #addgod "Sanguine Dragon"
        #end       
        
        #selectnation 170   --Cangobia Fang and Feather
        #addgod "King of Beasts"    
        #addgod "Bane Dragon"      
        #addgod "Shadow Dragon"
        #addgod "Mutant Dragon"        
        #addgod "Crystal Dragon"
        #addgod "Fairy Dragon"
        #addgod "Sanguine Dragon"
        #addgod "Psionic Dragon"
        #addgod "Dragon of Blinding Virtue"
        #addgod 216 --red drag
        #addgod 265  --b drag
        #addgod 266 --g drag
        #addgod 7144 --cosmic egg
        #addgod "Black Dragon"
        #addgod 4993
        #end
